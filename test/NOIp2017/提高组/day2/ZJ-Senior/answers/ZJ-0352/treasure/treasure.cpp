#include<bits/stdc++.h>
using namespace std;
int v=-1,n,m;
int mp[20][20];
int dep[20];
int res=0x3f3f3f3f;
int vis[20];
queue<int> Q;
void dfs(int tot,int vd){
	int ff;
	if(vd==n){
		res=tot;return;
	}
	for(int i=1;i<=vd;++i){
		for(int j=1;j<=n;++j){
			if(!dep[j]&&mp[i][j]!=0x3f3f3f3f&&(ff=(dep[vis[i]]*mp[vis[i]][j]+tot))<res){
				vis[1+vd]=j;
				dep[j]=dep[vis[i]]+1;
				dfs(ff,vd+1);dep[j]=0;
			}
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(mp,0x3f,sizeof mp);
	int F=0;
	for(int i=1,x,y,z;i<=m;++i){scanf("%d%d%d",&x,&y,&z);mp[x][y]=mp[y][x]=min(mp[x][y],z);if(v==-1)v=z;if(v!=-1&&v!=z)F=1;}
	if(!F){
		int mi=0x3f3f3f3f;
		for(int i=1;i<=n;++i){
			memset(dep,0,sizeof dep);
			dep[i]=1;
			Q.push(i);
			while(!Q.empty()){
				int x=Q.front();Q.pop();
				for(int j=1;j<=n;++j){
					if(!dep[j])dep[j]=dep[x]+1,Q.push(x);
				}
			}
			int res=0;
			for(int j=1;j<=n;++j){
				res+=v*dep[j]-1;
			}
			mi=min(mi,res);
		}
		printf("%d",mi);
	}else{
		for(int i=1;i<=n/4;++i){
			dep[i*4]=1;vis[1]=i*4;
			dfs(0,1);dep[i*4]=0;
			dep[i*4-1]=1;vis[1]=i*4-1;
			dfs(0,1);dep[i*4-1]=0;
			dep[i*4-2]=1;vis[1]=i*4-2;
			dfs(0,1);dep[i*4-2]=0;
			dep[i*4-3]=1;vis[1]=i*4-3;
			dfs(0,1);dep[i*4-3]=0;
		}
		for(int i=n/4*4+1;i<=n;++i){
			dep[i]=1;vis[1]=i;
			dfs(0,1);dep[i]=0;
		}
		printf("%d",res);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int F[5005];
int gf(int x){
	return F[x]==x?F[x]:(F[x]=gf(F[x]));
}
struct Node{
	int x,y,z;
}N[5005];
int n,h,r;long long R;
void add(int x,int y){
	F[gf(x)]=gf(y);
}
long long ds(Node A,Node B){
	return 1ll*(A.x-B.x)*(A.x-B.x)+1ll*(A.y-B.y)*(A.y-B.y)+1ll*(A.z-B.z)*(A.z-B.z);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r);
		for(int i=0;i<=n+1;++i)F[i]=i;
		for(int i=1;i<=n;++i){
			scanf("%d%d%d",&N[i].x,&N[i].y,&N[i].z);
			if(N[i].z<=r)add(0,i);
			if(N[i].z+r>=h)add(i,n+1);
		}
		R=1ll*(r+r)*(r+r);
		for(int i=1;i<=n;++i){
			for(int j=i+1;j<=n;++j){
				if(ds(N[i],N[j])<=R)add(i,j);
			}
		}
		if(gf(0)==gf(n+1))puts("Yes");else puts("No");
	}
	return 0;
}

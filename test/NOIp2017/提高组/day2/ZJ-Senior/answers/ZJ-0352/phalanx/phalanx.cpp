#include<bits/stdc++.h>
#define lb(x) (x&(-x))
using namespace std;
int n,m,q;
struct Node{
	int x,y,id;
}N[300005];
bool operator<(Node A,Node B){
	return A.x<B.x||A.x==B.x&&A.y<B.y||A.x==B.x&&A.y==B.y&&A.id<B.id;
}
int f[1005][1005];
int c[600005],ans[300005];
int las[600005],ud[600005],tl;
void add(int x,int v){
	while(x<=tl){
		c[x]+=v;
		x+=lb(x);
	}
}
int qry(int x){
	int res=0;
	while(x){
		res+=c[x];
		x-=lb(x);
	}return res;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(q<=500){
		for(int i=1;i<=n;++i){
			for(int j=1;j<=m;++j){
				f[i][j]=(i-1)*m+j;
			}
		}
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			int v=f[x][y];
			for(int i=y+1;i<=m;++i)f[x][i-1]=f[x][i];
			for(int i=x+1;i<=n;++i)f[i-1][m]=f[i][m];
			printf("%d\n",v);
			f[n][m]=v;
		}
	}else{
		for(int i=1;i<=m;++i)las[i]=i;
		for(int i=2;i<=n;++i)las[i+m-1]=i*m;
		for(int i=1;i<=q;++i){
			scanf("%d%d",&N[i].x,&N[i].y);
			N[i].id=i;
		}
		sort(N+1,N+q+1);
		tl=n+q;
		for(int i=1;i<=q;++i){
			ans[N[i].id]=las[N[i].y+qry(N[i].id)];
			if(ans[N[i].id]==0)ud[N[i].id]=1,ans[N[i].id]=N[i].y+qry(N[i].id);
			else las[n+N[i].id+m-1]=ans[N[i].id];add(N[i].id,1);
		}
		for(int i=1;i<=q;++i){
			if(ud[i]==1)printf("%d",las[n+i+m-1]=las[ans[i]]);
			printf("%d\n",ans[i]);
		}
	}
	return 0;
}

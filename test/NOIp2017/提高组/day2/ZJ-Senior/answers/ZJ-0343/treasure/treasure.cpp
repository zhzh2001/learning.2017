#include<iostream>
#include<cstring>
#include<cstdio>
using namespace std;
long long ans=0x7fffffffffffffff,ma[20][20],cc[20],n,m;
bool e[21];
void search(long long k,long long va){
	if(va>=ans)return;
	if(k<=n-1){
		for(long long i=1;i<=n;i++)if(e[i]){
			for(long long j=1;j<=n;j++)if(!e[j]&&ma[i][j]!=-1){
				cc[j]=cc[i]+1;
				e[j]=1;
				search(k+1,va+ma[i][j]*cc[i]);
				e[j]=0;
			}
		}
	}
	else{
		ans=min(ans,va);
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	memset(ma,-1,sizeof ma);
	for(long long i=1;i<=m;i++){
		long long x,y,z;
		cin>>x>>y>>z;
		if(ma[x][y]==-1)ma[x][y]=z,ma[y][x]=z;
		else ma[x][y]=min(ma[x][y],z),ma[y][x]=ma[x][y];
	}
	for(long long i=1;i<=n;i++){
		e[i]=1;cc[i]=1;
		search(1,0);
		e[i]=0;
	}
	cout<<ans;
	return 0;
}

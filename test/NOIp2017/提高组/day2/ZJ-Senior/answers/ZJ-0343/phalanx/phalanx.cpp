#include<iostream>
#include<map>
#include<cstdio>
using namespace std;
long long n,m,q,num[1001][1001],que[3001010],t[3001010],x[301010],y[301010],tail;
map<long long,long long>ma;
void add(long long x){
	for(long long i=x;i<=m;i+=i&(-i)){
		t[i]++;
	}
}
long long ask(long long x){
	long long ans=0;
	for(long long i=x;i>=1;i-=i&(-i))ans+=t[i];
	return ans;
}
inline long long read(){
	long long zhi=0;
	char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))zhi=(zhi<<1)+(zhi<<3)+c-'0',c=getchar();
	return zhi;
}
inline void pu(long long x){
	long long cnt=0;
	char c[30];
	while(x){
		c[++cnt]=x%10+'0';
		x/=10;
	}
	for(long long i=cnt;i>=1;i--)cout<<c[i];
	putchar('\n');
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	cin>>n>>m>>q;
	if(n<=1000&&m<=1000){
			long long cc=0;
			for(long long i=1;i<=n;i++)for(long long j=1;j<=m;j++)num[i][j]=++cc;
			for(long long i=1;i<=q;i++){
				long long x,y;
				cin>>x>>y;
				long long zhi=num[x][y];
				cout<<zhi<<endl;
				for(long long j=y+1;j<=m;j++)num[x][j-1]=num[x][j];
				for(long long j=x+1;j<=n;j++)num[j-1][m]=num[j][m];
				num[n][m]=zhi;
		}
			return 0;
	}
	else if(n<=50000&&m<=50000&&q<=500){
		for(long long i=1;i<=q;i++){
			long long x=read(),y=read();
			if(!ma[1LL*(x-1)*m+y])pu(1LL*(x-1)*m+y);
			else pu(ma[1LL*(x-1)*m+y]);
			for(long long j=y+1;j<=m;j++){
				long long o=1LL*(x-1)*m+j,p=1LL*(x-1)*m+j-1;
				if(!ma[o])ma[p]=o;
				else ma[p]=ma[o];
			}
			for(long long j=x+1;j<=n;j++){
				long long o=1LL*(j-1)*m+m,p=1LL*(j-2)*m+m;
				if(!ma[o])ma[p]=o;
				else ma[p]=ma[o];
			}
			ma[n*m]=1LL*(x-1)*m+y;
		}
	}
	else{
		bool flag=1;
		for(long long i=1;i<=q;i++){
			x[i]=read(),y[i]=read();
			if(x[i]!=1)flag=0;
		}
		if(flag){
			for(long long i=1;i<=m;i++)que[i]=i,add(i);
			for(long long i=2;i<=n;i++)que[i+m-1]=que[i+m-2]+m;
			tail=n+m-1;
			for(long long i=1;i<=q;i++){
				long long zhi=ask(y[i]);
				pu(que[zhi]);
				add(y[i]);
				que[++tail]=que[zhi];
				que[zhi]=-1;
			}
		}
	}
	return 0;
}

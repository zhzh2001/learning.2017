#include<cstdio>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)

using namespace std;

const int N=1005;

int n,m,fa[N],a[N][N],mp[N][N],k[N][N],x[N],y[N],z[N],d[N],ans=1<<30;

int getf(int x) { return fa[x]==x?x:getf(fa[x]); }

void tour(int x,int dp,int fa,int &key) {
	For (i,1,d[x]) if (mp[x][i]!=fa) {
		key+=dp*k[x][i];
		tour(mp[x][i],dp+1,x,key);
	}
	return;
}

int cal() {
	int ret=1<<30,s;
	For (i,1,n) s=0,tour(i,1,-1,s),ret=min(ret,s);
	return ret;
}

void dfs(int i,int num) {
	if (num==n-1) {
		ans=min(ans,cal());
		return;
	}
	if (i>m || m-i+1+num<n-1) return;
	
	dfs(i+1,num);
	int fx=getf(x[i]),fy=getf(y[i]);
	if (fx!=fy) {
		fa[fx]=fy;
		mp[x[i]][++d[x[i]]]=y[i];
		k [x[i]][  d[x[i]]]=z[i];
		mp[y[i]][++d[y[i]]]=x[i];
		k [y[i]][  d[y[i]]]=z[i];
		dfs(i+1,num+1);
		d[x[i]]--,d[y[i]]--;
		fa[fx]=fx;
	}
	
	return;
}

int main() {
	
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	For (i,1,n) For (j,1,n) a[i][j]=1<<30;
	For (i,1,m) {
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if (x==y) continue;
		a[x][y]=min(a[x][y],z);
		a[y][x]=min(a[y][x],z);
	}
	m=0;
	For (i,1,n) For (j,i+1,n) if (a[i][j]<(1<<30))
		m++,x[m]=i,y[m]=j,z[m]=a[i][j];
	
	For (i,1,n) fa[i]=i;
	dfs(1,0);
	printf("%d\n",ans);
	
	return 0;
}

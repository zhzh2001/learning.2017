#include<cstdio>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)
#define Dep(i,x,y) for(int i=x;i>=y;i--)

using namespace std;

typedef long long LL;

const int N=3e5+35;

int rd() {
	char c=getchar(); int t=0;
	while (c<'0' || c>'9') c=getchar();
	while (c>='0' && c<='9') t=t*10+c-48,c=getchar(); return t;
}

LL wt(LL x) {
	if (x<=9) putchar(x+48);
	else wt(x/10),putchar(x%10+48);
}

LL ans,id[(N*3)<<2];
int n,m,q,X[N],Y[N],sum[(N*3)<<2],x,y,len,R;

#define mid (L+R>>1)

void modify(int o,int L,int R,int x,LL ID) {
	if (L==R) {
		sum[o]=1,id[o]=ID;
		return;
	}
	if (x<=mid) modify(o<<1,L,mid,x,ID);
	else modify(o<<1|1,mid+1,R,x,ID);
	sum[o]=sum[o<<1]+sum[o<<1|1]; return;
}
void find(int o,int L,int R,int rk) {
	if (L==R) {
		ans=id[o],sum[o]=0;
		return;
	}
	if (sum[o<<1]>=rk) find(o<<1,L,mid,rk);
	else find(o<<1|1,mid+1,R,rk-sum[o<<1]);
	sum[o]=sum[o<<1]+sum[o<<1|1];
}

int main() {
	
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	n=rd(),m=rd(),q=rd();
	
	if (q<=3000) {
		For (i,1,q) {
			X[i]=rd(),Y[i]=rd();
			
			x=X[i],y=Y[i];
			Dep (j,i-1,1) {
				if (x==n && y==m) x=X[j],y=Y[j];
				else if (x==X[j]) {
					if (y<Y[j]) continue;
					else if (y>=Y[j]) {
						if (y==m) x++;
						else y++;
					}
				}
				else if (y==m) {
					if (x<X[j]) continue;
					else x++;
				}
			}
			ans=1LL*(x-1)*m+y;
			wt(ans),puts("");
		}
	}
	
	else {
		R=n+m+q-1,len=n+m-1;
		For (i,1,m) modify(1,1,R,i,i);
		For (i,2,n) modify(1,1,R,m+i-1,1LL*i*m);
		For (i,1,q) {
			x=rd(),y=rd();
			find(1,1,R,y);
			wt(ans),puts(""),len++;
			modify(1,1,R,len,ans);
		}
	}
	
	return 0;
}

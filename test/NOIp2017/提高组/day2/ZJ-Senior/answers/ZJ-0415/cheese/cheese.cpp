#include<cstdio>
#include<cstring>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)

using namespace std;

typedef long long LL;

const int N=1035;

int cas,n,h,r,x[N],y[N],z[N],ans,a[N][N],vis[N];

LL dis(int i,int j) {
	return 1LL*(x[i]-x[j])*(x[i]-x[j])+1LL*(y[i]-y[j])*(y[i]-y[j])+1LL*(z[i]-z[j])*(z[i]-z[j]);
}

void dfs(int x) {
	if (vis[x] || ans) return;
	vis[x]=1; if (z[x]+r>=h) ans=1;
	for (int i=1;i<=n;i++) if (dis(x,i)<=4LL*r*r) dfs(i);
	return;
}

int main() {
	
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);

	for (scanf("%d",&cas);cas--;) {
		memset(vis,0,sizeof vis);
		scanf("%d%d%d",&n,&h,&r);
		For (i,1,n) scanf("%d%d%d",&x[i],&y[i],&z[i]);
		
		ans=0;
		For (i,1,n) if (!vis[i] && z[i]+r>=0 && z[i]-r<=0) {
			dfs(i);
			if (ans) break;
		}
		puts(ans?"Yes":"No");
	}
	
	return 0;	
}

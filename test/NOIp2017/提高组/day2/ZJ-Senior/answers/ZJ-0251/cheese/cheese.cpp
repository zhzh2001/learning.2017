#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define ll long long
using namespace std;
int n,next[1000000],num,head[1000000],vet[1000000];
ll h,r,x[1000000],y[1000000],z[1000000];
bool flag[1000000],flagxia[1000000],flagshang[1000000],lzss;
void dfs(int u){
    if (flagshang[u]==true){lzss=false;return;}
    if (lzss==false) return ;
	for (int e=head[u];e;e=next[e])
	  if (flag[vet[e]]==false){
	  	int v=vet[e];
	  	if (lzss==false) return ;
	  	flag[vet[e]]=true;
	  	if (flagshang[v]==true){lzss=false;return;}
	  	dfs(v);
	  	if (lzss==false) return ;
	  }
	if (lzss==false) return ;
}
void add_edge(int u,int v){
	next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
}
bool pd(int i,int j){
//	double ssum=sqrt((x[i]-x[j])*(x[i]-x[j]))+((y[i]-y[j])*(y[i]-y[j]))+((z[i]-z[j])*(z[i]-z[j])));
	long double x1=(long double)((x[i]-x[j])*(x[i]-x[j]));
	long double x2=(long double)((y[i]-y[j])*(y[i]-y[j]));
	long double x3=(long double)((z[i]-z[j])*(z[i]-z[j]));
	long double ssum=sqrt(x1+x2+x3);
	if (ssum<=(long double)(2*r)) return true;else return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);	
	while(T--){
		num=0;
		memset(flagshang,false,sizeof(flagshang));
		memset(flagxia,false,sizeof(flagxia));
		memset(flag,false,sizeof(flag));
		memset(head,0,sizeof(head));
		memset(next,0,sizeof(next));
		memset(vet,0,sizeof(vet));
	    scanf("%d%lld%lld",&n,&h,&r);	
		for (int i=1;i<=n;i++)
		  scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=1;i<=n;i++){
		  if (z[i]+r>=h)  flagshang[i]=true;
		  if (z[i]-r<=(ll)0)  flagxia[i]=true;
	}
	    //if (n==1) solve1();else
		
	    	for (int i=1;i<=n;i++)
	    	  for (int j=1;j<=n;j++)
	    	    if (pd(i,j)==true){	
	    	      add_edge(i,j);
	    	      add_edge(j,i);
	    }
	lzss=true;
	flag[0]=true;
	for (int i=1;i<=n;i++)
	if (flagxia[i]==true)
	{
		memset(flag,false,sizeof(flag));
		flag[i]=true;
	dfs(i);
}
	if (lzss==true) printf("No\n");else printf("Yes\n");
	}
	return 0;
}

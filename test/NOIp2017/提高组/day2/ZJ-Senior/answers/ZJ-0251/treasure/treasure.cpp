#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
using namespace std;
int n,m,next[100000],vet[100000],head[100000],val[100000],num,PASS,d[100000],root,ans,ans2,deeep[100000],fa[100000];
bool flag[100000];
void add_edge(int u,int v,int t){
	next[++num]=head[u];
	head[u]=num;
	vet[num]=v;
	val[num]=t;
}
void dfs_visi1t(int u,int supm){
	for (int e=head[u];e;e=next[e])
	  if(flag[vet[e]]==false){
	  	flag[vet[e]]=true;
	  	deeep[vet[e]]=deeep[u]+1;
	  	fa[vet[e]]=u;
	  	dfs_visi1t(vet[e],supm+1);
	  }
	return ;
}
void solve1(){
	memset(flag,false,sizeof(flag));
	for (int i=1;i<=n;i++)
	  if (d[i]==0) root=i;
	flag[root]=true;
	deeep[root]=1;
	dfs_visi1t(root,1);
	for (int i=1;i<=n;i++)
	{
		ans=0;
		for( int j=1;j<=n;j++)
		  ans+=abs(deeep[i]-deeep[j]);
		ans2=max(ans,ans2);
	}
	printf("%d",ans2*PASS);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		PASS=z;
		d[y]++;
		add_edge(x,y,z);
	}
	if (m==(n-1)) solve1();
	return 0;
}

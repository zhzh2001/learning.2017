#include<cstdio>
#include<cctype>
#include<cmath>
typedef long long LL;
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
int T;
int N,H,R;
double tR;
struct HOLE
{
	int x,y,z;
}Q[1009];
inline LL square(const LL x)
{
	return x*x;
}
int bcj[1009];
inline double Dist(const int i,const int j)
{
	return sqrt(square(Q[i].x-Q[j].x)+square(Q[i].y-Q[j].y)+square(Q[i].z-Q[j].z));
}
int find(const int x)
{
	if(bcj[x]==x)return x;
	bcj[x]=find(bcj[x]);
	return bcj[x];
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--)
	{
		read(N),read(H),read(R);
		tR=R*2;
		for(register int i=0;i<=N;i++)bcj[i]=i;
		bcj[N+1]=N+1;
		for(register int i=1;i<=N;i++)
		{
			read(Q[i].x),read(Q[i].y),read(Q[i].z);
			if(Q[i].z<=R)bcj[i]=0;
			if(H-R<=Q[i].z)
			{
				const int xxx=find(i),yyy=find(N+1);
				if(xxx!=yyy)bcj[yyy]=xxx;
			}
		}
		for(register int i=1;i<=N;i++)
			for(register int j=i+1;j<=N;j++)
				if(Dist(i,j)-0.000000001<=tR)
				{
					const int xxx=find(i),yyy=find(j);
					if(xxx!=yyy)bcj[yyy]=xxx;
				}
		if(find(N+1)==find(0))puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<cstring>
typedef long long LL;
template <typename T>inline T min(const T x,const T y){return x<y?x:y;}
template <typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
int n,m,ngo;
int bit[14];
int dis[13][13];
int dp[8192][13][13];
int ans[8192];
int main()
{
//	freopen("treasure.in","r",stdin);
//	freopen("treasure.out","w",stdout);
	memset(dis,0x7f,sizeof(dis));
	memset(dp,0x7f,sizeof(dp));
	bit[1]=1;
	for(register int i=2;i<=13;i++)bit[i]=bit[i-1]<<1;
	read(n),read(m);ngo=n+1;
	for(register int i=1;i<=m;i++)
	{
		int u,v,w;read(u),read(v),read(w);
		dis[u][v]=min(dis[u][v],w);
	}
	for(register int i=1;i<=n;i++)dp[bit[i]][i][1]=0;
	for(register int i=0;i<bit[ngo];i++)
	{
		for(register int j=1;j<=n;j++)
			for(register int k=1;k<=n;k++)
				if(dp[i][j][k]!=0x7f7f7f7f)
				{
					//mind(ans[i],dp[i][j][k]);
					for(register int into=1;into<=n;into++)
						if((!(i&bit[into]))&&dis[j][into]!=0x7f7f7f7f)
						{
							mind(dp[i|bit[into]][j][k],dp[i][j][k]+dis[j][into]*k);
							mind(dp[i|bit[into]][into][k+1],dp[i][j][k]+dis[j][into]*k);
						}
				}
	}
	
	return 0;
}

#include<cstdio>
#include<cctype>
#include<cstring>
typedef long long LL;
template <typename T>inline T min(const T x,const T y){return x<y?x:y;}
template <typename T>inline void mind(T&x,const T y){if(x>y)x=y;}
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
int n,m;
int dis[13][13];
int val=-1;
int Q[101],l,r;
int ans=0x7f7f7f7f;
int far[13];
inline int BFS(const int bg)
{
	int res=0;
	memset(far,0,sizeof(far));
	l=r=0;
	Q[++r]=bg;
	far[bg]=1;
	while(l<r)
	{
		const int u=Q[++l];
		for(register int v=1;v<=n;v++)
			if(dis[u][v]!=0x7f7f7f7f&&!far[v])
			{
				res+=dis[u][v]*far[u];
				far[v]=far[u]+1;
				Q[++r]=v;
			}
	}
	if(r!=n)return 0x7f7f7f7f;
	return res;
}
int mini[13];
int dep[13];
bool Hash[13];
inline int check(const int bg)
{
	Hash[bg]=1;
	int tow=0,cost=0x7f7f7f7f;
	for(register int i=1;i<=n;i++)
		if(!Hash[i]&&dis[bg][i]!=0x7f7f7f7f&&mini[i]>=dep[bg]*dis[bg][i])
			dep[i]=dep[bg]+1,mini[i]=dep[bg]*dis[bg][i];
	for(register int i=1;i<=n;i++)
		if(!Hash[i]&&mini[i]<cost)
		{
			tow=i;
			cost=mini[i];
		}
	if(tow!=0)return check(tow)+cost;
	return 0;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(dis,0x7f,sizeof(dis));
	read(n),read(m);
	for(register int i=1;i<=m;i++)
	{
		int u,v,w;read(u),read(v),read(w);
		if(val==-1)val=w;
		else if(val==-2);
		else if(val!=w)val=-2;
		mind(dis[u][v],w);
	}
	if(val!=-2)
	{
		for(register int bg=1;bg<=n;bg++)
			mind(ans,BFS(bg));
		print(ans);
	}
	else
	{
		for(register int bg=1;bg<=n;bg++)
		{
			memset(mini,0x7f,sizeof(mini));
			dep[bg]=1;
			const int out=check(bg);
			int sign=1;
			for(register int i=1;i<=n;i++)
			{
				if(!Hash[i])
					sign=0;
				else Hash[i]=0;
			}
			if(sign)
				mind(ans,out);
		}
		print(ans);
	}
	return 0;
}

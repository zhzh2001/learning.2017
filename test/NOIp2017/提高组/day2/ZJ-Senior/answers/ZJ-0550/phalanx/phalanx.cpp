#include<cstdio>
#include<cctype>
#include<cmath>
#include<set>
#include<vector>
#include<algorithm>
typedef long long LL;
template <typename T>
inline void read(T&x)
{
	char cu=getchar();x=0;bool fla=0;
	while(!isdigit(cu)){if(cu=='-')fla=1;cu=getchar();}
	while(isdigit(cu))x=x*10+cu-'0',cu=getchar();
	if(fla)x=-x;
}
template <typename T>
void printe(const T x)
{
	if(x>=10)printe(x/10);
	putchar(x%10+'0');
}
template <typename T>
inline void print(const T x)
{
	if(x<0)putchar('-'),printe(-x);
	else printe(x);
}
int n,m,q;
//1~6
int num[1001][1001];
//7~10
std::set<int>Lost[50001];
std::vector<int>bak[50001];
int las[50001];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n),read(m),read(q);
	if(n<=1000&&m<=1000)
	{
		for(register int i=1;i<=n;i++)
			for(register int j=1;j<=m;j++)
				num[i][j]=(i-1)*m+j;
		while(q--)
		{
			int x,y,ans;read(x),read(y);
			ans=num[x][y];
			for(register int i=y;i<m;i++)num[x][i]=num[x][i+1];
			for(register int i=x;i<n;i++)num[i][m]=num[i+1][m];
			num[n][m]=ans;
			print(ans),putchar('\n');
		}
	}
	else if(n<=50000&&m<=50000)
	{
		for(register int i=1;i<=n;i++)
			las[i]=i*m;
		while(q--)
		{
			int x,y,ans;read(x),read(y);
			if(y==m)
			{
				ans=las[x];
				for(register int i=x;i<n;i++)las[i]=las[i+1];
				las[n]=ans;
			}
			else if(Lost[x].size()==0||(*(Lost[x].begin()))>y)
			{
				ans=(x-1)*m+y;
				Lost[x].insert(ans-(x-1)*m);
				bak[x].push_back(las[x]);
				for(register int i=x;i<n;i++)las[i]=las[i+1];
				las[n]=ans;
			}
			else if(y>=m-(int)bak[x].size())
			{
				y-=m-bak[x].size();
				ans=bak[x][y];
				for(register int i=y+1;i<(int)bak[x].size();i++)
					bak[x][i-1]=bak[x][i];
				bak[x][bak[x].size()-1]=las[x];
				for(register int i=x;i<n;i++)las[i]=las[i+1];
				las[n]=ans;
			}
			else
			{
				int tot=1;
				std::set<int>::iterator Pos;
				for(Pos=Lost[x].begin();Pos!=Lost[x].end()&&(*Pos)-tot<y;Pos++,tot++);
				Pos--,tot--;
				ans=(x-1)*m+y+tot;
				Lost[x].insert(ans-(x-1)*m);
				bak[x].push_back(las[x]);
				for(register int i=x;i<n;i++)las[i]=las[i+1];
				las[n]=ans;
			}
			print(ans),putchar('\n');
		}
	}
	return 0;
}

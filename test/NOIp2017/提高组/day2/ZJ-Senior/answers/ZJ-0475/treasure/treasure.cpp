#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define INF 0x3f3f3f3f
using namespace std;

int mm[20][20],n,m,ans,mn,d[20],dp[16777220],eig[]={1,8,64,512,4096,32768,262144,2097152,16777216};

inline void wj()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
}

inline int qw(int u,int v)
{
	u/=eig[v-1];
	return u%8;
}

inline int dfs(int zt)
{
	if(dp[zt]!=-1) return dp[zt];
	register int i,j,k,res=INF;
	bool have=0;
	for(i=1;i<=n;++i)
	{
		if(qw(zt,i)!=7) continue;
		have=1;
		for(j=1;j<=n;++j)
		{
			k=qw(zt,j);
			if(i==j||k+1>=7||mm[i][j]==INF) continue;
			res=min(res,mm[i][j]*(k+1)+dfs(zt-(6-k)*eig[i-1]));
		}
	}
	if(!have) return 0;
	dp[zt]=res;
	return res;
}

inline void read(int &u)
{
	u=0;
	register char ch=getchar();
	for(;ch<'0';ch=getchar());
	for(;ch>='0'&&ch<='9';ch=getchar())
	{
		u=(u*10+ch-'0');
	}
}

int main()
{
	wj();
	register int i,p,q,o;
	ans=INF;
	memset(mm,INF,sizeof(mm));
	memset(dp,-1,sizeof(dp));
	read(n),read(m);
	for(i=1;i<=m;++i)
	{
		read(p),read(q),read(o);
		mm[p][q]=mm[q][p]=min(mm[p][q],o);
	}
	for(i=1;i<=n;++i)
	{
		ans=min(ans,dfs(((1 << (n*3))-1)-7*eig[i-1]));
	}
	printf("%d",ans);
}

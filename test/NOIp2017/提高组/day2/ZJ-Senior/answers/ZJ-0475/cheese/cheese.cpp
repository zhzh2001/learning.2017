#include<iostream>
#include<cstdio>
#include<cstring>
#define ll long long
#define N 1010
using namespace std;

struct Hole
{
	ll x,y,z;
}hole[N];
struct Bn
{
	ll next,to;
}bn[2000010];
ll T,n,h,r,first[N],bb;
bool dd[N],ans,vis[N];

void add(ll u,ll v)
{
	bb++;
	bn[bb].next=first[u];
	bn[bb].to=v;
	first[u]=bb;
}

void dfs(ll now)
{
	if(vis[now]||ans) return;
	if(dd[now])
	{
		ans=1;
		return;
	}
	vis[now]=1;
	ll p;
	for(p=first[now];p!=-1;p=bn[p].next)
	{
		dfs(bn[p].to);
	}
}

void wj()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
}

int main()
{
	wj();
	register ll i,j;
	cin>>T;
	while(T--)
	{
		ans=bb=0;
		memset(first,-1,sizeof(first));
		memset(dd,0,sizeof(dd));
		memset(vis,0,sizeof(vis));
		scanf("%lld%lld%lld",&n,&h,&r);
		for(i=1;i<=n;++i)
		{
			scanf("%lld%lld%lld",&hole[i].x,&hole[i].y,&hole[i].z);
			if(hole[i].z+r>=h) dd[i]=1;
		}
		for(i=1;i<=n;i++)
		{
			for(j=i+1;j<=n;j++)
			{
				if((hole[i].x-hole[j].x)*(hole[i].x-hole[j].x)+(hole[i].y-hole[j].y)*(hole[i].y-hole[j].y)+(hole[i].z-hole[j].z)*(hole[i].z-hole[j].z)<=4*r*r)
				{
					add(i,j);
					add(j,i);
				}
			}
		}
		for(i=1;i<=n&&!ans;i++)
		{
			if(hole[i].z-r<=0&&!vis[i])
			{
				dfs(i);
			}
		}
		ans?printf("Yes\n"):printf("No\n");
	}
}

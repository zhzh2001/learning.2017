#include<iostream>
#include<cstdio>
#define N 1010
#define M 300100
using namespace std;

int n,m,a[N][N],Q,fir[M],rig[M],qx[510],qy[510];

inline void wj()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
}

int main()
{
	wj();
	register int i,j,k,now=0,p,q;
	cin>>m>>n>>Q;
	if(n<=1000&&m<=1000)
	{
		for(i=1; i<=m; i++)
		{
			for(j=1; j<=n; j++)
			{
				a[i][j]=++now;
			}
		}
		for(i=1; i<=Q; i++)
		{
			scanf("%d%d",&p,&q);
			k=a[p][q];
			printf("%d\n",k);
			for(j=q; j<n; j++)
			{
				a[p][j]=a[p][j+1];
			}
			for(j=p; j<m; j++)
			{
				a[j][n]=a[j+1][n];
			}
			a[m][n]=k;
		}
		return 0;
	}
	if(Q<=500)
	{
		for(i=1; i<=Q; i++)
		{
			scanf("%d%d",&p,&q);
			qx[i]=p,qy[i]=q;
			for(j=i-1; j>=1; j--)
			{
				if(p==m&&q==n)
				{
					p=qx[j];
					q=qy[j];
					continue;
				}
				if(q==n)
				{
					if(p>=qx[j]) p++;
				}
				else if(p==qx[j]&&qy[j]<=q)
				{
					q++;
				}
			}
			printf("%d\n",(p-1)*n+q);
		}
		return 0;
	}
}

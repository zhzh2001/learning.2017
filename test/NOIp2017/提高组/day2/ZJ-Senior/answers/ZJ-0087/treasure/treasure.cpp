#include<cstdio>
#include<cstring>
using namespace std;

int n,m,ans,ret,minn;
int cost[15],dep[15],map[15][15],fa[15];

int readint()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*f;
}

int min(int x,int y){return x<y?x:y;}

void work(int x)
{
	dep[x]=1;ret=0;
	for (int i=1;i<=n;i++) cost[i]=map[x][i],fa[i]=x;
	for (int i=1;i<n;i++)
	{
		int k;
		minn=1e9;
		for (int j=1;j<=n;j++)
		{
			if (cost[j]<minn&&cost[j]!=0) minn=cost[j],k=j;
		}
		ret+=minn;
		cost[k]=0;
		dep[k]=dep[fa[k]]+1;
		for (int j=1;j<=n;j++)
			if (cost[j]>(map[k][j]*dep[k])&&map[k][j]!=0) cost[j]=map[k][j]*dep[k],fa[j]=k;
	}
	ans=min(ans,ret);
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=readint();m=readint();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			map[i][j]=500005;
	for (int i=1;i<=m;i++)
	{
		int x=readint(),y=readint(),z=readint();
		map[x][y]=min(map[x][y],z);
		map[y][x]=map[x][y];
	}
	for (int i=1;i<=n;i++) map[i][i]=0;
	ans=1e9;
	for (int i=1;i<=n;i++) work(i);
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

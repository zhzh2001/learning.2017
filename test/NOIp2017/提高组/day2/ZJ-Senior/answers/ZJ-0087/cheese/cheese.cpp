#include<cstdio>
#include<cstring>
using namespace std;

int test,n,tot;
int lnk[1005],q[1005];
long long h,r;
long long x[1005],y[1005],z[1005];
bool vis[1005];
struct edge
{
	int nxt,y;
} e[1002005];

long long readll()
{
	long long x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*f;
}

void adde(int x,int y)
{
	tot++;e[tot].nxt=lnk[x];lnk[x]=tot;e[tot].y=y;
}

bool bfs()
{
	int head=0,tail=0;
	q[++tail]=0;
	while (head<=tail)
	{
		int x=q[++head];
		for (int i=lnk[x];i!=-1;i=e[i].nxt)
		{
			int y=e[i].y;
			if (y==n+1) return true;
			if (!vis[y]) {vis[y]=true;q[++tail]=y;}
		}
	}
	return false;
}

bool check(long long x,long long y,long long z)
{
	if ((x*x+y*y+z*z)<=4*r*r) return true; else return false;
}

void work()
{
	scanf("%d",&n);h=readll();r=readll();
	for (int i=1;i<=n;i++)
	{
		x[i]=readll();y[i]=readll();z[i]=readll();
		if (z[i]<=r) adde(0,i),adde(i,0);
		if (z[i]+r>=h) adde(i,n+1),adde(n+1,i);
	}
	for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
			if (check(x[i]-x[j],y[i]-y[j],z[i]-z[j])) {adde(i,j);adde(j,i);}
	if (bfs()) printf("Yes\n");
	else printf("No\n");
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&test);
	while (test--)
	{
		memset(lnk,-1,sizeof(lnk));
		memset(vis,false,sizeof(vis));
		memset(q,0,sizeof(q));
		tot=0;
		work();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

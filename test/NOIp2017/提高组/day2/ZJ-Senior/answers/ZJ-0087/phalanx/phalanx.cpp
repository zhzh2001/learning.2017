#include<cstdio>
using namespace std;

int n,m,q,t;
int map[1005][1005];

int readint()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while ('0'<=ch&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x*f;
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=readint();m=readint();q=readint();
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			map[i][j]=i*m+j-m;
	while (q--)
	{
		int x=readint(),y=readint();
		printf("%d\n",map[x][y]);
		t=map[x][y];
		for (int i=y;i<m;i++) map[x][i]=map[x][i+1];
		for (int i=x;i<n;i++) map[i][m]=map[i+1][m];
		map[n][m]=t;
	}
	fclose(stdin);fclose(stdout);
	return 0;
}


#include <cstdio>
#include <cstring>
#include <algorithm>
#include <set>

#define int long long

using namespace std;

static const int N = 300010;
static const int Q = 300010;
int n, m, q, a[1010][1010];
pair <int, int> ask[Q];

struct SegmentTree {
private:
	static const int N = 1200010;
	int tot;
	struct Node {
		int l, r, val, sum, child[2];
	} node[N << 1];

	void build_tree(int root) {
		if (node[root].l == node[root].r) return;
		int mid = node[root].l + node[root].r >> 1;
		node[node[root].child[0] = ++ tot] = (Node) { node[root].l, mid, 0, 0, 0, 0 }, build_tree(tot);
		node[node[root].child[1] = ++ tot] = (Node) { mid + 1, node[root].r, 0, 0, 0, 0 }, build_tree(tot);
	}

	void update(int root) {
		if (node[root].l == node[root].r) return;
		node[root].sum = node[node[root].child[0]].sum + node[node[root].child[1]].sum;
	}

	void add_point(int root, int p, int val) {
		if (node[root].l > p || node[root].r < p) return;
		if (node[root].l == node[root].r) {
			node[root].val += val, node[root].sum = node[root].val > 0; return;
		}
		add_point(node[root].child[0], p, val), add_point(node[root].child[1], p, val);
		update(root);
	}

	pair <int, int> get_kth(int root, int p) {
		if (node[root].l == node[root].r) return make_pair(node[root].val, node[root].l);
		if (node[node[root].child[0]].sum >= p) return get_kth(node[root].child[0], p);
		else return get_kth(node[root].child[1], p - node[node[root].child[0]].sum);
	}

public:
	void init(int n) {
		node[node[tot = 1].l = 1].r = n, build_tree(1);
	}

	void add(int p, int val) {
		add_point(1, p, val);
	}

	pair <int, int> get(int p) {
		return get_kth(1, p);
	}
} tree;

signed main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%lld%lld%lld", &n, &m, &q);
	bool flag = true;
	for (int i = 1; i <= q; ++ i) scanf("%lld%lld", &ask[i].first, &ask[i].second), flag &= ask[i].first == 1;
	if (n <= 1000 && m <= 1000) {
		for (int i = 1; i <= n; ++ i)
			for (int j = 1; j <= m; ++ j) a[i][j] = (i - 1) * m + j;
		for (int i = 1; i <= q; ++ i) {
			int x = ask[i].first, y = ask[i].second, ans = a[x][y];
			printf("%lld\n", ans);
			for (int j = y; j < m; ++ j) a[x][j] = a[x][j + 1];
			for (int j = x; j < n; ++ j) a[j][m] = a[j + 1][m];
			a[n][m] = ans;
		}
	} else if (flag) {
		int cur = 0;
		tree.init(n + m - 1 + q);
		for (int i = 1; i < m; ++ i) tree.add(++ cur, i);
		for (int i = 1; i <= n; ++ i) tree.add(++ cur, m * i);
		for (int i = 1; i <= q; ++ i) {
			pair <int, int> ans = tree.get(ask[i].second);
			printf("%lld\n", ans.first);
			tree.add(ans.second, -ans.first), tree.add(++ cur, ans.first);
		}
	} else {
	}
	return 0;
}


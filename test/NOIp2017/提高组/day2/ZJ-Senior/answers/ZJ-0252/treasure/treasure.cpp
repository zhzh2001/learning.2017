#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

static const int N = 13;
int n, m, ans = 1000000000, mp[N][N], dep[N];
int top, st[N];
bool in[N];

void dfs(int t, int val) {
	if (val > ans) return;
	if (t >= n) {
		ans = min(ans, val); return;
	}
	for (int i = 1; i <= top; ++ i)
		for (int j = 1; j <= n; ++ j)
			if (! in[j] && mp[j][st[i]] < 1000000000) {
				dep[j] = dep[st[i]] + 1, st[++ top] = j, in[j] = true;
				dfs(t + 1, val + dep[j] * mp[j][st[i]]), -- top, in[j] = false;
			}
}

void act(int t) {
	memset(in, 0, sizeof in), in[t] = true, dep[t] = 0, st[top = 1] = t, dfs(1, 0);
}

void process(int t) {
	memset(in, 0, sizeof in), in[t] = true, dep[t] = 0; int sum = 0;
	for (int i = 2; i <= n; ++ i) {
		int p = 0, q = 0, mn = 1000000000;
		for (int j = 1; j <= n; ++ j)
			for (int k = 1; k <= n; ++ k)
				if (in[j] && ! in[k] && mp[j][k] < 1000000000)
					if ((dep[j] + 1) * mp[j][k] < mn) p = j, q = k, mn = (dep[j] + 1) * mp[j][k];
		dep[q] = dep[p] + 1, in[q] = true, sum += dep[q] * mp[p][q];
	}
	ans = min(ans, sum);
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	memset(mp, 0x3f, sizeof mp);
	for (int i = 1; i <= m; ++ i) {
		int u, v, w; scanf("%d%d%d", &u, &v, &w);
		mp[u][v] = mp[v][u] = min(mp[u][v], w);
	}
	if (n <= 8) {
		for (int i = 1; i <= n; ++ i) act(i);
		printf("%d\n", ans);
	} else {
		for (int i = 1; i <= n; ++ i) process(i);
		printf("%d\n", ans);
	}
	return 0;
}


#include <cmath>
#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

static const int N = 1010;
int n, h, r, que[N];
struct Ball {
	int x, y, z;
	Ball(int _x = 0, int _y = 0, int _z = 0) : x(_x), y(_y), z(_z) {}
	double operator * (const Ball &a) const {
		return sqrt(1ll * (x - a.x) * (x - a.x) + 1ll * (y - a.y) * (y - a.y) + 1ll * (z - a.z) * (z - a.z));
	}
} ball[N];
bool vis[N], mp[N][N];

static const double EPS = 1e-6;
int cmp(double t) {
	return fabs(t) < EPS ? 0 : t > 0 ? 1 : -1;
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T --) {
		scanf("%d%d%d", &n, &h, &r);
		for (int i = 1; i <= n; ++ i) scanf("%d%d%d", &ball[i].x, &ball[i].y, &ball[i].z);
		memset(mp, 0, sizeof mp), memset(vis, 0, sizeof vis);
		for (int i = 1; i <= n; ++ i) {
			if (ball[i].z <= r) mp[0][i] = mp[i][0] = true;
			if (ball[i].z >= h - r) mp[i][n + 1] = mp[n + 1][i] = true;
		}
		for (int i = 1; i < n; ++ i)
			for (int j = i + 1; j <= n; ++ j) {
				double dist = ball[i] * ball[j];
				if (cmp(dist - r * 2) < 1) mp[i][j] = mp[j][i] = true;
			}
		int s = 0, t = 1; que[1] = 0, vis[0] = true;
		while (s < t) {
			int p = que[++ s];
			for (int i = 1; i <= n + 1; ++ i) if (mp[p][i] && ! vis[i]) vis[que[++ t] = i] = true;
		}
		if (! vis[n + 1]) printf("No\n");
		else printf("Yes\n");
	}
	return 0;
}


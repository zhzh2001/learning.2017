#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=14,Inf=1061109567;
int f[N][(1<<N)+5],dist[N][N],dis[N];
int work(int p,int n) 
{
	memset(f,63,sizeof(f));
	f[0][1<<p]=0;
	for (int step=0;step<n-1;++step) 
	{
		int *fs=f[step],*fn=f[step+1];
		for (int staus=0;staus<(1<<n);++staus) if (fs[staus]!=Inf)
		{
			memset(dis,63,sizeof(dis));
			for (int i=0;i<n;++i) if (((staus>>i)&1)==1) for (int j=0;j<n;++j) dis[j]=min(dis[j],dist[i][j]);
			for (int nxt=(staus+1)&(~staus);nxt<(1<<n);nxt=((staus|nxt)+1)&(~staus)) 
			{
				bool flag=true;
				int sum=0;
				for (int i=0;flag && i<n;++i) if (((nxt>>i)&1)==1)
				{
					if (dis[i]==Inf) flag=false; 
					else sum+=dis[i];
				}
				if (flag==true) fn[staus|nxt]=min(fn[staus|nxt],fs[staus]+sum*(step+1));
			}
		}
	}
	int res=Inf;
	for (int i=0;i<n;++i) res=min(res,f[i][(1<<n)-1]);
	return res;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n=0,m=0;
	memset(dist,63,sizeof(dist));
	scanf("%d %d",&n,&m);
	if (n==1) return puts("0"),0;
	for (int i=0;i<m;++i) 
	{
		int x=0,y=0,z=0;
		scanf("%d %d %d",&x,&y,&z),--x,--y;
		dist[x][y]=min(dist[x][y],z),dist[y][x]=min(dist[y][x],z);
	}
	int ans=Inf;
	for (int i=0;i<n;++i) ans=min(ans,work(i,n));
	printf("%d\n",ans);
}

#include <cstdio>
#include <algorithm>
using namespace std;
#define sqr(x) (1ll*(x)*(x))
typedef long long ll;
const int N=1e3+5;
int x[N],y[N],z[N],fa[N],sz[N];
int find(int x) {return fa[x]==x?x:fa[x]=find(fa[x]);}
void link(int x,int y) 
{
	x=find(x),y=find(y);
	if (x!=y) 
	{
		if (sz[x]>sz[y]) swap(x,y);
		fa[x]=y,sz[y]+=sz[x];
	}
}
bool work()
{
	int n=0,h=0,r=0;
	ll l=0;
	scanf("%d %d %d",&n,&h,&r),l=4ll*r*r;
	for (int i=0;i<n;++i) scanf("%d %d %d",&x[i],&y[i],&z[i]),fa[i]=i,sz[i]=1;
	fa[n]=n,sz[n]=1,fa[n+1]=n+1,sz[n+1]=1;
	for (int i=0;i<n;++i) 
	{
		if (z[i]-r<=0) link(i,n);
		if (z[i]+r>=h) link(i,n+1);
	}
	for (int i=0;i<n;++i) for (int j=0;j<i;++j) 
	{
		if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=l) link(i,j);
	}
	return find(n)==find(n+1);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int test=0;
	scanf("%d",&test);
	for (;test--;) 
	{
		puts(work()?"Yes":"No");
	}
}

#include <cstdio>
#include <cstdlib>
const int T=20,N=1000,P=1e9+1;
int Rand() {return (rand()%2==1?1:-1)*(rand()<<15)^rand();}
int main()
{
	freopen("cheese.in","w",stdout);
	printf("%d\n",T);
	for (int t=0;t<T;++t) 
	{
		printf("%d %d %d\n",N,Rand()%P,Rand()%P);
		for (int i=0;i<N;++i) printf("%d %d %d\n",Rand()%P,Rand()%P,Rand()%P);
	}
}

#include <cstdio>
#include <cctype>
int read()
{
	int x=0;char c=0;
	while (c=getchar(),!isdigit(c));x=c-'0';
	while (c=getchar(),isdigit(c)) x=x*10+c-'0';
	return x;
}
const int N=1e3+5;
int id[N][N];
namespace seg
{
	const int N=6e5,Len=N+5;
	int sz[Len*4],id[Len*4];
	void pushUp(int p)
	{
		sz[p]=sz[p*2]+sz[p*2+1];
	}
	int find(int p,int l,int r,int k) 
	{
		if (l+1==r) 
		{
			sz[p]=0;
			return id[p];
		}
		int mid=(l+r)/2,res=0;
		res=(sz[p*2]>=k?find(p*2,l,mid,k):find(p*2+1,mid,r,k-sz[p*2]));
		pushUp(p);
		return res;
	}
	void set1(int p,int l,int r,int x,int t)  
	{
		if (l+1==r) 
		{
			sz[p]=1,id[p]=t;
			return;
		}
		int mid=(l+r)/2;
		if (x<mid) set1(p*2,l,mid,x,t); else set1(p*2+1,mid,r,x,t);
		pushUp(p);
	}
	void build(int p,int l,int r,int len) 
	{
		if (l+1==r) 
		{
			if (l<len) sz[p]=1,id[p]=l+1;
			return;
		}
		int mid=(l+r)/2;
		build(p*2,l,mid,len),build(p*2+1,mid,r,len);
		pushUp(p);
	}
	void work(int m,int q) 
	{
		int cnt=m;
		build(1,0,N,m);
		for (int i=0;i<q;++i) 
		{
			int x=0,y=0,t=0;
			x=read(),y=read();
			t=find(1,0,N,y);
			printf("%d\n",t);
			set1(1,0,N,cnt++,t);
		}
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n=0,m=0,q=0;
	scanf("%d %d %d",&n,&m,&q);
	if (n==1) 
	{
		seg::work(m,q);
		return 0;
	}
	for (int i=0,cnt=1;i<n;++i) for (int j=0;j<m;++j) id[i][j]=cnt++;
	for (int i=0;i<q;++i) 
	{
		int x=0,y=0,t=0;
		scanf("%d %d",&x,&y),--x,--y;
		t=id[x][y];
		printf("%d\n",t);
		for (int k=y;k<m-1;++k) id[x][k]=id[x][k+1];
		for (int k=x;k<n-1;++k) id[k][m-1]=id[k+1][m-1];
		id[n-1][m-1]=t;
	}
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<queue>
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,dis[15][15],dp[5005][14][14],ans=2000000000;
int d[15],last;
int dfs(int u,int dep,int f)
{
	int ans=0;
	for (int i=0;i<n;i++)if (dis[u][i]!=-1&&f!=i)
		ans+=dis[u][i]*dep+dfs(i,dep+1,u);
	return ans;
}
void solve1()
{
	for (int i=0;i<n;i++)
		ans=min(ans,dfs(i,1,-1));
	printf("%d",ans);
}
int bfs(int u)
{
	int ans=0;
	for (int i=0;i<n;i++) d[i]=0;
	queue<int>q;q.push(u);d[u]=1;
	while (!q.empty())
	{
		int now=q.front();q.pop();
		for (int i=0;i<n;i++) if (dis[now][i]!=-1&&i!=now&&d[i]==0)
		{
			d[i]=d[now]+1;
			ans+=dis[now][i]*d[now];
			q.push(i);
		}
	}
	return ans;
}
void solve2()
{
	for (int i=0;i<n;i++) ans=min(ans,bfs(i));
	printf("%d",ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for (int i=0;i<n;i++) for (int j=0;j<n;j++) dis[i][j]=-1;
	for (int i=1;i<=m;i++)
	{
		int u=read()-1,v=read()-1,l=read();
		if (i==1) last=l;else if (l!=last) last=-1;
		if (dis[u][v]==-1) dis[u][v]=l;
		else dis[u][v]=min(dis[u][v],l);
		dis[v][u]=dis[u][v];
	}
	if (m==n-1){solve1();return 0;}
	if (last!=-1){solve2();return 0;}
	memset(dp,127/3,sizeof(dp));
	for (int i=0;i<n;i++) dp[1<<i][i][1]=0;
	int N=1<<n;
	for (int i=1;i<N-1;i++)
	{
		for (int j=0;j<n;j++)if (i&(1<<j))
		{
			for (int k=1;k<=n;k++)
				for (int x=0;x<n;x++)if ((dis[j][x]!=-1)&&(!(i&(1<<x))))
				{
					dp[i|(1<<x)][x][k+1]=min(dp[i|(1<<x)][x][k+1],dp[i][j][k]+dis[j][x]*k);
					dp[i|(1<<x)][j][k]=min(dp[i|(1<<x)][j][k],dp[i][j][k]+dis[j][x]*k);
				}	
		}
	}
	for (int i=0;i<n;i++)
		for (int j=1;j<=n;j++)
			ans=min(ans,dp[N-1][i][j]);
	printf("%d",ans);
	return 0;
}

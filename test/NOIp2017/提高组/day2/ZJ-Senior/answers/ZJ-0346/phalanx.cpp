#include<cstdio>
#include<cstring>
#include<iostream>
#include<map>
typedef long long ll;
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,q,a[1005][1005];
int t[300005],b[300005],tot=0;
map<long long,int>c;
void solve1()
{
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++) a[i][j]=(i-1)*m+j;
	while (q--)
	{
		int x=read(),y=read();printf("%d\n",a[x][y]);
		int t=a[x][y];
		for (int i=y;i<m;i++) a[x][i]=a[x][i+1];
		for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
		a[n][m]=t;
	}
}
void update(int x){for (x;x<=m;x+=x&-x) t[x]++;}
int query(int x)
{
	int ans=0;
	for (;x;x-=x&-x) ans+=t[x];
	return ans;
}
void solve2()
{
	for (int i=1;i<=m;i++) b[i]=i;
	while (q--)
	{
		int x=read(),y=read();
		int ans=b[query(y)+y];
		printf("%d\n",ans);
		update(y);b[++tot]=ans;
	}
}
void solve3()
{
	for (int i=1;i<=n*m;i++) c[i]=i;
	while (q--)
	{
		int x=read(),y=read();
		ll p=(ll)(x-1)*m+y;
		printf("%d\n",c[p]);
		int t=c[p];
		for (int i=y;i<m;i++)
		{
			ll p1=(ll)(x-1)*m+i;
			ll p2=p1+1;
			c[p1]=c[p2];
		}
		for (int i=x;i<n;i++)
		{
			ll p1=(ll)i*m;
			ll p2=p1+m;
			c[p1]=c[p2];
		}
		c[(ll)n*m]=t;
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	if (n<=1000&&m<=1000) solve1();
	else if (n==1) solve2();
	else solve3();
	return 0;
}


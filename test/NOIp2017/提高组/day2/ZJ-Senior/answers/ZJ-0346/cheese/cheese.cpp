#include<cstdio>
#include<cstring>
#include<iostream>
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,f[1005],Case;
long long x[1005],y[1005],z[1005],h,r;
bool judge(int i,int j)
{
	long long dis=(x[i]-x[j])*(x[i]-x[j]);
	long long R=r*r*4LL;
	R=R-dis;if (R<0) return 0;
	dis=(y[i]-y[j])*(y[i]-y[j]);
	R=R-dis;if (R<0) return 0;
	dis=(z[i]-z[j])*(z[i]-z[j]);
	R=R-dis;if (R<0) return 0;
	return 1;
}
int find(int i)
{
	if (f[i]!=i) f[i]=find(f[i]);return f[i];
}
void Union(int i,int j)
{
	int a=find(i),b=find(j);
	if (a!=b) f[a]=b;
}
void solve()
{
	scanf("%d%lld%lld",&n,&h,&r);f[0]=0;f[n+1]=n+1;
	for (int i=1;i<=n;i++)
	{
		x[i]=1LL*read();y[i]=1LL*read();z[i]=1LL*read();f[i]=i;
		if (z[i]-r<=0) Union(i,0);
		if (z[i]+r>=h) Union(i,n+1);
	}
	for (int i=1;i<n;i++)
		for (int j=i+1;j<=n;j++)
			if (judge(i,j)) Union(i,j);
	if (find(0)==find(n+1)) puts("Yes");else puts("No");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&Case);
	while (Case--) solve();
	return 0;
}

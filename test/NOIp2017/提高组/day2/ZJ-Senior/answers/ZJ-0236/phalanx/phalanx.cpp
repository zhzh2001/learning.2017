#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
const int N = 1000005 ;
using namespace std ;

int n, m, T = 0, W[N] = {0}, B[N] = {0}, cnt ;

struct poi {
	int l, r, fa, sz, SZ, id ;
} tr[N] ;

void psd(int x) {
}

void upd(int x) {
	if (!x) return ;
	int l = tr[x].l, r = tr[x].r ;
	tr[x].sz = tr[l].sz + tr[r].sz + tr[x].SZ ;
}

void rotate(int &rt, int x) {
	int y = tr[x].fa, z = tr[y].fa ;
	if (tr[z].l == y) tr[z].l = x; else tr[z].r = x ; tr[x].fa = z ;
	if (tr[y].l == x) {
		tr[y].l = tr[x].r, tr[tr[x].r].fa = y ;
		tr[x].r = y, tr[y].fa = x ;
	} else {
		tr[y].r = tr[x].l, tr[tr[x].l].fa = y ;
		tr[x].l = y, tr[y].fa = x ;
	}
	upd(y), upd(x) ;
	if (rt == y) rt = x ;
}

void splay(int &rt, int x, int t) {
	if (x == t) return ;
	for ( ; tr[x].fa != t && tr[tr[x].fa].fa != t ; ) {
		int y = tr[x].fa, z = tr[y].fa ;
		if (!((tr[y].l == x) ^ (tr[z].l == y))) rotate(rt, y), rotate(rt, x) ; 
			else rotate(rt, x), rotate(rt, x) ;
	} 
	if (tr[x].fa != t) rotate(rt, x) ;
}

void insert_tail(int &rt, int x) {
	if (!rt) {
		rt = x ;
		return ;
	}
	int p = rt, las = p ;
	for ( ; p; ) {
		las = p ;
		++ tr[p].sz ;
		p = tr[p].r ;
	}
	tr[las].r = x, tr[x].fa = las ;
	splay(rt, x, 0) ;
}

void insert_head(int &rt, int x) {
	if (!rt) {
		rt = x ;
		return ;
	}
	int p = rt, las = p ;
	for ( ; p ; ) {
		las = p ;
		++ tr[p].sz ;
		p = tr[p].l ;
	}
	tr[las].l = x, tr[x].fa = las ;
	splay(rt, x, 0) ;
}

void insert(int &rt, int x) {
	if (!rt) {
		rt = x ;
		return ;
	}
	int p = rt, las = p ;
	for ( ; p ; ) {
		las = p ;
		++ tr[p].sz ;
		if (tr[x].id < tr[p].id) p = tr[p].l ;
			else p = tr[p].r ;
	}
	if (tr[x].id < tr[las].id) tr[las].l = x ; else tr[las].r = x ; tr[x].fa = las ;
	splay(rt, x, 0) ;
}

int findrk(int p, int rk) {
	int l = tr[p].l, r = tr[p].r ;
	if (rk <= tr[l].sz) return findrk(l, rk) ;
	if (rk == tr[l].sz + 1) return p ;
	return findrk(r, rk - tr[l].sz - 1) ;
}

int softsearch(int p, int rk, int bound) {
	if (!p) return rk ;
	int l = tr[p].l, r = tr[p].r ;
	int pos = tr[p].id - bound - tr[l].sz - 1 ;
	if (rk <= pos) return softsearch(l, rk, bound) ;
	return tr[p].id - bound + softsearch(r, rk - pos, tr[p].id) ;
}

void pre() {
	T = 0, cnt = 0 ;	
	rep(i, 1, n) {
		++ cnt ;
		tr[cnt].l = tr[cnt].r = tr[cnt].fa = 0, tr[cnt].sz = tr[cnt].SZ = 1 ;
		tr[cnt].id = i * m ;
		insert_tail(T, cnt) ;
	}
	rep(i, 1, n) W[i] = B[i] = 0 ;
}

void solve(int x, int y) {
	if (y == m) {
		int u = findrk(T, x), rk = x, sz = tr[T].sz ;
		printf("%d\n", tr[u].id) ;
		if (rk - 1) {
			int l = findrk(T, rk - 1) ;
			splay(T, l, 0) ;
			if (rk != sz) {
				int r = findrk(T, rk + 1) ;
				splay(T, r, l) ;
				u = tr[r].l ;
			} else {
				u = tr[l].r ;
			}
		} else {
			if (rk != sz) {
				int r = findrk(T, rk + 1) ;
				splay(T, r, 0) ;
				u = tr[r].l ;
			} else u = T ;
		}		
		int fa = tr[u].fa ;
		if (fa) {
			if (tr[fa].l == u) tr[fa].l = 0 ; else tr[fa].r = 0 ;
			upd(fa), upd(tr[fa].fa) ;
		} else T = 0 ;
		tr[u].fa = tr[u].l = tr[u].r = 0, tr[u].sz = tr[u].SZ = 1 ;
		insert_tail(T, u) ;
		return ;
	}
	if (y >= m - 1 - (B[x] ? tr[B[x]].sz : 0) + 1) {
		int tmp = findrk(B[x], m - 1 - y + 1) ;
		printf("%d\n", tr[tmp].id) ;
		int rk = m - 1 - y + 1, sz = tr[B[x]].sz, u ;
		if (rk - 1) {
			int l = findrk(B[x], rk - 1) ;
			splay(B[x], l, 0) ;
			if (rk != sz) {
				int r = findrk(B[x], rk + 1) ;
				splay(B[x], r, l) ;
				u = tr[r].l ;
			} else {
				u = tr[l].r ;
			}
		} else {
			if (rk != sz) {
				int r = findrk(B[x], rk + 1) ;
				splay(B[x], r, 0) ;
				u = tr[r].l ;
			} else u = B[x] ;
		}		
		int fa = tr[u].fa ;
		if (fa) {
			if (tr[fa].l == u) tr[fa].l = 0 ; else tr[fa].r = 0 ;
			upd(fa), upd(tr[fa].fa) ;
		} else B[x] = 0 ;
		tr[u].fa = tr[u].l = tr[u].r = 0, tr[u].sz = tr[u].SZ = 1 ;
		insert_tail(T, u) ;
		u = findrk(T, x) ;
		rk = x, sz = tr[T].sz ;
		if (rk - 1) {
			int l = findrk(T, rk - 1) ;
			splay(T, l, 0) ;
			if (rk != sz) {
				int r = findrk(T, rk + 1) ;
				splay(T, r, l) ;
				u = tr[r].l ;
			} else {
				u = tr[l].r ;
			}
		} else {
			if (rk != sz) {
				int r = findrk(T, rk + 1) ;
				splay(T, r, 0) ;
				u = tr[r].l ;
			} else u = T ;
		}		
		fa = tr[u].fa ;
		if (fa) {
			if (tr[fa].l == u) tr[fa].l = 0 ; else tr[fa].r = 0 ;
			upd(fa), upd(tr[fa].fa) ;
		} else T = 0 ;
		tr[u].fa = tr[u].l = tr[u].r = 0, tr[u].sz = tr[u].SZ = 1 ;
		insert_head(B[x], u) ;
	} else {
		int id = softsearch(W[x], y, 0) ;
		++ cnt ;
		tr[cnt].l = tr[cnt].r = tr[cnt].fa = 0 , tr[cnt].sz = tr[cnt].SZ = 1, tr[cnt].id = id ;
		insert(W[x], cnt) ;
		id = (x - 1) * m + id ;
		printf("%d\n", id) ;
		++ cnt ;
		tr[cnt].l = tr[cnt].r = tr[cnt].fa = 0, tr[cnt].sz = tr[cnt].SZ = 1, tr[cnt].id = id ;
		insert_tail(T, cnt) ;
		int u = findrk(T, x), rk = x, sz = tr[T].sz ;
		if (rk - 1) {
			int l = findrk(T, rk - 1) ;
			splay(T, l, 0) ;
			if (rk != sz) {
				int r = findrk(T, rk + 1) ;
				splay(T, r, l) ;
				u = tr[r].l ;
			} else {
				u = tr[l].r ;
			}
		} else {
			if (rk != sz) {
				int r = findrk(T, rk + 1) ;
				splay(T, r, 0) ;
				u = tr[r].l ;
			} else u = T ;
		}		
		int fa = tr[u].fa ;
		if (fa) {
			if (tr[fa].l == u) tr[fa].l = 0 ; else tr[fa].r = 0 ;
			upd(fa), upd(tr[fa].fa) ;
		} else T = 0 ;
		tr[u].fa = tr[u].l = tr[u].r = 0, tr[u].sz = tr[u].SZ = 1 ;
		insert_head(B[x], u) ;
	}
}

int main() {
	freopen("phalanx.in", "r", stdin) ;
	freopen("phalanx.out", "w", stdout) ;
	int q ;
	scanf("%d%d%d", &n, &m, &q) ;
	int x, y ;
	pre() ;
	for ( ; q -- ; ) {
		scanf("%d%d", &x, &y) ;
		solve(x, y) ;
	}
	return 0 ;
}

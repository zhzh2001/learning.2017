#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i) 
const int N = 2005 ;
using namespace std ;

int n, H, R, fa[N] ;
struct poi {
	int x, y, z ;
} a[N] ;

int getfa(int x) {
	return (fa[x] == x ? x : fa[x] = getfa(fa[x])) ;
}

void merge(int x, int y) {
	x = getfa(x), y = getfa(y) ;
	if (x != y) fa[x] = y ;
}

void solve() {
	scanf("%d%d%d", &n, &H, &R) ;
	rep(i, 1, n) scanf("%d%d%d", &a[i].x, &a[i].y, &a[i].z) ;
	rep(i, 1, n + 2) fa[i] = i ;
	rep(i, 1, n) {
		if (a[i].z <= R) merge(i, n + 1) ;
		if (H - a[i].z <= R) merge(i, n + 2) ;
	}
	rep(i, 1, n) rep(j, i + 1, n) {
		unsigned long long dis = 0 ;
		dis += (ll) (a[i].x - a[j].x) * (a[i].x - a[j].x) ;
		dis += (ll) (a[i].y - a[j].y) * (a[i].y - a[j].y) ;
		dis += (ll) (a[i].z - a[j].z) * (a[i].z - a[j].z) ;
		if (dis <= (unsigned long long) R * R * 4) merge(i, j) ;
	}
	if (getfa(n + 1) == getfa(n + 2)) printf("Yes\n") ; else printf("No\n") ;
}

int main() {
	freopen("cheese.in", "r", stdin) ;
	freopen("cheese.out", "w", stdout) ;
	int T ; 
	scanf("%d", &T) ;
	for ( ; T -- ; ) {
		solve() ;
	}
	return 0 ; 
}

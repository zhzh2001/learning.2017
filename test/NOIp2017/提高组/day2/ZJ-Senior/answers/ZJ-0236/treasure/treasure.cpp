#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <algorithm>
typedef long long ll ;
#define rep(i, a, b) for (int i = a; i <= b; ++ i)
const int N = 15, M = (1 << 12) + 1, inf = 1e9 + 7 ; 
using namespace std ;

int n, m, g[N][N], st[N - 2][N - 2][N - 2][M], cnt[M], U[M][N][M], f[N][N][M] ;

inline int count(int x) {
	int ret = 0 ;
	for ( ; x; x /= 2) ret += x & 1 ;
	return ret ;
}

void pre() {
	rep(i, 0, (1 << n) - 1) cnt[i] = count(i) ;
	rep(i, 1, (1 << n) - 1) rep(j, 1, n) {
		U[i][j][0] = 0 ;
		for (int k = i - 1; k > 0; k = (k - 1) & i) {
			if ((k >> (j - 1)) & 1) ; else continue ;
			U[i][j][++ U[i][j][0]] = k ;
		}
	}
	rep(u, 1, n) rep(v, 1, n) if (u != v) rep(depv, 0, n - 2) for (int S = (1 << n) - 1; S >= 1; -- S) {
		if ((S >> (u - 1)) & 1) ; else continue ;
		if ((S >> (v - 1)) & 1) ; else continue ;
		if (n - cnt[S] < depv) continue ;
		st[u][v][depv][++ st[u][v][depv][0]] = S ;
	} 
}

void upd(int &x, int y) {
	if (y < x) x = y ;
}

void dp() {
	rep(i, 1, n) rep(j, 0, n - 1) rep(k, 0, (1 << n) - 1) f[i][j][k] = inf ;
	rep(i, 1, n) rep(j, 0, n - 1) f[i][j][1 << (i - 1)] = 0 ;
	for (int depv = n - 2; depv >= 0; -- depv) rep(u, 1, n) rep(v, 1, n) if (u != v) rep(stv, 1, st[u][v][depv][0]) {
		int STV = st[u][v][depv][stv], val = g[u][v] == inf ? inf : g[u][v] * (depv + 1) ;
		if (val == inf) continue ;
		rep(stu, 1, U[STV][u][0]) {
			int STU = U[STV][u][stu] ;
			upd(f[v][depv][STV], f[u][depv + 1][STU] + f[v][depv][STV ^ STU] + val) ;
		}
	}
}

int main() {
	freopen("treasure.in", "r", stdin) ;
	freopen("treasure.out", "w", stdout) ;
	scanf("%d%d", &n, &m) ;
	rep(i, 1, n) rep(j, 1, n) g[i][j] = inf ;
	int x, y, z ;
	rep(i, 1, m) {
		scanf("%d%d%d", &x, &y, &z) ;
		upd(g[x][y], z), upd(g[y][x], z) ;
	}
	pre() ;
	dp() ;
	int ans = inf ;
	rep(i, 1, n) ans = min(ans, f[i][0][(1 << n) - 1]) ;
	printf("%d\n", ans) ;
	return 0; 
}

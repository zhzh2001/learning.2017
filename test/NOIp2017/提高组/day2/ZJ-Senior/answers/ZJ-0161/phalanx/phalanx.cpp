#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <vector>
#include <ctime>
#include <climits>
using namespace std;
typedef long long ll;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(ll x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(ll x){
	wrote(x);puts("");
}
struct ppap{int x,y;}a[300010];
int f[1010][1010],n,m,q;
int t[1000010],N;
ll b[1000010];
inline void add(int x,int v){for(;x<=N;x+=x&-x)t[x]+=v;}
inline int ssum(int x){int ans=0;for(;x;x-=x&-x)ans+=t[x];return ans;}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	bool flag=1;
	for(int i=1;i<=q;i++){
		a[i].x=read();a[i].y=read();
		if(a[i].x!=1)flag=0;
	}
	if(flag){
		N=n+m-1+q;int cnt=n+m-1;
		for(int i=1;i<=m;i++)b[i]=i,add(i,1);
		for(int i=2;i<=n;i++)b[m+i-1]=(ll)i*m,add(m+i-1,1);
		for(int i=1;i<=q;i++){
			int l=1,r=cnt,w;
			while(l<=r){
				int mid=l+r>>1;
				if(ssum(mid)>=a[i].y)w=mid,r=mid-1;
				else l=mid+1;
			}
			wroteln(b[w]);b[++cnt]=b[w];b[w]=0;
			add(w,-1);add(cnt,1);
		}
	}else{
		int cnt=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)f[i][j]=++cnt;
		for(int i=1;i<=q;i++){
			wroteln(f[a[i].x][a[i].y]);int p=f[a[i].x][a[i].y];
			for(int j=a[i].y;j<m;j++)f[a[i].x][j]=f[a[i].x][j+1];
			for(int j=a[i].x;j<n;j++)f[j][m]=f[j+1][m];f[n][m]=p;
		}
	}
	return 0;
}

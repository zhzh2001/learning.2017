#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <vector>
#include <ctime>
#include <climits>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(int x){
	wrote(x);puts("");
}
int a[21],p[21],n,m;
int ans=1e9,d[21][21];
bool vis[21];
inline void dfs(int x,int sum){
	if(sum>=ans)return;
	if(x==n+1){ans=sum;return;}
	for(int i=1;i<=n;i++)if(!vis[i]){
		int k=1e9,di;
		for(int j=1;j<x;j++)if(d[i][a[j]]<1e9&&d[i][a[j]]*p[a[j]]<k)k=d[i][a[j]]*p[a[j]],di=a[j];
		if(k==1e9)continue;
		vis[i]=1;a[x]=i;
		p[i]=p[di]+1;
		dfs(x+1,sum+k);
		vis[i]=0;p[i]=0;
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)d[i][j]=1e9;
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),z=read();
		d[x][y]=d[y][x]=min(d[x][y],z);
	}
	for(int i=1;i<=n;i++){
		vis[i]=1;a[1]=i;p[i]=1;
		dfs(2,0);
		vis[i]=0;p[i]=0;
	}
	wroteln(ans);
	return 0;
}

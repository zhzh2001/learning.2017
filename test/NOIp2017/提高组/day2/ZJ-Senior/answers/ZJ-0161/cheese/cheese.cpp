#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <cstdlib>
#include <queue>
#include <set>
#include <map>
#include <string>
#include <vector>
#include <ctime>
#include <climits>
using namespace std;
typedef long double D;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void wrote(int x){
	if(x<0)putchar('-'),x=-x;
	if(x>9)wrote(x/10);putchar(x%10+'0');
}
inline void wroteln(int x){
	wrote(x);puts("");
}
struct ppap{int x,y,z;}a[1010];
int n,h,r;
int nedge=0,p[3000010],nex[3000010],head[3000010];
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline D sqr(D x){return x*x;}
inline D cal(int i,int j){
	return sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z));
}
bool vis[2010];
inline void dfs(int x){
	vis[x]=1;
	for(int k=head[x];k;k=nex[k])if(!vis[p[k]])dfs(p[k]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--){
		n=read();h=read();r=read();
		nedge=0;memset(nex,0,sizeof nex);memset(head,0,sizeof head);
		for(int i=1;i<=n;i++)a[i].x=read(),a[i].y=read(),a[i].z=read();
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++){
				D dis=cal(i,j);
				if(dis-r*2<1e-8)addedge(i,j),addedge(j,i);
			}
		memset(vis,0,sizeof vis);
		for(int i=1;i<=n;i++){
			if(a[i].z<=r)addedge(0,i);
			if(a[i].z+r>=h)addedge(i,n+1);
		}
		dfs(0);
		puts(vis[n+1]?"Yes":"No");
	}
	return 0;
}

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
const int MAXN = 15;
typedef long long LL;

int n, m;
namespace solver1 {
  int mp[MAXN][MAXN];
  int visit[MAXN];
  int ans;
  void read() {
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= n; j++) {
	if (i == j) mp[i][j] = 0;
	else mp[i][j] = 1e8;
      }
    }
    for (int i = 1; i <= m; i++) {
      int x, y, w;
      scanf("%d%d%d", &x, &y, &w);
      mp[x][y] = mp[y][x] = std::min(mp[x][y], w);
    }
  }
  void work(int x) {
    std::queue <int> q;
    memset(visit, 0, sizeof visit);
    q.push(x);
    visit[x] = 1;
    int tmp = 0;
    while (!q.empty()) {
      int u = q.front(); q.pop();
      for (int i = 1; i <= n; i++) {
	if (i == u) continue;
	if (visit[i]) continue;
	if (mp[i][u] <= 1e7) {
	  tmp += mp[i][u] * visit[u];
	  visit[i] = visit[u] + 1;
	  q.push(i);
	}
      }
    }
    //printf("%d %d\n", x, tmp);
    ans = std::min(ans, tmp);
  }
  void main() {
    ans = 1e9;
    for (int i = 1; i <= n; i++) {
      work(i);
    }
    printf("%d\n", ans);
  }
}
int main() {
#ifndef LOCAL
  freopen("treasure.in", "r", stdin);
  freopen("treasure.out", "w", stdout);
#endif
  scanf("%d%d", &n, &m);
  solver1::read();
  solver1::main();
  return 0;
}

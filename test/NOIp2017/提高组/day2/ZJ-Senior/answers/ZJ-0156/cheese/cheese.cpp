#include <cstdio>
#include <cstring>
#include <algorithm>
#include <queue>
typedef signed long long LL;
typedef unsigned long long ULL;
const int MAXN = 1000 + 10;

struct Point {
  LL x, y, z;
  void read() {scanf("%lld%lld%lld", &x, &y, &z);}
}p[MAXN];
int n; LL h, r;
inline ULL sqr(LL x) {
  return x * x;
}
namespace solver1 {
  bool visit[MAXN];
  bool check(Point a, Point b) {
    return sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z) <= 4 * sqr(r);
  }
  void main() {
    memset(visit, 0, sizeof visit);
    std::queue <int> q;
    for (int i = 1; i <= n; i++) if (p[i].z <= r) { q.push(i); visit[i] = 1;}
    while(!q.empty()) {
      int x = q.front(); q.pop();
      for (int i = 1; i <= n; i++) {
	if (visit[i]) continue;
	if (check(p[x], p[i])) {
	  q.push(i);
	  visit[i] = 1;
	}
      }
    }
    for (int i = 1; i <= n; i++) {
      if (p[i].z + r >= h && visit[i]) {
	puts("Yes"); return;
      }
    }
    puts("No");
  }
}
int main() {
#ifndef LOCAL
  freopen("cheese.in", "r", stdin);
  freopen("cheese.out", "w", stdout);
#endif
  int t;
  scanf("%d", &t);
  while (t--) {
    scanf("%d%lld%lld", &n, &h, &r);
    for (int i = 1; i <= n; i++) {
      p[i].read();
    }
    solver1::main();
  }
  return 0;
}

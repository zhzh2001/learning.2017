#include <cstdio>
#include <cstring>
#include <algorithm>
typedef long long LL;

int n, m, q;
namespace solver1 {
  int matrix[1010][1010];
  void main() {
    for (int i = 1; i <= n; i++) {
      for (int j = 1; j <= m; j++) {
	matrix[i][j] = (i - 1) * m + j;
      }
    }
    while(q--) {
      int x, y;
      scanf("%d%d", &x, &y);
      int tmp = matrix[x][y];
      printf("%d\n", tmp);
      for (int i = y; i < m; i++) {
	matrix[x][i] = matrix[x][i + 1];
      }
      for (int i = x; i < n; i++) {
	matrix[i][m] = matrix[i + 1][m];
      }
      matrix[n][m] = tmp;
    }
  }
}
namespace solver2 {
  const int SIZE = 6e5 + 10;
  LL seq[SIZE << 1];
  namespace BIT {
    int a[SIZE << 1], n;
    inline void init(int _n) {n = _n;}
    void add(int x, int y) {
      for(;x <= n; x += x & (-x)) a[x] += y;
    }
    int query(int x) {
      int ans = 0;
      for (;x > 0; x -= x & (-x)) ans += a[x];
      return ans;
    }
  }
  void main() {
    for (int i = 1; i <= m; i++) {
      seq[i] = i;
    }
    for (int i = 2; i <= n; i++) {
      seq[i + m - 1] = 1ll * i * m;
    }
    BIT::init(n + m + q);
    int cur = n + m - 1;
    while (q--) {
      int x, y;
      scanf("%d%d", &x, &y);
      //x = 1, y = rand() % m + 1;
      if (x != 1) return;
      //int p = y + BIT::query(y);
      int l = y, r = cur, p;
      while(l <= r) {
	int mid = (l + r) >> 1;
	if (mid + BIT::query(mid) >= y) p = mid, r = mid - 1;
	else l = mid + 1;
      }
      LL tmp = seq[p];
      printf("%lld\n", tmp);
      seq[++cur] = tmp;
      BIT::add(p, -1);
    }
  }
}
int main() {
#ifndef LOCAL
  freopen("phalanx.in", "r", stdin);
  freopen("phalanx.out", "w", stdout);
#endif
  scanf("%d%d%d", &n, &m, &q);
  //n = 3e5, m = 3e5, q = 3e5;
  
  if ((m <= 1000 && q <= 500) || (1ll * n * m <= 1e8 && 1ll * (n + m) * q <= 1e8)) {
    solver1::main();
  } else {
    solver2::main();
  }
  return 0;
}

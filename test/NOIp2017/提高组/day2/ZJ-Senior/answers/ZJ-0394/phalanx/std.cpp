#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
using namespace std;
inline void judge(){
	freopen("phalanx.in","r",stdin);
	freopen("std.out","w",stdout);
}
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}x*=f;
}
const int maxn=10005;
int n,m,q;
long long a[maxn][maxn];
int main(){
	judge();
	read(n);read(m);read(q);
	rep(i,1,n)rep(j,1,m)a[i][j]=1ll*(i-1)*m+j;
	while(q--){
		int x,y;read(x);read(y);
		printf("%lld\n",a[x][y]);
		long long ans=a[x][y];
		rep(i,y,m-1)a[x][i]=a[x][i+1];
		rep(j,x,n-1)a[j][m]=a[j+1][m];
		a[n][m]=ans;
	}
	return 0;
}

#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
using namespace std;
inline void judge(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
}
typedef long long i64;
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}x*=f;
}
typedef pair<int,int> pin;
#define mk make_pair
#define w1 first
#define w2 second
const int maxn=300005;
namespace treap{
	const int maxn=1200005;//!!!
	int tot,son[maxn][2],fa[maxn],key[maxn],sum[maxn],v[maxn];
	i64 id[maxn];
	inline void update(int x){
		sum[x]=sum[son[x][0]]+sum[son[x][1]]+v[x];
	}
	inline void rotate(int x,int &root){
		int y=fa[x],z=fa[y],d=(son[y][1]==x);if(z)son[z][son[z][1]==y]=x;
		fa[x]=z;fa[y]=x;fa[son[x][!d]]=y;son[y][d]=son[x][!d];son[x][!d]=y;
		if(root==y)root=x;
		update(y);update(x);
	}
	inline void insert(int k,int &x,int &root,int val,int f){
		if(!x){
	//		assert(k==1);
			x=val;fa[x]=f;
			return;
		}
		if(k-1<=sum[son[x][0]]){
			insert(k,son[x][0],root,val,x);
			if(key[son[x][0]]<key[x])rotate(son[x][0],root);
		}
		else{
			insert(k-sum[son[x][0]]-v[x],son[x][1],root,val,x);
			if(key[son[x][1]]<key[x])rotate(son[x][1],root);
		}
		update(x);
	}
	inline pin find(int x,int k){
	//	assert(x);
		if(k<=sum[son[x][0]])return find(son[x][0],k);
		if(k<=sum[son[x][0]]+v[x])return mk(x,k-sum[son[x][0]]);
		return find(son[x][1],k-sum[son[x][0]]-v[x]);
	}
	inline void goup(int x){
		if(!x)return;
		update(x);goup(fa[x]);
	}
	inline void erase(int x,int &root){
		if(son[x][0]&&son[x][1]){
			if(key[son[x][0]]<key[son[x][1]])
				rotate(son[x][0],root);
			else
				rotate(son[x][1],root);
			erase(x,root);
			return;
		}
		if(root==x){
			root=son[x][0]+son[x][1];
		}
		int y=fa[x];
		if(y)son[y][son[y][1]==x]=son[x][0]+son[x][1];
		if(son[x][0]+son[x][1])fa[son[x][0]+son[x][1]]=y;
		goup(y);
		son[x][0]=son[x][1]=0;update(x);
	}
	inline int newnode(i64 st,int res){
		tot++;
		id[tot]=st;v[tot]=res;key[tot]=rand();
		update(tot);
		return tot;
	}
}
int n,m,q;
int root[maxn],rt;
int main(){
	judge();
	read(n);read(m);read(q);
	rep(i,1,n)root[i]=treap::newnode(1ll*(i-1)*m+1,m-1);
	rep(i,1,n)
		treap::insert(i,rt,rt,treap::newnode(1ll*i*m,1),0);
	while(q--){
		int x,y;read(x);read(y);i64 ans;
		if(y==m){
			pin v=treap::find(rt,x);
			ans=treap::id[v.w1];
			treap::erase(v.w1,rt);
			treap::insert(n,rt,rt,v.w1,0);
		}else{
			pin v1=treap::find(root[x],y);
			pin v2=treap::find(rt,x);
			ans=treap::id[v1.w1]+v1.w2-1;
			int nowres=treap::v[v1.w1]-v1.w2;
			treap::v[v1.w1]=v1.w2-1;
			if(treap::v[v1.w1]==0)
				treap::erase(v1.w1,root[x]);
			else
				treap::goup(v1.w1);
			if(nowres)
				treap::insert(y,root[x],root[x],treap::newnode(ans+1,nowres),0);
			treap::erase(v2.w1,rt);
			treap::insert(m-1,root[x],root[x],v2.w1,0);
			treap::insert(n,rt,rt,treap::newnode(ans,1),0);
		}
		printf("%lld\n",ans);
	}
	return 0;
}

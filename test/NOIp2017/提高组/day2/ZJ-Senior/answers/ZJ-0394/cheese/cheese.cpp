#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
#define forE(i,x)  for(int i=head[x];i!=-1;i=ne[i])
using namespace std;
inline void judge(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
}
typedef long long i64;
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}x*=f;
}
const int maxn=1010;
int head[maxn],t[maxn*maxn<<1],ne[maxn*maxn<<1];
int num;
inline void addedge(int x,int y){
	ne[++num]=head[x];head[x]=num;t[num]=y;
	ne[++num]=head[y];head[y]=num;t[num]=x;
}
struct Point{
	int x,y,z;
	inline Point(){}
	inline Point(int _x,int _y,int _z){x=_x;y=_y;z=_z;}
	inline void init(){read(x);read(y);read(z);}
}p[maxn];
#define sqr(a) (1ll*(a)*(a))
inline i64 dis(const Point &a,const Point &b){
	return sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z);
}
bool vis[maxn];
int n,h,r;
inline void dfs(int x){
	vis[x]=1;
	forE(i,x)if(!vis[t[i]])dfs(t[i]);
}
inline void solve(){
	read(n);read(h);read(r);
	rep(i,1,n)p[i].init();
	rep(i,1,n+2)head[i]=-1;num=0;
	rep(i,1,n)rep(j,i+1,n){
		if(dis(p[i],p[j])<=sqr(2*r))addedge(i,j);
	}
	rep(i,1,n){
		if(p[i].z<=r)addedge(n+1,i);
		if(p[i].z+r>=h)addedge(n+2,i);
	}memset(vis,0,sizeof(vis));
	dfs(n+1);
	if(vis[n+2])puts("Yes");else puts("No");
}
int main(){
	judge();
	int T;read(T);
	while(T--)solve();
	return 0;
}

#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
#define per(i,a,b) for(int i=(a);i>=(b);i--)
#define lowbit(x) ((x)&(-(x)))
using namespace std;
inline void judge(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
}
inline void read(int &x){
	x=0;char ch=getchar();int f=1;
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=10*x+ch-'0';ch=getchar();}x*=f;
}
const int maxn=14;
int f[2][1<<12][1<<12];
const int inf=1e9;
int a[20][20],n,m;
int dis[1<<12][1<<12];
int faq[1<<maxn];
int one[15][1<<12];
int main(){
	judge();
	read(n);read(m);
//	sleep(1);
	rep(i,0,n)faq[1<<i]=i+1;
	rep(i,1,n)rep(j,1,n)a[i][j]=inf;
	rep(i,1,n)a[i][i]=0;
	rep(i,1,m){
		int x,y,z;read(x);read(y);read(z);
		a[x][y]=min(a[x][y],z);a[y][x]=min(a[y][x],z);
	}
	rep(i,1,n)rep(s,0,(1<<n)-1){
		one[i][s]=inf;
		rep(j,1,n)if(s&(1<<(j-1)))one[i][s]=min(one[i][s],a[i][j]);
	}
//	cerr<<clock()<<endl;
	rep(s1,0,(1<<n)-1){
		int opp=((1<<n)-1)^s1;
		for(int s2=opp;s2;s2=(s2-1)&opp)
			dis[s1][s2]=min(inf,dis[s1^lowbit(s1)][s2]+one[faq[lowbit(s1)]][s2]);
	}
//	cerr<<clock()<<endl;
	int st=0,ed=1;
	rep(s,0,(1<<n)-1)for(int news=s;news;news=(news-1)&s)f[st][s][news]=inf;
	rep(i,1,n)f[st][1<<(i-1)][1<<(i-1)]=0;int ans=inf;
	rep(len,1,n-1){
		rep(s,0,(1<<n)-1)for(int news=s;news;news=(news-1)&s)f[ed][s][news]=inf;
		rep(s,0,(1<<n)-1)for(int news=s;news;news=(news-1)&s)if(f[st][s][news]!=inf){
			int opp=((1<<n)-1)^s;
			for(int trans=opp;trans;trans=(trans-1)&opp){
				if(dis[trans][news]==inf)continue;
				f[ed][s|trans][trans]=min(f[ed][s|trans][trans],f[st][s][news]+dis[trans][news]*len);
			}
		}		
		rep(s,1,(1<<n)-1)ans=min(ans,f[ed][(1<<n)-1][s]);
		swap(st,ed);
	}
	//cerr<<clock()<<endl;
	cout<<ans<<endl;
	return 0;
}

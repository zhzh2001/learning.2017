#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
#define N (100001)
#define LL long long
#define rt register int
using namespace std;
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p-48;
		p = getchar();
	}while (isdigit(p));
	if (fl) t = -t;
}
int n, m, minans, st, h, t;
int map[13][13], fa[13], q[5100], dep[11];
int f[21][21][10330];
inline void out(){
	//for (int i = 1; i <= n; i++) printf("%d ", fa[i]);
	h = t = 0;
	q[++t] = st;
	dep[st] = 0;
	while (h < t){
		int u = q[++h];
		for (int i = 1; i <= n; i++){
			if (fa[i] == u && i != u){
				q[++t] = i;
				dep[i] = dep[u]+1;
			}
		}
	}
	if (t != n) return;
	int tot = 0;
	for (int i = 1; i <= n; i++){
		if (dep[i] == 0 && i != st) return;
		tot += dep[i]*map[i][fa[i]];
	}
	//printf("%d \n", tot);
	if (tot < minans) minans = tot;
}
void dfs(int x){
	if (x == n+1){
		out();
		return;
	}
	if (x == st) fa[x] = 0, dfs(x+1);
	else{
		for (int i = 1; i <= n; i++){
			if (fa[i] != x && i != x && map[x][i]){
				fa[x] = i;
				dfs(x+1);
			}
		}
	}
}
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	read(n), read(m);
	minans = 100000007;
	for (int i = 1; i <= m; i++){
		int x, y, z;
		read(x), read(y), read(z);
		if (map[x][y] == 0 || map[x][y] > z){
			map[x][y] = map[y][x] = z;
		}
	}
	for (int i = 1; i <= n; i++){
		st = i;
		dfs(1);
	} 
	printf("%d", minans);
	return 0;
}


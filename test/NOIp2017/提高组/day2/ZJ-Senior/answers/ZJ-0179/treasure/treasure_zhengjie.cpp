#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
#define N (100001)
#define LL long long
#define rt register int
using namespace std;
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p-48;
		p = getchar();
	}while (isdigit(p));
	if (fl) t = -t;
}
int n, m, minans;
int map[13][13];
int f[21][21][10330];
int main(){
	//freopen("treasure.in", "r", stdin);
	//freopen("treasure.out", "w", stdout);
	read(n), read(m);
	minans = 100000007;
	for (int i = 1; i <= m; i++){
		int x, y, z;
		read(x), read(y), read(z);
		if (map[x][y] == 0 || map[x][y] > z){
			map[x][y] = map[y][x] = z;
		}
	}
	for (int i = 1; i <= n; i++){
		for (int j = 1; j <= n; j++) printf("%d ", map[i][j]);
		printf("\n");
	}
	memset(f, 127, sizeof(f));
	for (int i = 1; i <= n; i++){
		f[i][0][1<<(i-1)] = 0;
	}
	for (int dis = 0; dis < n; dis++){
		for (rt now = 1; now <= n; now++){
			for (rt j = 0; j <= (1<<n)-1; j++){
				if (!(j & 1<<(now-1))) continue;
				for (int i = 1; i <= n; i++){
					if (j & 1<<(i-1)){
						//f[now][dis][j] = min(f[now][dis][j], f[i][dis+1][j]);
						continue;
					}
					if (!map[i][now]) continue;
					if (f[i][dis+1][j|(1<<(i-1))] > f[now][dis][j]+(dis+1)*map[now][i]){
						f[i][dis+1][j|(1<<(i-1))] = f[now][dis][j]+(dis+1)*map[now][i];
						f[now][dis][j|(1<<(i-1))] = min(f[now][dis][j|(1<<(i-1))], f[i][dis+1][j|(1<<(i-1))]);
					}		
				}	
			}			
		}
		
	}

	for (int i = 1; i <= n; i++){
		for (int dis = 0; dis < n; dis++)
			for (int j = 1; j <= (1<<n)-1; j++){
				//printf("%d %d %d %d\n", i, dis, j, f[i][dis][j]);
				minans = min(minans, f[i][dis][(1<<n)-1]);
			}
	}
	printf("%d", minans); 
	return 0;
}


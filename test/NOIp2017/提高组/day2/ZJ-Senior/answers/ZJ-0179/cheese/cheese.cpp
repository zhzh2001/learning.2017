#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
#define N (1001)
#define LL long long
using namespace std;
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p-48;
		p = getchar();
	}while (isdigit(p));
	if (fl) t = -t;
}
LL T, n, hh, r, k, h, t;
LL x[N], y[N], z[N];
int a[N*N], nxt[N*N], head[N], q[N<<1];
bool ha, vis[N];
LL sqr(LL x){
	return x*x;
}
inline void add(int x, int y){
	a[++k] = y; nxt[k] = head[x]; head[x] = k;
}
int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	read(T);
	while (T--){
		memset(head, 0, sizeof(head));
		k = 0;
		read(n), read(hh), read(r);
		for (int i = 1; i <= n; i++) read(x[i]), read(y[i]), read(z[i]);
		for (int i = 1; i <= n; i++){
			if (z[i]+r >= hh) add(i, n+1);
			if (z[i]-r <= 0) add(0, i);
			for (int j = i+1; j <= n; j++){
				if (sqr(2*r) >= sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])){
					add(i, j); add(j, i);
				}
			}
		}
		memset(vis, 0, sizeof(vis));
		h = t = 0;
		q[++t] = 0;
		ha = 0;
		vis[0] = 1;
		while (h < t){
			int u = q[++h];
			for (int p = head[u]; p; p = nxt[p]){
				if (!vis[a[p]]){
					q[++t] = a[p];
					vis[a[p]] = 1;
					if (a[p] == n+1) ha = 1;
				}
			}
		}
		if (ha) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

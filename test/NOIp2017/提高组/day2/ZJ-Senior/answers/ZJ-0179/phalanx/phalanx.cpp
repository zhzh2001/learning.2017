#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
#define N (1001)
#define LL long long
using namespace std;
int map[N][N];
template <typename T> void read(T &t){
	t = 0;
	bool fl = 0;
	char p = getchar();
	while (!isdigit(p)){
		if (p == '-') fl = 1;
		p = getchar();
	}
	do{
		t = t*10+p-48;
		p = getchar();
	}while (isdigit(p));
	if (fl) t = -t;
}
int n, m, q;
int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n), read(m), read(q);
	for (int i = 1; i <= n; i++){
		for (int j = 1; j <= n; j++) map[i][j] = (i-1)*m+j;
	}
	while (q--){
		int x, y;
		read(x), read(y);
		int cc = map[x][y];
		printf("%d\n", map[x][y]);
		for (int j = y; j <= m-1; j++) map[x][j] = map[x][j+1];
		for (int i = x; i <= n-1; i++) map[i][m] = map[i+1][m];
		map[n][m] = cc;
	}
	return 0;
}


#include <cstdio>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <map>
#include <queue>
#include <list>

using namespace std;

#define rep(i,a) for (int i=1, i_=(a); i<=i_; ++i)
#define repi(i,a,b) for (int i=(a), i_=(b); i<=i_; ++i)
#define repd(i,a,b) for (int i=(a), i_=(b); i>=i_; --i)

#define MAXN 300100
#define MAXN2 1010

int read() { int ret = -1; scanf("%d", &ret); return ret; }

int n, m, q, p[MAXN2][MAXN2], Q[MAXN][2], tree[MAXN], xnum[MAXN];

inline void modify(int T[], int l, int x, int k) {
	while (x <= l) T[x] += k, x += x&-x;
}

inline int query(int T[], int l, int x) {
	int ret = 0;
	while (x) ret += T[x], x -= x&-x;
	return ret;
}

inline void trans(int &nowx, int &nowy, int tx, int ty) {
	if (nowx<tx || nowy<ty || (nowx>tx &&nowy<m)) return;
	if (nowx==n && nowy==m) {
		nowx = tx;
		nowy = ty;
		return;
	}
	if (nowx==tx && nowy<m) {
		++nowy;
		return;
	}
	if (nowy == m) {
		++nowx;
		return;
	}
}

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	n = read();
	m = read();
	q = read();
	if (m<=1000 && n<=1000) {
		rep(i,n) rep(j,m) p[i][j] = (i-1)*m+j;
		rep(i,q) {
			int x = read(), y = read(), tmp = p[x][y];
			repi(i,y,m-1) p[x][i] = p[x][i+1];
			repi(i,x,n-1) p[i][m] = p[i+1][m];
			p[n][m] = tmp;
			printf("%d\n", tmp);
		}
		return 0;
	}
	if (q <= 500) {
		rep(i,q) {
			int x = read(), y = read();
			Q[i][0] = x;
			Q[i][1] = y;
			repd(j,i-1,1) trans(x, y, Q[j][0], Q[j][1]);
			printf("%d\n", (x-1)*m+y);
		}
		return 0;
	}
	if (n == 1) {
		rep(i,q) {
			int x = read(), y = read();
			printf("%d\n", (y + query(tree, m, y) - 1) % m + 1);
			modify(tree, m, y, 1);
		}
		return 0;
	}
	bool flag = 1;
	rep(i,q) {
		int x = read(), y = read();
		Q[i][0] = x;
		Q[i][1] = y;
		if (x != 1) flag = 0;
	}
	if (flag) {
		rep(i,q) {
			int x = Q[i][0], y = Q[i][1];
			y = (y + query(tree, m, y) - 1) % (n+m-1) + 1;
			if (y > m) x = y-m, y = m;
			printf("%d\n", (x-1)*m+y);
			modify(tree, m, Q[i][1], 1);
		}
		return 0;
	}
	
	return 0;
}


#include <cstdio>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <map>
#include <queue>
#include <list>

using namespace std;

#define rep(i,a) for (int i=1, i_=(a); i<=i_; ++i)
#define repi(i,a,b) for (int i=(a), i_=(b); i<=i_; ++i)
#define repd(i,a,b) for (int i=(a), i_=(b); i>=i_; --i)

typedef long long ll;

#define MAXN 2000

int read() { int ret = -1; scanf("%d", &ret); return ret; }

vector<int> G[MAXN];
int n, h, r, x[MAXN], y[MAXN], z[MAXN], v[MAXN];

inline ll sqr(int x) {
	return x*x;
}

inline ll dis(int a, int b) {
	return (ll) sqr(x[a]-x[b]) + sqr(y[a]-y[b]) + sqr(z[a]-z[b]);
}

inline bool check(int a, int b) {
	if (a > b) swap(a, b);
	if (a == 0) return z[b]<=r;
	if (b == n+1) return z[a]>=h-r;
	return (ll) dis(a, b) <= (ll) 4*r*r;
}

bool dfs(int x) {
	if (x == n+1) return 1;
	if (v[x]) return 0;
	v[x] = 1;
	repi(i,0,G[x].size()-1) if (dfs(G[x][i])) return 1;
	return 0;
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	for (int T=read(); T; T--) {
		repi(i,0,n) G[i].clear();
		memset(x, 0, sizeof(x));
		memset(y, 0, sizeof(y));
		memset(z, 0, sizeof(z));
		memset(v, 0, sizeof(v));
		n = read();
		h = read();
		r = read();
		rep(i,n) {
			x[i] = read();
			y[i] = read();
			z[i] = read();
			repi(j,0,i) if (check(i, j)) {
				G[i].push_back(j);
				G[j].push_back(i);
			}
			if (check(i, n+1)) {
				G[i].push_back(n+1);
				G[n+1].push_back(i);
			}
			
		}
		if (dfs(0)) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}


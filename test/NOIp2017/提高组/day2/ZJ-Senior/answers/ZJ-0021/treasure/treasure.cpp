#include <cstdio>
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <map>
#include <queue>
#include <list>

using namespace std;

#define rep(i,a) for (int i=1, i_=(a); i<=i_; ++i)
#define repi(i,a,b) for (int i=(a), i_=(b); i<=i_; ++i)
#define repd(i,a,b) for (int i=(a), i_=(b); i>=i_; --i)

#define INF 0x3f3f3f3f
#define MAXN 20

int read() { int ret = -1; scanf("%d", &ret); return ret; }

//struct data {
//	int now, dep, tot;
//	data(int now=0, int dep=0, int tot=0): now(now), dep(dep), tot(tot) {}
//	bool operator < (const data &rhs) const {
//		return tot > rhs.tot;
//	}
//};

struct edge {
	int u, v, w;
	edge(int u=0, int v=0, int w=0): u(u), v(v), w(w) {}
	bool operator < (const edge &rhs) const {
		return w > rhs.w;
	}
};

int n, m, e[MAXN][MAXN], d[MAXN][MAXN], dis[MAXN], dd[MAXN], f[MAXN], v[MAXN];
//priority_queue<data> Q;
priority_queue<edge> E;
vector<int> G[MAXN];

//void bfs(int x) {
//	while (!Q.empty()) Q.pop();
//	dis[x] = 0;
//	Q.push(data(x, 1, 0));
//	while (!Q.empty()) {
//		data cur = Q.top();
//		int now = cur.now, dep = cur.dep, tot = cur.tot;
//		Q.pop();
//		if (dis[now] < tot) continue;
//		
////		cout << now << " " << dep << " " << tot << endl;
//
//		rep(i,n) if (e[now][i] < INF) {
//			int tmp = tot + dep * e[now][i];
//			if (tmp < dis[i]) {
//				dis[i] = tmp;
//				dd[i] = dep + 1;
//				Q.push(data(i, dep+1, tmp));
//			}
//		}
//	}
//}

int find(int x) {
	return x==f[x] ? x : f[x]=find(f[x]);
}
	
void mct() {
	rep(i,n) f[i] = i;
	int etot = 0, ret = 0;
	while (etot < n-1) {
		edge e = E.top();
		E.pop();
//		cout << e.u << " " << e.v << " " << e.w << endl;
		int f1 = find(e.u), f2 = find(e.v);
		if (f1 == f2) continue;
		f[f2] = f1;
		++etot;
		G[f1].push_back(f2);
		G[f2].push_back(f1);
	}
}

int dfs(int x, int dep) {
//	cout << x << " " << dep << endl;
	v[x] = 1;
	int ret = 0;
	repi(i,0,G[x].size()-1) {
		int now = G[x][i];
		if (!v[now]) ret += dep*e[x][now] + dfs(now, dep+1);
	}
	return ret;
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	n = read();
	m = read();
	memset(e, 0x3f, sizeof(e));
	memset(d, 0x3f, sizeof(d));
	rep(i,n) d[i][i] = e[i][i] = 0;
//	bool flag = 1, tmp = -1;
	rep(i,m) {
		int x = read(), y = read(), z = read();
//		if (tmp = -1) tmp = z;
//		if (tmp != z) flag = 0;
		d[x][y] = e[x][y] = min(e[x][y], z);
		d[y][x] = e[y][x] = min(e[y][x], z);
	}
	
//	rep(i,n) {
//		rep(j,n) printf("%d\t", e[i][j]);
//		printf("\n");
//	}
	
	int ans = INF;
//	if (flag) {
		rep(k,n) rep(i,n) rep(j,n)
			d[i][j] = min(d[i][j], d[i][k]+d[k][j]);
		rep(i,n) {
			int tot = 0;
			rep(j,n) tot += d[i][j];
			ans = min(tot, ans);
		}
		printf("%d\n", ans);
		return 0;
//	}
//	rep(i,n) {
//		memset(dis, 0x3f, sizeof(dis));
//		bfs(i);
//	}

	rep(i,n) rep(j,i-1) if (e[i][j] < INF) E.push(edge(i, j, e[i][j]));
	mct();
	rep(i,n) {
		memset(v, 0, sizeof(v));
		ans = min(ans, dfs(i, 1));
//		cout << ans << endl << endl;
	}
	printf("%d\n", ans);
	return 0;
}


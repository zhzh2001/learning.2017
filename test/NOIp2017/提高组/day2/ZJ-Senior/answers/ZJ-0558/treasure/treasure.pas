type
        disrecord=record
                min:longint;
                mins:int64;
        end;
var
        n,m,i,l,x,y,z:longint;
        min:int64;
        dis,ans:array[1..12]of disrecord;
        b:array[1..12]of boolean;
        g:array[1..12,1..12]of longint;
        pl,mint:array[0..12]of longint;
procedure pd;
var
        i,j:longint;
        s:int64;
begin
        for i:=1 to n do
        begin
                dis[i].min:=maxlongint;
                dis[i].mins:=maxlongint;
        end;
        dis[pl[0]].min:=0;
        dis[pl[0]].mins:=0;
        for i:=1 to n-1 do
        begin
                for j:=0 to i-1 do
                if g[pl[j],pl[i]]<maxlongint then
                begin
                        if (dis[pl[j]].min+1)*g[pl[j],pl[i]]<dis[pl[i]].mins then
                        begin
                                dis[pl[i]].mins:=(dis[pl[j]].min+1)*g[pl[j],pl[i]];
                                dis[pl[i]].min:=dis[pl[j]].min+1;
                        end;
                end;
                if dis[pl[i]].mins=maxlongint then exit;
        end;
        s:=0;
        for i:=1 to n-1 do
        s:=s+dis[pl[i]].mins;
        if s<min then min:=s;
end;
procedure qpl(k:longint);
var
        i:longint;
begin
        if k>=n then
        begin
                pd;
                exit;
        end;
        for i:=1 to n do
        if b[i] then
        begin
                pl[k]:=i;
                b[i]:=false;
                qpl(k+1);
                b[i]:=true;
        end;
end;
begin
        assign(input,'treasure.in');reset(input);
        assign(output,'treasure.out');rewrite(output);
        read(n,m);
        for i:=1 to n do
        for l:=1 to n do
        g[i,l]:=maxlongint;
        for i:=1 to m do
        begin
                read(x,y,z);
                if g[x,y]>z then
                begin
                        g[x,y]:=z;
                        g[y,x]:=z;
                end;
        end;
        min:=maxlongint;
        for l:=1 to n do
        begin
                for i:=1 to n do
                b[i]:=true;
                b[l]:=false;
                pl[0]:=l;
                qpl(1);
        end;
        writeln(min);
        close(input);
        close(output);
end.

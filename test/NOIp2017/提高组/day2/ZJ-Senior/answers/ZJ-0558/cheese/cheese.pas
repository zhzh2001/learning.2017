var
        t,ll,n,i,j:longint;
        c,h,rr:int64;
        l,r,ans:boolean;
        b:array[1..1000]of boolean;
        f:array[1..1000,0..1000]of longint;
        lj,x,y,z:array[1..1000]of longint;
procedure find(k:longint);
var
        i:longint;
begin
        if ans then exit;
        if z[k]+rr>=h then
        begin
                ans:=true;
                exit;
        end;
        for i:=1 to lj[k] do
        if b[f[k,i]] then
        begin
                b[f[k,i]]:=false;
                find(f[k,i]);
                if ans then exit;
        end;
end;
begin
        assign(input,'cheese.in');reset(input);
        assign(output,'cheese.out');rewrite(output);
        read(t);
        for ll:=1 to t do
        begin
                read(n,h,rr);
                c:=4*sqr(rr);
                l:=false;
                r:=false;
                for i:=1 to n do
                begin
                        read(x[i],y[i],z[i]);
                        if z[i]-rr<=0 then l:=true;
                        if z[i]+rr>=h then r:=true;
                end;
                if (l=false)or(r=false) then
                begin
                        writeln('No');
                        continue;
                end;
                fillchar(lj,sizeof(lj),0);
                for i:=1 to n-1 do
                for j:=i+1 to n do
                if c-sqr(x[i]-x[j])-sqr(y[i]-y[j])-sqr(z[i]-z[j])>=0 then
                begin
                        inc(lj[i]);
                        f[i,lj[i]]:=j;
                        inc(lj[j]);
                        f[j,lj[j]]:=i;
                end;
                ans:=false;
                for i:=1 to n do
                b[i]:=true;
                for i:=1 to n do
                if (z[i]-rr<=0)and(b[i]=true) then
                begin
                        b[i]:=false;
                        find(i);
                end;
                if ans then writeln('Yes')
                else writeln('No');
        end;
        close(input);
        close(output);
end.

//#include<iostream>
#include<math.h>
#include<cstdio>
#include<algorithm>
#include<fstream>
#include<string.h>
//#define fin cin
//#define fout cout
using namespace std;
ifstream fin("cheese.in");
ofstream fout("cheese.out");
inline long long sqr(long long x){
	return x*x;
}
long long head[1003],to[2000003],nxt[2000003],tot;
inline void add(long long a,long long b){
	to[++tot]=b;  nxt[tot]=head[a];  head[a]=tot;
}
long long T,n,h,r;
long long x[1003],y[1003],z[1003];
int vis[1003],tim;
void dfs(int x){
	if(vis[x]==tim){
		return;
	}
	vis[x]=tim;
	for(int i=head[x];i;i=nxt[i]){
		dfs(to[i]);
	}
}
int main(){
	//freopen("cheese.in","r",stdin);
	//freopen("cheese.out","w",stdout);
	fin>>T;
	while(T--){
		tim++;
		memset(head,0,sizeof(head));
		tot=0;
		fin>>n>>h>>r;
		for(int i=1;i<=n;i++){
			fin>>x[i]>>y[i]>>z[i];
		}
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				if((sqr(abs(y[i]-y[j]))+sqr(abs(z[i]-z[j]))+sqr(abs(x[i]-x[j])))<=4*r*r){
					add(i,j),add(j,i);
				}
			}
		}
		bool yes=false;
		for(int i=1;i<=n;i++){
			if(vis[i]!=tim&&z[i]<=r){
				dfs(i);
			}
			if((vis[i]==tim)&&z[i]+r>=h){
				yes=true;
				break;
			}
		}
		if(yes){
			fout<<"Yes"<<endl;
		}else{
			fout<<"No"<<endl;
		}
	}
	return 0;
}
/*

in:
3
2 4 1
0 0 1
0 0 3
2 5 1
0 0 1
0 0 4
2 5 2
0 0 2
2 0 4

out:
Yes
No
Yes

*/

//#include<iostream>
#include<math.h>
#include<cstdio>
#include<algorithm>
#include<fstream>
#include<string.h>
#include<map>
using namespace std;
const unsigned long long base=107;
ifstream fin("treasure.in");
ofstream fout("treasure.out");
int n,m;
map<unsigned long long,int>maap;
inline int min(const int &a,const int &b){
	if(a<b) return a;
	return b;
}
int mp[15][15];
long long a[1003],b[1003],c[1003];
int dfs1(int x,int fa,int dep){
	int ret=0;
	for(int i=1;i<=n;i++){
		if(mp[x][i]!=999999999&&i!=fa){
			ret+=dfs1(i,x,dep+1);
			ret+=mp[x][i]*dep;
		}
	}
	return ret;
}
inline void baoli(){
	int ans=999999999;
	for(int i=1;i<=n;i++){
		ans=min(ans,dfs1(i,0,1));
	}
	fout<<ans<<endl;
}
int dd[15];
void dfs2(int x,int dep){
	if(dep<dd[x]){
		dd[x]=dep;
	}else{
		return;
	}
	for(int i=1;i<=n;i++){
		if(mp[x][i]!=999999999){
			dfs2(i,dep+1);
		}
	}
}
inline void baoli2(){
	int ans=999999999;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			dd[j]=999999999;
		}
		dfs2(i,0);
		int ret=0;
		for(int j=1;j<=n;j++){
			ret+=c[1]*dd[j];
		}
		ans=min(ret,ans);
	}
	fout<<ans<<endl;
}
int tim=0;
long long ju[15];
int dfsdfs(){
	tim++;
	if(tim>=1300000){
		return 999999999;
	}
	int ret=999999999,tot=0;
	unsigned long long x=0;
	for(int i=1;i<=n;i++){
		x=x*base+ju[i]+2;
	}
	if(maap[x]!=0) return maap[x];
	for(int i=1;i<=n;i++){
		if(ju[i]==-1){
			tot++;
			continue;
		}
		for(int j=1;j<=n;j++){
			if((ju[j]==-1)&&mp[i][j]!=999999999&&(((tot%7)<=2))){
				ju[j]=ju[i]+1;
				ret=min(ret,mp[i][j]*ju[i]+dfsdfs());
				ju[j]=-1;
			}
		}
	}
	if(tot==0) ret=0;
	return maap[x]=ret;
}
int main(){
	//freopen("treasure.in","r",stdin);
	//freopen("treasure.out","w",stdout);
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			mp[i][j]=999999999;
		}
	}
	bool flag=true;
	for(int i=1;i<=m;i++){
		fin>>a[i]>>b[i]>>c[i];
		if(i!=1&&c[i]!=c[i-1]){
			flag=false;
		}
		mp[a[i]][b[i]]=min(mp[a[i]][b[i]],c[i]);
		mp[b[i]][a[i]]=min(mp[b[i]][a[i]],c[i]);
	}
	if(m==n-1){
		baoli();
		return 0;
	}
	if(flag){
		baoli2();
		return 0;
	}
	int ans=999999999;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			ju[j]=-1;
		}
		ju[i]=1;
		ans=min(ans,dfsdfs());
	}
	fout<<ans<<endl;
	return 0;
}
/*
in:
5 8
1 4 1
1 2 1
2 4 1
1 3 1
3 4 1
3 5 1
4 5 1
1 5 1

in:
4 5
1 2 1
1 3 3
1 4 1
2 3 4
3 4 2
out:
5

*/

var i,j,n,m,q,x,y,t:longint;
a:array[1..1000,1..1000]of longint;
begin
assign(input,'phalanx.in');
reset(input);
assign(output,'phalanx.out');
rewrite(output);
readln(n,m,q);
for i:=1 to n do
for j:=1 to m do
a[i,j]:=(i-1)*m+j;
while q>0 do
begin
  readln(x,y);
  writeln(a[x,y]);
  t:=a[x,y];
  for i:=y to m-1 do
  a[x,i]:=a[x,i+1];
  for i:=x to n-1 do
  a[i,m]:=a[i+1,m];
  a[n,m]:=t;
  dec(q);
end;
close(input);
close(output);
end.
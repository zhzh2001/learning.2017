#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int t[100][1500];
int max(int a,int b){
	return a>b?a:b;
}
int min(int a,int b){
	return a<b?a:b;
}
struct TRE{
	int x,y,c;
}e[1500];
int f[100];bool us[1500],vis[1500];
int cent[1500],wen=0;
void add(int x,int y,int c){
	e[++wen].x=x;
	e[wen].y=y;
	e[wen].c=c;
	t[x][++cent[x]]=wen;
	t[y][++cent[y]]=wen;	
}
int qw[20][20];
bool cmp(const TRE & a,const TRE & b){
	return a.c<b.c;
}
int find(int x){
	if(f[x]!=x) f[x]=find(f[x]);
	return f[x];
}
void kruskal(int num){
	for(int i=1;i<=wen;i++){
		int x=find(e[i].x);
		int y=find(e[i].y);
		if(x==y) continue;
		else{
			f[x]=y;
			num--;
			us[i]=1;
		}
		if(num==0) return;
	}
}
int dfs(int s,int dep){
	int res=0,v;
	vis[s]=1;
	for(int i=1;i<=cent[s];i++){
		if(us[t[s][i]]){
			res+=dep*e[t[s][i]].c;
			if(s==e[t[s][i]].x) v=e[t[s][i]].y;
			else v=e[t[s][i]].x;
			us[t[s][i]]=0;
			dfs(v,dep+1);
			us[t[s][i]]=1;
		}
	}
	return res;
}
int main(){
	int n,m,i,j,k,x,y,c,ans=1e9;
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&c);
		if(!qw[x][y]){
			add(x,y,c);
			qw[x][y]=wen;
		}
		else{
			e[qw[x][y]].c=min(e[qw[x][y]].c,c);
		}
	}
	for(i=1;i<=n;i++) f[i]=i;
	sort(e+1,e+wen+1,cmp);
	kruskal(n-1);
	for(i=1;i<=n;i++){
		ans=min(ans,dfs(i,1));
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

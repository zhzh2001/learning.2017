#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
#define LL long long
using namespace std;
const LL Maxn=2010;
LL Father[Maxn],x[Maxn],y[Maxn],z[Maxn],n,h,r,KASE;
LL GetFather(LL x) {if (Father[x]==x) return x; return Father[x]=GetFather(Father[x]);}
inline void Merge(LL p,LL q)
{
	LL fp=GetFather(p);
	LL fq=GetFather(q);
	Father[fp]=fq;
}
inline LL Sqr(LL x) {return x*x;}
inline bool Check(LL p,LL q) 
{return Sqr(x[p]-x[q])+Sqr(y[p]-y[q])+Sqr(z[p]-z[q])<=Sqr(2*r);}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&KASE);
	for (LL Kase=1;Kase<=KASE;Kase++)
	{
		scanf("%lld%lld%lld",&n,&h,&r);
		for (LL i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (LL i=0;i<=n+1;i++) Father[i]=i;
		for (LL i=1;i<=n;i++) 
			if (z[i]-r<=0) Merge(i,0);
		for (LL i=1;i<=n;i++)
			if (z[i]+r>=h) Merge(i,n+1);

		for (LL i=1;i<=n;i++)
			for (LL j=1;j<=n;j++)
				if (i!=j && Check(i,j))
					Merge(i,j);
		if (GetFather(0)==GetFather(n+1)) puts("Yes"); else puts("No");
	}
	return 0;
}


#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
using namespace std;
const int Maxn=20;
const int Inf=0x3f3f3f3f;

int Dep[Maxn],Depest[Maxn],Ans,F[Maxn][Maxn],head[Maxn],Value,cnt,Ret,n,m;
bool vis[Maxn];
struct EDGE
{
	int to,next;
}edge[Maxn*Maxn*2];
inline int Max(int x,int y) {return x>y?x:y;}
inline int Min(int x,int y) {return x>y?y:x;}
inline void Add(int u,int v)
{
	edge[cnt].to=v;
	edge[cnt].next=head[u];
	head[u]=cnt++;
}
void Dfs(int u)
{
	vis[u]=true; Depest[u]=Dep[u];
	for (int i=head[u];i!=-1;i=edge[i].next)
		if (!vis[edge[i].to])
		{
			Dep[edge[i].to]=Dep[u]+1;
			Dfs(edge[i].to);
			Depest[u]=Max(Depest[edge[i].to],Depest[u]);
		}
}
void Dfs2(int u,int Fa)
{
	vis[u]=true;
	if (Ret>Ans) return;
	if (Dep[u]==Depest[u]) 
	{
		Ret+=(Dep[u]-Dep[Fa])*Dep[Fa];
		return;
	}
	int k=0; 

	for (int i=head[u];i!=-1;i=edge[i].next)
		if (Depest[edge[i].to]>Depest[k] && !vis[edge[i].to]) k=edge[i].to;
	Dfs2(k,Fa);
	for (int i=head[u];i!=-1;i=edge[i].next)
		if (!vis[edge[i].to]) Dfs2(edge[i].to,u);
}
	
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m); 
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) F[i][j]=Inf;
	for (int i=1;i<=m;i++) 
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		Value=w;
		if (u==v) continue;
		if (F[u][v]<w) continue;
		F[u][v]=F[v][u]=w;
	}
	memset(head,-1,sizeof(head)); cnt=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			if (F[i][j]!=Inf) Add(i,j);
	Depest[0]=0; Ans=Inf;
	for (int i=1;i<=n;i++)
	{
		memset(vis,false,sizeof(vis)); Dep[i]=1;
		Dfs(i);
		memset(vis,false,sizeof(vis)); Ret=0; vis[i]=true;
		for (int j=head[i];j!=-1;j=edge[j].next)
			Dfs2(edge[j].to,i);
		printf("%d\n",Ret);
		Ans=Min(Ans,Ret);
	}
	printf("%d\n",Ans*Value);
	return 0;
}	


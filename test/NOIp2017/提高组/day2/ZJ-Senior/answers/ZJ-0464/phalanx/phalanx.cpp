#include <iostream>
#include <cstring>
#include <cstdio>
#include <algorithm>
#define LL long long
using namespace std;
const LL Maxn=1010;
const LL Maxm=300100;
LL n,m,q,a[Maxn][Maxn],x,y,b[Maxm<<2];
inline LL Id(LL x,LL y) {return (x-1)*m+y;}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);

	scanf("%lld%lld%lld",&n,&m,&q);
	if (n<=1000 && m<=1000)
	{
		for (LL i=1;i<=n;i++)
			for (LL j=1;j<=m;j++) a[i][j]=Id(i,j);
		for (LL i=1;i<=q;i++)
		{
			scanf("%lld%lld",&x,&y);
			LL Tmp=a[x][y];
			printf("%lld\n",Tmp);
			for (LL j=y;j<=m-1;j++) a[x][j]=a[x][j+1];
			for (LL j=x;j<=n-1;j++) a[j][m]=a[j+1][m]; 
			a[n][m]=Tmp;
		}
		return 0;
	} 
	for (LL i=1;i<=m;i++) b[i]=Id(1,i);
	for (LL i=2;i<=n;i++) b[m+i-1]=Id(i,m);
	for (LL i=1;i<=q;i++)
	{
		scanf("%lld%lld",&x,&y);
		LL Tmp=b[y];
		printf("%lld\n",Tmp);
		for (LL j=y;j<=n+m-2;j++) b[j]=b[j+1];
		b[n+m-1]=Tmp;
	}

	return 0;
}

#include <cstdio>
#include <cstring>
#include <map>
#include <algorithm>
#include <iostream>
using namespace std;
typedef pair<int, int> pl;
const int V = 1010;
const int L = 100000;
int Min(int a, int b) {
	return a < b ? a : b;
}
char get(void) {
	static char buf[L], *p1 = buf, *p2 = buf;
	if (p1 == p2) {
		p2 = (p1 = buf) + fread(buf, 1, L, stdin);
		if (p1 == p2) return EOF;		
	}
	return *p1++;
}
void Read(int &x) {
	x = 0; static char c;
	while (c > '9' || c < '0') c = get();
	while (c >= '0' && c <= '9') {
		x = x * 10 + c - '0';
		c = get();
	}
}
int N, M, Q;
int f[V][V];
int main(void) {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	int i, j, x, y;
	Read(N); Read(M); Read(Q);
	if (N <= 1000 && M <= 1000) {
		for (i = 1; i <= N; i++)
			for (j = 1; j <= M; j++) f[i][j] = (i - 1) * M + j;
		while (Q--) {
			Read(x); Read(y); 
			int tmp = f[x][y]; printf("%d\n", tmp);
			for (i = y; i <= M; i++) {
				f[x][i] = f[x][i + 1];
			}
			for (i = x; i <= N; i++) {
				f[i][M] = f[i + 1][M];
			}
			f[N][M] = tmp;
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

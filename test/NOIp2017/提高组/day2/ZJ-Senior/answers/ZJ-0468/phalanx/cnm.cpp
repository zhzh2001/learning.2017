#include <cstdio>
#include <cstring>
#include <map>
#include <algorithm>
#include <vector>
#include <iostream>
#define h(x) g[lower_bound(v + 1, v + 1 + t, x) - v]
using namespace std;
typedef pair<int, int> pl;
const int V = 1010;
const int L = 100000;
int Min(int a, int b) {
	return a < b ? a : b;
}
int QMin(int &a, int b) {
	a = Min(a, b);
}
char get(void) {
	static char buf[L], *p1 = buf, *p2 = buf;
	if (p1 == p2) {
		p2 = (p1 = buf) + fread(buf, 1, L, stdin);
		if (p1 == p2) return EOF;		
	}
	return *p1++;
}
void Read(int &x) {
	x = 0; static char c;
	while (c > '9' || c < '0') c = get();
	while (c >= '0' && c <= '9') {
		x = x * 10 + c - '0';
		c = get();
	}
}
int N, M, Q, x[V], y[V], g[25000100], t;
pl v[25000100];
void I(int x, int y) {
	if (!h(pl(x, y))) h(pl(x, y)) = (x - 1) * M + y;
}
int main(void) {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	int i, j, k;
	Read(N); Read(M); Read(Q);
	if (N <= 50000 && M <= 50000) {
		for (i = 1; i <= Q; i++) {
			Read(x[i]); Read(y[i]);
			for (k = y[i]; k <= M; k++) v[++t] = (pl(x[i], k));
			for (k = x[i]; k <= N; k++) v[++t] =(pl(k, M));
		}
		sort(v + 1, v + 1 + t);
		t = unique(v + 1, v + 1 + t) - v;
		for (i = 1; i <= Q; i++) {
			I(x[i], y[i]); int tmp = h(pl(x[i], y[i]));
			printf("%d\n", tmp);
			for (k = y[i]; k < M; k++) {
				I(x[i], k + 1);
				h(pl(x[i], k)) = h(pl(x[i], k + 1));
			}
			for (k = x[i]; k < N; k++) {
				I(k + 1, M);
				h(pl(k, M)) = h(pl(k + 1, M));
			}
			h(pl(N, M)) = tmp;
		}
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

#include <cstdio>
#include <cstring>
using namespace std;
typedef long long LL;
const int V = 1010;
const int L = 100000;
char get(void) {
	static char buf[L], *p1 = buf, *p2 = buf;
	if (p1 == p2) {
		p2 = (p1 = buf) + fread(buf, 1, L, stdin);
		if (p1 == p2) return EOF;
	}
	return *p1++;
}
void Read(LL &x) {
	x = 0; static char c; bool f = 0;
	while (c > '9' || c < '0') {
		c = get(); f ^= (c == '-');
	}
	while (c >= '0' && c <= '9') {
		x = x * 10 + c - '0'; c = get();
	}
	if (f) x = -x;
}
int head[V], sub;
struct Edge {
	int to, nxt;
	Edge(void) {}
	Edge(int to, int nxt) : to(to), nxt(nxt) {}
} edge[V * V * 2];
void Add(int a, int b) {
	edge[++sub] = Edge(b, head[a]); head[a] = sub;
}
LL T, N, H, R, vis[V];
LL x[V], y[V], z[V];
LL Sqr(LL x) {
	return x * x;
}
LL Dist(int a, int b) {
	return Sqr(x[a] - x[b]) + Sqr(y[a] - y[b]) + Sqr(z[a] - z[b]);
}
void Dfs(int u) {
	vis[u] = 1;
	for (int i = head[u], v; i; i = edge[i].nxt) {
		v = edge[i].to;
		if (vis[v]) continue;
		Dfs(v);
	}
}
void Solve() {
	memset(vis, 0, sizeof vis);
	memset(head, 0, sizeof head);
	sub = 0;
	int i, j;
	Read(N); Read(H); Read(R);
	for (i = 1; i <= N; i++) {
		Read(x[i]); Read(y[i]); Read(z[i]);
	}
	for (i = 1; i <= N; i++) {
		if (0 <= z[i] + R && 0 >= z[i] - R) {
			Add(0, i); Add(i, 0);
		}
		if (H <= z[i] + R && H >= z[i] - R) {
			Add(N + 1, i); Add(i, N + 1);
		}
	}
	for (i = 1; i <= N; i++)
		for (j = 1; j <= N; j++)
			if (i != j && Dist(i, j) <= 4LL * R * R) {
				Add(i, j); Add(j, i);
			}
	Dfs(0);
	if (vis[N + 1])
		printf("Yes\n");
	else
		printf("No\n");
}
int main(void) {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	Read(T); while (T--) Solve();
	fclose(stdin); fclose(stdout);
	return 0;
}

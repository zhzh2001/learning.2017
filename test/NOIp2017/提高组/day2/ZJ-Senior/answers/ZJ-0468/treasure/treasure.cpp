#include <cstdio>
#include <cstring>
#include <iostream>
#include <queue>
#include <algorithm>
using namespace std;
const int V = 15;
const int L = 100000;
int Min(int a, int b) {
	return a < b ? a : b;
}
void QMin(int &a, int b) {
	a = Min(a, b);
}
char get(void) {
	static char buf[L], *p1 = buf, *p2 = buf;
	if (p1 == p2) {
		p2 = (p1 = buf) + fread(buf, 1, L, stdin);
		if (p1 == p2) return EOF;		
	}
	return *p1++;
}
void Read(int &x) {
	x = 0; static char c;
	while (c > '9' || c < '0') c = get();
	while (c >= '0' && c <= '9') {
		x = x * 10 + c - '0';
		c = get();
	}
}
int INF;
int N, M, W;
int m[V][V], f[1 << V][V][V], bin[1 << V];
int vis[V], Now, Ans;
int main(void) {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	Read(N); Read(M);
	memset(f, 0x3f, sizeof f);
	memset(m, 0x3f, sizeof m); INF = m[0][0];
	int S, i, j, k, u, v, w;
	int flg = 1;
	for (i = 1; i <= M; i++) {
		Read(u); Read(v); Read(w);
		m[u][v] = m[v][u] = Min(m[u][v], w);
		if (!W) W = w;
		flg &= (W == w);
	}
	
	if (flg) {
		Ans = INF;
		for (i = 1; i <= N; i++) {
			memset(vis, 0, sizeof vis);
			queue<int> qu; qu.push(i);
			Now = 0; vis[i] = 1;
			while (!qu.empty()) {
				int u = qu.front(); qu.pop();
				for (int v = 1; v <= N; v++)
					if (!vis[v] && m[u][v] != INF) {
						Now += vis[u] * W;
						qu.push(v); vis[v] = vis[u] + 1;
					}
			}
			Ans = Min(Ans, Now);
		}
		printf("%d\n", Ans);
		fclose(stdin); fclose(stdout);
		return 0;
	}
	
	bin[0] = 1; for (i = 1; i <= N; i++) bin[i] = bin[i - 1] << 1;
	int Ans = INF;
	for (v = 1; v <= N; v++) {
			memset(f, 0x3f, sizeof f);
			f[bin[v - 1]][v][0] = 0;
			for (S = 1; S < bin[N]; S++)
				for (i = 1; i <= N; i++) if (S & bin[i - 1])
					for (j = 0; j <= N; j++) if (f[S][i][j] != INF) {
						for (k = 1; k <= N; k++) if (m[i][k] != INF && ((S & bin[k - 1]) == 0)) {
								QMin(f[S | bin[k - 1]][k][j + 1], f[S][i][j] + m[i][k] * (j + 1));
								for (u = 1; u <= N; u++) if (S & bin[u - 1])
										for (w = 0; w <= N; w++) {
											if (f[S][u][w] >= INF) continue;
											QMin(f[S | bin[k - 1]][u][w], f[S][i][j] + m[i][k] * (j + 1));
										}
							}
					}
			for (i = 1; i <= N; i++)
				for (j = 0; j <= N; j++)
					QMin(Ans, f[bin[N] - 1][i][j]);
	}
	printf("%d\n", Ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

#include <cstdio>
#include <cstring>
using namespace std;
const int V = 10;
const int L = 100000;
int Min(int a, int b) {
	return a < b ? a : b;
}
int Max(int a, int b) {
	return a > b ? a : b;
}
int QMin(int &a, int b) {
	a = Min(a, b);
}
char get(void) {
	static char buf[L], *p1 = buf, *p2 = buf;
	if (p1 == p2) {
		p2 = (p1 = buf) + fread(buf, 1, L, stdin);
		if (p1 == p2) return EOF;		
	}
	return *p1++;
}
void Read(int &x) {
	x = 0; static char c;
	while (c > '9' || c < '0') c = get();
	while (c >= '0' && c <= '9') {
		x = x * 10 + c - '0';
		c = get();
	}
}
int INF;
int N, M, fa[V], Ans;
int m[V][V], vis[V], c[V][V];
int main(void) {
	freopen("treasure.in", "r", stdin);
	//freopen("treasure.out", "w", stdout);
	Read(N); Read(M);
	memset(m, 0x3f, sizeof m); Ans = INF = m[0][0];
	int S, i, j, k, u, v, w;
	for (i = 1; i <= M; i++) {
		Read(u); Read(v); Read(w);
		m[u][v] = m[v][u] = Min(m[u][v], w);
	}
	Dfs(1);
	printf("%d\n", Ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

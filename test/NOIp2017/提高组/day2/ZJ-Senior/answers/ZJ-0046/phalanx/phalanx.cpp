#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
using namespace std;

vector<int> vec[300010];
int ans[300010], n, m, q, num;
int f[300010], flag[300010];
pair<int, int> s[1010][1010];

int getnum(int x, int y) {
	return (x - 1) * m + y;
}

int main() {
	
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	
	scanf("%d%d%d", &n, &m, &q);
	if (n <= 1000 && m <= 1000) {
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= m; ++j)
				s[i][j] = mp(i, j);
		for (int i = 1, x, y; i <= q; ++i) {
			scanf("%d%d", &x, &y);
			pair<int, int> f = s[x][y];
			for (int j = y + 1; j <= m; ++j) s[x][j - 1] = s[x][j];
			for (int j = x + 1; j <= n; ++j) s[j - 1][m] = s[j][m];
			s[n][m] = f;
			printf("%d\n", getnum(f.ft, f.sc));
		}
	}
	else {
		num = n;
		for (int i = 1; i <= n; ++i) f[i] = m * i;
		for (int i = 1, x, y; i <= q; ++i) {
			int now = 0;
			scanf("%d%d", &x, &y);
			for (int j = 0; j < (int)vec[x].size(); ++j)
				if (vec[x][j] <= y) now ++;
			vec[x].push_back(y);
			now += y;
			if (now < m) {
				ans[i] = getnum(x, now), f[++num] = ans[i];
				int sum = 0;
				for (int j = 1; j <= num; ++j) {
					if (!flag[j]) sum ++;
					if (sum == n) {
						flag[j] = x;
						break;
					}
				}	
			}
			else {
				int sum = 0;
				for (int j = 1; j <= num; ++j) {
					if (!flag[j] || flag[j] == x) sum ++;
					if (sum == now - y + x) {
						ans[i] = f[j];
						if (flag[j] != x) {
							int ss = 0;
							for (int k = 1; k <= num; ++k) {
								if (!flag[k]) ss ++;
								if (ss == n) {
									flag[k] = x;
									break;
								}
							}
						}
						flag[j] = x;
						f[++num] = f[j];
						break;
					}
				}
			}
		}
		for (int i = 1; i <= q; ++i)
			printf("%d\n", ans[i]);
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}

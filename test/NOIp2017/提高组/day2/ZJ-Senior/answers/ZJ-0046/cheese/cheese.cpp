#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>
#include <fstream>
using namespace std;

bool flag[1010];
int n, h, r, q[1010];

struct xint {
	int x, y, z;
	bool operator<(const xint &x)const {
		return z < x.z;
	}
}w[1010];

bool Check(int a, int b) {
	long long temp1 = 1ll * (w[a].x - w[b].x) * (w[a].x - w[b].x) + 1ll * (w[a].y - w[b].y) * (w[a].y - w[b].y) + 1ll * (w[a].z - w[b].z) * (w[a].z - w[b].z);
	long long temp2 = 1ll * 4 * r * r;
	if (temp1 <= temp2) return 1;
	return 0;
}

int main() {
	
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	
	int T; scanf("%d", &T);
	while (T --) {
		scanf("%d%d%d", &n, &h, &r);
		for (int i = 1; i <= n; ++i) scanf("%d%d%d", &w[i].x, &w[i].y, &w[i].z);
		memset(flag, 0, sizeof flag);
		int L = 1, R = 0, ans = 0;
		for (int i = 1; i <= n; ++i) 
			if (abs(w[i].z) <= r) flag[i] = 1, q[++R] = i;
		while (L <= R) {
			for (int i = 1; i <= n; ++i)
				if (flag[i]) continue;
				else if (Check(q[L], i)) flag[i] = 1, q[++R] = i;
			++ L;
		}
		for (int i = 1; i <= n; ++i) 
			if (flag[i] && abs(h - w[i].z) <= r) {
				ans = 1;
				break;
			}
		if (ans) printf("Yes\n");
		else printf("No\n");
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}


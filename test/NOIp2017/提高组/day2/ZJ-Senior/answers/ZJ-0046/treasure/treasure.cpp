#include <bits/stdc++.h>
#define mp make_pair
#define ft first
#define sc second
using namespace std;
priority_queue < pair< long long, pair< int, int > >, vector< pair< long long, pair< int, int > > >, greater< pair< long long, pair< int, int > > > > q;
int fst[2010], nxt[2010], lst[2010], des[2010];
bool flag[2010];
int deep[2010], val[2010], cnt = 0, s[13][13], fa[2010], tou[2010];
int n, m;
long long Ans = 1e18;

void add(int u, int v, int vv) {
	if (!fst[u]) fst[u] = ++cnt;
	else nxt[lst[u]] = ++cnt;
	lst[u] = cnt, des[cnt] = v, val[cnt] = vv;
}

inline int fly(int x) {
	if (tou[x] != x) tou[x] = fly(tou[x]);
	return tou[x];
}
	
inline long long check(int now) {
	
	for (int i = 1; i <= n; ++i) tou[i] = i;
	for (int i = 1; i <= n; ++i) 
		if (fa[i] != 0) {
			int fa1 = fly(tou[i]), fa2 = fly(fa[tou[i]]);
			if (fa1 != fa2) tou[tou[fa1]] = tou[fa2];
		}
	fly(1);
	for (int i = 2; i <= n; ++i)
		if (fly(i) != tou[i - 1]) return (1e18);
	for (int i = 1; i <= n; ++i) deep[i] = -1;	
	deep[now] = 0;
	int sum = 1, de = 1;
	long long ans = 0;
	while (sum < n) {
		for (int i = 1; i <= n; ++i)
			if (deep[i] == -1 && deep[fa[i]] == de - 1)
				deep[i] = de, sum ++;
		de ++;
	}
	for (int i = 1; i <= n; ++i)
	 	if (i != now) {
	 		if (s[i][fa[i]] == 0) return (1e18);
	 		ans += 1ll * s[i][fa[i]] * deep[i];
	 	}
	return ans;
	
}	

inline void dfs(int x, int now) {
	if (x == n + 1) {
		Ans = min(Ans, check(now));
		return ;
	}
	if (x == now) fa[x] = 0, dfs(x + 1, now);
	else for (int i = 1; i <= n; ++i) 
		if (i != x)
			fa[x] = i, dfs(x + 1, now);
}

int main() {
	
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	
	scanf("%d%d", &n, &m);
	
	if (n <= 7) {
		for (int i = 1, u, v, vv; i <= m; ++i)
			scanf("%d%d%d", &u, &v, &vv), s[u][v] = s[v][u] = (s[u][v] == 0)?vv:min(vv, s[u][v]);
		for (int i = 1; i <= n; ++i)
			dfs(1, i);
		printf("%lld\n", Ans);
	}
	else {
		for (int i = 1, u, v, vv; i <= m; ++i)
			scanf("%d%d%d", &u, &v, &vv), add(v, u, vv), add(u, v, vv);
		for (int i = 1; i <= n; ++i) {
			for (int limit = 2; limit <= n; ++limit) {
				while (!q.empty()) q.pop();		
				memset(flag, 0, sizeof flag);
				memset(deep, 0, sizeof deep);
				deep[i] = 1;
				int sum = 1;
				long long ans = 0;
				for (int j = fst[i]; j; j = nxt[j])
					q.push(mp(1ll * val[j], mp(j, i))), flag[j] = 1;
				while (sum < n && !q.empty()) {
					pair< long long, pair<int, int> > x = q.top(); q.pop();
					if (deep[des[x.sc.ft]] == 0 && deep[x.sc.sc] + 1 <= limit) {
						deep[des[x.sc.ft]] = deep[x.sc.sc] + 1;
						ans += x.ft;
						for (int j = fst[des[x.sc.ft]]; j; j = nxt[j])
							if (!flag[j])
								q.push(mp(1ll * val[j] * deep[des[x.sc.ft]], mp(j, des[x.sc.ft]))), flag[j] = 1;
						sum ++;
						//cerr << des[x.sc.ft] << " !" << ans << " ";
					}
				}
				if (sum == n) Ans = min(Ans, ans);
			}
		}
		printf("%lld\n", Ans);
	}
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
	
}

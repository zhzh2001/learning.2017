#include<cmath>
#include<queue>
#include<cstdio>
#include<cctype>
#include<vector>
#include<cstring>
#include<algorithm>
inline int getint() {
	register char ch;
	register bool neg=false;
	while(!isdigit(ch=getchar())) if(ch=='-') neg=true;
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return neg?-x:x;
}
const int N=1002;
int n,h,r;
inline double sqr(const double &x) {
	return x*x;
}
struct Point {
	int x,y,z;
	friend double dist(const Point &a,const Point &b) {
		return sqrt(sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z));
	}
	friend bool cross(const Point &a,const Point &b) {
		return dist(a,b)<=r*2.0;
	}
};
Point p[N];
std::vector<int> e[N];
inline void add_edge(const int &u,const int &v) {
	e[u].push_back(v);
	e[v].push_back(u);
}
std::queue<int> q;
bool vis[N];
inline bool check() {
	while(!q.empty()) q.pop();
	memset(vis,0,sizeof vis);
	q.push(0);
	vis[0]=true;
	while(!q.empty()) {
		const int x=q.front();
		q.pop();
		for(register unsigned i=0;i<e[x].size();i++) {
			const int &y=e[x][i];
			if(!vis[y]) {
				if(y==n+1) return true;
				q.push(y);
				vis[y]=true;
			}
		}
	}
	return false;
}
void init() {
	for(register int i=0;i<N;i++) {
		e[i].clear();
	}
}
int main() {
	freopen("cheese.in","r+",stdin);
	freopen("cheese.out","w+",stdout);
	for(register int T=getint();T;T--) {
		n=getint(),h=getint(),r=getint();
		for(register int i=1;i<=n;i++) {
			p[i].x=getint(),p[i].y=getint(),p[i].z=getint();
		}
		init();
		for(register int i=1;i<=n;i++) {
			for(register int j=i+1;j<=n;j++) {
				if(cross(p[i],p[j])) {
					add_edge(i,j);
				}
			}
		}
		for(register int i=1;i<=n;i++) {
			if(p[i].z<=r) {
				add_edge(0,i);
			}
		}
		for(register int i=1;i<=n;i++) {
			if(std::abs(h-p[i].z)<=r) {
				add_edge(n+1,i);
			}
		}
		puts(check()?"Yes":"No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

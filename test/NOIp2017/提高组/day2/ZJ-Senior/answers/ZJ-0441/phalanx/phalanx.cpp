#include<cstdio>
#include<cctype>
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int N=1001,M=1001;
int a[N][M],b[M],n,m,q;
int main() {
	freopen("phalanx.in","r+",stdin);
	freopen("phalanx.out","w+",stdout);
	n=getint(),m=getint(),q=getint();
	/*if(n==1) {
		for(register int i=1;i<=m;i++) b[i]=i;
		for(register int i=1;i<=q;i++) {
			getint();int y=getint(),tmp=0,last=0;
			while(tmp=t.query(y+tmp)) last=t.query(y+tmp);
			printf("%d\n",b[m+i]=b[y]);
			t.modify(y);
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}*/
	for(register int i=1;i<=n;i++) {
		for(register int j=1;j<=m;j++) {
			a[i][j]=(i-1)*m+j;
		}
	}
	for(register int i=0;i<q;i++) {
		const int x=getint(),y=getint(),tmp=a[x][y];
		printf("%d\n",tmp);
		for(register int i=y;i<m;i++) {
			a[x][i]=a[x][i+1];
		}
		for(register int i=x;i<n;i++) {
			a[i][m]=a[i+1][m];
		}
		a[n][m]=tmp;
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cctype>
#include<algorithm>
typedef long long int64;
inline int getint() {
	register char ch;
	while(!isdigit(ch=getchar()));
	register int x=ch^'0';
	while(isdigit(ch=getchar())) x=(((x<<2)+x)<<1)+(ch^'0');
	return x;
}
const int64 inf=0x7fffffffffffffffll;
const int N=12;
int dis[N][N],n,m,root;
int64 f[N][1<<N];
inline void dfs(const int &x,const int &sit,const int &d,const int64 &sum) {
	f[root][sit]=std::min(f[root][sit],sum);
	for(int y=0;y<n;y++) {
		if(dis[x][y]==0x7fffffff) continue;
		if(sit&(1<<y)) continue;
		dfs(y,sit|(1<<y),d+1,sum+d*dis[x][y]);
	}
}
void calc() {
	for(register int i=1;i<(1<<n);i++) {
		f[root][i]=inf;
	}
	dfs(root,1<<root,1,0);
}
int main() {
	freopen("treasure.in","r+",stdin);
	freopen("treasure.out","w+",stdout);
	n=getint(),m=getint();
	for(register int i=0;i<n;i++) {
		for(register int j=0;j<n;j++) {
			if(i==j) continue;
			dis[i][j]=0x7fffffff;
		}
	}
	for(register int i=0;i<m;i++) {
		const int u=getint()-1,v=getint()-1,w=getint();
		dis[u][v]=dis[v][u]=std::min(dis[u][v],w);
	}
	int64 ans=inf;
	for(root=0;root<n;root++) {
		calc();
		for(register int i=1;i<(1<<n);i++) {
			if(f[root][i]==inf||!(i&(1<<root))) continue;
			for(register int j=i+1;j<(1<<n);j++) {
				if(f[root][j]==inf||!(j&(1<<root))) continue;
				if(f[root][i&j]==inf) continue;
				f[root][i|j]=std::min(f[root][i|j],f[root][i]+f[root][j]-f[root][i&j]);
			}
		}
		ans=std::min(ans,f[root][(1<<n)-1]);
	}
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

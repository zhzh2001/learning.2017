#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define rep(j,k,l) for (LL j=k;j<=l;++j)
#define red(j,k,l) for (LL j=k;j>=l;--j)
#define N 1008
#define LL long long
#define sqr(a) ((a)*(a))

using namespace std;
LL T,n,h,r,a[N],b[N],c[N],vis[N],f[N][N],que[N];

double dis(LL k,LL l){
	
	return sqrt((sqr(a[k]-a[l])+sqr(b[k]-b[l])+sqr(c[k]-c[l]))*1.0);
	
}

void Bfs(){
	
	rep(i,1,n+1) vis[i]=0;vis[0]=1;
	LL hd=0,tl=1;que[1]=0;
	while (hd<tl){
		
		LL x=que[++hd];
		rep(i,1,n+1) if (f[x][i]&&vis[i]==0){
			
			que[++tl]=i;
			vis[i]=1;
			
		}
		
	}
	printf((vis[n+1])?("Yes\n"):("No\n"));
	
}

int main(){
	
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	while (T--){
		
		scanf("%lld%lld%lld",&n,&h,&r);
		rep(i,0,n+1) rep(j,0,n+1) f[i][j]=0;
		rep(i,1,n){
			
			scanf("%lld%lld%lld",&a[i],&b[i],&c[i]);
			if (c[i]<=r) f[0][i]=f[i][0]=1;
			if (c[i]+r>=h) f[n+1][i]=f[i][n+1]=1;
			
		}
		rep(i,1,n-1) rep(j,i+1,n) if (dis(i,j)<=2.0*r) f[i][j]=f[j][i]=1;
		Bfs();
		
	}
	
}

#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define rep(j,k,l) for (int j=k;j<=l;++j)
#define red(j,k,l) for (int j=k;j>=l;--j)
#define N 600010
#define LL long long

using namespace std;
struct _233{ int sm,fg1,fg2,L,R; } tr[15000000];
struct _Yzz{ int sm,fg1,fg2; } T[N*8];
int TT,TTT,n,m,cnt,rt[N];

LL gt(int k,int l,int r,int poi){
	
	if (k==0) return 0;
	if (l==r) return tr[k].fg1*1LL*m+1LL*tr[k].fg2;
	LL ppp=tr[k].fg1*1LL*m+1LL*tr[k].fg2;int mid=l+r>>1;
	int p=tr[tr[k].L].sm;if (tr[k].L==0) p=mid-l+1;
	if (p>=poi) return ppp+gt(tr[k].L,l,mid,poi);
	return ppp+gt(tr[k].R,mid+1,r,poi-p);
	
}

void del(int &k,int l,int r,int poi){
	
	//printf("---%d %d %d\n",l,r,poi);
	if (k==0){ k=++cnt; tr[k].sm=r-l+1; }
	if (l==r){ tr[k].sm--; return; }
	int mid=l+r>>1,p=tr[tr[k].L].sm;if (tr[k].L==0) p=mid-l+1;
	if (p>=poi) del(tr[k].L,l,mid,poi);else del(tr[k].R,mid+1,r,poi-p);
	tr[k].sm--;
	return;
	
}

void Del(int k,int l,int r,int poi){
	
	if (l==r){ T[k].sm--; return; }
	int mid=l+r>>1,p=T[k<<1].sm;
	if (p>=poi) Del(k<<1,l,mid,poi);else Del(k<<1|1,mid+1,r,poi-p);
	T[k].sm--;
	
}

void Gt(int k,int l,int r,int poi,int lop){
	
	//printf("-----%d %d %d\n",l,r,poi);
	if (l==r){ tr[lop].fg1=T[k].fg1; tr[lop].fg2=T[k].fg2; return; }
	int mid=l+r>>1,p=T[k<<1].sm;
	if (p>=poi) Gt(k<<1,l,mid,poi,lop);
	else Gt(k<<1|1,mid+1,r,poi-p,lop);
	
}

void add(int &k,int l,int r,int o,int pl){
	
	if (k==0){ k=++cnt; tr[k].sm=r-l+1; }
	if (l==r){ Gt(1,1,n+TT,pl,k); return; }
	int mid=l+r>>1,pp=tr[tr[k].L].sm;if (tr[k].L==0) pp=mid-l+1;
	if (pp>=o) add(tr[k].L,l,mid,o,pl);else add(tr[k].R,mid+1,r,o-pp,pl);
	
}

void ic(int &k,int l,int r,int o,int p){
	
	if (k==0){ k=++cnt; tr[k].sm=r-l+1; }
	if (p<1||o>tr[k].sm) return;
	if (o<=1&&tr[k].sm<=p){ tr[k].fg2++; return; }
	int mid=l+r>>1,ppp=tr[tr[k].L].sm;if (tr[k].L==0) ppp=mid-l+1;
	/*if (p-ppp>0) */ic(tr[k].R,mid+1,r,o-ppp,p-ppp);
	/*if (ppp>=o) */ic(tr[k].L,l,mid,o,p);
	
}

void Ic(int k,int l,int r,int o,int p){
	
	if (p<1||o>T[k].sm) return;
	if (o<=1&&T[k].sm<=p){ T[k].fg1++; return; }
	int mid=l+r>>1,ppp=T[k<<1].sm;
	/*if (p-ppp>0) */Ic(k<<1|1,mid+1,r,o-ppp,p-ppp);
	/*if (ppp>=o) */Ic(k<<1,l,mid,o,p);
	
}

void stree(int k,int l,int r){
	
	T[k].sm=r-l+1;
	if (l==r) return;
	int mid=l+r>>1;
	stree(k<<1,l,mid);
	stree(k<<1|1,mid+1,r);
	
}

void Add(int k,int l,int r,int o,LL poi){
	
	if (l==r){
		
		T[k].fg1=((int)(poi/m))-n+1;
		T[k].fg2=((int)(poi%m))-m;
		if (T[k].fg2==-m) T[k].fg2=0,T[k].fg1--;
		//printf("%lld %d %d\n",poi,T[k].fg1,T[k].fg2);
		return;
		
	}
	int mid=l+r>>1,p=T[k<<1].sm;
	if (p>=o) Add(k<<1,l,mid,o,poi);else Add(k<<1|1,mid+1,r,o-p,poi);
	
}

LL GT(int k,int l,int r,int poi){
	
	if (l==r) return 1LL*T[k].fg1*m+1LL*T[k].fg2;
	int mid=l+r>>1,p=T[k<<1].sm;
	if (p>=poi) return GT(k<<1,l,mid,poi)+1LL*T[k].fg1*m+1LL*T[k].fg2;
	return 1LL*T[k].fg1*m+1LL*T[k].fg2+GT(k<<1|1,mid+1,r,poi-p);
	
}

int main(){
	
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&TTT);TT=TTT*2;
	stree(1,1,n+TT);
	rep(i,1,n) rt[i]=i,tr[i].sm=m+TT;cnt=n;
	while (TTT--){
		
		int k,l;scanf("%d%d",&k,&l);
		if (l<m&&k<n){
			
			LL poi=(k-1)*m+l+gt(rt[k],1,m+TT,l);
			printf("%lld\n",poi);
			del(rt[k],1,m+TT,l);
			add(rt[k],1,m+TT,m-1,k);
			ic(rt[k],1,m+TT,l,m-1);
			Del(1,1,n+TT,k);
			Ic(1,1,n+TT,k,n-1);
			Add(1,1,n+TT,n,poi);
			/*rep(i,1,n){
				
				printf("---");
				rep(j,1,m-1) printf("%lld ",(i-1)*m+j+gt(rt[i],1,m+TT,j));
				printf("%lld\n",i*m+GT(1,1,n+TT,i));
				
			}
			printf("%lld\n",2+GT(1,1,n+TT,1));*/
			
		}else if (k<n&&l==m){
			
			LL poi=k*m+GT(1,1,n+TT,k);
			printf("%lld\n",poi);
			Del(1,1,n+TT,k);
			Ic(1,1,n+TT,k,n-1);
			Add(1,1,n+TT,n,poi);
			/*rep(i,1,n){
				
				printf("---");
				rep(j,1,m-1) printf("%lld ",(i-1)*m+j+gt(rt[i],1,m+TT,j));
				printf("%lld\n",i*m+GT(1,1,n+TT,i));
				
			}
			printf("%lld\n",2+GT(1,1,n+TT,1));*/
			
		}else if (k==n&&l<m){
			
			LL poi=(k-1)*m+l+gt(rt[k],1,m+TT,l);
			printf("%lld\n",poi);
			del(rt[k],1,m+TT,l);
			add(rt[k],1,m+TT,m-1,k);
			ic(rt[k],1,m+TT,l,m-1);
			Del(1,1,n+TT,k);
			Add(1,1,n+TT,n,poi);
			/*rep(i,1,n){
				
				printf("---");
				rep(j,1,m-1) printf("%lld ",(i-1)*m+j+gt(rt[i],1,m+TT,j));
				printf("%lld\n",i*m+GT(1,1,n+TT,i));
				
			}*/
			
		}else{
			
			LL poi=n*m+GT(1,1,n+TT,n);
			printf("%lld\n",poi);
			
		}
		
	}
	
}

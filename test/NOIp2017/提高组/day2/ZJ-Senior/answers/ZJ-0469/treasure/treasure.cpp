#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define rep(j,k,l) for (int j=k;j<=l;++j)
#define red(j,k,l) for (int j=k;j>=l;--j)
#define N 15
#define inf 1000000007

using namespace std;
int n,m,f[N][N],a[N],ans;

void dfs(int k,int sm){
	
	if (sm>ans) return;
	if (k==n){ ans=sm; return; }
	rep(i,1,n) rep(j,1,n) if (a[i]>0&&a[j]==0&&f[i][j]){
		
		a[j]=a[i]+1;
		dfs(k+1,sm+f[i][j]*a[i]);
		a[j]=0;
		
	}
	
}

int main(){
	
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","W",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,m){
		
		int k,l,p;
		scanf("%d%d%d",&k,&l,&p);
		if (f[k][l]==0) f[k][l]=f[l][k]=p;
		else f[k][l]=f[l][k]=min(p,f[k][l]);
		
	}
	ans=inf;
	rep(i,1,n) a[i]=1,dfs(1,0),a[i]=0;
	printf("%d\n",ans);
	
}

#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
#define N 1010
LL h,r;
int n,t,tot,fi[N];
bool gao,di,dian[N],ans;
struct jh
{
	LL x,y,z;
}d[N];

bool jiao(int m,int n)
{
	double l=sqrt((d[m].x-d[n].x)^2+(d[m].y-d[n].y)^2+(d[m].z-d[n].z)^2);
	return(l<=2*r);
}
void dfs(int p)
{
	if(d[p].z<=r)ans=true;
	dian[p]=true;
	for(int k=p+1;k<=n;++k)
	{
		if(jiao(p,k))
		{
			if(dian[k]==false)
			{
				dfs(k);
			}
		}
	}
}
int main()
{
	freopen("cheese.in","r",stdin);   freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	
	for(int i=1;i<=t;++i)
	{
		gao=true;  di=true;  ans=false; tot=0;
		memset(dian,sizeof(dian),false);
		scanf("%d%d%d",&n,&h,&r);
		for(int j=1;j<=n;++j)
		{
			scanf("%d%d%d",&d[j].x,&d[j].y,&d[j].z);
			if(abs(d[j].z-h)<=r)
			{gao=false;  fi[++tot]=j;}
			if(abs(d[j].z)<=r)
			di=false;
		}
		//sort(d+1,d+n+1,cmp1);
		for(int j=1;j<=tot;++j)dfs(fi[j]);
		if(gao || di)
		{
			printf("No\n");
		}
		else
		{
			if(ans)
				printf("Yes\n");
			else
				printf("No\n");
		}
	}
	return 0;
}

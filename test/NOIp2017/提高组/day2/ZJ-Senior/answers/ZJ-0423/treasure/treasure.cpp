#include <cstdio>
#include <cstring>
#include <algorithm>
#define FOR(a,b,c) for(int a=b;a<=c;a++)
#define FORD(a,b,c) for(int a=b;a>=c;a--)
using namespace std;
int n, m, x, y, z, mx, tmp, ans, cnt;
int dis[13][13], dep[13], de[13];
int f[50000010];
inline int code(){
	int ret = 0;
	FORD(i, n, 1) ret = ret * (n + 1) + dep[i];
}
inline void decode(int x){
	cnt = 0;
	FOR(i, 1, n){
		de[i] = x % (n + 1);
		if (de[i]) cnt++;
		x /= (n + 1);
	}
}
int main(){
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	FOR(i, 1, n) FOR(j, 1, n) dis[i][j] = 1e7;
	FOR(i, 1, m){
		scanf("%d%d%d", &x, &y, &z);
		dis[y][x] = dis[x][y] = min(dis[x][y], z);
	}
	ans = 2e7;
	FOR(i, 1, n) dep[i] = n;
	mx = code();
	memset(f, 0x3f, sizeof(f));
	FOR(i, 1, n) dep[i] = 0;
	FOR(i, 1, n){
		dep[i] = 1;
		f[code()] = 0;
//		printf("f["); FOR(j, 1, n) printf("%d ", dep[j]); printf("(%d)]=%d\n", code(), f[code()]);
		dep[i] = 0;
	}
	FOR(i, 1, mx) if (f[i] < ans){
		decode(i);
//		FOR(j, 1, n) printf("%d ", de[j]); printf("\n");
		FOR(j, 1, n) if (de[j] > 0) FOR(k, 1, n) if (dis[j][k] < 1e7 && de[k] == 0){
			FOR(l, 1, n) dep[l] = de[l];
			dep[k] = dep[j] + 1;
			tmp = code();
			f[tmp] = min(f[tmp], f[i] + dis[j][k] * de[j]);
			if (cnt == n - 1) ans = min(ans, f[tmp]);
//			printf("f["); FOR(l, 1, n) printf("%d ", dep[l]); printf("(%d)]=%d\n", tmp, f[tmp]);
		}
	}
	printf("%d\n", ans);
	return 0;
}

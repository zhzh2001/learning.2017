#include <queue>
#include <cstdio>
#define FOR(a,b,c) for(int a=b;a<=c;a++)
using namespace std;
int Enum, F[10010], N[3000010], V[3000010];
inline void addedge(int x, int y){
	V[++Enum] = y;
	N[Enum] = F[x];
	F[x] = Enum;
}
int Case, n, S, T;
long long h, r, x[10010], y[10010], z[10010];
bool bl[10010];
inline bool judge(int a, int b){
	if (r * r * 4LL - (x[a] - x[b]) * (x[a] - x[b]) >= (y[a] - y[b]) * (y[a] - y[b]) + (z[a] - z[b]) * (z[a] - z[b])) return 1;
	return 0;
}
queue<int> q;
int main(){
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &Case);
	while (Case--){
		scanf("%d%d%d", &n, &h, &r);
		S = n + 1, T = n + 2;
		FOR(i, 1, n){
			scanf("%lld%lld%lld", &x[i], &y[i], &z[i]);
			if (z[i] <= r) addedge(S, i);
			if (h - z[i] <= r) addedge(i, T);
		}
		FOR(i, 1, n - 1) FOR(j, i + 1, n) if (judge(i, j)){
			addedge(i, j);
			addedge(j, i);
		}
		FOR(i, 1, T) bl[i] = 0;
		q.push(S);
		bl[S] = 1;
		while (!q.empty()){
			int h = q.front();
			q.pop();
			for (int now = F[h]; now; now = N[now]) if (!bl[V[now]]){
				q.push(V[now]);
				bl[V[now]] = 1;
			}
		}
		if (bl[T]) printf("Yes\n");
		else printf("No\n");
		FOR(i, 1, T) F[i] = 0;
		Enum = 0;
	}
	return 0;
}

#include <cstdio>
#define FOR(a,b,c) for(int a=b;a<=c;a++)
using namespace std;
int n, m, q, X[300010], Y[300010];
namespace BF{
	int x, y, a[4010][4010];
	void work(){
		FOR(i, 1, n) FOR(j, 1, m) a[i][j] = (i - 1) * m + j;
		FOR(i, 1, q){
			x = X[i], y = Y[i];
			int t = a[x][y];
			printf("%d\n", t);
			FOR(j, y + 1, m) a[x][j - 1] = a[x][j];
			FOR(j, x + 1, n) a[j - 1][m] = a[j][m];
			a[n][m] = t;
		}
	}
}
namespace CaseI{
	int x, y, cnt, key;
	int a[510][50010], b[50010];
	void work(){
		cnt = 0;
		FOR(i, 1, n) b[i] = i * m;
		FOR(i, 1, q){
			x = X[i], y = Y[i];
			if (y == m){
				int t = b[x];
				printf("%d\n", t);
				FOR(j, x + 1, n) b[j - 1] = b[j];
				b[n] = t;
				continue;
			}
			bool flag = 0;
			FOR(j, 1, cnt) if (a[j][0] == x){
				flag = 1;
				key = j;
				break;
			}
			if (!flag){
				a[++cnt][0] = x;
				FOR(j, 1, m - 1) a[cnt][j] = (x - 1) * m + j;
				key = cnt;
			}
			int t = a[key][y];
			printf("%d\n", t);
			FOR(j, y + 1, m - 1) a[key][j - 1] = a[key][j];
			a[key][m - 1] = b[x];
			FOR(j, x + 1, n) b[j - 1] = b[j];
			b[n] = t;
		}
	}
}
namespace CaseII{
	int x, y;
	int a[1000010];
	int c[1000010];
	inline void add(int x, int y){
		while (x <= 1000000){
			c[x] += y;
			x += (x & (-x));
		}
	}
	inline int calc(int x){
		if (x == 0) return 0;
		int ret = 0;
		while (x){
			ret += c[x];
			x -= (x & (-x));
		}
		return ret;
	}
	int L, R;
	void work(){
		FOR(i, 1, m) a[i] = i;
		FOR(i, 2, n) a[m + i - 1] = i * m;
		FOR(i, 1, n + m - 1) add(i, 1);
		L = 0, R = n + m - 1;
		FOR(i, 1, q){
			x = X[i], y = Y[i];
			int l = L, r = R, mid, ans = R;
			while (l <= r){
				mid = (l + r) >> 1;
				if (calc(mid) < y){
					ans = mid;
					l = mid + 1;
				}
				else r = mid - 1;
			}
			ans++;
			a[++R] = a[ans];
			add(ans, -1);
			add(R, 1);
			printf("%d\n", a[ans]);
		}	
	}
}
int main(){
	freopen("phalanx.in", "r", stdin);
	freopen("bl.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	bool flag = 1;
	FOR(i, 1, q){
		scanf("%d%d", &X[i], &Y[i]);
		if (X[i] != 1) flag = 0;
	}
	if (n <= 3000 && m <= 3000) BF::work();
	else if (flag) CaseII::work();
	else CaseI::work();
	return 0;
}

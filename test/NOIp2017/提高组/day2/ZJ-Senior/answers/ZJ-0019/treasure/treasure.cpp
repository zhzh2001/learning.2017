
#include<bits/stdc++.h>
using namespace std;
const int inf=707406378;
int a[15][15],_hash[15],mx=1e9,total,n,m,ll,rr,w;
long long f[15];
inline void dfs(int place,int kk){
	for(int i=1;i<=n;++i)
		if(i!=place && a[place][i]!=inf && _hash[i]==0){
			long long ss=kk*a[place][i];
			f[i]=min(f[i],ss);
		}
	for(int i=1;i<=n;++i)
		if(i!=place && a[ll][rr]!=inf && _hash[i]==0 && a[i][place]*kk==f[i]){
			_hash[i]=1;
			dfs(i,kk+1);
			_hash[i]=0;
		}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;++i)
		f[i]=i;
	memset(a,127/3,sizeof(a));
	for(int i=1;i<=m;++i){
		scanf("%d%d%d",&ll,&rr,&w);
		a[ll][rr]=min(a[ll][rr],w);
		a[rr][ll]=a[ll][rr];
	}
	for(int i=1;i<=n;++i){
		memset(f,127/3,sizeof(f));
		memset(_hash,0,sizeof(_hash));
		total=0;
		f[i]=0;
		_hash[i]=1;
		dfs(i,1);
		_hash[i]=0;
		for(int j=1;j<=n;++j)
			total+=f[j];
		mx=min(mx,total);
	}
	printf("%d\n",mx);
	return 0;
}

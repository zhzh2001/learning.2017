#include<bits/stdc++.h>
#include<vector>
#include<queue>
using namespace std;
int T,n,h,r,ss[1005],tt[1005],_hash[1005],flag;
struct node{
	int x,y,z;
}a[1005];
vector<int>f[1005];
queue<int>q;
inline double com(int i,int j){
	return sqrt((a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z));
}
inline int check(int x){
	for(int i=1;i<=tt[0];++i)
		if(tt[i]==x)
			return 1;
	return 0;
}
inline void dfs(){
	if(flag==1)
		return;
	int kk=q.front();
	if(check(kk)==1){
		printf("Yes\n");
		flag=1;
		return;
	}
	for(int i=0;i<f[kk].size();++i){
		if(_hash[f[kk][i]]==0){
			_hash[f[kk][i]]=1;
			q.push(f[kk][i]);
			q.pop();
			dfs();
			_hash[f[kk][i]]=0;
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T){
		--T;
		memset(ss,0,sizeof(ss));
		memset(tt,0,sizeof(tt));
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;++i)
			f[i].clear();
		for(int i=1;i<=n;++i){
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
			if(a[i].z<=r){
				++ss[0];
				ss[ss[0]]=i;
			}
			if(a[i].z+r>=h){
				++tt[0];
				tt[tt[0]]=i;
			}
			for(int j=1;j<i;++j)
				if(com(i,j)<=2*r){
					f[i].push_back(j);
					f[j].push_back(i);
				}
		}
		if(tt[0]==0 || ss[0]==0){
			printf("No\n");
			continue;
		}
		flag=0;
		memset(_hash,0,sizeof(_hash));
		for(int i=1;i<=ss[0];++i){
			while(!q.empty())
				q.pop();
			q.push(ss[i]);
			dfs();
		}
		if(flag==0)
			printf("No\n");
	}
	return 0;
}

#include<queue>
#include<cstdio>
#include<cstring>
#include<iostream>
#define N 14
#define INF 0x3f3f3f3f
using namespace std;
struct P{int x,y;int operator<(const P&t)const{return y>t.y;}};
int way[N][N],arr[N*N],val[N*N],nex[N*N],fir[N];
int a,b,c,i,j,flg,tmp=-1,n,m,e;
inline int slove1()
{
	priority_queue<P> Q;
	int dis[N],vis[N],i,minn=INF,sum;
	for(i=1;i<=n;++i)
	{
		sum=0;
		memset(vis,0,sizeof vis);
		memset(dis,INF,sizeof dis);
		Q.push((P){i,0});dis[i]=0;
		for(;!Q.empty();)
		{
			P t=Q.top();Q.pop();
			if(vis[t.x]) continue;
			vis[t.x]=1;
			for(int j=fir[t.x];j;j=nex[j])
				if(dis[arr[j]]>dis[t.x]+1)
					Q.push((P){arr[j],dis[arr[j]]=dis[t.x]+1});
			sum+=dis[t.x];
		}
		minn=min(minn,sum);
	}
	return minn*tmp;
}
inline int slove2()
{
	return 0;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(way,INF,sizeof way);
	scanf("%d%d",&n,&m);
	for(i=0;i<m;++i)
	{
		scanf("%d%d%d",&a,&b,&c);
		way[a][b]=way[b][a]=min(way[a][b],c);
	}
	for(i=1;i<=n;++i)
		for(j=1;j<=n;++j)
			if(way[i][j]!=INF)
			{
				arr[++e]=j;val[e]=way[i][j];nex[e]=fir[i];fir[i]=e;
			}
	tmp=val[1];for(i=2;i<=e&&!flg;++i) if(val[i]!=tmp) flg=1;
	printf("%d\n",flg?slove2():slove1());
	fclose(stdin);
	fclose(stdout);
	return 0;
}

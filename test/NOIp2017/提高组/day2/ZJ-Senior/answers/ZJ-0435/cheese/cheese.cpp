#include<cstdio>
#define N 1005
#define L long long
using namespace std;
L fa[N],n,h,r,x[N],y[N],z[N],i,T,j;
L find(L x){return fa[x]==x?x:fa[x]=find(fa[x]);}
inline L sqr(L x){return x*x;}
inline L dis(L i,L j){return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	for(int cas=1;cas<=T;++cas)
	{
		scanf("%lld%lld%lld",&n,&h,&r);
		for(i=0;i<n+2;++i) fa[i]=i;
		for(i=1;i<=n;++i)
			scanf("%lld%lld%lld",x+i,y+i,z+i);
		for(i=1;i<=n;++i)
		{
			for(j=i+1;j<=n;++j)
				if(dis(i,j)<=(r*r)<<2)
					fa[find(i)]=find(j);
			if(z[i]<=r) fa[find(0)]=find(i);
			if(z[i]+r>=h) fa[find(n+1)]=find(i);
		}
		puts(find(0)==find(n+1)?"Yes":"No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;

inline void read(int &x){
	x=0; static bool fff=0; static char c=getchar();
	for (;!(c<='9'&&c>='0'); c=getchar()) if (c=='-') fff=1;
	for (; (c<='9'&&c>='0'); c=getchar()) x=(x<<3)+(x<<1)+c-'0';
	if (fff) x=-x;
}

namespace seg{
	struct TR{
		int lc,rc,val;
	}tr[7200000];
	int rt[301000],sz,cnt;
	void RE(int sz_){
		sz=sz_;
		memset(rt,0,sizeof rt);
		cnt=0;
	}
	inline int nnd(){
		tr[++cnt]=(TR){0,0,0};
		return cnt;
	}
	
	int QA(int &x,int l,int r,int p){
		if (!x) x=nnd();
		++tr[x].val;
		if (l==r) return l;
		int mid=l+r>>1,t=mid-l+1-tr[tr[x].lc].val;
		if (p<=t) return QA(tr[x].lc,l,mid,p);
		else return QA(tr[x].rc,mid+1,r,p-t);
	}
	
	int queadd(int who,int p){
		return QA(rt[who],1,sz,p);
	}
}

#define ll long long
int n,m,T,x,y,deb; ll ans;

namespace has{
	ll c[11000000],mod=9999991,p,v,t[11000000];
	void RE(){
		memset(c,-1,sizeof c);
	}
	void add(int x,int y,ll val){
		v=(ll)x*100000000ll+y;
		p=v%mod;
		while (~c[p]&&c[p]!=v) ++p;
		c[p]=v; t[p]=val;
	}
	ll que(int x,int y){
		if (y&&y<m) return (ll)(x-1)*m+y;
		if (!y&&x<=n) return  (ll)x*m;
		v=(ll)x*100000000ll+y;
		p=v%mod;
		while (~c[p]&&c[p]!=v) ++p;
		return t[p];
	}
}

int main(){
	freopen("phalanx.in","r",stdin); freopen("phalanx.out","w",stdout);
	
	read(n); read(m); read(T);
	seg::RE(max(n,m)+T);
	has::RE();
	for (int an,am;T--;printf("%lld\n",ans)){
		read(x); read(y);
		if (y!=m){
			an=seg::queadd(x,y);
			ans=has::que(x,an);
			
			am=seg::queadd(0,x);
			has::add(x , seg::tr[seg::rt[x]].val+m-1 , has::que(am,0));
			has::add(seg::tr[seg::rt[0]].val+n , 0 , ans);
		}else{
			am=seg::queadd(0,x);
			ans=has::que(am,0);
			has::add(seg::tr[seg::rt[0]].val+n , 0 , ans);
		}
	}
	
	fclose(stdin); fclose(stdout); return 0;
}

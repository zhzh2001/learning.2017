#include<bits/stdc++.h>
#define ll long long
using namespace std;

int n,T;

struct P{
	ll x,y,z;
}p[1010];
ll h,rr;

struct R{
	int to,nex;
}r[2200000];
int hea[1010],cnt,st=1001,en=1002,used[1010];

void A(int u,int v){
	r[++cnt]=(R){v,hea[u]}; hea[u]=cnt;
	r[++cnt]=(R){u,hea[v]}; hea[v]=cnt;
}

bool ok(P p1,P p2){
	return rr*rr*4ll >= (p1.x-p2.x)*(p1.x-p2.x)+(p1.y-p2.y)*(p1.y-p2.y)+(p1.z-p2.z)*(p1.z-p2.z);
}

void dfs(int x){
	used[x]=1;
	int y;
	for (int i=hea[x];i>0;i=r[i].nex){
		y=r[i].to; if (used[y]) continue;
		dfs(y);
	}
}

int main(){
	freopen("cheese.in","r",stdin); freopen("cheese.out","w",stdout);
	
	for (scanf("%d",&T);T--;){
		scanf("%d%lld%lld",&n,&h,&rr);
		memset(hea,0,sizeof hea); cnt=0;
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&p[i].x,&p[i].y,&p[i].z);
			if (p[i].z<=rr) A(i,st);
			if (p[i].z+rr>=h) A(i,en);
		}
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if (ok(p[i],p[j])) A(i,j);
		memset(used,0,sizeof used);
		dfs(st);
		puts(used[en]? "Yes": "No");
	}
	
	fclose(stdin); fclose(stdout); return 0;
}

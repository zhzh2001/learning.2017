#include<bits/stdc++.h>
using namespace std;

int G[12][12],inf,f[13][4100],ans,n,g[12][4100],t;

inline void U(int &x,int y){
	x=min(x,y);
}

void dfs(int x,int cur,int now,int val){
	if (val>=inf) return;
	if (x>=n){
		U(f[t+1][now],val);
		if (now==(1<<n)-1) U(ans,f[t+1][now]);
		return;
	}
	dfs(x+1,cur,now,val);
	if (g[x][cur]!=inf) dfs(x+1,cur,now|(1<<x),val+g[x][cur]*(t+1));
}

int main(){
	freopen("treasure.in","r",stdin); freopen("treasure.out","w",stdout);
	
	register int m,x,y,z,i,j;
	scanf("%d%d",&n,&m);
	memset(G,8,sizeof G); inf=G[0][0];
	memset(f,8,sizeof f); ans= n==1? 0: inf;
	for (;m--;){
		scanf("%d%d%d",&x,&y,&z); --x, --y;
		G[x][y]=G[y][x]= min(G[x][y],z);
	}
	memset(g,8,sizeof g);
	for (x=0;x<n;++x){
		for (j=0;j<(1<<n);++j) if (!(j>>x&1)){
			for (i=0;i<n;++i) if (j>>i&1)
				U(g[x][j],G[x][i]);
		}
	}
	
	for (i=0;i<n;++i) f[0][1<<i]=0;
	for (t=0;t<n-1;++t)
		for (j=0;j<(1<<n);++j) if (f[t][j]!=inf)
			dfs(0,j,j,f[t][j]);
	
	cout<<ans;
	
	fclose(stdin); fclose(stdout); return 0;
}

#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cstring>
#define maxn 15
#define maxx 2147483647
using namespace std;
int n,m,x,y,quan;
int map[maxn][maxn];
int dis[maxn],bs[maxn];
bool vis[maxn];
int ans=maxx;
void dfs(int x,int now,int zhi,int ret){
	if(now==n){
		if(ret<ans) ans=ret;
		return ;
	}
	int que[maxn],tot=0;
	for(int i=1;i<=n;i++)
	 if(vis[i]==0&&map[x][i]<maxx){
	 	vis[i]=1;
	 	que[++tot]=i,now++,ret+=zhi*map[x][i];
	 }
	for(int i=1;i<=tot;i++){
		dfs(i,now,zhi+1,ret);
	}
}
void find(int x){
	int h=0,t=1;
	int ret=0;
	int que[maxn],shu[maxn];
	que[t]=x;shu[t]=1;
	while(h<t){
		h++;
		int u=que[h];
		for(int i=1;i<=n;i++)
		if(vis[i]==0&&map[u][i]<maxx)
		 if(dis[i]>map[u][i]*shu[h]){
			dis[i]=map[u][i]*shu[h];
			bs[i]=shu[h];
		}
		int k=0;
		for(int i=1;i<=n;i++)
		 if(dis[i]<dis[k]&&vis[i]==0)k=i;
		if(k!=0){
			t++;
			vis[k]=1;
			que[t]=k;
			ret+=dis[k];
			shu[t]=bs[k]+1;
		}
	}
	if(ret<ans&&ret!=0) ans=ret;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	 for(int j=1;j<=n;j++)
	  map[i][j]=maxx;
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&quan);
		map[x][y]=min(map[x][y],quan);
		map[y][x]=min(map[y][x],quan);
	}
	for(int i=1;i<=n;i++){
		memset(vis,0,sizeof(vis));
		dfs(i,1,1,0);
	}
	for(int i=1;i<=n;i++){
		memset(vis,0,sizeof(vis));
		memset(dis,63,sizeof(dis));
		vis[i]=1;
		find(i);
	}
	printf("%d\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

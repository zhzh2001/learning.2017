#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cstring>
#define maxn 1010
#define LL long long
using namespace std;
int T,n;
LL h,r;
int head[maxn],tot;
bool vis[maxn];
int que[maxn*2];
struct edgee{
	int to,next;
}edge[maxn*maxn*2];
struct nodee{
	LL xi,yi,zi;
}nod[maxn];
LL sq(LL x){
	return x*x;
}
LL abss(LL x){
	return (x>0)?x:-x;
}
void addedge(int u,int v){
	edge[++tot].to=v;edge[tot].next=head[u];head[u]=tot;
}
bool bfs(){
	memset(vis,0,sizeof(vis));
	int h=0,t=1;
	que[t]=0;
	while(h<t){
		h++;
		int u=que[h];
		for(int i=head[u];i!=-1;i=edge[i].next){
			int v=edge[i].to;
			if(vis[v]==0){
				if(v==n+1) return 1;
				que[++t]=v;
				vis[v]=1;
			}
		}
	}
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		memset(head,-1,sizeof(head));
		tot=0;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&nod[i].xi,&nod[i].yi,&nod[i].zi);
		}
		for(int i=1;i<=n;i++){
			if(abss(nod[i].zi)<=r){
				addedge(0,i);
				addedge(i,0);
			}
			if(nod[i].zi+r>=h){
				addedge(n+1,i);
				addedge(i,n+1);
			}
		 for(int j=i+1;j<=n;j++){
		 	LL x=sq(nod[i].xi-nod[j].xi),y=sq(nod[i].yi-nod[j].yi),z=sq(nod[i].zi-nod[j].zi);
		 	if(x+y<=4*sq(r)-z){
		 		addedge(i,j);
		 		addedge(j,i);
			 }
		 }
	   }
	   if(bfs()){
	   	printf("Yes\n");
	   }else{
	   	printf("No\n");
	   }  
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

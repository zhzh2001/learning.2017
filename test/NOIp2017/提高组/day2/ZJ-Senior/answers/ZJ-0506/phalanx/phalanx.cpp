#include<cstdio>
#include<cstdlib>
#include<iostream>
#define maxn 1010
#define maxm 3*100010
using namespace std;
int n,m,q,x,y,tot;
int map[maxn][maxn];
int tree[maxm*2];
int a[maxm*2];
int lowbit(int x){
	return x&(-x);
}
int query(int x){
	int ret=0;
	while(x){
		ret+=tree[x];
		x-=lowbit(x);
	}
	return ret;
}
void add(int x,int zhi){
	while(x<maxm*2){
		tree[x]+=zhi;
		x+=lowbit(x);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n==1){
		for(int i=1;i<=m;i++)a[i]=i;
		for(int i=1;i<=q;i++){
		 scanf("%d%d",&x,&y);
		 tot=query(y);
		 printf("%d\n",a[y-tot]);
		 add(y-tot,-1);
		 a[m+i]=a[y-tot];
		}
	}else
	if(n<=1000&&m<=1000){
		for(int i=1;i<=n;i++)
		 for(int j=1;j<=m;j++){
		 	map[i][j]=(i-1)*m+j;
		 }
		for(int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			printf("%d\n",map[x][y]);
			int t=map[x][y];
			for(int i=y;i<=m;i++)map[x][i]=map[x][i+1];
			for(int i=x;i<=n;i++)map[i][m]=map[i+1][m];
			map[n][m]=t;
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

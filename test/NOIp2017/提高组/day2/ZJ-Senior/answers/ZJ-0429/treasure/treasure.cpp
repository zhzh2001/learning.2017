#include<cstdio>
#include<cstring>
#include<iostream>

#define nn 5010
using namespace std;

inline int Read(){
	char c=getchar();int num=0,f=1;
	while ('0'>c||c>'9') {if(c=='-')f=-1;c=getchar();}
	while ('0'<=c&&c<='9') num=num*10+c-'0',c=getchar();
	return(num*f);
}

int mp[20][20],n,m,mn[20][nn];
int g[nn][nn],f[nn],F[nn],ans=1e9+1;

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	n=Read(),m=Read();
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++) mp[i][j]=1e8+1;
	for (int i=1;i<=m;i++){
		int a=Read(),b=Read(),c=Read();
		mp[a][b]=min(mp[a][b],c);mp[b][a]=mp[a][b];
	}
	
	memset(mn,1,sizeof(mn));
	for (int i=1;i<=n;i++)
	for (int j=1;j<(1<<n);j++)
	for (int k=1;k<=n;k++) if(j&(1<<k-1)) mn[i][j]=min(mn[i][j],mp[i][k]);
	
	for (int i=1;i<(1<<n);i++){
		int u=((1<<n)-1)^i;
		for (int j=u;j;j=(j-1)&u)
		for (int k=1;k<=n;k++) if(j&(1<<k-1)) g[i][j]+=mn[k][i];
	}
	
	memset(f,1,sizeof(f));
	for (int i=1;i<=n;i++) f[1<<i-1]=0;
	for (int d=1;d<n;d++){
		memset(F,1,sizeof(F));
		for (int j=1;j<(1<<n);j++){
			int u=((1<<n)-1)-j;
			for (int k=u;k;k=(k-1)&u) 
			F[j|k]=min(F[j|k],f[j]+g[j][k]*d);
		}
		for (int i=1;i<(1<<n);i++) f[i]=min(f[i],F[i]);
	}
	printf("%d\n",f[(1<<n)-1]);
}

#include<cstdio>
#include<cstring>
#include<iostream>

#define nn 300010
#define ll long long
using namespace std;


inline int Read(){
	char c=getchar();int num=0;
	while ('0'>c||c>'9') c=getchar();
	while ('0'<=c&&c<='9') num=num*10+c-'0',c=getchar();
	return(num);
}
int wr[20],wt;
inline void Write(ll x){
	for (;x;x/=10) wr[++wt]=x%10;
	for (;wt;wt--) putchar('0'+wr[wt]);puts("");
}

ll id[nn*50];
int t[nn*50],ch[nn*50][2],N;
inline void Del(int x,int l,int r,int a){
	t[x]--;
	if (l==r) return;
	int md=(l+r)/2;
	if (!ch[x][0]) t[ch[x][0]=++N]=md-l+1;
	if (!ch[x][1]) t[ch[x][1]=++N]=r-md;
	if (t[ch[x][0]]>=a) Del(ch[x][0],l,md,a);
	else Del(ch[x][1],md+1,r,a-t[ch[x][0]]);
}
inline ll Find(int x,int l,int r,int a){
	if (l==r){
		if (!id[x]) return(l);
		else return(id[x]);
	}
	int md=(l+r)/2;
	if (!ch[x][0]) t[ch[x][0]=++N]=md-l+1;
	if (!ch[x][1]) t[ch[x][1]=++N]=r-md;
	if (t[ch[x][0]]>=a) return(Find(ch[x][0],l,md,a));
	else return(Find(ch[x][1],md+1,r,a-t[ch[x][0]]));
}
inline void Ins(int x,int l,int r,int a,ll b){
	t[x]++;
	if (l==r) {id[x]=b;return;}
	int md=(l+r)/2;
	if (!ch[x][0]) ch[x][0]=++N;
	if (!ch[x][1]) ch[x][1]=++N;
	if (a<=md) Ins(ch[x][0],l,md,a,b);
	else Ins(ch[x][1],md+1,r,a,b);
}

int rt[nn],rta[nn],n,m,q,ad[nn];

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	n=Read(),m=Read(),q=Read();
	for (int i=1;i<=n;i++) t[rt[i]=++N]=m-1;t[rt[n+1]=++N]=n;
	for (int i=1;i<=n;i++) rta[i]=++N;rta[n+1]=++N;
	
	for (int i=1;i<=q;i++){
		int x=Read(),y=Read();
		
		if (y!=m){
			ll rs=0;
			if (t[rt[x]]>=y) rs=m*1ll*(x-1)+Find(rt[x],1,m-1,y);
			else{
				int yy=y-t[rt[x]];
				rs=Find(rta[x],1,q,yy);
			}
			
			ll nw=0;
			if (t[rt[n+1]]>=x) nw=m*1ll*Find(rt[n+1],1,n,x);
			else{
				int xx=x-t[rt[n+1]];
				nw=Find(rta[n+1],1,q,xx);
			}
			
			Write(rs);
			if (t[rt[x]]>=y) Del(rt[x],1,m-1,y);
			else{
				int yy=y-t[rt[x]];
				Del(rta[x],1,q,yy);
			}
			Ins(rta[x],1,q,++ad[x],nw);
			
			if (t[rt[n+1]]>=x) Del(rt[n+1],1,n,x);
			else{
				int xx=x-t[rt[n+1]];
				Del(rta[n+1],1,q,xx);
			}
			Ins(rta[n+1],1,q,++ad[n+1],rs);
		}else{
			ll nw=0;
			if (t[rt[n+1]]>=x) nw=m*1ll*Find(rt[n+1],1,n,x);
			else{
				int xx=x-t[rt[n+1]];
				nw=Find(rta[n+1],1,q,xx);
			}
			
			Write(nw);
			if (t[rt[n+1]]>=x) Del(rt[n+1],1,n,x);
			else{
				int xx=x-t[rt[n+1]];
				Del(rta[n+1],1,q,xx);
			}
			Ins(rta[n+1],1,q,++ad[n+1],nw);
		}
	}
}

#include<cstdio>
#include<cstring>
#include<iostream>

#define nn 2010
#define ll long long
using namespace std;

inline int Read(){
	char c=getchar();int num=0,f=1;
	while ('0'>c||c>'9') {if(c=='-')f=-1;c=getchar();}
	while ('0'<=c&&c<='9') num=num*10+c-'0',c=getchar();
	return(num*f);
}

ll h,r;
int mp[nn][nn],v[nn],n;
struct Poi{int x,y,z;}a[nn];
inline ll Sqr(ll a) {return(a*a);}
inline void Dfs(int x){
	v[x]=1;
	for (int i=1;i<=n;i++) if(mp[x][i]&&!v[i]) Dfs(i);
}

inline void Work(){
	n=Read(),h=Read(),r=Read();
	for (int i=1;i<=n;i++) a[i].x=Read(),a[i].y=Read(),a[i].z=Read();
	memset(mp,0,sizeof(mp));
	for (int i=1;i<=n;i++)
	for (int j=i+1;j<=n;j++){
		ll d=r*r*4ll;
		d-=Sqr(a[i].x-a[j].x);if(d<0)continue;
		d-=Sqr(a[i].y-a[j].y);if(d<0)continue;
		d-=Sqr(a[i].z-a[j].z);if(d<0)continue;
		mp[i][j]=mp[j][i]=1;
	}
	memset(v,0,sizeof(v));
	for (int i=1;i<=n;i++) if(a[i].z-r<=0&&!v[i]) Dfs(i);
	for (int i=1;i<=n;i++) if(a[i].z+r>=h&&v[i]) {puts("Yes");return;}
	puts("No");
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	int cs=Read();
	for (int ts=1;ts<=cs;ts++)
		Work();
}

const maxn=1005;
var t,n,h,r,i,j:longint;
    f:boolean;
    x,y,z,father:array[0..1005]of int64;

function dist(a,b:int64):real;
  begin
    dist:=sqrt(sqr(x[a]-x[b])+sqr(y[a]-y[b])+sqr(z[a]-z[b]));
  end;

function find(a:longint):longint;
  begin
    if a<>father[a] then find:=find(father[a])
    else find:=a;
    father[a]:=find;
  end;

procedure union(a,b:longint);
  var f1,f2:longint;
  begin
    f1:=find(a);
    f2:=find(b);
    if f1<>f2 then father[f2]:=f1;
  end;

begin
  assign(input,'cheese.in'); assign(output,'cheese.out');
  reset(input); rewrite(output);
  readln(t);
  while t>0 do
  begin
    readln(n,h,r); f:=false;
    for i:=1 to n do father[i]:=i;
    for i:=1 to n do
      readln(x[i],y[i],z[i]);
    for i:=1 to n do
      for j:=1 to n do
       if (i<>j)and(dist(i,j)<=2*r) then union(i,j);
    for i:=1 to n do
      if ((z[i]<=r)and(z[father[i]]+r>=h))or
         ((z[i]+r>=h)and(z[father[i]]<=r)) then
      begin
        f:=true;
        writeln('Yes');
        break;
      end;
    if not f then writeln('No');
    dec(t);
  end;
  close(input); close(output);
end.
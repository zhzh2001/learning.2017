const maxn=3*100000+5;
var n,m,q,i,j,x0,y0:longint;
    sum:int64;
    tre:array[0..maxn]of longint;
    tree:array[0..1000,0..1000]of longint;


function lowbit(k:longint):longint;
  begin
    lowbit:=k and (-k);
  end;

procedure jia1(x,y,num:longint);
  var l:longint;
  begin
    l:=y;
    while (l<=m) do
    begin
      inc(tree[x][l],num);
      if n=1 then inc(tre[l],num);
      l:=l+lowbit(l);
    end;
  end;
procedure jia2(x,y:longint);
  var k:longint;
  begin
    k:=x;
    while k<=n do
    begin
      k:=k+lowbit(k);
      inc(tree[k][y],m);
    end;
  end;

function he(x,y:longint):longint;
  var k,he2:longint;
  begin
    k:=y; he:=0; he2:=0;
    while k>=1 do
    begin
      inc(he,tree[x,k]);
      if n=1 then inc(he2,tre[k]);
      k:=k-lowbit(k);
    end;
    if (n=1)or(n>1000) then he:=he2;
  end;

begin
  assign(input,'phalanx.in'); assign(output,'phalanx.out');
  reset(input); rewrite(output);
  readln(n,m,q);
  for i:=1 to n do
    for j:=1 to m do
    begin
      if n<=1000 then tree[i][j]:=(i-1)*n+j;
      if (n=1)or(n>1000) then tre[j]:=tree[i][j];
    end;
  while q>0 do
  begin
    readln(x0,y0);
    writeln(he(x0,y0)-he(x0,y0-1));
    jia1(x0,y0,1);
    if (n=1)or(n>1000) then tre[m]:=tre[m]-(m-y0+1);
    if n<=1000 then
      begin
        dec(tree[x0][m]);
        jia2(x0,y0);
        tree[n][m]:=sum;
      end;
    dec(q);
  end;
  close(input); close(output);
end.
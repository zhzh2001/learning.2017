const maxn=12; maxm=1005; //2^13=8192;
var n,m,i:longint;
    depth:array[0..20]of longint;
    map:array[0..15,0..15]of longint;
    dp:array[0..15,0..15,0..15,0..8200]of longint;
    w,u,v:array[0..maxm]of longint;

function min(x,y:longint):longint;
  begin
    if x>y then min:=y
    else min:=x;
  end;

{procedure work;
  var i,j,k,state:longint;
  begin
    for i:=1 to n do dp[i][i][i][0]:=0;
    for k:=1 to n do
    begin
      depth[k]:=0;
      dp[k][k][k][1 shl (k-1)]:=0;
      //state:=state or (1 shl (k-1));
      for i:=1 to n do
       for j:=1 to n do
         if map[i,j]<>0 then
         for state:=1 to (1 shl (n+1))-1 do
           dp[k][i][j][state]:=min(dp[k][i][j][state],
           dp[k][][i][state]+map[i,j]*depth[i]);
    end;
  end;}

begin
  assign(input,'treasure.in'); assign(output,'treasure.out');
  reset(input); rewrite(output);
  readln(n,m);
  fillchar(dp,sizeof(dp),$5f);
  for i:=1 to m do
  begin
    readln(u[i],v[i],w[i]);
    if (map[u[i],v[i]]=0)or(map[u[i],v[i]]>w[i]) then
    begin
      map[u[i],v[i]]:=w[i];
      map[v[i],u[i]]:=w[i];
    end;
  end;
  //work;
  randomize;
  writeln(random(70)+30);
  close(input); close(output);
end.

#include <cstdio>
#include <algorithm>
using namespace std;
int n, m, ans, s, cnt, f[13];
struct xx
{
	int x, y, v;
} a[10001];
bool cmp(xx x, xx y)
{
	return x.v < y.v;
}
int gf(int x)
{
	if (x == f[x])	return x;
	else
	{
		f[x] = gf(f[x]);
		return f[x];
	}
}
int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; ++i)
		scanf("%d%d%d", &a[i].x, &a[i].y, &a[i].v);
	sort(a + 1, a + m + 1, cmp);
	for (int i = 1; i <= n; ++i)
		f[i] = i;
	for (int i = 1; i <= m; ++i)
	{
		if (gf(a[i].x) != gf(a[i].y))
		{
			f[gf(a[i].x)] = f[a[i].y];
			ans += a[i].v * (rand() % i + 1);
			++cnt;
		}
		if (cnt == n - 1)
			break;
	}
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
}

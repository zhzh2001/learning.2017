#include <cstdio>
using namespace std;
int n, m, q, i, x, y;
int a[1001][1001];
int c[300001][3], b[300001][3];
void gx()
{
	for (int j = i - 1; j >= 1; --j)
	{
		if (x == n && y == m)
		{
			x = b[j][1];
			y = b[j][2];
			continue;
		}
		if (x == c[j][1])
			y++;
		if (y == m && x > c[j][1])
			x++;
	}
}
int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if (n <= 1000 || m <= 1000)
	{
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= m; ++j)
				a[i][j] = (i - 1) * m + j;
		for (int i = 1; i <= q; ++i)
		{
			scanf("%d%d", &x, &y);
			printf("%d\n", a[x][y]);
			int t = a[x][y];
			for (int j = y; j < m; ++j)
				a[x][j] = a[x][j + 1];
			for (int j = x; j < n; ++j)
				a[j][m] = a[j + 1][m];
			a[n][m] = t;
		}
	}
	else
	{
		for (i = 1; i <= q; ++i)
		{
			scanf("%d%d", &x,  &y);
			c[i][1] = x;
			c[i][2] = y;
			if (x == n && y == m)
			{
				printf("%d\n", b[i - 1][1] * (m - 1) + b[i - 1][2]);
				continue;
			}
			gx();
			b[i][1] = x;
			b[i][2] = y;
			printf("%d\n", (x - 1) * m + y);
		}
	}
	fclose(stdin);
	fclose(stdout);
}

#include <cstdio>
#include <cctype>
using namespace std;

int n,m,q,x,y,w,map[1010][1010];

inline char nc(void){
	static char ch[100010],*p1=ch,*p2=ch;
	return p1==p2&&(p2=(p1=ch)+fread(ch,1,100010,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &a){
	static char c=nc();int f=1;
	for (;!isdigit(c);c=nc()) if (c=='-') f=-1;
	for (a=0;isdigit(c);a=(a<<3)+(a<<1)+c-'0',c=nc());
	return (void)(a*=f);
}

int main(void){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n),read(m),read(q);
	for (int i=1; i<=n; ++i)
		for (int j=1; j<=m; ++j)
			map[i][j]=(i-1)*m+j;
	while (q--){
		read(x),read(y);
		printf("%d\n",w=map[x][y]);
		for (int i=y; i<m; ++i) map[x][i]=map[x][i+1];
		for (int i=x; i<n; ++i) map[i][m]=map[i+1][m];
		map[n][m]=w;
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

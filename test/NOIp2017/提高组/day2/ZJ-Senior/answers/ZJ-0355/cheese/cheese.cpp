#include <cstdio>
#include <cctype>
#include <cstring>
using namespace std;

typedef long long ll;
const int N=1010;
ll t,n,h,r,st,ed,x[N],y[N],z[N],que[N],tou,wei;
bool map[N][N],b[N];

inline char nc(void){
	static char ch[100010],*p1=ch,*p2=ch;
	return p1==p2&&(p2=(p1=ch)+fread(ch,1,100010,stdin),p1==p2)?EOF:*p1++;
}

inline void read(ll &a){
	static char c=nc();int f=1;
	for (;!isdigit(c);c=nc()) if (c=='-') f=-1;
	for (a=0;isdigit(c);a=(a<<3)+(a<<1)+c-'0',c=nc());
	return (void)(a*=f);
}

int main(void){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (read(t); t; --t){
		read(n),read(h),read(r),st=0,ed=n+1,memset(map,0,sizeof map);
		for (int i=1; i<=n; ++i){
			read(x[i]),read(y[i]),read(z[i]);
			if (z[i]<=r) map[st][i]=1;
			if (z[i]>=h-r) map[i][ed]=1;
		}
		for (int i=1; i<=n; ++i)
			for (int j=1; j<=n; ++j)
				if (i!=j&&(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])<=r*r*4) map[i][j]=1;
		memset(b,0,sizeof b),que[tou=wei=1]=st,b[st]=1;
		while (tou<=wei){
			int p=que[tou++];
			for (int i=st; i<=ed; ++i) if (map[p][i]&&!b[i]) que[++wei]=i,b[i]=1;
			if (b[ed]) break;
		}
		puts(b[ed]?"Yes":"No");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

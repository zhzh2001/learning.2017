#include <cstdio>
#include <cctype>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

typedef long long ll;
int n,m,map[13][13];
ll pow[13],f[1<<12],c[1<<12][1<<12],ans;

inline char nc(void){
	static char ch[100010],*p1=ch,*p2=ch;
	return p1==p2&&(p2=(p1=ch)+fread(ch,1,100010,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &a){
	static char c=nc();int f=1;
	for (;!isdigit(c);c=nc()) if (c=='-') f=-1;
	for (a=0;isdigit(c);a=(a<<3)+(a<<1)+c-'0',c=nc());
	return (void)(a*=f);
}

int main(void){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n),read(m),memset(map,0x3f,sizeof map),pow[0]=1;
	for (int i=1; i<=n; ++i) pow[i]=pow[i-1]*13;
	for (int i=1,x,y,w; i<=m; ++i) read(x),read(y),read(w),map[x][y]=map[y][x]=min(map[x][y],w);
	memset(f,0x3f,sizeof f);
	for (int i=1; i<=n; ++i) f[1<<(i-1)]=0,c[1<<(i-1)][c[1<<(i-1)][0]=1]=pow[i-1];
	for (int s=1; s<(1<<n); ++s)
		for (int i=1; i<=n; ++i)
			if (s&(1<<(i-1)))
				for (int j=1; j<=n; ++j)
					if (!(s&(1<<(j-1)))&&map[i][j]!=0x3f3f3f3f)
						for (int k=1; k<=c[s][0]; ++k)
							if (f[s|(1<<(j-1))]>f[s]+map[i][j]*(c[s][k]/pow[i-1]%13)){
								f[s|(1<<(j-1))]=f[s]+map[i][j]*(c[s][k]/pow[i-1]%13);
								c[s|(1<<(j-1))][c[s|(1<<(j-1))][0]=1]=c[s][k]+pow[(j-1)]*(c[s][k]/pow[i-1]%13+1);
							}
							else if (f[s|(1<<(j-1))]==f[s]+map[i][j]*(c[s][k]/pow[i-1]%13)&&c[s|(1<<(j-1))][0]<=4000)
								c[s|(1<<(j-1))][++c[s|(1<<(j-1))][0]]=c[s][k]+pow[(j-1)]*(c[s][k]/pow[i-1]%13+1);
	ans=1ll<<60;
	for (int i=1; i<=n; ++i) ans=min(ans,f[(1<<n)-1]);
	printf("%lld\n",ans);
	fclose(stdin),fclose(stdout);
	return 0;
}

#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;

typedef long long LL;
const int N = 14;

int dp[46000000];
int G[N][N];

int tab[N];
int in[N], n, m;
//��Ȳ�����n/2 ??
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	memset(dp, 127, sizeof dp);
	memset(G, -1, sizeof G);
	scanf("%d%d", &n, &m);
	for (int i = 1, x, y, l; i <= m; i ++) {
		scanf("%d%d%d", &x, &y, &l);
		x --; y --;
		if (G[x][y] == -1) G[x][y] = G[y][x] = l;
		else G[x][y] = G[y][x] = min(G[x][y], l);
	}
	
	int base = n + 1;
	if (n > 8) base = (n + 1) / 2 + 1;
	
	for (int i = tab[0] = 1; i <= n; i ++) tab[i] = tab[i - 1] * base;
	
	int tot = tab[n], Ans = dp[1];
	for (int i = 0; i < n; i ++) dp[tab[i]] = 0;
	
	for (register int s = 1; s <= tot; s ++) {
		if (dp[s] >= Ans) continue;
		bool flag = 1;
		for (register int i = 0; i < n; i ++) {
			in[i] = (s / tab[i]) % base;
			if (!in[i]) flag = 0;
		}
		if (flag) {
			Ans = min(Ans, dp[s]);
			continue;
		}
		for (register int i = 0; i < n; i ++) {
			if (!in[i] || in[i] == base - 1) continue;
			for (register int j = 0; j < n; j ++)
				if (!in[j] && G[i][j] != -1) dp[s + tab[j] * (in[i] + 1)] = min(dp[s + tab[j] * (in[i] + 1)], dp[s] + in[i] * G[i][j]);
		}
	}
	printf("%d\n", Ans);
	return 0;
}

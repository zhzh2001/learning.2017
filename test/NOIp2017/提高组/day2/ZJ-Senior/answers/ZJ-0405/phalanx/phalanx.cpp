#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

typedef long long LL;
const int N = 3e5 + 5;

int n, m, q;
vector <LL> vec[50005];
LL rt[50005];
int G[1005][1005];

inline void score30() {
	int cnt = 0;
	for (int i = 1; i <= n; i ++)
		for (int j = 1; j <= m; j ++) G[i][j] = ++ cnt;
	int x, y, t;
	while (q --) {
		scanf("%d%d", &x, &y);
		t = G[x][y];
		printf("%d\n", t);
		for (int i = y; i < m; i ++) G[x][i] = G[x][i + 1];
		for (int i = x; i < n; i ++) G[i][m] = G[i + 1][m];
		G[n][m] = t;
	}
}
bool use[50005];
int x[300005], y[300005];
inline void score50() {
	for (int i = 1; i <= q; i ++) {
		scanf("%d%d", &x[i], &y[i]);
		use[x[i]] = 1;
	}
	for (int i = 1; i <= n; i ++)
		if (use[i]) {
			vec[i].resize(m + 5);
			for (int j = 1; j <= m; j ++) vec[i][j] = (i - 1) * m + j;
		}
	for (int i = 1; i <= n; i ++) rt[i] = i * m;
	int X, Y;
	LL t;
	for (int j = 1; j <= q; j ++) {
		X = x[j]; Y = y[j];
		t = vec[X][Y];
		printf("%lld\n", t);
		if (X == n && Y == m) continue;
		for (int i = Y; i < m; i ++) vec[X][i] = vec[X][i + 1];
		for (int i = X; i < n; i ++) {
			rt[i] = rt[i + 1];
			if (vec[i].size()) vec[i][m] = rt[i];
		}
		rt[n] = t;
		if (vec[n].size()) vec[n][m] = t;
	}
}

LL arr[N];

struct Splay {
#define upd(x) size[x] = 1 + size[ch[x][0]] + size[ch[x][1]];
	int sz;
	LL key[N << 1];
	int fa[N << 1], ch[N << 1][2], size[N << 1], rt;
	Splay() {
		sz = 0;
		memset(fa, 0, sizeof fa);
		memset(ch, 0, sizeof ch);
		memset(size, 0, sizeof size);
	}
	inline int build(int l, int r) {
		int x = ++ sz, mid = (l + r) >> 1;
		key[x] = arr[mid];
		if (l < mid) {
			ch[x][0] = build(l, mid - 1);
			fa[ch[x][0]] = x;
		}
		if (mid < r) {
			ch[x][1] = build(mid + 1, r);
			fa[ch[x][1]] = x;
		}
		upd(x);
		return x;
	}
	inline void init(int tot) {
		rt = build(1, tot);
	}
	inline void link(int x, int p, int d) {
		if (x) fa[x] = p;
		if (p) ch[p][d] = x;
	}
	inline void rotate(int x) {
		int f = fa[x], d = (x == ch[f][1]);
		link(x, fa[f], f == ch[fa[f]][1]);
		link(ch[x][!d], f, d);
		link(f, x, !d);
		upd(f); upd(x);
	}
	inline void splay(int x) {
		rt = x;
		int f;
		for (; fa[x]; rotate(x)) {
			if (!fa[fa[x]]) {
				rotate(x);
				break;
			}
			f = fa[x];
			if ((x == ch[f][1]) == (f == ch[fa[f]][1])) rotate(f);
			else rotate(x);
		}
	}
	inline void ins(int pos) {
		ch[pos][0] = ch[pos][1] = 0;
		int p = rt;
		while (ch[p][1]) p = ch[p][1];
		fa[pos] = p;
		ch[p][1] = pos;
		upd(pos);
		while (fa[pos]) {
			pos = fa[pos];
			upd(pos);
		}
	}
	inline int kth(int k) {
		int p = rt;
		while (1) {
			if (size[ch[p][0]] + 1 == k) return p;
			if (size[ch[p][0]] >= k) p = ch[p][0];
			else {
				k -= size[ch[p][0]] + 1;
				p = ch[p][1];
			}
		}
	}
	inline void work(int k) {
		int x = kth(k);
		printf("%lld\n", key[x]);
		if (k == 1) {
			splay(x);
			fa[ch[x][1]] = 0;
			rt = ch[x][1];
			ins(x);
			return;
		}
		int pre = kth(k - 1);
		splay(pre);
		if (ch[x][1]) {
			fa[ch[x][1]] = fa[x];
			ch[fa[x]][x == ch[fa[x]][1]] = ch[x][1];
		}
		int p = ch[x][1];
		while (fa[p]) {
			p = fa[p];
			upd(p);
		}
		ins(x);
	}
};

inline void score80() {
	for (int i = 1; i <= m; i ++) arr[i] = i;
	int tot = m;
	for (int j = 2; j <= n; j ++) {
		arr[tot + 1] = arr[tot] + m;
		tot ++;
	}
	Splay s;
	s.init(tot);
	int x, y;
	while (q --) {
		scanf("%d%d", &x, &y);
		s.work(y);
	}
}

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if (n <= 1000) score30();
	else if (1LL * (n + m) * q <= 40000000) score50();
	else score80();
	return 0;
}

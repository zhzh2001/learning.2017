#include <cstdio>
#include <iostream>
#include <algorithm>
#define sqr(x) ((x) * (x))
using namespace std;

typedef long long LL;
const int N = 1015;

LL x[N], y[N], z[N];
int fa[N];

inline LL dis(int a, int b) {
	return sqr(x[a] - x[b]) + sqr(y[a] - y[b]) + sqr(z[a] - z[b]);
}

inline int getf(int x) {
	return fa[x] == x ? x : fa[x] = getf(fa[x]);
}

inline void U(int x, int y) {
	x = getf(x); y = getf(y);
	if (x != y) fa[x] = y;
}

inline void solve() {
	int n;
	LL h, r;
	scanf("%d%lld%lld", &n, &h, &r);
	for (int i = 1; i <= n; i ++) scanf("%lld%lld%lld", &x[i], &y[i], &z[i]);
	for (int i = 1; i <= n + 2; i ++) fa[i] = i;
	for (int i = 1; i <= n; i ++) {
		if (z[i] <= r) U(i, n + 1);
		if (z[i] >= h - r) U(i, n + 2);
		for (int j = i + 1; j <= n; j ++)
			if (dis(i, j) <= 4 * r * r) U(i, j);
	}
	if (getf(n + 1) == getf(n + 2)) puts("Yes");
	else puts("No");
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T;
	scanf("%d", &T);
	while (T --) solve();
	return 0;
}

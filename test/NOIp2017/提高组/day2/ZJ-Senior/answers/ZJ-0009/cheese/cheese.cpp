#include<bits/stdc++.h>
#define maxn 1010
#define ll long long
#define R register
using namespace std;
ll t,n,h,r,zou[maxn],f,len;
struct st{
	ll x,y,z;
}dian[maxn];

inline ll fin(ll i,ll j)
{
	ll a = dian[i].x - dian[j].x;
	a *= a;
	ll b = dian[i].y - dian[j].y;
	b *= b;
	ll c = dian[i].z - dian[j].z;
	c *= c;
	if(a + b + c <= len) return 1;
	return 0;
}

void dfs(ll now)
{
	zou[now] = 1;
	for(ll i=1;i<=n;i++)
	{
		if(fin(now,i) && !zou[i])
		{
			dfs(i);
		}
	}
	if(dian[now].z + r >= h) f = 1;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&t);
	while(t--)
	{
		f = 0;
		scanf("%lld%lld%lld",&n,&h,&r);
		len = (r * r) << 2;
		for(R ll i=1;i<=n;i++)
		{
			zou[i] = 0;
			scanf("%lld%lld%lld",&dian[i].x,&dian[i].y,&dian[i].z);
		}
		for(int i=1;i<=n;i++)
			if(!zou[i] && dian[i].z - r <= 0)
				dfs(i);
		if(f) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

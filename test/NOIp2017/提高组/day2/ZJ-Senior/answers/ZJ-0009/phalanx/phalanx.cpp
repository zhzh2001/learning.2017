#include<bits/stdc++.h>
#define maxn 3010
#define R register
using namespace std;
int s[maxn][maxn],n,m,x,y,temp,t;
inline int read()
{
	int lin = 0;
	char x = getchar();
	while(x < '0' || x > '9') x = getchar();
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read();
	m = read();
	t = read();
	if(n == 1)
	{
		int q[340000] = {0};
		for(R int i=1;i<=m;i++) q[i] = i;
		for(R int i=1;i<=t;i++)
		{
			x = read();
			y = read();
			printf("%d\n",q[y]);
			temp = q[y];
			for(R int j=y;j<m;j++)
				q[j] = q[j + 1];
			q[m] = temp;
		}
		return 0;
	}
	for(R int i=1;i<=n;i++)
		for(R int j=1;j<=m;j++)
			s[i][j] = (i - 1) * m + j;
	while(t--)
	{
		x = read();
		y = read();
		temp = s[x][y];
		printf("%d\n",temp);
		for(R int i=y;i<m;i++)
			s[x][i] = s[x][i + 1];
		for(R int i=x;i<n;i++)
			s[i][m] = s[i + 1][m];
		s[n][m] = temp;
	}
	return 0;
}

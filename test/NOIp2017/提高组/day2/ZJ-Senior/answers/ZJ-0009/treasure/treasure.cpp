#include<bits/stdc++.h>
#define R register
using namespace std;
int lian[13][13],zou[13],n,m,u,v,val,ke[13],sh[13],step[13],ans = 1000000000;
inline int read()
{
	int lin = 0;
	char x = getchar();
	while(x < '0' || x > '9') x = getchar();
	while(x >= '0' && x <= '9')
	{
		lin = (lin << 1) + (lin << 3) + x - '0';
		x = getchar();
	}
	return lin;
}
void dfs(int roo,int val,int t)
{
	int a[13] = {0},b[13] = {0},c[13] = {0};
	if(t == n)
	{
		ans = min(ans,val);
		return;
	}
	for(int i=1;i<=n;i++)
		if(ke[i])
		{
			zou[i] = 1;
			step[i] = step[sh[i]] + 1;
			for(int j=1;j<=n;j++)
			{
				if(!zou[j] && lian[i][j])
				{
					if(!ke[j])
					{
						c[j] = 1;
						a[j] = ke[j];
						b[j] = sh[j];
						ke[j] = 1;
						sh[j] = i;
					}
					else
					{
						if(step[sh[j]] * lian[sh[j]][j] > step[i] * lian[i][j])
						{
							c[j] = 1;
							a[j] = ke[j];
							b[j] = sh[j];
							sh[j] = i;
						}
					}
				}
			}
			ke[i] = 0;
			dfs(roo,val + step[sh[i]] * lian[sh[i]][i],t + 1);
			for(int j=1;j<=n;j++)
				if(c[j])
				{
					ke[j] = a[j];
					sh[j] = b[j];
					c[j] = 0;
				}
			ke[i] = 1;
			zou[i] = 0;
		}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	//scanf("%d%d",&n,&m);
	n = read();
	m = read();
	for(R int i=1;i<=m;i++)
	{
		//scanf("%d%d%d",&u,&v,&val);
		u = read();
		v = read();
		val = read();
		if(!lian[u][v]) lian[u][v] = lian[v][u] = val;
		else lian[u][v] = lian[v][u] = min(val,lian[u][v]);
	}
	
	for(R int i=1;i<=n;i++)
	{
		memset(ke,0,sizeof(ke));
		memset(sh,0,sizeof(sh));
		memset(step,0,sizeof(step));
		zou[i] = 1;
		step[i] = 1;
		for(R int j=1;j<=n;j++)
			if(i != j && lian[i][j])
			{
				ke[j] = 1;
				sh[j] = i;
			}
		dfs(i,0,1);
		zou[i] = 0;
	}
	printf("%d",ans);
	return 0;
}

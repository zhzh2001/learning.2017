#include<cmath>
#include<cstdio>

using namespace std;

int n,m,q,cnt;
int x[505],y[505];
int sq,l[605],suc[300005],pre[300005],num[300005];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(q<=2000)
	{
		for(int i=1; i<=q; ++i) scanf("%d%d",&x[i],&y[i]);
		for(int i=1; i<=q; ++i)
		{
			long long a=x[i],b=y[i];
			for(int j=i-1; j; --j)
			{
				if(a==n&&b==m) a=x[j],b=y[j];
				else
				{
					if(b==m)
					{
						if(x[j]<=a) ++a;
					}
					else
					{
						if(x[j]==a) ++b;
					}
				}
			}
			printf("%lld\n",(a-1)*m+b);
		}
	}
	else if(n==1)
	{
		sq=sqrt(m);
		for(int i=1; i<=m; ++i)
		{
			pre[i]=i-1;
			suc[i]=i+1;
			num[i]=i;
			if((i-1)%sq==0) l[cnt++]=i;
		}
		int t=m;
		for(int i=1; i<=q; ++i)
		{
			int a,b,f,g,adr;
			scanf("%d%d",&a,&b);
			f=(b-1)/sq;
			g=(b-1)%sq;
			adr=l[f];
			while(g--) adr=suc[adr];
			printf("%d\n",num[adr]);
			suc[pre[adr]]=suc[adr];
			pre[suc[adr]]=pre[adr];
			suc[t]=adr;
			if((b-1)%sq>0) ++f;
			for(int j=f; j<cnt; ++j) l[j]=suc[l[j]];
			pre[adr]=t;
			suc[adr]=m+1;
			t=adr;
		}
	}
	return 0;
}

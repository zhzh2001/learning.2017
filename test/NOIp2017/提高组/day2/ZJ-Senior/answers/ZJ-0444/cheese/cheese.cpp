#include<cmath>
#include<cstdio>
#include<cstring>

using namespace std;

int T;
int n;
long long h,r;
long long xi[1005],yi[1005],zi[1005];
bool g[1005][1005], vis[1005];

void dfs(int u)
{
	vis[u]=true;
	for(int i=1; i<=n+1; ++i)
		if(g[u][i]&&(!vis[i])) dfs(i);
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		memset(g,false,sizeof(g));
		scanf("%d%lld%lld",&n,&h,&r);
		for(int i=1; i<=n; ++i) scanf("%lld%lld%lld",&xi[i],&yi[i],&zi[i]);
		for(int i=1; i<=n; ++i) if(abs(zi[i])<=r) g[0][i]=g[i][0]=true;
		for(int i=1; i<=n; ++i) if(abs(h-zi[i])<=r) g[i][n+1]=g[n+1][i]=true;
		for(int i=1; i<=n; ++i)
		{
			for(int j=i+1; j<=n; ++j)
			{
				long long x=xi[j]-xi[i],y=yi[j]-yi[i],z=zi[j]-zi[i];
				if(x*x+y*y+z*z<=4LL*r*r) g[i][j]=g[j][i]=true;
			}
		}
		memset(vis,false,sizeof(vis));
		dfs(0);
		if(vis[n+1]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<queue>
#include<cstdio>
#include<cstring>
#include<climits>
#include<algorithm>

using namespace std;

int n,m,ans=INT_MAX;
int f[13], dep[13];
int g[13][13];
int vis[13];
queue<int> Q;

struct node
{
	int to,v;
};

bool cmp(const node &a, const node &b)
{
	return a.v>b.v;
}

void dfs(int d, int c)
{
	if(c>=ans) return;
	bool flag=true;
	for(int i=1; i<=n; ++i)
	{
		if(vis[i]==0)
		{
			flag=false;
			break;
		}
	}
	if(flag)
	{
		ans=c;
		return;
	}
	int cnt=0,sum=0;
	node x[13];
	for(int i=1; i<=n; ++i)
	{
		if(vis[i]==d)
		{
			for(int j=1; j<=n; ++j)
			{
				if(vis[j]==0&&g[i][j]<=500000)
				{
					++cnt;
					x[cnt].to=j;
					x[cnt].v=g[i][j];
				}
			}
		}
		else if(vis[i]==0)
		{
			sum+=max(d,dep[i])*f[i];
			if(c+sum>=ans) return;
		}
	}
	sort(x+1,x+cnt+1,cmp);
	for(int i=(1<<cnt)-1; i; --i)
	{
		int cs=0;
		for(int j=1; j<=cnt; ++j)
		{
			if((1<<(j-1))&i)
			{
				vis[x[j].to]=d+1;
				cs+=x[j].v*d;
			}
		}
		dfs(d+1,c+cs);
		for(int j=1; j<=cnt; ++j)
			if((1<<(j-1))&i) vis[x[j].to]=0;
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,63,sizeof(f));
	memset(g,63,sizeof(g));
	for(int i=1; i<=m; ++i)
	{
		int a,b,v;
		scanf("%d%d%d",&a,&b,&v);
		if(v<g[a][b])
		{
			g[a][b]=g[b][a]=v;
			if(v<f[a]) f[a]=v;
			if(v<f[b]) f[b]=v;
		}
	}
	for(int i=1; i<=n; ++i)
	{
		memset(dep,0,sizeof(dep));
		Q.push(i);
		while(!Q.empty())
		{
			int t=Q.front();
			Q.pop();
			for(int j=1; j<=13; ++j) if(dep[j]==0&&g[t][j]<=500000) dep[j]=dep[t]+1; 
		}
		vis[i]=1;
		dfs(1,0);
		vis[i]=0;
	}
	printf("%d\n",ans);
	return 0;
}

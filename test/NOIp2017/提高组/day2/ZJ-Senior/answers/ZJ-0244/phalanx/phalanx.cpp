#include <map>
#include <set>
#include <queue>
#include <ctime>
#include <cmath>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define debug(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
#define lowbit(x) x&-x
#define ll long long
#define db double
const int M=3e5+5;
const int N=1e3+5;
void rd(int &x){
	char c;x=0;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),isdigit(c));
	x*=f;
}
int n,m,q;
struct Q{
	int x,y;
}a[M];
struct P1{
	int id[N][N];
	bool chk(){
		return n<=1e3&&m<=1e3&&q<=500;
	}
	void solve(){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++)id[i][j]=(i-1)*m+j;
		}
		for(int i=1,x,y;i<=q;i++){
			x=a[i].x,y=a[i].y;
			int res=id[x][y];
			printf("%d\n",res);
			for(int j=y;j<m;j++)id[x][j]=id[x][j+1];
			for(int j=x;j<n;j++)id[j][m]=id[j+1][m];
			id[n][m]=res;
		}
	}
}P1;
struct P2{
	bool chk(){
		if(n!=1)return 0;
		for(int i=1;i<=q;i++){
			if(a[i].x!=1)return 0;
		}
		return 1;
	}
	int id[M<<1];
	struct BIT{
		int num[M<<1];
		void add(int x,int v){
			while(x<2*M){
				num[x]+=v;
				x+=lowbit(x);
			}
		}
		int query(int x){
			int res=0;
			while(x){
				res+=num[x];
				x-=lowbit(x);
			}
			return res;
		}
	}T;
	void solve(){
		int tot=0;
		for(int i=1;i<=m;i++){
			id[++tot]=i;
			T.add(i,1);
		}
		T.add(1,1);
		T.add(m+1,-1);
		for(int i=1,y;i<=q;i++){
			y=a[i].y+1;
			int l=1,r=tot,res=0,ans;
			while(l<=r){
				int md=l+r>>1;
				int cnt=T.query(md);
				if(cnt>=y){
					res=md;
					r=md-1;
				}else l=md+1;
			}
			ans=id[res];
			printf("%d\n",ans);
			T.add(res,-1);
			T.add(tot+1,1);
			tot++;
			T.add(tot,1);
			T.add(tot+1,-1);
			id[tot]=ans;
		}
	}
}P2;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	rd(n),rd(m),rd(q);
	for(int i=1;i<=q;i++){
		rd(a[i].x),rd(a[i].y);
	}
	if(0);
	else if(P1.chk())P1.solve();
	else if(P2.chk())P2.solve();
	return 0;
}

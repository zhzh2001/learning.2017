#include <map>
#include <set>
#include <queue>
#include <ctime>
#include <cmath>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define debug(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
#define ll long long
#define db double
const int M=1005;
void rd(int &x){
	char c;x=0;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),isdigit(c));
	x*=f;
}
struct node{int x,y,z;}a[M];
vector<int>e[M];
int cas,n,h,r;
queue<int>q;
bool mk[M];
ll P(int x){
	return 1ll*x*x;
}
void ADD_EGDE(){
	ll d=P(2*r);
	for(int i=1;i<=n;i++){
		for(int j=i+1;j<=n;j++){
			ll dis=P(a[i].x-a[j].x)+P(a[i].y-a[j].y)+P(a[i].z-a[j].z);
			if(dis<=d){
				e[i].push_back(j);
				e[j].push_back(i);
			}
		}
	}
	for(int i=1;i<=n;i++){
		if(a[i].z<=r)e[0].push_back(i);
		if(h-a[i].z<=r)e[i].push_back(n+1);
	}
}
void BFS(){
	memset(mk,0,sizeof mk);
	while(!q.empty())q.pop();
	q.push(0);
	while(!q.empty()){
		int u=q.front();
		q.pop();
		if(mk[u])continue;
		mk[u]=1;
		if(u==n+1)break;
		for(int i=0,v;i<e[u].size();i++){
			v=e[u][i];
			if(mk[v])continue;
			q.push(v);
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	rd(cas);
	while(cas--){
		rd(n),rd(h),rd(r);
		for(int i=1;i<=n;i++)rd(a[i].x),rd(a[i].y),rd(a[i].z);
		ADD_EGDE();
		BFS();
		if(mk[n+1])puts("Yes");
		else puts("No");
		for(int i=0;i<=n+1;i++)e[i].clear();
	}
	return 0;
}

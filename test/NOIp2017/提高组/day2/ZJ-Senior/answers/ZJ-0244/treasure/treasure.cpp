#include <map>
#include <set>
#include <queue>
#include <ctime>
#include <cmath>
#include <bitset>
#include <vector>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define debug(x) cout<<#x<<" = "<<x<<endl;
using namespace std;
#define ll long long
const int M=12;
void rd(int &x){
	char c;x=0;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do x=(x<<1)+(x<<3)+(c^48);
	while(c=getchar(),isdigit(c));
	x*=f;
}
struct KK{
	int s,w;
	vector<int>dis;
	bool operator<(const KK &a)const{
		return w>a.w;
	}
};
map<vector<int>,bool>mk[1<<M];
map<vector<int>,int>dp[1<<M];
struct node{int v,w;};
priority_queue<KK>q;
vector<node>e[M];
vector<int>dis;
int n,m,S,res;
void chk(int &x,int y){
	if(x==-1||x>y)x=y;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	rd(n),rd(m);
	S=(1<<n)-1;
	for(int i=1,u,v,w;i<=m;i++){
		scanf("%d %d %d",&u,&v,&w);
		u--,v--;
		e[u].push_back((node){v,w});
		e[v].push_back((node){u,w});
	}
	for(int i=0;i<n;i++)dis.push_back(0);
	for(int i=0;i<n;i++){
		for(int j=0;j<n;j++)dis[j]=0;
		dis[i]=1;
		q.push((KK){(1<<i),dp[1<<i][dis]=0,dis});
	}
	while(!q.empty()){
		KK now=q.top();
		q.pop();
		int s,w;
		s=now.s;
		if(mk[s].find(now.dis)!=mk[s].end())continue;
		w=now.w;
		mk[s][dis=now.dis]=1;
		if(s==S){
			res=w;
			break;
		}
		for(int i=0;i<n;i++){
			if(s&(1<<i)){
				for(int j=0,v,ww,ns;j<e[i].size();j++){
					v=e[i][j].v;
					if(s&(1<<v))continue;
					ww=e[i][j].w;
					dis[v]=dis[i]+1;
					ns=s|(1<<v);
					ll nd=w+dis[i]*ww;
					if(dp[ns].find(dis)==dp[ns].end()||dp[ns][dis]>nd){
						dp[ns][dis]=nd;
						q.push((KK){ns,dp[ns][dis],dis});
					}
					dis[v]=0;
				}
			}
		}
	}
	printf("%d\n",res);
	return 0;
}

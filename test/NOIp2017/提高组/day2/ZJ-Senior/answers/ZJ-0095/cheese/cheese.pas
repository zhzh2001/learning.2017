var
  n,h,i,j,t,k,d:longint;
  r,v:int64;
  w:boolean;
  x,y,z:array[0..1023]of int64;
  s:array[0..1023]of boolean;
procedure sort(l,r:longint);
var
  i,j:longint;
  u:int64;
begin
  i:=l;
  j:=r;
  u:=z[random(r-l+1)+l];
  repeat
    while z[i]<u do inc(i);
    while u<z[j] do dec(j);
    if i<=j then
    begin
      v:=x[i];x[i]:=x[j];x[j]:=v;
      v:=y[i];y[i]:=y[j];y[j]:=v;
      v:=z[i];z[i]:=z[j];z[j]:=v;
      inc(i);dec(j);
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
assign(input,'cheese.in');
assign(output,'cheese.out');
reset(input);
rewrite(output);
readln(t);
randomize;
for k:=1 to t do
begin
  readln(n,h,r);
  for i:=1 to n do
    readln(x[i],y[i],z[i]);
  sort(1,n);
  fillchar(s,sizeof(s),0);
  i:=1;
  while z[i]<=r do
  begin
    s[i]:=true;
    inc(i);
  end;
  for i:=i to n do
  begin
    if not s[i] then
      for j:=i-1 downto 1 do
      begin
        if z[i]-z[j]>r shl 1 then break;
        if s[j] then
          if sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=r*r shl 2 then
          begin s[i]:=true; break; end;
      end;
    if not s[i] then
      for j:=i+1 to n do
      begin
        if z[j]-z[i]>r shl 1 then break;
        if s[j] then
          if sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=r*r shl 2 then
          begin s[i]:=true; break; end;
      end;
  end;
  d:=h-r;
  w:=false;
  for i:=n downto 1 do
    if s[i] and(z[i]>=d) then begin w:=true; break; end;
  if w then writeln('Yes') else writeln('No');
end;
close(input);
close(output);
end.
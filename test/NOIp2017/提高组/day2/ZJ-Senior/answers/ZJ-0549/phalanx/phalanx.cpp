#include<cmath>
#include<vector>
#include<cstdio>
#define ll long long
using namespace std;
namespace FastIO{
	const int LBC=2333333;
	char LZH[LBC],*SSS=LZH,*TTT=LZH;
	inline char gc(){
		if (SSS==TTT){
			TTT=(SSS=LZH)+fread(LZH,1,LBC,stdin);
			if (SSS==TTT) return EOF;
		}
		return *SSS++;
	}
	inline int read(){
		int x=0;
		char ch=gc();
		for (;ch<'0'||ch>'9';ch=gc());
		for (;ch>='0'&&ch<='9';ch=gc())
			x=x*10-48+ch;
		return x;
	}
	inline void write(ll x){
		static int a[20],top;
		for (top=0;x;a[++top]=x%10,x/=10);
		for (;top;putchar(a[top--]+'0'));
		puts("");
	}
}
using namespace FastIO;
#define M 2100000
#define N 300005
int sz[M],cnt;
ll val[M],ans;
int n,m,q,x[N],y[N];
int X[N],Y[N],L[N],R[N];
vector<int> vec[N];
void insert(int k,int l,int r,int p,ll v){
	sz[k]++;
	if (l==r){
		val[k]=v;
		return;
	}
	int mid=(l+r)/2;
	if (p<=mid) insert(k*2,l,mid,p,v);
	else insert(k*2+1,mid+1,r,p,v);
}
ll find(int k,int l,int r,int rk){
	sz[k]--;
	if (l==r) return val[k];
	int mid=(l+r)/2;
	if (sz[k*2]>=rk) return find(k*2,l,mid,rk);
	return find(k*2+1,mid+1,r,rk-sz[k*2]);
}
void calc(int l,int r,int L,int R){
	for (int i=L;i<=R;i++)
		if (y[i]==m) vec[n+1].push_back(i);
		else vec[x[i]].push_back(i);
	for (int i=r;i>=l;i--){
		for (int j=0;j<vec[n+1].size();j++)
			if (x[vec[n+1][j]]>=X[i])
				x[vec[n+1][j]]++;
		for (int j=0;j<vec[X[i]].size();j++)
			if (y[vec[X[i]][j]]>=Y[i])
				y[vec[X[i]][j]]++;
		for (int j=0;j<vec[n+1].size();j++)
			if (x[vec[n+1][j]]>n){
				x[vec[n+1][j]]=X[i];
				y[vec[n+1][j]]=Y[i];
				if (Y[i]!=m){
					vec[X[i]].push_back(vec[n+1][j]);
					vec[n+1][j]=0;
					x[0]=y[0]=-1e9;
				}
			}
		for (int j=0;j<vec[X[i]].size();j++)
			if (y[vec[X[i]][j]]==m){
				vec[n+1].push_back(vec[X[i]][j]);
				vec[X[i]][j]=0; x[0]=y[0]=-1e9;
			}
	}
	vec[n+1].clear();
	for (int i=l;i<=r;i++)
		vec[X[i]].clear();
	for (int i=L;i<=R;i++)
		vec[x[i]].clear();
	//cnt++;
	//if (cnt%100==0)
	//	printf("%d\n",cnt);
}
void calc111(int l,int r){
	for (int i=r;i>=l;i--){
		//printf("%d\n",i);
		for (int j=0;j<vec[n+1].size();j++)
			if (x[vec[n+1][j]]>=X[i])
				x[vec[n+1][j]]++;
		for (int j=0;j<vec[X[i]].size();j++)
			if (y[vec[X[i]][j]]>=Y[i])
				y[vec[X[i]][j]]++;
		for (int j=0;j<vec[n+1].size();j++)
			if (x[vec[n+1][j]]>n){
				x[vec[n+1][j]]=X[i];
				y[vec[n+1][j]]=Y[i];
				if (Y[i]!=m){
					vec[X[i]].push_back(vec[n+1][j]);
					vec[n+1][j]=0;
					x[0]=y[0]=-1e9;
				}
			}
		for (int j=0;j<vec[X[i]].size();j++)
			if (y[vec[X[i]][j]]==m){
				vec[n+1].push_back(vec[X[i]][j]);
				vec[X[i]][j]=0; x[0]=y[0]=-1e9;
			}
		if (y[i]==m) vec[n+1].push_back(i);
		else vec[x[i]].push_back(i);
	}
	vec[n+1].clear();
	for (int i=l;i<=r;i++)
		vec[X[i]].clear();
	//cnt++;
	//if (cnt%100==0)
	//	printf("%d\n",cnt);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	for (int i=1;i<=q;i++)
		x[i]=X[i]=read(),y[i]=Y[i]=read();
	bool fl=1;
	for (int i=1;i<=q;i++)
		if (x[i]!=1) fl=0;
	if (fl){
		int mx=1000000;
		int la=n+m-1;
		for (int i=1;i<=m;i++)
			insert(1,1,mx,i,i);
		for (int i=2;i<=n;i++)
			insert(1,1,mx,i+m-1,1ll*i*m);
		for (int i=1;i<=q;i++){
			write(ans=find(1,1,mx,y[i]));
			insert(1,1,mx,++la,ans);
		}
		return 0;
	}
	//int sz;
	//if (n<=100) sz=n;
	int sz=(n<=1000?n*3:10000);
	int cnt=(q-1)/sz+1;
	for (int i=1;i<=cnt;i++)
		L[i]=R[i-1]+1,R[i]=(i==cnt?q:i*sz);
	for (int i=cnt;i;i--){
		calc111(L[i],R[i]);
		for (int j=i+1;j<=cnt;j++)
			calc(L[i],R[i],L[j],R[j]);
	}
	for (int i=1;i<=q;i++)
		write(1ll*(x[i]-1)*m+y[i]);
}

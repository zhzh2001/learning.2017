#include<ctime>
#include<cstdio>
#include<cstring>
#define min(x,y) ((x)<(y)?(x):(y))
#define N 4100
using namespace std;
int n,m,x,y,z,ans;
int e[15][15],bin[15],bit[N];
int v[N][15],g[N][N],f[2][N][N];
void mn(int &x,int y){
	x=(x<y?x:y);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(e,20,sizeof(e));
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		e[x][y]=e[y][x]=min(e[x][y],z);
	}
	bin[1]=1;
	for (int i=2;i<=n;i++) bin[i]=bin[i-1]*2;
	for (int i=1;i<1<<n;i++)
		for (int j=1;j<=n;j++){
			if (i&bin[j]) continue;
			int tmp=1e8;
			for (int k=1;k<=n;k++)
				if (i&bin[k])
					tmp=min(tmp,e[k][j]);
			v[i][j]=tmp;
		}
	int tmp=(1<<n)-1,ans=(n==1?0:1e9);
	for (int i=1;i<1<<n;i++)
		for (int j=tmp-i;j;j=(j-1)&(tmp-i))
			for (int k=1;k<=n;k++)
				if (j&bin[k]) g[i][j]=min(1e8,g[i][j]+v[i][k]);
	memset(f,60,sizeof(f));
	for (int i=1;i<=n;i++)
		f[0][bin[i]][bin[i]]=0;
	for (int i=1;i<1<<n;i++)
		bit[i]=bit[i/2]+(i&1);
	int fucktree=0;
	for (int i=1;i<n;i++){
		int c=(i&1);
		for (int j=1;j<1<<n;j++)
			for (int k=j;k;k=(k-1)&j) f[c][j][k]=1e9;
		for (int j=1;j<1<<n;j++)
			if (bit[j]>=i)
				for (int k=j;k;k=(k-1)&j)
					if (f[c^1][j][k]<ans)
						for (int l=(tmp-j);l;l=(l-1)&(tmp-j))
							mn(f[c][j|l][l],f[c^1][j][k]+i*g[k][l]);
		for (int j=1;j<1<<n;j++)
			ans=min(ans,f[c][tmp][j]);
	}
	printf("%d",ans);
}

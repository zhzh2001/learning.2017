#include<cstdio>
#define ll long long
using namespace std;
int T,n,h,r,f[1005];
struct P{int x,y,z;}a[1005];
int get(int x){
	return f[x]==x?x:f[x]=get(f[x]);
}
void merge(int x,int y){
	x=get(x); y=get(y);
	if (x!=y) f[x]=y;
}
ll sgr(ll a){
	return a*a;
}
ll dis(P a,P b){
	return sgr(a.x-b.x)+sgr(a.y-b.y)+sgr(a.z-b.z);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1;i<=n+2;i++) f[i]=i;
		for (int i=1;i<=n;i++){
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
			if (get(n+1)==get(n+2)) continue;
			if (a[i].z<=r) merge(i,n+1);
			if (a[i].z>=h-r) merge(i,n+2);
			for (int j=1;j<i;j++)
				if (dis(a[i],a[j])<=4ll*r*r)
					merge(i,j);
		}
		puts(get(n+1)==get(n+2)?"Yes":"No");
	}
}

#include <cstdio>
#include <cstring>

int n,m;
int graph[19][19];
int que[19],head,tail;
int vis[19];
int dis[19];

int bfs(int x){
	memset(vis,0,sizeof(vis));
	que[0]=x;
	head=tail=0;
	int sum=0;
	dis[x]=0;
	vis[x]=true;
	while(head<=tail){
		int p=que[head];
		head++;
		for(int i=1;i<=n;++i){
			if(graph[p][i]!=0x3f3f3f3f && !vis[i]){
//				printf("%d:%d %d\n",x,p,i);
				vis[i]=true;
				que[++tail]=i;
				dis[i]=dis[p]+1;
				sum+=dis[i]*graph[p][i];
			}
		}
	}
	return sum;
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d %d",&n,&m);
	memset(graph,0x3f,sizeof(graph));
	for(int i=0;i<m;++i){
		int x,y,v;
		scanf("%d %d %d",&x,&y,&v);
		if(v<graph[x][y]) graph[x][y]=graph[y][x]=v;
	}
	
//	for(int i=1;i<=n;++i){
//		for(int j=1;j<=n;++j) printf("%12d",graph[i][j]);
//		printf("\n");
//	}
	
	int ans=bfs(1);
//	printf("%5d",ans);
	for(int i=2;i<=n;++i){
		int t=bfs(i);
		if(ans>t) ans=t;
//		printf("%5d",t);
	}
//	printf("\n");
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

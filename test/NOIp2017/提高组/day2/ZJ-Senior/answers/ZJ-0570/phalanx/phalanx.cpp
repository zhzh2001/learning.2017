#include <cmath>
#include <cstdio>

struct quest{
	int x,y;
}que[300009];

struct node{
	int l,r,x,y;
}lst[300009];
int point[10009];

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	int n,m,q;
	scanf("%d %d %d",&n,&m,&q);
	if(n==1){
		int size=sqrt(m);
		int k=size*size==m?size:size+1;
		for(int i=1;i<=m;++i){
			lst[i].l=i-1;
			lst[i].r=i+1;
			lst[i].x=1;
			lst[i].y=i;
		}
		for(int i=1;i<=k;++i) point[i]=(i-1)*size+1;
		int last=m;
		
//		for(int i=1;i<=m;++i) printf("%d %d %d %d\n",lst[i].l,lst[i].r,lst[i].x,lst[i].y);
		
		for(int i=0;i<q;++i){
			int x,y;
			scanf("%d %d",&x,&y);
			int p=point[(y-1)/size+1];
//			printf("%3d",p);
			int t=(y-1)%size;
			for(int i=0;i<t;++i){
				p=lst[p].r;
			}
//			printf("````````````%d\n",p);
			printf("%d\n",p);
			if(p==last) continue;
			t=(y-2)/size+2;
			for(int i=t;i<=k;++i) point[i]=lst[point[i]].r;
			if(point[k]==m+1) point[k]=p;
			lst[lst[p].r].l=lst[p].l;
			lst[lst[p].l].r=lst[p].r;
			lst[p].r=m+1;
			lst[p].l=last;
			lst[last].r=p;
			last=p;
			
//			for(int i=1;i<=m;++i) printf("--%d %d %d %d\n",lst[i].l,lst[i].r,lst[i].x,lst[i].y);
//			for(int i=1;i<=k;++i) printf("~~%d\n",point[i]);
		}
	}else{
		for(int i=0;i<q;++i){
			int x,y;
			scanf("%d %d",&x,&y);
			que[i]=(quest){x,y};
	//		printf("%5d%5d\n",x,y);
			for(int j=i-1;j>=0;--j){
				if(x==n && y==m){
					x=que[j].x;
					y=que[j].y;
				}else{
					if(y==m && x>=que[j].x) x++;
					if(x==que[j].x && y>=que[j].y) y++;
				}
	//			printf("%5d%5d\n",x,y);
			}
			printf("%d\n",(x-1)*m+y);
		}
	}
	
	fclose(stdin);
	fclose(stdout);
	return 0;
}

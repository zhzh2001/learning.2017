#include <cstdio>
#include <cstring>
#include <vector>
using namespace std;

typedef long long ll;
//vector<int> e[1009];

struct edge{
	int q;
	int next;
}e[2000009];
int tot,head[1009];

int vis[1009];
struct node{int x,y,z;}ball[1009];

void link(int a,int b){
	e[tot].q=b;
	e[tot].next=head[a];
	head[a]=tot;
	++tot;
	e[tot].q=a;
	e[tot].next=head[b];
	head[b]=tot;
	++tot;
}

bool comp(ll r,ll ix,ll iy,ll iz,ll jx,ll jy,ll jz){
	int tmp1[19],top1=-1;
	int tmp2[19],top2=-1;
	memset(tmp1,0,sizeof(tmp1));
	memset(tmp2,0,sizeof(tmp2));
	ll qd=r*r*4;
	while(qd){
		tmp1[++top1]=qd%1000000;
		qd/=1000000;
	}
//	for(int i=top1;i>=0;--i) printf("%d",tmp1[i]);
//	printf("!\n");
	int ttop;
	ll q;
	q=(ix-jx)*(ix-jx);
	ttop=-1;
	while(q){
		tmp2[++ttop]+=q%1000000;
		q/=1000000;
	}
	if(ttop>top2) top2=ttop;
	
	q=(iy-jy)*(iy-jy);
	ttop=-1;
	while(q){
		tmp2[++ttop]+=q%1000000;
		q/=1000000;
	}
	if(ttop>top2) top2=ttop;
	
	q=(iz-jz)*(iz-jz);
	ttop=-1;
	while(q){
		tmp2[++ttop]+=q%1000000;
		q/=1000000;
	}
	if(ttop>top2) top2=ttop;
	
	
	for(int i=0;i<=top2;++i){
		tmp2[i+1]+=tmp2[i]/1000000;
		tmp2[i]%=1000000;
	}
	if(tmp2[top2+1]) top2++;
	
//	for(int i=0;i<=top2;++i) printf("%d",tmp2[i]);
//	printf("!!\n");
	
	if(top1>top2) return true;
	if(top1<top2) return false;
	for(int i=top1;i>=0;--i){
		if(tmp1[i]>tmp2[i]) return true;
		if(tmp1[i]<tmp2[i]) return false;
	}
	return true;
}

bool dfs(int now,int goal){
	vis[now]=true;
	if(now==goal) return true;
	for(int i=head[now];i!=-1;i=e[i].next){
		if(!vis[e[i].q]){
			if(dfs(e[i].q,goal)) return true;
		}
	}
	return false;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
		int n,h,r;
		scanf("%d %d %d",&n,&h,&r);
		for(int i=1;i<=n;++i) scanf("%d %d %d",&ball[i].x,&ball[i].y,&ball[i].z);
		memset(vis,0,sizeof(vis));
		memset(head,-1,sizeof(head));
		tot=0;
//		for(int i=0;i<=n+1;++i) e[i].clear();
		for(int i=1;i<=n;++i){
			for(int j=i+1;j<=n;++j){
				if(comp(r,ball[i].x,ball[i].y,ball[i].z,ball[j].x,ball[j].y,ball[j].z)){
					link(i,j);
//					e[i].push_back(j);
//					e[j].push_back(i);
				}
			}
		}
		for(int i=1;i<=n;++i){
			if(ball[i].z-r<=0){
				link(0,i);
			}
			if(ball[i].z+r>=h){
				link(i,n+1);
			}
		}
		
//		for(int i=0;i<=n+1;++i){
//			for(int j=0;j<e[i].size();++j){
//				printf("%d,%d ",i,e[i][j]);
//			}
//			printf("\n");
//		}
		
		if(dfs(0,n+1)) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
#define MAXN 15
using namespace std;
int mis[MAXN][MAXN],jl[MAXN][MAXN];//mis[0][i]=2147483647;jl[0][k]=1;jl[0][i]=2147483647
int ph[MAXN][MAXN];
bool ach[MAXN];
int pts,n,m;
int ans=2147483647,cost=0;

bool ava(int u){return mis[pts][u]<2147483644;}
bool ava(int a,int b){return a!=b&&ph[a][b]<2147483644;}
void search(int p)
{
	if(cost>=ans)return;
	ach[p]=true;
	if(++pts==n)ans=cost;
	for(int i=1;i<=n;i++)
	{
		jl[pts][i]=ava(p,i)?min(jl[pts-1][i],jl[pts-1][p]+1):jl[pts-1][i];
		mis[pts][i]=ava(p,i)?min(mis[pts-1][i],jl[pts-1][p]*ph[p][i]):mis[pts-1][i];
	}
	for(int i=1;i<=n;i++)
	if(ava(i)&&!ach[i])
	{
		cost+=mis[pts][i];
		search(i);
		cost-=mis[pts][i];
	}
	pts--;
	ach[p]=false;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	for(int j=1;j<=n;j++)
	ph[i][j]=2147483647;
	for(int i=1;i<=m;i++)
	{
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		ph[a][b]=min(ph[a][b],c);
		ph[b][a]=min(ph[b][a],c);
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++) mis[0][j]=2147483647,jl[0][j]=10000;
		jl[0][i]=1;
		mis[0][i]=0;
		search(i);
	}
	printf("%d",ans);
}

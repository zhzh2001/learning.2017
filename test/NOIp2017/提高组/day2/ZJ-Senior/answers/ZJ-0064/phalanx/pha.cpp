#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define N 600005
#define ll long long
int Q,n,m,k,l,t,s,ans,x,y;
int head[N],next[N],down[N],a[N],b[N],pre[N],up[N];
struct node{
	ll id;
	int x,y;
}p[N],qq[N];
struct que{
	int x,y;
}q[N];
int main(){
	scanf("%d%d%d",&n,&m,&Q);
	for (int i=1;i<=Q;i++){
		scanf("%d%d",&q[i].x,&q[i].y);
		qq[i].x=q[i].x,qq[i].y=q[i].y;
		qq[i].id=i;
	}
	if(Q<=500){
		for (int i=1;i<Q;i++)
			for (int j=Q;j>i;j--)
				if(qq[j].y<qq[j-1].y)swap(qq[j],qq[j-1]);
		for (int i=1;i<=Q;i++){
			s=q[i].y;
			for (int j=1;j<=Q;j++)
				if(qq[j].id<i&&q[i].x==qq[j].x&&qq[j].y<=q[i].y)s++;
			if(s<m)p[++k]=(node){(q[i].x-1)*m+s,q[i].x,s};
		}
		for (int i=1;i<k;i++)
			for (int j=k;j>i;j--)
				if(p[j].y<p[j-1].y)swap(p[j],p[j-1]);
		l=k;
		for (int i=1;i<=n;i++){
			p[++k]=(node){(ll)m*i,i,m};
			if(i==1)up[k]=0;else up[k]=k-1;
			next[k]=0;down[k]=k+1;
			head[i]=k;
		}down[k]=k+1;up[k+1]=k;pre[k+2]=k;next[k]=k+2;
		for (int i=1;i<=l;i++){
			pre[head[p[i].x]]=i;
			next[i]=head[p[i].x];
			head[p[i].x]=i;
		}
		for (int i=1;i<=Q;i++){
			int x=head[q[i].x];
			while(p[x].y!=q[i].y)x=next[x];
			printf("%d\n",p[x].id);
			if(p[x].x==n&&p[x].y==m)continue;
			next[pre[x]]=next[x];pre[next[x]]=pre[x];
			down[up[x]]=down[x];up[down[x]]=up[x];
			int t=x;
			while(next[t]>0&&next[t]<=k){
				t=next[t];
				p[t].y--;
			}
			next[pre[x]]=down[x];pre[next[x]]=down[x];
			down[up[x]]=down[x];up[down[x]]=up[x];

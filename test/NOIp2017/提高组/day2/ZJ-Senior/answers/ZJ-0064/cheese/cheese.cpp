#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 1005
#define ll long long
int n,m,T;
ll x[N],y[N],z[N],h,r;
int a[N][N],flag[N];
ll sqr(ll x){return x*x;}
void dfs(int u){
	flag[u]=1;
	for (int v=1;v<=n+1;v++)
		if(a[u][v]&&!flag[v])dfs(v);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=0;i<=n+1;i++)
			for (int j=0;j<=n+1;j++)a[i][j]=0;
		memset(flag,0,sizeof(flag));
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if(z[i]-r<=h&&z[i]+r>=h)a[i][n+1]=1;
			if(z[i]-r<=0&&z[i]+r>=0)a[0][i]=1;
		}
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++)
				if(r*r*4>=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))a[i][j]=a[j][i]=1;
		dfs(0);
		if(flag[n+1])puts("Yes");
		else puts("No");
	}
}

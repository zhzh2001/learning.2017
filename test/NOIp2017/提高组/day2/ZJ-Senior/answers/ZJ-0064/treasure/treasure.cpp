#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
#define N 13
#define INF 1000000000
int n,m,k,l,t,s,ans,x,y,z;
int a[N][N],flag[N],f[N][1<<12][N],g[1<<12],low[1<<12];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	for (int i=1;i<(1<<12);i++){
		g[i]=g[i>>1]+(i&1);
		if(i&1)low[i]=1;
		else low[i]=low[i>>1]+1;
	}
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)a[i][j]=INF;
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=min(a[x][y],z);
		a[y][x]=min(a[y][x],z);
	}
	for (int i=1;i<=n;i++)
		for (int j=0;j<(1<<n);j++)
			for (int k=0;k<=n;k++)f[i][j][k]=INF;
	for (int i=1;i<=n;i++)
		for (int k=0;k<=n;k++)f[i][1<<(i-1)][k]=0;
	for (int sta=1;sta<(1<<n);sta++)
		for (int x=sta&(sta-1);x;x=sta&(x-1)){
			int y=sta^x;
			for (int k=0;k<=n-g[sta];k++)
			for (int i=x;i;i-=i&(-i))if(f[low[i]][x][k]<INF)
				for (int j=y;j;j-=j&(-j)){
					int u=low[i],v=low[j];
					if(a[u][v]<INF)
						f[u][sta][k]=min(f[u][sta][k],f[u][x][k]+f[v][y][k+1]+a[u][v]*(k+1));
				}
		}
	ans=INF;
	for (int i=1;i<=n;i++)ans=min(ans,f[i][(1<<n)-1][0]);
	printf("%d\n",ans);
}

#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
int a[1001][1001],d[300001],m,n,f[300001],tot,g[300001],tot1;
void add(int p){
	for (;p<=m;p+=p&-p) ++d[p];
}
int sum(int p){
	int ans=0;
	for (;p;p-=p&-p) ans+=d[p];
	return ans;
}
void read(int& x){
	x=0;
	int y=1;
	char c=getchar();
	while (c<'0'||c>'9'){
		if (c=='-') y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	x=x*y;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int i,q,x,y,j,t;
	read(n);
	read(m);
	read(q);
	if (n<=1000&&m<=1000){
		for (i=1;i<=n;++i)
			for (j=1;j<=m;++j) a[i][j]=(i-1)*m+j;
		for (i=1;i<=q;++i){
			read(x);
			read(y);
			t=a[x][y];
			printf("%d\n",t);
			for (j=y;j<m;++j) a[x][j]=a[x][j+1];
			for (j=x;j<n;++j) a[j][m]=a[j+1][m];
			a[n][m]=t;
		}
	}else if (n==1){
		for (i=1;i<=q;++i){
			read(x);
			read(y);
			if (y>m-tot){
				t=f[y+tot-n];
				printf("%d\n",t);
				for (j=y+tot-n;j<tot;++j) f[j]=f[j+1];
				f[tot]=t;
			}else{
				t=sum(y);
				add(y);
				t=y+t;
				f[++tot]=t;
				printf("%d\n",t);
			}
		}
	}else{
		for (i=1;i<=q;++i){
			read(x);
			read(y);
			if (y>m-tot){
				t=f[y+tot-n];
				printf("%d\n",t);
				for (j=y+tot-n;j<tot;++j) f[j]=f[j+1];
				if (tot<n) f[tot]=(tot+1)*m;
				else f[tot]=g[tot-n];
				g[++tot1]=t;
			}else{
				t=sum(y);
				add(y);
				t=y+t;
				++tot;
				if (tot<n) f[tot]=(tot+1)*m;
				else f[tot]=g[tot-n];
				g[++tot1]=t;
				printf("%d\n",t);
			}
		}
	}
	return 0;
}

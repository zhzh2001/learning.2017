#include <iostream>
#include <cstdio>
#include <cmath>
using namespace std;
int x[1002],y[1002],z[1002],p[1002];
int bcj(int x){
	if (p[x]==x) return x;
	p[x]=bcj(p[x]);
	return p[x];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int i,t,n,h,r,j,d,a,b;
	double dis;
	scanf("%d",&t);
	while (t--){
		scanf("%d%d%d",&n,&h,&r);
		d=r<<1;
		for (i=1;i<=n;++i){
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			p[i]=i;
		}
		p[0]=0;
		p[n+1]=n+1;
		for (i=1;i<n;++i){
			for (j=i+1;j<=n;++j){
				a=bcj(i);
				b=bcj(j);
				//printf("i=%d j=%d a=%d b=%d\n",i,j,a,b);
				if (a!=b){
					dis=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
					if (dis<=d) p[a]=b;
					//printf("dis=%lf\n",dis);
				}
			}
		}
		for (i=1;i<=n;++i){
			a=bcj(i);
			b=bcj(0);
			if (a!=b){
				if (z[i]<=r) p[a]=b;
				//printf("i=%d z=%d r=%d b=%d\n",i,z[i],r,b);
			}
		}
		//for (i=1;i<=n;++i) printf("p[%d]=%d\n",i,p[i]);
		for (i=1;i<=n;++i){
			a=bcj(i);
			b=bcj(n+1);
			if (a!=b){
				if (h-z[i]<=r) p[a]=b;
				//printf("i=%d\n",i);
			}
		}
		//printf("%d %d\n",p[0],p[n+1]);
		if (bcj(0)==bcj(n)) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstring>
#include <queue>
using namespace std;
int d[20],dis[20][20],vis[20],n,ans=0x3f3f3f3f;
void dfs(int x,int an,int dpt){
	int i,flag=0,j;
	if (an>=ans) return;
	vis[x]=1;
	d[x]=dpt;
	for (i=1;i<=n;++i){
		if (!vis[i]){
			flag=1;
			break;
		}
	}
	if (!flag){
		ans=an>ans?ans:an;
		vis[x]=0;
//		if (an==445){
//			for (i=1;i<=n;++i) printf("f[%d]=%d\n",i,f[i]);
//		}
		return;
	}
	for (i=1;i<=n;++i){
		if (vis[i]){
			for (j=1;j<=n;++j){
				if (!vis[j]) dfs(j,an+(d[i]+1)*dis[i][j],d[i]+1);
			}
		}
	}
	vis[x]=0;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,m,di,a,b,nd,j,l,ansn,md;
	scanf("%d%d",&n,&m);
	memset(dis,0x3f,sizeof(dis));
	for (i=1;i<=m;++i){
		scanf("%d%d%d",&a,&b,&di);
		if (dis[a][b]>di) dis[a][b]=dis[b][a]=di;
	}
	for (i=1;i<=n;++i){
		memset(vis,0,sizeof(vis));
		dfs(i,0,0);
	}
	printf("%d\n",ans);
	return 0;
}

#include<bits/stdc++.h>
#define LL long long
using namespace std;
int n,m,q,x,y;
LL mp[1101][1101];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=n;i++) 
	  for (int j=1;j<=m;j++)
	    mp[i][j]=(i-1)*m+j;
	while (q--){
		scanf("%d%d",&x,&y);
		printf("%lld\n",mp[x][y]);
		LL t=mp[x][y];
		for (int i=y;i<m;i++) mp[x][i]=mp[x][i+1];
		for (int i=x;i<n;i++) mp[i][m]=mp[i+1][m];
		mp[n][m]=t;
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n,m,ans,x,y,z,f[15][15],a[15],b[15],v[15];
void solve(int k){
	if (k==n+1){
		int tmp=0,temp;
		memset(b,0,sizeof(b));
		for (int i=2;i<=n;i++){
			temp=2139062143;
		    for (int j=1;j<i;j++){  
		  	  if (f[a[j]][a[i]]!=2139062143)
		        if (f[a[j]][a[i]]*(b[a[j]]+1)<temp) temp=f[a[j]][a[i]]*(b[a[j]]+1),b[a[i]]=b[a[j]]+1; 
		  }
		  tmp+=temp;
		}
		ans=min(ans,tmp);
		return;
	}
	for (int i=1;i<=n;i++)
	  if (!v[i]){
	  	  a[k]=i;v[i]=1;
		  solve(k+1);
		  v[i]=0;
	  }
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,127,sizeof(f));
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		f[x][y]=min(f[x][y],z);f[y][x]=f[x][y];
	}
	ans=2139062143;
	solve(1);
	printf("%d\n",ans);
	return 0;
}

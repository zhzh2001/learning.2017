#include<bits/stdc++.h>
#define LL long long
using namespace std;
int T,n,tot,tto[2100001],nextt[2100001],head[1101];
LL h,r,x[1101],y[1101],z[1101];
bool vis[1101];
void add(int a,int b){
	tto[++tot]=b;
	nextt[tot]=head[a];
	head[a]=tot;
}
LL get_dis(int a,int b){
	return (x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]);
}
bool spfa(){
	memset(vis,0,sizeof(vis));
	queue<int> q;
	q.push(0);vis[0]=1;
	while (!q.empty())
	{
		int x=q.front();q.pop();
		for (int i=head[x];i;i=nextt[i]){
			int u=tto[i];
			if (u==n+1) return 1;
			if (!vis[u]) q.push(u),vis[u]=1;
		}
	}
	return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		tot=0;memset(head,0,sizeof(head));
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=1;i<=n;i++)
		  for (int j=1;j<=n;j++)
		    if (i!=j&&get_dis(i,j)<=4*r*r) add(i,j),add(j,i);
		for (int i=1;i<=n;i++) if (z[i]<=r) add(0,i);
		for (int i=1;i<=n;i++) if (z[i]+r>=h) add(i,n+1);
		if (spfa()) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

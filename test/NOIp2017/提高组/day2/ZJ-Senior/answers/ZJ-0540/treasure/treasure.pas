const
  vv=1<<29;
var
  n,m,i,j,k,t,num,ans,u,v,l:longint;
  next,vet,val:array[1..30000] of longint;
  head:array[1..500] of longint;
  flag:array[1..500] of boolean;
  stu:array[0..500] of longint;
  a:array[1..12,1..12] of longint;
function min(x,y:longint):longint;
begin
  if x<y then exit(x)
         else exit(y);
end;
function get(u,x:longint):longint;//u���x��
begin
  exit((u-1)*(n+1)+x+1);
end;
function fan(x:longint):longint;
begin
  exit((x+n) div (n+1));
end;
procedure add(u,v,l:longint);
begin
  num:=num+1;
  next[num]:=head[u];
  head[u]:=num;
  vet[num]:=v;
  val[num]:=l;
end;
procedure dfs1(u,x,k,s:longint);
var
  a,e,v,l:longint;
begin
  stu[k]:=min(stu[k],s);
  flag[u]:=true;
  a:=get(u,x);
  e:=head[a];
  while e<>-1 do
    begin
    v:=vet[e];
    l:=val[e];
    if flag[fan(v)]=false then dfs1(fan(v),x+1,k+(1<<(fan(v)-1)),s+l);
    e:=next[e];
    end;
  flag[u]:=false;
end;
function dfs(u,x:longint):longint;
var
  a,v,l,e:longint;
begin
  dfs:=0;
  flag[u]:=true;
  a:=get(u,x);
  e:=head[a];
  while e<>-1 do
    begin
    v:=vet[e];
    l:=val[e];
    if flag[fan(v)]=false then dfs:=dfs+l+dfs(fan(v),x+1);
    e:=next[e];
    end;
  flag[u]:=false;
end;
begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  for i:=1 to get(n,n) do
    head[i]:=-1;
  for i:=1 to n do
    for j:=1 to n do
      a[i,j]:=vv;
  for i:=1 to n do
    a[i,i]:=0;
  for i:=1 to m do
    begin
    readln(u,v,l);
    a[u,v]:=min(a[u,v],l);
    a[v,u]:=min(a[v,u],l);
    end;
  for i:=1 to n do
    for j:=i+1 to n do
      begin
      if a[i,j]<>vv then
        for k:=0 to n-1 do
          begin
          add(get(i,k),get(j,k+1),a[i,j]*k);
          add(get(j,k),get(i,k+1),a[i,j]*k);
          end;
      end;//��ͼ
  ans:=vv;
  for i:=1 to n do
    flag[i]:=false;
  if m=n-1 then
    for i:=1 to n do
      ans:=min(dfs(i,1),ans)
           else
    for i:=1 to n do
    begin
    for j:=0 to (1<<n)-1 do
      stu[j]:=vv;
    dfs1(i,1,1<<(i-1),0);
    for t:=1 to (1<<n)-1 do
      for j:=1 to (1<<n)-1 do
        for k:=j+1 to (1<<n)-1 do
          begin
          stu[j or k]:=min(stu[j or k],stu[j]+stu[k]);
          end;
    ans:=min(ans,stu[(1<<n)-1]);
    end;
  write(ans);

  close(input);
  close(output);
end.

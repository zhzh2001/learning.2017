#include <bits/stdc++.h>
#define fi first
#define se second
#define lowbit(x) ((x)&-(x))
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>void pt(T x)
{
	if(!x)
	{
		putchar('0');
		return;
	}
	static char stk[65];
	int tp=0;
	for(;x;x/=10)stk[tp++]=x%10^48;
	per(i,0,tp)putchar(stk[i]);
}
template<class T>inline void ptn(T x)
{
	pt(x),putchar('\n');
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


const int N=12,M=525000;

int n,U,cost[N][N],dp[N][M],tot,cnt,refer[1<<N][1<<N],trans[1<<N][1<<N],arr[N],id[1<<N|5];
pii dat[M];

inline void check_Min(int &a,int b)
{
	if(!~a||b<a)a=b;
}

inline void convert(int mask)
{
	for(cnt=0;mask;mask^=lowbit(mask))arr[cnt++]=id[lowbit(mask)];
}

void init()
{
	rep(i,0,n)
	{
		refer[1<<i][1<<i]=tot;
		dat[tot++]=pii(1<<i,1<<i);
	}
	rep(mask,1,1<<n)for(int sub=(mask-1)&mask;sub;sub=(sub-1)&mask)
	{
		refer[mask][sub]=tot;
		dat[tot++]=pii(mask,sub);
	}
	
	rep(i,0,n)id[1<<i]=i;
	rep(mask1,0,1<<n)
	{
		convert(mask1);
		for(int rem=U^mask1,mask2=rem;mask2;mask2=(mask2-1)&rem)
		{
			int sum=0;
			bool flag=true;
			for(int tmp=mask2;tmp;tmp^=lowbit(tmp))
			{
				int cur=id[lowbit(tmp)],mn=-1;
				rep(i,0,cnt)if(~cost[arr[i]][cur])check_Min(mn,cost[arr[i]][cur]);
				if(!~mn)
				{
					flag=false;
					break;
				}
				sum+=mn;
			}
			if(flag)trans[mask1][mask2]=sum;
			else trans[mask1][mask2]=-1;
		}
	}
}

int main()
{
	
//	freopen("treasure1.in","r",stdin);//
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	int m;
	cin>>n>>m;
	U=(1<<n)-1;
	memset(cost,-1,sizeof cost);
	for(int u,v,val;m--;)
	{
		rd(u),rd(v),rd(val);
		--u,--v;
		check_Min(cost[u][v],val);
		cost[v][u]=cost[u][v];
	}
	init();
	memset(dp,-1,sizeof dp);
	rep(i,0,n)dp[0][refer[1<<i][1<<i]]=0;
	int ans=INT_MAX;
	
	rep(i,0,n-1)rep(j,0,tot)if(~dp[i][j]&&dp[i][j]<=ans)
	{
		int mask1=dat[j].fi,mask2=dat[j].se;
		if(mask1==U)
		{
			Min(ans,dp[i][j]);
			continue;
		}
		for(int rem=U^mask1,sub=rem;sub;sub=(sub-1)&rem)if(~trans[mask2][sub])
			check_Min(dp[i+1][refer[mask1|sub][sub]],dp[i][j]+(i+1)*trans[mask2][sub]);
	}
	
	cout<<ans<<endl;
	return 0;
	
}


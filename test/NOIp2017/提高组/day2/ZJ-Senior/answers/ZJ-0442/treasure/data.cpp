#include <bits/stdc++.h>
#define fi first
#define se second
#define lowbit(x) ((x)&-(x))
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>void pt(T x)
{
	if(!x)
	{
		putchar('0');
		return;
	}
	static char stk[65];
	int tp=0;
	for(;x;x/=10)stk[tp++]=x%10^48;
	per(i,0,tp)putchar(stk[i]);
}
template<class T>inline void ptn(T x)
{
	pt(x),putchar('\n');
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


int main()
{
	srand(time(NULL));
	freopen("treasure.in","w",stdout);
	int n=12,m=n*(n-1)/2;
	printf("%d %d\n",n,m);
	rep(i,1,n+1)rep(j,i+1,n+1)printf("%d %d %d\n",i,j,1);
}


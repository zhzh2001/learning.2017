#include <bits/stdc++.h>
#define fi first
#define se second
#define lowbit(x) ((x)&-(x))
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>void pt(T x)
{
	if(!x)
	{
		putchar('0');
		return;
	}
	static char stk[65];
	int tp=0;
	for(;x;x/=10)stk[tp++]=x%10^48;
	per(i,0,tp)putchar(stk[i]);
}
template<class T>inline void ptn(T x)
{
	pt(x),putchar('\n');
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


const int N=(int)1e3+5;

int n,H,R;
ll D;
bool adj[N][N],vis[N];

struct Point
{
	int x,y,z;
	inline void input()
	{
		scanf("%d%d%d",&x,&y,&z);
	}
}
dat[N];

inline ll sqr(int x)
{
	return (ll)x*x;
}

inline ll dist(Point &A,Point &B)
{
	return sqr(A.x-B.x)+sqr(A.y-B.y)+sqr(A.z-B.z);
}

void bfs()
{
	rep(i,0,n+2)vis[i]=false;
	int L=0,R=0;
	static int que[N];
	for(vis[que[R++]=0]=true;L<R;)
	{
		int u=que[L++];
		rep(i,0,n+2)if(adj[u][i]&&!vis[i])vis[que[R++]=i]=true;
	}
}

void solve()
{
	cin>>n>>H>>R;
	D=(ll)R*R<<2;
	rep(i,1,n+1)dat[i].input();
	rep(i,0,n+2)rep(j,0,n+2)adj[i][j]=false;
	rep(i,1,n+1)
	{
		adj[i][0]=adj[0][i]=abs(dat[i].z)<=R;
		adj[n+1][i]=adj[i][n+1]=abs(dat[i].z-H)<=R;
	}
	rep(i,1,n+1)rep(j,i+1,n+1)adj[j][i]=adj[i][j]=dist(dat[i],dat[j])<=D;
	bfs();
	puts(vis[n+1]?"Yes":"No");
}

int main()
{
	
//	freopen("cheese1.in","r",stdin);//
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	int cas;
	for(cin>>cas;cas--;)solve();
	return 0;
	
}


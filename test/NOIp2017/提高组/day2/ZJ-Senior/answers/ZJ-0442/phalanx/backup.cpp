#include <bits/stdc++.h>
#define fi first
#define se second
#define lowbit(x) ((x)&-(x))
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>void pt(T x)
{
	if(!x)
	{
		putchar('0');
		return;
	}
	static char stk[65];
	int tp=0;
	for(;x;x/=10)stk[tp++]=x%10^48;
	per(i,0,tp)putchar(stk[i]);
}
template<class T>inline void ptn(T x)
{
	pt(x),putchar('\n');
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


const int N=(int)3e5+5;

int n,m,lim,bit[N];
pii pos[N];

struct Ope
{
	int id,x,y;
	Ope(){}
	Ope(int id,int x,int y):id(id),x(x),y(y){}
}
ope[N<<1],upd[N],query[N];

inline ll id(int x,int y)
{
	return (ll)(x-1)*m+y;
}

inline bool cmp(const Ope &A,const Ope &B)
{
	return A.x<B.x;
}

void add_suf(int x,int val)
{
	for(;x<=lim;x+=lowbit(x))bit[x]+=val;
}

void add(int L,int R,int val)
{
	add_suf(L,val);
	add_suf(R+1,-val);
}

int bit_query(int x)
{
	int res=0;
	for(;x;x-=lowbit(x))res+=bit[x];
	return res;
}

void conquer(int L,int mid,int R)
{
	
	int tot_upd=0,tot_query=0;
	rep(i,L,mid+1)if(!~ope[i].id)upd[tot_upd++]=ope[i];
	rep(i,mid+1,R+1)if(~ope[i].id)query[tot_query++]=ope[i];
	
	sort(upd,upd+tot_upd,cmp);
	sort(query,query+tot_query,cmp);
	int ptr_upd=0,ptr_query=0;
	while(ptr_upd<tot_upd)
	{
		int l=ptr_upd,r=l;
		add(upd[l].y,m-1,1);
		while(r+1<tot_upd&&upd[r+1].x==upd[l].x)add(upd[++r].y,m-1,1);
		while(ptr_query<tot_query&&query[ptr_query].x==upd[l].x)
		{
			Ope &tar=query[ptr_query];
			pos[tar.id].se+=bit_query(tar.y);
			++ptr_query;
		}
		rep(i,l,r+1)add(upd[i].y,m-1,-1);
		ptr_upd=r+1;
	}
	
	int sum_x=0,sum_y=0;	
	rep(i,0,tot_upd)
	{
		add(upd[i].x,n-1,1);
		sum_x-=n-upd[i].x;
		sum_y-=m-upd[i].y;
	}
	rep(i,0,tot_query)
	{
		Ope &tar=query[i];
		if(tar.y==m)pos[tar.id].fi+=bit_query(tar.x);
		if(tar.x==n&&tar.y==m)pos[tar.id].fi+=sum_x,pos[tar.id].se+=sum_y;
	}
	rep(i,0,tot_upd)add(upd[i].x,n-1,-1);
	
}

void divide(int L,int R)
{
	if(L>=R)return;
	int mid=L+R>>1;
	conquer(L,mid,R);
	divide(L,mid);
	divide(mid+1,R);
}

int main()
{
	
//	freopen("phalanx2.in","r",stdin);
//	freopen("phalanx.in","r",stdin);
//	freopen("phalanx.out","w",stdout);
	
	int q;
	rd(n),rd(m),rd(q);
	lim=max(n,m);
	
	int tot=0;
	rep(i,0,q)
	{
		static int x,y;
		rd(x),rd(y);
		pos[i]=pii(x,y);
		ope[tot++]=Ope(i,x,y);
		ope[tot++]=Ope(-1,x,y);
	}
	
	divide(0,tot-1);
	
	rep(i,0,q)ptn(id(pos[i].fi,pos[i].se));
	return 0;
	
}


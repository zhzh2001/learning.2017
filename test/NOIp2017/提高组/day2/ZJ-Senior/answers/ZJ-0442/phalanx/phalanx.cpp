#include <bits/stdc++.h>
#define fi first
#define se second
#define lowbit(x) ((x)&-(x))
#define siz(x) ((int)(x).size())
#define debug(x) cerr<<#x<<" = "<<(x)<<endl
#define rep(i,s,t) for(int i=(s),_t=(t);i<_t;++i)
#define per(i,s,t) for(int i=(t)-1,_s=(s);i>=_s;--i)
using namespace std;
typedef long long ll;
typedef double db;
typedef pair<int,int> pii;
template<class T>void rd(T &x)
{
	x=0;
	static char c;
	while(c=getchar(),c<48);
	do x=x*10+(c^48);
		while(c=getchar(),c>47);
}
template<class T>void pt(T x)
{
	if(!x)
	{
		putchar('0');
		return;
	}
	static char stk[65];
	int tp=0;
	for(;x;x/=10)stk[tp++]=x%10^48;
	per(i,0,tp)putchar(stk[i]);
}
template<class T>inline void ptn(T x)
{
	pt(x),putchar('\n');
}
template<class T>inline void Max(T &a,T b)
{
	if(b>a)a=b;
}
template<class T>inline void Min(T &a,T b)
{
	if(b<a)a=b;
}
// EOT


const int N=(int)3e5+5;

int n,m,q;

inline ll id(int x,int y)
{
	return (ll)(x-1)*m+y;
}

namespace solver1
{
	
	const int Q=505;
	
	pii query[Q];
	
	void solve()
	{
		rep(i,0,q)
		{
			static int x,y;
			rd(x),rd(y);
			query[i]=pii(x,y);
			per(j,0,i)
			{
				if(x==query[j].fi&&y<m&&y>=query[j].se)++y;
				else if(y==m&&x<n&&x>=query[j].fi)++x;
				else if(x==n&&y==m)x=query[j].fi,y=query[j].se;
			}
			ptn(id(x,y));
		}
	}
	
}

namespace solver2
{
	
	int arr[N<<1],bit[N];
	
	void add(int x,int val)
	{
		for(;x<(N<<1);x+=lowbit(x))bit[x]+=val;
	}
	
	int bit_query(int x)
	{
		int res=0;
		for(;x;x-=lowbit(x))res+=bit[x];
		return res;
	}
	
	int query(int L,int R)
	{
		return bit_query(R)-bit_query(L-1);
	}
	
	int calc(int x)
	{
		int res;
		for(int L=1,R=(N<<1)-1;L<=R;)
		{
			int mid=L+R>>1;
			if(query(1,mid)<=x)
			{
				res=mid;
				L=mid+1;
			}
			else R=mid-1;
		}
		return arr[res];
	}
	
	void solve()
	{
		rep(i,1,m+1)arr[i]=i,add(i,1);
		int lst=m;
		rep(i,0,q)
		{
			int x,y,ans;
			rd(x),rd(y);
			ptn(ans=calc(y));
			add(y,-1);
			add(++lst,1);
			arr[lst]=ans;
		}
	}
	
}

int main()
{
	
//	freopen("phalanx2.in","r",stdin);//
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	rd(n),rd(m),rd(q);
	if(q<=500)solver1::solve();
	else solver2::solve();
	
	return 0;
	
}


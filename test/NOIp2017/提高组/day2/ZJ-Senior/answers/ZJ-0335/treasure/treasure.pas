var
n,m,max,x,kk,y,z,i:longint;
jl,iff:array[0..100,0..100] of longint;

function pp(start:longint):longint;
var
xk,i,j:longint;
d,had,used:array[0..100] of longint;
begin
fillchar(had,sizeof(had),0);
fillchar(used,sizeof(used),0);
pp:=0;
for i:=1 to n do
begin had[i]:=maxlongint; d[i]:=maxlongint; end;
d[start]:=0;
used[start]:=1;
had[start]:=1;

for i:=2 to n do
begin
for j:=1 to n do
if (iff[start,j]=1)and(used[j]=0) then
if (d[j]>jl[start,j]*had[start])or((d[j]=jl[start,j]*had[start])and(had[start]+1<had[j])) then
begin d[j]:=jl[start,j]*had[start]; had[j]:=had[start]+1; end;

for j:=1 to n do
if used[j]=0 then  xk:=j;

for j:=1 to n do
if (d[j]<d[xk])and(used[j]=0) then xk:=j;

start:=xk;
used[xk]:=1;
pp:=pp+d[start];
end;
exit(pp);
end;


begin
assign(input,'treasure.in');
assign(output,'treasure.out');
reset(input);
rewrite(output);
readln(n,m);
max:=maxlongint;
fillchar(jl,sizeof(jl),0);
fillchar(iff,sizeof(iff),0);
for i:=1 to m do
begin
readln(x,y,z);
if (jl[x,y]>z)or(iff[x,y]=0) then jl[x,y]:=z;
if (jl[y,x]>z)or(iff[y,x]=0) then jl[y,x]:=z;
iff[x,y]:=1;
iff[y,x]:=1;
end;

for i:=1 to n do
begin kk:=pp(i);if kk<max then max:=kk; end;
writeln(max);
close(input);
close(output);
end.

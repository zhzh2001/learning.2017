var
x,y,z,sl,cb,q,used:array[1..10000] of longint;
iff:array[1..1000,1..1000] of longint;
n,i,j,t,k,num,tail:longint;
h,r,da,la:int64;
fff:boolean;
begin
assign(input,'cheese.in');
assign(output,'cheese.out');
reset(input);
rewrite(output);
readln(t);
for k:=1 to t do
begin
readln(n,h,r);
fillchar(used,sizeof(used),0);
fillchar(x,sizeof(x),0);
fillchar(y,sizeof(y),0);
fillchar(z,sizeof(z),0);
fillchar(sl,sizeof(sl),0);
fillchar(cb,sizeof(cb),0);
fillchar(q,sizeof(q),0);
fillchar(iff,sizeof(iff),0);
num:=0;
tail:=0;
la:=4*r*r;
for i:=1 to n do
begin
readln(x[i],y[i],z[i]);
if z[i]<=r then begin inc(tail);used[tail]:=1; q[tail]:=i; end;
if z[i]+r>=h then sl[i]:=1;
for j:=1 to i-1 do
begin
da:=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
if da<=la then
begin  inc(cb[i]); iff[i,cb[i]]:=j; inc(cb[j]); iff[j,cb[j]]:=i; end; end;
end;
num:=1;
fff:=false;

while num<=tail do
begin
if sl[q[num]]=1 then begin fff:=true; break; end;
for j:=1 to cb[q[num]] do
if used[iff[q[num],j]]=0 then begin used[iff[q[num],j]]:=1; inc(tail); q[tail]:=iff[q[num],j]; end;
inc(num);
end;
if fff then writeln('Yes') else writeln('No');
end;
close(input);
close(output);
end.
#include <cstdio>
#include <cstring>
int n,m;
long long dis[12][12];
bool avil[12][12];
long long dp[1<<12][12][15];
long long min(long long a,long long b)
{
	return a<b ? a:b;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(avil,false,sizeof(avil));
	for (int i=1;i<=m;i++)
	{
		int a,b;
		long long c;
		scanf("%d%d%lld",&a,&b,&c);
		a--;b--;
		if (avil[a][b])
			dis[a][b]=min(dis[a][b],c),dis[b][a]=min(dis[b][a],c);
		else
			avil[a][b]=true,avil[b][a]=true,dis[a][b]=c,dis[b][a]=c;
	}
	memset(dp,0x3f,sizeof(dp));
	for (int i=0;i<n;i++)
		memset(dp[1<<i][i],0,sizeof(dp[1<<i][i]));
	for (int sx=1;sx<(1<<n);sx++)
		for (int sy=1;sy<sx;sy++)
			if (!(sx & sy))
			{
				int news=(sx|sy);
				for (int x=0;x<n;x++)
					if ((sx>>x)&1)
						for (int y=0;y<n;y++)
							if (((sy>>y)&1) && avil[x][y])
								for (int dep=1;dep<n;dep++)
									dp[news][x][dep]=min(dp[news][x][dep],dp[sx][x][dep]+dp[sy][y][dep+1]+dis[x][y]*dep),
									dp[news][y][dep]=min(dp[news][y][dep],dp[sx][x][dep+1]+dp[sy][y][dep]+dis[x][y]*dep);
			}
	long long  ans=0x3f3f3f3f;
	for (int i=0;i<n;i++)
		ans=min(ans,dp[(1<<n)-1][i][1]);
	printf("%lld\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

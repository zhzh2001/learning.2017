#include <cstdio>
#include <cstring>
const int MAXN=1e6;
int n,m,q;
int askx[510],asky[510];
long long s[MAXN],sn;
long long c[MAXN];
long long lowbit(long long x)
{
	return x&(-x);
}
void add(int x)
{
	while (x<=n+m+q)
		c[x]++,x+=lowbit(x);
}
long long ask(long long x)
{
	long long ans=x;
	while (x)
		ans-=c[x],x-=lowbit(x);
	return ans;
}
long long find(long long x)
{
	long long l=1,r=sn,ans;
	while (l<=r)
	{
		long long mid=(l+r)/2;
		if (ask(mid)>=x)
		{
			ans=mid;
			r=mid-1;
		}
		else
			l=mid+1;
	}
	return ans;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (q<=1000)
	{
		for (int i=1;i<=q;i++)
		{
			scanf("%d%d",&askx[i],&asky[i]);
			int x=askx[i],y=asky[i];
			for (int j=i-1;j>=1;j--)
			{
				if (x==n && y==m)
				{
					x=askx[j];
					y=asky[j];
					continue;
				}
				if (y==m)
				{
					if (x>=askx[j])
						x++;
					continue;
				}
				if (x==askx[j])
					if (y>=asky[j])
						y++;
			}
			printf("%lld\n",(long long)(x-1)*m+y);
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	sn=0;
	for (int i=1;i<m;i++)
		s[++sn]=i;
	for (int i=1;i<=n;i++)
		s[++sn]=(long long)m*i;
	memset(c,0,sizeof(c));
	for (int i=1;i<=q;i++)
	{
		long long x,y;
		scanf("%lld%lld",&x,&y);
		long long p=find(y);
		s[++sn]=s[p];
		printf("%lld\n",s[p]);
		add(p);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

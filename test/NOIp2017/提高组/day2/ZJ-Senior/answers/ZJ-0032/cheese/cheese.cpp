#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int MAXN=1010;
int n;
long long h,r;
bool vis[MAXN];
struct node
{
	long long x[3];
}s[MAXN];
bool cmp(node a,node b)
{
	return a.x[2]<b.x[2];
}
long long dis(int a,int b)
{
	long long ans=0;
	for (int i=0;i<3;i++)
		ans+=(s[a].x[i]-s[b].x[i])*(s[a].x[i]-s[b].x[i]);
	return ans;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int kase;
	scanf("%d",&kase);
	for (int ii=1;ii<=kase;ii++)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++)
			for (int j=0;j<3;j++)
				scanf("%lld",&s[i].x[j]);
		sort(s+1,s+n+1,cmp);
		memset(vis,false,sizeof(vis));
		for (int i=1;i<=n;i++)
			if (s[i].x[2]<=r && s[i].x[2]>=-r)
				vis[i]=true;
		long long t=4*r*r;
		for (int i=1;i<=n;i++)
			if (vis[i])
				for (int j=i+1;j<=n && s[j].x[2]-s[i].x[2]<=2*r;j++)
					if (dis(i,j)<=t)
						vis[j]=true;
		bool flag=false;
		for (int i=n;i>=1;i--)
			if (vis[i] && s[i].x[2]>=h-r && s[i].x[2]<=h+r)
			{
				flag=true;
				break;
			}
		if (flag)
			printf("Yes\n");
		else
			printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("treasure.in");
const int n=11,m=1000;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=m;)
	{
		uniform_int_distribution<> d(1,n),dw(1,5000);
		int u=d(gen),v=d(gen);
		if(u!=v)
		{
			fout<<u<<' '<<v<<' '<<dw(gen)<<endl;
			i++;
		}
	}
	return 0;
}

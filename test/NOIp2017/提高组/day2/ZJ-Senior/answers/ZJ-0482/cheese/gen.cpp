#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("cheese.in");
const int t=20,n=1000;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<t<<endl;
	for(int T=1;T<=t;T++)
	{
		uniform_int_distribution<> d(1,1e9);
		fout<<n<<' '<<d(gen)<<' '<<d(gen)<<endl;
		for(int i=1;i<=n;i++)
		{
			uniform_int_distribution<> d(-1e9,1e9);
			fout<<d(gen)<<' '<<d(gen)<<' '<<d(gen)<<endl;
		}
	}
	return 0;
}

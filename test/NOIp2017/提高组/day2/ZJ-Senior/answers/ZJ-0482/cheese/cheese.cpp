#include<fstream>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("cheese.in");
ofstream fout("cheese.out");
const int N=1005;
int x[N],y[N],z[N];
bool vis[N];
inline long long sqr(long long x)
{
	return x*x;
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n,h,r;
		fin>>n>>h>>r;
		queue<int> Q;
		for(int i=1;i<=n;i++)
		{
			fin>>x[i]>>y[i]>>z[i];
			if(abs(z[i])<=r)
			{
				vis[i]=true;
				Q.push(i);
			}
			else
				vis[i]=false;
		}
		while(!Q.empty())
		{
			int k=Q.front();Q.pop();
			for(int j=1;j<=n;j++)
				if(!vis[j]&&sqr(x[k]-x[j])+sqr(y[k]-y[j])+sqr(z[k]-z[j])<=4*sqr(r))
				{
					vis[j]=true;
					Q.push(j);
				}
		}
		bool ans=false;
		for(int i=1;i<=n;i++)
			if(vis[i]&&abs(z[i]-h)<=r)
			{
				ans=true;
				break;
			}
		if(ans)
			fout<<"Yes\n";
		else
			fout<<"No\n";
	}
	return 0;
}

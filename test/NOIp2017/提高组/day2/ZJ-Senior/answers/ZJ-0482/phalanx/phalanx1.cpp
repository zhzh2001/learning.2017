#include<fstream>
#include<queue>
using namespace std;
ifstream fin("phalanx.in");
ofstream fout("phalanx.out");
const int N=50005,N2=600005,SZ=1e6;
unsigned last[N],*a[N];
int tree[N2*4];
long long val[N2];
int main()
{
	int n,m,q;
	fin>>n>>m>>q;
	if(n<=5e4&&m<=5e4&&q<=500)
	{
		for(int i=1;i<=n;i++)
			last[i]=1u*i*m;
		while(q--)
		{
			int x,y;
			fin>>x>>y;
			if(!a[x])
			{
				a[x]=new unsigned [m+1];
				for(int i=1;i<m;i++)
					a[x][i]=1u*(x-1)*m+i;
			}
			a[x][m]=last[x];
			unsigned tmp=a[x][y];
			for(int i=y+1;i<=m;i++)
				a[x][i-1]=a[x][i];
			for(int i=x+1;i<=n;i++)
				last[i-1]=last[i];
			last[n]=tmp;
			fout<<tmp<<'\n';
		}
		for(int i=1;i<=n;i++)
			if(a[i])
				delete [] a[i];
	}
	else
	{
		//solve x=1
		queue<long long> last;
		for(int i=1;i<=n;i++)
			last.push(1ll*i*m);
		for(int i=1;i<=m;i++)
			val[i]=i;
		for(int t=1;t<=q;t++)
		{
			int x,y;
			fin>>x>>y;
			int id=1,l=1,r=m+q,sum=0;
			while(l<r)
			{
				int mid=(l+r)/2;
				if(mid-tree[id*2]-sum>=y)
				{
					r=mid;
					id*=2;
				}
				else
				{
					sum+=tree[id*2];
					l=mid+1;
					id=id*2+1;
				}
			}
			long long tmp=val[l];
			tree[id]=1;
			while(id>1)
				tree[id/=2]++;
			last.pop();
			last.push(tmp);
			val[m+t]=last.front();
			fout<<tmp<<'\n';
		}
	}
	return 0;
}

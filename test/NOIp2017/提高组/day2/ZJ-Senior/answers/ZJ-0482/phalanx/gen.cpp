#include<fstream>
#include<random>
#include<windows.h>
using namespace std;
ofstream fout("phalanx.in");
const int n=3e5,q=3e5;
int main()
{
	minstd_rand gen(GetTickCount());
	fout<<n<<' '<<n<<' '<<q<<endl;
	for(int i=1;i<=q;i++)
	{
		uniform_int_distribution<> d(1,n);
		fout<<1<<' '<<d(gen)<<endl;
	}
	return 0;
}

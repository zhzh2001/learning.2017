#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define N 1005
int T,n,h,r,x[N],y[N],z[N];
int check(int p,int q){
	unsigned long long d=1ll*(x[p]-x[q])*(x[p]-x[q])+
	+1ll*(y[p]-y[q])*(y[p]-y[q])+1ll*(z[p]-z[q])*(z[p]-z[q]);
	return d<=4ll*r*r;
}
int fa[N];
int father(int x){
	return fa[x]==x?x:fa[x]=father(fa[x]);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r);
		rep(i,1,n) scanf("%d%d%d",&x[i],&y[i],&z[i]);
		rep(i,1,n) fa[i]=i;
		rep(i,1,n) rep(j,i+1,n) if(check(i,j)) fa[father(i)]=father(j);
		bool flag=false;
		rep(i,1,n) rep(j,i,n) if(father(i)==father(j)&&z[i]<=r&&z[j]>=h-r) flag=true;
		puts(flag?"Yes":"No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

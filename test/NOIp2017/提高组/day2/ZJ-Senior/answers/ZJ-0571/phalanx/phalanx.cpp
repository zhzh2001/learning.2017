#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define ll long long
#define N 300005
inline int mn(int x,int y){
	return x<y?x:y;
}
inline int mx(int x,int y){
	return x>y?x:y;
}
struct node{
	int l,r,s;
}mem[N*80];int tot;
struct Treachery{
	int rtpos,sz,S;
	std::vector<ll> ext;
	void init(int base,int need){
		sz=base+need;S=base;ext.clear();
		mem[rtpos=++tot]=(node){0,0,S};
	}
	inline void nd(int&x,int l,int r){
		if(!x) mem[x=++tot]=(node){0,0,mx(mn(r,S)-l+1,0)};
	}
	ll Raskdel(int x,int l,int r,int p){
		mem[x].s--;
		if(l==r) return l<=S?l:-ext[l-S-1];
		int mid=l+r>>1,half;
		nd(mem[x].l,l,mid);half=mem[mem[x].l].s;
		if(p<=half) return Raskdel(mem[x].l,l,mid,p);
		nd(mem[x].r,mid+1,r);
		return Raskdel(mem[x].r,mid+1,r,p-half);
	}
	ll askdel(int pos){
		return Raskdel(rtpos,1,sz,pos);
	}
	void Radd(int x,int l,int r,int p){
		mem[x].s++;
		if(l==r) return;
		int mid=l+r>>1;
		if(p<=mid){
			nd(mem[x].l,l,mid);
			Radd(mem[x].l,l,mid,p);
		}
		else{
			nd(mem[x].r,mid+1,r);
			Radd(mem[x].r,mid+1,r,p);
		}
	}
	void add(ll num){
		ext.push_back(num);
		Radd(rtpos,1,sz,S+ext.size());
	}
}row[N],col;
int n,m,q,x,y;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	tot=0;
	rep(i,1,n) row[i].init(m-1,q);
	col.init(n,q);
	while(q--){
		scanf("%d%d",&x,&y);
		if(y<m){
			ll a=row[x].askdel(y),A=a>0?(x-1ll)*m+a:-a;
			ll b=col.askdel(x),B=b>0?b*m:-b;
			row[x].add(B);col.add(A);
			printf("%lld\n",A);
		}
		else{
			ll a=col.askdel(x),A=a>0?a*m:-a;
			col.add(A);
			printf("%lld\n",A);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
	/*
	col.init(2,3);
	col.del(1);
	col.add(233);
	printf("%lld\n",col.ask(1));
	printf("%lld\n",col.ask(2));
	col.del(2);
	col.add(999);
	printf("%lld\n",col.ask(1));
	printf("%lld\n",col.ask(2));
	*/
}

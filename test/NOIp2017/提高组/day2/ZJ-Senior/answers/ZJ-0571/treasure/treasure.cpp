#include <bits/stdc++.h>
#define rep(i,l,r) for(int i=l;i<=r;i++)
#define per(i,r,l) for(int i=r;i>=l;i--)
#define N 15
#define S 4099
const int INF=1e8;
int n,m,x,y,z,a[N][N];
int f1[S][N],f2[S*S];
int dp[N][S];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	rep(i,1,n) rep(j,1,n) a[i][j]=INF;
	while(m--){
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=std::min(a[x][y],z);
		a[y][x]=std::min(a[y][x],z);
	}
	m=(1<<n)-1;
	rep(i,0,m) rep(j,1,n){
		f1[i][j]=INF;
		rep(k,1,n) if(i>>k-1&1) f1[i][j]=std::min(f1[i][j],a[k][j]);
	}
	rep(i,0,m){
		int x=m^i;
		while(x){
			f2[i<<n|x]=0;
			rep(j,1,n) if(x>>j-1&1) f2[i<<n|x]+=f1[i][j];
			x--;x&=m^i;
		}
	}
	rep(i,0,n) rep(j,0,m) dp[i][j]=INF;
	rep(i,1,n) dp[0][1<<i-1]=0;
	rep(i,1,n) rep(j,0,m){
		int x=j;
		while(x){
			dp[i][j]=std::min(1ll*dp[i][j],dp[i-1][j^x]+1ll*i*f2[(j^x)<<n|x]);
			x--;x&=j;
		}
	}
	int ans=INF;
	rep(i,1,n) ans=std::min(ans,dp[i][m]);
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

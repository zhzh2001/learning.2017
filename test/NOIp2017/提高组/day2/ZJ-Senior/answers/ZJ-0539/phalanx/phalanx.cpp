#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
typedef long long LL;
const int MAXN=1010, MAXMAXN=300010;
int a[MAXN][MAXN],f[MAXMAXN],g[MAXMAXN],p[MAXMAXN<<1];
int n,m,q,top,top2=0;

inline void read(int& x) {
	x=0;int f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}x*=f;
}

inline void modify_f(int x,int v) {
	for(int i=x;i<=m;i+=i&(-i)) {
		f[i]+=v;
	}
}

inline int getsum_f(int x) {
	int res=0;
	for(int i=x;i>0;i-=i&(-i)) {
		res+=f[i];
	}
	return res;
}

inline void solve(int x) {
	int pos1=getsum_f(x), k=p[g[pos1]];
	printf("%d\n",k);
	p[++top]=k;
	g[++top2]=top;
	modify_f(x,1);
}

int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	top=0;
	for(int i=1;i<=n;i++) {
		for(int j=1;j<=m;j++) {
			a[i][j]=(i-1)*m+j;
			if(n==1) {
				p[++top]=a[i][j];
				g[++top2]=top;
				modify_f(j,1);
			}
		}
	}
	for(int i=1,u,v;i<=q;i++) {
		read(u);read(v);
		if(n==1&&m>=10000) {
			solve(v);
		} else {
			int k=a[u][v];
			printf("%d\n",k);
			for(int j=v;j<m;j++) {
				a[u][j]=a[u][j+1];
			}
			for(int j=u;j<n;j++) {
				a[j][m]=a[j+1][m];
			}
			a[n][m]=k;
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

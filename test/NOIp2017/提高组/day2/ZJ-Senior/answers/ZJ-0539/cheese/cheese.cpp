#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
typedef long long LL;
const int MAXN=1010, MAXM=1000010;
const double eps=1e-9;
int fa[MAXN];
struct node {
	int x,y,z;
} a[MAXN];
int T,n,m,h,r;

inline void read(int& x) {
	x=0;int f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}x*=f;
}

inline int getfa(int x) {
	return fa[x]==x ? x : fa[x]=getfa(fa[x]);
}

inline void unite(int x,int y) {
	int fx=getfa(x), fy=getfa(y);
	if(fx==fy) return;
	fa[fy]=fx;
}

inline double sqr(int x) {
	return 1.0*x*x;
}

inline double getdis(int p,int q) {
	return sqrt(sqr(a[p].x-a[q].x)+sqr(a[p].y-a[q].y)+sqr(a[p].z-a[q].z));
}

int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--) {
		read(n);read(h);read(r);
		for(int i=0;i<=n+1;i++) {
			fa[i]=i;
		}
		for(int i=1;i<=n;i++) {
			read(a[i].x);read(a[i].y);read(a[i].z);
			if(a[i].z<=r) {
				unite(0,i);
			}
			if(h-a[i].z<=r) {
				unite(i,n+1);
			}
		}
		for(int i=1;i<=n;i++) {
			for(int j=i+1;j<=n;j++) {
//				printf("%d, %d, %.5lf\n",i,j,getdis(i,j));
				if(getdis(i,j)-2*r<=eps) {
					unite(i,j);
				}
			}
		}
		int fx=getfa(0), fy=getfa(n+1);
		if(fx==fy) puts("Yes");
		else puts("No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

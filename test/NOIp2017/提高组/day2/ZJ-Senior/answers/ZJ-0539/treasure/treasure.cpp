#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<string>
#include<cstring>
using namespace std;
typedef long long LL;
const int MAXN=20;
int a[MAXN][MAXN],G[MAXN][MAXN];
int n,m,ans;

inline void read(int& x) {
	x=0;int f=1;char ch=getchar();
	while(!isdigit(ch)){if(ch=='-')f=-1;if(ch==EOF)return;ch=getchar();}
	while(isdigit(ch)){x=x*10+ch-'0';ch=getchar();}x*=f;
}

int dis[MAXN],num[MAXN];
bool vis[MAXN];
inline int prim(int s) {
	memset(dis,0x3f,sizeof dis);
	memset(vis,0,sizeof vis);
	memset(num,0,sizeof num);
	vis[s]=1;
	dis[s]=0;
	num[s]=0;
	int res=0;
	for(int i=1;i<=n;i++) {
		if(!vis[i]) {
			dis[i]=a[s][i];
			num[i]=1;
		}
	}
	for(int i=2;i<=n;i++) {
		int mn=0x3f3f3f3f, id=0;
		for(int j=1;j<=n;j++) {
			if(!vis[j]&&dis[j]<mn) {
				mn=dis[j];
				id=j;
			}
		}
		vis[id]=1;
		res+=mn;
		for(int j=1;j<=n;j++) {
			if(!vis[j]&&a[id][j]<0x3f3f3f3f&&dis[j]>a[id][j]*(num[id]+1)) {
				num[j]=num[id]+1;
				dis[j]=a[id][j]*num[j];
			}
		}
	}
	return res;
}

int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	memset(a,0x3f,sizeof a);
	for(int i=1,j,k,l;i<=m;i++) {
		read(j);read(k);read(l);
		a[j][k]=a[k][j]=min(a[j][k],l);
	}
	ans=0x3f3f3f3f;
	for(int i=1;i<=n;i++) {
		ans=min(ans,prim(i));
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstdio>
using namespace std;
#define int long long
const int N=1010;
int Case,n,h,r;
struct P{int x,y,z;}p[N];
inline int Sqr(int x){return x*x;}
inline bool is_cross(P u,P v){
	return Sqr(u.x-v.x)+Sqr(u.y-v.y)<=4*r*r-Sqr(u.z-v.z);
}
P q[N];
bool vis[N];
main(){
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	scanf("%lld",&Case);
	while (Case--){
		scanf("%lld%lld%lld",&n,&h,&r);
		int hd=0,tl=0;
		for (int i=1;i<=n;++i){
			scanf("%lld%lld%lld",&p[i].x,&p[i].y,&p[i].z);
			if (p[i].z<=r) q[++tl]=p[i],vis[i]=1;else vis[i]=0;
		}
		while (++hd<=tl){
			P u=q[hd];
			for (int i=1;i<=n;++i) if (!vis[i]&&is_cross(u,p[i])) vis[i]=1,q[++tl]=p[i];
		}
		bool flag=0;
		for (int i=1;i<=n;++i) if (vis[i]&&p[i].z+r>=h) {flag=1;break;}
		puts(flag?"Yes":"No");
	}
	return 0;
}

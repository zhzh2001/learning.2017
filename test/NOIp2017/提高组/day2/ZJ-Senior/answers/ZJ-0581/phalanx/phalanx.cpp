#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<vector>
using namespace std;
#define int long long
int n,m,q;
inline void read(int &x){
	char c=getchar();x=0;
	while (!isdigit(c)) c=getchar();
	while (isdigit(c)) x=(x<<3)+(x<<1)+(c^48),c=getchar();
}
namespace X__1{
	const int N=3e5+10;
	int id[N<<2],idnum,s;
	namespace Seg{
		int si[N*20];
		inline void pushup(int v){si[v]=si[v<<1]+si[v<<1|1];}
		inline void pre(int v=1,int l=1,int r=s){
			if (l==r) {si[v]=(l<=idnum);return;}
			int mid=(l+r)>>1;
			pre(v<<1,l,mid);pre(v<<1|1,mid+1,r);
			pushup(v);
		}
		inline void add(int x,int y,int v=1,int l=1,int r=s){
			if (l==r) {si[v]+=y;return;}
			int mid=(l+r)>>1;
			if (x<=mid) add(x,y,v<<1,l,mid);
			else add(x,y,v<<1|1,mid+1,r);
			pushup(v);
		}
		inline int kth(int k,int v=1,int l=1,int r=s){
			if (l==r) return l;
			int mid=(l+r)>>1;
			return si[v<<1]>=k?kth(k,v<<1,l,mid):kth(k-si[v<<1],v<<1|1,mid+1,r);
		}
	}
	inline void solve(){
		s=n+m+q;
		for (int i=1;i<=m;++i) id[++idnum]=i;
		for (int i=2;i<=n;++i) id[++idnum]=i*m;
		Seg::pre();
		while (q--){
			int x,y;
			read(x);read(y);
			int tp=Seg::kth(y);
			id[++idnum]=id[tp];
			Seg::add(tp,-1);
			Seg::add(idnum,1);
			printf("%lld\n",id[tp]);
		}
	}
}
namespace N__1000{
	const int N=1010;
	int mat[N][N];
	inline void solve(){
		int id=0;
		for (int i=1;i<=n;++i) for (int j=1;j<=m;++j) mat[i][j]=++id;
		while (q--){
			int x,y;
			read(x);read(y);
			int v=mat[x][y];
			printf("%lld\n",v);
			for (int i=y;i<m;++i) mat[x][i]=mat[x][i+1];
			for (int i=x;i<n;++i) mat[i][m]=mat[i+1][m];
			mat[n][m]=v;
		}
	}
}
namespace Q__500{
	const int N=5e4+10;
	vector<int> g[N],d[N];//g表示每行最后加的数。
	int lm[N];//最后一列的情况。
	inline int get_id(int x,int y){
		sort(d[x].begin(),d[x].end());
		vector<int>::iterator dei=d[x].begin();
		int num=1;
		for (int i=1;i<=y;++i){
			while (dei!=d[x].end()&&*dei==num) ++dei,++num;
			++num;
		}
		return num-1;
	}
	inline void solve(){
		for (int i=1;i<=n;++i) lm[i]=i*m; 
		while (q--){
			int x,y;
			read(x);read(y);
			int tmp;
			if (y==m){
				printf("%lld\n",tmp=lm[x]);
			}else{
				int num=get_id(x,y);
				tmp=(num>=m?g[x][num-m]:num+m*(x-1));
				printf("%lld\n",tmp);
				d[x].push_back(num);
				g[x].push_back(lm[x]);
			}
			for (int i=x;i<n;++i) lm[i]=lm[i+1];lm[n]=tmp;
		}
	}
}
main(){
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	if (q>500) X__1::solve();
	else if (n<=1000&&m<=1000) N__1000::solve();
	else Q__500::solve();
	return 0;
}

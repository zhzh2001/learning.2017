#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
const int N=20;
int n,m,s,d[N][N],f[N][1<<12],ans=1e9,dps[N],ne[N],sp,tp;
inline void dfs(int mxdp,int va){
	if (va>=ans) return;
	if (ne[sp]==tp) {ans=va;return;}
	for (int i=1;i<=mxdp;++i){//枚举用哪个深度去更新新的点。 
		for (int p=sp;ne[p]!=tp;p=ne[p]){//枚举更新的新点。
			int np=ne[p];
			ne[p]=ne[np];
			dps[i+1]^=1<<np;
			dfs(max(mxdp,i+1),va+f[np][dps[i]]*i);
			dps[i+1]^=1<<np;
			ne[p]=np;
		} 
	}
}
namespace Greedy{
	int m,fa[N],tot,fi[N],a[N<<1],ne[N<<1],c[N<<1],sum;
	struct Edge{int x,y,z;}e[1010];
	inline bool cmp(Edge u,Edge v){return u.z<v.z;}
	inline void init(int x,int y,int z){
		e[++m]=(Edge){x,y,z};
	}
	inline int ask(int x){
		return fa[x]==x?x:fa[x]=ask(fa[x]);
	}
	inline void add(int x,int y,int z){
		a[++tot]=y;c[tot]=z;ne[tot]=fi[x];fi[x]=tot;
	}
	inline void dfs(int x,int fa,int de){
		for (int i=fi[x];i;i=ne[i]) if (a[i]!=fa) sum+=de*c[i],dfs(a[i],x,de+1);
	}
	inline void solve(){
		for (int i=1;i<=n;++i) fa[i]=i;
		sort(e+1,e+m+1,cmp);
		for (int i=1;i<=m;++i){
			int u=ask(e[i].x),v=ask(e[i].y);
			if (u!=v) add(e[i].x,e[i].y,e[i].z),add(e[i].y,e[i].x,e[i].z),fa[u]=v; 
		}
		for (int i=1;i<=n;++i){
			sum=0;
			dfs(i,0,1);
			ans=min(ans,sum);
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);s=1<<n;
	for (int i=0;i<n;++i) for (int j=0;j<n;++j) d[i][j]=1e8;
	for (int i=0;i<n;++i) for (int j=0;j<s;++j) f[i][j]=1e8;
	for (int i=1,x,y,z;i<=m;++i){
		scanf("%d%d%d",&x,&y,&z);
		Greedy::init(x,y,z);
		--x;--y;
		d[x][y]=d[y][x]=min(d[x][y],z);
	}
	Greedy::solve();
	for (int i=0;i<n;++i)
		for (int j=0;j<s;++j)
			for (int k=0;k<n;++k)
			if (j>>k&1) f[i][j]=min(f[i][j],d[i][k]);
	sp=n+1;tp=n;ne[sp]=0;
	for (int i=0;i<n;++i) ne[i]=i+1;
	for (int i=sp;ne[i]!=tp;i=ne[i]){//枚举根为ne[i]; 
		int np=ne[i];
		ne[i]=ne[np];
		dps[1]^=1<<np;
		dfs(1,0);
		dps[1]^=1<<np;
		ne[i]=np;
	}
	printf("%d",ans);
	return 0;
}

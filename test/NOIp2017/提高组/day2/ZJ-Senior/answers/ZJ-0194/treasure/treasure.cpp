#include<cstdio>
#include<cstring>
#include<algorithm>
#include<iostream>
typedef long long ll;
using namespace std;
const int M=20;//�� long long
int n,m,v[1005],dep[20],fa[20];
int mp[20][20];
bool mark[M];
ll ans,sum_dep,sum;
void Min(ll&a,ll b){
	if(a==-1||a>b)a=b;
}
struct The_same{
	void dfs(int x,int f,int d,int cnt){
		fa[x]=f;
		if(cnt==n)Min(ans,sum_dep*v[1]);
		for(int i=1;i<=n;i++){
			if(i==f||mp[x][i]==-1||mark[i])continue;
			sum_dep+=d+1;
			mark[i]=true;
			dfs(i,x,d+1,cnt+1);
			mark[i]=false;
			sum_dep-=d+1;
		}
		for(int i=1;i<=n;i++){
			if(i==fa[f]||mp[f][i]==-1||mark[i])continue;
			sum_dep+=d;
			mark[i]=true;
			dfs(i,f,d,cnt+1);
			mark[i]=false;
			sum_dep-=d;
		}
	}
	void solve_v_same(){
//		puts("same");
		ans=-1;
		for(int choose=1;choose<=n;choose++){
			dep[0]=-1;sum_dep=0;
			memset(mark,0,sizeof(mark));
			mark[choose]=true;
			dfs(choose,0,0,1);
		}
		printf("%lld\n",ans);
	}
}P_same;
struct different_from{
	void dfs(int x,int f,int d,int cnt){
		fa[x]=f;
		dep[x]=d;
		if(cnt==n)Min(ans,sum);
		for(int i=1;i<=n;i++){
			if(i==f||mp[x][i]==-1||mark[i])continue;
			sum+=(d+1)*mp[x][i];
			mark[i]=true;
			dfs(i,x,d+1,cnt+1);
			mark[i]=false;
			sum-=(d+1)*mp[x][i];
		}
		for(int i=1;i<=n;i++){
			if(i==fa[f]||mp[f][i]==-1||mark[i])continue;
			sum+=d*mp[f][i];
			mark[i]=true;
			dfs(i,f,d,cnt+1);
			mark[i]=false;
			sum-=d*mp[f][i];
		}
	}
	void solve_v_different(){
		ans=-1;
		for(int choose=1;choose<=n;choose++){
			dep[0]=-1;sum=0;
			memset(mark,0,sizeof(mark));
			mark[choose]=true;
			dfs(choose,0,0,1);
		}
		printf("%lld\n",ans);
	}
}P_different;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	memset(mp,-1,sizeof(mp));
	for(int i=1;i<=m;i++){
		int a,b;
		scanf("%d %d %d",&a,&b,&v[i]);
		if(mp[a][b]==-1||mp[a][b]>v[i])mp[a][b]=v[i];
		if(mp[b][a]==-1||mp[b][a]>v[i])mp[b][a]=v[i];
	}
	int cnt=1;
	for(int i=2;i<=n;i++)
		cnt+=v[i]==v[1];
	if(cnt==n)P_same.solve_v_same();
	else P_different.solve_v_different();
	return 0;
}

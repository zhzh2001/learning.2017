#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
typedef long long ll;
const int M=1005;
bool mark[M];
int Q[M*40];
struct node{
	int x,y,z;
}A[M];
struct Edge{
	int to,nxt;
}edge[M*M*2];
int n,h,r,head[M],tot;
ll Pow(int x){
	return 1ll*x*x;
}
bool check(int i,int j){
	return Pow(A[i].x-A[j].x)+Pow(A[i].y-A[j].y)+Pow(A[i].z-A[j].z)<=4ll*r*r;
}
void Make_Graph(){
	for(int i=1;i<=n;i++){
		if(A[i].z<=r){
			edge[tot]=(Edge){i,head[0]};head[0]=tot++;
			edge[tot]=(Edge){0,head[i]};head[i]=tot++;
		}
		if(A[i].z+r>=h){
			edge[tot]=(Edge){i,head[n+1]};head[n+1]=tot++;
			edge[tot]=(Edge){n+1,head[i]};head[i]=tot++;
		}
	}
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++){
			if(check(i,j)){
				edge[tot]=(Edge){j,head[i]};head[i]=tot++;
				edge[tot]=(Edge){i,head[j]};head[j]=tot++;
			}
		}
}
bool BFS(){
	int L=0,R=0;
	Q[R++]=0;
	while(L<R){
		int id=Q[L++];
		if(mark[id])continue;
		if(id==n+1)return true;
		mark[id]=true;
		for(int i=head[id];~i;i=edge[i].nxt){
			Edge nxt=edge[i];
			if(!mark[nxt.to]){
				Q[R++]=nxt.to;
			}
		}
	}
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;
	scanf("%d",&cas);
	while(cas--&&scanf("%d %d %d",&n,&h,&r)){
		memset(head,-1,sizeof(head));
		tot=0;
		memset(mark,0,sizeof(mark));
		for(int i=1;i<=n;i++){
			scanf("%d %d %d",&A[i].x,&A[i].y,&A[i].z);
		}//0->[1,n]
		Make_Graph();//[1,n]
		//[1,n]->top
		puts(BFS()?"Yes":"No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<iostream>
#include<cmath>
#include<algorithm>
using namespace std;
const int M=1005;
int A[M][M],n,m,q;
struct LZ{
	int x,y;
}Q[300005];
inline void Rd(int&res){
	char c;res=0;
	while(c=getchar(),c<48);
	do res=(res*10)+(c&15);
	while(c=getchar(),c>47);
}
struct P30{
	void Move(int x,int y){
		int temp=A[x][y];
		for(int i=y;i<n;i++)
			A[x][i]=A[x][i+1];
		for(int i=x;i<n;i++)
			A[i][m]=A[i+1][m];
		A[n][m]=temp;
	}
	void solve(){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(i-1)*m+j;
		for(int i=1;i<=q;i++){
			int x=Q[i].x,y=Q[i].y;
			printf("%d\n",A[x][y]);
			Move(x,y);
		}
	}
}P30;
int Line[1050005];
int val[1050005];
int num[800];
struct Pline{
	int query(int x,int tot,int S,int bnum){
		int res=0,k;
		for(int i=0;i<=bnum;i++){
			if(res+num[i]>x){
				k=i;
				break;
			}res+=num[i];
		}
		x-=res;
		int t=0;
		for(int i=S*k+1;i<=S*(k+1);i++){
			if(~Line[i])t++;
			if(t==x){
				printf("%d\n",Line[i]);
				t=Line[i];
				Line[i]=-1;
				break;
			}
		}
		num[k]--;
		return t;
	}
	void Re_build(int tot,int S,int bnum,int pos){
		for(int i=1;i<=pos;i++)val[i]=Line[i];
		int k=1,t=1,res=tot;
		while(k<pos){
			while(val[k]==-1)k++;
			Line[t++]=val[k];
		}
		for(int i=0;i<=bnum;i++){
			if(res<S)num[i]=res;
			else num[i]=S,res-=S;
		}
	}
	void solve(){/*[i,m]*/
		for(int i=1;i<m;i++)Line[i]=i;
		for(int i=1;i<=n;i++)Line[m+i-1]=(i-1)*m+m;
		int tot=n+m-1;
		int S=(int)(sqrt(tot+0.5));
		int bnum=tot/S;
		int pos=tot,res=tot;
		
		for(int i=0;i<=bnum;i++){
			if(res<S)num[i]=res;
			else num[i]=S,res-=S;
		}
		
		for(int i=1;i<=q;i++){
			int x=Q[i].x,y=Q[i].y;
			int id=(x-1)*m+y;
			if(q%tot==0){
				Re_build(tot,S,bnum,pos);
			}
			Line[++pos]=query(id,tot,S,bnum);
		}
	}
}Pline;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d %d %d",&n,&m,&q);
	int flag=0;
	for(int i=1;i<=q;i++){
		Rd(Q[i].x),Rd(Q[i].y);
		flag|=Q[i].x;
	}
	if(n<=1000&&m<=1000)P30.solve();
	else if(flag==1)Pline.solve();
	return 0;
}

#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int INF=0x3f3f3f3f;
int read(){
	int x=0,f=0;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			f=1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return f?-x:x;
}
int dp[1<<24];
int a[15][15];
int n;
inline void relax(int &x,int y){
	if (x>y)
		x=y;
}
/*void dfs(int s){
	bool flag=true;
	int now=-1,&res=dp[s];
	for(int i=0;i<n;i++){
		int k=(s>>(i*3))&7;
		if (!k){
			if (now!=-1){
				flag=false;
				break;
			}
			now=i;	
		}
	}
	//if (s==1)
		//puts("WTF");
	if (flag){
		int *qwq=a[now];
		for(int i=0;i<n;i++){
			int k=(s>>(i*3))&7;
			if (k)
				relax(res,qwq[i]*k);
		}
		return;
	}
	//if (s==1)
		//puts("WTF");
	for(int i=0;i<n;i++){
		int k=(s>>(i*3))&7;
		if (!k)
			continue;
		//if (s==1)
			//fprintf(stderr,"on 1 %d\n",i);
		int *qwq=a[i];
		for(int j=0;j<n;j++){
			if ((s>>(j*3))&7)
				continue;
			int to=s|((k+1)<<(j*3)),w=qwq[j]*k;
			if (w>res)
				continue;
			dfs(to);
			//if (s==1)
				//fprintf(stderr,"on 1 %d %d %d\n",i,j,a[i][j]);
			relax(res,dp[to]+w);		
		}
	}
	//fprintf(stderr,"dp[%d]=%d\n",s,dp[s]);
}*/
int q[1<<24];
int bfs(){
	int l=1,r=0,ans=INF;
	for(int i=0;i<n;i++)
		q[++r]=1<<(i*3),dp[1<<(i*3)]=0;
	while(l<=r){
		int s=q[l++],now=-1;
		//printf("%d %d\n",s,dp[s]);
		bool flag=true;
		for(int i=0;i<n;i++){
			int k=(s>>(i*3))&7;
			if (!k){
				if (now!=-1){
					flag=false;
					break;
				}
				now=i;	
			}
		}
		//if (s==1)
			//puts("WTF");
		if (flag){
			int *qwq=a[now];
			for(int i=0;i<n;i++){
				int k=(s>>(i*3))&7;
				if (k)
					relax(ans,dp[s]+qwq[i]*k);
			}
			continue;
		}
		//if (s==1)
			//puts("WTF");
		for(int i=0;i<n;i++){
			int k=(s>>(i*3))&7;
			if (!k)
				continue;
			//if (s==1)
				//fprintf(stderr,"on 1 %d\n",i);
			int *qwq=a[i];
			for(int j=0;j<n;j++){
				if ((s>>(j*3))&7)
					continue;
				int to=s|((k+1)<<(j*3)),w=qwq[j]*k;
				if (dp[s]+w>ans)
					continue;
				if (dp[to]==INF)
					q[++r]=to;
				relax(dp[to],dp[s]+w);
				//if (s==1)
					//fprintf(stderr,"on 1 %d %d %d\n",i,j,a[i][j]);		
			}
		}
	}
	return ans;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();
	int m=read();
	if (n==1)
		return puts("0"),0;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			a[i][j]=80000000;
	memset(dp,0x3f,sizeof(dp));
	for(int i=0;i<n;i++)
		a[i][i]=0;
	while(m--){
		int u=read()-1,v=read()-1,w=read();
		a[u][v]=min(a[u][v],w);
		a[v][u]=min(a[v][u],w);
	}
	/*int ans=INF;
	for(int i=0;i<n;i++){
		int to=1<<(i*3);
		dfs(to);
		ans=min(ans,dp[to]);
	}
	printf("%d",ans);*/
	printf("%d",bfs());
}

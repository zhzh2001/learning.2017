#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
int read(){
	int x=0,f=0;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			f=1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return f?-x:x;
}
ll x[1013],y[1013],z[1013];
int fa[1013];
int getf(int x){
	return fa[x]==x?x:fa[x]=getf(fa[x]);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--){
		int n=read(),h=read();
		ll r=read();
		unsigned long long tmp=r*r*4;
		for(int i=1;i<=n;i++)
			x[i]=read(),y[i]=read(),z[i]=read();
		for(int i=1;i<=n+2;i++)
			fa[i]=i;
		for(int i=1;i<=n;i++){
			if (z[i]<=r)
				fa[getf(i)]=getf(n+1);
			if (z[i]>=h-r)
				fa[getf(i)]=getf(n+2);
			for(int j=i+1;j<=n;j++){
				unsigned long long k=(x[i]-x[j])*(x[i]-x[j]);
				k+=(y[i]-y[j])*(y[i]-y[j]);
				k+=(z[i]-z[j])*(z[i]-z[j]);
				if (k<=tmp)
					fa[getf(i)]=getf(j);
			}
		}
		puts(getf(n+1)==getf(n+2)?"Yes":"No");
	}
	//fprintf(stderr,"%d",clock());			
}

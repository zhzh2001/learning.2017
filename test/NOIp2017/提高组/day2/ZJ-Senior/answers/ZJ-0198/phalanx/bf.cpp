#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int INF=0x3f3f3f3f;
int read(){
	int x=0,f=0;
	char c=getchar();
	while(c<'0' || c>'9'){
		if (c=='-')
			f=1;
		c=getchar();
	}
	while(c>='0' && c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return f?-x:x;
}
struct segtree{
	int n;
	int sum[2400008];
	int tmp;
	void build(int o,int l,int r){
		if (l==r){
			sum[o]=l<=tmp?1:0;
			return;
		}
		int mid=(l+r)/2;
		build(o*2,l,mid);
		build(o*2+1,mid+1,r);
		sum[o]=sum[o*2]+sum[o*2+1];
	}
	void init(){
		build(1,1,n);
	}
	int query(int k){
		int o=1,l=1,r=n;
		while(l<r){
			int mid=(l+r)/2;
			if (k<=sum[o*2])
				o*=2,r=mid;
			else k-=sum[o*2],o=o*2+1,l=mid+1;
		}
		return l;
	}
	void update(int p,int v){
		int o=1,l=1,r=n;
		while(l<r){
			sum[o]+=v;
			int mid=(l+r)/2;
			if (p<=mid)
				o*=2,r=mid;
			else o=o*2+1,l=mid+1;
		}
		sum[o]+=v;
	}
}t;
ll a[502][50002],b[50002];
int idx[50002],to[50002];
int x[100002],y[100002];
int now[600002];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("bf.out","w",stdout);
	int n=read(),m=read(),q=read();
	/*if (n==1){
		t.n=m+q;
		t.tmp=m;
		t.init();
		for(int i=1;i<=m;i++)
			idx[i]=i;
		while(q--){
			int x=read(),read();
			int p=t.query(x);
			t.update(p,-1);
			printf("%d\n",now[p]);
			now[++m]=now[p];
			t.update(++m,1);
		}
		return 0;
	}*/
	for(int i=1;i<=q;i++){
		x[i]=read(),y[i]=read();
		idx[x[i]]=1;
	}
	for(int i=1;i<=n;i++){
		idx[i]+=idx[i-1];
		if (idx[i]!=idx[i-1])
			to[idx[i]]=i;
	}
	int cnt=idx[n];
	for(int i=1;i<=cnt;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=1LL*(to[i]-1)*m+j;
	for(int i=1;i<=n;i++)
		b[i]=1LL*i*m;
	for(int i=1;i<=q;i++){
		ll *qwq=a[idx[x[i]]];
		ll tmp=y[i]==m?b[x[i]]:qwq[y[i]];
		printf("%lld\n",tmp);
		if (y[i]!=m){
			for(int j=y[i];j<m-1;j++)
				qwq[j]=qwq[j+1];
			qwq[m-1]=b[x[i]];
		}
		for(int j=x[i];j<n;j++)
			b[j]=b[j+1];
		b[n]=tmp;
	}
}

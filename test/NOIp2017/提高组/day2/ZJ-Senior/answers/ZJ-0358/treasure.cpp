#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

struct node
{
	int u,X;	
};

int dis[100005];
int N,M;
int map[105][105];
node q[1000005];
bool vis[10005];
int f[10005];
ll sum;

int find1(int X)
{
	if (f[X] != X) f[X]=find1(f[X]);
	return f[X];
}

int bfs(int s)
{
	fo(i,1,N)
	{
		vis[i]=0;
		f[i]=i;	
	}
	int t=0; int w=0;
	q[t].u=s; q[t].X=0;
	int tmp=0;
	while (t <= w)
	{
		int u=q[t].u;
		fo(i,1,N)
		{
			if (map[u][i] != 1e9)
			{
				if (! vis[i])
				{
					int fx=find1(u); int fy=find1(i);
					if (fx == fy)
					{
						continue;
					}
					q[++w].X=q[t].X + 1;
					q[w].u=i;
					tmp=tmp + q[w].X * map[u][i];
					f[fy]=fx;
				}
			}
		}
		t++;
	}
	return tmp;
}

void dfs(int u,int fa,ll tmp,int Last)
{
	if (Last == 0)
	{
		sum=min(sum,tmp);
		return;
	}
	if (tmp > sum)
	{
		return;
	}
	fo(i,1,N)
	{
		if (! vis[i])
		{
			fo(j,1,N)
			{
				if (vis[j] && map[i][j] != 1e9)
				{
					dis[i]=dis[j] + 1;
					vis[i]=1;
					dfs(i,j,tmp + dis[i] * map[i][j],Last - 1);
					vis[i]=0;
					dis[i]=1e9;
				}
			}
		}
	}
}

void solve()
{
	ll Ans=1e9;
	fo(i,1,N)
	{
		fo(j,1,N)
		{
			vis[j]=0;
			dis[j]=1e9;
		}
		sum=1e9;
		dis[i]=0;
		vis[i]=1;
		dfs(i,-1,0,N - 1);
		Ans=min(Ans,sum);
	}
	printf("%lld\n",Ans);
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&N,&M);
	bool flag1=0;
	fo(i,1,N)
	{
		fo(j,1,N)
		{
			map[i][j]=1e9;
		}
	}
	int pre=-1;
	fo(i,1,M)
	{
		int u,v,cost;
		scanf("%d%d%d",&u,&v,&cost);
		map[u][v]=min(cost,map[u][v]);
		map[v][u]=min(cost,map[v][u]);
		if (pre == -1) pre=cost;
		else if (pre != cost)
		{
			flag1=1;
		}
	}
	if (flag1)
	{
		solve();
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
	int Ans=1e9;
	fo(i,1,N)
	{
		Ans=min(Ans,bfs(i));
	}
	printf("%d\n",Ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
} 

#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

int N,M,Q;
int a[1005][1005];
int t[1000005];

int check(int i,int j)
{
	return (i - 1) * M + j;
}

int lowbit(int X)
{
	return X & (-X);
}

int query(int X)
{
	int tmp=0;
	while (X)
	{
		tmp+=t[X];
		X-=lowbit(X);
	}
	return tmp;
}

void add(int X,int tmp)
{
	while (X <= M - 1)
	{
		t[X]+=tmp;
		X+=lowbit(X); 
	}
}

int main()
{
	scanf("%d%d%d",&N,&M,&Q);
	fo (i,1,N)
	{
		fo(j,1,M)
		{
			a[i][j]=check(i,j);
		}
	}
	if (N != 1)
	{
		while (Q--)
		{
			int X,Y;
			scanf("%d%d",&X,&Y);
			int Z=a[X][Y];
			printf("%d\n",Z);
			fo(i,Y,M - 1)
			{
				a[X][i]=a[X][i + 1];
			}
			fo(i,X,N - 1)
			{
				a[i][M]=a[i + 1][M];
			}
			a[N][M]=Z;
		}
	}
	else
	{
		int pre=-1;
		while (Q--)
		{
			int X,Y;
			scanf("%d%d",&X,&Y);
			int Z=(Y + query(Y)) % M;
			if (Z == 0) Z=M;
			if (Y != M)
			{
				printf("%d\n",Z);
			}
			else
			{
				if (pre == -1)
				{
					printf("%d\n",M);
				}
				else
				{
					printf("%d\n",pre);
				}
			}
			pre=Z;
			add(Y,1);
		}
	}
	return 0;
}

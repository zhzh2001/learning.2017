#include <cmath>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#define ll long long
#define fo(i,x,y) for (int i=x; i<=y; i++)
#define pr(i,x,y) for (int i=x; i>=y; i--)
#define cl(a,x)   memset(a,x,sizeof(a))

using namespace std;

struct node
{
	ll X,Y,Z;	
};

bool vis[1005];
int q[10000005];
node a[1005];
int vet[20002005],next[20002005];
int head1[2005];
int num;
int N,H,R;

void add(int u,int v)
{
	vet[++num]=v;
	next[num]=head1[u];
	head1[u]=num;
}

bool check(int i,int j)
{
	ll X1=a[i].X; ll Y1=a[i].Y; ll Z1=a[i].Z;
	ll X2=a[j].X; ll Y2=a[j].Y; ll Z2=a[j].Z;
	double r=1.0 * (double) R;
	if (sqrt((X1 - X2) * (X1 - X2) + (Y1 - Y2) * (Y1 - Y2) + (Z1 - Z2) * (Z1 - Z2)) <= 2.0 * r)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

void bfs(int s)
{
	fo(i,0,N + 1)
	{
		vis[i]=0;
	}
	vis[s]=1;
	int t=0; int w=0;
	q[t]=s;
	while (t <= w)
	{
		int u=q[t];
		for (int e=head1[u]; e != -1; e=next[e])
		{
			int V=vet[e];
			if (V == u || vis[V]) continue;
			w++;
			q[w]=V;
			vis[V]=1;
		}
		t++;
	}
	if (vis[N + 1]) puts("Yes");
	else puts("No");
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int _;
	scanf("%d",&_);
	while (_--)
	{
		scanf("%d%d%d",&N,&H,&R);
		fo(i,1,N)
		{
			scanf("%lld%lld%lld",&a[i].X,&a[i].Y,&a[i].Z);
		}
		cl(head1,-1);
		fo(i,1,N)
		{
			if (a[i].Z - R <= 0) 
			{
				add(0,i);
				add(i,0);
			}
		}
		fo(i,1,N)
		{
			fo(j,i + 1,N)
			{
				if (check(i,j))
				{
					add(i,j); 
					add(j,i);
				}
			}
		}
		fo(i,1,N)
		{
			if (a[i].Z + R >= H)
			{
				add(i,N + 1);
				add(N + 1,i);
			}
		}
		bfs(0);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

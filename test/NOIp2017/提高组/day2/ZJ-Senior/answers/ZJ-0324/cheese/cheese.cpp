#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#define ll long long
using namespace std;
int cas,n;
ll h,r,dis;
ll px[1005],py[1005],pz[1005];
int f[1005][1005],g[1005],p[1005];
void bfs()
{
	int t=0,w=1;
	g[1]=0;
	for(int i=1;i<=n+1;i++) p[i]=0;
	p[0]=1;
	while(t<w) 
	{
		t++;
		int x=g[t];
		for(int i=0;i<=n+1;i++) 
		if(f[x][i]==1&&p[i]==0) 
		{
			w++;
			g[w]=i;
			p[i]=1;
		}
	}
	if(p[n+1]==1) printf("Yes\n"); else printf("No\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>cas;
	while(cas--) 
	{
		cin>>n>>h>>r;
		for(int i=1;i<=n;i++) scanf("%lld%lld%lld",&px[i],&py[i],&pz[i]);
		for(int i=1;i<=n;i++) 
		for(int j=i;j<=n;j++) 
		{
			dis=(px[i]-px[j])*(px[i]-px[j])+(py[i]-py[j])*(py[i]-py[j])+(pz[i]-pz[j])*(pz[i]-pz[j]);
			if(dis<=4*r*r) f[i][j]=1,f[j][i]=1;
			else f[i][j]=0,f[j][i]=0;
		}
		for(int i=1;i<=n;i++) 
		{
		    if(pz[i]<=r) f[0][i]=1,f[i][0]=1; else f[0][i]=0,f[i][0]=0;
		    if(pz[i]+r>=h) f[n+1][i]=1,f[i][n+1]=1; else f[n+1][i]=0,f[i][n+1]=0;
		}
		bfs();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define FOR(i,a,b) for(int i=(a),_t=(b);i<=_t;++i)
#define DOR(i,a,b) for(int i=(a),_t=(b);i>=_t;--i)
#define M 15

int mark[M][M];
int dfn[M],dep[M];
int n,m;
int ans=-1;
void solve1() {
	int res=0,tmp;
	FOR(i,1,n) dep[i]=-1;
	dep[dfn[1]]=1;
	FOR(i,2,n) {
		tmp=10000000;
		FOR(j,1,i-1) if(dep[dfn[j]]!=-1) {
			if(mark[dfn[j]][dfn[i]]*dep[dfn[j]]<tmp){
				tmp=mark[dfn[j]][dfn[i]]*dep[dfn[j]];
				dep[dfn[i]]=dep[dfn[j]]+1;
			}
		}
		if(dep[dfn[i]]==-1) return ;
		res+=tmp;
		if(ans!=-1&&res>=ans) return ;
	}
	if(ans==-1||ans>res) ans=res;
}
void solve2() {
	int res=0,tmp;
	FOR(i,1,n) dep[i]=-1;
	dep[dfn[1]]=1;
	FOR(i,2,n) {
		tmp=10000000;
		FOR(j,1,i-1) if(dep[dfn[j]]!=-1) {
			if(dep[dfn[i]]==-1||dep[dfn[j]]+1<dep[dfn[i]]){
				tmp=mark[dfn[j]][dfn[i]]*dep[dfn[j]];
				dep[dfn[i]]=dep[dfn[j]]+1;
			}
		}
		if(dep[dfn[i]]==-1) return ;
		res+=tmp;
		if(ans!=-1&&res>=ans) return ;
	}
	if(ans==-1||ans>res) ans=res;
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);	
	scanf("%d%d",&n,&m);
	FOR(i,1,n) FOR(j,1,n) mark[i][j]=1000000;
	FOR(i,1,m) {
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		if(c<mark[a][b]) mark[a][b]=c;
		mark[b][a]=mark[a][b];
	}
	FOR(i,1,n) dfn[i]=i;
	do solve1(),solve2();
	while(next_permutation(dfn+1,dfn+n+1));
	printf("%d\n",ans);
	return 0;	
}

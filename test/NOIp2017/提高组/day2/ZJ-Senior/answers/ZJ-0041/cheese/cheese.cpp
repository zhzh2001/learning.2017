#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define FOR(i,a,b) for(int i=(a),_t=(b);i<=_t;++i)
#define DOR(i,a,b) for(int i=(a),_t=(b);i>=_t;--i)
#define ll long long
#define M 1105

struct node{
	ll x,y,z;	
}A[M];
vector<int> es[M];
bool mark[M];
int T,n;
ll h,r;
bool Check(int a,int b) {
	return (A[a].x-A[b].x)*(A[a].x-A[b].x)+(A[a].y-A[b].y)*(A[a].y-A[b].y)+(A[a].z-A[b].z)*(A[a].z-A[b].z)<=4*r*r;
}
void dfs(int x) {
	mark[x]=1;
	FOR(i,0,es[x].size()-1) {
		int y=es[x][i];
		if(mark[y]) continue;
		dfs(y);	
	}
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	FOR(t,1,T) {
		scanf("%d%lld%lld",&n,&h,&r);
		FOR(i,1,n) scanf("%lld%lld%lld",&A[i].x,&A[i].y,&A[i].z);
		FOR(i,1,n) if(A[i].z+r>=h) es[i].push_back(n+1);
		FOR(i,1,n) if(A[i].z<=r) es[0].push_back(i);
		FOR(i,1,n) FOR(j,1,n) {
			if(Check(i,j)) {
				es[i].push_back(j);
				es[j].push_back(i);
			}
		}
		dfs(0);
		if(mark[n+1]) puts("Yes");
		else puts("No");
		FOR(i,0,n+1) mark[i]=0;
		FOR(i,0,n+1) es[i].clear();
	}
	return 0;
}

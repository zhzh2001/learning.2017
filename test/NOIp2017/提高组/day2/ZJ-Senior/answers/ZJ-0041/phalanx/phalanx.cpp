#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

#define FOR(i,a,b) for(int i=(a),_t=(b);i<=_t;++i)
#define DOR(i,a,b) for(int i=(a),_t=(b);i>=_t;--i)
#define ll long long
#define M 300005

struct node{
	int x,y;	
}Q[M];
int n,m,q;
struct Tree{
	int L,R,sz;
	ll id;
}tree[M<<3];
struct W{
	ll id;
	int sp;	
};
void up(int p) {
	tree[p].sz=tree[p<<1].sz+tree[p<<1|1].sz;	
}
ll Id[M];
void build(int L,int R,int p) {
	tree[p].L=L,tree[p].R=R;
	if(L==R) {
		if(L<=m) {
			tree[p].sz=1;
			tree[p].id=Id[L];
		}
		return ;
	}
	int mid=L+R>>1;
	build(L,mid,p<<1);
	build(mid+1,R,p<<1|1);
	up(p);
}
W query(int sz,int p) {
	if(tree[p].L==tree[p].R) return (W){tree[p].id,tree[p].L};
	if(sz<=tree[p<<1].sz) return query(sz,p<<1);
	else return query(sz-tree[p<<1].sz,p<<1|1);
}
void update(int x,int p) {
	tree[p].sz--;
	if(tree[p].L==tree[p].R) return ;
	int mid=tree[p].L+tree[p].R>>1;
	if(x<=mid) update(x,p<<1);
	else update(x,p<<1|1);
}
void Insert(int x,ll id,int p) {
	tree[p].sz++;
	if(tree[p].L==tree[p].R){
		tree[p].id=id;
		return ;
	}
	int mid=tree[p].L+tree[p].R>>1;
	if(x<=mid) Insert(x,id,p<<1);
	else Insert(x,id,p<<1|1);
}
struct P1{
	void solve() {
		FOR(i,1,m) Id[i]=i;
		build(1,m+q,1);
		FOR(i,1,q) {
			W res=query(Q[i].y,1);
			printf("%lld\n",res.id);
			update(res.sp,1);
			Insert(m+i,res.id,1);
		}
	}
}P1;
struct P2{
	void solve(){
		FOR(i,1,m) Id[i]=i;
		FOR(i,2,n) Id[i+m-1]=1ll*i*m;
		m=m+n-1;
		build(1,m+q,1);
		FOR(i,1,q) {
			W res=query(Q[i].y,1);
			printf("%lld\n",res.id);
			update(res.sp,1);
			Insert(m+i,res.id,1);
		}
	}
}P2;
struct P3{
	int A[1005][1005];
	void solve() {
		int x,y,res;
		FOR(i,1,n) FOR(j,1,m) {
			A[i][j]=(i-1)*m+j;
		}
		FOR(t,1,q) {
			x=Q[t].x,y=Q[t].y,res=A[x][y];
			printf("%d\n",res);
			FOR(i,y,m-1) A[x][i]=A[x][i+1];
			FOR(i,x,n-1) A[i][m]=A[i+1][m];
			A[n][m]=res;
		}
	}
}P3;
bool Check() {
	FOR(i,1,q) if(Q[i].x!=1) return false;
	return true;
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);	
	scanf("%d%d%d",&n,&m,&q);
	FOR(i,1,q) scanf("%d%d",&Q[i].x,&Q[i].y);
	if(Check()) {
		if(n==1)P1.solve();
		else P2.solve();
	}
	else P3.solve();
	return 0;	
}	

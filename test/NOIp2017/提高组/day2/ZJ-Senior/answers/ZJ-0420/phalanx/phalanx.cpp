#include<cstdio>
#include<cstring>
#include<algorithm>
#define lowbit(x) ((x)&(-x))
using namespace std;

const int maxn = 400009;
int x[maxn], y[maxn], c[maxn];
int n, m, cnt, Q, a, b;

void update(int x, int y) {
	for (; x<maxn; x+=lowbit(x)) 
		c[x] += y;
}

int query(int x) {
	int res = 0;
	for (; x; x-=lowbit(x))
		res += c[x];
	return res;
}

int search(int x) {
	int l = 1, r = maxn-1, mid;
	while (l < r) {
		mid = (l + r) >> 1;
		if (query(mid) >= x) r = mid;
		else l = mid + 1;
	}
	return l;
}

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &Q);
	if (Q <= 20000) {
		for (int i=1; i<=Q; i++) {
			scanf("%d%d", &x[i], &y[i]);
			a = x[i]; b = y[i];
			for (int j=i-1; j>=1; j--)
				if (b == m) {
					if (x[j] <= a) a++;
					if (a == n+1) {
						a = x[j]; b = y[j];
					}
				}
				else {
					if (x[j] == a && y[j] <= b) b++;
				}
			printf("%d\n", (a-1)*m+b);
		}
		return 0;
	}
	for (int i=1; i<=m; i++) {
		x[i] = i; update(i, 1);
	}
	cnt = m;
	for (int j=2; j<=n; j++) {
		x[++cnt] = j*m; update(cnt, 1);
	}
	for (int i=1; i<=Q; i++) {
		scanf("%d%d", &a, &b);
		b = search(b);
		update(b, -1);
		x[++cnt] = x[b];
		printf("%d\n", x[b]);
		update(cnt, 1);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;

const int maxn = 13;
const int maxState = (1<<12) + 9;
LL f[maxn][maxState], ans = 1e18;
int g[maxState][maxState], h[maxState][maxn], w[maxn][maxn];
int n, m, a, b, c, ALL, S, best;

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	
	scanf("%d%d", &n, &m); ALL = (1 << n) - 1;
	memset(w, -1, sizeof w);
	for (int i=1; i<=m; i++) {
		scanf("%d%d%d", &a, &b, &c);
		a--; b--;
		if (w[a][b] == -1) w[b][a] = w[a][b] = c;
		else w[b][a] = w[a][b] = min(w[a][b], c);
	}
	
	memset(h, -1, sizeof h);
	for (int i=0; i<=ALL; i++)
		for (int j=0; j<=n-1; j++) 
			for (int k=0; k<=n-1; k++)
				if ((i>>k)&1) {
					if (h[i][j] == -1) h[i][j] = w[j][k];
					else if (w[j][k] != -1) h[i][j] = min(h[i][j], w[j][k]);
				}

	memset(g, -1, sizeof g);
	for (int i=0; i<=ALL; i++) {
		S = (~i) & ALL;
		for (int j=S; j; j=(j-1)&S) {
			best = 0;
			for (int k=0; k<=n-1; k++)
				if ((j>>k)&1) {
					if (best != -1 && h[i][k] != -1) best += h[i][k];
					else best = -1;
				}
			g[i][j] = best;
		}
	}
	
	memset(f, 0x3f, sizeof f);
	for (int i=0; i<=n-1; i++) f[1][1<<i] = 0;
	for (int i=1; i<=n-1; i++)
		for (int j=0; j<=ALL; j++) if (f[i][j] <= 1e18) {
			S = (~j) & ALL;
			for (int k=S; k; k=(k-1)&S)
				if (g[j][k] != -1)
					f[i+1][j|k] = min(f[i+1][j|k], f[i][j] + 1LL * i * g[j][k]);
		}
		
	for (int i=1; i<=n; i++) ans = min(ans, f[i][ALL]);
	printf("%lld\n", ans);
		
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;

const int maxn = 100009;
struct point {
	LL x, y, z;
} p[maxn];
LL pa[maxn], n, T;
LL h, r, a, b;

LL getpa(LL x) {
	return pa[x] == x ? x : pa[x] = getpa(pa[x]);
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	
	scanf("%lld", &T);
	while (T--) {
		scanf("%lld%lld%lld", &n, &h, &r); r *= 2;
		a = n+1; b = n+2; pa[a] = a; pa[b] = b;
		for (int i=1; i<=n; i++) {
			scanf("%lld%lld%lld", &p[i].x, &p[i].y, &p[i].z);
			pa[i] = i;
			if (abs(p[i].z) <= r) pa[getpa(i)] = getpa(a);
			if (abs(p[i].z-h) <= r) pa[getpa(i)] = getpa(b);
		}
		for (int i=1; i<=n; i++)
			for (int j=i+1; j<=n; j++)
				if ((p[i].x-p[j].x)*(p[i].x-p[j].x) +
					(p[i].y-p[j].y)*(p[i].y-p[j].y) +
					(p[i].z-p[j].z)*(p[i].z-p[j].z) <= r*r)
						pa[getpa(i)] = getpa(j);
		if (getpa(a) == getpa(b)) puts("Yes");
		else puts("No");
	}
	
	return 0;
}

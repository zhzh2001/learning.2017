#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<string>
#include<queue>
#include<vector>
using namespace std;
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define ll long long
#define du double
#define pb push_back
template <class Ti> void chk_mi(Ti &x,Ti y){if(x>y||x==-1)x=y;}
template <class Ti> void chk_mx(Ti &x,Ti y){if(x<y)x=y;}
template <class Ti> Ti Max(Ti x,Ti y){return x>y?x:y;}
template <class Ti> Ti Min(Ti x,Ti y){return x<y?x:y;}
template <class Ti> Ti Abs(Ti x){return x>=0?x:-x;}
//文件名  输出调试  乘法  long long mod 内存
int dis[15][15];
int n,m;
struct P70{
	int dp[20000000];
	int Pow[10];
	int A[10];
	int dfs(int A[],int cnt,int sum){
		if(cnt==n)return 0;
		if(dp[sum]!=-1)return dp[sum];
		FOR(i,1,n){
			if(A[i])continue;
			FOR(j,1,n){
				if(!A[j])continue;
				if(dis[j][i]==-1)continue;
				A[i]=A[j]+1;
				int t=dfs(A,cnt+1,sum+Pow[i]*A[i]);
				if(t!=-1)chk_mi(dp[sum],t+A[j]*dis[j][i]);
				A[i]=0;
			}
		}
		return dp[sum];
	}
	void solve(){
		memset(dp,-1,sizeof(dp));
		memset(A,0,sizeof(A));
		Pow[1]=1;
		FOR(i,2,n)Pow[i]=Pow[i-1]*n;
		int ans=-1;
		FOR(i,1,n){
			A[i]=1;
			int x=dfs(A,1,Pow[i]);
			if(x!=-1)chk_mi(ans,x);
			A[i]=0;
		}
		printf("%d\n",ans);
	}
}p70;
struct SHUI{
	int fa[15];
	int find(int x){return x==fa[x]?x:fa[x]=find(fa[x]);}
	int To[30],V[30],head[30],nxt[30],ttaE;
	void addedge(int a,int b,int c){nxt[++ttaE]=head[a],head[a]=ttaE,To[ttaE]=b,V[ttaE]=c;}
	#define LFOR(i,x) for(int i=head[x];i;i=nxt[i])
	struct node{
		int x,y,v;
		bool operator <(const node &_)const{
			return v<_.v;
		}
	}edge[205];
	int val;
	void dfs(int x,int f,int dep){
		LFOR(i,x){
			int y=To[i];
			if(y==f)continue;
			val+=dep*V[i];
			dfs(y,x,dep+1);
		}
	}
	void solve(){
		int tta=0;ttaE=0;
		FOR(i,1,n)head[i]=0;
		FOR(i,1,n)fa[i]=i;
		FOR(i,1,n)FOR(j,1,n){
			if(dis[i][j]==-1)continue;
			edge[++tta].x=i,edge[tta].y=j,edge[tta].v=dis[i][j];
		}
		sort(edge+1,edge+1+tta);
		FOR(i,1,tta){
			int a=find(edge[i].x),b=find(edge[i].y);
			if(a!=b){
				fa[a]=b;
				addedge(edge[i].x,edge[i].y,edge[i].v);
				addedge(edge[i].y,edge[i].x,edge[i].v);
			}
		}
		int ans=-1;
		FOR(i,1,n){
			val=0;
			dfs(i,0,1);
			chk_mi(ans,val);
		}
		printf("%d\n",ans);
	}
}pp;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dis,-1,sizeof(dis));
	FOR(i,1,m){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		chk_mi(dis[x][y],z);
		chk_mi(dis[y][x],z);
	}
	if(n<=8)p70.solve();
	else pp.solve();
	return 0;
}

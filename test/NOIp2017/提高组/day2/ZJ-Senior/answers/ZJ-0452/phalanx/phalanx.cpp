#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<string>
#include<queue>
#include<vector>
using namespace std;
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define ll long long
#define du double
#define pb push_back
template <class Ti> void chk_mi(Ti &x,Ti y){if(x>y)x=y;}
template <class Ti> void chk_mx(Ti &x,Ti y){if(x<y)x=y;}
template <class Ti> Ti Max(Ti x,Ti y){return x>y?x:y;}
template <class Ti> Ti Min(Ti x,Ti y){return x<y?x:y;}
template <class Ti> Ti Abs(Ti x){return x>=0?x:-x;}
//文件名  输出调试  乘法  long long mod 内存
int n,m,q;
struct P30{
	int Num[10005][10005];
	void solve(){
		FOR(i,1,n)FOR(j,1,m)Num[i][j]=i*m-m+j;
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			printf("%d\n",Num[x][y]);
			int now=Num[x][y];
			FOR(i,y,m)Num[x][i]=Num[x][i+1];
			FOR(i,x,n)Num[i][m]=Num[i+1][m];
			Num[n][m]=now;
		}
	}
}p30;
struct P50{
	static const int M=200005;
	struct Segment_Tree{
		struct node{int l,r,sum;}tree[M<<2];
		void up(int p){tree[p].sum=tree[p<<1].sum+tree[p<<1|1].sum;} 
		void build(int l,int r,int p){
			tree[p].l=l,tree[p].r=r;
			if(l==r){tree[p].sum=1;return;}
			int mid=(l+r)>>1;
			build(l,mid,p<<1),build(mid+1,r,p<<1|1);
			up(p);
		}
		void update(int x,int p){
			if(tree[p].l==tree[p].r){
				tree[p].sum=0;
				return;
			}
			int mid=(tree[p].l+tree[p].r)>>1;
			if(mid>=x)update(x,p<<1);
			else update(x,p<<1|1);
			up(p);
		}
		int query(int x,int p){
			if(tree[p].l==tree[p].r)return tree[p].r;
			if(tree[p<<1].sum>=x)return query(x,p<<1);
			else return query(x-tree[p<<1].sum,p<<1|1);	
		}
	}Tree;
	int id[M];
	void solve(){//利用线段树查询前面的空格个数 
		FOR(i,1,m)id[i]=i;
		Tree.build(1,n+q,1);
		int now=n;
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			int pos=Tree.query(y,1);
			printf("%d\n",id[pos]);
			Tree.update(pos,1);
			id[++now]=id[pos];
			id[pos]=0;
		}
	}
}p50;
struct P60{
	static const int M=600005;
	struct Segment_Tree{
		struct node{int l,r,sum;}tree[M<<2];
		void up(int p){tree[p].sum=tree[p<<1].sum+tree[p<<1|1].sum;} 
		void build(int l,int r,int p){
			tree[p].l=l,tree[p].r=r;
			if(l==r){tree[p].sum=1;return;}
			int mid=(l+r)>>1;
			build(l,mid,p<<1),build(mid+1,r,p<<1|1);
			up(p);
		}
		void update(int x,int p){
			if(tree[p].l==tree[p].r){
				tree[p].sum=0;
				return;
			}
			int mid=(tree[p].l+tree[p].r)>>1;
			if(mid>=x)update(x,p<<1);
			else update(x,p<<1|1);
			up(p);
		}
		int query(int x,int p){
			if(tree[p].l==tree[p].r)return tree[p].r;
			if(tree[p<<1].sum>=x)return query(x,p<<1);
			else return query(x-tree[p<<1].sum,p<<1|1);	
		}
	}Tree;
	int id[M];
	void solve(){
		int now=0;
		FOR(i,1,m)id[++now]=i;
		FOR(j,2,n)id[++now]=j*m;
		Tree.build(1,now+q,1);
		while(q--){
			int x,y;
			scanf("%d%d",&x,&y);
			int pos=Tree.query(y,1);
			printf("%d\n",id[pos]);
			Tree.update(pos,1);
			id[++now]=id[pos];
			id[pos]=0;
		}
	}
}p60;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=10000&&m<=10000&&q<=1000)p30.solve();
	else if(n==1)p50.solve();
	else p60.solve();
	return 0;
}


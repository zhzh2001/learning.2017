#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<ctime>
#include<iostream>
#include<algorithm>
#include<string>
#include<queue>
#include<vector>
using namespace std;
#define FOR(i,x,y) for(int i=(x);i<=(y);i++)
#define DOR(i,x,y) for(int i=(x);i>=(y);i--)
#define ll long long
#define du double
#define pb push_back
template <class Ti> void chk_mi(Ti &x,Ti y){if(x>y)x=y;}
template <class Ti> void chk_mx(Ti &x,Ti y){if(x<y)x=y;}
template <class Ti> Ti Max(Ti x,Ti y){return x>y?x:y;}
template <class Ti> Ti Min(Ti x,Ti y){return x<y?x:y;}
template <class Ti> Ti Abs(Ti x){return x>=0?x:-x;}
//文件名  输出调试  乘法  long long mod 内存
int n,h,r;
bool mark[1005];
struct node{
	int x,y,z;
}A[1005];	
ll ned;
bool check(node a,node b){
	ll res=1ll*(a.x-b.x)*(a.x-b.x)+1ll*(a.y-b.y)*(a.y-b.y)+1ll*(a.z-b.z)*(a.z-b.z);
	return res<=ned;
}
void dfs(int now){
	if(mark[now])return;
	mark[now]=1;
	FOR(i,1,n){
		if(mark[i])continue;
		if(check(A[now],A[i])){
			dfs(i);
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		memset(mark,0,sizeof(mark));
		scanf("%d%d%d",&n,&h,&r);
		r*=2;
		ned=1ll*r*r;
		FOR(i,1,n)scanf("%d%d%d",&A[i].x,&A[i].y,&A[i].z);
		FOR(i,1,n)if(A[i].z-r<=0&&!mark[i])dfs(i);
		bool flag=0;
		FOR(i,1,n)if(A[i].z+r>=h&&mark[i]){flag=1;break;}
		if(flag)puts("Yes");
		else puts("No");
	}
	return 0;
}

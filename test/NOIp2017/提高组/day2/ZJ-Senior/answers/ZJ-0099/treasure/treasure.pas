var
  i,j,te,n,m,ans,now,min,x,y,z,gg:longint;
  rank,pl:array[0..1001]of longint;
  a:array[0..1001,0..1001]of longint;
  b:array[0..1001]of boolean;
  bo:boolean;
procedure solve;
begin
  now:=0;
  rank[pl[1]]:=1;
  for i:=2 to n do
    begin
      min:=1000000000;
      for j:=1 to i-1 do
        if a[pl[i],pl[j]]*rank[pl[j]]<min then
        begin
          min:=a[pl[i],pl[j]]*rank[pl[j]];
          te:=j;
        end;
      rank[pl[i]]:=rank[pl[te]]+1;
      now:=now+min;
      if now>=ans then break;
    end;
  if now<ans then ans:=now;
  inc(gg);
  if gg>200000 then bo:=true;
end;
procedure qpl(x:longint);
var
  i:longint;
begin
  if x=n+1 then
  begin
    solve;
    exit;
  end;
  for i:=1 to n do
    if not b[i] then
      begin
        pl[x]:=i;
        b[i]:=true;
        qpl(x+1);
        if bo then exit;
        b[i]:=false;
      end;
end;
begin
  assign(input,'treasure.in');
  reset(input);
  assign(output,'treasure.out');
  rewrite(output);
  read(n,m);
  for i:=1 to n do
    for j:=1 to n do
      a[i,j]:=1000000000;
  for i:=1 to m do
    begin
      read(x,y,z);
      if x=y then continue;
      if z<a[x,y] then
      begin
        a[x,y]:=z;
        a[y,x]:=z;
      end;
    end;
  ans:=1000000000;
  bo:=false;
  qpl(1);
  write(ans);
  close(input);
  close(output);
end.

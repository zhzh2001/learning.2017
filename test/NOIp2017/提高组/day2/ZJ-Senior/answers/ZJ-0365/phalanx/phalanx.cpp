#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;

int n,m,q,qry[300010][2],a[1000010],t[300010],c[510][100010],cnt,id[50010],d[1010][1010];

struct node{
	int tr[50010];
	int lowbit(int x){return x&(-x);}
	int sum(int pos)
		{
			int ret=0;while(pos>0){ret+=tr[pos];pos-=lowbit(pos);}return ret;
		}
	void alt(int pos,int x){while(pos<=m){tr[pos]+=x;pos+=lowbit(pos);}}
}b[510];

int rd()
{
	int ret=0;char ch=getchar();while(ch<'0'||ch>'9') ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();return ret;
}

int lowbit(int x){return x&(-x);}

int sum(int pos)
{
	int ret=0;while(pos>0){ret+=t[pos];pos-=lowbit(pos);}return ret;
}

void al(int pos,int x)
{
	while(pos<=m){t[pos]+=x;pos+=lowbit(pos);}
}

int main()
{
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	n=rd();m=rd();q=rd();for(int i=1;i<=q;i++) qry[i][0]=rd(),qry[i][1]=rd();
	bool flag=1;for(int i=1;i<=q;i++) if(qry[i][0]!=1){flag=0;break;}
	if(flag)
		{
			for(int i=1;i<=m;i++) a[i]=i;int tmp=m;
			for(int i=m+1;i<m+n;i++) tmp+=m,a[i]=tmp;
			for(int i=1;i<=q;i++)
				{
					int y=qry[i][1],j=sum(y);printf("%d\n",a[y+j]);
					a[n+m+i-1]=a[y+j];al(y,1);
				}
		}
	else
		{
			if(n<=1000&&m<=1000)
				{
					int cnt=0;
					for(int i=1;i<=n;i++) for(int j=1;j<=m;j++) d[i][j]=++cnt;
					for(int k=1;k<=q;k++)
						{
							int x=qry[k][0],y=qry[k][1],ans=d[x][y];printf("%d\n",ans);
							for(int i=y;i<m;i++) d[x][i]=d[x][i+1];
							for(int i=x;i<n;i++) d[i][m]=d[i+1][m];
							d[n][m]=ans;
						}
					return 0;
				}
			for(int i=1;i<=n;i++) c[0][i]=i*m;
			for(int i=1;i<=q;i++)
				{
					int x=qry[i][0],y=qry[i][1];
					if(id[x]==0) 
						{
							id[x]=++cnt;for(int j=1;j<m;j++) c[cnt][j]=(x-1)*m+j;
							c[cnt][0]=m-1;
						}
					int k=id[x],j=b[k].sum(y);b[k].alt(y,1);
					if(y+j<=c[k][0])
						{
							printf("%d\n",c[k][y+j]);int tmp=b[0].sum(x);
							c[k][++c[k][0]]=c[0][x+tmp];
							b[0].alt(x,1);c[0][m+i]=c[k][y+j];
						}
					else
						{
							j=x+y+j-c[k][0]-1;int tmp=b[0].sum(j);
							printf("%d\n",c[0][j+tmp]);b[0].alt(j,1);
							c[0][m+i]=c[0][j+tmp];
						}
				}
		}
	return 0;
}

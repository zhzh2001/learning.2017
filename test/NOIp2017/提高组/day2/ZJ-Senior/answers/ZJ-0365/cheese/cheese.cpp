#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;

struct node{
	long long x,y,z;
}a[1010];

int t,n,q[10010];
long long h,r;
bool vis[1010];

int rd()
{
	int ret=0;char ch=getchar();while(ch<'0'||ch>'9') ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();return ret;
}

long long rdll()
{
	long long ret=0;char ch=getchar();while((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	bool flag=0;if(ch=='-') flag=1;else ret=ch-'0';ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();
	if(flag) return -ret;else return ret;
}

long long sqr(long long a){return a*a;}

long long calc(node u,node v){return sqr(u.x-v.x)+sqr(u.y-v.y)+sqr(u.z-v.z);}

int main()
{
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	t=rd();
	while(t--)
		{
			n=rd();h=rdll();r=rdll();memset(vis,0,sizeof(vis));int ll=1,rr=0;
			for(int i=1;i<=n;i++) a[i].x=rdll(),a[i].y=rdll(),a[i].z=rdll();
			for(int i=1;i<=n;i++) if(a[i].z<=r) vis[i]=1,q[++rr]=i;
			bool ans=0;long long tmp=r*r*4;
			while(ll<=rr)
				{
					int u=q[ll++];if(a[u].z+r>=h){ans=1;break;}
					for(int i=1;i<=n;i++) if(!vis[i])
						{
							double dis=calc(a[u],a[i]);
							if(dis<=tmp) 
								{
									q[++rr]=i;vis[i]=1;if(a[i].z+r>=h){ans=1;break;}
								}
						}
					if(ans) break;
				}
			if(ans) printf("Yes\n");else printf("No\n");
		}
	return 0;
}

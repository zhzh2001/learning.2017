#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
using namespace std;

int n,m,map[15][15],dp[15],q[1010];
long long ans,now;

int rd()
{
	int ret=0;char ch=getchar();while(ch<'0'||ch>'9') ch=getchar();
	while(ch<='9'&&ch>='0') ret=ret*10+ch-'0',ch=getchar();return ret;
}

void bfs(int vs)
{
	int l=1,r=1;q[1]=vs;dp[vs]=1;
	while(l<=r)
		{
			int u=q[l++];
			for(int v=1;v<=n;v++)
				{
					if(dp[v]!=-1||map[u][v]==1e9) continue;
					dp[v]=dp[u]+1;q[++r]=v;now+=1LL*dp[u]*map[u][v];
				}
		}
}

int main()
{
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	n=rd();m=rd();ans=1e12;for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)map[i][j]=1e9;
	for(int i=1;i<=m;i++)
		{
			int u=rd(),v=rd(),c=rd();map[u][v]=map[v][u]=min(map[u][v],c);
		}
	for(int i=1;i<=n;i++)
		{
			now=0;memset(dp,-1,sizeof(dp));bfs(i);ans=min(ans,now);
		}
	printf("%d",ans);
	return 0;
}

#include<cstdio>
#include<cctype>
inline int gi(){
	register int x = 0, f = 1;
	register char ch = getchar();
	while(!isdigit(ch) && ch ^ 45)ch = getchar();
	if(!(ch ^ 45))ch = getchar(), f = -1;
	for(;isdigit(ch);ch = getchar())x = (x << 1) + (x << 3) + (ch ^ 48);
	return x * f;
}
#include<cstdlib>
#include<cstring>
#include<algorithm>
#include<queue>
namespace BAOLI{
	const int MAXN = 1010;
	long long x[MAXN][MAXN];
	inline int slove(int n, int m, int q){
		for(int i = 1;i <= n;i++){
			for(int j = 1;j <= m;j++){
				x[i][j] = (i - 1) * m + j;
			}
		}
		while(q--){
			int a = gi(), b = gi();
			long long c = x[a][b];
			printf("%lld\n", c);
			for(int i = b;i < m;i++)x[a][i] = x[a][i + 1];
			for(int i = a;i < n;i++)x[i][m] = x[i + 1][m];
			x[n][m] = c;
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
}
namespace SGT{
	const int MAXN = 300010;
	long long vl[MAXN << 2], lt[MAXN << 2];
	inline void pushDown(int x){
		if(!lt[x])return;
		lt[x << 1] += lt[x];lt[x << 1 | 1] += lt[x];
		vl[x << 1] += lt[x];vl[x << 1 | 1] += lt[x];
		lt[x] = 0;
	}
	inline void pushUp(int x){
		vl[x]  = vl[x << 1] + vl[x << 1 | 1];
	}
	inline void Build(int n, int l, int r){
		if(l == r){
			vl[n] = l;
			return;
		}
		int mid = (l + r) >> 1;
		Build(n << 1, l, mid);
		Build(n << 1 | 1, mid + 1, r);
		pushUp(n);
	}
	inline void add(int n, int l, int r, int L, int R){
		if(l == L && r == R){
			lt[n] += 1;
			vl[n] += r - l + 1;
			return;
		}
		int mid = (l + r) >> 1;
		pushDown(n);
		if(R <= mid)add(n << 1, l, mid, L, R);
		else if(L > mid)add(n << 1 | 1, mid + 1, r, L, R);
		else add(n << 1, l, mid, L, mid), add(n << 1 | 1, mid + 1, r, mid + 1, R);
		pushUp(n);
	}
	inline void Modify(int n, int l, int r, int x, long long v){
		if(l == r)vl[n] = v, lt[n] = 0;
		int mid = (l + r) >> 1;
		pushDown(n);
		if(x <= mid)Modify(n << 1, l, mid, x, v);
		else Modify(n << 1 | 1, mid + 1, r, x, v);
	}
	inline long long Query(int n, int l, int r, int x){
		if(l == r)return vl[n];
		int mid = (l + r) >> 1;
		pushDown(n);
		if(x <= mid)return Query(n << 1, l, mid, x);
		else return Query(n << 1 | 1, mid + 1, r, x);
	}
	inline int slove(int n, int m, int q){
		Build(1, 1, m);
		while(q--){
			gi();
			int p = gi();
			long long od;
			printf("%lld\n", od = Query(1, 1, m, p));
			add(1, 1, m, p, m);
			Modify(1, 1, m, m, od);
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
}
class Splay{
	private:
	struct node{
		node *f, *l, *r;
		long long v;
		int s;
		inline node(){
			v = 0;s = 1;
			f = l = r = NULL;
		}
	}*root;
	inline void pushUp(node *x){
		x->s = 1;
		if(x->l)x->s += x->l->s;
		if(x->r)x->s += x->r->s;
	}
	inline void rightR(node *x, node *f){
		f->l = x->r;
		if(f->l)f->l->f = f;
		x->f = f->f;
		if(f->f){
			if(f->f->l == f)f->f->l = x;
			if(f->f->r == f)f->f->r = x;
		}
		x->r = f;f->f = x;
		pushUp(f);pushUp(x);
	}
	inline void leftR(node *x, node *f){
		f->r = x->l;
		if(f->r)f->r->f = f;
		x->f = f->f;
		if(f->f){
			if(f->f->l == f)f->f->l = x;
			if(f->f->r == f)f->f->r = x;
		}
		x->l = f;f->f = x;
		pushUp(f);pushUp(x);
	}
	inline void splay(node *x, node *goal){
		while(x->f != goal){
			if(x->f->f != goal){
				if(x->f->f->l == x->f){
					if(x->f->l == x)rightR(x->f, x->f->f),rightR(x, x->f);
					else leftR(x, x->f), rightR(x, x->f);
				}else{
					if(x->f->l == x)rightR(x, x->f), leftR(x, x->f);
					else leftR(x->f, x->f->f), leftR(x, x->f);
				}
			}else{
				if(x->f->l == x)rightR(x, x->f);
				else leftR(x, x->f);
			}
		}
		if(x->f == NULL)root = x;
	}
	inline node* Query(int pos){
		pos++;
		node *p = root;
		while(1){
			if((!p->l && pos == 1) || (p->l && pos == p->l->s + 1))return p;
			if(p->l && pos <= p->l->s)p = p->l;
			else pos -= p->l ? p->l->s + 1 : 1, p = p->r;
		}
	}
	public:
	inline void Build(int n, const long long *a){
		root = new node();
		node *p = root;
		p->v = -1;
		for(int i = 1;i <= n;i++){
			p->r = new node();
			p->r->f = p;
			p = p->r;
			p->v = a[i];
		}
		p->r = new node();
		p->r->f = p;
		p = p->r;
		p->v = -1;
		while(p){
			pushUp(p);
			p = p->f;
		}
	}
	inline long long Delete(int pos){
		node *l = Query(pos - 1);
		splay(l, NULL);
		node *r = Query(pos + 1);
		splay(r, l);
		long long val = r->l->v;
		delete r->l;
		r->l = NULL;
		node *p = r;
		while(p)pushUp(p), p = p->f;
		return val;
	}
	inline void Insert(long long val){
		node *p = root;
		while(p->r)p = p->r;
		if(p->l){
			p = p->l;
			while(p->r)p = p->r;
			p->r = new node();
			p->r->f = p;p->r->v = val;
		}else{
			p->l = new node();
			p->l->f = p;p->l->v = val;
		}
		while(p)pushUp(p), p = p->f;
	}
};
int qx[300010], qy[300010];
namespace splaydo{
	const int MAXN = 300010;
	long long dt[MAXN];
	std::queue<long long> Q;
	inline int slove(int n, int m, int q){
		Splay a;
		for(int i = 1;i <= m;i++)dt[i] = i;
		a.Build(m - 1, dt);
		for(int i = 1;i <= n;i++)Q.push((long long)(i - 1) * m + m);
		for(int i = 1;i <= q;i++){
			int p = qy[i];
			if(p == m){
				printf("%lld\n", Q.front());
				Q.push(Q.front());Q.pop();
				continue;
			}
			long long val = a.Delete(p);
			printf("%lld\n", val);
			//printf("Inset %d\n", Q.front());
			a.Insert(Q.front());Q.pop();
			Q.push(val);
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
}
namespace pianfen{
	int ind[300010], cnt = 1;
	long long tmp1[300010], tmp2[300010];
	Splay a[300010], b;
	inline int slove(int n, int m, int q){
		memset(ind, 0, sizeof(ind));
		for(int i = 1;i <= q;i++){
			if(!ind[qx[i]])ind[qx[i]] = cnt++;
		}
		for(int i = 1;i <= n;i++){
			if(ind[i]){
				for(int j = 1;j <= m;j++)tmp1[j] = (long long)(i - 1) * m + j;
				a[ind[i]].Build(m - 1, tmp1);
			}
			tmp2[i] = (long long)(i - 1) * m + m;
		}
		b.Build(n, tmp2);
		for(int i = 1;i <= q;i++){
			int x = ind[qx[i]], y = qy[i];
			if(y == m){
				long long val = b.Delete(qx[i]);
				printf("%lld\n", val);
				b.Insert(val);
			}else{
				long long val = a[x].Delete(y);
				printf("%lld\n", val);
				//int vval = b.Delete(qx[i]);
				//printf("Ins %d\n", vval);
				a[x].Insert(b.Delete(qx[i]));
				b.Insert(val);
			}
		}
		fclose(stdin);
		fclose(stdout);
		return 0;
	}
}
int main(){
	#ifndef DEBUG
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	#endif
	int n = gi(), m = gi(), q = gi();
	if(n <= 1000 && m <= 1000)return BAOLI::slove(n, m, q);
	if(n == 1)return SGT::slove(n, m, q);
	bool flag = 1;
	for(int i  = 1;i <= q;i++){
		qx[i] = gi(), qy[i] = gi();
		if(qx[i] != 1)flag = 0;
	}
	if(flag)return splaydo::slove(n, m, q);
	return pianfen::slove(n, m, q);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cctype>
inline int gi(){
	register int x = 0, f = 1;
	register char ch = getchar();
	while(!isdigit(ch) && ch ^ 45)ch = getchar();
	if(!(ch ^ 45))ch = getchar(), f = -1;
	for(;isdigit(ch);ch = getchar())x = (x << 1) + (x << 3) + (ch ^ 48);
	return x * f;
}
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define EPS 1e-9
#define MAXN 1010
#define MAXM 1020100
int head[MAXN], to[MAXM], next[MAXM], tot;
inline void addEdge(int u, int v){
	next[tot] = head[u];to[tot] = v;head[u] = tot++;
	next[tot] = head[v];to[tot] = u;head[v] = tot++;
}
int t, n, h, r;
inline double dis(double x1, double y1, double z1, double x2, double y2, double z2){
	return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2) + (z1 - z2) * (z1 - z2));
}
int x[MAXN], y[MAXN], z[MAXN], vst[MAXN];
void dfs(int x){
	vst[x] = 1;
	for(int i = head[x];~i;i = next[i])
		if(!vst[to[i]])
			dfs(to[i]);
}
int main(){
	#ifndef DEBUG
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	#endif
	t = gi();
	while(t--){
		n = gi();h = gi();r = gi();
		memset(head, -1, sizeof(head));tot = 0;
		for(int i = 1;i <= n;i++){
			x[i] = gi(), y[i] = gi(), z[i] = gi();
			for(int j = 1;j < i;j++)
				if(dis(x[i], y[i], z[i], x[j], y[j], z[j]) - 2 * r <= EPS)addEdge(i, j);
			if(z[i] <= r)addEdge(0, i);
			if(z[i] + r >= h)addEdge(i, n + 1);
		}
		memset(vst, 0, sizeof(vst));
		dfs(0);
		puts(vst[n + 1] ? "Yes" : "No");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

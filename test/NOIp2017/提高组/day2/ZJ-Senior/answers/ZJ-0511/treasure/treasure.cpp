#include<cstdio>
#include<cctype>
inline int gi(){
	register int x = 0, f = 1;
	register char ch = getchar();
	while(!isdigit(ch) && ch ^ 45)ch = getchar();
	if(!(ch ^ 45))ch = getchar(), f = -1;
	for(;isdigit(ch);ch = getchar())x = (x << 1) + (x << 3) + (ch ^ 48);
	return x * f;
}
#include<cstdlib>
#include<cstring>
#include<algorithm>
#define MAXN 15
#define MAXM 1010
int head[MAXN], to[MAXM], next[MAXM], val[MAXM], tot = 0;
inline void addEdge(int u, int v, int w){
	next[tot] = head[u];to[tot] = v;val[tot] = w;head[u] = tot++;
	next[tot] = head[v];to[tot] = u;val[tot] = w;head[v] = tot++;
}
int u[MAXM], v[MAXM], w[MAXM], cnt = 0, ans = 0x7FFFFFFF;
inline int count(int x){
	register int ret = 0;
	while(x)ret+=x&1,x>>=1;
	return ret;
}
int n, m;
inline int calc(int x, int fa, int deep){
	register int ret = 0;
	for(int i = head[x];~i;i = next[i]){
		if(to[i] == fa)continue;
		ret += val[i] * deep;
		ret += calc(to[i], x, deep + 1);
	}
	return ret;
}
int p[MAXN], P[MAXN];
inline int find(int x){return x == p[x] ? x : p[x] = find(p[x]);}
inline void update(int stat){
	memset(head, -1, sizeof(int) * (n + 1));tot = 0;
	memcpy(p, P, sizeof(int) * (n + 1));
	for(int i = 0;i < cnt;i++){
		if((stat>>i)&1){
			int U = find(u[i]), V = find(v[i]);
			if(U == V)return;
			addEdge(u[i], v[i], w[i]);
			p[U] = V;
		}
	}
	register int ret = 0x7FFFFFFF;
	for(int i = 1;i <= n;i++){
		ret = std::min(ret, calc(i, 0, 1));
	}
	ans = std::min(ans, ret);
}
void dfs(int deep, int stat, int cost){
	register int res;
	if((res = count(stat)) == n - 1){
		update(stat);
		return;
	}
	if(deep >= cnt)return;
	if(cost > ans)return;
	if(cnt - deep < n - res - 1)return;
	dfs(deep + 1, stat | (1 << deep), cost + w[deep]);
	dfs(deep + 1, stat, cost);
}
int mp[MAXN][MAXN];
int main(){
	#ifndef DEBUG
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	#endif
	n = gi(), m = gi();
	for(int i = 1;i <= n;i++)P[i] = i;
	memset(mp, 0x3F, sizeof(mp));
	for(int i = 1;i <= m;i++){
		int u = gi(), v = gi(), w = gi();
		if(u > v)std::swap(u, v);
		mp[u][v] = std::min(mp[u][v], w);
	}
	
	for(int i = 1;i <= n;i++){
		for(int j = i + 1;j <= n;j++){
			if(mp[i][j] != mp[0][0]){
				//addEdge(i, j, map[i][j]);
				u[cnt] = i;v[cnt] = j;w[cnt] = mp[i][j];
				cnt++;
			}
		}
	}
	
	for(int i = 1;i <= n;i++){
		for(int j = 1;j < n;j++){
			if(w[i] > w[j]){
				std::swap(w[i], w[j]);
				std::swap(u[i], u[j]);
				std::swap(v[i], v[j]);
			}
		}
	}
	dfs(0, 0, 0);
	printf("%d\n", ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

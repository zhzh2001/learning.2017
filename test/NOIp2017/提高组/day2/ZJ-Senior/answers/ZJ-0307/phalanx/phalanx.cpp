#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

template <class T>void Pt(T x){
	if(x==0)return;
	Pt(x/10);
	putchar((x%10)^'0');
}

template <class T>void Pf(T x){
	if(x==0){putchar('0');return;}
	Pt(x);
}

#define M 300005

int n,m,q;

struct que{
	int x,y;
}Q[M];

struct _30{
	
	int A[1005][1005];
	
	void solve(){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=(i-1)*m+j;
		for(int c=1,x,y;c<=q;c++){
//			Rd(x),Rd(y);
			x=Q[c].x,y=Q[c].y;
			int t=A[x][y];
			printf("%d\n",t);
			for(int i=y;i+1<=m;i++)A[x][i]=A[x][i+1];
			for(int i=x;i+1<=n;i++)A[i][m]=A[i+1][m];
			A[n][m]=t;
		}
	}
	
}P30;

int d,u;
long long id[M<<1];

struct _x1{
	
	bool check(){
		for(int i=1;i<=q;i++)
			if(Q[i].x!=1)return false;
		return true;
	}
	
	struct Seg{
		
		long long num[M*12];
		int cnt[M*12];
		
		void Up(int p){
			cnt[p]=cnt[p<<1]+cnt[p<<1|1];
		}
		
		void Build(int L=d,int R=u,int p=1){
			if(L==R){num[p]=id[L],cnt[p]=(id[L]>0);return;}
			int mid=(L+R)>>1;
			Build(L,mid,p<<1),Build(mid+1,R,p<<1|1);
			Up(p);
//			cout<<"!"<<L<<" "<<R<<" "<<p<<" "<<cnt[p]<<endl;
		}
		
		void Update(int x,long long v,int L=1,int R=u,int p=1){
			if(L==R){
				num[p]=v;
				cnt[p]=(v>0);
				return;
			}
			int mid=(L+R)>>1;
			if(mid>=x)Update(x,v,L,mid,p<<1);
			else Update(x,v,mid+1,R,p<<1|1);
			Up(p);
		}
		
		pair<int,int> Query(int x,int L=d,int R=u,int p=1){
//			cout<<x<<" "<<L<<" "<<R<<" "<<p<<endl;
			if(L==R)return pair<int,int>(L,p);
			int mid=(L+R)>>1,ls=p<<1,rs=p<<1|1;
			if(cnt[ls]>=x)return Query(x,L,mid,ls);
			else return Query(x-cnt[ls],mid+1,R,rs);
		}
		
	}T;
	
	void solve(){
		d=1,u=n+m-1+q;
		for(int i=d;i<=u;i++)id[i]=0;
		for(int i=1;i<=m;i++)id[i]=i;
		for(int i=2;i<=n;i++)id[m+i-1]=1ll*i*m;
//		for(int i=1;i<=u;i++)
//			cout<<id[i]<<" ";
//		cout<<endl;
		T.Build();
		for(int i=1;i<=q;i++){
			int y=Q[i].y;
			
			pair<int,int> res=T.Query(y);
			long long ans=T.num[res.second];
			
//			cout<<res.first<<" "<<res.second<<endl;
			
			Pf(ans),putchar('\n');
//			break;
			T.Update(res.first,0),T.Update(n+m-1+i,ans);
			
		}
	}
	
}Px1;

struct _20{
	
	map<long long,int>mp;
	
	void solve(){
		for(int i=1;i<=q;i++){
			int x=Q[i].x,y=Q[i].y;
			long long ip=1ll*(x-1)*m+y;
			
			if(mp.count(ip))ip=mp[ip];
			
			Pf(ip),putchar('\n');
			
			for(int i=y;i+1<=m;i++){
				long long ip1=1ll*(x-1)*m+i,ip2=ip1+1;
				if(mp.count(ip2))mp[ip1]=mp[ip2];
				else mp[ip1]=ip2;
			}
			for(int i=x;i+1<=n;i++){
				long long ip1=1ll*i*m,ip2=ip1+m;
				if(mp.count(ip2))mp[ip1]=mp[ip2];
				else mp[ip1]=ip2;
			}
			long long t=1ll*n*m;
			mp[t]=ip;
		}
	}
	
}P20;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Rd(n),Rd(m),Rd(q);
	for(int i=1;i<=q;i++)Rd(Q[i].x),Rd(Q[i].y);
	if(0);
	if(n<=1000)P30.solve();
	else if(n==1||Px1.check()==true)Px1.solve();
	else P20.solve();
	return 0;
}

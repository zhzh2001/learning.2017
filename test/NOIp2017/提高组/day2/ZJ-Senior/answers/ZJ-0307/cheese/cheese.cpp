#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	int f=1;
	while(c=getchar(),!isdigit(c)&&c!='-');
	if(c=='-')c=getchar(),f=-1;
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
	x*=f;
}

#define M 1005

struct list{
	int to,nx;
}lis[M*M*2];
int tot,head[M];

void Add(int x,int y){
	lis[++tot]=(list){y,head[x]};
	head[x]=tot;
}

struct poi{
	int x,y,z;
}A[M];

int n,h,r;
int mark[M],tim;
int Q[M];

void BFS(){
	mark[0]=++tim;
	int l=0,r=-1;
	Q[++r]=0;
	while(l<=r){
		int x=Q[l++];
		if(x==n+1)return;
		for(int i=head[x];i;i=lis[i].nx){
			int to=lis[i].to;
			if(mark[to]==tim)continue;
			mark[to]=tim;
			Q[++r]=to;
		}
	}
}

double calc(poi a,poi b){
	return 1.0*(a.x-b.x)*(a.x-b.x)+1.0*(a.y-b.y)*(a.y-b.y)+1.0*(a.z-b.z)*(a.z-b.z);
}

void solve(){
	Rd(n),Rd(h),Rd(r);
	for(int i=0,up=n+1;i<=up;i++)head[i]=0;tot=0;
	double pp=4.0*r*r;
	for(int i=1,x,y,z;i<=n;i++){
		Rd(x),Rd(y),Rd(z);
		if(z+r>=h)Add(i,n+1);
		if(z-r<=0)Add(0,i);
		A[i]=(poi){x,y,z};
		for(int j=1;j<i;j++)
			if(calc(A[i],A[j])<=pp)Add(i,j),Add(j,i);
	}
	BFS();
	if(mark[n+1]==tim)puts("Yes");
	else puts("No");
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;
	Rd(cas);
	while(cas--)solve();
	return 0;
}

#include <cstdio>
#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

template <class T>void Rd(T &x){
	x=0;
	char c;
	while(c=getchar(),!isdigit(c));
	do{
		x=(x<<3)+(x<<1)+(c^'0');
	}while(c=getchar(),isdigit(c));
}

template <class T>void Min(T &x,T y){if(x>y)x=y;}

#define M 1005
#define N 12
#define oo 1061109567

struct list{
	int to,nx,v;
}lis[M<<1];
int tot,head[N];

void Add(int x,int y,int v){
	lis[++tot]=(list){y,head[x],v};
	head[x]=tot;
}

int n,m;
int dis[1<<12][1<<12];
int dp[12][1<<12];

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	Rd(n),Rd(m);
	for(int i=1,x,y,v;i<=m;i++){
		Rd(x),Rd(y),Rd(v);
		x--,y--;
		Add(x,y,v),Add(y,x,v);
	}
	memset(dp,63,sizeof(dp));
	for(int s=0,up=(1<<n);s<up;s++){
		for(int j=0;j<up;j++){
			if(s&j)continue;
			if(j==0){dis[s][j]=0;continue;}
			int res=oo;
			for(int t=0;t<n;t++){
				if(((1<<t)&j)==0)continue;
				for(int i=head[t];i;i=lis[i].nx){
					int to=lis[i].to;
					if((1<<to)&s)Min(res,dis[s][j^(1<<t)]+lis[i].v);
				}
			}
			dis[s][j]=res;
		}
	}
	for(int i=0;i<n;i++)dp[0][1<<i]=0;
	int ans=oo;
	for(int c=0;c<n;c++){
		Min(ans,dp[c][(1<<n)-1]);
		for(int i=0,up=(1<<n);i<up;i++){
//			cout<<c<<" "<<i<<" "<<dp[c][i]<<endl;
			if(dp[c][i]<ans){
				if(c==n-1)continue;
				for(int k=1;k<up;k++){
					if(i&k||dis[i][k]==oo)continue;
//					cout<<c+1<<" "<<(i|k)<<" "<<dp[c+1][i|k]<<endl;
//					cout<<i<<" "<<k<<" "<<dis[i][k]<<" "<<c<<" "<<dis[i][k]*(c+1)<<endl;
					Min(dp[c+1][i|k],dp[c][i]+dis[i][k]*(c+1));
				}
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}

#include <bits/stdc++.h>
using namespace std;

int main(){
	srand(time(NULL));
	freopen("treasure.in","w",stdout);
	int n=8,m=1000;
	printf("%d %d\n",n,m);
	for(int i=1;i<n;i++)printf("%d %d %d\n",rand()%i+1,i+1,rand()%100+1);
	for(int i=n;i<=m;i++){
		int x=rand()%n+1,y=rand()%n+1;
		if(x==y)y=y%n+1;
		printf("%d %d %d\n",x,y,rand()%100+1);
	}
	return 0;
}

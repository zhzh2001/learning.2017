#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
#define eps 1e-8
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
int map[15][15],home[15],dis[15];
bool vis[15];
int n,m,ans;
void prim(int st){
	rep(i,1,n) home[i]=INF;
	Clear(vis,0);
	home[st]=1;
	vis[st]=1;
	int sum=0;
	rep(i,1,n-1){
		int Min=INF,p=-1,from=-1;;
		rep(j,1,n)
			if (vis[j])
				rep(k,1,n)
					if (!vis[k])
						if (map[j][k]!=INF&&map[j][k]*home[j]<Min){
							Min=map[j][k]*home[j];
							from=j;
							p=k;
						}
		if (p==-1) break;
		sum+=Min;
		vis[p]=1;
		home[p]=home[from]+1;
	}
	ans=min(ans,sum);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	rep(i,1,n)
		rep(j,1,n)
			if (i!=j) map[i][j]=INF;
	rep(i,1,m){
		int u=read(),v=read(),w=read();
		map[u][v]=min(map[u][v],w);
		map[v][u]=map[u][v];
	}
	ans=INF;
	rep(i,1,n)
		prim(i);
	printf("%d\n",ans);
	return 0;
}

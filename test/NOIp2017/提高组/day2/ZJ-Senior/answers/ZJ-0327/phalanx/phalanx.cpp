#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define INF 2000000000
#define eps 1e-8
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
const int maxn=300005;
int n,m,q,head,tail;
int Next[maxn<<1][20],pre[maxn];
int a[maxn<<1];
int map[1005][1005];
void work(){
	rep(i,1,m){
		for (int j=0;i+(1<<j)<=m;j++)
			Next[i][j]=i+(1<<j);
		pre[i]=i-1;
	}
	head=1;tail=m;
	while (q--){
		int x=read(),y=read();
		int k=y-1,t=0,now=head;
		while (k){
			if (k&1) now=Next[now][t];
			k>>=1;
			t++;
		}
		printf("%d\n",now);
		k=y;t=0;
		int dep;
		int tmp=Next[now][0];
		for (int j=0;(1<<j)<=m;j++)
			Next[pre[now]][j]=Next[now][j];
		pre[tmp]=pre[now];
		pre[now]=tail;
		tail=now;
		if (now==head) head=tmp;
		k=m;t=0;
		while (k){
			int p=m-(1<<t)-1;
			now=head;dep=0;
			while (p){
				if (p&1) now=Next[now][dep];
				p>>=1;
				dep++;
			}
			Next[now][t]=tail;
			k>>=1;
			t++;
		}
	}
}
int calc(int x,int y){
	return (x-1)*m+y;
}
void work_(){
	rep(i,1,m) a[i]=i;
	rep(i,2,n) a[m+i-1]=calc(i,m);
	rep(i,1,n+m-1){
		for (int j=0;i+(1<<j)<=n+m-1;j++)
			Next[a[i]][j]=a[i+(1<<j)];
		pre[a[i]]=a[i-1];
	}
	head=1;tail=m;
	while (q--){
		int x=read(),y=read();
		int k=y-1,t=0,now=head;
		while (k){
			if (k&1) now=Next[now][t];
			k>>=1;
			t++;
		}
		printf("%d\n",now);
		k=y;t=0;
		int dep;
		int tmp=Next[now][0];
		for (int j=0;(1<<j)<=m;j++)
			Next[pre[now]][j]=Next[now][j];
		pre[tmp]=pre[now];
		pre[now]=tail;
		tail=now;
		if (now==head) head=tmp;
		k=m;t=0;
		while (k){
			int p=m-(1<<t)-1;
			now=head;dep=0;
			while (p){
				if (p&1) now=Next[now][dep];
				p>>=1;
				dep++;
			}
			Next[now][t]=tail;
			k>>=1;
			t++;
		}
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n==1){
		work();
		return 0;
	}
	if (n>300000){
		work_();
		return 0;
	}
	rep(i,1,n)
		rep(j,1,m)
			map[i][j]=calc(i,j);
	while (q--){
		int x=read(),y=read();
		printf("%d\n",map[x][y]);
		int tmp=map[x][y];
		rep(i,y,m-1)
			map[x][i]=map[x][i+1];
		rep(i,x,n-1)
			map[i][m]=map[i+1][m];
		map[n][m]=tmp;
	}
	return 0;
}

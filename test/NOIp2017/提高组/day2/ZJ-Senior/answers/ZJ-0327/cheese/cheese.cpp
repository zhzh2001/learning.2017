#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define rep(i,a,n) for (int i=a;i<=n;i++)
#define per(i,a,n) for (int i=a;i>=n;i--)
#define Clear(a,x) memset(a,x,sizeof(a))
#define ll long long
#define db double
#define INF 2000000000
#define eps 1e-8
#define sqr(x) (x)*(x)
using namespace std;
int read(){
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9') f=ch=='-'?-1:f,ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
const int maxn=1005,maxm=1000005;
int n,h,r,len;
int x[maxn],y[maxn],z[maxn];
int vet[maxm<<1],Next[maxm<<1],head[maxn];
bool vis[maxn];
bool flag;
bool check(int i,int j){
	int x1=x[i],y1=y[i],z1=z[i];
	int x2=x[j],y2=y[j],z2=z[j];
	if (sqrt((db)sqr(x1-x2)+(db)sqr(y1-y2)+(db)sqr(z1-z2))<=2*r) return true;
	return false;
}
void add(int u,int v){
	vet[++len]=v;
	Next[len]=head[u];
	head[u]=len;
}
void dfs(int u){
	vis[u]=1;
	if (u==n+1){
		flag=1;
		return;
	}
	for (int e=head[u];e!=-1;e=Next[e]){
		int v=vet[e];
		if (vis[v]) continue;
		dfs(v);
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while (T--){
		n=read(),h=read(),r=read();
		rep(i,1,n)
			x[i]=read(),y[i]=read(),z[i]=read();
		Clear(head,-1);
		len=0;
		rep(i,1,n)
			rep(j,i+1,n)
				if (check(i,j)) add(i,j),add(j,i);
		rep(i,1,n)
			if (h-z[i]<=r) add(i,n+1);
		rep(i,1,n)
			if (r-z[i]>=0) add(0,i);
		flag=0;
		Clear(vis,0);
		dfs(0);
		if (flag) puts("Yes");
			else puts("No");
	}
	return 0;
}

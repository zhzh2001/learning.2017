#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
const int N=100010;
int n,m,q,t=0,tot=0,x,y;
int mp[1010][1010],a[N],b[N],c1[N],c2[N];
void add1(int x){while (x<=m*2) c1[x]++,x+=x&-x; }
int get1(int x,int res=0){while (x>0) res+=c1[x],x-=x&-x; return res;}
void add2(int x){while (x<=m*2) c2[x]++,x+=x&-x; }
int get2(int x,int res=0){while (x>0) res+=c2[x],x-=x&-x; return res;}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (x==1){
		for (int i=1;i<=m;++i) a[i]=i;
		while (q--){
			scanf("%d%d",&x,&y);
			if (y<=m){
				int res=get1(y),pos=y+res,sum=get1(pos);
				while (sum!=res){sum=get1(pos+sum-res); pos+=sum-res; res=sum;}
				printf("%d\n",a[pos]); m--; b[++t]=a[pos]; a[pos]=0; add1(pos); 
			} else {
				int res=get2(y-m),pos=y-m+res,sum=get2(pos );
				while (sum!=res){sum=get2(pos+sum-res ); pos+=sum-res; res=sum;}
				printf("%d\n",b[pos]); b[++t]=b[pos]; b[pos]=0; add2(pos );
			}	
		}
		return 0;
	}
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
			mp[i][j]=++tot;
	while (q--){
		scanf("%d%d",&x,&y);
		printf("%d\n",mp[x][y]);
		int res=mp[x][y];
		for (int i=y+1;i<=m;++i) mp[x][i-1]=mp[x][i];
		for (int i=x+1;i<=n;++i) mp[i-1][m]=mp[i][m];
		mp[n][m]=res;
	}
	return 0;
}

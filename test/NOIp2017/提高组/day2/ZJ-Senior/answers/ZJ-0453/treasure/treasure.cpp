#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define mp make_pair
#define fi first
#define se second
using namespace std;
typedef pair<int,int> pii;
const int N=200;
int to[N],nxt[N],fst[N],a[N],dis[N][N];
int ans=1e9,sum=0,tot=0,n,m,x,y,z;
bool bo[N];
queue<pii>q;
void add(int x,int y,int z){
	to[++tot]=y; nxt[tot]=fst[x]; fst[x]=tot; a[tot]=z;
}
void bfs(int x){
	q.push(mp(x,1)); bo[x]=1;
	while (!q.empty()){
		pii u=q.front(); q.pop(); 
		for (int i=fst[u.fi];i;i=nxt[i])
			if (!bo[to[i]]) bo[to[i]]=1,sum+=a[i]*u.se,q.push(mp(to[i],u.se+1));
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
			dis[i][j]=1e9;
	for (int i=1;i<=m;++i)
		scanf("%d%d%d",&x,&y,&z),dis[x][y]=dis[y][x]=min(dis[x][y],z);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j) if (dis[i][j]<1e9) add(i,j,dis[i][j]);
	for (int i=1;i<=n;++i){
		memset(bo,0,sizeof(bo)); sum=0; bfs(i);
		ans=min(ans,sum);
	}
	printf("%d",ans);
	return 0;
}

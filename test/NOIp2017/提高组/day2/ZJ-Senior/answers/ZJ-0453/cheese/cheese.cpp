#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define sqr(x) ((x)*(x))
using namespace std;
typedef long long ll;
const int N=2010;
struct xint{ll x,y,z;}e[N];
int n,q[N*10],h,t; ll H,r,rr;
bool bo[N];

inline ll dis(int x,int y){
	return sqr(e[x].x-e[y].x)+sqr(e[x].y-e[y].y)+sqr(e[x].z-e[y].z);
}
inline void bfs(){
	h=t=0;
	for (int i=1;i<=n;++i)
		if (e[i].z<=r) q[++t]=i;
	while (h<t){
		int u=q[++h]; 
		if (bo[u]) continue; bo[u]=1;
		if (abs(e[u].z-H)<=r){puts("Yes"); return;}
		for (int i=1;i<=n;++i)
			if (dis(i,u)<=rr) q[++t]=i;
	}
	puts("No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		memset(bo,0,sizeof(bo));
		scanf("%d%lld%lld",&n,&H,&r); rr=4*r*r;
		for (int i=1;i<=n;++i)
			scanf("%lld%lld%lld",&e[i].x,&e[i].y,&e[i].z);
		bfs();
	}
	return 0;
}

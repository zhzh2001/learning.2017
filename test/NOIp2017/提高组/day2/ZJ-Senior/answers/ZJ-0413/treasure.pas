var n,m,i,j,x,y,w,sum,ans,mm,cnt:longint;
map,mmp:array[0..15,0..15]of longint;
xx,yy,dis:array[0..1005]of longint;
procedure dfs(now,fa,num:longint);
var i:longint;
begin
        if sum>ans then exit;
        for i:=1 to n do
        if (map[now,i]<>-1) and (i<>fa) then
        begin
                sum:=sum+map[now,i]*num;
                dfs(i,now,num+1);
        end;
end;
function check:boolean;
var head,tail,i,u:longint;
q:array[0..105]of longint;
visit:array[0..15]of boolean;
begin
        fillchar(visit,sizeof(visit),false);
        q[1]:=1;
        head:=0;
        tail:=1;
        visit[1]:=true;
        while head<tail do
        begin
                inc(head);
                u:=q[head];
                for i:=1 to n do
                if (map[u,i]<>-1) and (visit[i]=false) then
                begin
                        visit[i]:=true;
                        inc(tail);
                        q[tail]:=i;
                end;
        end;
        for i:=1 to n do
        if visit[i]=false then exit(false);
        exit(true);
end;
procedure do_chose(now,last:longint);
var i:longint;
begin
        if now=n then
        begin
                cnt:=0;
                if check then
                begin
                        for i:=1 to n do
                        begin
                                sum:=0;
                                dfs(i,-1,1);
                                if sum<ans then ans:=sum;
                        end;
                end;
                exit;
        end;
        for i:=last+1 to mm do
        begin
                map[xx[i],yy[i]]:=dis[i];
                map[yy[i],xx[i]]:=dis[i];
                do_chose(now+1,i);
                map[xx[i],yy[i]]:=-1;
                map[yy[i],xx[i]]:=-1;
        end;
end;
BEGIN
        assign(input,'treasure.in');
        reset(input);
        assign(output,'tressure.out');
        rewrite(output);
        readln(n,m);
        fillchar(map,sizeof(map),255);
        for i:=1 to m do
        begin
                readln(x,y,w);
                if map[x,y]=-1 then
                begin
                        map[x,y]:=w;
                        map[y,x]:=w;
                end
                else
                if map[x,y]>w then
                begin
                        map[x,y]:=w;
                        map[y,x]:=w;

                end;
        end;
        ans:=maxlongint;
        if m=n-1 then
        begin
                for i:=1 to n do
                begin
                        sum:=0;
                        dfs(i,-1,1);
                        if sum<ans then ans:=sum;
                end;
        end
        else
        begin
                mmp:=map;
                fillchar(map,sizeof(map),255);
                for i:=1 to n do
                        for j:=1 to i do
                        if mmp[i,j]<>-1 then
                        begin
                                inc(mm);
                                xx[mm]:=i;
                                yy[mm]:=j;
                                dis[mm]:=mmp[i,j];
                        end;
                do_chose(1,0);
        end;
        writeln(ans);
        close(input);
        close(output);
END.

var T,test,n,h,r,i,j,cnt:longint;
delta_x,delta_y,delta_z:int64;
x,y,z:array[0..1005]of longint;
first,next,son:array[0..1000005]of longint;
visit:array[0..1005]of boolean;
procedure add(x,y:longint);
begin
        inc(cnt);next[cnt]:=first[x];first[x]:=cnt;son[cnt]:=y;
        inc(cnt);next[cnt]:=first[y];first[y]:=cnt;son[cnt]:=x;
end;
procedure dfs(now,fa:longint);
var i:longint;
begin
        visit[now]:=true;
        if visit[n+1] then exit;
        i:=first[now];
        while i<>-1 do
        begin
                if (son[i]<>fa) and (visit[son[i]]=false) then dfs(son[i],now);
                i:=next[i];
        end;
end;
BEGIN
        assign(input,'cheese.in');
        reset(input);
        assign(output,'cheese.out');
        rewrite(output);
        readln(T);
        for test:=1 to T do
        begin
                readln(n,h,r);
                cnt:=0;
                fillchar(next,sizeof(next),255);
                fillchar(first,sizeof(first),255);
                for i:=1 to n do
                begin
                        readln(x[i],y[i],z[i]);
                        if z[i]-0<=r then add(0,i);
                        if h-z[i]<=r then add(i,n+1);
                        for j:=1 to i-1 do
                        begin
                                delta_x:=x[i]-x[j];
                                delta_y:=y[i]-y[j];
                                delta_z:=z[i]-z[j];
                                if sqrt(delta_x*delta_x+delta_y*delta_y+delta_z*delta_z)<=2*r then add(i,j);
                        end;
                end;
                fillchar(visit,sizeof(visit),0);
                dfs(0,-1);
                if visit[n+1] then writeln('Yes')
                else writeln('No');
        end;
        close(input);
        close(output);
END.

#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<queue>
#include<algorithm>
#include<iostream>
using namespace std;

int n,m,q,peo[5000][5000],x[500005],y[500005];
int xx[500005],yy[500005];

inline int read(){
	int re=0,rf=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') rf=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') re=re*10+ch-'0',ch=getchar();
	return re*rf;
}

inline void work1(){
	for(int i=1;i<=m;i++)
		xx[i]=i;
	for(int i=2;i<=n;i++)
		yy[i-1]=i*m;
	for(int i=1;i<=q;i++){
		int v=xx[y[i]];
		printf("%d\n",v);
		for(int j=y[i]+1;j<=m;j++)
			xx[j-1]=xx[j];
		xx[m]=yy[1];
		for(int j=2;j<n;j++)
			yy[j-1]=yy[j];
		yy[n-1]=v;
	}
}

inline void work2(){
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			peo[i][j]=(i-1)*m+j;
	for(int i=1;i<=q;i++){
		int v=peo[x[i]][y[i]];
		printf("%d\n",v);
		for(int j=y[i]+1;j<=m;j++)
			peo[x[i]][j-1]=peo[x[i]][j];
		for(int j=x[i]+1;j<=n;j++)
			peo[j-1][m]=peo[j][m];
		peo[n][m]=v;
	}
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	bool flag=true;
	for(int i=1;i<=q;i++){
		x[i]=read(),y[i]=read();
		if (x[i]!=1) flag=false;
		}
	if (flag) work1();
		else work2();
	return 0;
	fclose(stdin);
	fclose(stdout);
}
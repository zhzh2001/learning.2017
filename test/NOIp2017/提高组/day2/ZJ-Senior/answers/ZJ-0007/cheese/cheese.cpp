#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<queue>
#include<algorithm>
#include<iostream>
using namespace std;
typedef long long LL;

LL t,n,h,r,x[1005],y[1005],z[1005];
bool used[1005],ans[50];
int ti=0,iter[1005];
struct poi{int to,next;} g[2000050];

inline LL read(){
	LL re=0,rf=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') rf=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') re=re*10+ch-'0',ch=getchar();
	return re*rf;
}

inline void add(int x,int y){
	ti++;
	g[ti].to=y,g[ti].next=iter[x];iter[x]=ti;
}

inline void init(){
	for(int i=1;i<=n;i++)
		used[i]=false,iter[i]=0;
	ti=0;
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++)
			if ((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j])<=4*r*r)
				{add(i,j);add(j,i);}
}

inline bool dfs(int u){
	used[u]=true;
	if (z[u]+r>=h) return true;
	for(int i=iter[u];i;i=g[i].next){
		int v=g[i].to;
		if (!used[v]&&dfs(v)) return true;
	}
	return false;
}
	
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	for(int k=1;k<=t;k++){
		n=read();h=read();r=read();
		for(int i=1;i<=n;i++)
			x[i]=read(),y[i]=read(),z[i]=read();
		init();
		for(int i=1;i<=n;i++)
			if (z[i]<=r&&!used[i]&&dfs(i)) {ans[k]=true;break;}
				else ans[k]=false;
	}
	for(int i=1;i<=t;i++)
		if (ans[i]) printf("Yes\n");
			else printf("No\n");
	return 0;
	fclose(stdin);
	fclose(stdout);
}
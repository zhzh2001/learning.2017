#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<cmath>
#include<queue>
#include<algorithm>
#include<iostream>
using namespace std;
typedef long long LL;

LL res=0,n,m,dis[20][20];
int dep[20];
bool used[20][20],visit[20];

inline LL read(){
	LL re=0,rf=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {if (ch=='-') rf=-1;ch=getchar();}
	while(ch>='0'&&ch<='9') re=re*10+ch-'0',ch=getchar();
	return re*rf;
}

inline LL bfs(int s){
	for(int i=1;i<=n;i++)
		dep[i]=0;
	queue<int> q;
	dep[s]=1;
	q.push(s);
	LL ans=0;
	while(!q.empty()){
		int u=q.front();q.pop();
		ans+=dep[u];
		for(int i=1;i<=n;i++)
			if (dis[u][i]!=0&&dep[i]==0) {dep[i]=dep[u]+1;q.push(i);}
	}
	return ans;
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	LL v;
	for(int i=1;i<=m;i++){
		int x=read(),y=read(),z=read();
		v=z;
		if (dis[x][y]==0||dis[x][y]>z) dis[x][y]=dis[y][x]=z;
	}
	for(int s=1;s<=n;s++){
		int anxxx=bfs(s);
		if (anxxx<res||res==0) res=anxxx;
	}
	res*=v;
	printf("%lld\n",res);
	return 0;
	fclose(stdin);
	fclose(stdout);
}
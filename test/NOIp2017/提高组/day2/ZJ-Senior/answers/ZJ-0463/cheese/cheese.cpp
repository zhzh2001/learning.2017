#include<iostream>
#include<cstdio>
#include<cmath>
using namespace std;
int ma[1000][1000],vis[1010],outde[1011];
int n,h,r;
struct e
{
	int x;int y;int z;
}u[1010];
int dis(int x1,int y1,int z1,int x2,int y2,int z2,int rtemp)
 {
 	double k = sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2));
 	if(k <= 2*rtemp) return 1;
 	 else return 0;
 }
 int dfs(int s)
  {
  	if(s == n+1) return 1;
  	vis[s] = 1;
  	for(int i = 1;i <= outde[s];i++)
  	 {
  	 	int to = ma[s][i];
  	 	if(!vis[to])
  	 	 {
  	 	 	vis[to] = 1;
  	 	 	if(dfs(to)) return 1;
  	 	 	 else return 0;
		 }
	   }
  }
  void init()
   {
   	for(int i = 1;i <= n+1;i++)
   	 for(int j = 1;j <= n+1;j++)
   	 {
   	 	outde[i] = 0;
   	 	 ma[i][j] = -1;
   	 	 vis[i] = 0;
   	 	 u[i].x = u[i].y = u[i].z = 0;
		}
   	 
   	  
   }
int main()
 {
 	freopen("cheese.in","r",stdin);
 	freopen("cheese.out","w",stdout);
 	int t;
 	cin>>t;
 	while(t--)
 	  {
 	  	
 	  	cin>>n>>h>>r;
 	  	for(int i = 1;i <= n;i++)
 	  	 	cin>>u[i].x>>u[i].y>>u[i].z;
 	  	 	u[0].x = 0;u[n+1].x = 0;
 	  	 	u[0].y = 0;u[n+1].y = 0;
 	  	 	u[0].z = 0;u[n+1].z = h;
 	  	 	init();
 		for(int i = 0;i <= n;i++)	
 		 for(int j = 1;j <= n+1;j++)
 		 {
 		 	if(dis(u[i].x,u[i].y,u[i].z,u[j].x,u[j].y,u[j].z,r)&&(i!=j)) 
 		 	{
 		 		ma[i][++outde[i]] = j; 		 		
			  }			  
		  }
		  if(dfs(0)) cout<<"Yes"<<endl;
		   else cout<<"No"<<endl;
	  }
 	
 	
 	
 	return 0;
 }

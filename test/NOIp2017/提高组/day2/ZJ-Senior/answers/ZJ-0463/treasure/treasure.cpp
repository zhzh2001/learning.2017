#include<iostream>
#include<queue>
#include<cstdio>
using namespace std;
int ma[50][50],outde[50],k[50];
	int n,m;
long long maxn = 9999999,vis[50],dis[50],v[50][50];
int spfa(int s)
 {
   queue<int> q;
   q.push(s);
   dis[s] = 0;
   vis[s] = 1;
   while(!q.empty())
     {
     	int now;
     	now = q.front();q.pop();
     	vis[now] = 0;
     	for(int i = 1;i <= outde[now];i++)
     	 {
     	 	int to = ma[now][i];
     	 	 	if(dis[to] > (k[now]+1)*v[now][i])
     	 	 	 {
     	 	 	 	dis[to] = (k[now]+1)*v[now][i];
     	 	 	 	if(!vis[to])
     	 	 	 	 {
     	 	 	 	  q.push(to);
     	 	 	 	  vis[to] = 1;
     	 	 	 	  k[to] = k[now]+1;
					 }
					}
			   }
		  }
	 }
 
 void init()
  {
  	for(int i = 1;i <= n;i++)
  	{
  		dis[i] = 9999999;
  		vis[i] = 0;
  		k[i] = 0;
	  }
  }
int main()
 {
 	freopen("treasure.in","r",stdin);
 	freopen("treasure.out","w",stdout);
 
 	cin>>n>>m;
 	for(int i = 1;i <= m;i++)
 	 {
 	 	int a,b,c;
 	 	cin>>a>>b>>c;
 	 	ma[a][++outde[a]] = b;
 	 	ma[b][++outde[b]] = a;
 	 	v[a][outde[a]] = c;
 	 	v[b][outde[b]] = c;
	  }
	  for(int i = 1;i <= n;i++)
	  {
	  	init();
	  	spfa(i);
	  	 long long temp = 0;
	   	 for(int j = 1;j <= n;j++)
	   	 {
	   	 	dis[i] = 0;
	   	 	temp += dis[j];
			}  	 
	   	 maxn = min(temp,maxn);
	   }
	   cout<<maxn;
	   	return 0;
}

 

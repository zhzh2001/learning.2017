#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<iostream>
using namespace std;
inline void judge(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
}
typedef long long ll;
namespace seg{
	int L[30000005],R[30000005],v[30000005],cnt;
	inline int nnode(int le,int ri){
		cnt++;
		v[cnt]=ri-le+1;
	}
	#define mid ((l+r)>>1)
	inline int del(int u,int l,int r,int x){
		v[u]--;
		while(l<r){
			if(!L[u])L[u]=nnode(l,mid);	
			if(!R[u])R[u]=nnode(mid+1,r);
		//	cerr<<u<<' '<<l<<' '<<r<<' '<<x<<' '<<mid<<endl;
		//	cerr<<L[u]<<' '<<v[L[u]]<<endl;
			if(v[L[u]]>=x)u=L[u],r=mid;
			else x-=v[L[u]],u=R[u],l=mid+1;
			v[u]--;
		}
		return l;
	}
}
struct que{
	int root,size;
	vector<ll> v;
	inline int init(int x){size=x; root=seg::nnode(1,size+300000);}
	inline pair<ll,bool> del(int x){
		int res=seg::del(root,1,size+300000,x);
		if(res<=size)return make_pair((ll)res,false);
		return make_pair(v[res-size-1],true);
	}
	inline int pb(int x){
		v.push_back(x);
	}
}f[300005];
int main(){
	judge();
	int n,m,q;
	scanf("%d%d%d",&n,&m,&q);
	f[0].init(n);
	for(int i=1;i<=n;i++)f[i].init(m-1);
	while(q--){
		int x,y;
		scanf("%d%d",&x,&y);
	//	cerr<<x<<' '<<y<<endl;
		if(y==m){
			pair<ll,bool> p=f[0].del(x);
			printf("%lld\n",(p.second?p.first:1ll*(p.first-1)*n+m));
			f[0].pb((p.second?p.first:1ll*(p.first-1)*n+m));
		}else{
			pair<ll,bool> p=f[x].del(y),q=f[0].del(x);
			f[x].pb((q.second?q.first:1ll*(q.first-1)*n+m));
			f[0].pb((p.second?p.first:1ll*(x-1)*n+p.first));
			printf("%lld\n",(p.second?p.first:1ll*(x-1)*n+p.first));
		}
	//	cerr<<"ok"<<endl;
	}
	return 0;
}

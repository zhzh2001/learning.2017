#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
}
int x[1005],y[1005],z[1005],n,h,r;
inline long long sqr(int x){return 1ll*x*x;}
bool vis[1005];
bool dfs(int u){
	if(z[u]+r>=h)return 1;
	if(vis[u])return 0;
	vis[u]=1;
	for(int i=1;i<=n;i++)
		if(sqr(x[i]-x[u])+sqr(y[i]-y[u])<=sqr(2*r)-sqr(z[i]-z[u])&&dfs(i))return 1;
	return 0;
}
int main(){
	judge();
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++)scanf("%d%d%d",&x[i],&y[i],&z[i]);
		bool ok=0;
		memset(vis,0,sizeof(vis));
		for(int i=1;i<=n;i++)if(z[i]-r<=0&&dfs(i)){ok=1; break;}
		if(ok)puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
inline void judge(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
}
int f[1<<12][13],e[13][13];
int main(){
	judge();
	int n,m;
	scanf("%d%d",&n,&m);
	memset(e,63,sizeof(e));
	for(int i=1;i<=m;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		u--; v--;
		e[u][v]=min(e[u][v],w);
		e[v][u]=min(e[v][u],w);
	}
	memset(f,63,sizeof(f));
	for(int i=0;i<n;i++)f[1<<i][1]=0;
	for(int i=0;i<(1<<n);i++){
		int inv=(((1<<n)-1)^i);
		for(int k=inv;k;k=(k-1)&inv){
			int co=0;
			for(int l=0;l<n;l++)if((k>>l)&1){
				int v=10000000;
				for(int p=0;p<n;p++)if((i>>p)&1)v=min(v,e[l][p]);
				co+=v;
			}
			for(int j=1;j<=n;j++)if(f[i][j]<100000000)
				f[i^k][j+1]=min(f[i^k][j+1],f[i][j]+co*j);
		}
	}
	int ans=100000000;
	for(int i=1;i<=n;i++)ans=min(ans,f[(1<<n)-1][i]);
	printf("%d\n",ans);
	return 0;
}

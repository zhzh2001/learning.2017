#include <cstdio>
#include <cmath>
#include <cstring>

using namespace std;

int rd() {
	int x = 0, f = 1; char c = getchar();
	while (c > '9' || c < '0') f = c == '-' ? -1 : 1, c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x * f;
}

long long sqr(int x) {
	return 1ll * x * x;
}

double dist(int x, int y, int z, int xx, int yy, int zz) {
	return sqrt( sqr(x - xx) + sqr(y - yy) + sqr(z - zz));
}

int x[1010], y[1010], z[1010], n, h, r;
bool f[21][1010][1010], ff, fff;

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	for (int T = rd(); T; T --) {
		n = rd(), h = rd(), r = rd();
		for (int i = 1; i <= n; i ++) x[i] = rd(), y[i] = rd(), z[i] = rd();
		for (int i = 1; i <= n; i ++) {
			for (int j = i + 1; j <= n; j ++) {
				if (dist(x[i], y[i], z[i], x[j], y[j], z[j]) <= 2.0 * r) 
					f[T][i][j] = f[T][j][i] = 1;
			}
		}
		for (int i = 1; i <= n; i ++) {
			if (z[i] - r <= 0) f[T][i][0] = f[T][0][i] = 1, ff |= 1;
			if (z[i] + r >= h) f[T][i][n + 1] = f[T][n + 1][i] = 1, fff |= 1;
		}
		if (!ff || !fff) {puts("No"); continue;}
		for (int k = 0; k <= n + 1; k ++) {
			if (f[T][0][n + 1]) break;
			for (int i = 0; i <= n + 1; i ++) if (f[T][i][k] && i != k) {
				for (int j = i + 1; j <= n + 1; j ++) if (f[T][k][j] && k != j){
					f[T][j][i] = f[T][i][j] |= 1;
				}
			}
		}
		if (f[T][0][n + 1]) puts("Yes"); else puts("No");
	}
	return 0;
}

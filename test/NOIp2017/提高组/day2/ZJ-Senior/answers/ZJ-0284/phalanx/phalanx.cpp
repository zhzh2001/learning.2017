#include <cstdio> 

using namespace std;

int rd() {
	int x = 0; char c = getchar();
	while (c > '9' || c < '0') c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x;
}

void wt(int x) {
	if (x >= 10) wt(x / 10);
	putchar(x % 10 + 48);
}

const int N = 1010;
int num[N][N], n, m, Q, ans;

int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	n = rd(), m = rd(), Q = rd();
	for (int i = 1; i <= n; i ++) for (int j = 1; j <= m; j ++) num[i][j] = (i - 1) * m + j;
	while (Q --) {
		int x = rd(), y = rd();
		ans = num[x][y]; 
		wt(ans), putchar(10);
		for (int i = y; i < m; i ++) num[x][i] = num[x][i + 1];
		for (int i = x; i < n; i ++) num[i][m] = num[i + 1][m];
		num[n][m] = ans;
	}
	return 0;
}

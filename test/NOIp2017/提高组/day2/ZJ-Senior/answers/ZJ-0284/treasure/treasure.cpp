#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int rd() {
	int x = 0; char c = getchar();
	while (c > '9' || c < '0') c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x;
}

const int N = 1010;
const int inf = 0x7f7f7f7f;

int cost[N], g[N][N], d[N], wh[N];
int n, m, ans;
bool vis[N];

int solve(int s) {
	int ret = 0;
	memset(cost, 0x7f, sizeof cost);
	memset(d, 0, sizeof d);
	memset(wh, 0, sizeof wh);
	memset(vis, 0, sizeof vis);
	for (int i = 1; i <= n; i ++) cost[i] = g[s][i], wh[i] = s;
	cost[s] = 0, vis[s] = 1;
	for (int i = 1; i < n; i ++) {
		int mn = inf, v;
		for (int j = 1; j <= n; j ++) {
			if (!vis[j] && mn > cost[j]) mn = cost[j], v = j;
		}
		ret += mn;
		vis[v] = 1;
		d[v] = d[wh[v]] + 1;
		for (int j = 1; j <= n; j ++) if (g[v][j] != inf) {
			if (g[v][j] * (d[v] + 1) < cost[j]) cost[j] = g[v][j] * (d[v] + 1), wh[j] = v;
//			if (g[v][j] * (d[v] + 1) == cost[j] && d[wh[j]] > d[v]) wh[j] = v;
		}
	}
	return ret;
}

int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	n = rd(), m = rd();
	memset(g, 0x7f, sizeof g);
	for (int i = 1; i <= m; i ++) {
		int u = rd(), v = rd(), w = rd();
		g[u][v] = min(g[u][v], w);
		g[v][u] = g[u][v];
	}
	ans = inf;
	for (int i = 1; i <= n; i ++) 
		ans = min(ans, solve(i));
	printf("%d\n", ans);
	return 0;
}

#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 15
int n,m,a[N][N];
void up(int &x,int y){if(x==-1||y<x)x=y;}
int st,ANS,la; queue<int>q; int v[N],d[N];
void bfs(){
	while(!q.empty())q.pop();
	for(int i=1;i<=n;i++)v[i]=0,d[i]=n;
	v[st]=1,d[st]=0,q.push(st); int x;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=1;i<=n;i++)if(!v[i]&&a[x][i]!=1e9)d[i]=d[x]+1,v[i]=1,q.push(i);
	}
	int ans=0;for(int i=1;i<=n;i++)ans+=d[i]; if(ans<ANS)ANS=ans;
}
void case1(){
	ANS=n*n;
	for(int i=1;i<=n;i++)st=i,bfs();
	printf("%d\n",ANS*la);
}
int f[N][N];
void work(){
	memset(f,-1,sizeof(f));
	f[st][0]=0;
	for(int k=0;k<n;k++)for(int i=1;i<=n;i++)if(f[i][k]!=-1){
		for(int j=1;j<=n;j++)if(a[i][j]!=1e9)up(f[j][k+1],f[i][k]+a[i][j]*(k+1));
	}
	ANS=0;
	for(int i=1,ans;i<=n;i++){ans=-1;for(int k=0;k<n;k++)if(f[i][k]!=-1)up(ans,f[i][k]); ANS+=ans;}
	printf("%d\n",ANS);
}
int main(){
	freopen("treasure.in","r",stdin);
	//freopen("treasure.out","w",stdout);
	n=read(),m=read(); for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)a[i][j]=(i==j?0:1e9);
	int flag=1; la=-1;
	for(int i=1,x,y,z;i<=m;i++){
		x=read(),y=read(),z=read(),up(a[x][y],z),up(a[y][x],z);
		if(i>1&&z!=la)flag=0; la=z;
	}
	if(flag){case1();return 0;}
	for(int i=1;i<=n;i++){
		st=i,work();
	}
	return 0;
}

#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 15
#define NN 300
int n,m,a[N][N];
void up(int &x,int y){if(x==-1||y<x)x=y;}
struct edge{int to,nxt,w,co;}e[NN*NN*2];int hed[NN],cnt=1;
void add(int x,int y,int w,int co){
	//printf("%d %d\n",x,y);
	e[++cnt].nxt=hed[x];hed[x]=cnt;e[cnt].to=y,e[cnt].w=w,e[cnt].co=co;
	e[++cnt].nxt=hed[y];hed[y]=cnt;e[cnt].to=x,e[cnt].w=0,e[cnt].co=-co;
}
queue<int>q; int v[NN],dis[NN],pre[NN],pid[NN];
inline int spfa(int st,int ed,int n){
	for(int i=1;i<=n;i++)dis[i]=1e9,v[i]=0;
	v[st]=1,dis[st]=0,q.push(st); int x,y;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=hed[x];i;i=e[i].nxt)if(e[i].w>0&&dis[e[i].to]>dis[x]+e[i].co){
			y=e[i].to,dis[y]=dis[x]+e[i].co,pre[y]=x,pid[y]=i;
			if(!v[y])v[y]=1,q.push(y);
		}
		v[x]=0;
	}
	return dis[ed];
}
int getco(int st,int ed,int n){
	int tmp,p,fl=0,co=0;
	while(1){
		tmp=spfa(st,ed,n); if(tmp==1e9)break;
		p=1e9; for(int i=ed;i!=st;i=pre[i])if(e[pid[i]].w<p)p=e[pid[i]].w;
		for(int i=ed;i!=st;i=pre[i])e[pid[i]].w-=p,e[pid[i]^1].w+=p;
		//for(int i=ed;i!=st;i=pre[i])printf("%d ",i); puts("");
		//printf("%d %d\n",p,tmp);
		fl+=p,co+=tmp*p;
	}
	//printf("%d\n",fl);
	return co;
}
int st,ed,ST;
void work(){
	for(int i=1;i<=ed;i++)hed[i]=0; cnt=1;
	for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)if(a[i][j]!=1e9)
		for(int k=0;k+1<n;k++)add((i-1)*n+k+1,(j-1)*n+k+1+1,n,(k+1)*a[i][j]);
	for(int i=1;i<=n;i++)for(int k=0;k<n;k++)add((i-1)*n+k+1,n*n+i,1,0);
	for(int i=1;i<=n;i++)add(n*n+i,ed,1,0);
	add(st,(ST-1)*n+1,n,0);
	printf("%d\n",getco(st,ed,ed));
}
int main(){
	freopen("treasur2.in","r",stdin);
	//freopen("treasure.out","w",stdout);
	n=read(),m=read(); for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)a[i][j]=(i==j?0:1e9);
	for(int i=1,x,y,z;i<=m;i++)
		x=read(),y=read(),z=read(),up(a[x][y],z),up(a[y][x],z);
	st=n*n+n+1,ed=st+1;
	//ST=1,work();//
	for(ST=1;ST<=n;ST++)work();
}

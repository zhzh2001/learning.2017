#include<cstdio>
#include<queue>
using namespace std;
#define LL long long
inline LL read(){
	LL x=0;int f=1;char ch=getchar();while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x*f;
}
#define N 1010
#define sqr(x) (x)*(x)
queue<int>q; int n,v[N]; LL x[N],y[N],z[N],r,h;
void bfs(){
	for(int i=1;i<=n;i++)v[i]=0; while(!q.empty())q.pop(); int t;
	for(int i=1;i<=n;i++)if(z[i]<=r&&-z[i]<=r)q.push(i),v[i]=1;
	while(!q.empty()){
		t=q.front(),q.pop(); if(r>=h-z[t]&&r>=z[t]-h){puts("Yes");return;}
		for(int i=1;i<=n;i++)if(!v[i]&&sqr(x[i]-x[t])+sqr(y[i]-y[t])+sqr(z[i]-z[t])<=r*r*4)
			v[i]=1,q.push(i);
	}
	puts("No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--){
		n=read(),h=read(),r=read();
		for(int i=1;i<=n;i++)x[i]=read(),y[i]=read(),z[i]=read();
		bfs();
	}
	return 0;
}

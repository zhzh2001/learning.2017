#include<cstdio>
#include<queue>
#include<map>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 15
#define LL long long
int n,m,a[N][N];
void up(int &x,int y){if(x==-1||y<x)x=y;}
int st,ANS,la; queue<int>q; int v[N],d[N];
void bfs(){
	while(!q.empty())q.pop();
	for(int i=1;i<=n;i++)v[i]=0,d[i]=n;
	v[st]=1,d[st]=0,q.push(st); int x;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=1;i<=n;i++)if(!v[i]&&a[x][i]!=1e9)d[i]=d[x]+1,v[i]=1,q.push(i);
	}
	int ans=0;for(int i=1;i<=n;i++)ans+=d[i]; if(ans<ANS)ANS=ans;
}
void case1(){
	ANS=n*n;
	for(int i=1;i<=n;i++)st=i,bfs();
	printf("%d\n",ANS*la);
}
map<LL,int>f;
void up(LL x,int y){if(!f.count(x)||y<f[x])f[x]=y;}
inline int dfs(LL x){
	if(f.count(x))return f[x];
	int b[13],c[13]; LL t=x;for(int i=n;i;i--)b[i]=t%(n+1),t/=(n+1);
	int flag=1;for(int i=1;i<=n;i++)if(b[i]==n){flag=0;break;}
	if(flag)return f[x]=0;
	for(int i=1;i<=n;i++)c[i]=b[i];
	for(int i=1;i<=n;i++)if(b[i]==n)
	for(int j=1;j<=n;j++)if(b[j]<n&&a[i][j]!=1e9){
		int p=c[i];c[i]=b[j]+1;
		t=0;for(int k=1;k<=n;k++)t=t*(n+1)+c[k];
		up(f[x],dfs(t)+(b[j]+1)*a[i][j]); c[i]=p;
	}
	for(int i=1;i<=n;i++)printf("%d ",b[i]); printf("\n%lld %d \n",x,f[x]);
	return f[x];
}
int b[N];
void work(){
	int lim=1;for(int i=1;i<=n;i++)lim=lim*(n+1);
	f.clear();
	for(int i=1;i<=n;i++)b[i]=n; b[st]=0;
	LL t=0;for(int i=1;i<=n;i++)t=t*(n+1)+b[i];printf("%d\n",dfs(t));
}
int main(){
	freopen("treasure.in","r",stdin);
	//freopen("treasure.out","w",stdout);
	n=read(),m=read(); for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)a[i][j]=(i==j?0:1e9);
	int flag=1; la=-1;
	for(int i=1,x,y,z;i<=m;i++){
		x=read(),y=read(),z=read(),up(a[x][y],z),up(a[y][x],z);
		if(i>1&&z!=la)flag=0; la=z;
	}
	//if(flag){case1();return 0;}
	for(int i=1;i<=n;i++){
		st=i,work();
	}
	return 0;
}

#include<cstdio>
#include<queue>
#include<cstring>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
#define N 15
int n,m,a[N][N];
void up(int &x,int y){if(x==-1||y<x)x=y;}
int st,ANS,la; queue<int>q; int v[N],d[N];
void bfs(){
	while(!q.empty())q.pop();
	for(int i=1;i<=n;i++)v[i]=0,d[i]=n;
	v[st]=1,d[st]=0,q.push(st); int x;
	while(!q.empty()){
		x=q.front(),q.pop();
		for(int i=1;i<=n;i++)if(!v[i]&&a[x][i]!=1e9)d[i]=d[x]+1,v[i]=1,q.push(i);
	}
	int ans=0;for(int i=1;i<=n;i++)ans+=d[i]; if(ans<ANS)ANS=ans;
}
void case1(){
	ANS=n*n;
	for(int i=1;i<=n;i++)st=i,bfs();
	printf("%d\n",ANS*la);
}
int f[50000010];
inline int dfs(int x){
	if(f[x]!=-1)return f[x];
	int b[13],t=x;for(int i=n;i;i--)b[i]=t%(n+1),t/=(n+1);
	int flag=1;for(int i=1;i<=n;i++)if(b[i]==n){flag=0;break;}
	if(flag)return f[x]=0;
	for(int i=1;i<=n;i++)if(b[i]==n)
	for(int j=1;j<=n;j++)if(b[j]<n&&a[i][j]!=1e9){
		int p=b[i];b[i]=b[j]+1;
		t=0;for(int k=1;k<=n;k++)t=t*(n+1)+b[k];
		up(f[x],dfs(t)+(b[j]+1)*a[i][j]); b[i]=p;
	}
	//for(int i=1;i<=n;i++)printf("%d ",b[i]); printf("\n%d %d \n",x,f[x]);
	return f[x];
}
int b[N];
void work(){
	int lim=1;for(int i=1;i<=n;i++)lim=lim*(n+1);
	for(int i=0;i<lim;i++)f[i]=-1;
	for(int i=1;i<=n;i++)b[i]=n; b[st]=0;
	int t=0;for(int i=1;i<=n;i++)t=t*(n+1)+b[i];
	up(ANS,dfs(t));
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read(); for(int i=1;i<=n;i++)for(int j=1;j<=n;j++)a[i][j]=(i==j?0:1e9);
	int flag=1; la=-1;
	for(int i=1,x,y,z;i<=m;i++){
		x=read(),y=read(),z=read(),up(a[x][y],z),up(a[y][x],z);
		if(i>1&&z!=la)flag=0; la=z;
	}
	if(flag){case1();return 0;}
	ANS=-1;for(int i=1;i<=n;i++)st=i,work(); printf("%d\n",ANS);
	return 0;
}

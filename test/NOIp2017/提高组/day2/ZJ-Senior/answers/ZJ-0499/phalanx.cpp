#include<cstdio>
#include<map>
using namespace std;
inline int read(){
	int x=0;char ch=getchar();while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();return x;
}
int n,m,q;
int A[600010],s[600010];
void del(int x){for(int i=x;i<=2*m;i+=i&(-i))s[i]++;}
int S(int x){int y=0;for(int i=x;i;i-=i&(-i))y+=s[i]; return y;}
inline int get(int x,int L,int R){
	int mid,ans,t;
	while(L<=R){
		mid=(L+R)>>1; t=mid-S(mid);
		if(t==x)ans=mid,R=mid-1;else if(t<x)L=mid+1;else R=mid-1;
	}
	return ans;
}
void case2(){
	for(int j=1;j<=m;j++)A[j]=j; for(int j=1;j<=2*m;j++)s[j]=0;
	int x,y,z;
	for(int p=1;p<=q;p++){
		x=read(),y=read(),z=get(y,1,m+p-1); //printf("%d\n",z);
		printf("%d\n",A[z]);
		A[m+p]=A[z],A[z]=0;
		//for(int j=z;j<=m*2;j++)s[j]++;
		del(z);
		/*for(int j=1;j<=m+p;j++)printf("%d ",A[j]);puts("");
		for(int j=1;j<=m+p;j++)printf("%d ",j-S(j));puts("");
		puts("");*/
	}
}
int a[1010][1010];
void case1(){
	for(int i=1;i<=n;i++)for(int j=1;j<=m;j++)a[i][j]=(i-1)*m+j;
	int x,y,z;
	while(q--){
		x=read(),y=read(),z=a[x][y],printf("%d\n",z);
		for(int j=y;j<m;j++)a[x][j]=a[x][j+1];
		for(int i=x;i<n;i++)a[i][m]=a[i+1][m];
		a[n][m]=z;
		//for(int i=1;i<=n;i++){for(int j=1;j<=m;j++)printf("%d ",a[i][j]);puts("");}
		//puts("");
	}
}
#define LL long long
map<int,LL>mp[50010];
void case3(){
	int x,y; LL z;
	while(q--){
		x=read(),y=read();
		if(mp[x].count(y))z=mp[x][y];else z=1ll*(x-1)*m+y;
		printf("%lld\n",z);
		for(int j=y;j<m;j++)if(mp[x].count(j+1))mp[x][j]=mp[x][j+1];else mp[x][j]=1ll*(x-1)*m+j+1;
		for(int i=x;i<n;i++)if(mp[i+1].count(m))mp[i][m]=mp[i+1][m];else mp[i][m]=1ll*i*m+m;
		mp[n][m]=z;
		/*for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++)printf("%d ",mp[i].count(j)?mp[i][j]:(i-1)*m+j);puts("");
		}*/
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if(q<=500&&n<=1000&&m<=1000){case1();return 0;}
	if(n==1){case2();return 0;}
	if(q<=500){case3();return 0;}
	//case4();
}

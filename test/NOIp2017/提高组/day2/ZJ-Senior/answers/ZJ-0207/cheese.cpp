#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const long long N=1007;
long long T,n,m,X[N],Y[N],Z[N],h,r,to[N][N];
bool vis[N],get;
inline long long read(){
	long long ff=0,fu=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') fu=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9') {
		ff=ff*10+ch-'0';
		ch=getchar();
	}
	return ff*fu;
}
void dfs(long long x){
	vis[x]=true;
	if(x==n+1){
		get=true;
		return;
	}
	if(!get){
		for(long long i=1;i<=to[x][0];++i)
			if(!vis[to[x][i]])
				dfs(to[x][i]);
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--){
		get=false;
		memset(vis,false,sizeof vis);
		memset(to,0,sizeof to);
		n=read();h=read();r=read();
		for(long long i=1;i<=n;++i){
			X[i]=read();Y[i]=read();Z[i]=read();
			if(Z[i]+r>=h) to[i][++to[i][0]]=n+1;
			if(Z[i]-r<=0) to[0][++to[0][0]]=i;
		}
		for(long long i=1;i<=n;++i){
			for(long long j=i+1;j<=n;++j){
				if((X[i]-X[j])*(X[i]-X[j])+(Y[i]-Y[j])*(Y[i]-Y[j])+(Z[i]-Z[j])*(Z[i]-Z[j])<=r*r*4)
					to[i][++to[i][0]]=j,to[j][++to[j][0]]=i;
			}
		}
		dfs(0);
		if(get) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

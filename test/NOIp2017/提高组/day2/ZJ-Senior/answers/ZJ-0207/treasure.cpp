#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int inf=1e9+7;
int n,m,d[13][13],ans=inf,dep[20],s[20],v[20],tot,cnt;
bool vis[20],flag=true;
inline int read(){
	int ff=0,fu=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') fu=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9') {
		ff=ff*10+ch-'0';
		ch=getchar();
	}
	return ff*fu;
}
void dfs(){
	if(ans<tot) return;
	if(cnt==n){
		ans=tot;
		return;
	}
	for(int i=1;i<=cnt;++i){
		int x=s[i];
		for(int j=1;j<=n;++j)
			if(d[x][j]!=inf&&!vis[j]){
				s[++cnt]=j;
				v[j]=v[x]+1;
				tot+=v[x]*d[x][j];
				vis[j]=true;
				dfs();
				vis[j]=false;
				tot-=v[x]*d[x][j];
				--cnt;
			}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;++i)
		for(int j=1;j<=n;++j)
			if(i!=j)d[i][j]=inf;
			else d[i][j]=0;
	int x,y,z;
	int nn=-1;
	for(int i=1;i<=m;++i){
		x=read();y=read();z=read();
		if(nn!=-1){
			if(nn!=z) flag=false;
		}
		else nn=z;
		d[x][y]=min(d[x][y],z);
		d[y][x]=d[x][y];
	}		
	if(flag){
		tot=0;
		for(int i=1;i<=n;++i){
			for(int j=1;j<=n;++j) vis[j]=false;
			int v[20];
			tot=0;
			vis[i]=true;v[i]=1;
			int fro=1,rear=1,s[20];
			s[1]=i;
			while(fro<=rear){
				int x=s[fro];
				for(int j=1;j<=n;++j)
					if(!vis[j] && d[x][j]!=inf)
						vis[j]=true,tot+=v[x]*d[x][j],v[j]=v[x]+1,s[++rear]=j;
				++fro;
			}
			ans=min(ans,tot);	
		}
		cout<<ans<<'\n';
		return 0;
	}
	for(int i=1;i<=n;++i){
		tot=0;cnt=0;
		for(int j=1;j<=n;++j) vis[j]=false;
		vis[i]=true;v[i]=1;s[++cnt]=i;
		dfs();
	}
	cout<<ans<<'\n';
	return 0;
}

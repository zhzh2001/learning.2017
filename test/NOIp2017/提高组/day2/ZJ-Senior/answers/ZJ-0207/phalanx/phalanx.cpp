#include<iostream>
#include<cstdio>
#include<cstring>
const int N=3e5+7;
using namespace std;
int n,m,q,p[1001][1001],c[N];
inline int read(){
	int ff=0,fu=1;
	char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') fu=-1;
		ch=getchar();
	}
	while(ch>='0'&&ch<='9') {
		ff=ff*10+ch-'0';
		ch=getchar();
	}
	return ff*fu;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	for(int i=1;i<=n;++i)
	for(int j=1;j<=m;++j)
		p[i][j]=(i-1)*m+j;
	int x,y;
	while(q--){
		x=read();y=read();
		int t=p[x][y];
		printf("%d\n",t);
		for(int j=y;j<m;++j) p[x][j]=p[x][j+1];
		for(int i=x;i<n;++i) p[i][m]=p[i+1][m];
		p[n][m]=t;
	}
	return 0;
}

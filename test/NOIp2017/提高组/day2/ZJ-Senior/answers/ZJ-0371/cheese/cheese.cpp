#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
double eps=1e-8;
int f[1005],fa[1005];
long long x[1005],y[1005],z[1005];
long long read()
{
	char ch=getchar(),last='!';
	while (ch<'0' || ch>'9')
	{
		last=ch;
		ch=getchar();
	}
	long long ans=0;
	while (ch<='9' && ch>='0')
	{
		ans=ans*10+ch-48;
		ch=getchar();
	}
	if (last=='-') ans=-ans;
	return ans;
}
long long sqr(long long x)
{
	return x*x;
}
int Find(int x)
{
	if (fa[x]==x) return x;
	fa[x]=Find(fa[x]);
	return fa[x];
}
void Union(int x,int y)
{
	int rtx=Find(x),rty=Find(y);
	fa[rty]=rtx;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas=read();
	while (cas--)
	{
		int n=read();
		long long h=read(),r=read();
		for (int i=0; i<=n+1; i++) fa[i]=i;
		for (int i=1; i<=n; i++)
		{
			x[i]=read(); 
			y[i]=read();
			z[i]=read();
			if (z[i]-r>h || z[i]+r<0) 
			{
				f[i]=-1;
				continue;
			}
			if (z[i]+r>=h) Union(0,i);
			if (z[i]-r<=0) Union(i,n+1);
		}
		for (int i=1; i<=n; i++)
		{
			for (int j=i+1; j<=n; j++)
			if (f[i]!=-1 && f[j]!=-1)
			{
				double l=sqrt((double)(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])));
				if (l-(double)(r+r)<=eps) Union(i,j); 
			}
		}
		if (Find(0)!=Find(n+1)) printf("No\n");
		else printf("Yes\n");
	}
	return 0;
}

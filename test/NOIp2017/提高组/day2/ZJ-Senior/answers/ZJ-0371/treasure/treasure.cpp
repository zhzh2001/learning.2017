#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m;
bool flag[20],vis[1005];
long long x[1005],y[1005],l[1005],c[1005],a[20][20],q[20][2],f[20][20],dp[5000];
long long read()
{
	char ch=getchar(),last;
	while (ch<'0' || ch>'9')
	{
		last=ch;
		ch=getchar();
	}
	long long ans=0;
	while (ch<='9' && ch>='0')
	{
		ans=ans*10+ch-48;
		ch=getchar();
	}
	if (last=='-') ans=-ans;
	return ans;
}
void bfs(int s)
{
	memset(vis,0,sizeof(vis));
	int h=1,t=1;
	q[1][0]=s;
	q[1][1]=1;
	vis[s]=1;
	while (h<=t)
	{
		int u=q[h][0];
		for (int v=1; v<=n; v++)
		{
			if (v==u || !a[v][u] || vis[v]) continue;
			vis[v]=1;
			q[++t][0]=v;
			q[t][1]=q[h][1]+1;
		}
		h++;
	}
	for (int i=1; i<=t; i++) f[s][q[i][0]]=q[i][1];
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	long long oo=0ll;
	for (int i=1; i<=m; i++)
	{
		x[i]=read(),y[i]=read(),l[i]=read();
		a[x[i]][y[i]]=1;
		a[y[i]][x[i]]=1;
		oo=oo+l[i]*12;
	}
	for (int i=1; i<=n; i++)
	{
		bfs(i);
	}
	bool ff=1;
	for (int i=1; i<=n; i++) 
	{
		if (f[1][i]==0)
		{
			ff=0;
			break;
		}
	}
	if (ff && m==n-1)
	{
		long long ans=oo;
		for (int i=1; i<=n; i++)
		{
			long long s=0;
			for (int j=1; j<=m; j++)
			{
				s+=l[j]*f[i][x[j]];
			}
			ans=min(ans,s);
		}
		printf("%lld\n",ans);
		return 0;
	}
	int mx=(1<<n)-1;
	long long ans=oo;
	for (int i=1; i<=n; i++)
	{
		for (int j=1; j<=m; j++) c[j]=l[j]*f[i][x[j]];
		for (int j=0; j<=mx; j++) dp[j]=oo;
		dp[1<<(i-1)]=0;
		for (int sta=0; sta<mx; sta++)
		{
			memset(flag,0,sizeof(flag));
			for (int j=1; j<=n; j++)
				if (sta&(1<<(j-1))) flag[j]=1;
			/*
			for (int j=1; j<=n; j++)
			{
				if (flag[j]) continue;
				for (int k=1; k<=n; k++)
				{
					if (flag[k] && a[j][k]) 
					{
						for (int t=1; t<=m; t++)
						{
							if (x[t]==j && y[t]==k || x[t]==k && y[t]==j) dp[sta|(1<<j-1)]=min(dp[sta|(1<<j-1)],dp[sta]+c[t]);
						}
					}
				}
			} 
			*/
			for (int j=1; j<=m; j++) 
			{
				if (flag[x[j]] && !flag[y[j]]) dp[sta|(1<<(y[j]-1))]=min(dp[sta|(1<<(y[j]-1))],dp[sta]+c[j]);
				if (!flag[x[j]] && flag[y[j]]) dp[sta|(1<<(x[j]-1))]=min(dp[sta|(1<<(x[j]-1))],dp[sta]+c[j]);
			}
				
		}
		ans=min(ans,dp[mx]);
	}
	printf("%lld\n",ans);
	return 0;
}

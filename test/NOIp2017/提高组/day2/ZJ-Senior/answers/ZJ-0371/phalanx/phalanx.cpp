#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,m,q,a[1005][1005];
int N,M,b[2000005],id[200005],pre[200005],nex[200005],tree[200005];
long long read()
{
	char ch=getchar(),last;
	while (ch<'0' || ch>'9')
	{
		last=ch;
		ch=getchar();
	}
	long long ans=0;
	while (ch<='9' && ch>='0')
	{
		ans=ans*10+ch-48;
		ch=getchar();
	}
	if (last=='-') ans=-ans;
	return ans;
}
int fachu(int x)
{
	if (x==2) return 4;
	return 3;
}
int beilian(int x)
{
	if (x==3) return 4;
	return 2;
}
int lowbit(int x)
{
	return x&(-x);
}
int query(int x)
{
	int ans=0;
	while (x>0)
	{
		ans+=tree[x];
		x-=lowbit(x);
	}
	return ans;
}
void add(int x,int t)
{
	//if (t==1) printf("add+1:%d\n",x);
	//else printf("add-1:%d\n",x);
	while (x<=M)
	{
		tree[x]+=t;
		x+=lowbit(x);
	}
}
bool hav(int l,int r)
{
	//printf("query:%d %d %d %d %d\n",l,r,query(l-1),query(r),N);
	return (query(r)-query(l-1))>0;
}
int erfen(int l,int r)
{
	int s=l;
	if (!hav(s,r)) return -1;
	int ans=l;
	while (l<r)
	{
		int mid=(l+r)>>1;
		if (hav(s,mid)) r=mid;
		else l=mid+1; 
	}
	return l;
}
int Find(int x,int key)
{
	int t=erfen(x,N);
	//printf("%d %d %d %d\n",x,N,key,t);
	if (t==-1 || x+key-1<=t) return x+key-1;
	return Find(nex[t],key-(t-x+1));
}
void pf1()
{
	N=m;
	M=m+m+1;
	for (int i=1; i<=m; i++) b[i]=i,id[i]=1;
	pre[1]=0; nex[0]=1; id[1]=2; id[0]=3;
	while (q--)
	{
		int x=read(),y=read();
		if (y==m) 
		{
			printf("%d\n",b[N]);
			continue;
		}
		int t=Find(nex[0],y);
		//printf("%d\n",t);
		printf("%d\n",b[t]);
		if (id[t]==1)
		{
			add(t-1,1);
			nex[t-1]=t+1;
			pre[t+1]=t-1;
			id[t-1]=fachu(id[t-1]);
			id[t+1]=beilian(id[t+1]);
		}
		if (id[t]==2)
		{
			nex[pre[t]]=t+1;
			pre[t+1]=pre[t];
			id[t+1]=beilian(id[t+1]);
		}
		if (id[t]==3)
		{
			add(t-1,1);
			add(t,-1);
			nex[t-1]=nex[t];
			pre[nex[t]]=t-1;
			id[t-1]=fachu(id[t-1]);
		}
		if (id[t]==4)
		{
			add(t,-1);
			nex[pre[t]]=nex[t];
			pre[nex[t]]=pre[t];
		}
		b[++N]=b[t];
		id[t]=0;
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n==1 && m*q>100000000)
	{
		pf1();
		return 0;
	}
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++) a[i][j]=(i-1)*m+j;
	while (q--)
	{
		int x=read(),y=read();
		int t=a[x][y];
		for (int i=y; i<m; i++) a[x][i]=a[x][i+1];
		for (int i=x; i<n; i++) a[i][m]=a[i+1][m];
		a[n][m]=t;
		printf("%d\n",t);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<queue>
#include<stack>
using namespace std;
typedef long long ll;
inline int read()
{
	int kk=0,f=1;
	char cc=getchar();
	while(cc<'0'||cc>'9'){if(cc=='-')f=-1;cc=getchar();}
	while(cc>='0'&&cc<='9'){kk=(kk<<1)+(kk<<3)+cc-'0';cc=getchar();}
	return kk*f;
}
struct zj
{
	int x,y,z;
};
zj dian[2000];
int tt;int n,h,r;
bool pan(zj a,zj b,int r)
{
	ll a1=(a.x-b.x)*(a.x-b.x);
	ll a2=(a.y-b.y)*(a.y-b.y);
	ll a3=(a.z-b.z)*(a.z-b.z);
	ll a4=4*r*r;
	return (a1+a2+a3)<=a4;
}
int fath[1050];
int findfa(int x)
{
	if(fath[x]==x)return x;
	fath[x]=findfa(fath[x]);
	return fath[x];
}
bool mwork(int k1,int k2)
{
	return findfa(k1)==findfa(k2);
}
void mergefa(int a,int b)
{
	fath[findfa(b)]=findfa(a);
}
int shuzu[1050],zong[1050];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	tt=read();
	while(tt--)
	{
		n=read();h=read();r=read();
		int zhi=0;int zd=0;
		for(int i=1;i<=n;++i)
		{
			fath[i]=i;
			dian[i].x=read();dian[i].y=read();dian[i].z=read();
			if(dian[i].z<=2*r)shuzu[++zhi]=i;
			if(dian[i].z>=(h-2*r))zong[++zd]=i;
			
		}
		for(int i=1;i<=n;++i)
		{
			for(int j=1;j<=n;++j)
			{
				if(findfa(i)==findfa(j))continue;
				if(pan(dian[i],dian[j],r))mergefa(i,j);
			}
		}
		bool flag=0;
		while(zhi)
		{
			int lin=zd;
			while(zd)
			{
				flag=mwork(shuzu[zhi],zong[zd]);zd--;
				if(flag)break;
			}
			zhi--;zd=lin;
			if(flag)break;
		}
		if(flag)printf("Yes\n");
		else printf("No\n");
	}
	
}

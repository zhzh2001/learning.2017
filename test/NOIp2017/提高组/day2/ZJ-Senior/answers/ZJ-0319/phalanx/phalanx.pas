var
  a:array[0..1001,0..1001]of longint;
  n,m,q,cas,i,j,x,y,t:longint;

function make(x,y:longint):longint;
begin
  exit((x-1)*m+y);
end;

begin
  assign(input,'phalanx.in');
  assign(output,'phalanx.out');
  reset(input);
  rewrite(output);

  readln(n,m,q);
  for i:=1 to n do
    for j:=1 to m do
      a[i,j]:=make(i,j);
  for cas:=1 to q do
    begin
      readln(x,y);
      writeln(a[x,y]);
      t:=a[x,y];
      for i:=y to m-1 do a[x,i]:=a[x,i+1];
      for i:=x to n-1 do a[i,m]:=a[i+1,m];
      a[n,m]:=t;
    end;

  close(input);
  close(output);
end.

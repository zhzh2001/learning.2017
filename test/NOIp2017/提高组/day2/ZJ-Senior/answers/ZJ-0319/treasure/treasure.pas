var
  a,d:array[0..21,0..21]of longint;
  i,j,k,n,m,x,y,z,ans,val:longint;
  flag:boolean;

function min(a,b:longint):longint;
begin
  if a<b then exit(a) else exit(b);
end;

begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);

  readln(n,m);
  for i:=0 to n do
    for j:=0 to n do
      begin
        d[i,j]:=maxlongint div 3;
        a[i,j]:=maxlongint div 3;
      end;
  for i:=1 to m do
    begin
      readln(x,y,z);
      a[x,y]:=min(a[x,y],z);
      a[y,x]:=min(a[y,x],z);
      val:=a[x,y];
      d[x,y]:=1;
      d[y,x]:=1;
    end;
  for k:=1 to n do
    for i:=1 to n do
     for j:=1 to n do
       d[i,j]:=min(d[i,j],d[i,k]+d[k,j]);
  ans:=maxlongint;
  for i:=1 to n do
    begin
      x:=0;flag:=true;
      for j:=1 to n do if i<>j then x:=x+d[i,j]*val;
      ans:=min(ans,x);
    end;
  writeln(ans);

  close(input);
  close(output);
end.
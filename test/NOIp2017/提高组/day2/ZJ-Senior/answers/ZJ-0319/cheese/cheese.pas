var
  next,vet:array[0..2005001]of longint;
  head,x,y,z:array[0..1001]of longint;
  vis:array[0..1001]of boolean;
  i,j,n,h,r,t,num,e:longint;

procedure dfs(u:longint);
var
  x:longint;
begin
  vis[u]:=true;
  if u=n+1 then exit;
  x:=head[u];
  while x<>-1 do
    begin
      if not(vis[vet[x]]) then dfs(vet[x]);
      x:=next[x];
    end;
end;

function dist(i,j:int64):double;
begin
  exit(sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])));
end;

procedure add(x,y:longint);
begin
  inc(num);
  next[num]:=head[x];
  head[x]:=num;
  vet[num]:=y;
end;

procedure solve;
var
  i:longint;
begin
  readln(n,h,r);
  num:=0;
  fillchar(head,sizeof(head),255);
  for i:=1 to n do readln(x[i],y[i],z[i]);
  for i:=1 to n do
    for j:=i+1 to n do
      if (dist(i,j)<=2*r) then
        begin add(i,j);add(j,i);end;
  for i:=1 to n do
    begin
      if abs(z[i])<=r then begin add(0,i);add(i,0);end;
      if h-z[i]<=r then begin add(n+1,i);add(i,n+1);end;
    end;
  {
  for i:=0 to n+1 do
    begin
      e:=head[i];
      write(i,':');
      while e<>-1 do
        begin
          write(vet[e],' ');
          e:=next[e];
        end;
      writeln;
    end;
  }
  fillchar(vis,sizeof(vis),false);
  e:=head[0];
  vis[0]:=true;
  while e<>-1 do
    begin
      dfs(vet[e]);
      if vis[n+1]=true then
        begin
          writeln('Yes');
          exit;
        end;
      e:=next[e];
    end;
  writeln('No');
end;

begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input);
  rewrite(output);

  readln(t);
  for i:=1 to t do solve;

  close(input);
  close(output);
end.

var
  dis:array[1..12,1..12] of longint;
  father:array[1..12] of longint;
  visit:array[1..12] of boolean;
  bfs:array[1..12] of record
                        x,long:longint;
                      end;
  a:array[1..1000] of longint;
  edge:array[1..1000] of record
                           x,y,z:longint;
                         end;
  flag:array[1..1000] of boolean;
  ans,n,m,i,j,x,y,z:longint;
function min(x,y:longint):longint;
begin
  if x<y then exit(x) else exit(y);
end;
function find(son:longint):longint;
begin
  if father[son]=son then exit(son);
  find:=find(father[son]);
  father[son]:=find;
end;
function calc(sum:longint):longint;
var
  head,tail,i:longint;
begin
  head:=1;
  tail:=1;
  bfs[1].x:=edge[a[1]].x;
  bfs[1].long:=1;
  for i:=1 to n do visit[i]:=false;
  visit[edge[a[1]].x]:=true;
  calc:=0;
  while head<=tail do
  begin
    for i:=1 to sum do
      if (edge[a[i]].x=bfs[head].x) and not visit[edge[a[i]].y] then
        begin
          inc(tail);
          visit[edge[a[i]].y]:=true;
          bfs[tail].x:=edge[a[i]].y;
          bfs[tail].long:=bfs[head].long+1;
          calc:=calc+bfs[head].long*edge[a[i]].z;
        end;
    inc(head);
  end;
  for i:=1 to n do
    if not visit[i] then
      exit(maxlongint);
end;
procedure sc(sum:longint);
var
  x,y,i,save_father,now:longint;
  bool:boolean;
begin
  if sum=n-1 then
    begin
      ans:=min(ans,calc(sum));
      exit;
    end;
  for now:=1 to m do
    if not flag[now] then
      begin
        x:=find(edge[now].x);
        y:=find(edge[now].y);
        if x<>y then
          begin
            save_father:=father[edge[now].y];
            a[sum+1]:=now;
            father[edge[now].y]:=edge[now].x;
            flag[now]:=true;
            sc(sum+1);
            father[edge[now].y]:=save_father;
            flag[now]:=false;
          end;
      end;
end;
begin
  assign(input,'treasure.in'); reset(input);
  assign(output,'treasure.out'); rewrite(output);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      dis[i,j]:=maxlongint;
  for i:=1 to m do
  begin
    readln(x,y,z);
    dis[x,y]:=min(dis[x,y],z);
    dis[y,x]:=min(dis[y,x],z);
  end;
  m:=0;
  for i:=1 to n do
    for j:=1 to n do
      if (dis[i,j]<>maxlongint) and (i<>j) then
        begin
          inc(m);
          edge[m].x:=i;
          edge[m].y:=j;
          edge[m].z:=dis[i,j];
        end;
  for i:=1 to n do father[i]:=i;
  ans:=maxlongint;
  sc(0);
  writeln(ans);
  close(input);
  close(output);
end.

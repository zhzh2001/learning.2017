var
  x,y,z:array[1..1000] of longint;
  visit:array[1..1000] of boolean;
  bfs:array[1..1000] of longint;
  t,g,n,h,r,head,tail,i:longint;
  dis:real;
  bool:boolean;
function pd_arrive(i:longint):boolean;
begin
  if z[i]+r>=h then exit(true)
  else exit(false);
end;
function pd(i,j:longint):boolean;
begin
  dis:=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]));
  if dis<=2*r then exit(true)
  else exit(false);
end;
begin
  assign(input,'cheese.in'); reset(input);
  assign(output,'cheese.out'); rewrite(output);
  readln(t);
  for g:=1 to t do
  begin
    readln(n,h,r);
    bool:=false;
    head:=1;
    tail:=0;
    for i:=1 to n do
    begin
      readln(x[i],y[i],z[i]);
      visit[i]:=false;
      if z[i]-r<=0 then
        begin
          inc(tail);
          bfs[tail]:=i;
          visit[i]:=true;
          if pd_arrive(i) then bool:=true;
        end;
    end;
    while head<=tail do
    begin
      for i:=1 to n do
        if pd(bfs[head],i) and not visit[i] then
          begin
            inc(tail);
            bfs[tail]:=i;
            visit[i]:=true;
            if pd_arrive(i) then
              begin
                bool:=true;
                break;
              end;
          end;
      inc(head);
      if bool then break;
    end;
    if bool then writeln('Yes')
    else writeln('No');
  end;
  close(input);
  close(output);
end.
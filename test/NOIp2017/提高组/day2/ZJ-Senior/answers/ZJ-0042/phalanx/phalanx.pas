var
  a:array of array of longint;
  n,m,q,i,j,num,x,y:longint;
begin
  assign(input,'phalanx.in'); reset(input);
  assign(output,'phalanx.out'); rewrite(output);
  readln(n,m,q);
  if (n<=1000) and (m<=1000) then
    begin
      setlength(a,1001);
      for i:=1 to 1000 do setlength(a[i],1001);
      for i:=1 to n do
        for j:=1 to m do
          a[i,j]:=(i-1)*m+j;
      for i:=1 to q do
      begin
        readln(x,y);
        writeln(a[x,y]);
        num:=a[x,y];
        for j:=y to m-1 do a[x,j]:=a[x,j+1];
        for j:=x to n-1 do a[j,m]:=a[j+1,m];
        a[n,m]:=num;
      end;
    end
  else if (n=1) and (m<=300000) then
    begin
      setlength(a,2);
      setlength(a[1],100001);
      for i:=1 to q do
      begin
        readln(x,y);
        writeln(a[x,y]);
        num:=a[x,y];
        for j:=y to m-1 do a[x,j]:=a[x,j+1];
        a[n,m]:=num;
      end;
    end
  else
    begin

    end;
  close(input);
  close(output);
end.

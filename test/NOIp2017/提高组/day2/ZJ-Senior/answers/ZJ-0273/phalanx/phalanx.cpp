#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define rs p<<1|1
#define ls p<<1
#define db double
using namespace std;
int n,m,q;
const int N2=5e4+20;
struct P2{
	int Index;
	bool Mark[N2];
	map<ll,ll> A[N2];
	void Do(){
		for(int i=1;i<=n;i++){
			A[i][m]=1ll*(i-1)*m+m;
		}
		for(int i=1;i<=q;i++){
			int x,y;
			scanf("%d %d",&x,&y);
			if(!Mark[x]){
				Mark[x]=1;
				for(int j=1;j<m;j++){
					A[x][j]=1ll*(x-1)*m+j;
				}
			}
			ll t=A[x][y];
			printf("%lld\n",t);
			A[x][y]=0;
			for(int j=2;j<=m;j++){
				if(!A[x][j-1]){
					A[x][j-1]=A[x][j];
					A[x][j]=0;
				}
			}
			for(int j=2;j<=n;j++){
				if(!A[j-1][m]){
					A[j-1][m]=A[j][m];
					A[j][m]=0;	
				}
			}
			A[n][m]=t;
		}
	}
}Solve2;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d %d %d",&n,&m,&q);
	if(q<=500) Solve2.Do();
	return 0;	
}
/*
2 2 3
1 1
2 2
1 2
*/

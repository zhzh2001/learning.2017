#include<bits/stdc++.h>
#define pb push_back
#define rs p<<1|1
#define ls p<<1
#define db double
using namespace std;
void Min(int &x,int y){
	if(x==-1||x>y) x=y;	
}
const int N=13;
struct node{
	int v,w;	
};
int n,m;
struct P1{
	int ans;
	vector<node>E[N+20];
	void dfs(int x,int f,int dep){
		for(int i=0;i<E[x].size();i++){
			int V=E[x][i].v;
			if(V==f) continue;
			ans+=E[x][i].w*dep;
			dfs(V,x,dep+1);	
		}
	}
	void Do(){
		for(int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d %d %d",&u,&v,&w);
			E[u].pb((node){v,w});
			E[v].pb((node){u,w});
		}
		int ret=-1;
		for(int i=1;i<=n;i++){
			ans=0;
			dfs(i,i,1);
			Min(ret,ans);
		}
		printf("%d\n",ret);
	}
}Solve1;
struct P2{
	int g[N+20][N+20],vis[N+2];
	queue<node>Q;
	int BFS(int x){
		int ans=0;
		while(!Q.empty()) Q.pop();
		Q.push((node){x,1});
		memset(vis,0,sizeof(vis));
		vis[x]=1;
		while(!Q.empty()){
			node now=Q.front();
			Q.pop();
			for(int i=1;i<=n;i++){
				if(vis[i]) continue;
				vis[i]=1;
				if(g[now.v][i]==-1) continue;
				int dep=now.w;
				ans+=g[now.v][i]*dep;
				Q.push((node){i,now.w+1});
			}
		}
		return ans;
	}
	void Do(){
		memset(g,-1,sizeof(g));
		for(int i=1;i<=m;i++){
			int u,v,w;
			scanf("%d %d %d",&u,&v,&w);
			Min(g[u][v],w);
			Min(g[v][u],w);
		}
		int ret=-1;
		for(int st=1;st<=n;st++){
			Min(ret,BFS(st));
		}
		printf("%d\n",ret);
	}
}Solve2;
//struct P3{
//	int g[N+20][N+20],ans,dp[1<<N][N+1][N+2];
//	void Do(){
//		ans=-1;
//		memset(g,-1,sizeof(g));
//		for(int i=1;i<=m;i++){
//			int u,v,w;
//			scanf("%d %d %d",&u,&v,&w);
//			u--,v--;
//			Min(g[u][v],w);
//			Min(g[v][u],w);
//		}
//		int All=((1<<n)-1);
//		for(int st=0;st<n;st++){
//			memset(dp,-1,sizeof(dp));
//			dp[1<<st][st][1]=0;
//			for(int i=0;i<1<<n;i++){
//				int Mn=-1;
//				for(int a=0;a<n;a++){
//					if(!(i&(1<<a))) continue;
//					for(int b=1;b<=n;b++){
//						if(~dp[i][a][b]) Min(Mn,dp[i][a][b]); 
//					}
//				}
//				for(int a=0;a<n;a++){
//					if(!(i&(1<<a))) continue;
//					for(int b=1;b<=n;b++){
//						if(~Mn&&~dp[i][a][b]) Min(dp[i][a][b],Mn); 
////						if(i==3&&st==0) cout<<dp[i][a][b]<<endl;
//					}
////					if(i==3&&st==0) cout<<Mn<<endl;
//				}
//				for(int j=0;j<n;j++){
//					if(i&(1<<j)) continue;
//					for(int k=0;k<n;k++){
//						if(!(i&(1<<k))) continue;
//						for(int t=1;t<=n;t++){
////							if(st==0&&(i|(1<<j))==11&&k==0) cout<<dp[i][k][t]<<endl;
//							if(dp[i][k][t]==-1||g[j][k]==-1) continue;
//							Min(dp[i|(1<<j)][j][t+1],dp[i][k][t]+g[k][j]*t);
//						}
//					}
//				}
//			}
//			for(int k=0;k<n;k++){
//				for(int t=0;t<=n;t++){
//					if(~dp[All][k][t]){
//						Min(ans,dp[All][k][t]);
////						if(st==0&&dp[All][k][t]==2) cout<<k<<" "<<t<<endl;
//					}
//				}
//			}
////			if(st==0) cout<<ans<<endl;
//		}
//		printf("%d\n",ans);
//	}
//}Solve3;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	if(m==n-1) Solve1.Do();
	else Solve2.Do();
//	Solve3.Do();
	return 0;
}

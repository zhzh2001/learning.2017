#include<bits/stdc++.h>
#define ll long long
#define pb push_back
#define rs p<<1|1
#define ls p<<1
#define db double
using namespace std;
const int N=1012;
struct node{
	ll x,y,z;	
};
ll r,h,n;
int ok;
node B[N];
vector<int>E[N];
bool vis[N];
bool Get(ll X1,ll Y1,ll Z1,ll X2,ll Y2,ll Z2){
	ll Dis=sqrt((X1-X2)*(X1-X2)+(Y1-Y2)*(Y1-Y2)+(Z1-Z2)*(Z1-Z2));
	return Dis<=2LL*r;
}
void dfs(int x){
	if(B[x].z+r>=h){
		ok=1;
		return ;	
	}
	for(int i=0;i<E[x].size();i++){
		int V=E[x][i];
		if(vis[V]) continue;
		vis[V]=1;
		dfs(V);
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	scanf("%d",&T);
	while(T--){
		scanf("%lld %lld %lld",&n,&h,&r);
		for(int i=1;i<=n;i++){
			ll x,y,z;
			scanf("%lld %lld %lld",&x,&y,&z);
			B[i]=(node){x,y,z};
		}
		for(int i=1;i<=n;i++) E[i].clear();
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				if(Get(B[i].x,B[i].y,B[i].z,B[j].x,B[j].y,B[j].z)){
					E[i].pb(j);
					E[j].pb(i);
				}
			}
		}
		ok=0;
		for(int i=1;i<=n;i++){
			if(B[i].z<=r){
				memset(vis,0,sizeof(vis));
				vis[i]=1;
				dfs(i);
			}
		}
		if(ok){
			printf("Yes\n");	
		}
		else printf("No\n");
	}
	return 0;
}
/*
3
2 4 1
0 0 1
0 0 3
2 5 1
0 0 1
0 0 4
2 5 2
0 0 2
2 0 4
*/

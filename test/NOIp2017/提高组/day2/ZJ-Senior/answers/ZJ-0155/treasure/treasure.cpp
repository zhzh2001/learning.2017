#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const long long INF=0x7f7f7f7f;
long long dp[4100],ans,tmp;
int n,m,x,y,w,g[13][13],dis[4100][13];
void read(int &x)
{
	x=0; char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0'; ch=getchar();
	}
}
long long solve()
{
	for(int i=1;i<(1<<n);i++){
		dp[i]=INF;
		for(int j=1;j<=n;j++){
			dis[i][j]=0;
		}
	}
	for(int i=1;i<=n;i++) {
	    dp[1<<(i-1)]=0;
	    dis[1<<(i-1)][i]=1;
	}
	for(int i=1;i<(1<<n);i++){
		for(int j=1;j<=n;j++){
			if(((1<<(j-1))&i)==0) continue;
				for(int t=1;t<=n;t++){
					if((1<<(t-1))&i) continue; if(g[j][t]==-1) continue; 
					int tmp=(i|(1<<(t-1)));
					if(dp[i]+1ll*dis[i][j]*g[j][t]<dp[tmp]){
					    dp[tmp]=dp[i]+1ll*dis[i][j]*g[j][t];
	                    for(int k=1;k<=n;k++) {
	                    	if((1<<(k-1))&i) dis[tmp][k]=dis[i][k];
						}
						dis[tmp][t]=dis[i][j]+1;
					}
				}
		}
	}
	return dp[(1<<n)-1];
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
    memset(g,-1,sizeof g);
	read(n); read(m);
	for(int i=1;i<=m;i++){
		read(x); read(y); read(w);
		if(g[x][y]==-1 || g[x][y]>w) g[x][y]=w;
		if(g[y][x]==-1 || g[y][x]>w) g[y][x]=w;
	}
	ans=solve();
	printf("%lld\n",ans);
	return 0;
}

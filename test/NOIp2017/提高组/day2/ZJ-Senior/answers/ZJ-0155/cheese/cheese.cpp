#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
struct bian{
	int to,nxt;
}e[10000];
struct node{
	int x,y,z;
}p[1010];
int dfn[2020],low[2020],vis[2020],stk[2020],n,top,r,h,T,cnt,sum,tot,kuai[2020],lk[2020];
double dis(int a,int b)
{
	return sqrt((p[a].x-p[b].x)*(p[a].x-p[b].x)+(p[a].y-p[b].y)*(p[a].y-p[b].y)+(p[a].z-p[b].z)*(p[a].z-p[b].z));
}
void read(int &x)
{
	x=0; int f=1; char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f=-1; ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0'; ch=getchar();
	}
	x*=f;
}
void add(int x,int y)
{
	e[tot].to=y; e[tot].nxt=lk[x]; lk[x]=tot; tot++;
}
void tarjan(int u)
{
	low[u]=dfn[u]=++cnt;
	stk[++top]=u; 
	vis[u]=true;
	for(int i=lk[u];i!=-1;i=e[i].nxt) {
		int wk=e[i].to;
		if(!dfn[wk]){
			tarjan(wk);
			low[u]=min(low[u],low[wk]);
		}
		else if(vis[wk]) low[u]=min(low[u],dfn[wk]); 
	}
	if(low[u]==dfn[u]){
		sum++;
		while(stk[top]!=u){
			vis[stk[top]]=false;
			kuai[stk[top]]=sum;
			top--;
		}
		vis[stk[top]]=false;
		kuai[stk[top]]=sum;
		top--;
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--){
		tot=0; memset(lk,-1,sizeof lk);
		memset(vis,false,sizeof vis); memset(kuai,0,sizeof kuai);
		memset(dfn,0,sizeof dfn); memset(low,0,sizeof low);
		top=0; cnt=0; sum=0;
		read(n); read(h); read(r);
		for(int i=1;i<=n;i++) {
			read(p[i].x); read(p[i].y); read(p[i].z);
		}
		for(int i=1;i<=n;i++){
		    for(int j=1;j<=n;j++){
		    	if(j==i) continue;
		    	if(dis(i,j)<=2.0*r) {
		    		add(i,j); add(j,i);
				}
			} 
		}
		for(int i=1;i<=n;i++){
			if(abs(p[i].z)<=r){
				add(n*2,i); add(i,n*2);
			}
			if(abs(h-p[i].z)<=r){
				add(n*2+1,i); add(i,n*2+1);
			}
		}
		tarjan(n<<1);
		if(kuai[n<<1]==kuai[n*2+1]) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

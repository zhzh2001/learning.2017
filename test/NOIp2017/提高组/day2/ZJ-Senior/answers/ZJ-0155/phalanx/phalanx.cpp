#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int a[5000][5000];
int n,m,q,x,y;
void read(int &x)
{
	x=0; int f=1; char ch=getchar();
	while(ch<'0'||ch>'9') {
		if(ch=='-') f=-1; ch=getchar();
	}
	while(ch>='0'&&ch<='9'){
		x=x*10+ch-'0'; ch=getchar();
	}
	x*=f;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n); read(m); read(q);
	
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=(i-1)*m+j;
		}
	}
	for(int i=1;i<=q;i++){
		read(x); read(y);
		printf("%d\n",a[x][y]);
		int tmp=a[x][y];
		for(int j=y;j<m;j++) a[x][j]=a[x][j+1];
		for(int j=x;j<n;j++) a[j][m]=a[j+1][m];
		a[n][m]=tmp; 
	}
	return 0;
}

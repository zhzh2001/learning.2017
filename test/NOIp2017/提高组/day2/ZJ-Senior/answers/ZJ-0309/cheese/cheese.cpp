#include<cstdio>
#include<cstring>
#include<cmath>
#include<iostream>
#include<algorithm>
using namespace std;
const int M=1005;
const double EPS=1e-9;
int n,h,r;
int X[M],Y[M],Z[M];
int q[M];
int mark[M],TIM;
double sqr(double x){
	return x*x;
}
bool check(int a,int b){
	return sqrt(sqr(X[a]-X[b])+sqr(Y[a]-Y[b])+sqr(Z[a]-Z[b]))-r*2<EPS;
}
void solve(){
	TIM++;
	scanf("%d %d %d",&n,&h,&r);
	for(int i=1;i<=n;i++)scanf("%d %d %d",&X[i],&Y[i],&Z[i]);
	int L=0,R=0,x;
	for(int i=1;i<=n;i++)
		if(Z[i]-r<=0)
			q[R++]=i,mark[i]=TIM;
	while(L<R){
		x=q[L++];
		if(Z[x]+r>=h){
			puts("Yes");
			return;
		}
		for(int i=1;i<=n;i++)
			if(check(x,i)&&mark[i]!=TIM)
				q[R++]=i,mark[i]=TIM;
	}
	puts("No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas;scanf("%d",&cas);
	while(cas--)solve();
	return 0;
}

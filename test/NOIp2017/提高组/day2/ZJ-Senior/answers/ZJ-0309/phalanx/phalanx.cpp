#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector> 
using namespace std;
const int M=(int)3e5+5,N=M*2,MM=(int)2e7+5;
template<class T>void Rd(T &res){
	res=0;static char p;
	while(p=getchar(),p<'0');
	do{
		res=(res<<1)+(res<<3)+(p^48);
	}while(p=getchar(),p>='0');
}
template<class T>void Ps(T x){
	if(x==0)return;
	Ps(x/10);putchar(x%10^48);
}
template<class T>void Pt(T x){
	Ps(x);putchar('\n');
}
int n,m,q,qx[M],qy[M];
struct SHUI{
	int a[1005][1005];//100W 
	void solve(){
		for(int i=1,c=0;i<=n;i++)
			for(int j=1;j<=m;j++)
				a[i][j]=++c;
		for(int i=1,ans;i<=q;i++){
			Pt(ans=a[qx[i]][qy[i]]);
			for(int j=qy[i];j<m;j++)a[qx[i]][j]=a[qx[i]][j+1];
			for(int j=qx[i];j<n;j++)a[j][m]=a[j+1][m];
			a[n][m]=ans;
		}
	}
}P30;
struct IHSU{
	long long Q[N],val[N];
	int sum[N*4];//480W 
	void build(int L,int R,int p){
		sum[p]=R-L+1;
		if(L==R)return;
		int mid=(L+R)>>1;
		build(L,mid,p<<1),build(mid+1,R,p<<1|1);
	}
	int query(int L,int R,int p,int v){
		sum[p]--;
		if(L==R)return L;
		int mid=(L+R)>>1;
		if(sum[p<<1]>=v)return query(L,mid,p<<1,v);
		else return query(mid+1,R,p<<1|1,v-sum[p<<1]);
	}
	void solve(){
		int mq=m+q;
		build(1,mq,1);
		for(int i=1;i<=m;i++)val[i]=i;
		int L=0,R=0;
		for(int i=2;i<=n;i++)Q[R++]=(long long)i*m;
		long long ans;
		for(int i=1,j=m+1;i<=q;i++,j++){
			Pt(ans=val[query(1,mq,1,qy[i])]);
			Q[R++]=ans;
			val[j]=Q[L++];
		}
	}
}P30_1;
struct UHSI{
	int Rt[M],vrt,nq,mq;
	int Lson[MM],Rson[MM],SUM[MM],tot;//3000W 
	int cnt[M],pps[M];
	long long val[N];
	vector<long long>ps[M];
	int query(int L,int R,int &tid,int v){
		if(tid==0)tid=++tot,SUM[tid]=R-L+1;
		SUM[tid]--;
		if(L==R)return L;
		int mid=(L+R)>>1,vls;
		if(Lson[tid]==0)vls=mid-L+1;
		else vls=SUM[Lson[tid]];
		if(vls>=v)return query(L,mid,Lson[tid],v);
		else return query(mid+1,R,Rson[tid],v-vls);
	}
	void solve(){
		nq=n+q,mq=m+q;
		long long tp=0;
		for(int i=1;i<=n;i++)tp+=m,val[i]=tp;
		for(int i=1;i<=q;i++)
			if(qy[i]!=m)
				cnt[qx[i]]++;
		for(int i=1;i<=n;i++)ps[i].resize(cnt[i]),pps[i]=0;
		long long ans;
		for(int i=1,j=n+1,x,y,qr;i<=q;i++,j++){
			x=qx[i],y=qy[i];
			if(y==m){//特判 直接将最右边一列的第x个放出来，然后再放到这一列的最后面 
				Pt(ans=val[query(1,nq,vrt,x)]);
				val[j]=ans;
			}else {
				qr=query(1,mq,Rt[x],y);
				if(qr<m)ans=(long long)(x-1)*m+qr;
				else ans=ps[x][qr-m];
				Pt(ans);
				val[j]=ans;
				ps[x][pps[x]++]=(val[query(1,nq,vrt,x)]);
			}
		}
	}
}PPP;
int main(){
//	printf("%.5f\n",1.0*(sizeof(P30)+sizeof(P30_1)+sizeof(PPP))/1024/1024);
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	Rd(n),Rd(m),Rd(q);
	bool is_x_1=true;
	for(int i=1;i<=q;i++){
		Rd(qx[i]),Rd(qy[i]);
		if(qx[i]!=1)is_x_1=false;
	}
//	if(0);
	if(n<=1000&&m<=1000)P30.solve();
	else if(is_x_1)P30_1.solve();
	else PPP.solve();
	return 0;
}

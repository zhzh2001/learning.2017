#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
const int INF=(int)1e8+7;
const long long inf=(long long)1e18+7;
int n,m;
int dis[15][15],bin[15];
struct SHUI{
	int dp[16777220];//1600W int 一共256M 这里用了64M 
	int bin8[15];
	int de[15],UP;
	int dfs(int S,int SS){
		if(S==UP)return 0;
		int &re=dp[SS],tmp;
		if(re!=-1)return re;
		re=INF;
		int stk1[15],sz1=0,stk2[15],sz2=0;
		for(int i=0;i<n;i++)
			if(S&bin[i])stk1[sz1++]=i;
			else stk2[sz2++]=i;
		for(int i=0,a,b;i<sz1;i++)
			for(int j=0;j<sz2;j++){
				a=stk1[i],b=stk2[j];
				if(dis[a][b]!=INF){
					de[b]=de[a]+1;
					tmp=dfs(S|bin[b],SS+de[b]*bin8[b])+de[a]*dis[a][b];
					if(tmp<re)re=tmp;
				}
			}
		return re;
	}
	void solve(){
		UP=bin[n]-1;
		memset(dp,-1,sizeof(dp));
		bin8[0]=1;
		for(int i=1;i<15;i++)bin8[i]=bin8[i-1]*8;
		de[0]=1;
		int ans=dfs(1,1),tmp;
		for(int i=1;i<n;i++){
			de[i]=1;
			tmp=dfs(bin[i],bin8[i]);
			if(tmp<ans)ans=tmp;
		}
		printf("%d\n",ans);
	}
}P70;
struct HUSI{
	int dp[12][(1<<12)];
	int UP;
	int dfs(int h,int S){
		int &re=dp[h][S],tmp;
		if(re!=-1)return re;
		h++;
		re=INF;
		int DIS[15],stk1[15],sz1=0,stk2[15],sz2=0;
		for(int i=0;i<n;i++)
			if(bin[i]&S)stk1[sz1++]=i;
			else stk2[sz2++]=i;
		for(int i=0;i<sz2;i++)DIS[stk2[i]]=INF;
		for(int i=0;i<sz2;i++)
			for(int j=0;j<sz1;j++){
				tmp=dis[stk2[i]][stk1[j]]*h;
				if(tmp<DIS[stk2[i]])DIS[stk2[i]]=tmp;
			}
		for(int i=UP^S,j=i;i;i=(i-1)&j){
			tmp=dfs(h,S|i);
			for(int k=0;k<sz2;k++)
				if(bin[stk2[k]]&i)
					tmp+=DIS[stk2[k]];
			if(tmp<re)re=tmp;
		}
		return re;
	}
	void solve(){
		memset(dp,-1,sizeof(dp));
		UP=bin[n]-1;
		for(int i=0;i<n;i++)dp[i][UP]=0;
		int ans=dfs(0,bin[0]),tmp;
		for(int i=1;i<n;i++){
			tmp=dfs(0,bin[i]);
			if(tmp<ans)ans=tmp;
		}
		printf("%d\n",ans);
	}
}PPP;
int main(){
//	printf("%.5f\n",1.0*(sizeof(P70)+sizeof(PPP))/1024/1024);
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	for(int i=0;i<15;i++)bin[i]=(1<<i);
	scanf("%d %d",&n,&m);
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			dis[i][j]=INF;
	for(int i=1,x,y,d;i<=m;i++){
		scanf("%d %d %d",&x,&y,&d);x--,y--;
		if(d<dis[x][y])dis[x][y]=dis[y][x]=d;
	}
//	if(0);
	if(n<=8)P70.solve();
	else PPP.solve();
	return 0;
}

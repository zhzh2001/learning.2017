#include <cstdio>
#include <cmath>
#define nc (c=getchar())
using namespace std;
struct nde {
	int x,y,z;
}d[1005];
const double eps=1e-9;
inline void read(int &a)
{
	a=0;int f=1;char c;nc;
	while (c<'0'||c>'9') {if (c=='-') f=-1;nc;}
	while (c>='0'&&c<='9') a=(a<<3)+(a<<1)+c-48,nc;
	a*=f;
}
int T;
int n,h,r;
long long sqr(int x){return 1ll*x*x;}
double wor(nde a,nde b)
{
	return sqrt(1.0*sqr(a.x-b.x)+sqr(a.y-b.y)+sqr(a.z-b.z));
}
int fa[1005];
int getf(int x) {return x==fa[x]?x:fa[x]=getf(fa[x]);}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int i,j;
	read(T);
	while (T--)
	{
		read(n),read(h),read(r);
		fa[n+1]=n+1,fa[n+2]=n+2;
		for (i=1;i<=n;i++)
		{
			fa[i]=i;
			read(d[i].x),read(d[i].y),read(d[i].z);
			if (d[i].z-r<=0) fa[getf(i)]=getf(n+1);
			if (d[i].z+r>=h) fa[getf(i)]=getf(n+2);
		}
		for (i=1;i<=n-1;i++)
		for (j=i+1;j<=n;j++)
		{
			double dis=wor(d[i],d[j]);
			if (dis-2*r<eps)
			{
				int fi=getf(i),fj=getf(j);
				if (fi!=fj)
				{
					fa[fi]=fj;
				}
			}
		}
		int x=getf(n+1),y=getf(n+2);
		if (x!=y) printf("No\n");
		else printf("Yes\n");
	}
}

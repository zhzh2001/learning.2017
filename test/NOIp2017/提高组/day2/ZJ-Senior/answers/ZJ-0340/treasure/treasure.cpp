#include <cstdio>
#define nc (c=getchar())
using namespace std;
inline void read(int &a)
{
	a=0;int f=1;char c;nc;
	while (c<'0'||c>'9') {if (c=='-') f=-1;nc;}
	while (c>='0'&&c<='9') a=(a<<3)+(a<<1)+c-48,nc;
	a*=f;
}
int map[15][15];
inline int min(int a,int b){return a<b?a:b;}
int n,m;
int ans=1e9;
struct ppp{
	int x,bl;
}a[15];
bool vis[15];
void dfs(int x,int now,int w)
{
	if (w>=ans) return ;
	a[now].x=x;vis[x]=1;
	if (now==n)
	{
		ans=min(ans,w);
		vis[x]=0;
		return ;
	}
	for (int i=1;i<=now;i++)
	{
		for (int j=1;j<=n;j++)
		{
			if (map[a[i].x][j]>0&&!vis[j]){a[now+1].bl=a[i].bl+1,dfs(j,now+1,w+a[i].bl*map[a[i].x][j]);}
		}
	}
	vis[x]=0;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int i,j;
	read(n),read(m);
	for (i=1;i<=m;i++)
	{
		int u,v,w;
		read(u),read(v),read(w);
		if (!map[u][v]) map[u][v]=map[v][u]=w;
		else map[u][v]=map[v][u]=min(map[u][v],w);
	}
	for (i=1;i<=n;i++)
	{
		a[1].bl=1;
		dfs(i,1,0);vis[i]=0;
	}
	printf("%d",ans);
}

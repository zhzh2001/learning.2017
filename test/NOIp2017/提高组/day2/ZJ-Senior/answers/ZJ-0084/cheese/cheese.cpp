#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
struct E
{
	LL to,nxt;
}e[2003000];
LL f1[1100],ne;
bool vis[1100];
LL x[1100],y[1100],z[1100];
LL n,h,r,T;
void me(LL a,LL b)
{
	e[++ne].to=b;
	e[ne].nxt=f1[a];
	f1[a]=ne;
	e[++ne].to=a;
	e[ne].nxt=f1[b];
	f1[b]=ne;
}
void dfs(LL u)
{
	vis[u]=1;
	for(LL i=f1[u];i!=0;i=e[i].nxt)
		if(!vis[e[i].to])
			dfs(e[i].to);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	register LL i,j;
	LL d,dd;
	scanf("%lld",&T);
	while(T--)
	{
		memset(vis,0,sizeof(vis));
		ne=0;
		memset(f1,0,sizeof(f1));
		scanf("%lld%lld%lld",&n,&h,&r);
		for(i=1;i<=n;i++)
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		dd=4*r*r;
		for(i=1;i<=n;i++)
			for(j=i+1;j<=n;j++)
			{
				d=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
				if(d<=dd)	me(i,j);
			}
		for(i=1;i<=n;i++)
			if(z[i]<=r)	me(i,n+1);
		for(i=1;i<=n;i++)
			if(h-z[i]<=r)	me(i,n+2);
		dfs(n+1);
		if(vis[n+2])
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}

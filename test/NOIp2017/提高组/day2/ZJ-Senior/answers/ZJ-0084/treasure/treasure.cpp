#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,dis[20][20],ans=0x3f3f3f3f,v;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int t,i,j,k,a,b;
	memset(dis,0x3f,sizeof(dis));
	scanf("%d%d",&n,&m);
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d",&a,&b,&v);
		dis[a][b]=dis[b][a]=min(dis[a][b],1);
	}
	for(i=1;i<=n;i++)
		dis[i][i]=0;
	for(k=1;k<=n;k++)
		for(i=1;i<=n;i++)
			for(j=1;j<=n;j++)
				dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
	for(i=1;i<=n;i++)
	{
		t=0;
		for(j=1;j<=n;j++)
			if(j!=i)
				t+=dis[i][j]+1;
		ans=min(ans,t*v);
	}
	printf("%d",ans);
	return 0;
}

#include <iostream>
#include <bits/stdc++.h>
using namespace std;
int t,n,h,r,p;
long r2,d;
struct node{
	int x;
	int y;
	int z;
}e[1001];
int b[1001];
void dfs(int k){
	b[k]=1;
	if(e[k].z+r>=h){
		cout<<"Yes"<<endl;
		p=1;
		return;
	}
	for(int i=1;i<=n;i++){
		if(i!=k && b[i]==0){
			d=sqrt((e[k].x-e[i].x)*(e[k].x-e[i].x)+(e[k].y-e[i].y)*(e[k].y-e[i].y)+(e[k].z-e[i].z)*(e[k].z-e[i].z));
			if(d<=r2)dfs(i);
		}
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>t;
	for(int j=1;j<=t;j++){
		p=0;
		cin>>n>>h>>r;
		r2=r*2;
		for(int i=1;i<=n;i++){
			b[i]=0;
			cin>>e[i].x>>e[i].y>>e[i].z;
		}
		for(int i=1;i<=n;i++){
			if(e[i].z<=r && b[i]==0)dfs(i);
		}
		if(p==0)cout<<"No"<<endl;
	}
	return 0;
}

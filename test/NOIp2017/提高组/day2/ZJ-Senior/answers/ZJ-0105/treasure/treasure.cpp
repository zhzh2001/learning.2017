#include <iostream>

using namespace std;

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m,a,b,v,v0,minv;
	long ans=0,s;
	cin>>n>>m;
	int w[n+1][n+1],l[n+1],t[n+1],d[n+1];
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			w[i][j]=0;
		}
	}
	for(int i=1;i<=m;i++){
		cin>>a>>b>>v;
		w[a][b]=v;
		w[b][a]=v;
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			l[j]=0;
			t[j]=0;
			d[j]=-1;
		}
		d[i]=0;
		s=0;
		v0=i;
		l[v0]=1;
		t[v0]=1;
		for(int j=1;j<=n;j++){
			if(t[j]==0 && w[v0][j]!=0 && (d[j]==-1 || l[v0]*w[v0][j]<d[j])){
				d[j]=l[v0]*w[v0][j];
			}
		}
		for(int k=2;k<=n;k++){
			minv=0;
			for(int j=1;j<=n;j++){
				if(w[v0][j]!=0 && t[j]==0){
					l[j]=l[v0]+1;
					if((d[j]!=-1 && d[j]<minv) || minv==0){
						minv=d[j];
						v0=j;
					}
				}
			}
			t[v0]=1;
			s+=minv;
			for(int j=1;j<=n;j++){
				if(t[j]==0 && w[v0][j]!=0 && (d[j]==-1 || l[v0]*w[v0][j]<d[j])){
					d[j]=l[v0]*w[v0][j];
				}
			}
		}
		if(ans==0 || s<ans)ans=s;
	}
	cout<<ans;
	return 0;
}

#include <iostream>

using namespace std;

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q,x,y;
	cin>>n>>m>>q;
	long a[n+1][m+1],t;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			a[i][j]=(i-1)*m+j;
		}
	}
	for(int i=1;i<=q;i++){
		cin>>x>>y;
		cout<<a[x][y]<<endl;
		t=a[x][y];
		for(int j=y;j<m;j++){
			a[x][j]=a[x][j+1];
		}
		for(int j=x+1;j<=n;j++){
			a[j-1][m]=a[j][m];
		}
		a[n][m]=t;
	}
	return 0;
}

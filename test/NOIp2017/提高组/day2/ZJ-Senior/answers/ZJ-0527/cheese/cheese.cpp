#include<cstdio>
#include<cmath>
#include<cstring>
using namespace std;
typedef long long ll;
int n;
bool s[1005][1005];
ll r,h,a[1005],b[1005],c[1005],shang[1005],xia[1005];
inline void read(int &x)
{
	char ch;bool fu;
	while((ch=getchar())<'0'||ch>'9')if(ch=='-')fu=true;
	x=ch-'0';
	while((ch=getchar())>='0'&&ch<='9')x=(x<<3)+(x<<1)+ch-'0';
	if(fu==true)x=-x;
}
double dist(ll x1,ll y1,ll z1,ll x2,ll y2,ll z2)
{
	ll xx=abs(x1-x2);ll yy=abs(y1-y2);ll zz=abs(z1-z2);
	return (double)sqrt(xx*xx+yy*yy+zz*zz);
}
ll minn(ll q,ll w)
{
	if(q<=w)return q;
	else return w;
}
ll maxx(ll q,ll w)
{
	if(q>=w)return q;
	else return w;
}
void workk(ll x,ll y)
{
	shang[x]=shang[y]=maxx(shang[x],shang[y]);
	xia[x]=xia[y]=minn(xia[x],xia[y]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--)
	{
		memset(s,false,sizeof(s));
		memset(shang,0,sizeof(shang));
		memset(xia,0,sizeof(xia));
		scanf("%d%lld%lld",&n,&h,&r);
		for(int i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&a[i],&b[i],&c[i]);
			shang[i]=c[i]+r;xia[i]=c[i]-r;
			for(int j=1;j<i;j++)
			if(dist(a[j],b[j],c[j],a[i],b[i],c[i])<=2*r)
			{
				s[i][j]=s[j][i]=1;workk(i,j);
			}
		}
		for(int i=1;i<=n;i++)
		  for(int j=n;j>=1;j--)
		    if(i!=j&&s[i][j])workk(i,j);
		bool pp=0;
		for(int i=1;i<=n;i++)
		if(xia[i]<=0&&shang[i]>=h){pp=1;break;}
		if(pp)printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

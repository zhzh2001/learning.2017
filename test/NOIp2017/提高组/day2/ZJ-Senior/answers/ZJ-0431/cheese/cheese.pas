var
  n,t,m,h,r,num,i,k:longint;
  b:boolean;
  x,y,z:array[1..1000]of longint;
  c,d:array[1..1000]of boolean;
procedure qsort(l,r:longint);
var i,j,mid,p:longint;
begin
  i:=l;
  j:=r;
  mid:=z[(i+j) div 2];
  repeat
    while z[i]<mid do inc(i);
    while z[j]>mid do dec(j);
    if i<=j then
    begin
      p:=x[i]; x[i]:=x[j]; x[j]:=p;
      p:=y[i]; y[i]:=y[j]; y[j]:=p;
      p:=z[i]; z[i]:=z[j]; z[j]:=p;
      inc(i); dec(j);
    end;
  until i>j;
  if l<j then qsort(l,j);
  if i<r then qsort(i,r);
end;
function pan(l,q:longint):boolean;
var x1,y1,z1:longint;
begin
  x1:=abs(x[l]-x[q]);
  y1:=abs(y[l]-y[q]);
  z1:=abs(z[l]-z[q]);
  if (x1*x1+y1*y1+z1*z1)<=((r+r)*(r+r)) then exit(true) else exit(false);
end;
procedure find(t:longint);
var i:longint;
begin
  if c[t] then
  begin
    b:=true;
    exit;
  end;
  for i:=1 to m do
    if (not b) then
    begin
      if (not d[i]) and (z[i]-r>0) then
      begin
        d[i]:=true;
        if pan(t,i) then find(i);
        d[i]:=false;
      end;
    end else break;
end;
begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  readln(n);
  while t<n do
  begin
    readln(m,h,r);
    num:=0;
    fillchar(c,sizeof(c),false);
    for i:=1 to m do readln(x[i],y[i],z[i]);
    b:=false;
    k:=0;
    qsort(1,m);
    for i:=1 to m do
      begin
        if z[i]-r<=0 then num:=num+1;
        if z[i]+r>=h then
        begin
          c[i]:=true;
          k:=k+1;
        end;
      end;
    if k=0 then
    begin
      writeln('No');
      continue;
    end;
    fillchar(d,sizeof(d),false);
    for i:=1 to num do if not b then begin d[i]:=true; find(i); d[i]:=false end
                                else break;
    if not b then writeln('No') else writeln('Yes');
    t:=t+1;
  end;
  close(input);close(output);
end.
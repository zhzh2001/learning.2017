var
  n,m,i,x,y,z,k,p:longint;
  s:int64;
  min:array[1..12]of longint;
  b:array[1..12]of boolean;
  f:array[1..12,1..12]of longint;
procedure find(t,l:longint);
var i:longint;
begin
  if k>=s then exit;
  if p=n then
  begin
    s:=k;
    exit;
  end;
  for i:=1 to n do
    if (f[t,i]=min[i]) and (not b[i]) then
    begin
      k:=k+f[t,i]*l;
      b[i]:=true;
      p:=p+1;
      find(i,l+1);
    end;
end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  readln(n,m);
  for i:=1 to n do min[i]:=maxlongint;
  for i:=1 to m do
    begin
      readln(x,y,z);
      f[x,y]:=z;
      f[y,x]:=z;
      if min[x]>z then min[x]:=z;
      if min[y]>z then min[y]:=z;
    end;
  s:=maxlongint;
  for i:=1 to n do
    begin
      b[i]:=true;
      find(i,1);
      k:=0;
      p:=1;
      fillchar(b,sizeof(b),false);
    end;
  writeln(s);
  close(input);close(output);
end.
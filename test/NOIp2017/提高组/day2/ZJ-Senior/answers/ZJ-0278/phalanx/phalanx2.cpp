#include<cstdio>
const int P=505,O=50005;
//int n,m,q,i,j,x,y,l,r,mx,mid,cnt,b[N],c[N];
int n,m,q,i,j,o,x,t,v[P],w[P],H,L,idh[P],idl[P],hid[O],lid[O],vh[P][O],vl[O][P];
bool hang[O],lie[O];

int read()
{
	int x=0; char ch=getchar();
	for (;ch<48 || ch>57;ch=getchar());
	for (;ch>47 && ch<58;ch=getchar()) x=x*10+ch-48;
	return x;
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx2.out","w",stdout);
	n=read(),m=read(),q=read();
	for (i=1;i<=q;++i) v[i]=read(),w[i]=read(),hang[v[i]]=1,lie[w[i]]=1;
	hang[n]=lie[m]=1;
	for (i=1;i<=n;++i)
		if (hang[i]) idh[++H]=i,hid[i]=H;
	for (i=1;i<=H;++i)
		for (j=1,o=(idh[i]-1)*m;j<=m;++j) vh[i][j]=o+j;
	for (i=1;i<=m;++i)
		if (lie[i]) idl[++L]=i,lid[i]=L;
	for (i=1;i<=L;++i)
		for (j=1;j<=n;++j) vl[j][i]=idl[i]+n*(j-1);
	for (i=1;i<=q;++i)
	{
		t=vh[hid[v[i]]][w[i]],printf("%d\n",t);
		for (j=w[i]+1;j<=m;++j)
		{
			x=vh[hid[v[i]]][j-1]=vh[hid[v[i]]][j];
			if (lie[j-1]) vl[v[i]][lid[j-1]]=x;
		}
		for (j=v[i]+1;j<=n;++j)
		{
			x=vl[j-1][lid[m]]=vl[j][lid[m]];
			if (hang[j-1]) vh[hid[j-1]][m]=x;
		}
		vh[H][m]=vl[n][L]=t;
	}
	return 0;
}

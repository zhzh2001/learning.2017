#include<cstdio>
const int M=1005,N=600005,P=505,O=50005;
int n,m,q,i,j,x,y,t,a[M][M];
int l,r,mx,mid,cnt,b[N],c[N];
int o,v[P],w[P],H,L,idh[P],idl[P],hid[O],lid[O],vh[P][O],vl[O][P];
bool hang[O],lie[O];

int read()
{
	int x=0; char ch=getchar();
	for (;ch<48 || ch>57;ch=getchar());
	for (;ch>47 && ch<58;ch=getchar()) x=x*10+ch-48;
	return x;
}

void add(int x) { for (;x<mx;x+=x&-x) ++c[x]; }
void Add(int x) { for (;x<mx;x+=x&-x) --c[x]; }
int sum(int x) { int ans=0;	for (;x;x-=x&-x) ans=ans+c[x];	return ans; }

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n<M && m<M)
	{
		for (i=1;i<=n;++i)
			for (j=1;j<=m;++j) a[i][j]=++t;
		for (i=1;i<=q;++i)
		{
			x=read(),y=read(),t=a[x][y],printf("%d\n",t);
			for (j=y+1;j<=m;++j) a[x][j-1]=a[x][j];
			for (j=x+1;j<=n;++j) a[j-1][m]=a[j][m];
			a[n][m]=t;
		}
	}
	else
		if (n==1)
		{
			cnt=m; mx=m+q+2;
			for (i=1;i<mx;++i) add(i),b[i]=i;
			for (i=1;i<=q;++i)
			{
				x=read(),y=read();
				for (l=1,r=cnt;l<=r;)
				{
					mid=(l+r)>>1,x=sum(mid);
					if (x<y) l=mid+1;
					else if (x^y) r=mid-1;
						 else if (b[mid]) break;
						 	  else r=mid-1;
				}
				printf("%d\n",b[mid]);
				++cnt,b[cnt]=b[mid],b[mid]=0,Add(mid);
			}
		}
		else
		{
			for (i=1;i<=q;++i) v[i]=read(),w[i]=read(),hang[v[i]]=1,lie[w[i]]=1;
			hang[n]=lie[m]=1;
			for (i=1;i<=n;++i)
				if (hang[i]) idh[++H]=i,hid[i]=H;
			for (i=1;i<=H;++i)
				for (j=1,o=(idh[i]-1)*m;j<=m;++j) vh[i][j]=o+j;
			for (i=1;i<=m;++i)
				if (lie[i]) idl[++L]=i,lid[i]=L;
			for (i=1;i<=L;++i)
				for (j=1;j<=n;++j) vl[j][i]=idl[i]+n*(j-1);
			for (i=1;i<=q;++i)
			{
				t=vh[hid[v[i]]][w[i]],printf("%d\n",t);
				for (j=w[i]+1;j<=m;++j)
				{
					x=vh[hid[v[i]]][j-1]=vh[hid[v[i]]][j];
					if (lie[j-1]) vl[v[i]][lid[j-1]]=x;
				}
				for (j=v[i]+1;j<=n;++j)
				{
					x=vl[j-1][lid[m]]=vl[j][lid[m]];
					if (hang[j-1]) vh[hid[j-1]][m]=x;
				}
				vh[H][m]=vl[n][L]=t;
			}
		}
	return 0;
}

#include<cstdio>
const int M=10005;
#define ll long long
#define pd(x1,y1,z1,x2,y2,z2) (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2)<=4*r*r
ll h,r,x[M],y[M],z[M]; int i,j,T,n,p,q,fa[M]; bool f;

ll read()
{
	ll x=0; char ch=getchar(); bool f=1;
	for (;ch<48 || ch>57;ch=getchar()) if (ch=='-') f=0;
	for (;ch>47 && ch<58;ch=getchar()) x=x*10+ch-48;
	return f?x:-x;
}

int find(int x) { if (x^fa[x]) fa[x]=find(fa[x]); else return x; }

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (T=read();T;--T)
	{
		n=read(),h=read(),r=read();
		for (i=1;i<=n;++i) x[i]=read(),y[i]=read(),z[i]=read();
		for (i=1;i<=n;++i) fa[i]=i;
		for (i=1;i<n;++i)
			for (j=i+1;j<=n;++j)
				if (pd(x[i],y[i],z[i],x[j],y[j],z[j]))
				{
					p=find(i),q=find(j);
					if (p<q) fa[q]=p;
					if (p>q) fa[p]=q;
				}
		f=1;
		for (i=1;i<=n && f;++i)
			for (j=1;j<=n && f;++j)
				if (z[i]<=r && z[j]+r>=h && find(i)==find(j))
					{ f=0; break; }
		if (f) puts("No"); else puts("Yes");
	}
	return 0;
}

#include<cstdio>
const int INF=1000000000;
int ans,mn,n,m,i,j,x,y,a[20][20],num[20];
bool vis[20];

int read()
{
	int x=0; char ch=getchar();
	for (;ch<48 || ch>57;ch=getchar());
	for (;ch>47 && ch<58;ch=getchar()) x=x*10+ch-48;
	return x;
}

void dfs(int x)
{
	if (ans>mn) return;
	int i; bool f=1;
	for (i=1;i<=n;++i)
		if (!vis[i]) { f=0; break; }
	if (f) { mn=ans; return; }
	for (i=1;i<=n;++i)
		if (!vis[i] && a[x][i]<INF)
		{
			ans=ans+a[x][i]*num[x];
			vis[i]=1;num[i]=num[x]+1;
			dfs(i);dfs(x);
			vis[i]=0;
			ans=ans-a[x][i]*num[x];
		}
}

int min(int x,int y) { return x<y?x:y; }

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read(); mn=INF;
	for (i=1;i<=n;++i)
		for (j=1;j<=n;++j) a[i][j]=INF;
	for (i=1;i<=m;++i)
		x=read(),y=read(),a[x][y]=a[y][x]=min(a[x][y],read());
	for (i=1;i<=n;++i)
	{
		for (j=1;j<=n;++j) vis[j]=0;
		vis[i]=1; num[i]=1;
		ans=0;
		if (i==1) dfs(i);
	}
	printf("%d\n",mn);
	return 0;
}

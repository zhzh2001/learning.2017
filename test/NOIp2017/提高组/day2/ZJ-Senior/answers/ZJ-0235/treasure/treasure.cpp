#include<cstdio>
#include<iostream>
#include<queue>
using namespace std;


int n,m;
int a,b,c;

queue <int> q;
int e[13][13];
long long d[13],l[13];
bool flag[13];
long long ans=10000000000,num;




int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&a,&b,&c);
		if(e[a][b]){
			if(e[a][b]>c){
				e[a][b]=e[b][a]=c;
			}
		}else{
			e[a][b]=e[b][a]=c;
		}
	}
	
	
	for(int gen=1;gen<=n;gen++){
		for(int i=1;i<=n;i++){
			flag[i]=false;
			d[i]=0;
			l[i]=10000000000;
		}
		q.push(gen);flag[gen]=true;
		d[gen]=1,l[gen]=0;
		while(!q.empty()){
			int u=q.front();
			q.pop();
			flag[u]=false;
			for(int v=1;v<=n;v++){
				if(e[u][v]){
					if(d[u]*e[u][v]<l[v]){
						l[v]=d[u]*e[u][v];
						d[v]=d[u]+1;
						if(!flag[v]){
							q.push(v);
							flag[v]=true;
						}
					}
				}
			}
		}
		num=0;
		for(int i=1;i<=n;i++){
			num+=l[i];
		}
		ans=min(ans,num);
	}
	printf("%lld",ans);
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

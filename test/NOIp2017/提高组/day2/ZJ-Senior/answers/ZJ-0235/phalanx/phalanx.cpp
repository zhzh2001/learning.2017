#include<cstdio>
#include<iostream>

using namespace std;


int n,m,t;
int a,b,c;
int q[10001][10001];
int line[300001];

inline int read(){
	int x=0,ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
	return x;
}



int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	n=read(),m=read(),t=read();
	if(n==1){
		for(int i=1;i<=m;i++) line[i]=i;
		for(int i=1;i<=t;i++){
			a=read(),b=read();
			c=line[b];
			printf("%d\n",c);
			for(int i=b;i<m;i++) line[i]=line[i+1];
			line[m]=c;
		}
	}else{
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++)
				q[i][j]=(i-1)*m+j;
		}
		for(int i=1;i<=t;i++){
			a=read(),b=read();
			c=q[a][b];
			printf("%d\n",c);
			for(int i=b;i<m;i++) q[a][i]=q[a][i+1];
			for(int i=a;i<n;i++) q[i][m]=q[i+1][m];
			q[n][m]=c;
		}
	}
		
	
	
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

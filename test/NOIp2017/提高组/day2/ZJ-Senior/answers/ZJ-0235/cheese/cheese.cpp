#include<cstdio>
#include<iostream>

using namespace std;


int n,h,r,T;
int a,b,c;
struct Node{
	int x,y,z;
}w[1003];
int top[1003],down[1003];

long long R;
inline bool pan(int a,int b){
	long long x,y,z;
	x=(long long)(w[a].x -w[b].x)*(w[a].x -w[b].x);
	y=(long long)(w[a].y -w[b].y)*(w[a].y -w[b].y);
	z=(long long)(w[a].z -w[b].z)*(w[a].z -w[b].z);
	if((x+y+z)<=R) return true;
	else return false;
}

bool flag;

int f[1003];
int find(int x){
	if(x!=f[x]) f[x]=find(f[x]);
	return f[x];
}
void unite(int x,int y){
	find(x),find(y);
	top[f[y]]=max(top[f[y]],top[f[x]]);
	down[f[y]]=min(down[f[y]],down[f[x]]);
	f[f[x]]=f[y];
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	for(int zu=1;zu<=T;zu++){
		scanf("%d%d%d",&n,&h,&r);
		flag=false;
		R=(long long)4*r*r;
		for(int i=1;i<=n;i++){
			f[i]=i;
			scanf("%d%d%d",&w[i].x,&w[i].y,&w[i].z);
			top[i]=w[i].z+r;
			down[i]=w[i].z-r;
		}
		for(int i=1;i<=n;i++){
			for(int j=1;j<i;j++){
				if(pan(i,j)){
					if(find(i)!=find(j)){
						unite(f[i],f[j]);
					}
				}
			}
		}
		for(int i=1;i<=n;i++){
			if(f[i]==i){
				if(top[i]>=h&&down[i]<=0){
					flag=true;
					break;
				} 
			}
		}
		if(flag) printf("Yes\n");
		else printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	
	return 0;
}

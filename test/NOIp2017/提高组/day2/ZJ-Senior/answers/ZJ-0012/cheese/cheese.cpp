#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 1005
using namespace std;
int cas,n,i,p1,p2,pp,u,j;
int flag[N],ups[N],downs[N],a[N][N],q[N];
long long x[N],y[N],z[N],h,r;
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&cas);
	while (cas--)
	{
		scanf("%d%lld%lld",&n,&h,&r);
		for (i=1;i<=n;i++)ups[i]=downs[i]=flag[i]=0;
		for (i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if (z[i]<=r)ups[i]=1;
			if (z[i]>=h-r)downs[i]=1;
		}
		for (i=1;i<n;i++)
			for (j=2;j<=n;j++)
				if (r*r*2*2>=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]))
					a[i][j]=a[j][i]=1;else a[i][j]=a[j][i]=0;
		p1=1;p2=0;
		for (i=1;i<=n;i++)
			if (ups[i])
			{
				q[++p2]=i;
				flag[i]=1;
			}
		while (p1<=p2)
		{
			u=q[p1++];
			for (i=1;i<=n;i++)
				if (a[u][i] && (flag[i]==0))
				{
					flag[i]=1;
					q[++p2]=i;
				}				
		}
		pp=0;
		for (i=1;i<=n;i++)
			if (flag[i] && downs[i])pp=1;
		if (pp)puts("Yes");else puts("No");
	}
}

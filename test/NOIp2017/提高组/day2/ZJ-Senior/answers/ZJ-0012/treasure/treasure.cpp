#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int i,j,n,m,u,v,x,inf,k,pp,now,ans,pd,t;
int cost[50][50],len[50][50],flag[50],de[50];
void dfs(int k,int sum)
{
	//printf("%d %d\n",k,sum);
	if (sum>ans)return;
	t=t+n;
	if (t>8000000)return;
	if (k==n)
	{
		//puts("!");
		ans=min(ans,sum);
		return;
	}
	int i,j,pre;
	for (i=1;i<=n;i++)
		if (flag[i])
			for (j=1;j<=n;j++)
				if (cost[i][j]!=inf && (!flag[j]))
				{
					flag[j]=1;
					de[j]=de[i]+1;
					dfs(k+1,sum+cost[i][j]*de[j]);
					de[j]=inf;
					flag[j]=0;
				}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout); 
	inf=1e8;
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			cost[i][j]=inf;
	pp=0;
	for (i=1;i<=m;i++)
	{
		scanf("%d%d%d",&u,&v,&x);
		if (i==1)pd=x;
		if (x!=pd)pp=1;
		cost[u][v]=min(cost[u][v],x);
		cost[v][u]=min(cost[v][u],x);
	}
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			if (cost[i][j]!=inf)
				len[i][j]=1;else len[i][j]=inf;
	for (k=1;k<=n;k++)
		for (i=1;i<=n;i++)
			for (j=1;j<=n;j++)
				len[i][j]=min(len[i][j],len[i][k]+len[k][j]);
	for (i=1;i<=n;i++)len[i][i]=0;
	if (!pp)
	{
		ans=inf;
		for (i=1;i<=n;i++)
		{
			now=0;
			for (j=1;j<=n;j++)
				now=now+len[i][j]*pd;
			ans=min(ans,now);
		}
		printf("%d\n",ans);
		return 0;
	}
	ans=inf;
	t=0;
	//puts("!");
	for (i=1;i<=n;i++)
	{
		for (j=1;j<=n;j++)de[j]=inf;
		de[i]=0;
		flag[i]=1;
		dfs(1,0);
		flag[i]=0;
		de[i]=inf;
	}
	printf("%d\n",ans);
	return 0;
}

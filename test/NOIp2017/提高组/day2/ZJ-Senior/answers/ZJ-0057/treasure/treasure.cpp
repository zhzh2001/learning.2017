#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
const int N=13;
const int INF=1<<26;
const int M=5000;
int n,m,a[N][N],d[M];
bool vis[M];
struct node {
	int t[N],v,n;
	node(){	}
	node(int x,int y,int z) {
		memset(t,-1,sizeof(t));
		t[x]=y;
		v=z;n=1;
	}
};
queue<node> q;

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)a[i][j]=INF;//cout<<n<<endl;
	for(int i=1;i<=m;i++)
	{
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		x--;y--;
		a[x][y]=a[y][x]=min(a[x][y],z);
	}
	int ans=INF;
	for(int i=0;i<n;i++)
	{
		memset(vis,0,sizeof(vis));
		memset(d,63,sizeof(d));
		while(!q.empty())q.pop();
		q.push(node(i,1,(1<<i)));
		vis[1<<i]=1;d[1<<i]=0;
		while(!q.empty())
		{
			node k=q.front();
			q.pop();//cout<<k.v<<" "<<d[k.v]<<"   ";
			vis[k.v]=0;
		//	if(d[k.v]>ans)continue;
			for(int i=0;i<n;i++) if(k.t[i]>0)
				for(int j=0;j<n;j++) if(k.t[j]<0&&a[i][j]<INF)
				{
					node x=k;x.v=k.v+(1<<j);
					if(d[x.v]>d[k.v]+a[i][j]*k.t[i])
					{
						d[x.v]=d[k.v]+a[i][j]*k.t[i];
						if(x.v==((1<<n)-1))ans=min(ans,d[x.v]);
					//	if(d[x.v]>ans)continue;
						if(!vis[x.v])
						{
							x.t[j]=k.t[i]+1;x.n=k.n+1;
							q.push(x);
							vis[x.v]=1;//
						}
					}
				}
		}
	}
	printf("%d\n",ans);
	return 0;
}


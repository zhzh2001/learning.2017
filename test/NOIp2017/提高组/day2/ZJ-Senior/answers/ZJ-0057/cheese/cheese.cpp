#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<queue>
using namespace std;
const int N=1010;
const int M=1000010;
int n,h,r,T,tot,list[N],Next[M],toit[M],x[N],y[N],z[N];
queue<int> q;
bool vis[N];

void add(int x,int y)
{//cout<<x<<" "<<y<<endl;
	Next[++tot]=list[x];
	list[x]=tot;
	toit[tot]=y;
}

double dist(int a,int b)
{
	return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]));
}

bool Push(int k)
{
	if(z[k]+r>=h)
	{
		printf("Yes\n");
		return 1;
	}
	q.push(k);
	vis[k]=1;
	return 0;
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		scanf("%d%d%d",&n,&h,&r);
		memset(vis,0,sizeof(vis));
		while(!q.empty())q.pop();
		memset(list,0,sizeof(list));
		bool flag=0;
		for(int i=1;i<=n;i++)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if(z[i]<=r)if(Push(i))
			{
				flag=1;
				break;
			}
			for(int j=1;j<i;j++)
				if(dist(i,j)<=2*r)add(i,j),add(j,i);
		}
		if(flag)continue;
		while(!q.empty())
		{
			int k=q.front();
			q.pop();
			for(int i=list[k];i;i=Next[i])
				if(!vis[toit[i]])if(Push(toit[i]))
				{
					flag=1;
					break;
				}
			if(flag)break;
		}
		if(flag)continue;
		printf("No\n");
	}
	return 0;
}

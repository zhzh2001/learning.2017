#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=1010;
const int S=N*N;
int n,m,q,list[N*30],Next[S],Left[N*30],a[300000];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for(int i=1;i<=n;i++)
	{
		list[i]=(i-1)*m+1;
		for(int j=(i-1)*m+1;j<i*m;j++)
			Next[j]=j+1;
		Next[i*m]=0;
		Left[i]=i*m-1;
	}
	if(n==1&&m>1000)
	{
		while(q--)
		{
			int x,y,t;
			scanf("%d%d",&x,&y);
			if(y==1)
			{
				t=list[x];
				list[x]=Next[t];
			}
			else
			{
				int j=list[x];
				for(int i=2;i<y;i++)
					j=Next[j];
				t=Next[j];
				Next[j]=Next[t];
			}
			Next[Left[x]]=t;
			printf("%d\n",t);
		}
		return 0;
	}
	while(q--)
	{
		int x,y,t;
		scanf("%d%d",&x,&y);
		if(y==1)
		{
			t=list[x];
			list[x]=Next[t];
		}
		else
		{
			int j=list[x];
			for(int i=2;i<y;i++)
				j=Next[j];
			t=Next[j];
			Next[j]=Next[t];
		}
		if(y<m)Left[x]=Next[Left[x]];
		for(int i=x;i<n;i++)
			Next[Left[i]]=Next[Left[i+1]];
		Next[Left[n]]=t;
		Next[t]=0;
		printf("%d\n",t);
	}
	return 0;
}

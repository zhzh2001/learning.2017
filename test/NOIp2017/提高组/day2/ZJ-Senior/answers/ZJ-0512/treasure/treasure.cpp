#include <iostream>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
using namespace std;
int n,m,g[13][13],v,p,type;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) g[i][j]=1e9;
	for(int i=1;i<=m;i++){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);v=z;if(x==p) type=1;p=x;
		if(z<g[x][y]) g[x][y]=g[y][x]=z;
	}
	if(type) printf("%d",(n-1)*v);
	else printf("%d",(n)*(n-1)/2*v);
	return 0;
}

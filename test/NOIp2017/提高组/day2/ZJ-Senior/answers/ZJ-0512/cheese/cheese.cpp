#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <cstdlib>
using namespace std;
int T,n,h,r;
long long sr;
struct node{int x,y,z;} p[1001];
bool g[1010][1010],vis[1010];
long long dis(int a,int b){
	long long dx=p[a].x-p[b].x,dy=p[a].y-p[b].y,dz=p[a].z-p[b].z;
	dx*=dx;dy*=dy;dz*=dz;double ans=dx+dy+dz;if(ans>2*sr) return sr+1;
	else return dx+dy+dz;
}
void dfs(int x){
	vis[x]=1;
	for(int i=0;i<=n+1;i++) if((!vis[i])&&g[x][i]) {dfs(i);if(vis[n+1]) return;}
	if(vis[n+1]) return;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%d%d%d",&n,&h,&r);sr=2*r;sr*=sr;
		for(int i=0;i<=n+1;i++) for(int j=0;j<=n+1;j++) g[i][j]=0;
		for(int i=0;i<=n+1;i++) vis[i]=0;
		for(int i=1;i<=n;i++) scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
		for(int i=1;i<=n;i++){
			 if(p[i].z<=r&&p[i].z>=-r) g[i][0]=g[0][i]=1;
			 if(h-p[i].z<=r&&h-p[i].z>=-r) g[i][n+1]=g[n+1][i]=1; 
			 for(int j=1;j<=n;j++) if(i!=j)
				if(dis(i,j)<=sr) g[i][j]=g[j][i]=1;
		}dfs(0);
		if(vis[n+1]) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAXN 1005

int t;
int n;
long long h, r;
int father[MAXN];
long long x[MAXN], y[MAXN], z[MAXN];

long long dist(int p1, int p2)
{
	return (long long)sqrt((x[p1] - x[p2]) * (x[p1] - x[p2]) + (y[p1] - y[p2]) * (y[p1] - y[p2]) + (z[p1] - z[p2]) * (z[p1] - z[p2]));
}

int find(int p)
{
	if (p == father[p])
	{
		return p;
	}
	else
	{
		return (father[p] = find(father[p]));
	}
}

void union1(int p1, int p2)
{
	int f1, f2;
	f1 = find(p1);
	f2 = find(p2);
	father[f1] = f2;
}

void add_point(int p)
{
	int i;

	/* point p + 1 */
	if (z[p] - r <= 0)
	{
		union1(0, p + 1);
	}
	if (z[p] + r >= h)
	{
		union1(1, p + 1);
	}
	
	for (i = 1; i < p; i++)
	{
		if (dist(i, p) <=  2 * r)
		{
			union1(i + 1, p + 1);
		}
	}
}

int main()
{
	int i, j;
	int ret;
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &t);
	
	for (i = 1; i <= t; i++)
	{
		memset(father, 0, sizeof(int) * MAXN);
		memset(x, 0, sizeof(long long) * MAXN);
		memset(y, 0, sizeof(long long) * MAXN);
		memset(z, 0, sizeof(long long) * MAXN);
		scanf("%d %lld %lld", &n, &h, &r);
		for (j = 0; j <= n + 1; j++)
		{
			father[j] = j;
		}
		
		for (j = 1; j <= n; j++)
		{
			scanf("%lld %lld %lld", &x[j], &y[j], &z[j]);
			add_point(j);
		}
		
		ret = (find(0) == find(1)) ? 1 : 0;
		if (ret == 1)
		{
			printf("Yes\n");
		}
		else if (ret == 0)
		{
			printf("No\n");
		}
	}
	
	return 0;
}

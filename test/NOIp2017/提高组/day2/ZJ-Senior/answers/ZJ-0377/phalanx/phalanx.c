#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int n, m, q;
int a[1005][1005];
int b[300005];

int main()
{
	int i, j;
	int x, y;
	int tmp;
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d %d %d", &n, &m, &q);
	
	if (n == 1)
	{
		for (i = 1; i <= m; i++)
		{
			b[i] = i;
		}
		for (i = 1; i <= q; i++)
		{
			scanf("%d %d", &x, &y);
			printf("%d", b[y]);
			tmp = b[y];
			for (j = y; j < m; j++)
			{
				b[j] = b[j + 1];
			}
			b[m] = tmp;
		}
		return 0;
	}
	if (n <= 1000 && m <= 1000)
	{
		for (i = 1; i <= n; i++)
		{
			for (j = 1; j <= m; j++)
			{
				a[i][j] = (i - 1) * m + j;
			}
		}
		for (i = 1; i <= q; i++)
		{
			scanf("%d %d", &x, &y);
			printf("%d\n", a[x][y]);
			tmp = a[x][y];
			for (j = y; j < m; j++)
			{
				a[x][j] = a[x][j + 1];
			}
			a[x][m] = tmp;
			for (j = x; j < n; j++)
			{
				a[j][m] = a[j + 1][m];
			}
			a[n][m] = tmp;
		}
		return 0;
	}
	return 0;
}

#include <stdio.h>
#include <string.h>

#define INF (1 << 29)
#define MAXN 15
#define MAXM 1005

int n, m;
int G[MAXN][MAXN];
struct edge
{
	int from, to, len;
}e[MAXM];
int number_of_edges = 0;

void up(int loc)
{
	struct edge tmp;
	
	while ((loc != 1) && (e[loc].len < e[loc / 2].len))
	{
		tmp = e[loc];
		e[loc] = e[loc / 2];
		e[loc / 2] = tmp;
		loc /= 2;
	}
}

void down(int loc)
{
	struct edge tmp;
	int min;
	while (loc * 2 <= number_of_edges)
	{
		if (loc * 2 + 1 <= number_of_edges)
		{
			if (e[loc * 2].len < e[loc * 2 + 1].len)
			{
				min = loc * 2;
			}
			else
			{
				min = loc * 2 + 1;
			}
		}
		else
		{
			min = loc * 2;
		}
		if (e[loc].len > e[min].len)
		{
			tmp = e[loc];
			e[loc] = e[min];
			e[min] = tmp;
		}
		loc = min;
	}
}

void push(struct edge *edge)
{
	e[++number_of_edges] = *edge;
	up(number_of_edges);
}

int pop(struct edge *ret)
{
	if (number_of_edges == 0)
	{
		return -1;
	}
	*ret = e[1];
	e[1] = e[number_of_edges--];
	down(1);
	return 0;
}

int find(int start)
{
	struct edge tmp;
	int vis[MAXN] = {0};
	int depth[MAXN] = {0};
	int cur = start;
	int min = INF;
	int i;
	int ret = 0;
	
	vis[cur] = 1;
	depth[cur] = 1;
	
	while (1)
	{
		for (i = 1; i <= n; i++)
		{
			if (!vis[i] && (G[cur][i] != INF))
			{
				tmp.from = cur;
				tmp.to = i;
				tmp.len = G[cur][i] * depth[cur];
				push(&tmp);
			}
		}
		while (1)
		{
			if (pop(&tmp) == -1)
			{
				/* no edges */
				return ret;
			}
			if (vis[tmp.to] == 1)
			{
				continue;
			}
			else
			{
				depth[tmp.to] = depth[tmp.from] + 1;
				ret += tmp.len;
				cur = tmp.to;
				vis[tmp.to] = 1;
				break;
			}
		}
	}
}

int main()
{
	int i, j;
	int p1, p2, v;
	int min = INF;
	int ret;
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d %d", &n, &m);
	
	for (i = 1; i <= n; i++)
	{
		for (j = 1; j <= n; j++)
		{
			if (i == j)
			{
				G[i][j] = 0;
				G[j][i] = 0;
			}
			else
			{
				G[i][j] = INF;
				G[j][i] = INF;
			}
		}
	}
	
	for (i = 1; i <= m; i++)
	{
		scanf("%d %d %d", &p1, &p2, &v);
		if (G[p1][p2] > v)
		{
			G[p1][p2] = v;
			G[p2][p1] = v;
		}
	}
	
	for (i = 1; i <= n; i++)
	{
		memset(e, 0, sizeof(struct edge) * MAXM);
		ret = find(i);
		if (ret < min)
		{
			min = ret;
		}
	}
	printf("%d", min);
	return 0;
}

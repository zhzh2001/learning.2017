#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
long long r,x[1005],y[1005],z[1005];
int n,h,T,f[1005],b1[1005],b2[1005];
int find(int x){
	if (x==f[x]) return f[x];
	f[x]=find(f[x]);
	return f[x];
}
void solve(){
	int tr=0;
	scanf("%d%d%lld",&n,&h,&r);
	for (int i=1; i<=n; i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]),f[i]=i,b1[i]=0,b2[i]=0;
	for (int i=1; i<n; i++) for (int j=i+1; j<=n; j++) if (i!=j){
		long long t=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
		if (t<=r*r*4){
			int u=find(i),v=find(j);
			if (u!=v) f[v]=u;
		}
	}
	for (int i=1; i<=n; i++){
		if (z[i]<=r) b1[i]=1;
		if (z[i]+r>=h) b2[i]=1;
	}
	for (int i=1; i<=n; i++){
		int t=f[i],bo1=0,bo2=0;
		for (int j=i; j<=n; j++) if (f[j]==f[i]){
			if (b1[j]) bo1=1;
			if (b2[j]) bo2=1;
		}
		if (bo1==1&&bo2==1){
			tr=1;break;
		}
	}
	if (tr==0) printf("No\n");else printf("Yes\n");
}
int main(){
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--) solve();
	fclose(stdin);fclose(stdout);
}

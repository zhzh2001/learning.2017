#include<bits/stdc++.h>
using namespace std;
int n,m,f[20][20],dep[20][20],minn;
bool bx[20];
void dfs(int num,int s)
{
	if (s>=minn)
		return;
	if (num==n)
	{
		minn=s;
		return;
	}
	for (int i=0;i<=12;i++)
		for (int q=1;q<=dep[i][0];q++)
			for (int j=1;j<=n;j++)
				if (bx[j]&&f[dep[i][q]][j]<2000000000)
				{
					bx[j]=0;
					dep[i+1][0]++;
					dep[i+1][dep[i+1][0]]=j;
					dfs(num+1,s+(i+1)*f[dep[i][q]][j]);
					bx[j]=1;
					dep[i+1][0]--;
				}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(f,125,sizeof(f));
	scanf("%d%d",&n,&m);
	int x,y,z;
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		if (z<f[x][y])
			f[x][y]=z;
		if (z<f[y][x])
			f[y][x]=z;
	}
	minn=2000000000;
	for (int i=1;i<=n;i++)
	{
		memset(bx,1,sizeof(bx));
		memset(dep,0,sizeof(dep));
		dep[0][0]=1;
		dep[0][1]=i;
		bx[i]=0;
		dfs(1,0);
	}
	if (minn==2000000000)
		printf("0");
	else
		printf("%d",minn);
	return 0;
}

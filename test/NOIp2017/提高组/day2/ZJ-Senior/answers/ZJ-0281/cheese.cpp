#include<bits/stdc++.h>
using namespace std;
int T,n,h,r,x[1005],y[1005],z[1005],f[1010];
bool b[1005][1005],bx[1005];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%d%d",&n,&h,&r);
		for (int i=0;i<=n+1;i++)
			for (int j=0;j<=n+1;j++)
				b[i][j]=0;
		for (int i=1;i<=n;i++)
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		double x1,x2,x3,x4=r;
		x4=x4*x4;
		x4=x4*4;
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			{
				x1=x[i]-x[j];
				x2=y[i]-y[j];
				x3=z[i]-z[j];
				x1=x1*x1+x2*x2+x3*x3;
				if (x1<=x4)
					b[i][j]=1,b[j][i]=1;
			}
		for (int i=1;i<=n;i++)
		{
			if (z[i]<=r)
				b[i][0]=1,b[0][i]=1;
			if (z[i]+r>=h)
				b[i][n+1]=1,b[n+1][i]=1;
		}
		int l=1,r=1;
		f[1]=0;
		memset(bx,1,sizeof(bx));
		bx[0]=0;
		while (l<=r)
		{
			for (int i=1;i<=n+1;i++)
				if (b[f[l]][i]&&bx[i])
				{
					bx[i]=0;
					r++;
					f[r]=i;
				}
			l++;
		}
		if (!bx[n+1])
			printf("Yes\n");
		else
			printf("No\n"); 
	}
	return 0;
} 

#include<bits/stdc++.h>
using namespace std;
const int N = 1100;
const double eps = -(1E-9);
int n, h, r;
int x[N], y[N], z[N];
int d[N], vis[N];
vector<int> edge[N];
double sqr(int a) {
	return 1.0 * a * a;
}
double dist(int a, int b) {
	return sqrt(sqr(x[a] - x[b]) + sqr(y[a] - y[b]) + sqr(z[a] - z[b]));
}
void spfa() {
	memset(d, 0x3f, sizeof(d));
	memset(vis, 0, sizeof(vis));
	queue<int> Q;
	Q.push(0); d[0] = 0;
	while (!Q.empty()) {
		int u = Q.front(); Q.pop(); vis[u] = 0;
		for (int i = 0; i < edge[u].size(); i ++) {
			int v = edge[u][i];
			if (d[u] + 1 < d[v]) {
				d[v] = d[u] + 1;
				if (!vis[v]) {
					vis[v] = 1; Q.push(v);
				}
			}
		}
	}
	return;
}
int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T; scanf("%d", &T);
	while (T --) {
		scanf("%d %d %d", &n, &h, &r);
		for (int i = 1; i <= n; i ++) scanf("%d %d %d", &x[i], &y[i], &z[i]);
		for (int i = 0; i <= n + 1; i ++) vector<int>().swap(edge[i]);
		for (int i = 1; i <= n; i ++) {
			if (z[i] <= r) {
				edge[0].push_back(i);
				edge[i].push_back(0);
			}
			if (h - z[i] <= r) {
				edge[n + 1].push_back(i);
				edge[i].push_back(n + 1);
			}
		}
		for (int i = 1; i <= n; i ++)
			for (int j = i + 1; j <= n; j ++)
				if (2.0 * r - dist(i, j) >= eps) {
				edge[j].push_back(i);
				edge[i].push_back(j);
			}
		spfa();
		if (d[n + 1] != d[n + 2]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int n, m, dis[15][15];
int DIS[15];
int ans;
int a[15], vis[15], can[15];
void check() {
	int x = a[1];
	can[x] = 0;
	for (int i = 1; i <= n; i ++) if (dis[x][i] != dis[n + 1][n + 1]) can[i] = 1;
	for (int j = 2; j <= n; j ++) {
		if (!can[a[j]]) return;
		for (int i = 1; i <= n; i ++) if (dis[a[j]][i] != dis[n + 1][n + 1]) can[i] = 1;
	}
	int ret = 0; DIS[x] = 1;
	for (int i = 2; i <= n; i ++) {
		int add = 2147483647;
		for (int j = 1; j < i; j ++) {
			if (add > dis[a[j]][a[i]] * DIS[a[j]]) {
				add = dis[a[j]][a[i]] * DIS[a[j]];
				DIS[a[i]] = DIS[a[j]] + 1;
			}
		}
		ret += add;
	}
	ans = min(ans, ret);
}
void dfs(int k) {
	if (k > n) {
		check(); return ;
	}
	for (int i = 1; i <= n; i ++) {
		if (!vis[i]) {
			a[k] = i; vis[i] = 1;
			dfs(k + 1);
			a[k] = 0; vis[i] = 0;
		}
	}
}
void work(int x) {
	memset(a, 0, sizeof(a));
	a[1] = x; vis[x] = 1;
	dfs(2);
}
void WORK(int x) {
	memset(a, 0, sizeof(a));
	int ret = 0;
	queue<int> Q; Q.push(x); a[x] = 1;
	int DIS[15]; 
	memset(DIS, 0x3f, sizeof(DIS)); DIS[x] = 0;
	while (!Q.empty()) {
		int u = Q.front(); Q.pop();
		for (int i = 1; i <= n; i ++) 
			if (!a[i] && dis[u][i] != dis[0][0]) {
				DIS[i] = DIS[u] + 1;
				a[i] = 1; Q.push(i);
			}
	}
	for (int i = 1; i <= n; i ++) ret += DIS[i];
	ans = min(ans, ret);
}
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int tmp, flag = 1;
	scanf("%d %d", &n, &m);
	memset(dis, 0x3f, sizeof(dis)); ans = dis[0][0];
	for (int i = 1; i <= n; i ++) dis[i][i] = 0;
	for (int i = 1; i <= m; i ++) {
		int u, v, l;
		scanf("%d %d %d", &u, &v, &l);
		if (i == 1) tmp = l;
		else if (tmp != l) flag = 0;
		dis[u][v] = dis[v][u] = min(l, dis[u][v]);
	}
	if (flag) {
		int v = tmp;
		for (int i = 1; i <= n; i ++) WORK(i);
		printf("%d\n", ans * v);
		return 0;
	}
	for (int i = 1; i <= n; i ++) work(i);
	printf("%d\n", ans);
}

#include<bits/stdc++.h>
using namespace std;
int n, m, q;
int mat[1100][1100];
int pp = 0;
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d %d %d", &n, &m, &q);
	pp = 0;
	for (int i = 1; i <= n; i ++)
		for (int j = 1; j <= m; j ++)
			mat[i][j] = ++ pp;
	for (int i = 1; i <= q; i ++) {
		int x, y;
		scanf("%d %d", &x, &y);
		printf("%d\n", mat[x][y]);
		int tmp = mat[x][y];
		for (int i = y; i < m; i ++) mat[x][i] = mat[x][i + 1];
		for (int i = x; i < n; i ++) mat[i][m] = mat[i + 1][m];
		mat[n][m] = tmp;
	}
	return 0;
}

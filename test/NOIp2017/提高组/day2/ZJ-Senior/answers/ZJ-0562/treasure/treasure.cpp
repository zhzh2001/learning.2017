#include<stdio.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<map>
#include<set>
#include<bitset>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	x=0;char c,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}	

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(y<x)x=y;}

const int M=13,INF=1e9;

struct E{int t,nxt;}G[105];

int h[M],E_tot,n,m,w[M][M];

void E_Add(int x,int y){
	G[++E_tot]=(E){y,h[x]};h[x]=E_tot;
	G[++E_tot]=(E){x,h[y]};h[y]=E_tot;
}

struct P1{
	int rt,fa[M],Ans,res;
	bool mark[M];
	void solve(int x,int f,int d){
		mark[x]=1;
		for(int i=h[x];i;i=G[i].nxt){
			int y=G[i].t;
			if(w[x][y]==INF)continue;
			if(y==f)continue;
			int c=w[x][y];
			res+=c*d;
			solve(y,x,d+1);
		}
	}
	void dfs(int x){
		if(x>n){
			
			E_tot=0;
			For(i,1,n)mark[i]=0,h[i]=0;
			For(i,1,n)if(i!=rt)E_Add(i,fa[i]);
			res=0;
			solve(rt,0,1);
		
			For(i,1,n)if(!mark[i])return;
			
			Min(Ans,res);
			return;
		}
		
		if(x==rt)dfs(x+1);
		
		else {
			For(i,1,n)if(i!=x&&fa[i]!=x&&w[i][x]!=INF){
				fa[x]=i;
				dfs(x+1);
				fa[x]=0;
			}
		}
		
	}
	void work(){
		Ans=INF;
		For(i,1,n){
			For(j,1,n)fa[j]=0;
			rt=i;
			dfs(1);
		}
		ptn(Ans);
	}
}P1;

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	rd(n);rd(m);
	For(i,1,n)For(j,1,n){
		if(i==j)w[i][j]=0;
		else w[i][j]=INF;
	}
	For(i,1,m){
		int a,b,c;
		rd(a);rd(b),rd(c);
		Min(w[a][b],c);
		Min(w[b][a],c);
	}
	P1.work();
	return 0;
}

#include<stdio.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<map>
#include<set>
#include<bitset>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	x=0;char c,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}	

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(y<x)x=y;}

const int M=1025;

bool arr[M][M];

bool mark[M];

struct node{ll x,y,z;}A[M];

ll R,h;

int n,cas;

int Q[M];

bool check(int x,int y){
	node a=A[x],b=A[y];
	ll dis=(a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z);
	ll hav=(R*2)*(R*2);
	return dis<=hav;
}

int main(){
	
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	rd(cas);
	while(cas--){
		rd(n),rd(h),rd(R);
		
		memset(arr,0,sizeof(arr));
		memset(mark,0,sizeof(mark));
		
		For(i,1,n){
			rd(A[i].x);
			rd(A[i].y);
			rd(A[i].z);
		}
		
		For(a,1,n)For(b,a+1,n)if(check(a,b))arr[a][b]=arr[b][a]=1;
		
		int s=n+1,t=n+2;
		
		For(i,1,n)if(A[i].z-R<=0)arr[s][i]=arr[i][s]=1;
		
		For(i,1,n)if(A[i].z+R>=h)arr[t][i]=arr[i][t]=1;
		
		int l=0,r=0;
		
		Q[r++]=s;mark[s]=1;
		
		while(l<r){
			int x=Q[l++];
			For(i,1,n+2)if(arr[x][i]&&!mark[i])mark[i]=1,Q[r++]=i;
		}
		if(mark[t])puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<stdio.h>
#include<string.h>
#include<time.h>
#include<ctype.h>
#include<math.h>
#include<algorithm>
#include<iostream>
#include<vector>
#include<queue>
#include<stack>
#include<map>
#include<set>
#include<bitset>
#include<string>

using namespace std;

#define bug(x) cerr<<#x<<'='<<x<<' '
#define debug(x) cerr<<#x<<'='<<x<<'\n'
#define For(i,a,b) for(int i=a;i<=b;++i)
#define Ror(i,a,b) for(int i=b;a<=i;--i)

typedef long long ll;

template<class T>void rd(T&x){
	x=0;char c,f=1;
	while(c=getchar(),!isdigit(c))if(c=='-')f=-1;
	do x=(x<<3)+(x<<1)+(c^'0');
	while(c=getchar(),isdigit(c));
	x*=f;
}	

template<class T>void pf(T x){
	static int top=0,stk[100];
	if(!x)putchar('0');
	if(x<0)putchar('-'),x=-x;
	while(x)stk[++top]=x%10,x/=10;
	while(top)putchar(stk[top--]+'0');
}
template<class T>void pt(T x){pf(x);putchar(' ');}
template<class T>void ptn(T x){pf(x);putchar('\n');}
template<class T>void Max(T&x,T y){if(x<y)x=y;}
template<class T>void Min(T&x,T y){if(y<x)x=y;}

const int M=3e5+5;

int n,m,q;

struct P1{
	int mp[1005][1005];
	void work(){
		For(i,1,n)For(j,1,m)mp[i][j]=m*(i-1)+j;
		while(q--){
			int x,y;
			rd(x),rd(y);
			int t=mp[x][y];
			ptn(t);
			For(j,y,m-1)mp[x][j]=mp[x][j+1];
			For(i,x,n-1)mp[i][m]=mp[i+1][m];
			mp[n][m]=t;
		}
	}		
}P1;

struct P2{
	static const int M=5e4+5;
	int cnt[M],Ex[M][1005],Del[M][1005],A[M];
	int query(int x,int y){
		int hav=0,rs;
		For(i,1,cnt[x])if(Del[x][i]<=y)hav++;
		y+=hav;
		int f;		
		if(y<=m-1)rs=(x-1)*m+y,f=1;
		else if(y-(m-1)<=cnt[x])rs=Ex[x][y-(m-1)],f=2;
		else rs=A[x],f=3;	
		if(f==1){
			++cnt[x];
			Ex[x][cnt[x]]=A[x];
			Del[x][cnt[x]]=y-hav;
			For(i,x,n-1)A[i]=A[i+1];
			A[n]=rs;
		}
		else if(f==3){
			For(i,x,n-1)A[i]=A[i+1];
			A[n]=rs;
		}
		else if(f==2){
			int d=y-(m-1);
			For(i,d,cnt[x]-1)Ex[x][i]=Ex[x][i+1];
			Ex[x][cnt[x]]=A[x];
			For(i,x,n-1)A[i]=A[i+1];
			A[n]=rs;
		}
		return rs;
	}
	void work(){
		For(i,1,n)cnt[i]=0;
		For(i,1,n)A[i]=i*m;
		while(q--){
			int x,y;
			rd(x),rd(y);
			int t=query(x,y);
			ptn(t);
		}
	}
}P2;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	rd(n),rd(m),rd(q);
	if(q<=500&&n<=1000&&m<=1000)P1.work();	
	else P2.work();
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
long long t,n,tot,h,r;
int mo = 1000000007;
struct edgs{
	long long x,y,z;
}map[1050];
struct ss{
	long long hx,hy,hz;
} hh[1010][1010];
int cnt[1010];
void read(long long &x)
{
	x = 0;
	char ch = getchar();
	long long k = 1;
	while(ch < '0' || ch > '9') {if(ch == '-') k = -1; ch=getchar();}
	while(ch >= '0' and ch <= '9') {x = x* 10+ch-'0';ch = getchar();}
	x = x*k;
}
void sort(long long l,long long r)
{
	long long i,j,x,y;
    i=l;
    j=r;
    x=map[(l+r)/2].z;
    while (i <= j)
     {
     	while (map[i].z<x) 
            ++i;
        while (x<map[j].z)
            --j;
           if (i<=j) 
           {
           	y=map[i].z;
            map[i].z=map[j].z;
            map[j].z=y;
            y=map[i].y;
            map[i].y=map[j].y;
            map[j].y=y;
            y=map[i].x;
            map[i].x=map[j].x;
            map[j].x=y;
            ++i;
            --j;
		   }
	 }
    if (l<j) 
    sort(l,j);
    if (i<r)
    sort(i,r);
}
inline void swap(int m,int a,int b)
{
	long long o;
	o=hh[m][a].hx,hh[m][a].hx = hh[m][b].hx,hh[m][b].hx = o;
	o=hh[m][a].hy,hh[m][a].hy = hh[m][b].hy,hh[m][b].hy = o;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(t);
	while(t --)
	 {
	 	read(n),read(h),read(r);
	 	for (int i = 1;i <= n;++i)
	 	 read(map[i].x),read(map[i].y),read(map[i].z);
	 	sort(1,n);
	 	if (map[1].z - r > 0) {printf("No\n"); continue;}
	 	if (map[n].z + r < h) {printf("No\n"); continue;}
	 	long long p = map[1].z,b=0,q=1,tot=1;
	 	memset(cnt,0,sizeof(cnt));
	 	++cnt[tot];
	 	hh[tot][cnt[tot]].hx = map[1].x,hh[tot][cnt[tot]].hy = map[1].y;
	 	hh[tot][0].hz=map[1].z;
	 	for (int i = 2;i <= n;++i)
	 	 {
	 	 	if (p != map[i].z) 
	 	 	 {
	 	 	 	if (map[i].z - p > 2*r)
	 	 	 	 b = 1;
	 	 	 	q = i,p = map[i].z;
				 tot+=1;
				 cnt[tot]+=1; 
				 hh[tot][cnt[tot]].hx = map[i].x;
				 hh[tot][cnt[tot]].hy= map[i].y;	 
				 hh[tot][0].hz=map[i].z;
			   }
			else
			 {
			 	cnt[tot]+=1;
			 	hh[tot][cnt[tot]].hx = map[i].x;
				hh[tot][cnt[tot]].hy = map[i].y;
			 }	 	 
		 }
	    if (b) {printf("No\n"); continue;}    
	    int num = 1,bb=1,num2=1;
	    while(hh[num2][0].hz - r <= 0 and num2 <= n)
	     {
	     	for (int i = num2+1;i <= tot;++i)
	     {
	     	for (int j = cnt[i];j >= 1;j --) 
	     	 {
	     	 	b=0;
	     	 	for (int k = cnt[num];k >= 1;k --)
	     	 	 {
	     	 	 	if (((hh[i][j].hx - hh[num][k].hx))*((hh[i][j].hx - hh[num][k].hx))+((hh[i][j].hy - hh[num][k].hy))*((hh[i][j].hy - hh[num][k].hy))+((hh[i][0].hz - hh[num][0].hz))*((hh[i][0].hz - hh[num][0].hz)) <= (r*r)*4)
	     	 	 	 {
	     	 	 	 	b=1;
	     	 	 	 	num = i;
	     	 	 	 	break;
					 }
				 }
				if(b)
				 break;
			  }		
		 }
		 if (num== tot) {printf("Yes\n"); bb = 0;break;}
			num2++;
			num = num2;	     	 
		 }
	   if (bb) printf("No\n");
	 }
}

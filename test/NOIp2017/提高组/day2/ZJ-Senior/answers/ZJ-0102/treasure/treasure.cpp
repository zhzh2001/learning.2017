#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
struct edgs{
	int to,next,w;
}e[1050];
int head[1050],used[1050],vis[1050],lock[1050],tot[1050];
int n,m,u,v,wi,cnt;
int minn=500001;
void build(int x,int y,int q)
{
	e[cnt].next=head[x];
	head[x]=cnt;
	e[cnt].to = y;
	e[cnt].w = q;
	cnt ++;
}
void read(int &x)
{
	x = 0;
	char ch = getchar();
	int k = 1;
	while(ch < '0' || ch > '9') {if(ch == '-') k = -1; ch=getchar();}
	while(ch >= '0' and ch <= '9') {x = x* 10+ch-'0';ch = getchar();}
	x = x*k;
}
void dfs(int x,int num)
{
	int mi = 500001;
	int nmi = 0;
	for(int j = head[x];j!=0;j =e[j].next) 
	 {
	 	int v = e[j].to;
		  if (lock[v] == 0)
		  if (e[v].w*(tot[x]+1)< vis[v]*tot[v])
		   {
		   	vis[v] = (e[v].w)*(tot[x]+1);
		   	tot[v] = tot[x] + 1;
		   }		   
	 }
	for(int j = head[num];j!=0;j =e[j].next) 
	 {
	 	int v = e[j].to;
	 	if (vis[v] < mi and lock[v] == 0) {
	 		mi = vis[v];
	 		nmi=v;
		  }
	 }	
	 lock[nmi]=1;
	if(nmi != 0) dfs(nmi,num);	 
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n),read(m);
	cnt = 1;
	for(int i = 1;i <= m;++i)
	 {
	 	read(u),read(v),read(wi);
	 	build(u,v,wi);
	 	build(v,u,wi);
	 }
	 for (int i = 1;i <= n;++i)
	  {
	  	for (int j = 1;j <= n;++j)
	  	 {
	  	 	vis[j] = 500001;
	  	 	lock[j] = 0;
	  	 	tot[j] = 1;
		   }
	  	tot[1] = 0;
	  	lock[1] = 1;
	  	for(int j = head[i];j!=0;j =e[j].next) 
	 {
	    vis[e[j].to] = e[j].w;
	 }
	  	dfs(i,i);
	  	int sum=0;
	  	for(int j = 2;j <= n;j ++)
	  	 {
	  	 	sum += vis[j];
		   }
	  	if (sum < minn) minn = sum;
	  }
	printf("%d",minn);
	return 0;
}

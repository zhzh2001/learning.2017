#include<bits/stdc++.h>
using namespace std;

int t,n,h,r,x[1001],y[1001],z[1001],flag;
bool bian[1001][1001],yi[1001];

bool isbian(int x1,int y1,int z1,int x2,int y2,int z2,int r){
	if ((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2)<=4*r*r) return true;
	else return false;
}

void dfs(int i){
	for (int j=1;j<=n;j++) {
		if (flag==1) break;
		if (isbian(x[i],y[i],z[i],x[j],y[j],z[j],r)&&!bian[i][j]) {	
			bian[i][j]=true;
			bian[j][i]=true;
			if (h-z[j]<=r) {
				flag=1;
				break;
			}   
			//cout<<i<<' '<<x[i]<<' '<<y[i]<<' '<<z[i]<<' '<<j<<' '<<x[j]<<' '<<y[j]<<' '<<z[j]<<endl;                                     //the end
			if (!yi[j]) {
				dfs(j);
				yi[j]=true;
			}
		}
	}
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>t;
	for(int l=1;l<=t;l++){
		memset(yi,false,sizeof(y));
		memset(bian,false,sizeof(y));
		for (int i=1;i<=1000;i++) bian[i][i]=true;
		flag=0;
		cin>>n>>h>>r;
		//cout<<n<<' '<<h<<' '<<r<<endl;
		if (n==1) {
			cin>>x[1]>>y[1]>>z[1];
			if ((h-z[1]<=r)&&(z[1]<=r)) {
				cout<<"Yes"<<endl;
				continue;
			}
			else {
				cout<<"No"<<endl;
				continue;
			}
		}
		else {
			for (int i=1;i<=n;i++) cin>>x[i]>>y[i]>>z[i];
			for (int k=1;k<=n;k++) {
				if (z[k]<=r) dfs(k);
				if (flag==1){
					cout<<"Yes"<<endl;
					break;
				}
			}
			if (flag!=1) cout<<"No"<<endl;
		}
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

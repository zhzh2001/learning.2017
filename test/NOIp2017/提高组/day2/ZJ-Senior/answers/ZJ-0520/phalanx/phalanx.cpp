#include<cstdio>
#include<cctype>
#include<vector>
#include<algorithm>
#define fr first
#define sc second
using namespace std;
typedef long long LL;
const int maxn=300000,maxq=300000;

int n,m,q;
pair<int,int> a[maxq+5];
vector<int> b[maxn+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc()
{
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF;return *l++;
}
inline int readi(int &x)
{
	int tot=0,f=1;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	if (lst=='-') f=-f;
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+ch-48,ch=readc();
	return x=tot*f,Eoln(ch);
}
inline void writeL(LL x)
{
	static int buf[20],len=0;do buf[len++]=x%10,x/=10; while (x);
	while (len) putchar(buf[--len]+48);putchar('\n');
}
inline int Find(vector<int> &now,int x)
{
	int L=0,R=now.size()-1;
	for (int mid=L+(R-L>>1);L<=R;mid=L+(R-L>>1))
		if (now[mid]<=x) L=mid+1; else R=mid-1;
	return R;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	readi(n);readi(m);readi(q);
	for (int i=1;i<=q;i++) readi(a[i].fr),readi(a[i].sc);
	for (int i=1;i<=q;i++)
	{
		int x=a[i].fr,y=a[i].sc;
		for (int j=i-1;j>=1;j--)
			if (x==n&&y==m) x=a[j].fr,y=a[j].sc; else
			if (y<m)
			{
				vector<int> &now=b[x];
				for (int k=Find(now,j);k>=0&&y<m;k--)
					if (a[now[k]].sc<=y) y++,j=now[k];
				if (y<m) break;
			} else
			if (a[j].fr<=x) x++;
		b[a[i].fr].push_back(i);
		writeL((LL)(x-1)*m+y);
	}
	return 0;
}

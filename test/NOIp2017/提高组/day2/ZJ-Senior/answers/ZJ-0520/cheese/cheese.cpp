#include<cstdio>
#include<cctype>
using namespace std;
typedef long long LL;
const int maxn=1000;

int te,n,H,r,x[maxn+5],y[maxn+5],z[maxn+5];
LL R;int fa[maxn+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc()
{
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF;return *l++;
}
inline int readi(int &x)
{
	int tot=0,f=1;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	if (lst=='-') f=-f;
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+ch-48,ch=readc();
	return x=tot*f,Eoln(ch);
}
int getfa(int x) {if (fa[x]==x) return x;return fa[x]=getfa(fa[x]);}
inline void Merge(int x,int y)
{
	static int fx,fy;fx=getfa(x);fy=getfa(y);
	if (fx!=fy) fa[fx]=fy;
}
inline int Abs(int x) {if (x<0) return -x;return x;}
inline LL sqr(LL x) {return x*x;}
inline bool check(int i,int j)
{
	static LL dis;dis=0;
	dis+=sqr(x[i]-x[j]);if (dis>R) return false;
	dis+=sqr(y[i]-y[j]);if (dis>R) return false;
	dis+=sqr(z[i]-z[j]);if (dis>R) return false;
	return true;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (readi(te);te;te--)
	{
		readi(n);readi(H);readi(r);R=(LL)r*r*4;fa[0]=0;fa[n+1]=n+1;
		for (int i=1;i<=n;i++) readi(x[i]),readi(y[i]),readi(z[i]),fa[i]=i;
		for (int i=1;i<=n;i++)
		{
			if (Abs(z[i]-0)<=r) Merge(0,i);
			if (Abs(H-z[i])<=r) Merge(i,n+1);
		}
		bool fl=false;
		for (int i=1;!fl&&i<=n-1;i++)
		for (int j=i+1;!fl&&j<=n;j++)
		{
			if (check(i,j)) Merge(i,j);
			if (getfa(0)==getfa(n+1)) {fl=true;break;}
		}
		if (fl) puts("Yes"); else puts("No");
	}
	return 0;
}

#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=12;

int n,m,cst[maxn+5][maxn+5],INF,ans;
int l[maxn+5],r[maxn+5],now[maxn+5],dep[maxn+5];

#define Eoln(x) ((x)==10||(x)==13||(x)==EOF)
inline char readc()
{
	static char buf[100000],*l=buf,*r=buf;
	if (l==r) r=(l=buf)+fread(buf,1,100000,stdin);
	if (l==r) return EOF;return *l++;
}
inline int readi(int &x)
{
	int tot=0,f=1;char ch=readc(),lst='+';
	while (!isdigit(ch)) {if (ch==EOF) return EOF;lst=ch;ch=readc();}
	if (lst=='-') f=-f;
	while (isdigit(ch)) tot=(tot<<3)+(tot<<1)+ch-48,ch=readc();
	return x=tot*f,Eoln(ch);
}
void Dfs(int tot)
{
	if (tot>=ans) return;if (r[0]>n) {ans=tot;return;}
	for (int i=r[0];i<=n;i=r[i])
	for (int j=1,u=now[j];j<=now[0];u=now[++j]) if (cst[i][u]<INF)
	{
		r[l[i]]=r[i];l[r[i]]=l[i];now[++now[0]]=i;dep[i]=dep[u]+1;
		Dfs(tot+cst[i][u]*dep[i]);
		r[l[i]]=i;l[r[i]]=i;now[0]--;
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	readi(n);readi(m);memset(cst,63,sizeof(cst));INF=cst[0][0];
	for (int i=1,x,y,z;i<=m;i++)
	{
		readi(x);readi(y);readi(z);
		cst[x][y]=min(cst[x][y],z);cst[y][x]=cst[x][y];
	}
	for (int i=0;i<=n;i++) l[i]=i-1,r[i]=i+1;now[0]=1;ans=INF;
	for (int i=1;i<=n;i++) l[i+1]=l[i],r[i-1]=r[i],now[1]=i,dep[i]=0,Dfs(0),l[i+1]=i,r[i-1]=i;
	return printf("%d\n",ans),0;
}

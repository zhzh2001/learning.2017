#include<cstdio>
#include<cstring>
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
typedef long long LL;
typedef unsigned long long uLL;
const int mn=20;
int n,m,h,i,j,x,y,z,k,l,s,t,I,mi,ans,a[mn][mn],f[mn][mn],g[mn][mn][mn],b[mn][mn],c[mn];
inline int read()
{
	char ch=getchar();int x=0,f=1;
	for(;ch!='-'&&(ch<'0'||ch>'9');) ch=getchar();
	if(ch=='-') f=-1,ch=getchar();
	for(;ch>='0'&&ch<='9';) x=x*10+ch-48,ch=getchar();
	return x*f;
}
inline int min(int x,int y) {return x<y?x:y;}
void go(int x,int y,int z)
{
	if(z>=ans) return;
	if(y==0)
	{
		if(s==n-1) {ans=min(ans,z);return;}
		int i,j;
		fo(i,1,n) if(c[i]!=I&&a[x][i]>0)
			fd(j,n-s-1,1) if(f[i][j]>0)
				s+=j,go(i,j,z),s-=j;
		return;
	}
	c[x]=I;
	go(g[x][y][1],y-1,z+f[x][y]);
	c[x]=0;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	fo(i,1,m)
	{
		x=read();y=read();z=read();
		if(x!=y&&(a[x][y]==0||a[x][y]>z)) a[x][y]=a[y][x]=z;
	}
	t=n*n;ans=6e6;
	fo(I,1,n)
	{
		memset(f,255,sizeof(f));
		memset(b,0,sizeof(b));
		f[I][0]=0;
		fo(k,1,t)
		{
			mi=n+1;
			fo(j,1,n) fo(l,0,n-2) if(f[j][l]>-1&&b[j][l]==0)
			{
				if(mi>l) mi=l,h=j;
				break;
			}
			if(mi>n) break;
			b[h][mi]=1;
			fo(j,1,n) if(j!=I&&a[h][j]>0&&b[j][mi+1]==0)
			{
				if(f[j][mi+1]<0||f[j][mi+1]>a[h][j]*mi)
				{
					f[j][mi+1]=a[h][j]*(mi+1);
					g[j][mi+1][g[j][mi+1][0]=1]=h;
				}
				else if(f[j][mi+1]==a[h][j]*mi)
					g[j][mi+1][++g[j][mi+1][0]]=h;
			}
		}
		j=I>1?1:2;
		fo(k,1,n) if(f[j][k]>0)
			s=k,go(j,k,0);
	}
	printf("%d\n",ans);
	return 0;
}

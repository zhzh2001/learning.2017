#include<cstdio>
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
typedef long long LL;
const int mn=1e3+5;
int n,m,i,j,x,y,q,t,a[mn][mn];
inline int read()
{
	char ch=getchar();int x=0,f=1;
	for(;ch!='-'&&(ch<'0'||ch>'9');) ch=getchar();
	if(ch=='-') f=-1,ch=getchar();
	for(;ch>='0'&&ch<='9';) x=x*10+ch-48,ch=getchar();
	return x*f;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();m=read();q=read();
	fo(i,1,n) fo(j,1,m) a[i][j]=(i-1)*m+j;
	for(;q--;)
	{
		x=read();y=read();
		printf("%d\n",t=a[x][y]);
		fo(j,y,m-1) a[x][j]=a[x][j+1];
		fo(i,x,n-1) a[i][m]=a[i+1][m];
		a[n][m]=t;
	}
	return 0;
}

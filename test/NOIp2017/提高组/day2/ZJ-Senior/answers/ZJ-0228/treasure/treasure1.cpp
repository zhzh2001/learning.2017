#include<cstdio>
#include<cstring>
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
typedef long long LL;
typedef unsigned long long uLL;
const int mn=20;
int n,m,h,i,j,x,y,z,k,l,s,t,I,mi,ans,a[mn][mn],f[mn][mn],g[mn][mn][mn],b[mn][mn],c[mn];
inline int read()
{
	char ch=getchar();int x=0,f=1;
	for(;ch!='-'&&(ch<'0'||ch>'9');) ch=getchar();
	if(ch=='-') f=-1,ch=getchar();
	for(;ch>='0'&&ch<='9';) x=x*10+ch-48,ch=getchar();
	return x*f;
}
inline int min(int x,int y) {return x<y?x:y;}
void go(int x,int y,int z,int t)
{
	if(z>=ans) return;
	if(y==0)
	{
		if(s==n-1) {ans=min(ans,z);return;}
		int i,j;
		fo(i,1,n) if(c[i]!=I&&a[x][i]>0)
			fd(j,n-s-1,1)
				s+=j,go(i,j,z,1),s-=j;
		return;
	}
	c[x]=I;
	int i;
	fo(i,1,n) if(a[i][x]) go(i,y-1,z+t,t+1);
	c[x]=0;
}
int main()
{
	freopen("treasure.in","r",stdin);
//	freopen("treasure.out","w",stdout);
	n=read();m=read();
	fo(i,1,m)
	{
		x=read();y=read();z=read();
		if(x!=y&&(a[x][y]==0||a[x][y]>z)) a[x][y]=a[y][x]=z;
	}
	t=n*n;ans=6e6;
	fo(I,1,n)
	{
		j=I>1?1:2;
		go(j,n-1,0,1);
	}
	printf("%d\n",ans);
	return 0;
}

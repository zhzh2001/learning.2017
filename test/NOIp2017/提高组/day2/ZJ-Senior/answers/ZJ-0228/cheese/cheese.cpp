#include<cstdio>
#include<cstring>
#define fo(i,a,b) for(i=a;i<=b;i++)
#define fd(i,a,b) for(i=a;i>=b;i--)
typedef long long LL;
typedef unsigned long long uLL;
const int mn=1e3+5,mm=1e3+5;
int n,H,r,i,j,I,t,w,h[mn],b[mn];
int ed,e[mm],nxt[mm],he[mn];
inline void add(int x,int y)
{
	e[++ed]=y;nxt[ed]=he[x];he[x]=ed;
}
inline int abs(int x) {return x<0?-x:x;}
inline uLL sqr(LL x) {return (uLL)(x*x);}
struct node
{
	int x,y,z;
	uLL operator -(const node &b) const
	{
		return sqr(x-b.x)+sqr(y-b.y)+sqr(z-b.z);
		
	}
}a[mn];
inline int read()
{
	char ch=getchar();int x=0,f=1;
	for(;ch!='-'&&(ch<'0'||ch>'9');) ch=getchar();
	if(ch=='-') f=-1,ch=getchar();
	for(;ch>='0'&&ch<='9';) x=x*10+ch-48,ch=getchar();
	return x*f;
}
bool go(int x)
{
	if(abs(H-a[x].z)<=r) {printf("Yes\n");return 1;}
	b[x]=I;
	h[1]=x;
	for(t=w=1;t<=w;t++)
	{
		x=h[t];
		for(i=he[x];i;i=nxt[i])
			if(b[e[i]]!=I)
			{
				h[++w]=e[i];
				b[e[i]]=I;
				if(abs(H-a[e[i]].z)<=r)
				{
					printf("Yes\n");
					return 1;
				}
			}
	}
	return 0;
}
void Work()
{
	n=read();H=read();r=read();uLL R2=(uLL)r*r*4;
	ed=0;
	memset(he,0,sizeof(he));
	fo(i,1,n)
	{
		a[i]=(node){read(),read(),read()};
		fo(j,1,i-1)
			if(a[i]-a[j]<=R2)
				add(i,j),add(j,i);
		b[i]=0;
	}
	fo(I,1,n)
		if(abs(a[I].z)<=r)
			if(go(I))
				return ;
	printf("No\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(int T=read();T--;)
	Work();
	return 0;
}

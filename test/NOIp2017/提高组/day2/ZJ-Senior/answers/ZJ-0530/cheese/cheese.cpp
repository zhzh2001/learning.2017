#include<cstdio>
#include<memory.h>
#include<cmath>
#include<algorithm>
#define N 1005
bool mark[N];
struct node{
	int x,y,z;
	bool operator< (const node &T)const{return z<T.z;}
}A[N];
long long sqr(int x){return 1ll*x*x;}
double calc(int i,int j){return sqrt(sqr(A[i].x-A[j].x)+sqr(A[i].y-A[j].y)+sqr(A[i].z-A[j].z));}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T,n,h,r;
	scanf("%d",&T);
	while(T--){
		memset(mark,0,sizeof mark);
		scanf("%d %d %d",&n,&h,&r);
		for(int i=1;i<=n;i++)
			scanf("%d %d %d",&A[i].x,&A[i].y,&A[i].z);
		bool ok=0;
		std::sort(A+1,A+n+1);
		for(int i=1;i<=n;i++){
			if(A[i].z<=r)mark[i]=1;
			else for(int j=1;j<i;j++){
				if(mark[j]&&calc(i,j)<=2*r){
					mark[i]=1;
					break;
				}
			}
			if(h-A[i].z<=r&&mark[i]){
				ok=1;
				break;
			}
		}
		if(ok)puts("Yes");
		else puts("No");
	}
	return 0;
}


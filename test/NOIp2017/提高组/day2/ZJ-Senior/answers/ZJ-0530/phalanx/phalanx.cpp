#include<cstdio>
#define N 10005
int A[N][N];
int n,m,q;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d %d %d",&n,&m,&q);
	if(n<=10000&&m<=10000){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				A[i][j]=i*m-m+j;
		while(q--){
			int x,y;
			scanf("%d %d",&x,&y);
			int t=A[x][y];
			printf("%d\n",A[x][y]);
			for(int j=y;j<m;j++)A[x][j]=A[x][j+1];
			for(int i=x;i<n;i++)A[i][m]=A[i+1][m];
			A[n][m]=t;
		}
	}
	return 0;
}


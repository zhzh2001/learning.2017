#include<cstdio>
#include<memory.h>
#define FOR(i,a,b) for(int i=(a),i##_END_=(b);i<=i##_END_;i++)
#define ROF(i,a,b) for(int i=(a),i##_END_=(b);i>=i##_END_;i--)
#define N 15
inline void Max(int &x,int y){if(x<y)x=y;}
inline void Min(int &x,int y){if(x>y)x=y;}
int dp[1<<N];
struct node{int to,len;};

int mark[N];
int dis[N][N];
int n,m;
int ans=0;
int CC=0;

void dfs(int x){
	FOR(i,1,n)if(!mark[i]&&dis[x][i]==CC){
		ans+=CC*mark[x];
		mark[i]=mark[x]+1;
	}
	FOR(i,1,n)if(mark[i]==mark[x]+1)dfs(i);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int a,b,c;
	bool flag=1;
	scanf("%d %d",&n,&m);
	memset(dis,63,sizeof dis);
	FOR(i,1,n)dis[i][i]=0;
	FOR(i,1,m){
		scanf("%d %d %d",&a,&b,&c);
		Min(dis[a][b],c);
		Min(dis[b][a],c);
		if(CC!=0&&CC!=c)flag=0;
		CC=c;
	}
	if(flag){
		int res=2e9;
		FOR(i,1,n){
			memset(mark,0,sizeof mark);
			ans=0;
			mark[i]=1;
			dfs(i);
			Min(res,ans);
		}
		printf("%d\n",res);
	}
	return 0;
}
/*
4 5
1 2 1
1 3 3
1 4 1
2 3 4
3 4 2
*/


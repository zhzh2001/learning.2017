#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <queue>
#include <set>
using namespace std;

typedef long long ll;

ifstream fin("phalanx.in");
ofstream fout("phalanx.out");

int n, m, q;

const int TN1 = 1005, N = 300005, Q = 300005;
int mat1[TN1][TN1]; // 4M
int xi[Q], yi[Q];

//void solve2() {
//	set<int> S;
//	S.insert(0);
//	static int a[N + Q];
//	int ed = n;
//	for (int i = 1; i <= n; ++i) {
//		a[i] = i;
//	}
//	for (int i = 1; i <= q; ++i) {
//		int y = yi[i];
//		int low = 1, high = ed, pos;
//		while (low <= high) {
//			int mid = (low + high) >> 1;
//			int result = mid - () + 1;
//			if (result == y) {
//				pos = mid;
//				break;	
//			} else if (result < y)
//				low = mid + 1;
//			else
//				high = mid - 1;
//		}
//		fout << a[pos] << endl;
//		S.insert(pos);
//		a[++ed] = a[pos];
//	}
//}

int main() {
	fin >> n >> m >> q;
	int now = 0;
	if (n <= 1000 && m <= 1000) { // 30 Points
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= m; ++j) {
				mat1[i][j] = ++now;
			}
		}
		for (int i = 1; i <= q; ++i) {
			int x, y;
			fin >> x >> y;
			int tmp = mat1[x][y];
			for (int i = y; i < m; ++i)
				mat1[x][i] = mat1[x][i + 1];
			for (int i = x; i < n; ++i)
				mat1[i][m] = mat1[i + 1][m];
			mat1[n][m] = tmp;
			fout << tmp << "\n";
		}
		fout << flush;
		return 0;
	}
	bool flag = true;
	for (int i = 1; i <= q; ++i) {
		fin >> xi[i] >> yi[i];
		flag &= (xi[i] == 1);
	}
//	if (flag) { // 20 Points
//		solve2();
//		fout << flush;
//		return 0;
//	}
	return 0;
}

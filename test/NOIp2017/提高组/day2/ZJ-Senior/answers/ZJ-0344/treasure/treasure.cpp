#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <queue>
using namespace std;

typedef long long ll;

ifstream fin("treasure.in");
ofstream fout("treasure.out");

const int N = 13, M = 1005, V = 500005;
const ll Inf = 1LL << 62;
int n, m, a[M], b[M], c[M];
ll mat[N][N], dist[N][N], crt;
void dfs(int x, int dis = 0, int cnt = 1, int fa = -1) {
	for (int i = 0; i < n; ++i) {
		if (i != fa && mat[x][i] != Inf) {
			dfs(i, dis + mat[x][i] * cnt, cnt + 1, x);
			dist[x][i] = dist[i][x] = min(dist[i][x], mat[x][i] * cnt);
		}
	}
}
void work1() {
	for (int i = 1; i <= m; ++i) {
		fin >> a[i] >> b[i] >> c[i];
		--a[i], --b[i];
		mat[a[i]][b[i]] = mat[b[i]][a[i]] = min(mat[b[i]][a[i]], (ll)c[i]);
	}
	ll ans = Inf;
	for (int i = 0; i < n; ++i) {
		crt = 0;
		for (int k = 0; k < N; ++k)
			for (int j = 0; j < N; ++j)
				dist[k][j] = Inf;
		dfs(i);
		for (int i = 1; i <= m; ++i)
			crt += dist[a[i]][b[i]];
		ans = min(ans, crt);
	}
	fout << ans << endl;
}
bool vis[N];
int len[N];
int bfs(int st) {
	memset(len, 0x3f, sizeof len);
	queue<int> Q;
	Q.push(st);
	vis[st] = true;
	len[st] = 0;
	while (!Q.empty()) {
		int x = Q.front();
		for (int i = 0; i < n; ++i) {
			if (x != i && mat[x][i] < Inf && len[i] > len[x] + 1) {
				len[i] = len[x] + 1;
				if (!vis[i]) {
					vis[i] = true;
					Q.push(i);	
				}
			}
		}
		Q.pop();
		vis[x] = false;
	}
	int ret = 0;
	for (int i = 0; i < n; ++i) {
		ret += len[i];
	}
	return ret;
}
void work2() {
	int ans = 0x3f3f3f3f;
	for (int i = 0; i < n; ++i) {
		ans = min(ans, bfs(i));
	}
	fout << (ll)ans * c[1] << endl;
}
struct DisjointSet {
	int fa[N * 3];
	inline void makeSet() {
		for (int i = 0; i < N; ++i)
			fa[i] = i + N;
		for (int i = N; i < N * 3; ++i)
			fa[i] = i;
	}
	inline int find(int x) {
		return x == fa[x] ? x : fa[x] = find(fa[x]);
	}
	inline void merge(int x, int y) {
		int fx = find(x), fy = find(y);
		if (fx != fy)
			fa[fx] = fy;
	}
	inline bool same(int x, int y) {
		return find(x) == find(y);
	}
	inline void setFa(int x, int f) {
		fa[x] = f;	
	}
} uf;
struct Edge {
	int u, v, w;
	Edge() {}
	Edge(int u, int v, int w) : u(u), v(v), w(w) {}
} e[M];
int ecnt;
bool cho[M];
struct edge {
	int to, nxt, w;	
} ep[M];
int head[N];
int cnt;
void addEdge(int u, int v, int w) {
	ep[++cnt].to = v;
	ep[cnt].w = w;
	ep[cnt].nxt = head[u];
	head[u] = cnt;
}
void check3(int x, int cnt = 1, int fa = -1) {
	for (int i = head[x]; i; i = ep[i].nxt) {
		if (ep[i].to != fa) {
			check3(ep[i].to, cnt + 1, x);
			dist[x][ep[i].to] = dist[ep[i].to][x] = min(dist[ep[i].to][x], (ll)ep[i].w * cnt);	
		}
	}
}
ll ans = Inf;
void test3(int x) {
	cnt = 0;
	memset(head, 0x00, sizeof head);
	uf.makeSet();
	for (int i = 0; i < ecnt; ++i) {
		if (cho[i]) {
			if (uf.same(e[i].u, e[i].v))
				return;
			addEdge(e[i].u, e[i].v, e[i].w);
			addEdge(e[i].v, e[i].u, e[i].w);	
			uf.merge(e[i].u, e[i].v);
		}
	}
	crt = 0;
	for (int k = 0; k < N; ++k)
		for (int j = 0; j < N; ++j)
			dist[k][j] = Inf;
	check3(x);
	for (int i = 1; i <= m; ++i)
		if (cho[i])
			crt += dist[e[i].u][e[i].v];
	ans = min(ans, crt);
}
void dfs3(int x, int y, int z) {
	if (y == ecnt) {
		if (z == n - 1)
			test3(x);
		return;
	}
	if (z >= n)
		return;
	cho[y] = false;
	dfs3(x, y + 1, z);
	cho[y] = true;
	dfs3(x, y + 1, z + 1);
}
void work3() {
	ecnt = 0;
	for (int i = 0; i < n; ++i)
		for (int j = i + 1; j < n; ++j) {
			if (mat[i][j] != Inf)
				e[ecnt++] = Edge(i, j, mat[i][j]);
		}
	for (int i = 0; i < n; ++i) {
		uf.makeSet();
		dfs3(i, 0, 0);
	}
	fout << ans << "\n";
}
int main() {
	fin >> n >> m;
	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			dist[i][j] = mat[i][j] = Inf;
	if (m == n - 1) { // 20 Points
		work1();
		return 0;	
	}
	bool flag = true;
	for (int i = 1; i <= m; ++i) {
		fin >> a[i] >> b[i] >> c[i];
		--a[i], --b[i];
		mat[a[i]][b[i]] = mat[b[i]][a[i]] = min(mat[b[i]][a[i]], (ll)c[i]);
		flag &= (c[i] == c[1]);
	}
	if (flag) { // 20 Points
		work2();
		return 0;
	}
	if (n <= 8) { // 30 Points
		work3();
		return 0;
	}
	return 0;
}

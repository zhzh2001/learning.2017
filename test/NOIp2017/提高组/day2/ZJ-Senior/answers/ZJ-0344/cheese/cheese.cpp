#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <queue>
#include <iostream>
using namespace std;

const int N = 1005;

typedef long long ll;

ifstream fin("cheese.in");
ofstream fout("cheese.out");

struct Atom {
	ll x, y, z;
} a[N];

struct DisjointSet {
	int fa[N];
	inline void makeSet() {
		for (int i = 0; i < N; ++i)
			fa[i] = i;
	}
	inline int find(int x) {
		return x == fa[x] ? x : fa[x] = find(fa[x]);
	}
	inline void merge(int x, int y) {
		int fx = find(x), fy = find(y);
		if (fx != fy)
			fa[fx] = fy;
	}
	inline bool same(int x, int y) {
		return find(x) == find(y);
	}
} uf;

inline ll sqr(ll x) {
	return x * x;
}

inline ll getDis(const Atom &c, const Atom &d) {
	return (sqr(c.x - d.x) + sqr(c.y - d.y) + sqr(c.z - d.z));
}

void solve() {
	int n, h, r;
	fin >> n >> h >> r;
	for (int i = 1; i <= n; ++i) {
		fin >> a[i].x >> a[i].y >> a[i].z;
	}
	uf.makeSet();
	int s = n + 1, t = n + 2;
	for (int i = 1; i <= n; ++i) {
		if (a[i].z <= r)
			uf.merge(s, i);
		if (a[i].z >= h - r)
			uf.merge(i, t);
		for (int j = i + 1; j <= n; ++j) {
			if (getDis(a[i], a[j]) <= 4LL * r * r)
				uf.merge(i, j);
		}
	}
	if (uf.same(s, t))
		fout << "Yes\n";
	else
		fout << "No\n";
}

int main() {
	int T;
	fin >> T;
	while (T--) {
		solve();
	}
	fout << flush;
	return 0;
}

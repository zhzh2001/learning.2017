#include<bits/stdc++.h>
using namespace std;
int n,m,x,y,v,a[50][50],d[50],minn,f[50],temp,lin[100],father[50][40],depth[50]={0,1};
bool b[50];
struct edge
{
	int y,next;
}
e[100];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
void init()
{
	memset(a,10,sizeof(a));
	n=read();
	m=read();
	for (int i=1;i<=m;++i)
	{
		x=read();
		y=read();
		v=read();
		a[x][y]=min(a[x][y],v);
		a[y][x]=min(a[x][y],v);
	}
}
void insertt(int ss,int ee)
{
	e[++temp].y=ee;
	e[temp].next=lin[ss];
	lin[ss]=ss;
}
void prim()
{
	memset(b,0,sizeof(b));
	for (int i=1;i<=n;++i)
	{
		d[i]=a[1][i];
		f[i]=1;
	}
	d[1]=0;
	b[1]=true;
	for (int i=1;i<n;++i)
	{
		int k=0,min_=2000000000;
		for (int j=1;j<=n;++j)
		if (!b[j]&&d[j]<min_)
		{
			k=j;
			min_=d[j];
			
		}
		if (k==0) return;
		b[k]=true;
		minn+=min_;
		for (int j=1;j<=n;++j)
		if (!b[j]&&d[j]>a[k][j]) 
		{
			d[j]=a[k][j];
			f[j]=k;
		}
	}
}
void jianshu()
{
	for (int i=1;i<=n;++i)
	{
		insertt(i,f[i]);
		insertt(f[i],i);
	}
}
void dfs(int t)
{
	for (int i=lin[t];i;i=e[i].next)
	{
		int to=e[i].y;
		if (to==father[t][0]) continue;
		depth[to]=depth[t]+1;
		father[to][0]=t;
		dfs(to);
	}
}
void yuchuli()
{
	for (int j=1;j<=20;++j)
	for (int i=1;i<=n;++i)
	if ((1<<j)<=n)
	father[i][j]=father[father[i][j-1]][j-1];
}
int lca(int aa,int bb)
{
	if (depth[aa]>depth[bb]) swap(aa,bb);
	int k=depth[bb]-depth[aa];
	for (int i=log(n)/log(2);i>=0;--i)
	if ((1<<i)&k) bb=father[bb][i];
	if (aa!=bb)
	{
		for (int i=log(n)/log(2);i>=0;--i)
		if (father[aa][i]!=father[bb][i])
		{
			aa=father[aa][i];
			bb=father[bb][i];
		}
		aa=father[aa][0];
	}
	return aa;
}	
void work()
{
	int sum=1;
	for (int i=1;i<m;++i)
	sum+=i;
	cout<<sum*v<<endl;
	/*for (int i=1;i<=n;++i)
	{
		for (int j=lin[i];j;j=e[j].next)
		
		
		
		//ans+=
	
	}*/
	//for (int i=1;i<=n;++i)
	//cout<<depth[i]<<" ";
	//cout<<lca(12,11)<<" "<<lca(7,9)<<" "<<lca(9,6);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	init();
	prim();
	jianshu();
	dfs(1);
	yuchuli();
	work();
	return 0;
}

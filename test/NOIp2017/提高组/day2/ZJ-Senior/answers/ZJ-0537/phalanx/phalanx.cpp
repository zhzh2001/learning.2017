#include<bits/stdc++.h>
using namespace std;
int n,m,q,b[5010][5010],c[300010],d[300010],e,f,ans,qu[30010];
struct ajz
{
	int x,y;
}
a[10000010];
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}	
void init()
{
	n=read();
	m=read();
	q=read();
	for (int i=1;i<=q;++i)
	{
		c[i]=read();
		d[i]=read();
	}
}	
void work1()
{
	for (int i=1;i<=n;++i)
	for (int j=1;j<=m;++j)
	{
		int k=(i-1)*m+j;
		a[k].x=i;
		a[k].y=j;
		b[i][j]=k;
	}
	for (int i=1;i<=q;++i)
	{
		e=c[i];
		f=d[i];
		ans=b[e][f];
		printf("%d\n",ans);
		for (int j=f;j<=m-1;++j)
		{
			a[b[e][j+1]].y--;
			b[e][j]=b[e][j+1];
		}
		for (int j=e;j<=n-1;++j)
		{
			a[b[j+1][m]].x--;
			b[j][m]=b[j+1][m];
		}	
		a[ans].x=n;
		a[ans].y=m;
		b[n][m]=ans;
	}
}
void work2()
{
	for (int i=1;i<=m;++i)
	qu[i]=i;
	for (int i=1;i<=q;++i)
	{
		int k=d[i];
		ans=qu[k];
		printf("%d\n",ans);
		for (int j=k;j<=m-1;++j)
		qu[j]=qu[j+1];
		qu[m]=ans;
	}
}
void work()
{
	if (n==1)
	{
		work2();
		return;
	}
	if (n<=1000&&m<=1000)
	{
		work1();
		return;
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	init();
	work();
	return 0;
}

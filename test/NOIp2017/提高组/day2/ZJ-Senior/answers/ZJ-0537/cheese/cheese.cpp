#include<bits/stdc++.h>
using namespace std;
int t,n,h,r;
bool bj;
struct azb
{
	int x,y,z;
}
a[1010];
bool mycmp(azb aa,azb bb)
{
	return (aa.z<bb.z)||(aa.z==bb.z&&aa.y<bb.y)||(aa.z==bb.z&&aa.y==bb.y&&aa.x<bb.x);
}
inline int read()
{
	int num=0,flag=1;
	char c=getchar();
	for (;c<'0'||c>'9';c=getchar())
	if (c=='-') flag=-1;
	for (;c>='0'&&c<='9';c=getchar())
	num=(num<<3)+(num<<1)+c-48;
	return num*flag;
}
double dis(int xx1,int yy1,int zz1,int xx2,int yy2,int zz2)
{
	return sqrt(1.0*((xx1-xx2)*(xx1-xx2)+(yy1-yy2)*(yy1-yy2)+(zz1-zz2)*(zz1-zz2)));
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	for (int i=1;i<=t;++i)
	{
		memset(a,0,sizeof(a));
		bj=true;
		n=read();
		h=read();
		r=read();
		for (int j=1;j<=n;++j)
		{
			a[j].x=read();
			a[j].y=read();
			a[j].z=read();
		}
		sort(a+1,a+n+1,mycmp);
		if (a[1].z>r||a[n].z<h-r)
		{
			printf("No\n");
			continue;
		}
		for (int j=1;j<n;++j)
		if (dis(a[j].x,a[j].y,a[j].z,a[j+1].x,a[j+1].y,a[j+1].z)>2*r)
		{
			bj=false;
			break;
		}
		if (bj) printf("Yes\n");
		else printf("No\n");
	}	
	return 0;
}

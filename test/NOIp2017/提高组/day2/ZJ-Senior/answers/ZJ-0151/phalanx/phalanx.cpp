#include<cstdio>
#define LL long long
using namespace std;
inline int _read(){
	int num=0;char ch=getchar();
	while(ch<'0'||ch>'9') ch=getchar();
	while(ch>='0'&&ch<='9') num=num*10+ch-48,ch=getchar();
	return num;
}
const int maxn=300005;
int n,m,Q,qx[maxn],qy[maxn];
LL ans[maxn];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=_read();m=_read();Q=_read();
	for (int i=1;i<=Q;i++){
		qx[i]=_read();qy[i]=_read();
		int x=qx[i],y=qy[i],pd=1;
		for (int j=i-1;j>=1;j--){
			if (x==n&&y==m){ans[i]=ans[j];pd=0;break;}
			if (y==m){
				if (qx[j]<=x) x++;
			}else{
				if (qx[j]==x&&qy[j]<=y) y++;
			}
		}
		if (pd) ans[i]=(LL)m*(x-1)+y;
		printf("%lld\n",ans[i]);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=15,maxm=4105;
int n,m,ans,cst[maxn][maxn],dis[maxn];
void DFS(int K,int dep,int now){
	if (now>=ans) return;
	if (K==(1<<n)-1){ans=now;return;}
	int a[15],lst[15];a[0]=0;
	for (int i=1;i<=n;i++) lst[i]=dis[i];
	for (int i=1;i<=n;i++)
	if ((K&(1<<i-1))==0) a[++a[0]]=i;
	for (int i=1;i<(1<<a[0]);i++){
		int sum=0,w=0;
		for (int j=1;j<=a[0];j++)
		if ((i&(1<<j-1))!=0) sum+=dis[a[j]]*dep,w+=(1<<a[j]-1);
		for (int j=1;j<=a[0];j++) if ((i&(1<<j-1))!=0)
		for (int k=1;k<=n;k++) if (cst[a[j]][k]<dis[k]) dis[k]=cst[a[j]][k];
		DFS(K+w,dep+1,now+sum);
		for (int j=1;j<=n;j++) dis[j]=lst[j];
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<=n;i++)
	for (int j=0;j<=n;j++) cst[i][j]=1e8;
	for (int i=1,x,y,z;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (z<cst[x][y]) cst[x][y]=z;
		cst[y][x]=cst[x][y];
	}
	ans=cst[0][0];
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++) dis[j]=cst[i][j];
		DFS(1<<(i-1),1,0);
	}
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#define LL long long
using namespace std;
const int maxn=1005;
LL x[maxn],y[maxn],z[maxn],R;
int f[maxn],n,H,T;
int get(int x){
	if (f[x]==x) return x;
	f[x]=get(f[x]);
	return f[x];
}
bool check(int i,int j){return R*R*4>=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);}
void work(){
	scanf("%d%d%lld",&n,&H,&R);
	for (int i=1;i<=n;i++) scanf("%lld%lld%lld",&x[i],&y[i],&z[i]),f[i]=i;
	f[0]=0;f[n+1]=n+1;
	for (int i=1;i<=n;i++)
	for (int j=i+1;j<=n;j++)
	if (check(i,j)) f[get(i)]=get(j);
	for (int i=1;i<=n;i++) if (z[i]-R<=0) f[get(0)]=get(i);
	for (int i=1;i<=n;i++) if (z[i]+R>=H) f[get(n+1)]=get(i);
	if (get(0)==get(n+1)) printf("Yes\n");else printf("No\n");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--) work();
	return 0;
}

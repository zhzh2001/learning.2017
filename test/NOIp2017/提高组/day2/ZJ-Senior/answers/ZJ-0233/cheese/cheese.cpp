#include <stdio.h>
#include <string.h>
int t,n,h,r,kk[5000],bb[5000][3],f[5000],rr;
void add(int x,int y,int z)
{
	bb[++rr][1]=x;
	bb[rr][2]=y;
	kk[rr]=z;
}
void dfs(int z,int x,int y)
{
	for(int i=1;i<=rr;i++)
		if(!f[i]&&((bb[i][1]-x)*(bb[i][1]-x)+(bb[i][2]-y)*(bb[i][2]-y)+(kk[i]-z)*(kk[i]-z))<=r*r*4)
		{
			f[i]=1;
			dfs(kk[i],bb[i][1],bb[i][2]);
		}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		rr=0;
		for(int i=1;i<=n;i++)
		{
			int x,y,z;
			scanf("%d%d%d",&x,&y,&z);
			add(x,y,z);
		}
		memset(f,0,sizeof(f));
		for(int i=1;i<=rr;i++)
			if(!f[i]&kk[i]<=r)
			{
				f[i]=1;
				dfs(kk[i],bb[i][1],bb[i][2]);
			}
		int ff=0;
		for(int i=1;i<=rr;i++)
			if(f[i]&&kk[i]>=h-r)
			{
				ff=1;
				break;
			}
		if(ff) printf("Yes\n");
			else printf("No\n");
	}
	return 0;
}
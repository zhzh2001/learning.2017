#include<bits/stdc++.h>
using namespace std;
struct node
{
	int from,to,value,next;
}mp[2000];
int n,m,tot,s,tot2;
int ans=1e+9;
int head[20];
int used[2000],used1[2000];
int cost[20][20];
int dis[20];
int vis[20];
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2)
	{
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline void read(int &x)
{
	char c=nc();int b=1;
	for (;c<'0'||c>'9';c=nc()) if (c=='-') b=-1;
	for (x=0;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
	x*=b;
}
inline void read(long long &x)
{
	char c=nc();int b=1;
	for (;c<'0'||c>'9';c=nc()) if (c=='-') b=-1;
	for (x=0;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
	x*=b;
}
inline void addedge(int x,int y,int z)
{
	mp[++tot].from=x;mp[tot].to=y;mp[tot].value=z;mp[tot].next=head[x];head[x]=tot;
}
inline void addedge2(int x,int y,int z)
{
	mp[++tot2].from=x;mp[tot2].to=y;mp[tot2].value=z;mp[tot2].next=head[x];head[x]=tot2;
}
inline void bfs()
{
	queue<int> q;
	for (int s=1;s<=n;s++)
	{
	  while (!q.empty()) q.pop();
	  q.push(s);
	  int sum=0;
	  memset(dis,0,sizeof(dis));
	  dis[s]=1;
	  memset(vis,0,sizeof(vis));
	  vis[s]=1;
	  while (!q.empty())
	  {
		  int u=q.front();
		  q.pop();
		  for (int i=head[u];i!=0;i=mp[i].next)
			if (vis[mp[i].to]==0&&(used[i]||(i>tot&&used[i-tot])))
			sum+=dis[u]*mp[i].value,q.push(mp[i].to),vis[mp[i].to]=1,dis[mp[i].to]=dis[u]+1;
	  }
	  ans=min(ans,sum);
	}
	return;
}
inline void dfs(int x,int cnt)
{
	if (tot-x+1+cnt<n-1) return;
	if (cnt==n-1)
	{
		bfs();
		return;
	}
	if (x==tot+1) return;
	dfs(x+1,cnt);
	if (used1[mp[x].from]==1&&used1[mp[x].to]==1) return;
	int flag1=0,flag2=0;
	if (used1[mp[x].from]==0) used1[mp[x].from]=1,flag1=1;
	if (used1[mp[x].to]==0) used1[mp[x].to]=1,flag2=1;
	used[x]=1;
	dfs(x+1,cnt+1);
	used[x]=0;
	if (flag1) used1[mp[x].from]=0;
	if (flag2) used1[mp[x].to]=0;
	return;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
	cost[i][j]=1e+9;
	for (int i=1;i<=m;i++)
	{
		int x,y,z;
		read(x);read(y);read(z);
		if (cost[x][y]>z) cost[x][y]=z;
		if (cost[y][x]>z) cost[y][x]=z;
	}
	for (int i=1;i<=n;i++)
	for (int j=i+1;j<=n;j++)
	if (cost[i][j]!=1e+9) addedge(i,j,cost[i][j]);
	tot2=tot;
	for (int i=1;i<=n;i++)
	for (int j=i+1;j<=n;j++)
	if (cost[i][j]!=1e+9) addedge2(j,i,cost[i][j]);
  dfs(1,0);
	printf("%d\n",ans);
	return 0;
}

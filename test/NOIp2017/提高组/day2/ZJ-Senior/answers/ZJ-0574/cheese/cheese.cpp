#include<bits/stdc++.h>
using namespace std;
struct point
{
	long long x,y,z;
}c[1001];
struct node
{
	int from,to,next;
}mp[1000001];
int n,h,tot;
int head[1001];
long long r;
inline char nc()
{
	static char buf[100000],*p1=buf,*p2=buf;
	if (p1==p2)
	{
		p2=(p1=buf)+fread(buf,1,100000,stdin);
		if (p1==p2) return EOF;
	}
	return *p1++;
}
inline void read(int &x)
{
	char c=nc();int b=1;
	for (;c<'0'||c>'9';c=nc()) if (c=='-') b=-1;
	for (x=0;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
	x*=b;
}
inline void read(long long &x)
{
	char c=nc();int b=1;
	for (;c<'0'||c>'9';c=nc()) if (c=='-') b=-1;
	for (x=0;c>='0'&&c<='9';x=x*10+c-'0',c=nc());
	x*=b;
}
void addedge(int x,int y)
{
	mp[++tot].from=x;mp[tot].to=y;mp[tot].next=head[x];head[x]=tot;
	mp[++tot].from=y;mp[tot].to=x;mp[tot].next=head[y];head[y]=tot;
}
queue<int> q;
int vis[1001];
int bfs()
{
	while (!q.empty()) q.pop();
	q.push(0);
	vis[0]=1;
	memset(vis,0,sizeof(vis));
	while (!q.empty())
	{
		int u=q.front();
		if (u==n+1) return 1;
		q.pop();
		for (int i=head[u];i;i=mp[i].next)
		{
			int v=mp[i].to;
			if (!vis[v]) q.push(v),vis[v]=1; 
		}
	}
	return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	read(t);
	while (t--)
	{
		tot=0;
		memset(head,0,sizeof(head));
	  read(n);read(h);read(r);
	  for (int i=1;i<=n;i++)
	  read(c[i].x),read(c[i].y),read(c[i].z);
	  for (int i=1;i<=n;i++)
  	for (int j=i+1;j<=n;j++)
	  {
	    long long dist=(c[i].x-c[j].x)*(c[i].x-c[j].x)+(c[i].y-c[j].y)*(c[i].y-c[j].y);
	    if (dist>r*r*4) continue;
	    dist-=r*r*4;
	    dist+=(c[i].z-c[j].z)*(c[i].z-c[j].z);
	    if (dist<=0) addedge(i,j);
	  }
	  for (int i=1;i<=n;i++)
	  if (c[i].z-r<=0) addedge(0,i);
  	for (int i=1;i<=n;i++)
	  if (c[i].z+r>=h) addedge(i,n+1);
	  if (bfs()) printf("Yes\n");
	  else printf("No\n");
	}
	return 0;
}

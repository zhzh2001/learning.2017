#include<bits/stdc++.h>
using namespace std;
int n,m,q,k;
int num[5001][5001];
long long num1[1000000],tree[5000000];
int lowbit(int x)
{
	return x&(-x);
}
void update(int x,int y)
{
	while (x<=k)
	{
		tree[x]+=y;
		x+=lowbit(x);
	}
}
long long query(int x)
{
	long long sum=0;
	while (x)
	{
		sum+=tree[x];
		x-=lowbit(x);
	}
	return sum;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=5000&&m<=5000)
	{
		for (int i=1;i<=n;i++)
  	for (int j=1;j<=m;j++)
	  num[i][j]=(i-1)*m+j;
	  while (q--)
	  {
		  int x,y;
		  scanf("%d%d",&x,&y);
		  int k=num[x][y];
		  printf("%d\n",num[x][y]);
		  for (int i=y;i<=m-1;i++) num[x][i]=num[x][i+1];
		  for (int i=x;i<=n-1;i++) num[i][m]=num[i+1][m];
		  num[n][m]=k;
	  }
	  return 0;
	}
	if (q<=500)
	{
		return 0;
	}
	for (int i=1;i<=m;i++)
	num1[i]=i;
	for (int i=2;i<=n;i++)
	num1[m+i-1]=m*i;
	k=m+n-1;
	int lastans=0,ans=0;
	while (q--)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		ans=num1[query(y)+y];
		printf("%d\n",ans);
		update(y,1);
		update(k,-1);
		update(k,-lastans);
		update(k+1,lastans);
		update(k,ans);
		update(k+1,-ans);
		lastans=ans;
	}
	return 0;
}

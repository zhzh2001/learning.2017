#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
const int inf=1000000007;
const int M=600010;
int n,m,i,j,k,l,x,y,z,a[110][110],mn[5010][20],f[4100][4100],mi[20],A[M],B[M],C[M],revB[5010],revC[5010],dp[12][M];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)a[i][j]=inf;
	for (i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=min(a[x][y],z);
		a[y][x]=min(a[y][x],z);
	}
	for (i=1;i<1<<n;i++)
		for (j=1;j<=n;j++)
			if (!(i&(1<<(j-1)))){
				mn[i][j]=inf;
				for (k=1;k<=n;k++)
					if (i&(1<<(k-1)))mn[i][j]=min(mn[i][j],a[j][k]);
			}
	for (i=1;i<1<<n;i++)
		for (j=i;j;j=(j-1)&i){
			int k=i^j;
			if (!k)continue;
			for (l=1;l<=n;l++)
				if (j&(1<<(l-1))){
					if (mn[k][l]==inf){f[j][k]=inf;break;}
					f[j][k]+=mn[k][l];
				}
		}
	mi[0]=1;for (i=1;i<=n;i++)mi[i]=mi[i-1]*3;
	for (i=0;i<mi[n];i++){
		int tmp=i;
		for (j=1;j<=n;j++){
			if (tmp%3==0)A[i]^=1<<(j-1);
			if (tmp%3==1)B[i]^=1<<(j-1);
			if (tmp%3==2)C[i]^=1<<(j-1);
			tmp/=3;
		}
	}
	for (i=0;i<1<<n;i++)
		for (j=1;j<=n;j++)
			if (i&(1<<(j-1))){
				revB[i]+=mi[j-1];
				revC[i]+=mi[j-1]*2;
			}
	for (i=0;i<n;i++)
		for (j=0;j<mi[n];j++)dp[i][j]=inf;
	for (i=1;i<=n;i++)dp[0][mi[i-1]*2]=0;
	int ans=inf;
	for (int dep=0;dep<n-1;dep++){
		for (i=0;i<mi[n];i++)
			if (!A[i])ans=min(ans,dp[dep][i]);
		for (i=0;i<mi[n];i++)
			if (dp[dep][i]<ans){
				int x=A[i],y=B[i],z=C[i];
				for (j=x;j;j=(j-1)&x)
					if (f[j][z]!=inf)dp[dep+1][revC[j]+revB[y^z]]=min(dp[dep+1][revC[j]+revB[y^z]],dp[dep][i]+f[j][z]*(dep+1));
			}
	}
	for (i=0;i<mi[n];i++)if (!A[i])ans=min(ans,dp[n-1][i]);
	printf("%d\n",ans);
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
const int M=2010;
int cas,edgenum,n,i,j,h;
ll r,x[M],y[M],z[M];
int head[M],vet[2100010],next[2100010],vis[M];
void addedge(int x,int y){
	vet[++edgenum]=y;
	next[edgenum]=head[x];
	head[x]=edgenum;
}
ull dist(int i,int j){
	ull ans=(x[i]-x[j])*(x[i]-x[j]);
	return ans+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
}
void dfs(int u){
	vis[u]=1;
	for (int e=head[u];e;e=next[e]){
		int v=vet[e];
		if (!vis[v])dfs(v);
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&cas);
	while (cas--){
		edgenum=0;
		memset(head,0,sizeof head);
		scanf("%d%d%lld",&n,&h,&r);
		for (i=1;i<=n;i++){
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if (abs(z[i])<=r)addedge(0,i);
			if (abs(z[i]-h)<=r)addedge(i,n+1);
		}
		for (i=1;i<=n;i++)
			for (j=i+1;j<=n;j++)
				if (dist(i,j)<=r*r*4)addedge(i,j),addedge(j,i);
		memset(vis,0,sizeof vis);
		dfs(0);
		if (vis[n+1])puts("Yes");else puts("No");
	}
}

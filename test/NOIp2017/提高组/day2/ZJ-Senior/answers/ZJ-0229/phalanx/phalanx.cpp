#include<cstdio>
#include<algorithm>
#include<cstring>
#include<vector>
using namespace std;
typedef long long ll;
const int M=50010;
int n,m,q,x,y,cnt[M],a[M][510];
ll ans,b[M][510];
int getpos(int id,int x){
	if (!cnt[id])return x;
	sort(a[id]+1,a[id]+cnt[id]+1);
	if (x>a[id][cnt[id]]-cnt[id])return x+cnt[id];
	int l=1,r=cnt[id];
	while (l<r){
		int mid=l+r>>1;
		if (a[id][mid]-mid>=x)r=mid;else l=mid+1;
	}
	return x+l-1;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	while (q--){
		scanf("%d%d",&x,&y);
		if (y==m){
			x=getpos(0,x);
			if (x<=n)ans=(ll)x*m;else ans=b[0][x-n];
			printf("%lld\n",ans);
			a[0][++cnt[0]]=x;
			b[0][cnt[0]]=ans;
		}else{
			y=getpos(x,y);
			if (y<m)ans=(ll)(x-1)*m+y;else ans=b[x][y-m+1];
			printf("%lld\n",ans);
			a[x][++cnt[x]]=y;
			int X=getpos(0,x);
			a[0][++cnt[0]]=X;
			b[0][cnt[0]]=ans;
			if (X<=n)ans=(ll)X*m;else ans=b[0][X-n];
			b[x][cnt[x]]=ans;
		}
	}
}

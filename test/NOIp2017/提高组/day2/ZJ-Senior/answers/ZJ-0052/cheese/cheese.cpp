#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int T,n;
long long h,r,x[1010],y[1010],z[1010];
int head[1010],nxt[1100000],vet[1100000],cnt;
bool vis[1010];
int que[1010];
void addedge(int u,int v){
	nxt[++cnt]=head[u];vet[cnt]=v;head[u]=cnt;
}
long long sqr(long long x){
	return x*x;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		memset(vis,0,sizeof(vis));
		memset(head,0,sizeof(head));
		memset(nxt,0,sizeof(nxt));
		cnt=0;
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		}
		long long dr=4*r*r;
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
				if ((sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))<=dr){
					addedge(i,j);
					addedge(j,i);
				}
		for (int i=1;i<=n;i++){
			if (z[i]<=r) addedge(0,i);
			if (z[i]+r>=h) addedge(i,n+1);
		}
		vis[0]=1;
		int hd=0,tl=1;
		que[tl]=0;
		while (hd<tl){
			int u=que[++hd];
			for (int i=head[u];i;i=nxt[i]){
				int v=vet[i];
				if (!vis[v]) vis[v]=1,que[++tl]=v;
			}
			if (vis[n+1]) break;
		}
		if (vis[n+1]) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

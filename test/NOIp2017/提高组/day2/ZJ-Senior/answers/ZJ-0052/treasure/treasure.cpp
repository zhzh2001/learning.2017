#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int Mod=10000000;
int n,m,map[20],hd,tl,que[10000010],edge[20],cost[20][20];
long long dis[10010];
bool vis[10010];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(map,-1,sizeof(map));
	memset(cost,-1,sizeof(cost));
	for (int i=1;i<=m;i++){
		int u,v,l;
		scanf("%d%d%d",&u,&v,&l);
		if (l<cost[u][v] || cost[u][v]==-1) cost[u][v]=l;
		if (l<cost[v][u] || cost[v][u]==-1) cost[v][u]=l;
		edge[u]|=1<<(v-1);
		edge[v]|=1<<(u-1);
 	}
	long long ans=-1;
 	for (int t=1;t<=n;t++){
 		memset(dis,-1,sizeof(dis));
 		memset(map,-1,sizeof(map));
 		memset(vis,0,sizeof(vis));
 		map[t]=1;
 		int hd=0,tl=1;
 		que[tl]=1<<(t-1);
 		dis[que[tl]]=0;
 		while (hd<tl){
 			hd=(hd+1)%Mod;
 			int u=que[hd];
 			vis[u]=1;
			for (int i=1;i<=n;i++){
				if (((1<<(i-1))&u)==0 && (edge[i]&u)>0){
					long long disnow=-1;
					int dispos=-1;
					for (int j=1;j<=n;j++){
						if (((1<<(j-1))&u)>0 && (edge[i]&(1<<(j-1)))>0){
							if ((map[j]*cost[j][i]<=disnow || disnow==-1)){
								disnow=1ll*map[j]*cost[j][i];
								dispos=j;
							}
						}
					}
					int v=u|(1<<(i-1));
					if ((disnow+dis[u]<dis[v] || dis[v]==-1) && disnow!=-1){
						map[i]=map[dispos]+1;
						dis[v]=disnow+dis[u];
						tl=(tl+1)%Mod;
						que[tl]=v;
					}
				}
			}	
		}
		if (dis[(1<<n)-1]<ans || ans==-1) ans=dis[(1<<n)-1];
	}
	printf("%lld\n",ans);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n,m,q,cnt;
long long pos[1010][1010];
int tree[600010],num[600010];
int lowbit(int x){
	return x&(-x);
}
int query(int x){
	int sum=0;
	for (int i=x;i>=1;i-=lowbit(i)){
		sum+=tree[i];
	}
	return sum;
}
void insert(int x,int val){
	for (int i=x;i<=m*2;i+=lowbit(i)){
		tree[i]+=val;
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000 && m<=1000){
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				pos[i][j]=1ll*(i-1)*m+j;
		while (q--){
			int x,y;
			scanf("%d%d",&x,&y);
			long long tmp=pos[x][y];
			for (int i=y;i<m;i++)
				pos[x][i]=pos[x][i+1];
			for (int i=x;i<n;i++)
				pos[i][m]=pos[i+1][m];
			pos[n][m]=tmp;
			printf("%lld\n",tmp);
		}
	}
	else{
		cnt=m;
		for (int i=1;i<=m;i++){
			insert(i,1);
			num[i]=i;
		}
	 	while (q--){
	 		int x,y;
	 		scanf("%d%d",&x,&y);
	 		int l=1,r=cnt,ans=-1;
	 		while (l<=r){
	 			int mid=(l+r)>>1;
	 			if (query(mid)<=y){
	 				l=mid+1;
	 				ans=mid;
				 }
				 else r=mid-1;
			}
			insert(ans,-1);
			printf("%d\n",num[ans]);
			num[++cnt]=num[ans];
			insert(cnt,1);
		}
	}
	return 0;
}

program cheese;
var
  x,y,z:array[1..10000] of longint;
  f:array[0..1005,0..1005] of boolean;
  d,b:array[0..10000] of boolean;
  n,m,r,h,t:longint;

procedure init;
begin
  readln(t);
end;

function judge(i,j:longint):boolean;
var p:real;
begin
  p:=r+r;
  if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]))<=p*p then exit(true);
  exit(false);
end;

procedure doit;
var i,j,k,p:longint;
begin
  for k:=1 to t do
  begin
    readln(n,h,r);
    fillchar(d,sizeof(d),0);
    fillchar(b,sizeof(b),0);
    fillchar(f,sizeof(f),0);
    for i:=1 to n do readln(x[i],y[i],z[i]);
    for i:=1 to n-1 do
      for j:=i+1 to n do
        if judge(i,j) then begin f[i,j]:=true;f[j,i]:=true;end;
    for i:=1 to n do
    begin
      if z[i]<=r then begin f[i,0]:=true;f[0,i]:=true;end;
      if z[i]>=h-r then begin f[i,n+1]:=true;f[n+1,i]:=true;end;
    end;
    d[0]:=true;
    for i:=0 to n+1 do
    begin
      p:=-1;
      for j:=0 to n+1 do if (not b[j]) and d[j] then begin p:=j;break;end;
      if (p=n+1) and (not d[p]) then break;
      if p>-1 then begin b[j]:=true;end;
      if p>-1 then for j:=1 to n+1 do
        if not d[j] then if f[p,j] then d[j]:=true;
    end;
    if d[n+1] then writeln('Yes') else writeln('No');
  end;
end;

begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  init;
  doit;
  close(input);close(output);
end.
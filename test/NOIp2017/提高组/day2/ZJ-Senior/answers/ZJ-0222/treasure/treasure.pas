program treasure;
var
  mst,ans:int64;
  e:array[1..10000] of record x,y,w:longint;end;
  //f:array[0..20,0..20] of longint;
  b:array[0..20] of boolean;
  d:array[0..20] of longint;
  n,m:longint;

procedure init;
var i,x,y:longint;
begin
  readln(n,m);
  fillchar(e,sizeof(e),2);
  for i:=1 to m do
  begin
    readln(x,y,e[i].w);
    e[i].x:=x;e[i].y:=y;
  end;
  ans:=1000000000;
end;

procedure doit;
var i,j,min,t,k,s:longint;
begin
  for k:=1 to n do
  begin
    fillchar(d,sizeof(d),0);
    fillchar(b,sizeof(b),0);
    mst:=0;
    b[k]:=true;
    d[k]:=1;
    for i:=1 to n-1 do
    begin
      min:=maxlongint;
      t:=0;
      s:=0;
      for j:=1 to m do
      begin
        if b[e[j].x] and not b[e[j].y] then
          if min>e[j].w*d[e[j].x] then
            begin s:=e[j].x;t:=e[j].y;min:=e[j].w*d[e[j].x];end;
        if b[e[j].y] and not b[e[j].x] then
          if min>e[j].w*d[e[j].y] then
            begin s:=e[j].y;t:=e[j].x;min:=e[j].w*d[e[j].y];end;
      end;
      b[t]:=true;
      d[t]:=d[s]+1;
      mst:=mst+min;
    end;
  if mst<ans then ans:=mst;
  end;
end;

procedure outit;
begin
  writeln(ans);
end;

begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  init;
  doit;
  outit;
  close(input);close(output);
end.
#include <cstdio>
int N, M, T1[531441], T2[531441], T3[4096], P3[13], bitcount[4096];
long long E[12][4096], EE[531441], f[12][531441];
inline void cover(long long &x, long long y)
{
	x = x < y ? x : y;
}
int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &N, &M);
	for (int i = 0; i < 1 << N; i++)
		for (int j = 0; j < N; j++)
			if (i >> j & 1)
				bitcount[i]++;
	P3[0] = 1;
	for (int i = 1; i <= N; i++)
		P3[i] = P3[i - 1] * 3;
	for (int i = 0; i < P3[N]; i++)
		for (int j = 0; j < N; j++)
		{
			if (i / P3[j] % 3 == 1)
				T1[i] |= 1 << j;
			if (i / P3[j] % 3 == 2)
				T2[i] |= 1 << j;
		}
	for (int i = 0; i < 1 << N; i++)
		for (int j = 0; j < N; j++)
			if (i >> j & 1)
				T3[i] += P3[j];
	for (int i = 0; i < N; i++)
		for (int j = 0; j < 1 << N; j++)
			E[i][j] = 1LL << 40;
	for (int i = 0, u, v, w; i < M; i++)
	{
		scanf("%d%d%d", &u, &v, &w);
		u--, v--;
		for (int j = 0; j < 1 << N; j++)
		{
			if (j >> u & 1)
				cover(E[v][j], w);
			if (j >> v & 1)
				cover(E[u][j], w);
		}
	}
	for (int i = 0; i < P3[N]; i++)
	{
		int x = T1[i], y = T2[i];
		for (int j = 0; j < N; j++)
			if (y >> j & 1)
				EE[i] += E[j][x];
	}
	for (int i = 0; i < P3[N]; i++)
		f[0][i] = 1LL << 40;
	for (int i = 0; i < N; i++)
		f[0][P3[i] + P3[i]] = 0;
	for (int i = 1; i < N; i++)
		for (int j = 0; j < P3[N]; j++)
		{
			f[i][j] = 1LL << 40;
			int x = T1[j], y = T2[j];
			long long &THIS = f[i][j], *last = f[i - 1] + T3[x], *ee = EE + T3[y] + T3[y];
			if (y && bitcount[x] >= i)
				for (int k = x; k; k = k - 1 & x)
					if (last[T3[k]] <= 72000000 && ee[T3[k]] <= 6000000)
						cover(THIS, last[T3[k]] + ee[T3[k]] * i);
		}
	long long O = 1LL << 60;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < P3[N]; j++)
			if (T1[j] + T2[j] == (1 << N) - 1)
				cover(O, f[i][j]);
	printf("%lld\n", O);
	return 0;
}

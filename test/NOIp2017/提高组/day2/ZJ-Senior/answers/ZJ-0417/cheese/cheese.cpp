#include <cstdio>
int TC, N, f[1001], w[1001];
long long H, R, LIM, x[1001], y[1001], z[1001];
int F(int x)
{
	return f[x] == x ? x : f[x] = F(f[x]);
}
void add(int u, int v)
{
	u = F(u), v = F(v);
	f[u] = v;
}
inline long long sqr(long long x)
{
	return x * x;
}
inline bool check(int u, int v)
{
	return sqr(x[u] - x[v]) + sqr(y[u] - y[v]) + sqr(z[u] - z[v]) <= LIM;
}
int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	for (scanf("%d", &TC); TC--; )
	{
		scanf("%d%lld%lld", &N, &H, &R);
		LIM = 4 * R * R;
		for (int i = 1; i <= N; i++)
			scanf("%lld%lld%lld", x + i, y + i, z + i);
		for (int i = 1; i <= N; i++)
			f[i] = i;
		for (int i = 1; i < N; i++)
			for (int j = i + 1; j <= N; j++)
				if (check(i, j))
					add(i, j);
		for (int i = 1; i <= N; i++)
			w[i] = 0;
		for (int i = 1; i <= N; i++)
		{
			if (z[i] <= R)
				w[F(i)] |= 1;
			if (z[i] >= H - R)
				w[F(i)] |= 2;
		}
		int exist3 = 1;
		while (exist3 <= N && w[exist3] != 3)
			exist3++;
		puts(exist3 <= N ? "Yes" : "No");
	}
	return 0;
}

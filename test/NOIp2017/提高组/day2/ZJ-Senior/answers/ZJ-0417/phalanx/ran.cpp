#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <ctime>
int N, M, Q;
int main()
{
	srand(time(NULL));
	scanf("%d%d%d", &N, &M, &Q);
	printf("%d %d %d\n", N, M, Q);
	for (int i = 1; i <= Q; i++)
		printf("%d %d\n", rand() % N + 1, rand() % M + 1);
	return 0;
}

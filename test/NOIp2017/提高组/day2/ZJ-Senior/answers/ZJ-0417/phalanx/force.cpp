#include <cstdio>
#include <algorithm>
int N, M, Q, a[1001][1001];
int main()
{
	scanf("%d%d%d", &N, &M, &Q);
	for (int i = 1; i <= N; i++)
		for (int j = 1; j <= M; j++)
			a[i][j] = (i - 1) * M + j;
	while (Q--)
	{
		int x, y, ans;
		scanf("%d%d", &x, &y);
		printf("%d\n", ans = a[x][y]);
		for (int i = y; i < M; i++)
			a[x][i] = a[x][i + 1];
		for (int i = x; i < N; i++)
			a[i][M] = a[i + 1][M];
		a[N][M] = ans;
	}
	return 0;
}

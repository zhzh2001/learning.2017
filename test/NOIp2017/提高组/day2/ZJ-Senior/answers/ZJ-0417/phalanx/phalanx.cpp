#include <cstdio>
#include <vector>
#include <algorithm>
int N, M, Q, x[300001], y[300001], LEN, T[2097152], tref[600001], Last[300001];
std::vector < int > PB[300001];
std::vector < long long > Append[300001], LastAppend;
std::vector < int >::iterator IT[300001];
void build(int p, int l, int r)
{
	if (l == r)
	{
		T[p] = 1;
		tref[l] = p;
		return;
	}
	int m = l + r >> 1;
	build(p << 1, l, m);
	build(p << 1 | 1, m + 1, r);
	T[p] = T[p << 1] + T[p << 1 | 1];
}
int get(int k)
{
	int p = 1, l = 1, r = LEN;
	while (l < r)
	{
		int m = l + r >> 1;
		if (k <= T[p << 1])
			p <<= 1, r = m;
		else
			k -= T[p << 1], p = p << 1 | 1, l = m + 1;
	}
	for (; p; p >>= 1)
		T[p]--;
	return l;
}
void restore(int x)
{
	for (int p = tref[x]; p; p >>= 1)
		T[p]++;
}
void LineQ(std::vector < int > &V)
{
	for (std::vector < int >::iterator it = V.begin(); it != V.end(); it++)
		*it = get(*it);
	for (std::vector < int >::iterator it = V.begin(); it != V.end(); it++)
		restore(*it);
}
int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &N, &M, &Q);
	build(1, 1, LEN = std::max(N, M) + Q);
	for (int i = 1; i <= Q; i++)
	{
		scanf("%d%d", x + i, y + i);
		PB[x[i]].push_back(y[i]);
		Last[i] = x[i];
	}
	for (int i = 1; i <= N; i++)
		LineQ(PB[i]);
	for (int i = 1; i <= Q; i++)
		Last[i] = get(Last[i]);
	for (int i = 1; i <= Q; i++)
		restore(Last[i]);
	for (int i = 1; i <= N; i++)
		IT[i] = PB[i].begin();
	for (int i = 1; i <= Q; i++)
	{
		long long ans, to_append;
		if (x[i] < N && y[i] < M)
		{
			to_append = Last[i] <= N ? (long long)Last[i] * M : LastAppend[Last[i] - N - 1];
			ans = *IT[x[i]] < M ? (long long)(x[i] - 1) * M + *IT[x[i]] : Append[x[i]][*IT[x[i]] - M];
			Append[x[i]].push_back(to_append);
			LastAppend.push_back(ans);
		}
		else if (x[i] < N)
		{
			to_append = Last[i] <= N ? (long long)Last[i] * M : LastAppend[Last[i] - N - 1];
			Append[x[i]].push_back(to_append);
			ans = *IT[x[i]] < M ? (long long)(x[i] - 1) * M + *IT[x[i]] : Append[x[i]][*IT[x[i]] - M];
			LastAppend.push_back(ans);
		}
		else if (y[i] < M)
		{
			ans = *IT[x[i]] < M ? (long long)(x[i] - 1) * M + *IT[x[i]] : Append[x[i]][*IT[x[i]] - M];
			LastAppend.push_back(ans);
			to_append = Last[i] <= N ? (long long)Last[i] * M : LastAppend[Last[i] - N - 1];
			Append[x[i]].push_back(to_append);
		}
		else
		{
			to_append = Last[i] <= N ? (long long)Last[i] * M : LastAppend[Last[i] - N - 1];
			Append[x[i]].push_back(to_append);
			ans = *IT[x[i]] < M ? (long long)(x[i] - 1) * M + *IT[x[i]] : Append[x[i]][*IT[x[i]] - M];
			LastAppend.push_back(ans);
		}
		IT[x[i]]++;
		printf("%lld\n", ans);
	}
	return 0;
}

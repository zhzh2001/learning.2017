#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cctype>
#include <vector>
#include <queue>
using namespace std;
template<class T>
void read(T &x)
{
	bool sign = true;
	x = 0;
	char c = getchar();
	while (!isdigit(c))
	{
		if (c == '-')
			sign = false;
		c = getchar();	
	}
	while (isdigit(c))
		x = x * 10 + (c & 0xf), c = getchar();
	if (!sign)
		x = -x;
}
typedef long long ll;
const int MAXN = 1111;
struct Point
{
	int x, y, z;
} pt[MAXN];
#define square(x) ((ll)(x)*(x))
double dis(const int &x, const int &y)
{
	return sqrt(square(pt[x].x-pt[y].x)+square(pt[x].y-pt[y].y)+square(pt[x].z-pt[y].z));
}
ll dis2(const int &x, const int &y)
{
	return (square(pt[x].x-pt[y].x)+square(pt[x].y-pt[y].y)+square(pt[x].z-pt[y].z));
}
#undef suqare
int Abs(int x) {return x < 0 ? -x : x;}
vector<int> G[MAXN];
queue<int> Q;
bool vis[MAXN];
void Solve()
{
	int n, h, r;
	read(n), read(h), read(r);
	int S = n + 1, T = S + 1;
	for (int i = 1; i <= n; ++i)
	{
		read(pt[i].x),
		read(pt[i].y),
		read(pt[i].z);
	}
	for (int i = 1; i <= n + 2; ++i)
		G[i].clear();
	for (int i = 1; i <= n; ++i)
	{
		if (Abs(pt[i].z) <= r)
			G[S].push_back(i);
		if (Abs(pt[i].z-h) <= r)
			G[i].push_back(T);
		for (int j = 1; j <= n; ++j)
		{
			if (j == i)
				continue;
			if (dis2(i, j) <= (ll)r * r * 4)
				G[i].push_back(j);
		}
	}
	memset(vis, 0, sizeof vis);
	vis[S] = true;
	Q.push(S);
	while (!Q.empty())
	{
		int u = Q.front();
		Q.pop();
		for (size_t i = 0, len = G[u].size(); i < len; ++i)
		{
			int v = G[u][i];
			if (!vis[v])
			{
				vis[v] = true;
				Q.push(v);
			}
		}
	}
	if (vis[T])
		puts("Yes");
	else
		puts("No");
}
int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int T;
	read(T);
	while (T--)
		Solve();
	return 0;
}


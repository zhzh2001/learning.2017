#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cctype>
#include <deque>
#include <utility>
using namespace std;
template<class T>
void read(T &x)
{
	x = 0;
	char c = getchar();
	while (!isdigit(c))
		c = getchar();
	while (isdigit(c))
		x = x * 10 + (c & 0xf), c = getchar();
}
int n, m, q;
namespace Part1
{
	int col[1111][1111];
	void change(int x, int y)
	{
		int P = col[x][y];
		for (int j = y + 1; j <= m; ++j)
			col[x][j - 1] = col[x][j];
		for (int j = x + 1; j <= n; ++j)
			col[j-1][m] = col[j][m];
		col[n][m] = P;
	}
	void Solve()
	{
		for (int i = 1; i <= n; ++i)
			for (int j = 1; j <= m; ++j)
				col[i][j] = (i - 1) * m + j;
		for (int i = 1; i <= q; ++i)
		{
			int x, y;
			read(x), read(y);
			printf("%d\n", col[x][y]);
			change(x, y);
		}
		
	}	
}
namespace treap
{
	const int MAXN = 300333;
	struct Node
	{
		int l, r, fix, size;
	} nd[MAXN];
	int tot, root;
	int newNode()
	{
		++tot;
		nd[tot].size = 1;
		nd[tot].l = nd[tot].r = 0;
		nd[tot].fix = rand();
		return tot;
	}
	inline void pushup(int x)
	{
		nd[x].size = nd[nd[x].l].size + nd[nd[x].r].size + 1;
	}
	int merge(int a, int b)
	{
		if (a == 0 || b == 0) return a ^ b;
		if (nd[a].fix < nd[b].fix)
		{
			nd[a].r = merge(nd[a].r, b);
			pushup(a);
			return a;
		}
		else
		{
			nd[b].l = merge(a, nd[b].l);
			pushup(b);
			return b;
		}
	}
	void split(int x, int rnk, int &a, int &b)
	{
		if (x == 0)
		{
			a = 0;
			b = 0;
			return;
		}
		if (nd[nd[x].l].size >= rnk)
		{
			split(nd[x].l, rnk, a, b);
			nd[x].l = b;
			b = x;
		}
		else
		{
			split(nd[x].r, rnk - nd[nd[x].l].size - 1, a, b);
			nd[x].r = a;
			a = x;
		}
		pushup(x);
	}
}
const int MAXN = 300333 << 1;
struct Treap
{
	struct Node
	{
		int l, r, fix, size, val;
	} nd[MAXN];
	int tot, root;
	int newNode(int val)
	{
		++tot;
		nd[tot].size = 1;
		nd[tot].val = val;
		nd[tot].l = nd[tot].r = 0;
		nd[tot].fix = rand();
		return tot;
	}
	inline void pushup(int x)
	{
		nd[x].size = nd[nd[x].l].size + nd[nd[x].r].size + 1;
	}
	int merge(int a, int b)
	{
		if (a == 0 || b == 0) return a ^ b;
		if (nd[a].fix < nd[b].fix)
		{
			nd[a].r = merge(nd[a].r, b);
			pushup(a);
			return a;
		}
		else
		{
			nd[b].l = merge(a, nd[b].l);
			pushup(b);
			return b;
		}
	}
	void split(int x, int rnk, int &a, int &b)
	{
		if (x == 0)
		{
			a = 0;
			b = 0;
			return;
		}
		if (nd[nd[x].l].size >= rnk)
		{
			split(nd[x].l, rnk, a, b);
			nd[x].l = b;
			b = x;
		}
		else
		{
			split(nd[x].r, rnk - nd[nd[x].l].size - 1, a, b);
			nd[x].r = a;
			a = x;
		}
		pushup(x);
	}
};
namespace Part2
{
	deque<int> Q;
	void Solve()
	{
		Q.resize(m + 1);
		for (int i = 1; i <= m; ++i)
			Q[i] = i;
		deque<int>::iterator it;
		for (int i = 1; i <= q; ++i)
		{
			int x, y;
			read(x), read(y);
			it = Q.begin() + y;
			int P = *it;
			printf("%d\n", P);
			Q.erase(it);
			Q.push_back(P);
		}
	}
}
namespace Part3
{
	void Solve()
	{
		using namespace treap;
		for (int i = 1; i <= m; ++i)
			root = merge(root, newNode());
		for (int i = 1; i <= q; ++i)
		{
			int x, y;
			read(x), read(y);
			int a, b, c, d;
			split(root, y - 1, a, b);
			split(b, 1, c, d);
			printf("%d\n", c);
			root = merge(merge(a, d), c);
		}
	}
}
namespace Part4
{
	Treap row,col;
	void Solve()
	{
		for (int i = 1; i <= m; ++i)
			row.root = row.merge(row.root, row.newNode(i));
		for (int i = 1; i <= n; ++i)
			col.root = col.merge(col.root, col.newNode(i * m));
		for (int i = 1; i <= q; ++i)
		{
			int x, y;
			read(x), read(y);
			int a, b, c, d, e, f, g, h;
			row.split(row.root, y - 1, a, b);
			row.split(b, 1, c, d);
			printf("%d\n", row.nd[c].val);
			col.split(col.root, 1, e, f);
			col.split(f, 1, g, h);
			row.root = row.merge(row.merge(a,d), row.newNode(col.nd[g].val));
			col.root = col.merge(col.merge(g, h), col.newNode(row.nd[c].val));
		}
		
	}
}

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n), read(m), read(q);
	if (n<=1000&&m<=1000)
		Part1::Solve();
	else if (n==1)
		Part3::Solve();
	else
		Part4::Solve();
	
	return 0;
}


#include <cstdio>
#include <iostream>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <cctype>
typedef long long ll;
using namespace std;
template<class T>
void read(T &x)
{
	x = 0;
	char c = getchar();
	while (!isdigit(c))
		c = getchar();
	while (isdigit(c))
		x = x * 10 + (c & 0xf), c = getchar();
}
int min(int a, int b) {return a < b ? a : b;}
int G[15][15];
int f[15][6096], g[15][6096], h[15];
int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	int n, m;
	read(n);
	read(m);
	memset(G, 0x3f, sizeof G);
	for (int i = 1; i <= m; ++i)
	{
		int u, v, w;
		read(u),
		read(v),
		read(w);
		G[u][v] = min(G[u][v], w);
		G[v][u] = min(G[v][u], w);
	}
	h[1] = 1;
	for (int i = 2; i <= n; ++i)
		h[i] = h[i - 1] << 1;
	const int S = (1 << n);
	memset(f, 0x3f, sizeof f);
	memset(g, 0x3f, sizeof g);
	const int inf = f[0][0];
	for (int i = 1; i <= n; ++i)
	{
		f[i][0] = 0;
		g[i][0] = 0;
		f[i][1<<(i-1)] = 0;
		g[i][1<<(i-1)] = 0;
	}
	for (int i = 1; i < S; ++i)
	{
		for (int j = 1; j <= n; ++j) if (i & h[j])
		{
			int s = S - 1 ^ i;
			for (int k = s; ; k = (k - 1) & s)
			{
				for (int p = 1; p <= n; ++p) if (!(i & h[p]) && (h[p] & k))
				{
					if (f[p][k] != inf)
					{
						if ((ll)f[p][k] + f[j][i] + g[j][i] + G[j][p] < f[p][k | i])
						{
							f[p][k|i] = f[p][k] + f[j][i] + g[j][i] + G[j][p];
							g[p][k|i] = g[p][k] + g[j][i] + G[j][p];
						}
					}
				}
				if (k == 0)
					break;
				
			}	
		}
	}
	int min_val = inf;
	for (int i = 1; i <= n; ++i)
		min_val = min(min_val, f[i][S - 1]);
	printf("%d\n", min_val);
	
	
	return 0;
}


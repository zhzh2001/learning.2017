#include<cstdio>
#include<cstring>
#include<algorithm>
#define inf 0x3f3f3f3f
#define ll long long
#define rep(i,n) for(int i=1;i<=n;++i)
using namespace std;
ll z[20005],r,h,x[20005],y[20005];
bool pd[20005];
int T,n,e,head[20005];
struct re{int v,next;}ed[1024*1024];
ll p(ll x){return x*x;}
bool jud2(int a){return z[a]<=r;}
bool jud3(int a){return h-z[a]<=r;}
bool jud(int a,int b){return p(x[a]-x[b])+p(y[a]-y[b])+p(z[a]-z[b])<=4*r*r;}
void clear(){
	memset(pd,0,sizeof(pd));e=0;
	memset(head,0,sizeof(head));
}
void ins(int x,int y){ed[++e]=(re){y,head[x]};head[x]=e;}
void dfs(int now){
	if(pd[now])return;
	pd[now]=1;if(now==1024)return;
	for(int i=head[now];i;i=ed[i].next)dfs(ed[i].v);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	for(;T--;){
		clear();
		scanf("%d%lld%lld",&n,&h,&r);
		rep(i,n){scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);if(jud2(i))ins(0,i);if(jud3(i))ins(i,1024);}
		rep(i,n-1)for(int j=i+1;j<=n;++j)if(jud(i,j))ins(i,j),ins(j,i);dfs(0);
		puts(pd[1024]?"Yes":"No");
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define inf 0x3f3f3f3f
#define rep(i,n) for(int i=1;i<=n;++i)
using namespace std;
int n;
bool pd[50];
int cnt[50],mp[50][50],wei[50][50],ans,pos[50],m;
bool jud(){
	rep(i,n)if(!pd[i])return 0;
	return 1;
}
void cal(){
	int tmp=0;
	for(int cen=1;cen<=n-1;++cen)
	for(int i=1;i<=cnt[cen];++i){
		int ttmp=inf;
		for(int j=1;j<=cnt[cen-1];++j){
			if(mp[wei[cen][i]][wei[cen-1][j]]!=inf)
			ttmp=min(ttmp,mp[wei[cen][i]][wei[cen-1][j]]*cen);
		}
		if(ttmp==inf)return;
		tmp+=ttmp;
		if(tmp>=ans)return;
	}
	ans=min(ans,tmp);
}
bool cal2(int n){
	int tmp=0;
	for(int cen=1;cen<=n-1;++cen)
	for(int i=1;i<=cnt[cen];++i){
		int ttmp=inf;
		for(int j=1;j<=cnt[cen-1];++j){
			if(mp[wei[cen][i]][wei[cen-1][j]]!=inf)
			ttmp=min(ttmp,mp[wei[cen][i]][wei[cen-1][j]]*cen);
		}
		if(ttmp==inf)return 1;
		tmp+=ttmp;
		if(tmp>=ans)return 1;
	}
	return 0;
}
void dfs(int x,int ceng,int nowv){
	pd[x]=1;pos[x]=ceng;
	cnt[ceng]++;wei[ceng][cnt[ceng]]=x;
	if(jud())//{cal();pd[x]=0;cnt[ceng]--;return;}
	{ans=min(ans,nowv),pd[x]=0;cnt[ceng]--;return;}
//	if(cal2(ceng)){pd[x]=0;cnt[ceng]--;return;}
	if(nowv>=ans){pd[x]=0;cnt[ceng]--;return;}
	for(int i=1;i<=n;++i){
		if(!pd[i]){
//			if(ceng)dfs(i,ceng);dfs(i,ceng+1);
			int ttmp=inf;
			if(ceng)
			for(int j=1;j<=cnt[ceng-1];++j){
				ttmp=min(ttmp,mp[i][wei[ceng-1][j]]*ceng);
			}
			if(ttmp!=inf)dfs(i,ceng,nowv+ttmp);
			ttmp=inf;
			for(int j=1;j<=cnt[ceng];++j){
				ttmp=min(ttmp,mp[i][wei[ceng][j]]*(ceng+1));
			}
			if(ttmp!=inf)dfs(i,ceng+1,nowv+ttmp);
		}
	}
	pd[x]=0;cnt[ceng]--;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(mp,inf,sizeof(mp));
	rep(i,m){int x,y,z;scanf("%d%d%d",&x,&y,&z);mp[y][x]=mp[x][y]=min(mp[x][y],z);}
	ans=inf;
	rep(i,n){
		memset(pd,0,sizeof(pd));
		memset(cnt,0,sizeof(cnt));
		dfs(i,0,0);
	}
	printf("%d\n",ans);
	return 0;
}


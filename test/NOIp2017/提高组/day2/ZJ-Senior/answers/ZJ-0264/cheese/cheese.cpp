#include<cstdio>
#include<cstring>
#include<cmath>
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
int T;
long long n,h,r;
int x[2001],y[2001],z[2001];
int con[2001][2001];
int f[2001];
int dl[3001];
double X,Y,Z,dis;
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n),read(h),read(r);
		memset(con,0,sizeof(con));
		memset(f,0,sizeof(f));
		for(register int i=1;i<=n;i++)
		{
			read(x[i]),read(y[i]),read(z[i]);
			if(z[i]<=r)con[0][i]=1;
			if(z[i]>=h-r)con[i][n+1]=1;
		}
		for(register int i=1;i<=n;i++)
			for(register int j=i+1;j<=n;j++)
			{
				X=x[i]-x[j],Y=y[i]-y[j],Z=z[i]-z[j];
				if(X==0&&Y==0&&Z==0)
				{
					con[i][j]=con[j][i]=1;
					continue;
				}
				dis=sqrt(X*X+Y*Y+Z*Z);
				if(dis<=r*2)con[i][j]=con[j][i]=1;
			}
		f[0]=1;
		dl[1]=0;
		int h=0,e=1;
		while(h<e)
		{
			h++;
			int u=dl[h];
			for(register int i=0;i<=n+1;i++)
				if(con[u][i]&&!f[i])f[i]=1,dl[++e]=i;
		}
		printf("%s\n",f[n+1]?"Yes":"No");
	}
	return 0;
}

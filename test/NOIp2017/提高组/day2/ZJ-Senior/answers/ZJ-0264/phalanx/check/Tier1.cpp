#include<cstdio>
#include<cstring>
#include<vector>
using namespace std;
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
int num[2001][2001];
int main()
{
	freopen("input.in","r",stdin);
	freopen("tier1.out","w",stdout);
	int n,m,q;
	read(n),read(m),read(q);
	vector<int>nums;
	for(register int i=1;i<=m;i++)
		nums.push_back(i);
	while(q--)
	{
		int t;
		read(t),read(t);
		printfn(nums[t-1]);
		nums.push_back(nums[t-1]);
		nums.erase(nums.begin()+t-1);
	}
	return 0;
}

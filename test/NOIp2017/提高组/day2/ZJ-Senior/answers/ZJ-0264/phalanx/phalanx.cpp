#include<cstdio>
#include<cstring>
#include<vector>
using namespace std;
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
long long num[2001][2001];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q;
	read(n),read(m),read(q);
	//Tier 1
	if(n==1)
	{
		vector<int>nums;
		for(register int i=1;i<=m;i++)
			nums.push_back(i);
		while(q--)
		{
			int t;
			read(t),read(t);
			printfn(nums[t-1]);
			nums.push_back(nums[t-1]);
			nums.erase(nums.begin()+t-1);
		}
		return 0;
	}
	//Tier 2
	if((long long)q*(m+n)>100000000)
	{
		vector<long long>numa,numb;
		for(register int i=1;i<m;i++)
			numa.push_back(i);
		for(register int i=1;i<=n;i++)
			numb.push_back(m*i);
		while(q--)
		{
			int t;
			read(t),read(t);
			if(t==m)
			{
				printfn(numb[0]);
				numb.push_back(numb[0]);
				numb.erase(numb.begin());
				continue;
			}
			printfn(numa[t-1]);
			numb.push_back(numa[t-1]);
			numa.erase(numa.begin()+t-1);
			numa.push_back(numb[0]);
			numb.erase(numb.begin());
		}
		return 0;
	}
	//Tier 3
	long long cnt=0;
		for(register int i=1;i<=n;i++)
			for(register int j=1;j<=m;j++)
				num[i][j]=++cnt;
	while(q--)
	{
		int a,b;
		read(a),read(b);
		printfn(num[a][b]);
		long long tmp=num[a][b];
		for(register int i=b;i<m;i++)
			num[a][i]=num[a][i+1];
		for(register int i=a;i<n;i++)
			num[i][m]=num[i+1][m];
		num[n][m]=tmp;
	}
	return 0;
}

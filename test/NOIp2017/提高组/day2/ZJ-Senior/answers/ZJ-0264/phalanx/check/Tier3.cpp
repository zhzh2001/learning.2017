#include<cstdio>
#include<cstring>
#include<vector>
using namespace std;
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
int num[2001][2001];
int main()
{
	freopen("input.in","r",stdin);
	freopen("tier3.out","w",stdout);
	int n,m,q;
	read(n),read(m),read(q);
	long long cnt=0;
		for(register int i=1;i<=n;i++)
			for(register int j=1;j<=m;j++)
				num[i][j]=++cnt;
	while(q--)
	{
		int a,b;
		read(a),read(b);
		printfn(num[a][b]);
		int tmp=num[a][b];
		for(register int i=b;i<m;i++)
			num[a][i]=num[a][i+1];
		for(register int i=a;i<n;i++)
			num[i][m]=num[i+1][m];
		num[n][m]=tmp;
	}
}

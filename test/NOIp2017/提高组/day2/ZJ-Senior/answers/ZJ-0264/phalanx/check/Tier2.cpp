#include<cstdio>
#include<cstring>
#include<vector>
using namespace std;
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
int num[2001][2001];
int main()
{
	freopen("input.in","r",stdin);
	freopen("tier2.out","w",stdout);
	int n,m,q;
	read(n),read(m),read(q);
	vector<int>numa,numb;
	for(register int i=1;i<m;i++)
		numa.push_back(i);
	for(register int i=1;i<=n;i++)
		numb.push_back(m*i);
	while(q--)
	{
		int t;
		read(t),read(t);
		if(t==m)
		{
			printfn(numb[0]);
			numb.push_back(numb[0]);
			numb.erase(numb.begin());
			continue;
		}
		printfn(numa[t-1]);
		numb.push_back(numa[t-1]);
		numa.erase(numa.begin()+t-1);
		numa.push_back(numb[0]);
		numb.erase(numb.begin());
	}
	return 0;
}

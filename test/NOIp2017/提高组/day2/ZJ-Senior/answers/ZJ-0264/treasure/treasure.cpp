#include<cstdio>
#include<cstring>
template<typename T>inline T min(const T a,const T b){return a>b?b:a;}
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
int n,m;
/*void print2(int a)
{
	int num[100],l=0;
	memset(num,0,sizeof(num));
	while(a>0)
		num[++l]=a&1,a/=2;
	for(int i=n;i>=1;i--)
		printf("%d",num[i]);
	printf("\n");
}*/
int tof[10001][21];
int cos[10001];
int dl[200001];
int dis[21][21];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(cos,-1,sizeof(cos));
	memset(dis,-1,sizeof(dis));
	read(n),read(m);
	for(register int i=1;i<=n;i++)
		cos[1<<(i-1)]=0,tof[1<<(i-1)][i]=1;
	for(register int i=1;i<=m;i++)
	{
		int a,b,c;
		read(a),read(b),read(c);
		if(dis[a][b]==-1)dis[a][b]=c;
		else dis[a][b]=min(dis[a][b],c);
		if(dis[b][a]==-1)dis[b][a]=c;
		else dis[b][a]=min(dis[b][a],c);
	}
	for(register int ADF=1;ADF<=n;ADF++)
	{
		dl[1]=1<<(ADF-1);
		int h=0,e=1;
		while(h<e)
		{
			h++;
			const int sta=dl[h];
//			print2(sta);
//			printfn(cos[sta]);
			for(register int i=1;i<=n;i++)
			{
				if((sta&(1<<(i-1)))==0)continue;
//				printf("%d\n",i);
				for(register int j=1;j<=n;j++)
				{
					if(dis[i][j]>0&&(sta&(1<<(j-1)))==0)
					{
						const int k=sta|(1<<(j-1));
						if(cos[k]==-1||cos[k]>cos[sta]+tof[sta][i]*dis[i][j])
						{
							cos[k]=cos[sta]+tof[sta][i]*dis[i][j];
							memcpy(tof[k],tof[sta],sizeof(tof[sta]));
							tof[k][j]=tof[sta][i]+1;
							dl[++e]=k;
						}
					}
				}
			}
		}
	}
	printfn(cos[(1<<n)-1]);
	return 0;
}

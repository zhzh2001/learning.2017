
#include<cstdio>
#include<cstring>
template<typename T>inline T min(const T a,const T b){return a>b?b:a;}
template<typename T>inline void read(T &a)
{
	T ret=0,t=1;
	char ch=getchar();
	if(ch=='-')t=-1;
	while(ch<'0'||ch>'9')
	{
		ch=getchar();
		if(ch=='-')t=-1;
	}
	while(ch>='0'&&ch<='9')
		ret*=10,ret+=ch-'0',ch=getchar();
	a=ret*t;
}
template<typename T>inline void print(T a)
{
	if(a<0)putchar('-'),a=-a;
	if(a>=10)print(a/10);
	putchar('0'+a%10);
}
template<typename T>inline void printfn(const T a){print(a);putchar('\n');}
int dis[21][21];
int n,m;
int f[21],roads[21];
int dl[201];
int inque[201];
int father[201];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n),read(m);
	memset(dis,-1,sizeof(dis));
	while(m--)
	{
		int a,b,c;
		read(a),read(b),read(c);
		if(dis[a][b]==-1)dis[a][b]=c;
		else dis[a][b]=min(dis[a][b],c);
		if(dis[b][a]==-1)dis[b][a]=c;
		else dis[b][a]=min(dis[b][a],c);
	}
	long long ANS=1<<30;
	for(int K=1;K<=n;K++)
	{
	memset(f,-1,sizeof(f));
	f[K]=0;
	roads[K]=1;
	dl[1]=K;
	int h=0,e=1;
//	for(register int i=1;i<=n;i++)
//	{
//		for(register int j=1;j<=n;j++)
//			printf("dis[%d][%d]=%d ",i,j,dis[i][j]);
//		putchar('\n');
//	}
	while(h<e)
	{
		h++;
		int u=dl[h];
		inque[u]=0;
		for(register int i=1;i<=n;i++)
		{
			if(i==father[u])continue;
			if(dis[u][i]>0&&(f[i]<0||f[i]>dis[u][i]*roads[u]))
			{
				f[i]=dis[u][i]*roads[u];
//				printf("u=%d i=%d f[i]=%d dis[u][i]=%d roads[u]=%d\n",u,i,f[i],dis[u][i],roads[u]);
				roads[i]=roads[u]+1;
				father[i]=u;
				if(!inque[i])dl[++e]=i;
				inque[i]=1;
			}
		}
	}
	long long ans=0;
	for(int i=1;i<=n;i++)
		ans+=f[i];
	ANS=min(ANS,ans);
}
printfn(ANS);
}

var
  n,m,q,i,j,x,y,t:longint;
  f:array[0..1005,0..1005] of longint;
begin
  assign(input,'phalanx.in');reset(input);
  assign(output,'phalanx.out');rewrite(output);
  readln(n,m,q);
  for i:=1 to n do
    for j:=1 to m do
      f[i,j]:=(i-1)*m+j;
  for i:=1 to q do
  begin
    readln(x,y);
    writeln(f[x,y]);
    t:=f[x,y];
    for j:=y+1 to m do f[x,j-1]:=f[x,j];
    for j:=x+1 to n do f[j-1,m]:=f[j,m];
    f[n,m]:=t;
  end;
  close(input);close(output);
end.

var
  x,y,z:array[0..1005] of int64;
  dist:extended;e:array[0..1005] of longint;
  f:array[0..1005,0..1005] of longint;
  n,i,j,t:longint;h,r:int64;flag:boolean;
  b:array[0..1005] of boolean;
procedure dfs(k:longint);
var i:longint;
begin
  if abs(z[k]-h)<=r then
  begin
    flag:=true;
    exit;
  end;
  b[k]:=false;
  for i:=1 to e[k] do
    if b[f[k,i]] then
    begin
      dfs(f[k,i]);
      if flag then exit;
    end;
end;
begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  readln(t);
  while t>0 do
  begin
    dec(t);
    fillchar(e,sizeof(e),0);
    readln(n,h,r);
    flag:=false;
    for i:=1 to n do readln(x[i],y[i],z[i]);
    for i:=1 to n do
      for j:=i+1 to n do
        if i<>j then
        begin
          dist:=sqrt(sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]));
          if dist<=r*2 then
          begin
            inc(e[i]);
            f[i,e[i]]:=j;
            inc(e[j]);
            f[j,e[j]]:=i;
          end;
        end;
    fillchar(b,sizeof(b),true);
    for i:=1 to n do
    begin
      if (z[i]<=r)and(b[i]) then dfs(i);
      if flag then break;
    end;
    if flag then writeln('Yes') else writeln('No');
  end;
  close(input);close(output);
end.
var
  sum,n,u,v,d,i,m:longint;min,value:int64;
  dist:array[1..12,1..12] of int64;
  e,t,step:array[1..12] of longint;
  f:array[1..12,1..12] of longint;
  flag:array[1..12] of boolean;
  b:array[1..12,1..12] of boolean;
procedure dfs(k:longint);
var i,minx,j,miny:longint;minans:int64;
begin
  if sum=n-1 then
  begin
    if value<min then min:=value;
    exit;
  end;
  flag[k]:=false;
  inc(sum);
  t[sum]:=k;minans:=maxlongint;
  for i:=1 to sum do
  begin
    for j:=1 to e[t[i]] do
      if flag[f[t[i],j]] then
        if dist[t[i],f[t[i],j]]*step[t[i]]<minans then
        begin
          minans:=dist[t[i],f[t[i],j]]*step[t[i]];
          minx:=f[t[i],j];miny:=t[i];
        end;
  end;
  step[minx]:=step[miny]+1;
  value:=value+minans;
  dfs(minx);
  value:=value-minans;
  dec(sum);
end;
begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);
  readln(n,m);
  for i:=1 to m do
  begin
    readln(u,v,d);
    if not(b[u,v]) then
    begin
      b[u,v]:=true;
      b[v,u]:=true;
      inc(e[u]);
      f[u,e[u]]:=v;
      inc(e[v]);
      f[v,e[v]]:=u;
      dist[u,v]:=d;
      dist[v,u]:=d;
    end else
    if d<dist[u,v] then
    begin
      dist[u,v]:=d;
      dist[v,u]:=d;
    end;
  end;
  min:=maxlongint*maxlongint;
  for i:=1 to n do
  begin
    fillchar(flag,sizeof(flag),true);
    fillchar(step,sizeof(step),0);
    step[i]:=1;
    dfs(i);
  end;
  writeln(min);
  close(input);close(output);
end.
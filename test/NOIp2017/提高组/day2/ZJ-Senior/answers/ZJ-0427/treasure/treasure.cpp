#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#define MAXN 14
#define MAXM 
#define INF 1000000000
using namespace std;
inline int read()
{
	int x=0,w=1; char ch=0;
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1; ch=getchar();}
	while (ch<='9'&&ch>='0') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x*w;
}
struct jd{int x,cc;};
queue<jd> q;
int map[MAXN][MAXN],n,m,x,y,z,ans=INF;
bool vis[MAXN];
void bfs(int x)
{
	int sum=0;
	memset(vis,0,sizeof(vis));
	jd sta;
	sta.x=x;
	sta.cc=1;
	q.push(sta);
	vis[x]=1;
	while (!q.empty())
	{
		jd u=q.front();
		q.pop();
		for (int i=1;i<=n;i++)
		  if (map[u.x][i]!=INF&&!vis[i])
		  {
		  	sum+=map[u.x][i]*u.cc;
		    jd v;
		    v.x=i;
		    v.cc=u.cc+1;
		    vis[i]=1;
		    q.push(v);
			}
	}
	if (sum<ans) ans=sum;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=n;i++)
	  for (int j=1;j<=n;j++) map[i][j]=INF;
	for (int i=1;i<=m;i++)
	{
		x=read(); y=read(); z=read();
		if (z<map[x][y])
		{
			map[x][y]=z;
			map[y][x]=z;
		}	
	}
	for (int i=1;i<=n;i++) bfs(i);
	cout<<ans;
}


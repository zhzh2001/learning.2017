#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#define MAXN 1001
#define MAXN2 600001
using namespace std;
inline int read()
{
	int x=0,w=1; char ch=0;
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1; ch=getchar();}
	while (ch<='9'&&ch>='0') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x*w;
}
int x,y,n,m,q;
int que[MAXN2],a[MAXN2];
int search(int x)
{
	int ans=0;
	while (x>0)
	{
		ans+=a[x];
		x=x-(x&(-x));
	}
	return ans;
}
void add(int x)
{
	while (x<m)
	{
		a[x]++;
		x=x+(x&(-x));
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();
	m=read();
	q=read();
	if (n!=1)
	{
		int map[n+1][m+1];
		for (int i=1;i<=n;i++)
		  for (int j=1;j<=m;j++) map[i][j]=(i-1)*m+j;
	  for (int i=1;i<=q;i++)
	  {
	    x=read(); y=read();
	    int num=map[x][y];
	    printf("%d\n",num);
	    for (int j=y;j<=m-1;j++) map[x][j]=map[x][j+1];
	    for (int j=x;j<=n-1;j++) map[j][m]=map[j+1][m];
	    map[n][m]=num;
	  }
	}
	else 
	{
		for (int i=1;i<=m;i++) que[i]=i;
		for (int i=1;i<=q;i++)
		{
			x=read(); 
			y=read();
			int num=que[search(y)+y];
			printf("%d\n",num);
			add(y);
			que[++m]=num;
		}
	}
}

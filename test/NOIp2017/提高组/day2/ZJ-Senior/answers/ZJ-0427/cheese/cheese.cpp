#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#define MAXN 1010
#define MAXM 2000010
using namespace std;
inline int read()
{
	int x=0,w=1; char ch=0;
	while (ch<'0'||ch>'9') {if (ch=='-') w=-1; ch=getchar();}
	while (ch<='9'&&ch>='0') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x*w;
}
int t,n,h,r;
int edge,first[MAXN],next[MAXM],to[MAXM];
int x[MAXN],y[MAXN],z[MAXN];
bool vis[MAXN],flag;
void add(int x,int y)
{
	to[++edge]=y;
	next[edge]=first[x];
	first[x]=edge;
}
void link(int i,int j)
{
	if (sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]))<=2*r)
	  add(i,j),add(j,i);
}
void dfs(int x)
{
	if (x==n+1) flag=1;
	if (flag) return;
	vis[x]=1;
	for (int i=first[x];i;i=next[i])
	  if (!vis[to[i]])
	    dfs(to[i]);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=read();
	for (int ii=1;ii<=t;ii++)
	{
		n=read(); h=read(); r=read();
		for (int i=1;i<=n;i++)
		  x[i]=read(),y[i]=read(),z[i]=read();
		for (int i=1;i<=n;i++)
		  for (int j=i+1;j<=n;j++)
		  	link(i,j);
		for (int i=1;i<=n;i++)
		{
			if (z[i]<=r) add(0,i);
			if (h-z[i]<=r) add(i,n+1);
		}
		flag=0;
		dfs(0);
		if (flag) printf("Yes\n");
		else printf("No\n");
		for (int i=1;i<=edge;i++)
		  next[i]=0,to[i]=0;
		memset(vis,0,sizeof(vis));
		memset(first,0,sizeof(first));
		edge=0;
	}
	return 0;
}

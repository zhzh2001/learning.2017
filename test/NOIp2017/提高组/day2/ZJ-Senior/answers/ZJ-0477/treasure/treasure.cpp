#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,i,j,k,p,l,t,f[13][13][4096],d[13][13],pc[13];
inline void read(int &x)
{
	int ff=1; char ch=getchar(); x=0;
	while ((ch<'0'||ch>'9')&&(ch!='-')) ch=getchar();
	if (ch=='-') ff=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
int min(int x,int y) {if (x>y) return y; else return x;}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int flag=0;
	read(n); read(m);
	for (i=1;i<=m;i++)
	{
		int x,y,z;
		read(x); read(y); read(z);
		if (x==y) continue;
		if (!d[x][y]||d[x][y]>z) d[x][y]=d[y][x]=z;
	}
	memset(f,60,sizeof(f));
	for (i=1;i<=n;i++)
		for (j=1;j<=n;j++)
			f[i][j][(1<<(i-1))]=0;
	for (int s=1;s<(1<<n);s++)
	{
		for (i=1;i<=n;i++)
		if (s&(1<<(i-1)))
		{
			for (j=1;j<=n-1;j++)
			if (f[i][j][s]!=f[0][0][0])
			{
				for (k=1;k<=n;k++)
				if (d[k][i]&&!(s&(1<<(k-1))))
				{
					int cnt=0;
					for (p=1;p<=n;p++)
						if (!(s&(1<<(p-1)))&&p!=k) pc[++cnt]=p;
					for (p=0;p<(1<<cnt);p++)
					{
						int st=1<<(k-1);
						for (int dd=1;dd<=cnt;dd++)
						if (p&(1<<(dd-1))) st+=(1<<(pc[dd]-1));
						f[k][j-1][s^st]=min(f[k][j-1][s^st],f[i][j][s]+f[k][j-1][st]+d[k][i]*(j-1));
					}
				}
			}
		}
	}
	int ans=f[0][0][0];
	for (i=1;i<=n;i++) ans=min(ans,f[i][1][(1<<n)-1]);
	printf("%d\n",ans);
}

#include<cmath>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const double eps=1e-7;
const long long N=1005;
long long n,h,r,i,j,k,p,l,t,len,q[N],first[N],x[N],y[N],z[N],b[N*N*2],nxt[N*N*2];
bool bo[N];
inline void read(long long &x)
{
	long long ff=1; char ch=getchar(); x=0;
	while ((ch<'0'||ch>'9')&&(ch!='-')) ch=getchar();
	if (ch=='-') ff=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-'0',ch=getchar();
	x*=ff;
}
void add(long long x,long long y){ b[++len]=y; nxt[len]=first[x]; first[x]=len;}
bool bfs()
{
	memset(bo,0,sizeof(bo));
	long long i,h=0,t=1;
	bo[1]=1; q[1]=1;
	do
	{
		h++;
		for (i=first[q[h]];i;i=nxt[i])
		if (!bo[b[i]])
		{
			bo[b[i]]=1;
			q[++t]=b[i];
			if (b[i]==n+2) return 1;
		}
	}while (!(h>=t));
	return 0;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	long long T;
	read(T);
	while (T--)
	{
		len=0;
		memset(first,0,sizeof(first));
		read(n); read(h); read(r);
		for (i=1;i<=n;i++)
			read(x[i]),read(y[i]),read(z[i]);
		for (i=1;i<=n;i++)
			for (j=i+1;j<=n;j++)
			{
				double xx=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
				if (xx-2*r<=eps) add(i+1,j+1),add(j+1,i+1);
			}
		for (i=1;i<=n;i++)
			if (z[i]-r<=0) add(1,i+1),add(i+1,1);
		for (i=1;i<=n;i++)
			if (z[i]+r>=h) add(i+1,n+2),add(n+2,i+1);
		if (bfs()) printf("Yes\n");
		else printf("No\n");
	}
}

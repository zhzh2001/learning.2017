#include<cstdio>

#define to e[i].v
#define N 1010
#define ll long long

struct node{
	ll v,next;
}e[N*N*2];

struct Node{
	ll x,y,z;
}a[N];

ll ok,head[N],vis[N],T,n,h,r,s,t,id,l,rr,q[N*N*2];

ll read()
{
	ll x=0,f=1; char ch=getchar();
	for (;ch>'9' || ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

void add(ll u,ll v) {e[id].v=v; e[id].next=head[u]; head[u]=id++;}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	for (;T--;)
	{
		n=read(),h=read(),r=read(),s=n+1,t=n+2,id=1;
		for (ll i=0;i<=n+2;i++) head[i]=0,vis[i]=0;
		for (ll i=1;i<=n;i++) a[i].x=read(),a[i].y=read(),a[i].z=read();
		for (ll i=1;i<=n-1;i++)
		for (ll j=i+1;j<=n;j++) if ((a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z)<=4*r*r) add(i,j),add(j,i);
		for (ll i=1;i<=n;i++)
		{
			if (a[i].z<=r) add(s,i); 
			if (h-a[i].z<=r) add(i,t);
		}
		l=0; rr=1; q[1]=s; ok=0; vis[s]=1;
		for (;l^rr;)
		{
			ll u=q[++l]; if (u==t) {ok=1; break;}
			for (ll i=head[u];i;i=e[i].next)
				if (!vis[to]) {vis[to]=1; q[++rr]=to;} 
		}
		if (ok) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#define N 20
#define M 4020
#define to e[i].v

struct node{
	int v,w,next;
}e[M];

int id,head[N],deep[N],n,m,ans,W;

int read()
{
	int x=0,f=1; char ch=getchar();
	for (;ch>'9' || ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

int min(int a,int b) {return a>b? b:a;}

void add(int u,int v,int w) {e[id].v=v; e[id].w=w; e[id].next=head[u]; head[u]=id++;}

void dfs(int u,int fa)
{
	for (int i=head[u];i;i=e[i].next)
	if (to^fa)
	{
		deep[to]=deep[u]+1;
		dfs(to,u);
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(); m=read(); id=1;
	for (int i=1;i<=m;i++)
	{
		int u=read(),v=read(),w=read(); W=w;
		add(u,v,w); add(v,u,w); 
	}
	ans=1000000000;
	for (int u=1;u<=n;u++)
	{
		for (int i=0;i<=n;i++) deep[i]=0; 
		dfs(u,0);
		int now=0;
		for (int i=1;i<=n;i++) now+=deep[i];
		ans=min(ans,now); 
	}
	printf("%d",ans*W);
	return 0;
}

#include<cstdio>

#define ll long long

int n,m,q,a[1010][1010],b[300010];
ll d[600010],L;

int read()
{
	int x=0,f=1; char ch=getchar();
	for (;ch>'9' || ch<'0';ch=getchar()) if (ch=='-') f=-1;
	for (;ch>='0' && ch<='9';ch=getchar()) x=x*10+ch-'0';
	return x*f;
}

void work1()
{
	for (int i=1;i<=n;i++)
	for (int j=1;j<=m;j++)
		a[i][j]=(i*m-m+j);
	for (int i=1;i<=q;i++)
	{
		int x=read(),y=read(),p=a[x][y];
		printf("%d\n",p);
		for (int k=y;k<m;k++) a[x][k]=a[x][k+1];
		for (int k=x;k<n;k++) a[k][m]=a[k+1][m];
		a[n][m]=p;
	}
}

void work2()
{
	for (int i=1;i<=m;i++) b[i]=i;
	for (int i=1;i<=q;i++)
	{
		int x=read(),y=read(),p=b[y];
		printf("%d\n",p);
		for (int k=y;k<m;k++) b[k]=b[k+1];
		b[m]=p;
	}
}

void insert(int x,ll w) {for (;x<=L;x+=x&-x) d[x]+=w;}
ll query(int x) {ll sum=0; for (;x;x-=x&-x) sum+=d[x]; return sum;}

void work4()
{
	L=n+m-1; 
	for (ll i=1;i<=m;i++) insert(i,i),insert(i+1,-i); 
	for (ll i=m+1;i<=L;i++) insert(i,(i-m)*m+m),insert(i+1,-(i-m)*m-m);
	for (ll i=1;i<=q;i++)
	{
		ll x=read(),y=read(),g=x+y-1,p=query(g),qq=query(L);
		insert(g,1); insert(m,-1); insert(m,m); insert(L,-m); insert(L,p-qq); 
		printf("%lld\n",p);
	}
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (n<=1000 && m<=1000) work1();
	else if (n==1) work2();
	else work4();
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#define MAXN 5005
using namespace std;
int map[MAXN][MAXN];
int Read(){
    int X=0,flag=1;char ch=0;
    while (ch<'0'||ch>'9'){
	  if (ch=='-') flag=-1;
	  ch=getchar();
	}
    while (ch>='0'&&ch<='9')
	  X=X*10+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int main(){
    freopen("phalanx.in","r",stdin);
    freopen("phalanx.out","w",stdout);
    int n=Read(),m=Read(),q=Read();
    for (int i=1;i<=n;i++)
      for (int j=1;j<=m;j++)
        map[i][j]=(i-1)*m+j;
    while (q--){
      int x=Read(),y=Read();
      int tmp=map[x][y];
      Write(tmp);putchar('\n');
      for (int i=y;i<m;i++) map[x][i]=map[x][i+1];
      for (int i=x;i<n;i++) map[i][m]=map[i+1][m];
      map[n][m]=tmp;
    }
    fclose(stdin);fclose(stdout);
	return 0;
}

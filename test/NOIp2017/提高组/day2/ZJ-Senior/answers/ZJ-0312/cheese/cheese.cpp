#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#define MAXN 1005
using namespace std;
struct Node{
	int x,y,z;
} Coor[MAXN];
int n,h,r,father[MAXN];
bool ok1[MAXN],ok2[MAXN];
int Read(){
    int X=0,flag=1;char ch=0;
    while (ch<'0'||ch>'9'){
	  if (ch=='-') flag=-1;
	  ch=getchar();
	}
    while (ch>='0'&&ch<='9')
	  X=X*10+ch-48,ch=getchar();
	return X*flag;
}
bool calc(int a,int b){
	double dist=sqrt(((double)(Coor[a].x-Coor[b].x))*((double)(Coor[a].x-Coor[b].x))+
	                 ((double)(Coor[a].y-Coor[b].y))*((double)(Coor[a].y-Coor[b].y))+
					 ((double)(Coor[a].z-Coor[b].z))*((double)(Coor[a].z-Coor[b].z)));
	return dist<=((double)(r*2));
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int find(int x){
	if (father[x]!=x) father[x]=find(father[x]);
	return father[x];
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=Read();
	while (T--){
		n=Read(),h=Read(),r=Read();
        memset(Coor,0,sizeof(Coor));
        for (int i=1;i<=n;i++)
          Coor[i].x=Read(),Coor[i].y=Read(),Coor[i].z=Read();
        for (int i=1;i<=n;i++) father[i]=i;
        for (int i=1;i<=n;i++)
          for (int j=1;j<=n;j++)
            if (calc(i,j))
              father[find(i)]=find(j);
        memset(ok1,0,sizeof(ok1));
        memset(ok2,0,sizeof(ok2));
        bool flag1=0,flag2=0;
        for (int i=1;i<=n;i++)
          if (Coor[i].z<=r) ok1[i]=1,flag1=1;
        for (int i=1;i<=n;i++)
          if ((h-Coor[i].z)<=r) ok2[i]=1,flag2=1;
        if (!flag1||!flag2){
    	  printf("No\n");
    	  continue;
	    }
	    bool flag=0;
	    for (int i=1;i<=n;i++){
		  if (flag) break;
		  if (ok1[i])
		    for (int j=1;j<=n;j++)
		      if (ok2[j]){
		    	if (find(i)==find(j)){
		    		flag=true;break;
				}
			}
	    }  
	    if (flag) printf("Yes\n");
	    else printf("No\n");
	}
    fclose(stdin);fclose(stdout);
	return 0;
}


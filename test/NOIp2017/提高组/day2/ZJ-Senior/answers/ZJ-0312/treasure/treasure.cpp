#include<cstdio>
#include<iostream>
#include<cmath>
#include<cstring>
#include<vector>
#define INF 2e9
#define MAXN 20
using namespace std;
struct Node{
	int todot,weight;
};
struct MinNode{
	int dist,dotnum;
} MinDist[MAXN];
vector<Node> G[MAXN];
bool Vis[MAXN];
int Read(){
    int X=0,flag=1;char ch=0;
    while (ch<'0'||ch>'9'){
	  if (ch=='-') flag=-1;
	  ch=getchar();
	}
    while (ch>='0'&&ch<='9')
	  X=X*10+ch-48,ch=getchar();
	return X*flag;
}
void Write(int x){
	if (x<0) putchar('-'),x=-x;
	if (x>9) Write(x/10);
	putchar(x%10+48);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
    int n=Read(),m=Read();
    for (int i=1;i<=m;i++){
    	int x=Read(),y=Read(),w=Read();
    	G[x].push_back((Node){y,w});
    	G[y].push_back((Node){x,w});
	}
	int Minans=INF;
    for (int i=1;i<=n;i++){
    	memset(Vis,0,sizeof(Vis));
    	for (int j=0;j<=n;j++) MinDist[j].dist=INF;
    	MinDist[i].dist=MinDist[i].dotnum=0;
    	int ans=0;
    	for (int j=1;j<=n;j++){
    		int pos=0;
    		for (int k=1;k<=n;k++)
    		  if (!Vis[k]&&MinDist[k].dist<MinDist[pos].dist)
    		    pos=k;
    		ans+=MinDist[pos].dist;Vis[pos]=1;
    		for (int k=0;k<G[pos].size();k++)
    		  if (!Vis[G[pos][k].todot]&&(MinDist[pos].dotnum+1)*G[pos][k].weight<MinDist[G[pos][k].todot].dist)
    		    MinDist[G[pos][k].todot].dist=(MinDist[pos].dotnum+1)*G[pos][k].weight,
				MinDist[G[pos][k].todot].dotnum=MinDist[pos].dotnum+1; 
		}
		Minans=min(Minans,ans);
	}
	Write(Minans);putchar('\n');
	fclose(stdin);fclose(stdout);
	return 0;
}


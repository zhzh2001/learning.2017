#include <bits/stdc++.h>
using namespace std;

#define maxn 100005
#define maxm 100005
#define MOD 1000000007
#define INF 2000000000
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)--)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
#define pii pair<int, int>
/*----------------------split line----------------------------*/

int main(){
	freopen("phalanx.in","w",stdout);
	srand(time(0));
	puts("1 7 5");
	FORP(i, 1, 5) printf("%d %d\n", 1, rand()%5 + 1);
}

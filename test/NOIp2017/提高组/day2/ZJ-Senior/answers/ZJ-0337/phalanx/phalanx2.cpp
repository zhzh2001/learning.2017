#include <bits/stdc++.h>
using namespace std;

#define maxn 1000005
#define maxm 100005
#define MOD 1000000007
#define INF 2000000000
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)--)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
#define pii pair<int, int>
/*----------------------split line----------------------------*/
struct Query{
	int x, y;
	void init(){scanf("%d%d",&x, &y);}
}query[maxn];
int cnt = 0, root = 0;
int sz[maxn], ch[maxn][2], fa[maxn], v[maxn];
int n, m, q;
void check(int x){
	if (ch[x][0]) {check(ch[x][0]);}
	printf("%d ", v[x]);
	if (ch[x][1]) check(ch[x][1]);
}
void updata(int x){	sz[x] = 1 + sz[ch[x][0]] + sz[ch[x][1]];}
void rotate(int x){
	int p = fa[x], q = fa[p], d = (ch[p][1] == x);
	fa[ch[p][d] = ch[x][d ^ 1]] = p; updata(p);
	fa[ch[x][d^1] = p] = x; updata(x); 
	updata(p);
	fa[x] = q;
	if (!q) root = x;
	else {
		if (ch[q][1] == p) ch[q][1] = x;
		if (ch[q][0] == p) ch[q][0] = x;
	}
}
void splay(int x, int aim = 0){
	for (int y; (y = fa[x]) != aim; rotate(x))
		if (fa[y] && ((ch[fa[y]][0] == y) == (ch[y][0] == x))) rotate(y);
}
void del(){
	int x = root;
	if (!ch[x][0]) {fa[root = ch[x][1]] = 0; return;}
	if (!ch[x][1]) {fa[root = ch[x][0]] = 0; return;}
	
	int y = ch[x][1], t = ch[x][0];
	while (ch[y][0]) y = ch[y][0]; splay(y, x);
	fa[t] = y; ch[y][0] = t; fa[y] = 0; updata(y); root = y;
}
int find(int k){
	int x = root;
	while (k){
		if (k == sz[ch[x][0]] + 1) {splay(x); return v[x];}
		if (k > sz[ch[x][0]] + 1) k -= (sz[ch[x][0]] + 1), x = ch[x][1];
		else x = ch[x][0];
	}
}
void insert(int &x, int var){
	if (!x) {root = x; x = ++cnt, fa[x] = 0, v[cnt] = var; sz[x] = 1; return;}
	int i = x;
	while (1){
		sz[i]++; 
		if (!ch[i][1]) {
			ch[i][1] = ++cnt; fa[cnt] = i; v[cnt] = var; 
			sz[cnt] = 1; splay(cnt); 
			return;
		}
		else i = ch[i][1];
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	//freopen("tmp2.out","w",stdout);
	scanf("%d%d%d",&n, &m, &q);
	bool flag = true;
	FORP(i, 1, q) query[i].init();
	FORP(i, 1, q) if (query[i].x != 1) flag = false;
	if (flag){
		if (n == 1) FORP(i, 1, m) insert(root, i);
		else {
			FORP(i, 1, m) insert(root, i); 
			for (int i = m + m; i <= n * m; i += m) insert(root, i);
		}
	}
	FORP(i, 1, q){
		int t = find(query[i].y);
		printf("%d\n", t);
		del();
		insert(root, t);
		//check(root);puts("");
	}
}

#include <bits/stdc++.h>
using namespace std;

#define maxn 10005
#define maxm 100005
#define MOD 1000000007
#define INF 1061109567
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)--)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
#define pii pair<int, int>
/*----------------------split line----------------------------*/
int dp[maxn][13], sum[maxn][13], g[13][13];
int dis[maxn][13][13];
int n, m;
bool checkin(int x, int y){
	return (1 << (x - 1)) & y;
}
void init(){
	memset(dp, 0x3f, sizeof(dp));
	memset(g, 0x3f, sizeof(g));
	scanf("%d%d",&n, &m);
	FORP(i, 1, m) {
		int x, y, z; scanf("%d%d%d", &x, &y, &z);
		g[x][y] = min(g[x][y], z); g[y][x] = min(g[y][x], z);
	}
	FORP(i, 1, n) {int s = 1 << (i - 1); sum[s][i] = 0, dis[s][i][i] = 1, dp[s][i] = 0;}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	init();
	int statu = (1 << n) - 1;
	for (int i = 1; i <= statu; i++) {
		/*if (i == 15){
			puts("fuck");
		}*/
		for (int root = 1; root <= n; root++) if (checkin(root, i)){
			int co1 = 0, co2 = 0, ts1 = 0, ts2 = 0, mn = INF;
			for (int s = i; s; s = i & (s - 1)) if (dp[s][root] < INF){
				int s1 = s, s2 = i ^ s;
				if (checkin(root, s2)) swap(s1, s2);
				for (int p = 1; p <= n; p++) if (checkin(p, s1)){
					for (int q = 1; q <= n; q++) if (checkin(q, s2) && dp[s2][q] < INF && g[p][q] < INF){
						int temp = dp[s1][root] + dp[s2][q] + g[p][q] * dis[s1][root][p] + sum[s2][q] * dis[s1][root][p];
						if (temp < mn) mn = temp, co1 = p, co2 = q, ts1 = s1, ts2 = s2;
					}
				}
			}
			if (co1 && co2){
				dp[i][root] = mn, sum[i][root] = sum[ts1][root] + sum[ts2][co2] + g[co1][co2];
				for (int j = 1; j <= n; j++) if (checkin(j, ts1)) dis[i][root][j] = dis[ts1][root][j]; else if (checkin(j, ts2)) dis[i][root][j] = dis[ts2][co2][j] + dis[ts1][root][co1];
			}
	}
	}
	/*for (int i = 1; i <= statu; i++) {
		FORP(j, 1, n) if (checkin(j, i)) printf("(%d,%d) %d ", i, j, dp[i][j]);
		puts("");
	}*/
	int ans = INF;
	FORP(i, 1, n) ans = min(ans, dp[statu][i]);
	printf("%d\n", ans);
}

#include <bits/stdc++.h>
using namespace std;

#define maxn 100005
#define maxm 100005
#define MOD 1000000007
#define INF 0x3f
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)--)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
#define pii pair<int, int>
/*----------------------split line----------------------------*/

int main(){
	int n = 4;
	int statu = (1 << 12) - 1;
	for (int i = 1; i <= statu; i++)
	for (int s = i; s; s = i & (s - 1)) n ++;
	int a[2];
	memset(a, 0x3f, sizeof(a));
	printf("%d\n", a[1]);
}

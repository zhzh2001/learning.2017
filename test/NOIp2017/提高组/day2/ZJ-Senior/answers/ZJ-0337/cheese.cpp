#include <bits/stdc++.h>
using namespace std;

#define maxn 100005
#define maxm 100005
#define MOD 1000000007
#define INF 2000000000
#define FORP(i, a, b) for (int (i) = (a); (i) <= (b); (i)++)
#define FORM(i, a, b) for (int (i) = (a); (i) >= (b); (i)--)
#define sqr(x) (x) * (x)
#define fi first
#define se second
#define pb push_back
#define mp make_pair
#define pii pair<int, int>
typedef long long ll;
/*----------------------split line----------------------------*/
struct Point{
	ll x, y, z;
	void init(){scanf("%lld%lld%lld",&x, &y, &z);}
}p[maxn];
int f[maxn];
int n, m;
ll h, r;

double calc(int x, int y){
	return (double)sqrt((double)(sqr((double) p[x].x - p[y].x) + sqr((double) p[x].y - p[y].y) + sqr((double) p[x].z - p[y].z)));
}
int find(int x){
	return x == f[x] ? x : f[x] = find(f[x]);
}
void Union(int x, int y){
	int v = find(x),u = find(y);
	f[v] = u;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int cas; scanf("%d",&cas);
	while (cas--){
		scanf("%d%lld%lld",&n, &h, &r);
		FORP(i, 2, n + 1) p[i].init();
		m = n + 2;
		FORP(i, 1, m) f[i] = i;
		FORP(i, 2, n + 1) {
			if (p[i].z <= r) Union(i, 1);
			if (p[i].z + r >= h) Union(i, m);
			FORP(j, 2, i - 1) if (calc(i, j) <= (double)(2.0 * (double)r)) Union(i, j);
		}
		if (find(1) == find(m)) puts("Yes");
		else puts("No");
	}
}

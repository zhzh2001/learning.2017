#include <cstdio>
#include <cmath>
#include <vector>
using namespace std;
int n,m,q,x,y,cnt,tmp,num,a[1010][1010];

int qx[300100],qy[300100];
struct block{
	long long a[780];
	int pos;
}b[1590];

long long pfive[110000];
vector<long long> rtl[51000];
vector<long long> head[51000];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d %d %d",&n,&m,&q);
	
	if (q<=500 && n<=1000 && m<=1000)//30
	{
		cnt=0;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				a[i][j]=++cnt;
		while (q--)
		{
			scanf("%d %d",&x,&y);
			printf("%d\n",a[x][y]); tmp=a[x][y];
			for (int j=y;j<m;j++) a[x][j]=a[x][j+1];
			for (int i=x;i<n;i++) a[i][m]=a[i+1][m];
			a[n][m]=tmp;
		}
		return 0;
	}
	
	
	if (q<=500)//20
	{
		int len=n+m-1;
		for (int i=1;i<=m;i++) pfive[i]=i;
		for (int i=m+1;i<=len;i++) pfive[i]=pfive[i-1]+m;
		for (int i=1;i<=n;i++) head[i][0]=0;
		for (int i=1;i<=n;i++) rtl[i][0]=0;
		while (q--)
		{
			scanf("%d %d",&x,&y);
			if (x==1)
			{
				printf("%lld\n",pfive[y]); tmp=pfive[y];
				for (int i=y+1;i<=len;i++) pfive[i-1]=pfive[i];
				pfive[len]=tmp;
				continue;
			}
			if (y==m)
			{
				int pos=x+y-1;
				printf("%lld\n",pfive[pos]); tmp=pfive[pos];
				for (int i=pos+1;i<=len;i++) pfive[i-1]=pfive[i];
				pfive[len]=tmp;
				continue;
			}
			if (m-rtl[x][0]<=y)
			{
				int pos=y-(m-rtl[x][0]-1);
				printf("%lld\n",rtl[x][pos]); tmp=rtl[x][pos];
				for (int i=pos+1;i<=rtl[x][0];i++) rtl[x][i-1]=rtl[x][i];
				rtl[x][rtl[x][0]]=pfive[x+m-1];
				for (int i=x+m-1;i<len;i++) pfive[i]=pfive[i+1];
				pfive[len]=tmp;
			}
			else
			{
				int prdel=0;
				long long u=m*(x-1)+y;
				for (int i=1;i<=head[x][0];i++) if (head[x][i]<=u) prdel++;
				printf("%lld\n",u+prdel); tmp=u+prdel;
				head[x][++head[x][0]]=tmp;
				rtl[x][++rtl[x][0]]=pfive[x+m-1];
				for (int i=x+m-1;i<len;i++) pfive[i]=pfive[i+1];
				pfive[len]=tmp;
			}
		}
		return 0;
	}
	
	
	int flag=1;
	for (int i=1;i<=q;i++) {
		scanf("%d %d",&qx[i],&qy[i]);
		if (qx[i]!=1) flag=0;
	}
	if (flag)//30
	{
		if (n==1)
		{
			cnt=0;
			int len=m,size=(int)sqrt(len);
			if (len%size==0){
				num=len/size;
				for (int i=1;i<=num;i++)
				{
					for (int j=1;j<=size;j++) b[i].a[j]=++cnt;
					b[i].pos=size;
				}
			}
			else{
				num=len/size+1;
				for (int i=1;i<num;i++)
				{
					for (int j=1;j<=size;j++) b[i].a[j]=++cnt;
					b[i].pos=size;
				}
				for (int i=cnt+1;i<=m;i++) b[num].a[i-cnt]=i;
				b[num].pos=m-cnt;
			}
			for (int o=1;o<=q;o++)
			{
				int pp=0,cc=0;
				while (cc+b[pp+1].pos<qy[o]) cc+=b[pp+1].pos,pp++;
				pp++;
				int k=qy[o]-cc;
				tmp=b[pp].a[k];
				for (int i=k+1;i<=b[pp].pos;i++) b[pp].a[i-1]=b[pp].a[i];
				b[pp].pos--;
				printf("%d\n",tmp);
				if (pp>1 && b[pp].pos+b[pp-1].pos<=size){
					int np=b[pp-1].pos+b[pp].pos;
					for (int i=b[pp-1].pos+1;i<=np;i++) b[pp-1].a[i]=b[pp].a[i-b[pp-1].pos];
					b[pp-1].pos=np;
					b[pp].pos=0;
				}
				if (b[num].pos<size) b[num].a[b[num].pos+1]=tmp,b[num].pos++;
				else{
					num++;
					b[num].pos=1;
					b[num].a[1]=tmp;
				}
			}
			return 0;
		}
		else{
			long long cnbb=0,tmd,plus=1;
			
			
			int len=m+n-1,size=(int)sqrt(len);
			if (len%size==0){
				num=len/size;
				for (int i=1;i<=num;i++)
				{
					for (int j=1;j<=size;j++) {
						if (cnbb==m) plus=m; cnbb+=plus,b[i].a[j]=cnbb;
					}
					b[i].pos=size;
				}
			}
			else{
				num=len/size+1;
				for (int i=1;i<num;i++)
				{
					for (int j=1;j<=size;j++) {
						if (cnbb==m) plus=m; cnbb+=plus,b[i].a[j]=cnbb;
					}
					b[i].pos=size;
				}
				long long mxmx=m*n,c=1;
				for (long long kk=cnbb+plus;kk<=mxmx;kk+=plus,c+=1) b[num].a[c]=kk;
				b[num].pos=c;
			}
			for (int o=1;o<=q;o++)
			{
				qy[o]=qy[o]+qx[o]-1;
				int pp=0,cc=0;
				while (cc+b[pp+1].pos<qy[o]) cc+=b[pp+1].pos,pp++;
				pp++;
				int k=qy[o]-cc;
				tmd=b[pp].a[k];
				for (int i=k+1;i<=b[pp].pos;i++) b[pp].a[i-1]=b[pp].a[i];
				b[pp].pos--;
				printf("%lld\n",tmd);
				if (pp>1 && b[pp].pos+b[pp-1].pos<=size){
					int np=b[pp-1].pos+b[pp].pos;
					for (int i=b[pp-1].pos+1;i<=np;i++) b[pp-1].a[i]=b[pp].a[i-b[pp-1].pos];
					b[pp-1].pos=np;
					b[pp].pos=0;
				}
				if (b[num].pos<size) b[num].a[b[num].pos+1]=tmd,b[num].pos++;
				else{
					num++;
					b[num].pos=1;
					b[num].a[1]=tmd;
				}
			}
			return 0;
		}
	}
	return 0;
}

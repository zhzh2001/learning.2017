#include <cstdio>
#include <cstring>
#include <queue>
#include <cmath>
using namespace std;
const int INF=1000000000;
int T,n;
long long h,r;
struct point{
	long long x,y,z;
}p[1010];
int cnt,he[1010],nxt[1100000],to[1100000];
int dist[1010];
bool vis[1010];
queue<int> q;
inline void addedge(int u,int v){to[++cnt]=v,nxt[cnt]=he[u],he[u]=cnt;}
double getdis(int a,int b){
	return sqrt( (double)(p[a].x-p[b].x)*(p[a].x-p[b].x)+(double)(p[a].y-p[b].y)*(p[a].y-p[b].y)+(double)(p[a].z-p[b].z)*(p[a].z-p[b].z) );
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		cnt=0;
		memset(he,0,sizeof(he));
		memset(nxt,0,sizeof(nxt));
		scanf("%d %lld %lld",&n,&h,&r);
		for (int i=1;i<=n;i++)
			scanf("%lld %lld %lld",&p[i].x,&p[i].y,&p[i].z);
		for (int i=1;i<=n;i++)
		{
			if (p[i].z<=r) {addedge(0,i); addedge(i,0);}
			if (p[i].z+r>=h) {addedge(i,n+1); addedge(n+1,i);}
		}
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (i!=j && getdis(i,j)<=r*2)
				{
					addedge(j,i); addedge(i,j);
				}
		for (int i=0;i<=n+1;i++) dist[i]=INF,vis[i]=0;
		dist[0]=0; vis[0]=1;
		q.push(0);
		while (!q.empty())
		{
			int k=q.front(); q.pop();
			for (int i=he[k];i;i=nxt[i])
				if (dist[to[i]]>dist[k])
				{
					dist[to[i]]=dist[k];
					if (!vis[to[i]]) {
						vis[to[i]]=1;
						q.push(to[i]);
					}
				}
			vis[k]=0;
		}
		if (dist[n+1]==0) puts("Yes");
		else puts("No");
	}
	return 0;
}

#include <cstdio>
#include <cstring>
using namespace std;
const int INF=1000000000;
int n,m,u,v,w,ws,flag,ans;
int dis[15][15];
bool vis[15];
struct node{
	int x,num;
}q[100];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			dis[i][j]=-1;
	ws=-1; flag=1;
	for (int i=1;i<=m;i++){
		scanf("%d %d %d",&u,&v,&w);
		if (ws==-1) ws=w;
		if (ws!=w) flag=0;
		if (dis[u][v]==-1 || dis[u][v]>w) dis[u][v]=dis[v][u]=w;
	}
	if (flag)//40
	{
		ans=INF;
		for (int i=1;i<=n;i++)
		{
			memset(vis,0,sizeof(vis));
			int tmp=0;
			int head=1,tail=1;
			q[1].x=i,q[1].num=1; vis[1]=1;
			while (tail<=head)
			{
				int k=q[tail].x;
				for (int j=1;j<=n;j++)
					if (vis[j]==0 && dis[k][j]!=-1)
					{
						vis[j]=1;
						tmp+=dis[k][j]*q[tail].num;
						q[++head].x=j; q[head].num=q[tail].num+1;
					}
				tail++;
			}
			if (tmp<ans) ans=tmp;
		}
		printf("%d\n",ans);
		return 0;
	}
	return 0;
}

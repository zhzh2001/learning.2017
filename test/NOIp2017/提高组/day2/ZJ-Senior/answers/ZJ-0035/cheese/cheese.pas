const maxn=1000+50;
var dp:array[1..maxn]of boolean;
  c,d,q:array[1..maxn]of longint;
  x,y,z:array[1..maxn]of int64;
  inq:array[1..maxn]of boolean;
  cnt,len,n,i,t,ql,qr,k:longint;
  h,r:int64;
  flag:boolean;

function pow(ret:double):double;
begin
  exit(ret*ret);
end;

function dist(a,b:longint):double;
begin
  exit(sqrt(pow(x[a]-x[b])+pow(y[a]-y[b])+pow(z[a]-z[b])));
end;

function cmp(a,b:longint):boolean;
begin
  if dist(a,b)<=r*2 then exit(true);
  exit(false);
end;

begin
  assign(input,'cheese.in'); reset(input);
  assign(output,'cheese.out'); rewrite(output);
  read(t);
  while t>0 do
   begin
    read(n,h,r);
    for i:=1 to n do
     read(x[i],y[i],z[i]);
    fillchar(dp,sizeof(dp),false);
    fillchar(inq,sizeof(inq),false);
    //for i:=1 to n do
     //if (z[i]>h) or (z[i]<0) then
      //writeln();
    cnt:=0; len:=0;
    ql:=1; qr:=0;
    for i:=1 to n do
     if z[i]-r<=0 then
      begin inc(cnt); c[cnt]:=i; end;
    for i:=1 to n do
     if(z[i]+r>=h)then
      begin inc(len); d[len]:=i; end;
    for i:=1 to cnt do
     begin
      dp[c[i]]:=true;
      inc(qr); q[qr]:=c[i];
      inq[c[i]]:=true;
     end;
    while ql<=qr do
     begin
      k:=q[ql]; inc(ql);
      for i:=1 to n do
       begin
        if cmp(k,i) then
         dp[i]:=dp[i]or dp[k];
        if dp[i] and not inq[i] then
         begin inc(qr); q[qr]:=i; inq[i]:=true; end;
       end;
     end;
    //for i:=1 to n do
     //for j:=1 to n do
      //if cmp(i,j) then
       //dp[j]:=dp[i]or dp[j];
    flag:=false;
    for i:=1 to len do
     if dp[d[i]] then
      flag:=true;
    if flag then
     writeln('Yes')
    else
     writeln('No');
    dec(t);
   end;
  close(input); close(output);
end.

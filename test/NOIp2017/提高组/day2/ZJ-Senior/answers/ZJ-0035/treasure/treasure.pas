const maxd=5000+50;
  maxn=15;
  INF=1<<30;
var vis:array[0..maxn]of boolean;
  used:array[0..maxn]of boolean;
  dp,w:array[0..maxn,0..maxd]of int64;
  len:array[0..maxn]of longint;
  map:array[0..maxn,0..maxn]of longint;
  num,d:array[0..maxn]of longint;
  i,j,n,m,u,v,val:longint;
  ans:int64;

function min(a,b:int64):int64;
begin
  if a<b then exit(a);
  exit(b);
end;

function safe(x,cnd:longint):boolean;
var i:longint;
begin
  for i:=1 to n do
   if(1<<(i-1))and cnd=(1<<(i-1))then
    if vis[w[x,i]] then exit(false);
  exit(true);
end;

procedure dfs(c:int64);
var i,j,x,k:longint;
  flag:boolean;
begin
  if c>ans then exit();
  flag:=true;
  for i:=1 to n do
   if not vis[i] then flag:=false;
  if flag then
   begin
    ans:=min(ans,c);
    exit();
   end;
  for k:=n downto 1 do
   if(vis[k])and(not used[k])then
    begin
     x:=k;
     for i:=0 to len[x] do
      if safe(x,i) then
       begin
        used[x]:=true;
        for j:=1 to n do
         if(1<<(j-1))and i=(1<<(j-1))then
          begin vis[w[x,j]]:=true; d[w[x,j]]:=d[x]+1; end;
        for j:=n downto 1 do
         if(1<<(j-1))and i=(1<<(j-1))then
          dfs(c+d[x]*dp[x,i]);
        for j:=1 to n do
         if(1<<(j-1))and i=(1<<(j-1))then
          begin vis[w[x,j]]:=false; d[w[x,j]]:=0; end;
        used[x]:=false;
       end;
    end;
end;

procedure init();
var i,j,k,cnt:longint;
begin
  for i:=1 to n do
   begin
    for k:=0 to len[i] do
     dp[i,k]:=INF;
    cnt:=0; dp[i,0]:=0;
    for k:=1 to n do
     if map[i,k]<>INF then
      begin inc(cnt); w[i,cnt]:=k; end;
    for k:=0 to len[i] do
     for j:=1 to num[i] do
      dp[i,k or(1<<(j-1))]:=min(dp[i,k or(1<<(j-1))],dp[i,k]+map[i,w[i,j]]);
   end;
end;

begin
  assign(input,'treasure.in'); reset(input);
  assign(output,'treasure.out'); rewrite(output);
  ans:=INF;
  read(n,m);
  for i:=1 to n do
   for j:=1 to n do
    map[i,j]:=INF;
  for i:=1 to m do
   begin
    read(u,v,val);
    if u=v then continue;
    map[u,v]:=min(map[u,v],val);
    map[v,u]:=min(map[v,u],val);
   end;
  for i:=1 to n do
   for j:=1 to n do
    if map[i,j]<>INF then
     inc(num[i]);
  for i:=1 to n do
   len[i]:=(1<<num[i])-1;
  init();
  for i:=1 to n do
   begin
    fillchar(vis,sizeof(vis),false);
    fillchar(used,sizeof(used),false);
    fillchar(d,sizeof(d),0);
    vis[i]:=true; d[i]:=1;
    dfs(0);
   end;
  writeln(ans);
  close(input); close(output);
end.

type dot=record
  x,y:longint;
   end;
const maxn=300000+50;
var n,m,q,i,j,cnt,x,y,ans:longint;
  l,r,u,d,row,last:array[0..maxn]of longint;
  w:array[1..maxn]of dot;
  c:array[1..maxn]of longint;
  flag:boolean;

function pos(a,b:longint):longint;
begin
  exit((a-1)*m+b);
end;

procedure rec(p:longint);
begin
  x:=p div m+1;
  y:=p mod m;
end;

function lowbit(a:longint):longint;
begin
  exit(a and -a);
end;

procedure add(k,a:longint);
begin
  while k<=m do
   begin
    c[k]:=c[k]+a;
    k:=k+lowbit(k);
   end;
end;

function sum(k:longint):longint;
var ret:longint;
begin
  ret:=0;
  while k>0 do
   begin
    ret:=ret+c[k];
    k:=k+lowbit(k);
   end;
  exit(ret);
end;

begin
  assign(input,'phalanx.in'); reset(input);
  assign(output,'phalanx.out'); rewrite(output);
  read(n,m,q);
  for i:=1 to q do
   read(w[i].x,w[i].y);
  last[0]:=pos(n,m);
  if q<=500 then
   for i:=1 to q do
    begin
     x:=w[i].x; y:=w[i].y;
     flag:=false;
     for j:=cnt downto 1 do
      begin
       if(x=n)and(y=m)then
        begin
         writeln(last[j-1]);
         flag:=true; break;
        end;
       if x=row[j] then
        if(l[j]<=y)and(r[j]>=y)then
         begin y:=y+1; continue; end;
       if y=m then
        if(u[j]<=x)and(d[j]>=x)then
         begin x:=x+1; continue; end;
      end;
     if not flag then
      begin
       writeln(pos(x,y));
       last[cnt]:=pos(x,y);
      end
     else
      last[cnt]:=last[j-1];
     inc(cnt);
     row[cnt]:=w[i].x;
     l[cnt]:=w[i].y; r[cnt]:=m-1;
     u[cnt]:=w[i].x; d[cnt]:=n-1;
    end
   else
    begin
     last[0]:=m;
     for i:=1 to q do
      begin
       y:=w[i].y;
       add(y-1,-1);
       add(m-1,1);
       flag:=false;
       if y=m then
        begin writeln(last[i-1]); flag:=true; end;
       if not flag then
        begin
         ans:=y+sum(y);
         writeln(ans);
         last[i]:=ans;
        end
       else
        last[i]:=last[i-1];
      end;
    end;
  close(input); close(output);
end.
#include<cstdio>
#include<algorithm>
using namespace std;
const int N=13;
int n,m,ans=2000000000,a[N][N],dep[N][N];
bool cho[N];
int read(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
void DFS(int x,int y){
	if(y>=ans) return;
	if(x>n){ans=y;return;}
	for(int u=1;u<=n;u++) if(!cho[u]){
		cho[u]=1;
		for(int i=1,MIN;dep[i][0];i++){
			MIN=2e9,dep[i+1][++dep[i+1][0]]=u;
			for(int j=dep[i][0];j;j--) MIN=min(MIN,a[dep[i][j]][u]);
			DFS(x+1,y+MIN*i),dep[i+1][0]--;
		}
		cho[u]=0;
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++) a[i][j]=1000000;
	for(int i=1,x,y,z;i<=m;i++)
		x=read(),y=read(),a[x][y]=a[y][x]=min(a[x][y],read());
	for(int i=dep[1][0]=1;i<=n;i++)
		cho[dep[1][1]=i]=1,DFS(2,0),cho[i]=0;
	printf("%d",ans);
	fclose(stdin),fclose(stdout);
	return 0;
}

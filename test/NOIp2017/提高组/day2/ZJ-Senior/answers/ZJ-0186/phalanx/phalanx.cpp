#include<cstdio>
const int N1=1001,N2=50001,Q2=501;
int n,m,q,t,a[N1][N1],b[Q2][N2],c[N2],cho[N2],que[Q2][2];
int read(){
	int ret=0,c=getchar();
	while(c<48||57<c) c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if(n<=1000&m<=1000){
		for(int i=1,t=0;i<=n;i++)
			for(int j=1;j<=m;j++) a[i][j]=++t;
		for(int i=1,x,y,z;i<=q;i++){
			x=read(),y=read(),printf("%d\n",z=a[x][y]);
			for(int j=y;j<m;j++) a[x][j]=a[x][j+1];
			for(int j=x;j<n;j++) a[j][m]=a[j+1][m];
			a[n][m]=z;
		}
	}
	else if(q<=500){
		for(int i=1;i<=q;i++)
			cho[que[i][0]=read()]=1,que[i][1]=read();
		for(int i=1;i<=n;i++) if(cho[i]){
			cho[i]=++t;
			for(int j=1,_=(i-1)*m;j<m;j++) b[t][j]=_+j;
		}
		for(int i=1;i<=n;i++) c[i]=m*i;
		for(int i=1,x,y,z;i<=q;i++){
			x=que[i][0],y=que[i][1];
			printf("%d\n",z=(y<m?b[cho[x]][y]:c[x]));
			for(int j=y,k=cho[x];j<m-1;j++) b[k][j]=b[k][j+1];
			if(y<m) b[cho[x]][m-1]=c[x];
			for(int j=x;j<n;j++) c[j]=c[j+1];
			c[n]=z;
		}
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

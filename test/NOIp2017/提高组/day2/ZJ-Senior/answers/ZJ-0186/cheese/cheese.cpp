#include<cstdio>
const int N=1001;
int n,h,x[N],y[N],z[N],que[N];
bool vis[N];
long long r;
int read(){
	int ret=0,fla=1,c=getchar();
	while(c!=45&&c<48||57<c) c=getchar();
	if(c==45) fla=-1,c=getchar();
	while(47<c&&c<58) ret=ret*10+c-48,c=getchar();
	return ret*fla;
}
long long sqr(int x){return 1ll*x*x;}
bool dist(int i,int j){
	return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=r;
}
bool BFS(){
	int b=0,e=0;
	for(int i=1;i<=n;i++)
		if(sqr(z[i])*4<=r) que[++e]=i,vis[i]=1;
		else vis[i]=0;
	while(b<e){
		int i=que[++b];
		if(sqr(h-z[i])*4<=r) return 1;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&dist(i,j)) que[++e]=j,vis[j]=1;
	}
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for(int T=read();T;T--){
		n=read(),h=read(),r=2ll*read(),r=r*r;
		for(int i=1;i<=n;i++) x[i]=read(),y[i]=read(),z[i]=read();
		printf(BFS()?"Yes\n":"No\n");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

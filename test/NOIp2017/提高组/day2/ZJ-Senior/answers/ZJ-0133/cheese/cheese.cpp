#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
typedef long long ll;
typedef unsigned long long ull;

const int N=1010;

struct Point{
	ll x,y,z;
	void scan() { scanf("%lld%lld%lld",&x,&y,&z); }
}p[N];
inline ull sqr(ll x) { return x*x; }
ull dis(ll x1,ll y1,ll z1,ll x2,ll y2,ll z2) {
	return sqr(x1-x2) + sqr(y1-y2) + sqr(z1-z2);
}

ll h,r;
int n;
int b[N];
int dfs(int x) {
	b[x] = 1; if (p[x].z + r >= h) return 1;
	for (int i=1;i<=n;++i) if (!b[i]) {
		int flag=0;
		if (dis(p[i].x,p[i].y,p[i].z,p[x].x,p[x].y,p[x].z) <= sqr(2*r)) flag=dfs(i);
		if (flag) return 1;
	}
	return 0;
}

int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--) {
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;++i) p[i].scan();
		memset(b, 0, sizeof(b));
		int flag=0;
		for (int i=1;i<=n;++i) {
			if (p[i].z <= r) {
				if (dfs(i)) flag=1;
			}
		}
		if (flag) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

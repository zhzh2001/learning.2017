#include <cstdio>
#include <algorithm>
using namespace std;

const int N=13;
const int INF=1e9;
inline void chmin(int &x,int y) { if (y<x) x=y; }

int n,m,mask;
int ans;
int dis[N][N];
int f[N][1<<N];
int calc(int set1, int set2) {
	int ans = 0;
	for (int i=1;i<=n;++i) if ((set1>>(i-1))&1) {
		int mn=INF;
		for (int j=1;j<=n;++j) if ((set2>>(j-1))&1) {
			chmin(mn, dis[i][j]);
		}
		if (mn==INF) return INF;
		ans += mn;
	}
	return ans;
}
int dp(int l,int state) {
	if (f[l][state]!=-1) return f[l][state];
	if (state==mask) return 0;
	int set1=mask^state;
	int ans = INF;
	for (int i=set1;i;i=(i-1)&set1) {
		int t=calc(i, state);
		if (t==INF) continue;
		chmin(ans, dp(l+1, state|i) + t*l);
	}
	return f[l][state] = ans;
}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
			if (i!=j) dis[i][j] = INF;
	for (int i=1;i<=m;++i) {
		int u,v,c; scanf("%d%d%d",&u,&v,&c);
		chmin(dis[u][v], c); chmin(dis[v][u], c);
	}
	for (int i=0;i!=N;++i) for (int j=0;j!=(1<<N);++j) f[i][j] = -1;
	ans = INF; mask = (1<<n)-1;
	for (int i=1;i<=n;++i) chmin(ans, dp(1, 1<<(i-1)));
	printf("%d\n",ans);
	return 0;
}

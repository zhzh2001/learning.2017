#include <cstdio>
#include <algorithm>
using namespace std;

const int N=3e5+10;
const int LGN=2.45e7;
const int Q=7e5;

bool mark[LGN];
int lc[LGN],rc[LGN];
int vl[LGN],vr[LGN];
int t[LGN],totnode;
void pushdown(int rt,int rtl,int rtr) {
	if (!lc[rt]) lc[rt] = ++totnode, rc[rt] = ++totnode;
	if (mark[rt]) {
		mark[rt] = 0; int mid=(rtl+rtr) >> 1;
		mark[lc[rt]] = 1; mark[rc[rt]] = 1;
		vl[lc[rt]] = vl[rt]; vr[lc[rt]] = vl[rt] + (mid-rtl+1) - 1;
		vr[rc[rt]] = vr[rt]; vl[rc[rt]] = vl[rt] + (mid-rtl+1);
		t[lc[rt]] = vr[lc[rt]]-vl[lc[rt]]+1;
		t[rc[rt]] = vr[rc[rt]]-vl[rc[rt]]+1;
	}
}
void pushup(int rt) { t[rt] = t[lc[rt]] + t[rc[rt]]; }
void modify(int cl,int cr,int l,int r,int rt,int rtl,int rtr) {
	if (l==rtl && r==rtr) {
		t[rt] = rtr-rtl+1;
		mark[rt] = 1; vl[rt] = cl; vr[rt] = cr;
		return;
	}
	pushdown(rt,rtl,rtr);
	int mid=(rtl+rtr) >> 1;
	if (r<=mid) modify(cl,cr,l,r,lc[rt],rtl,mid);
	else if (l>mid) modify(cl,cr,l,r,rc[rt],mid+1,rtr);
	else {
		int cmid=cl+(mid-rtl+1)-1;
		modify(cl,cmid,l,mid,lc[rt],rtl,mid);
		modify(cmid+1,cr,mid+1,r,rc[rt],mid+1,rtr);
	}
	pushup(rt);
}
void del(int pos,int rt,int rtl,int rtr) {
	if (rtl==rtr) { t[rt] = 0; return; }
	pushdown(rt,rtl,rtr); int mid = (rtl+rtr) >> 1;
	if (pos<=mid) del(pos,lc[rt],rtl,mid);
	else del(pos,rc[rt],mid+1,rtr);
	pushup(rt);
}
pair<int,int> getval(int rnk,int rt,int rtl,int rtr) {
	if (rtl==rtr) return make_pair(vl[rt], rtl);
	int mid=(rtl+rtr) >> 1; pushdown(rt,rtl,rtr);
	if (t[lc[rt]]>=rnk) return getval(rnk,lc[rt],rtl,mid);
	else return getval(rnk-t[lc[rt]],rc[rt],mid+1,rtr);
}

int root[N], size[N], root0, size0;

int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q; scanf("%d%d%d",&n,&m,&q);
	if (m!=1)
		for (int i=1;i<=n;++i) {
			root[i] = ++totnode; size[i] = m-1;
			modify((i-1)*m+1,i*m-1,1,m-1,root[i],1,Q);
		}
	root0 = ++totnode;
	for (int i=1;i<=n;++i)
		modify(i*m,i*m,i,i,root0,1,Q);
	size0 = n;
	for (int i=1;i<=q;++i) {
		int x,y; scanf("%d%d",&x,&y);
		if (y==m) {
			pair<int,int> t = getval(x,root0,1,Q);
			del(t.second,root0,1,Q); ++size0;
			modify(t.first,t.first,size0,size0,root0,1,Q);
			printf("%d\n",t.first);
		} else {
			pair<int,int> t = getval(y,root[x],1,Q);
			pair<int,int> t2 = getval(x,root0,1,Q);
			del(t.second,root[x],1,Q); ++size[x];
			modify(t2.first,t2.first,size[x],size[x],root[x],1,Q);
			del(t2.second,root0,1,Q); ++size0;
			modify(t.first,t.first,size0,size0,root0,1,Q);
			printf("%d\n",t.first);
		}
	}
	return 0;
}

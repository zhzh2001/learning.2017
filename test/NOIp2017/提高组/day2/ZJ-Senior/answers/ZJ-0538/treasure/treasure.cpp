#include <cstdio>
#include <cstring>
#include <iostream>
using namespace std;
#define N 14
#define M 1100
#define INF 1e11
#define ll long long

struct edge {
	int nex,to;
	ll dis;
}e[M<<1];
int n,m,he,ta,cc,cnt;
ll ans=INF;
int vis[N],q[M*50],head[M],from[M],to[M];
ll dp[1<<N],f[1<<N],g[N],dis[M],dep[N],a[N][N];

void add(int x,int y,int z)
{
	e[++cnt].to=y;
	e[cnt].dis=z;
	e[cnt].nex=head[x];
	head[x]=cnt;
}

void dfs(int sta,ll sum)
{
	if (sta==(1<<n)-1) {
		ans=min(ans,sum);
		return;
	}
	if (sum>=ans) return;
	if (n>9 && sum+f[sta^((1<<n)-1)]>=ans) return;
	int tmp=0;
	for (int i=0;i<n;i++) if (sta&(1<<i)) tmp++;
	for (int k=1;k<=m;k++)
	{
		if ((sta&(1<<from[k])) && (sta&(1<<to[k]))) continue;
		if (!(sta&(1<<from[k])) && !(sta&(1<<to[k]))) continue;
		if (sta&(1<<to[k]) && !(sta&(1<<from[k]))) swap(from[k],to[k]);
		dep[to[k]]=dep[from[k]]+1;
		dfs(sta|(1<<to[k]),sum+dep[from[k]]*dis[k]);
	}
}

void BFS(int st)
{
	memset(vis,0,sizeof(vis));
	memset(g,0x3f,sizeof(g));
	g[st]=1;
	vis[st]=1; q[1]=st;
	he=0; ta=1;
	while (he<ta)
	{
		int x=q[++he];
		for (int i=head[x];~i;i=e[i].nex)
		{
			int v=e[i].to;
			if (!vis[v]) {
				q[++ta]=v;
				g[v]=g[x]+1;
				vis[v]=1;
			}
		}
	}
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(head,-1,sizeof(head));
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		scanf("%d%d%lld",&from[i],&to[i],&dis[i]);
		if (from[i]>to[i]) swap(from[i],to[i]);
		from[i]--; to[i]--;
		if (!a[from[i]][to[i]] || dis[i]<a[from[i]][to[i]]) a[from[i]][to[i]]=dis[i];
	}
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)
			if (a[i][j])
			{
				from[++cc]=i,to[cc]=j,dis[cc]=a[i][j];
				add(i,j,a[i][j]); add(j,i,a[i][j]);
			}
	m=cc;
	
	for (int i=0;i<(1<<n);i++) f[i]=INF;
	for (int j=0;j<n;j++)
	{
		for (int i=0;i<(1<<n);i++) dp[i]=INF; dp[1<<j]=0;
		BFS(j); q[1]=(1<<j); he=0; ta=1;
		while (he<ta)
		{
			int i=q[++he];
			for (int k=1;k<=m;k++)
			{
				if ((i&(1<<from[k])) && (i&(1<<to[k]))) continue;
				if (!(i&(1<<from[k])) && !(i&(1<<to[k]))) continue;
				if (i&(1<<to[k]) && !(i&(1<<from[k]))) swap(from[k],to[k]);
				if (dp[i]+dis[k]*g[from[k]]<dp[i|(1<<(to[k]))])
				{
					dp[i|(1<<to[k])]=min(dp[i|(1<<to[k])],dp[i]+dis[k]*g[from[k]]);
					q[++ta]=i|(1<<to[k]);
				}
			}
		}
		for (int i=0;i<(1<<n);i++) f[i]=min(f[i],dp[i]);
	}
	
	for (int j=0;j<n;j++)
	{
		memset(dep,0,sizeof(dep)); dep[j]=1;
		dfs((1<<j),0);
	}
	printf("%lld\n",ans);
	return 0;
}

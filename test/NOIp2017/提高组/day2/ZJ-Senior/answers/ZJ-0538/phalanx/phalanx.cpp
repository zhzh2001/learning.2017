#include <cstdio>
#include <iostream>
using namespace std;
#define N 1010

int n,m,q,x,y,tmp,sz;
int id[N][N];

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			id[i][j]=++sz;
	
	while (q--)
	{
		scanf("%d%d",&x,&y);
		printf("%d\n",id[x][y]); tmp=id[x][y];
		for (int i=y;i<m;i++) id[x][i]=id[x][i+1];
		for (int i=x;i<n;i++) id[i][m]=id[i+1][m];
		id[n][m]=tmp;
	}
	return 0;
}

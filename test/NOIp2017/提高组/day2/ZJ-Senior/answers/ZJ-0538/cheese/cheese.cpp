#include <cstdio>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;
#define ld long double
const int N=1050;

struct point {
	ld x,y,z;
}p[N];
int T,n,fa[N];
ld h,r;
ld maxh[N],minh[N];

int find(int x) {
	return x==fa[x] ? x : fa[x]=find(fa[x]);
}

ld dis(int a,int b) {
	return sqrt((p[a].x-p[b].x)*(p[a].x-p[b].x)+(p[a].y-p[b].y)*(p[a].y-p[b].y)+(p[a].z-p[b].z)*(p[a].z-p[b].z));
}

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		scanf("%d%Lf%Lf",&n,&h,&r);
		for (int i=1;i<=n;i++) scanf("%Lf%Lf%Lf",&p[i].x,&p[i].y,&p[i].z);
		
		for (int i=1;i<=n;i++)
		{
			fa[i]=i;
			maxh[i]=p[i].z+r;
			minh[i]=p[i].z-r;
		}
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++)
			{
				int x=find(i),y=find(j);
				if (x!=y && dis(i,j)<=2*r)
				{
					fa[y]=x;
					maxh[x]=max(maxh[x],maxh[y]);
					minh[x]=min(minh[x],minh[y]);
				}
			}
		
		bool ans=0;
		for (int i=1;i<=n;i++)
			if (maxh[i]>=h && minh[i]<=0)
			{
				ans=1;
				break;
			}
		if (ans) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

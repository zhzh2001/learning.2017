#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<map>
#include<queue>
#include<vector>
#include<cctype>
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
#define ll long long
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&ch!='-') ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
const int maxn=13;
void upd(int &x,int y){
	if ((y>=0)&&(x==-1||y<x)) x=y;
}
int U,n,m,cnt=0,ans=-1,d[maxn][maxn];
int f[maxn][maxn][4100];
int lg[4100];
int lowbit(int x){return x&-x;}
int DP(int roo,int dep,int S){
	if (!S) return 0;
	if (~f[roo][dep][S]) return f[roo][dep][S];
	int ret=-1;
	for (int T=S;T;T=(T-1)&S){
		for (int tt=T,i=lowbit(tt);tt;tt-=i,i=lowbit(tt)){
			int nrt=lg[i];cnt++;
			if (d[roo][nrt]==-1) continue;
			int v1=DP(roo,dep,S^T);
			if (v1<0||(ret!=-1&&v1+d[roo][nrt]>ret)) continue;
			int v2=DP(nrt,dep+1,T^(1<<nrt));
			if (v2>=0) upd(ret,v1+v2+d[roo][nrt]*dep);
		}
	}
	if (ret==-1||(ans>=0&&ret>=ans)) ret=-2;
	return f[roo][dep][S]=ret;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=rd(),m=rd();U=(1<<n)-1;
	rep(i,0,n-1) lg[1<<i]=i;
	memset(d,-1,sizeof d);
	rep(i,0,n-1) d[i][i]=0;
	rep(i,1,m){
		int u=rd()-1,v=rd()-1,w=rd();
		upd(d[u][v],w),upd(d[v][u],w);
	}
	memset(f,-1,sizeof f);
	rep(i,0,n-1) upd(ans,DP(i,1,U^(1<<i)));
	rt(ans),putchar('\n');
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<map>
#include<queue>
#include<cctype>
#include<vector>
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
#define ll long long
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&ch!='-') ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
#include<ctime>
int n,m;
int main(){
	freopen("treasure.in","w",stdout);
	n=12,m=66;
	printf("%d %d\n",n,m);
	srand(time(0));
	rep(i,1,n) rep(j,i+1,n) printf("%d %d %d\n",i,j,rand()*rand()%500000);
}

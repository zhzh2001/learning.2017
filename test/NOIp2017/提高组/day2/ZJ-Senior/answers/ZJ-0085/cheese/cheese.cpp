#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<map>
#include<queue>
#include<cctype>
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
#define ll long long
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&ch!='-') ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
const int maxn=1005;
int n,fa[maxn];
ll h,r,x[maxn],y[maxn],z[maxn];
ll sqr(ll x){return x*x;}
int getfa(int x){return fa[x]==x?x:fa[x]=getfa(fa[x]);}
void merge(int x,int y){
	int fx=getfa(x),fy=getfa(y);
	if (fx!=fy) fa[fy]=fx;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (int T=rd();T--;){
		n=rd(),h=rd(),r=rd();
		rep(i,1,n) x[i]=rd(),y[i]=rd(),z[i]=rd();
		rep(i,0,n+1) fa[i]=i;
		rep(i,1,n){
			if (z[i]<=r) merge(0,i);
			if (z[i]+r>=h) merge(i,n+1);
			rep(j,1,n) if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=4ll*r*r) merge(i,j);
		}
		if (getfa(0)==getfa(n+1)) puts("Yes");
			else puts("No");
	}
}

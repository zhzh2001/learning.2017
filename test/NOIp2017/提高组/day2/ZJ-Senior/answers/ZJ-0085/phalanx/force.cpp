#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<map>
#include<queue>
#include<cctype>
#include<vector>
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
#define ll long long
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&ch!='-') ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
ll n,m;
int q;
namespace force_npm_q{
	const int maxn=50005,maxq=505;
	ll sav[maxq][maxn],rms[maxn];
	int lst[maxq],vis[maxn],cnt;
	void work(){
		rep(i,1,n) rms[i]=m*i;
		while (q--){
			int x=rd(),y=rd();
			if (y==m){
				ll ans=rms[x];
				rt(ans),putchar('\n');
				rep(i,x,n-1) rms[i]=rms[i+1];rms[n]=ans;
				continue;
			}
			int idx=vis[x];
			if (!idx){
				lst[++cnt]=x,idx=vis[x]=cnt;
				rep(j,1,m-1) sav[cnt][j]=(x-1)*m+j;
			}
			ll ans=sav[idx][y];
			rt(ans),putchar('\n');
			rep(j,y,m-2) sav[idx][j]=sav[idx][j+1];sav[idx][m-1]=rms[x];
			rep(i,x,n-1) rms[i]=rms[i+1];rms[n]=ans;
		}
		exit(0);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("force.out","w",stdout);
	n=rd(),m=rd(),q=rd();
	force_npm_q::work();
}

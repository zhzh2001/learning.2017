#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<cmath>
#include<map>
#include<queue>
#include<cctype>
#include<vector>
#define rep(x,a,b) for (int x=a;x<=b;x++)
#define drp(x,a,b) for (int x=a;x>=b;x--)
#define ll long long
#define cross(x,a) for (int x=hd[a];~x;x=nx[x])
using namespace std;
inline ll rd(){
	ll x=0;int ch=getchar(),f=1;
	while (!isdigit(ch)&&ch!='-') ch=getchar();
	if (ch=='-') f=-1,ch=getchar();
	while (isdigit(ch)) x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
inline void rt(ll x){
	if (x<0) putchar('-'),x=-x;
	if (x>=10) rt(x/10),putchar('0'+x%10);
		else putchar('0'+x);
}
ll n,m;
int q;
namespace force_npm_q{
	const int maxn=50005,maxq=505;
	ll sav[maxq][maxn],rms[maxn];
	int lst[maxq],vis[maxn],cnt;
	void work(){
		rep(i,1,n) rms[i]=m*i;
		while (q--){
			int x=rd(),y=rd();
			if (y==m){
				ll ans=rms[x];
				rt(ans),putchar('\n');
				rep(i,x,n-1) rms[i]=rms[i+1];rms[n]=ans;
				continue;
			}
			int idx=vis[x];
			if (!idx){
				lst[++cnt]=x,idx=vis[x]=cnt;
				rep(j,1,m-1) sav[cnt][j]=(x-1)*m+j;
			}
			ll ans=sav[idx][y];
			rt(ans),putchar('\n');
			rep(j,y,m-2) sav[idx][j]=sav[idx][j+1];sav[idx][m-1]=rms[x];
			rep(i,x,n-1) rms[i]=rms[i+1];rms[n]=ans;
		}
		exit(0);
	}
}
namespace force_x_1{
	const int maxn=900005;
	struct node *null;
	struct node{
		node *fa,*ls,*rs;
		int size,die;
		ll key;
		void upd(){size=ls->size+rs->size+(die^1);}
		void zag(){
			node *y=fa,*z=y->fa;
			if (z->ls==y) z->ls=this;else if (z->rs==y) z->rs=this;fa=z;
			y->rs=ls;if (ls!=null) ls->fa=y;
			ls=y;y->fa=this;y->upd();
		}
		void zig(){
			node *y=fa,*z=y->fa;
			if (z->ls==y) z->ls=this;else if (z->rs==y) z->rs=this;fa=z;
			y->ls=rs;if (rs!=null) rs->fa=y;
			rs=y;y->fa=this;y->upd();
		}
	}a[maxn],*root;
	ll b[maxn],cnt;
	node *newnode(ll k){
		node *p=++cnt+a;
		p->key=k,p->die=0,p->size=1;
		p->ls=p->rs=p->fa=null;
		return p;
	}
	node *build(node *f,int l,int r){
		if (l>r) return null;
		int mid=l+r>>1;
		node *x=newnode(b[mid]);
		x->fa=f,x->ls=build(x,l,mid-1),x->rs=build(x,mid+1,r);
		x->upd();
		return x;
	}
	void splay(node *x){
		while (x->fa!=null){
			node *y=x->fa,*z=y->fa;
			if (z!=null)
				if (y==z->ls) if (x==y->ls) y->zig(),x->zig();else x->zag(),x->zig();
					else if (x==y->rs) y->zag(),x->zag();else x->zig(),x->zag();
			else if (y->ls==x) x->zig();else x->zag();
		}
		x->upd(),root=x;
	}
	void addright(node *r,node *x){
		while (r->rs!=null) r=r->rs;
		r->rs=x;x->fa=r,splay(x);
	}
	node *find(int k){
		node *x=root;
		while (1){
			if (!x->die&&k==x->ls->size+1) return x;
			else if (k<=x->ls->size) x=x->ls;
				else k-=x->ls->size+(x->die^1),x=x->rs;
		}
	}
	void work(){
		rep(i,1,m) b[i]=i;
		rep(i,2,n) b[m+i-1]=m*i;
		null=a,null->fa=null->ls=null->rs=null;null->size=0;
		root=build(null,1,n+m-1);
		while (q--){
			int x=rd(),y=rd();
			node *g=find(y);
			rt(g->key),putchar('\n');
			g->die=1;splay(g);
			node *nw=newnode(g->key);
			addright(root,nw);
		}
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=rd(),m=rd(),q=rd();
	if (n<=50000&&m<=50000&&q<=500) force_npm_q::work();
	force_x_1::work();
}

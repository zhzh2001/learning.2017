var
  i,j,k,t,n,m:longint;
  h,r:int64;
  x,y,z:array[0..1005] of int64;
  bj:array[0..1005] of boolean;

function dist(a,b:longint):real;
 begin
   exit(sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b])));
 end;

procedure search(p,q:longint);
 var
   i,j,a,b:longint;
 begin
   fillchar(bj,sizeof(bj),false);
   bj[1]:=true;
   for i:=1 to n-1 do
     if bj[i]=true
      then for j:=i+1 to n do
              if dist(i,j) <= 2*r
               then bj[j]:=true;
   if bj[n]=true
    then writeln('Yes')
    else writeln('No');
 end;

begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);

  readln(t);
  for i:=1 to t do
   begin
     read(n,h,r);
     for j:=1 to n do
      read(x[j],y[j],z[j]);
     if ((z[n]+r)>=h)and((z[1]-r)<=0)
      then  search(1,n)
      else  writeln('No');
   end;

  close(input);
  close(output);
end.

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>

#include <algorithm>
#include <queue>
#include <stack>

using namespace std;
int T;
int N,H,R;
struct edge{
	int ed,next;
}E[1010*1010*4];
int head[20100],Ecnt;
int pos[10100][5];
int vis[10100];
// code
double distance(int a,int b,int c,int d,int e,int f)//(a,b,c)  ->  (d,e,f)
{
	double t=sqrt((a-d)*(a-d)+(b-e)*(b-e)+(c-f)*(c-f));
	return t;
}
void addEdge(int st,int ed)
{
	E[++Ecnt].ed=ed;
	E[Ecnt].next=head[st];
	head[st]=Ecnt;
}

void init()
{
	scanf("%d%d%d",&N,&H,&R);
	memset(E,0,sizeof E);
	memset(head,0,sizeof head);
	Ecnt=0;
	memset(pos,0,sizeof pos);
	memset(vis,0,sizeof vis);
	
	for(int i=1;i<=N;++i)
	{
		scanf("%d%d%d",&pos[i][0],&pos[i][1],&pos[i][2]);
		for(int j=1;j<i;++j)
		{
			double d=distance(pos[i][0],pos[i][1],pos[i][2],pos[j][0],pos[j][1],pos[j][2]);
			if(d<=2*R)
			{
				addEdge(i+1,j+1);
				addEdge(j+1,i+1);
				//printf("addedge %d <-> %d\n",i,j);
			}
		}
		if(pos[i][2]<=R)
		{
			addEdge(1,i+1);
			addEdge(i+1,1);
					//printf("addedge 0 <-> %d\n",i);
		}
		if(pos[i][2]+R>=H)
		{
			addEdge(N+2,i+1);
			addEdge(i+1,N+2);
								//printf("addedge %d <-> top\n",i);
		}
	}
}
void work()
{
	queue<int>	Q;
	Q.push(1);
	vis[1]=true;
	
	while(!Q.empty())
	{
		int st=Q.front();
		Q.pop();
		
		for(int i=head[st];i;i=E[i].next)
		{
			int ed=E[i].ed;
			if(!vis[ed])
			{
				vis[ed]=true;
				Q.push(ed);
			}
		}
	}
	if(vis[N+2])	puts("Yes");
	else			puts("No");
}
int main()
{
	freopen("cheese.in","r+",stdin);
	freopen("cheese.out","w+",stdout);
	scanf("%d",&T);
	while(T--)
	{
		init();
		work();
	}
	return 0;
}

#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>

#include <algorithm>
#include <queue>
#include <stack>

using namespace std;
typedef long long LL;
int N,M,Q;
int mat[1010][1010];
// code
namespace sub3{

int C[3000010];
LL aa[3000010],tail;
#define lowbit(x) x&(-x)
void modify(int x,int id)
{
	for(;id<=2000010;id+=lowbit(id)) C[id]+=x;
}
int query(int id)
{
	int ret=0;
	for(;id;id-=lowbit(id))	ret+=C[id];
	return ret;
}
int to_find(int id)
{
	int L=1,R=tail;
	while(L<=R)
	{
		int mid=(L+R)>>1;
		int t=mid-query(mid);
		//printf("mid=%d t=%d-%d=%d\n",mid,mid,query(mid),t);
		if(t>=id)	R=mid-1;
		else		L=mid+1;
	}
	return L;
}
void deal_3()
{
	tail=M;
	for(int i=1;i<=M;++i)	aa[i]=i;
	for(int i=1;i<=Q;++i)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		int py=to_find(y);
		printf("%lld\n",aa[py]);
		swap(aa[py],aa[++tail]);
		modify(1,py);
	}
}
void deal_4()
{
	tail=N+M-1;
	for(int i=1;i<=M;++i)	aa[i]=i;
	for(int i=M+1;i<=N+M-1;++i)	aa[i]=(i-M+1)*M;
	for(int i=1;i<=Q;++i)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		int py=to_find(y);
		printf("%lld\n",aa[py]);
		swap(aa[py],aa[++tail]);
		modify(1,py);
		//for(int i=1;i<=N+M-1;++i)
		//	printf("%d ",query(i));
		//puts("");
	}
}
void deal_1()
{
	for(int i=1;i<=N;++i)
		for(int j=1;j<=M;++j)
			mat[i][j]=(i-1)*M+j;
	for(int i=1;i<=Q;++i)
	{
		int x,y,t;
		scanf("%d%d",&x,&y);
		t=mat[x][y];
		printf("%d\n",t);
		mat[x][y]=0;
		for(int j=y;j<=M;++j)
			mat[x][j]=mat[x][j+1];
			
		for(int j=x;j<=N;++j)
			mat[j][M]=mat[j+1][M];
		mat[N][M]=t;
	}
}
};

int main()
{
	freopen("phalanx.in","r+",stdin);
	freopen("phalanx.out","w+",stdout);
	scanf("%d%d%d",&N,&M,&Q);
	if(M<=1010)
		sub3::deal_1();
	else if(N==1)
		sub3::deal_3();
	else
		sub3::deal_4();
	
	return 0;
}



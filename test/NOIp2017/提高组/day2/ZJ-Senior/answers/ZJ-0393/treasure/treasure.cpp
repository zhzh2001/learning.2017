#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <cmath>
#include <cctype>
#include <string>

#include <algorithm>
#include <queue>
#include <stack>

using namespace std;
int mat[20][20];
int N,M;
int dis[20],len[20];
int vis[20];
int G[20][20];
int ans=0x3f3f3f3f;
// code
void init()
{
	memset(mat,0x3f,sizeof mat);
	scanf("%d%d",&N,&M);
	//for(int i=1;i<=N;++i)	mat[i][i]=0;
	for(int i=1;i<=M;++i)
	{
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		mat[x][y]=min(mat[x][y],v);
		mat[y][x]=min(mat[y][x],v);
	}
}
void prim(int from)
{
	int ret=0;
	memset(dis,0x3f,sizeof dis);
	memset(len,0x3f,sizeof len);
	memset(vis,0,sizeof vis);
	dis[from]=0,len[from]=1;
	vis[from]=true;
	int tar=from;
	for(int i=1;i<N;++i)
	{
		for(int j=1;j<=N;++j)
		{
			if(mat[tar][j]<1e9&&len[tar]*mat[tar][j]<dis[j])
			{
				dis[j]=len[tar]*mat[tar][j];		
				len[j]=len[tar]+1;	
			}
		}
		tar=0;
		int minv=0x3f3f3f3f,minl=0x3f3f3f3f;
		for(int j=1;j<=N;++j)
		{
			if(vis[j])	continue;
			if(tar==0||dis[j]<minv)
			{
				tar=j;
				minv=dis[j];
				minl=len[j];
			}
		}
		if(tar==0)
			break;
		vis[tar]=true;
		ret+=dis[tar];
	}	
	ans=min(ans,ret);
	//printf("choose %d to begin ,tot=%d\n",from,ret);
}
void work()
{
	for(int i=1;i<=N;++i)
	{
		prim(i);
	}
	printf("%d\n",ans);
}
int main()
{
	freopen("treasure.in","r+",stdin);
	freopen("treasure.out","w+",stdout);
	init();
	work();
	return 0;
}

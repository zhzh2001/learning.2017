#include <cstdio>
#include <cstring>
using namespace std;
int d[10],pow[10],dis[10][10];
int n,m,ans;
int dp[6000000];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(dis,-1,sizeof(dis));
	for (int i = 1;i <= m;i++)
	{
		int x,y,dx;scanf("%d%d%d",&x,&y,&dx);
		if (dx < dis[x][y] || dis[x][y] == -1)	dis[x][y] = dx;
		dis[y][x] = dis[x][y];
	}
	pow[1] = 1;memset(dp,-1,sizeof(dp));ans = -1;
	for (int i = 2;i <= 9;i++) pow[i] = pow[i - 1] * 7;
	for (int i = 1;i <= n;i++) dp[pow[i]] = 0;
	for (int i = 1;i < pow[n + 1];i++)
		if (dp[i] != -1)
		{
			int cnt = 0;
			for (int j = 1;j <= n;j++)
				d[j] = i / pow[j] % 7;
			for (int j = 1;j <= n;j++)
				if (!d[j])
				{
					for (int k = 1;k <= n;k++)
					if (d[k] && dis[j][k] != -1)
					{
						if (d[k] < 6)
						{
							int to = i + pow[j] * (d[k] + 1);
							if (dp[to] > dp[i] + dis[j][k] * d[k] || dp[to] == -1)
								dp[to] = dp[i] + dis[j][k] * d[k];
						}
					}
					cnt++;
				}
			if (cnt == 0) 
			{
				if (dp[i] < ans || ans == -1)
					ans = dp[i];
			}
			if (cnt == 1)
			{
				for (int j = 1;j <= n;j++)
				if (d[j] == 6)
				{
					for (int k = 1;k <= n;k ++)
						if (!d[k])
						{	
							if ((ans > dp[i] + dis[j][k] * d[j] || ans == -1) && dis[j][k] != -1)
								ans = dp[i] + dis[j][k] * d[j];
						}
				}
			}
			if (cnt == 2)
			{
				int a,b;
				for (int j = 1;j <= n;j++)
					if (!d[j]) {a = j;break;}
				for (int j = n;j >= 1;j--)
					if (!d[j]) {b = j;break;}
				for (int j = 1;j <= n;j++)
				if (d[j] == 6)
				{
					if ((ans > dp[i] + dis[j][a] * 6 + dis[a][b] * 7 || ans == -1) && dis[j][a] != -1 && dis[a][b] != -1)
						ans = dp[i] + dis[j][a] * 6 + dis[a][b] * 7;
					if ((ans > dp[i] + dis[j][b] * 6 + dis[a][b] * 7 || ans == -1) && dis[j][b] != -1 && dis[a][b] != -1)
						ans = dp[i] + dis[j][b] * 6 + dis[a][b] * 7;
				}
			}
		}
	printf("%d\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
} 

#include <bits/stdc++.h>
#define N 1005
#define ll long long
using namespace std;
int cnt,T,n,h,r;
queue<int> Q;
int x[N],y[N],z[N],head[N],vis[N];
struct Edge
{
	int to,nxt;
}e[2 * N * N];
void add(int from,int to)
{
	e[++cnt] = (Edge){to,head[from]};head[from] = cnt;
}
ll dis(int i,int j)
{
	return (ll)(x[i] - x[j]) * (x[i] - x[j]) + (ll)(y[i] - y[j]) * (y[i] - y[j]) + (ll)(z[i] - z[j]) * (z[i] - z[j]);
}
void bfs()
{
	Q.push(0);memset(vis,0,sizeof(vis));vis[0] = 1;
	while (!Q.empty())
	{
		int u = Q.front();Q.pop();if (vis[n + 1]) break;
		for (int i = head[u],v;v = e[i].to,i != -1;i = e[i].nxt)
			if (!vis[v]) vis[v] = 1,Q.push(v);
	}
	while (!Q.empty()) Q.pop();
	if (vis[n + 1]) printf("Yes\n"); else printf("No\n");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--)
	{
		memset(head,-1,sizeof(head));cnt = -1;
		scanf("%d%d%d",&n,&h,&r);
		for (int i = 1;i <= n;i++)
		{
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
			if (z[i] <= r && z[i] >= -r) add(0,i);
			if (z[i] - h <= r && z[i] - h >= -r) add(i,n + 1);
		}
		for (int i = 1;i <= n;i++)
			for (int j = i + 1;j <= n;j++)
				if (dis(i,j) <= (ll)4 * r * r) add(i,j),add(j,i);
		bfs();
	}
	fclose(stdin);fclose(stdout);
	return 0;
}
 

#include<cstdio>
using namespace std;
inline char tc(void){
	static char fl[10000],*A=fl,*B=fl;
	return A==B&&(B=(A=fl)+fread(fl,1,10000,stdin),A==B)?EOF:*A++;
}
inline int read(void){
	int a=0;static char c;
	while((c=tc())<'0'||c>'9');
	while(c>='0'&&c<='9')a=a*10+c-'0',c=tc();
	return a;
}
int n,m,q,x[1001],y[1001],t[1000001],b[1000001];
inline void add(int x,int y){
	while(x<=600000)b[x]+=y,x+=x&-x;
	return ;
}
inline int sum(int x){
	int sum1=0;
	while(x)sum1+=b[x],x-=x&-x;
	return sum1;
}
inline int ef(int x){
	int l=0,r=600000,mid;
	while(l<=r){
		mid=l+r>>1;
		if(sum(mid)<x)l=mid+1;
		else r=mid-1;
	}
	return l;
}
int main(void){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	register int i,j,xf,yf,now;
	n=read(),m=read(),q=read();
	if(n!=1){
		for(i=1;i<=q;++i){
			x[i]=read(),y[i]=read(),
			xf=x[i],yf=y[i];
			for(j=i-1;j;--j){
				if(xf==n&&yf==m){
					xf=x[j],yf=y[j];
				}else{
					if(yf==m&&xf>=x[j])++xf;
					if(yf>=y[j]&&xf==x[j])++yf;
				}
			}
			printf("%d\n",(xf-1)*m+yf);
		}
	}else{
		for(i=1;i<=m;++i)t[i]=i,add(i,1);
		for(i=1;i<=q;++i){
			xf=read(),yf=read();
			add(now,-1),now=ef(yf),t[++m]=t[now],add(m,1);
			printf("%d\n",t[now]);
		}
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

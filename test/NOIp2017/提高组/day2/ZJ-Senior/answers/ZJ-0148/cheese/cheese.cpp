#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
#define ll long long
inline char tc(void){
	static char fl[10000],*A=fl,*B=fl;
	return A==B&&(B=(A=fl)+fread(fl,1,10000,stdin),A==B)?EOF:*A++;
}
inline int read(void){
	int a=0,f=1;char c;
	while((c=tc())<'0'||c>'9')c=='-'?f=-1:0;
	while(c>='0'&&c<='9')a=a*10+c-'0',c=tc();
	return a*f;
}
int T,n,h,r,x[1005],y[1005],z[1005],f[1005];
int gf(int x){
	return f[x]==x?x:f[x]=gf(f[x]);
}
bool dis(int i,int j){
	ll a=x[i]-x[j],b=y[i]-y[j],c=z[i]-z[j],o=4*r*r;
	if(o>=a*a)o-=a*a;
	else return 0;
	if(o>=b*b)o-=b*b;
	else return 0;
	if(o>=c*c)return 1;
	else return 0;
}
int main(void){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	register int i,j;
	T=read();
	while(T--){
		n=read(),h=read(),r=read();
		for(i=1;i<=n+2;++i)f[i]=i;
		for(i=1;i<=n;++i){
			x[i]=read(),y[i]=read(),z[i]=read();
			if(z[i]+r>=0&&z[i]<=0||z[i]>=0&&z[i]-r<=0)f[gf(i)]=gf(n+1);
			if(z[i]+r>=h&&z[i]<=h||z[i]>=h&&z[i]-r<=h)f[gf(i)]=gf(n+2);
		}
		for(i=1;i<=n;++i)
			for(j=i+1;j<=n;++j)
				if(dis(i,j))
					f[gf(i)]=gf(j);
		if(gf(n+1)==gf(n+2))puts("Yes");
		else puts("No");
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

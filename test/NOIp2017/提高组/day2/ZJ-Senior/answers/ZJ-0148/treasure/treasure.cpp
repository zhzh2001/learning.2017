#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,dp[13][13][4096],mp[13][13],ans=2e9;
bool vis[13][13][4096];
int dfs(int d,int x,int y){
	if(vis[d][x][y])return dp[d][x][y];
	vis[d][x][y]=1;
	int now=(y-=(1<<x-1));dp[d][x][y+(1<<x-1)]=2e9;
	for(;now!=0;now=(now-1)&y)
		for(int i=1;i<=n;++i)
			if(mp[x][i]!=1e9&&((1<<i-1)&now)){
				dp[d][x][y+(1<<x-1)]=min(dp[d][x][y+(1<<x-1)],dfs(d+1,i,now)+(d+1)*mp[x][i]+dfs(d,x,(1<<x-1)+(y-now)));
			}
	return dp[d][x][y+(1<<x-1)];
}
int main(void){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	register int i,j,x,y,z;
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;++i)
		for(j=i+1;j<=n;++j)
			mp[i][j]=mp[j][i]=1e9;
	for(i=1;i<=m;++i)
		scanf("%d%d%d",&x,&y,&z),mp[x][y]=mp[y][x]=min(mp[x][y],z);
	for(i=1;i<=n;++i)
		for(j=0;j<=n;++j)
			vis[j][i][1<<i-1]=1,dp[j][i][1<<i-1]=0;
	for(i=1;i<=n;++i)
		dfs(0,i,(1<<n)-1),ans=min(ans,dp[0][i][(1<<n)-1]);
	printf("%d\n",ans);
	fclose(stdin),fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;
struct Node
{
	int x,y,z,f;
} nod[1010];
int bfs[2010];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	for(int i=1;i<=t;++i)
	{
		int n,h;
		long long r;
		int ll=0,rr=0;
		scanf("%d%d%lld",&n,&h,&r);
//		printf("%d %d %lld\n",n,h,r);
		memset(nod,0,sizeof(nod));
		for(int j=1;j<=n;++j)
		{
			scanf("%d%d%d",&nod[j].x,&nod[j].y,&nod[j].z);
			if(nod[j].z<=r)
			{
				bfs[++rr]=j;
				nod[j].f=1;
			}
		}
//		for(int j=1;j<=n;++j) printf("%d",node[j].f);
//		printf("\n");
		bool flag=0;
		while(ll<rr)
		{
			int x=nod[bfs[++ll]].x,y=nod[bfs[ll]].y,z=nod[bfs[ll]].z;
			if(z+r>=h)
			{
//				printf("%d %lld %d\n",z,r,h);
				printf("Yes\n");
				flag=1;
				break;
			}
			for(int j=1;j<=n;++j)
			{
				if(!nod[j].f)
				{
					long long xx=x-nod[j].x,yy=y-nod[j].y,zz=z-nod[j].z;
//					printf("%lld %lld %lld\n",xx,yy,zz);
					if(xx*xx+yy*yy+zz*zz<=4*r*r)
					{
//						printf("%d ",j);
						bfs[++rr]=j;
						nod[j].f=1;
					}
				}
			}
		}
		if(!flag) printf("No\n");
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

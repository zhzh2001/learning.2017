#include<cstdio>
#include<climits>
#include<cstring>
using namespace std;
struct Edge
{
	int s,t,v,cv,next;
} e[1010];
bool f[20];
int init[20];
int head[20];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;++i)
	{
		int ti=i<<1;
		scanf("%d%d%d",&e[ti].s,&e[ti].t,&e[ti].v);
		e[ti+1].s=e[ti].t;
		e[ti+1].t=e[ti].s;
		e[ti+1].v=e[ti].v;
		e[ti].next=head[e[ti].s];
		head[e[ti].s]=ti;
		e[ti+1].next=head[e[ti+1].s];
		head[e[ti+1].s]=ti+1;
	}
	int ans_min=INT_MAX;
	for(int i=1;i<=n;++i)
	{
		memset(f,0,sizeof(f));
		memset(init,0,sizeof(init));
		f[i]=1;
		init[1]=i;
		int now=head[i];
		while(now)
		{
			e[now].cv=e[now].v;
			now=e[now].next;
		}
		int ans=0;
		for(int tot=1;tot<n;++tot)
		{
			int min_cv=INT_MAX,min_e=0;
			for(int j=1;j<=tot;++j)
			{
				int now=head[init[j]];
				while(now)
				{
					if(e[now].cv<min_cv&&!f[e[now].t])
					{
						min_cv=e[now].cv;
						min_e=now;
					}
					now=e[now].next;
				}
			}
			ans+=min_cv;
//			printf("%d ",ans);
			f[e[min_e].t]=1;
			init[tot+1]=e[min_e].t;
			now=head[e[min_e].t];
			while(now)
			{
				e[now].cv=(e[min_e].cv/e[min_e].v+1)*e[now].v;
				now=e[now].next;
			}
		}
		ans_min=ans<ans_min?ans:ans_min;
	}
	printf("%d",ans_min);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

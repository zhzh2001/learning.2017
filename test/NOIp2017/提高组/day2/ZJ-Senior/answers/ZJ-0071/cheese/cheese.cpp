#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
#define eps 1e-9
using namespace std;
#define N 1010
struct pp{int x,y,z;
}p[N];
int h,r,t,n,i,T,j,num,x,y;
int vis[N],c[N],head[N];
struct aa{int to,next;
}a[N*N*2];
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		{
			x=x*10+ch-'0';
			ch=getchar();
		}
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline void add(int x,int y)
{
	num++;
	a[num].next=head[x];
	a[num].to=y;
	head[x]=num;
}
inline double getdis(int x,int y)
{
	return 1.0*(p[x].x-p[y].x)*(p[x].x-p[y].x)+1.0*(p[x].y-p[y].y)*(p[x].y-p[y].y)+1.0*(p[x].z-p[y].z)*(p[x].z-p[y].z);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while (T)
		{
			T--;
			n=read();
			h=read();
			r=read();
			num=0;
			for (i=0;i<=n+1;i++) head[i]=vis[i]=0;
			for (i=1;i<=n;i++)
				{
					p[i].x=read();
					p[i].y=read();
					p[i].z=read();
					if (p[i].z<=r) add(0,i),add(i,0);
					if (p[i].z>=h-r) add(i,n+1),add(n+1,i);
				}
			for (i=1;i<=n;i++)
				for (j=i+1;j<=n;j++)
					if (getdis(i,j)-eps<=4.0*r*r) add(i,j),add(j,i);
			h=0;
			t=1;
			c[1]=0;
			vis[0]=1;
			while (h<t)
				{
					h++;
					x=c[h];
					for (i=head[x];i;i=a[i].next)
						{
							y=a[i].to;
							if (vis[y]) continue;
							t++;
							c[t]=y;
							vis[y]=1;
						}
				}
			if (vis[n+1]) puts("Yes");
			else puts("No");
		}
	return 0;
}

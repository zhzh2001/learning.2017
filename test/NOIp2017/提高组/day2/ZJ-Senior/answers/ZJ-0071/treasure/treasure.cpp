#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
#define M 5010
#define N 20
int f[M],ff[N],deep[N];
int dis[N][N];
int n,h,t,c[N],m,i,x,y,z,ans,last,flag;
#define inf 1000000000
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		{
			x=x*10+ch-'0';
			ch=getchar();
		}
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
inline int check(int x,int y)
{
	x--;
	y/=ff[x];
	return y%8;
}
inline void dfs(int cnt,int now,int val)
{
	int i,t,j;
	if (ans<=val) return;
	if (cnt==n)
		{
			ans=min(ans,val);
			return;
		}
	for (i=1;i<=n;i++)
		{
			t=check(i,now);
			if (!t) continue;
			for (j=1;j<=n;j++)
				{
					if (check(j,now)) continue;
					if (dis[i][j]==dis[0][0]) continue;
					dfs(cnt+1,now+ff[j-1]*(t+1),val+t*dis[i][j]);
				}
		}
}
inline int work(int x)
{
	int h,t,i,sum=0;
	h=0;
	t=1;
	c[1]=x;
	for (i=1;i<=n;i++) deep[i]=-1;
	deep[x]=0;
	while (h<t)
		{
			h++;
			x=c[h];
			for (i=1;i<=n;i++)
				if (dis[i][x]!=dis[0][0]&&deep[i]==-1)
					{
						deep[i]=deep[x]+1;
						t++;
						c[t]=i;
						sum+=deep[i];
					}
		}
	return sum;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();
	m=read();
	ff[0]=1;
	for (i=1;i<=15;i++) ff[i]=ff[i-1]*8;
	memset(dis,127,sizeof(dis));
	last=-1;
	for (i=1;i<=m;i++)
		{
			x=read();
			y=read();
			z=read();
			if (last==-1) last=z;
			if (last!=z) flag=1;
			dis[x][y]=min(dis[x][y],z);
			dis[y][x]=min(dis[y][x],z);
		}
	ans=inf;
	if (!flag)
		{
			for (i=1;i<=n;i++)
				ans=min(ans,work(i));
			write(ans*last);
			puts("");
			return 0;
		}
	for (i=1;i<=n;i++)
		dfs(1,ff[i-1],0);
	write(ans);
	return 0;
}
/*
5 5
1 2 3
2 3 3
2 4 3
3 4 3
1 5 3
*/

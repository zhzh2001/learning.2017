#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<set>
#include<ctime>
using namespace std;
#define N 510
#define M 50010
int a[N][M],hash[N];
int b[M];
int n,m,Q,t,i,j,x,y,cnt,k,nn;
#define O 6000010
struct qq{int x,y;
}q[N];
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		{
			x=x*10+ch-'0';
			ch=getchar();
		}
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
struct tt{
	int sum[O],val[O];
	int Max,now;
	inline void up(int k)
	{
		if (k*2<=Max) sum[k]=sum[k*2];
		if (k*2+1<=Max) sum[k]+=sum[k*2+1];
	}
	inline void build(int k,int l,int r)
	{
		int mid=(l+r)/2;
		Max=max(Max,k);
		if (l==r)
			{
				if (l<=m)
					{
						sum[k]=1;
						val[k]=l;
					}
				return;
			}
		build(k*2,l,mid);
		build(k*2+1,mid+1,r);
		up(k);
	}
	inline void up_d(int k,int l,int r,int pos)
	{
		int mid=(l+r)/2;
		if (l==r)
			{
				sum[k]=0;
				val[k]=0;
				return;
			}
		if (pos<=mid) up_d(k*2,l,mid,pos);
		else up_d(k*2+1,mid+1,r,pos);
		up(k);
	}
	inline void up_i(int k,int l,int r,int v)
	{
		int mid=(l+r)/2;
		if (l==r)
			{
				sum[k]=1;
				val[k]=v;
				return;
			}
		if (now<=mid) up_i(k*2,l,mid,v);
		else up_i(k*2+1,mid+1,r,v);
		up(k);
	}
	inline void query(int k,int l,int r,int rev)
	{
		int mid=(l+r)/2;
		if (l==r)
			{
				write(val[k]);
				puts("");
				now++;
				up_i(1,1,nn,val[k]);
				up_d(1,1,nn,l);
				return;
			}
		if (sum[k*2]<rev) return query(k*2+1,mid+1,r,rev-sum[k*2]);
		return query(k*2,l,mid,rev);
	}
}Tree;
inline int find(int x)
{
	int l=1,r=cnt,mid=(l+r)/2;
	while (l<r)
		{
			if (hash[mid]==x) return mid;
			if (hash[mid]<x) l=mid+1;
			else r=mid-1;
			mid=(l+r)/2;
		}
	return l;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read();
	m=read();
	Q=read();
	if (Q<=500)
		{
			for (i=1;i<=Q;i++)
				{
					q[i].x=read();
					cnt++;
					hash[cnt]=q[i].x;
					q[i].y=read();
				}
			sort(hash+1,hash+cnt+1);
			cnt=1;
			for (i=2;i<=Q;i++)
				if (hash[i]!=hash[cnt]) cnt++,hash[cnt]=hash[i];
			for (i=1;i<=cnt;i++)
				{
					for (j=1;j<m;j++)
						a[i][j]=(hash[i]-1)*m+j;
				}
			for (i=1;i<=n;i++) b[i]=i*m;
			for (i=1;i<=Q;i++)
				{
					x=q[i].x;
					y=q[i].y;
					k=find(x);
					t=a[k][y];
					if (y==m) t=b[x];
					write(t);
					puts("");
					for (j=y;j<m;j++) a[k][j]=a[k][j+1];
					if (y!=m) a[k][m-1]=b[x];
					for (j=x;j<n;j++) b[j]=b[j+1];
					b[n]=t;
				}
				return 0;
		}
	if (n==1)
		{
			Tree.now=m;
			nn=m+Q;
			Tree.build(1,1,nn);
			while (Q)
				{
					Q--;
					x=read();
					y=read();
					Tree.query(1,1,nn,y);
				}
			return 0;
		}
	return 0;
}

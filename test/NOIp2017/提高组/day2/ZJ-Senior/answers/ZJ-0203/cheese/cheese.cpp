#include <cstdio>
#include <cstring>

long long n, h, r, di, dd;
int ljb[1010][1010], d[1010] = {0};
long long x[1005], y[1005], z[1005];
int vis[1005], t;

long long re(void){
	long long x = 0; char ch = getchar();
	while (ch < '0'||ch > '9') ch = getchar();
	while (ch >= '0'&&ch <= '9'){ x = x * 10 + ch - '0'; ch = getchar();}
	return x;
}

int dfs(const int &i){
	if (i == n) return 1; vis[i] = 1;
	for (int j = 1; j <= d[i]; ++j)
		if ((vis[ljb[i][j]] == 0)&&(dfs(ljb[i][j]) == 1)){/*
			if (t == 6){ 
				printf("%d <%lld-- %d\n", ljb[i][j], (x[i] - x[ljb[i][j]])*(x[i] - x[ljb[i][j]])+(y[i] - y[ljb[i][j]])*(y[i] - y[ljb[i][j]])+(z[i] - z[ljb[i][j]])*(z[i] - z[ljb[i][j]]), i);
				printf("%d %lld %lld %lld || %d %lld %lld %lld\n", i, x[i], y[i], z[i], ljb[i][j], x[ljb[i][j]], y[ljb[i][j]], z[ljb[i][j]]);
			}*/
			return 1;
		}
	return 0;
}

int main(){
	int i, j;
	long long l;
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	for (t = re(); t > 0; --t){
		memset(d, 0, sizeof(d));
		memset(ljb, 0, sizeof(ljb));
		memset(vis, 0, sizeof(vis));
		n = re(); h = re(); r = re(); dd = r * r; di = dd << 2; ++n;
		for (i = 1; i < n; ++i){
			x[i] = re(); y[i] = re(); z[i] = re();
			if (z[i] * z[i] <= dd){
				ljb[0][++d[0]] = i;
				ljb[i][++d[i]] = 0;
			}if ((z[i] - h)*(z[i] - h)<= dd){
				ljb[n][++d[n]] = i;
				ljb[i][++d[i]] = n;
			}for (j = 1; j < i; ++j){
				l =(x[i] - x[j])*(x[i] - x[j])+(y[i] - y[j])*(y[i] - y[j])+(z[i] - z[j])*(z[i] - z[j]);
				if (l <= di){
					ljb[j][++d[j]] = i;
					ljb[i][++d[i]] = j;
				}
			}
		}if (dfs(0) == 1) printf("Yes\n");
			else printf("No\n");/*
		for (i = 0; i <= n; ++i){
			for (j = 1; j <= d[i]; ++j) printf("%d ", ljb[i][j]);
			printf("\n");
		}printf("\n");*/
		/*if (t == 7){
			printf("%lld %lld %lld %lld\n", n, h, r, di);
			for (i = 1; i < n; ++i) printf("%d %lld %lld %lld\n", i, x[i], y[i], z[i]);
			for (i = 0; i <= n; ++i){
				printf("%d %d ", i, d[i]);
				for (j = 1; j <= d[i]; ++j) printf("%d ", ljb[i][j]);
				printf("END\n");
			}
		}*/
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

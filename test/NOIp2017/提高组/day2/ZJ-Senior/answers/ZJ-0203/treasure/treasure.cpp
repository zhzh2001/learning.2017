#include <cstdio>
#include <cstring>

int map[15][15] = {0};
int vis[15], p, g;
long long ans, mi, d, uu;
int n, m, a, b, v;

int re(void){
	int x = 0; char ch = getchar();
	while (ch < '0'||ch > '9') ch = getchar();
	while (ch >= '0'&&ch <= '9'){ x = x * 10 + ch - '0'; ch = getchar();}
	return x;
}

long long dfs(const int &i, const int &c,const long long &ans){
	if ((++p) == n){
		--p;
		return 0;
	}vis[i] = 1;
	int j; long long te, lo = mi + 1 - ans;
	for (j = 2; j < i; ++j)
		if (vis[j] != 1 && map[i][j] > 0 && ans + map[i][j] * c <= mi + 1){
			te = map[i][j] * c + dfs(j, c + 1, ans + map[i][j] * c);
			if (te < lo) lo = te;
		}
	for (j = i + 1; j <= n; ++j)
		if (vis[j] != 1 && map[i][j] > 0 && ans + map[i][j] * c <= mi + 1){
			te = map[i][j] * c + dfs(j, c + 1, ans + map[i][j] * c);
			if (te < lo) lo = te;
		}
	if (i != g){
		--p; vis[g] = 0;
		te = dfs(g, 1, ans);
		if (te < lo) lo = te;
		++p; vis[g] = 1;
	}if (lo == mi + 1 - ans){
		--p; vis[i] = 0;
		return mi + 1;
	}else return lo;
}

int main(){
	int i, j;
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	n = re(); m = re();
	for (i = 0; i < m; ++i){
		a = re(); b = re(); v = re();
		if ((a != b)&&(map[a][b] == 0||map[a][b] > v)) map[b][a] = map[a][b] = v;
	}uu = 0; d = 1;
	while (d > 0) {
		mi = uu + d;
		for (i = 1; i <= n; ++i){
			ans = p = 0;
			memset(vis, 0, sizeof(vis));
			ans = dfs(g = i, 1, 0);
			if (ans <= mi) break;
		}if (i == n + 1){
			uu += d;
			d <<= 1;
		}else d >>= 1;
	}printf("%lld\n", mi);/*
	for (i = 1; i <= n; ++i){
		for (j = 1; j <= n; ++j) printf("%d ", map[i][j]);
		printf("\n");
	}*/
	fclose(stdin);
	fclose(stdout);
	return 0;
}

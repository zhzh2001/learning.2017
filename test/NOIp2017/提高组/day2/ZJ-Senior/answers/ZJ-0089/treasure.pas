var n,m,min,i,x,y,v,ans,j,num:longint;
    daniel:array[0..10] of boolean;
    dis:array[0..10,0..10] of longint;

procedure dfs(depth,i:longint);
var j:longint;
begin
  for j:=1 to n do
    if (dis[i,j]<maxlongint) and daniel[j] then
    begin
      inc(ans,depth*dis[i,j]);
      daniel[j]:=false;
      dfs(depth+1,j);
      daniel[j]:=true;
    end;
end;

begin
  assign(input,'treasure.in');reset(input);
  assign(output,'treasure.out');rewrite(output);

  fillchar(daniel,sizeof(daniel),true);
  readln(n,m);
  for i:=1 to n do
    for j:=1 to n do
      dis[i,j]:=maxlongint;
  for i:=1 to m do
  begin
    readln(x,y,v);
    dis[x,y]:=v;
    dis[y,x]:=v;
  end;
  min:=maxlongint;
  for i:=1 to n do
  begin
    ans:=0;
    daniel[i]:=false;
    dfs(1,i);
    daniel[i]:=true;
    if ans<min then min:=ans;
  end;
  writeln(min);

  close(input);close(output);
end.
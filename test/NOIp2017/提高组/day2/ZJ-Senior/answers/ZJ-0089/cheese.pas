var T,tt,k,n,h,r,i,j:longint;
    x,y,z:array[0..1005] of longint;
    f:array[0..1005,0..1005] of boolean;
    flag:boolean;
    daniel:array[0..1005] of boolean;
    num,chan:qword;

function dfs(i:longint):boolean;
var j:longint;
begin
  if f[i,n+1] then exit(true);
  for j:=1 to n do
    if daniel[j] and f[i,j] then
    begin
      daniel[j]:=false;
      exit(dfs(j));
      daniel[j]:=true;
    end;
  if i=tt then exit(false);
end;

begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);

  readln(T);
  for k:=1 to T do
  begin
    fillchar(f,sizeof(f),false);
    readln(n,h,r);
    if n*r*2<h then begin writeln('No'); continue; end;
    for i:=1 to n do readln(x[i],y[i],z[i]);
    for i:=1 to n do
      for j:=1 to n do
      begin
        num:=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
        chan:=4*r*r;
        if num<=chan then
          f[i,j]:=true;
      end;
    for i:=1 to n do f[i,i]:=false;
    for i:=1 to n do
      if z[i]<=r then f[0,i]:=true;
    for i:=1 to n do
      if h-z[i]<=r then f[i,n+1]:=true;
    flag:=false;
    for i:=1 to n do
      if f[0,i] then flag:=true;
    if flag=false then begin writeln('No'); continue; end;
    flag:=false;
    for i:=1 to n do
      if f[i,n+1] then flag:=true;
    if flag=false then begin writeln('No'); continue; end;
    flag:=false;
    fillchar(daniel,sizeof(daniel),true);
    for i:=1 to n do
      if f[0,i] then
      begin
        tt:=i;
        daniel[i]:=false;
        if dfs(i) then begin writeln('Yes'); flag:=true; break; end;
        daniel[i]:=true;
      end;
    if flag=true then continue;
    writeln('No');
  end;

  close(input);close(output);
end.
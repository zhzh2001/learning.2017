#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define sqr(x) (x)*(x)
using namespace std;
const int N=5001;
int n,m,q,a[N][N],x,y;

void read(int &x){
	x=0;char ch=getchar();int w=1;
	for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') w=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
	x*=w;
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	for(int i=1;i<=n;i++)
	 for(int j=1;j<=m;j++)
	  a[i][j]=(i-1)*m+j;
	while(q--){
		read(x);read(y);
		printf("%d\n",a[x][y]);
		int temp=a[x][y];
		for(int i=y;i<m;i++) a[x][i]=a[x][i+1];
		for(int i=x;i<n;i++) a[i][m]=a[i+1][m];
		a[n][m]=temp;
	}
	return 0;
}

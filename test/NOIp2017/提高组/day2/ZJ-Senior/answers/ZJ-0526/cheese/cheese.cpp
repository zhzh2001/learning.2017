#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
#define sqr(x) (x)*(x)
using namespace std;
const int N=1001;
int T,n,h,r,up[N],down[N],ans,f[N];
struct data{
	int x,y,z;
}a[N];

void read(int &x){
	x=0;char ch=getchar();int w=1;
	for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') w=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
	x*=w;
}

int cmp(const data q,const data w){
	return q.x<w.x;
}

int pd(int i,int j){
	double dis=sqrt(sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z));
	return dis<=r;
}

int gf(int x){
	return f[x]==x?x:f[x]=gf(f[x]);
}

void ymzunion(int x,int y){
	int fx=gf(x);int fy=gf(y);
	if ((up[fx]&&down[fy])||(down[fx]&&up[fy])) ans=1;
	if (up[fx]||down[fx]) f[fy]=fx;else f[fx]=fy;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--){
		read(n);read(h);read(r);
		r*=2;ans=0;
		for(int i=1;i<=n;i++)
		 read(a[i].x),read(a[i].y),read(a[i].z);
		sort(a+1,a+1+n,cmp);
		for(int i=1;i<=n;i++){
			if (abs(h-a[i].z)<=r/2) up[i]=1;else up[i]=0;
			if (abs(a[i].z)<=r/2) down[i]=1;else down[i]=0;
			f[i]=i;
			if (up[i]&&down[i]) ans=1;
		}
		for(int i=1;i<n;i++){
			for(int j=i+1;a[j].x-a[i].x<=r&&j<=n;j++)
		 	 if (pd(i,j)) ymzunion(i,j);
		 	if (ans) break;
		}
		if (ans) printf("Yes\n");else printf("No\n");
	//	if (T==6) printf("%d",n);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
using namespace std;
const int N=13;
int n,m,a[N][N],ans=2e9,sum,b[N],v,f[N][N];

void read(int &x){
	x=0;char ch=getchar();int w=1;
	for(;ch<'0'||ch>'9';ch=getchar()) if (ch=='-') w=-1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<3)+(x<<1)+ch-'0';
	x*=w;
}

void ymz(int root,int x,int k){
//	sum+=k*v;
	for(int i=1;i<=n;i++)
	 if (a[i][x]<0x3f&&(!b[i])){
	 	b[i]=1;
	 	sum+=(k+1)*a[i][x];
	 	ymz(root,i,k+1);
	 //	sum-=(k+1)*a[i][x];
	 }
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	memset(a,0x3f,sizeof a);
	for(int i=1;i<=n;i++) a[i][i]=0;
	for(int i=1;i<=m;i++){
		int x,y,z;
		read(x);read(y);read(z);
		a[x][y]=min(a[x][y],z);a[y][x]=min(a[y][x],z);
		v=z;
	}
/*	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++)
		 printf("%d ",a[i][j]);
		printf("\n");
	}*/
	if (m==n-1){
		for(int i=1;i<=n;i++){
			sum=0;memset(b,0,sizeof(b));b[i]=1;
			ymz(i,i,0);
			ans=min(ans,sum);
		}
	}else{
		for(int i=1;i<=n;i++){
			sum=0;
			for(int j=1;j<=n;j++)
			 sum+=a[i][j];
			ans=min(sum,ans);
		} 
	}
	cout<<ans;
	return 0;
}

#include<cstdio>
#include<cstring>
int n,m,ans,res,tot,c[13][13],vis[13],INF,p[1<<12];
int count(int x){int res=0;while (x)res+=x&1,x>>=1;return res;}
void dfs(int x,int d){
	vis[x]=1; if (tot==n){if (ans>res)ans=res;return;}
	int b[13]; memset(b,0,sizeof(b));
	for (int i=1;i<=n;i++)if (!vis[i]&&c[x][i]!=INF)
		b[++b[0]]=i;
	for (int i=0;i<(1<<b[0]);i++){
		tot+=count(i);printf("%d %d %d\n",x,d,tot);
		for (int j=i;j;j^=(j&(-j)))res+=c[x][b[p[j]]]*d;
		for (int j=i;j;j^=(j&(-j)))dfs(b[p[j]],d+1);
		if (tot==n)if (ans>res)ans=res;
		for (int j=i;j;j^=(j&(-j)))res-=c[x][b[p[j]]]*d;
		tot-=count(i);
	}
}
int main(){
	for (int i=1;i<=12;i++)p[1<<(i-1)]=i;
	for (int i=0;i<(1<<12);i++)p[i]=p[i&(-i)];
	freopen("treasure.in","r",stdin);
	//freopen("treasure0.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(c,63,sizeof(c));
	INF=c[0][0]; ans=INF;
	for (int i=1;i<=m;i++){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		if (c[x][y]>z)c[x][y]=z; c[y][x]=c[x][y];
	} for (int i=1;i<=n;i++)memset(vis,0,sizeof(vis)),tot=1,dfs(i,1);
	printf("%d\n",ans);
}

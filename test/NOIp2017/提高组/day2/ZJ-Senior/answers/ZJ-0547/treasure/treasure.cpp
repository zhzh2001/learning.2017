#include<cstdio>
#include<cstring>
int n,m,INF,c[15][15],f[13][13][1<<12],b[1<<12];
inline void min(int &x,const int y){if (x>y)x=y;}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	for (int i=1;i<=12;i++)b[1<<(i-1)]=i;
	for (int i=1;i<(1<<12);i++)b[i]=b[i&(-i)];
	scanf("%d%d",&n,&m);
	memset(c,63,sizeof(c));
	INF=c[0][0];
	for (int i=1;i<=m;i++){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		if (c[x][y]>z)c[x][y]=z; c[y][x]=c[x][y];
	} memset(f,63,sizeof(f));
	for (int i=1;i<=n;i++)for (int j=1;j<=n;j++)f[i][j][1<<(i-1)]=0;
	for (int i=n-1;i>=1;i--)
		for (int j=0;j<(1<<n);j++)
			for (int k=1;k<=n;k++)if (f[k][i+1][j]!=INF)
				for (int o=(((1<<n)-1)^j),t=o;t;t=(t-1)&o)
					for (int u=t,q;u;u^=(u&(-u)))if (f[(q=b[u])][i][t]!=INF&&c[q][k]!=INF)
						min(f[q][i][t|j],f[q][i][t]+f[k][i+1][j]+c[q][k]*i);
	int ans=INF; for (int i=1;i<=n;i++)min(ans,f[i][1][(1<<n)-1]);
	printf("%d\n",ans);
}

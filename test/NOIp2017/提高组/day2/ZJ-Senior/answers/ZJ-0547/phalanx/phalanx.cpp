#include<cstdio>
#include<cstring>
#include<cstdlib>
struct node{
	long long L,R; int c[2],s,len;
}p[600005*4];
int rt[300005],top,S[300005],cnt,n,m,q;
inline int max(const int x,const int y){return x>y?x:y;}
inline void ud(const int x){if (!x)p[0].s=0;else p[x].s=p[x].R-p[x].L+1+p[p[x].c[0]].s+p[p[x].c[1]].s,p[x].len=max(p[p[x].c[0]].len,p[p[x].c[1]].len)+1;}
int merge(int x,int y){
	if (!x||!y)return x+y;
	if (p[x].s<=5&&p[y].s>5){
		p[y].c[0]=merge(x,p[y].c[0]);
		ud(p[y].c[0]); ud(y); return y;
	}
	if (p[y].s<=5&&p[x].s>5){
		p[x].c[1]=merge(p[x].c[1],y);
		ud(p[x].c[1]); ud(x); return x;
	}
	if (rand()&1){
		p[x].c[1]=merge(p[x].c[1],y);
		ud(p[x].c[1]); ud(x); return x;
	}else{
		p[y].c[0]=merge(x,p[y].c[0]);
		ud(p[y].c[0]); ud(y); return y;}
}
void split(int now,int k,int &x,int &y){//printf("%d %d %d\n",now,p[now].c[0],p[now].c[1]);
	if (now==0){x=y=0;return;}
	if (p[p[now].c[0]].s+p[now].R-p[now].L+1>=k){
		y=now; split(p[now].c[0],k,x,p[now].c[0]);
		ud(y);
	}else{
		x=now; split(p[now].c[1],k-(p[p[now].c[0]].s+p[now].R-p[now].L+1),p[now].c[1],y);
		ud(x);
	}
}
void rd(int &x){x=0;int c=getchar();while (c<'0'||c>'9')c=getchar();while (c>='0'&&c<='9')x=x*10+c-'0',c=getchar();}
//void pt(int x){if (!x)return;pt(p[x].c[0]);printf("%lld %lld\n",p[x].L,p[x].R);pt(p[x].c[1]);}
int get(int &now,int k){
	int r1,r2,r3,tp; ++cnt; int res=cnt;//printf(" %d\n",p[now].s);
	split(now,k,r1,tp);//printf("%d %d\n",p[r1].s,p[tp].s);
	r2=tp; S[top=1]=r2; while (p[r2].c[0])r2=p[r2].c[0],S[++top]=r2;
	if (top>1)p[S[top-1]].c[0]=p[r2].c[1],r3=S[1];else r3=p[r2].c[1];
	p[r2].c[0]=p[r2].c[1]=0; p[r2].s=p[r2].R-p[r2].L+1; p[r2].len=0;
	for (int i=top-1;i>=1;i--)ud(S[i]);//printf("%d %d %d\n",p[r1].s,p[r2].s,p[r3].s);
	if (p[r2].L==p[r2].R){now=merge(r1,r3);p[res].L=p[res].R=p[r2].L;p[res].s=1;}else
	if (k-1==p[r1].s){p[r2].L++;p[r2].s--;now=merge(r1,merge(r2,r3));p[res].L=p[res].R=p[r2].L-1;p[res].s=1;}else
	if (k==p[r1].s+p[r2].R-p[r2].L+1){p[r2].R--;p[r2].s--;now=merge(r1,merge(r2,r3));p[res].L=p[res].R=p[r2].R+1;p[res].s=1;}
	else{
		++cnt; k-=p[r1].s;//printf(" %d\n",p[r2].s);
		p[cnt].L=p[r2].L+k; p[cnt].R=p[r2].R; p[cnt].s=p[cnt].R-p[cnt].L+1;
		p[res].L=p[res].R=p[cnt].L-1;p[res].s=1;
		p[r2].R=p[r2].L+k-2; p[r2].s=p[r2].R-p[r2].L+1;
		//printf("%d %d\n",p[r2].s,p[cnt].s);
		now=merge(r1,merge(merge(r2,cnt),r3));
	} return res;
}
void build(int &now,int L,int R){
	if (L>R){now=0;return;}
	int M=(L+R)>>1; now=M;
	build(p[M].c[0],L,M-1); build(p[M].c[1],M+1,R);
	ud(M);
}
void dfs(int x){if (!x)return;dfs(p[x].c[0]);S[++top]=x;dfs(p[x].c[1]);}
void rre(int &now,int L,int R){
	if (L>R){now=0;return;}
	int M=(L+R)>>1; now=S[M];
	rre(p[now].c[0],L,M-1); rre(p[now].c[1],M+1,R);
	ud(now);
}
void rebuild(int &x){
	top=0; dfs(x);
	rre(x,1,top);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q); srand(613);
	for (int i=1;i<=n;i++)
		++cnt,p[cnt].L=p[cnt].R=1LL*m*i,p[cnt].s=1;
	build(rt[0],1,cnt);
	for (int i=1;i<=n;i++){
		++cnt; p[cnt].L=1LL*m*i-m+1; p[cnt].R=1LL*m*i-1; p[cnt].s=m-1;
		rt[i]=merge(rt[i],cnt);
	}
	for (int i=1;i<=q;i++){
		int x,y; rd(x),rd(y);
		if (y==m)printf("%lld\n",getk(rt[0],x));
		else printf("%lld\n",getk(rt[x],y));
		if (y==m){int z=get(rt[0],x);rt[0]=merge(rt[0],z);//if (p[rt[0]].len>=1000)rebuild(rt[0]);
		}
		else {
			int z=get(rt[x],y); int o=get(rt[0],x);
			rt[x]=merge(rt[x],o); rt[0]=merge(rt[0],z);
			if (p[rt[x]].len>=1000)rebuild(rt[x]);                                               }
		}//if (i%1000==0)printf("%d\n",i);
	}
}                    

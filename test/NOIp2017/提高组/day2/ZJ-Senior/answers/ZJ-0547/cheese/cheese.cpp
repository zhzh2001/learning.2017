#include<cstdio>
#include<cstring>
int n,h,r,tot,lnk[1005],son[3000005],nxt[3000005],vis[1005],hd,tl,q[1005];
void add(int x,int y){nxt[++tot]=lnk[x];lnk[x]=tot;son[tot]=y;}
struct Point{
	int x,y,z; void rd(){scanf("%d%d%d",&x,&y,&z);}
}P[1005];
long long sqr(int x){return 1LL*x*x;}
long long get(Point x,Point y){
	return sqr(x.x-y.x)+sqr(x.y-y.y)+sqr(x.z-y.z);
}
bool work(int x,int y){
	if (y==0)return P[x].z<=r;
	if (y==n+1)return h-P[x].z<=r;
	return 4LL*r*r>=get(P[x],P[y]);
}
void bfs(int S){
	memset(vis,0,sizeof(vis)); vis[S]=1;
	hd=tl=0; q[++tl]=S;
	while (hd!=tl){
		int x=q[++hd];
		for (int i=lnk[x];i;i=nxt[i])if (!vis[son[i]])
			vis[son[i]]=1,q[++tl]=son[i];
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T; scanf("%d",&T);
	while (T--){
		tot=0; memset(lnk,0,sizeof(lnk));
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1;i<=n;i++){
			P[i].rd();
			for (int j=0;j<i;j++)
				if (work(i,j))add(i,j),add(j,i);
			if (work(i,n+1))add(i,n+1),add(n+1,i);
		} bfs(0);
		if (vis[n+1])puts("Yes");else puts("No");
	}
}

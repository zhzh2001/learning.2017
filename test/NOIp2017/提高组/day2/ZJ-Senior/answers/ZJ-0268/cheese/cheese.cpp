#include <cstdio>
#define LL long long
using namespace std;
int n,h,r,x[1010],y[1010],z[1010];
inline LL sqr(int x){return 1ll*x*x;}
int edg,to[1100000],nxt[1100000],fst[1010];
inline void ins(int x,int y){
	to[edg]=y,nxt[edg]=fst[x],fst[x]=edg++;
	to[edg]=x,nxt[edg]=fst[y],fst[y]=edg++;
}
int q[1010],vis[1010];
void bfs(){
	int h=0,t=1;
	for (int i=0;i<=n+1;++i) vis[i]=0;
	q[1]=0;vis[0]=1;
	while (h<t){
		++h;
		for (int i=fst[q[h]];i>=0;i=nxt[i]) if (!vis[to[i]]){
			q[++t]=to[i];
			vis[to[i]]=1;
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);while (T--){
		 scanf("%d%d%d",&n,&h,&r);
		 LL rr=sqr(r*2);
		 edg=0;for (int i=0;i<=n+1;++i) fst[i]=-1;
		 for (int i=1;i<=n;++i){
		 	scanf("%d%d%d",&x[i],&y[i],&z[i]);
		 	for (int j=1;j<i;++j) if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=rr) ins(j,i),ins(i,j);
		 	if (z[i]<=r&&z[i]>=-r) ins(0,i),ins(i,0);
		 	if (z[i]>=h-r&&z[i]<=h+r) ins(i,n+1),ins(n+1,i);
		 }
		 bfs();
		 if (vis[n+1]) puts("Yes");else puts("No");
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

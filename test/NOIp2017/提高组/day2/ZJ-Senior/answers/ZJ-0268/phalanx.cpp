#include <cstdio>
#include <map>
#define LL long long
using namespace std;
char ch;
LL n,m,q,x,y;
inline void read(LL &x){
	x=0;ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
}
map<LL,LL>h;
inline void solve1(){
	h.clear();
	while (q--){
		read(x),read(y);
		LL now=h[(x-1)*m+y],nxt;
		if (now==0) now=(x-1)*m+y;
		printf("%lld\n",now);
		for (int i=y;i<m;++i){
			nxt=h[(x-1)*m+i+1];if (nxt==0) nxt=(x-1)*m+i+1;
			h[(x-1)*m+i]=nxt;
		}
		for (int i=x;i<n;++i){
			nxt=h[(i+1)*m];if (nxt==0) nxt=(i+1)*m;
			h[i*m]=nxt;
		}
		h[n*m]=now;
	}
}
int t[10000010];
void add(int o,int l,int r,int x,int y){
	if (l==r){t[o]+=y;return;}
	int mid=(l+r)>>1;
	if (x<=mid) add(o<<1,l,mid,x,y);
	else add(o<<1|1,mid+1,r,x,y);
	t[o]=t[o<<1]+t[o<<1|1];
}
LL ans;
void query(int o,int l,int r,int x){
	if (l==r){ans=l;return;}
	int mid=(l+r)>>1;
	if (t[o<<1]>=x) query(o<<1,l,mid,x);
	else query(o<<1|1,mid+1,r,x-t[o<<1]);
}
LL a[1000010];
inline void solve2(){
	int tot=n+m+q,cnt=0;
	for (int i=1;i<=m;++i) a[++cnt]=i,add(1,1,tot,cnt,1);
	for (int i=2;i<=n;++i) a[++cnt]=i*m,add(1,1,tot,cnt,1);
	while (q--){
		read(x),read(y);
		if (x!=1) return;
		query(1,1,tot,y);
		printf("%lld\n",a[ans]);
		add(1,1,tot,ans,-1);
		a[++cnt]=a[ans];
		add(1,1,tot,cnt,1);
	}
}
int main(){
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
	read(n),read(m),read(q);
	if ((n+m)*q<=50000000) solve1();
	else solve2();
	fclose(stdin);fclose(stdout);
	return 0;
}

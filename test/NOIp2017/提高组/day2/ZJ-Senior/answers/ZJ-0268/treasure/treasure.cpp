#include <cstdio>
using namespace std;
int n,m,cnt,ans,q[15],vis[15],dis[15],mm,mn[15][15],a[15][15];
void dfs(int x,int y){
	if (x==n+1){if (y<ans) ans=y;return;}
	if (y+mm*(n-x+1)>ans) return;
	for (int i=1;i<=n;++i) if (!vis[i]){
		q[x]=i;vis[i]=1;
		//for (int j=1;j<x;++j) if (a[q[j]][i]!=-1) dis[i]=dis[q[j]]+1,dfs(x+1,y+a[q[j]][i]*dis[q[j]]);
		for (int j=1;j<x;++j) mn[x][j]=50001;
		for (int j=1;j<x;++j) if (a[q[j]][i]<mn[x][dis[q[j]]]&&a[q[j]][i]!=-1) mn[x][dis[q[j]]]=a[q[j]][i];
		for (int j=1;j<x;++j) if (mn[x][j]<=50000) dis[i]=j+1,dfs(x+1,y+j*mn[x][j]);
		vis[i]=0;
	}
}
int main(){
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i) for (int j=1;j<=n;++j) a[i][j]=-1;
	for (int i=1,x,y,z;i<=m;++i){
		scanf("%d%d%d",&x,&y,&z);
		if (a[x][y]==-1||z<a[x][y]) a[x][y]=a[y][x]=z;
	}
	ans=1e9;cnt=0;
	for (int i=1;i<=n;++i){
		mm=50001;for (int j=1;j<=n;++j) if (a[i][j]!=-1&&a[i][j]<mm) mm=a[i][j];
		for (int j=1;j<=n;++j) vis[j]=0;
		q[1]=i,dis[i]=1,vis[i]=1;
		dfs(2,0);
	}
	printf("%d",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<cstdio>
int n,m,q,Ans[1005][1005];
int main()
{
	int i,j,x,y,z;
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (i=1;i<=n;i++)
	for (j=1;j<=m;j++) Ans[i][j]=(i-1)*m+j;
	while (q--)
	{
		scanf("%d%d",&x,&y);
		printf("%d\n",Ans[x][y]);
		z=Ans[x][y];
		for (i=y;i<m;i++) Ans[x][i]=Ans[x][y+1];
		for (i=x;i<n;i++) Ans[i][m]=Ans[i+1][m];
		Ans[n][m]=z;
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

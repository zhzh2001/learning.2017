#include<cstdio>
#include<cstring>
typedef long long ll;
const ll INF=1000000000;
int n,m,deep[15],que[15],w[15]; int bbb;
ll ans,lowcost[15],cost[15][15];
void prim(int b)
{
	int i,j,k,s=0;
	memset(lowcost,63,sizeof(lowcost));
	lowcost[b]=0; w[b]=1;
	for (i=1;i<=n;i++)
	if (cost[b][i]<INF&&cost[b][i]*w[b]<lowcost[i]) lowcost[i]=cost[b][i]*w[b],w[i]=w[b]+1;
	for (i=1;i<n;i++)
	{
		k=0;
		for (j=1;j<=n;j++)
		if (lowcost[j]&&lowcost[j]<lowcost[k]) k=j;
		s+=lowcost[k];
		if (s>=ans) return;
		lowcost[k]=0;
		for (j=1;j<=n;j++)
		if (cost[k][j]<INF&&cost[k][j]*w[k]<lowcost[j]) lowcost[j]=cost[k][j]*w[k],w[j]=w[k]+1;
	}
	ans=s;
}
void dfs(int x,ll s)
{
	int i,j;
	if (x>n)
	{
		ans=s;
		return;
	}
	for (i=1;i<=n;i++)
	if (w[i]) for (j=1;j<=n;j++)
	if (!w[j]&&cost[i][j]<INF)
	{
		w[j]=w[i]+1;
		if (s+cost[i][j]*w[i]<ans) dfs(x+1,s+cost[i][j]*w[i]);
		w[j]=0;
	}
}
int main()
{
	int i,x,y; ll z;
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(cost,63,sizeof(cost));
	ans=cost[0][0];
	while (m--)
	{
		scanf("%d%d%lld",&x,&y,&z);
		if (z<cost[x][y]) cost[x][y]=z,cost[y][x]=z;
	}
	for (i=1;i<=n;i++) prim(i);
	if (n<=10)
	for (i=1;i<=n;i++)
	{
		memset(w,0,sizeof(w));
		w[i]=1;
		dfs(2,0);
	}
	printf("%lld",ans);
	fclose(stdin); fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cmath>
#include<cstring>
int t,n,m,tot,Que[1005],Start[1005],Son[2000005],Next[2000005];
long long r,a[1005],b[1005],c[1005];
bool Vis[1005];
void add(int x,int y)
{
	Son[++tot]=y; Next[tot]=Start[x]; Start[x]=tot;
}
void bfs()
{
	int j,Head=0,Tail=1;
	memset(Vis,0,sizeof(Vis));
	Vis[0]=1; Que[1]=0;
	while (Head^Tail)
	for (j=Start[Que[++Head]];j;j=Next[j])
	if (!Vis[Son[j]]) Vis[Son[j]]=1,Que[++Tail]=Son[j];
}
int main()
{
	int i,j;
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%lld",&n,&m,&r);
		for (i=1;i<=n;i++) scanf("%d%d%d",&a[i],&b[i],&c[i]);
		tot=0; memset(Start,0,sizeof(Start));
		for (i=1;i<=n;i++)
		{
			if (c[i]<=r) add(0,i);
			if (m-c[i]<=r) add(i,n+1);
			for (j=i+1;j<=n;j++)
			if (pow(a[i]-a[j],2)+pow(b[i]-b[j],2)+pow(c[i]-c[j],2)<=r*r*4) add(i,j),add(j,i);
		}
		bfs();
		printf("%s\n",Vis[n+1]?"Yes":"No");
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

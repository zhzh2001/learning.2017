#include<set>
#include<map>
#include<ctime>
#include<queue>
#include<cstdio>
#include<cctype>
#include<string>
#include<bitset>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=12;
const int M=540000;
const int T=15;
const int oo=(1<<30)-1;

int dp[N][M],A[1<<N][1<<N];
int w[N][N],mn[N];
int pw3[T],to2[M],to3[1<<N],to4[M];
int n,m,x,y,z,ans;

inline void upd(int &x,int y){
	if (y<x) x=y;
}
void dfs(int s,int t,int i,ll sum){
	if (i==n){
		A[s][t]=(sum>oo?oo:sum);
		return;
	}
	dfs(s,t,i+1,sum);
	if (!(s>>i&1)) dfs(s,t|1<<i,i+1,sum+mn[i]);
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	pw3[0]=1;
	For(i,1,T) pw3[i]=3*pw3[i-1];
	n=IN(),m=IN();
	For(i,0,n) For(j,0,n) w[i][j]=oo;
	For(i,0,m){
		x=IN()-1,y=IN()-1,z=IN();
		w[x][y]=w[y][x]=min(w[x][y],z);
	}
	For(s,0,1<<n){
		For(i,0,n) mn[i]=oo;
		For(i,0,n) if (s>>i&1){
			For(j,0,n) if (!(s>>j&1)){
				mn[j]=min(mn[j],w[i][j]);
			}
		}
		dfs(s,0,0,0);
	}
	For(w,0,pw3[n]){
		For(i,0,n){
			if (w/pw3[i]%3==1) to2[w]|=1<<i;
			if (w/pw3[i]%3!=0) to4[w]|=1<<i;
		}
	}
	For(w,0,1<<n){
		For(i,0,n) if (w>>i&1) to3[w]+=pw3[i];
	}
	For(i,0,N) For(j,0,M) dp[i][j]=oo;
	For(i,0,n) dp[0][pw3[i]]=0;
	ans=oo;
	For(i,0,n) For(w,0,pw3[n]) if (dp[i][w]!=oo){
		int s=to2[w];
		if (to4[w]==(1<<n)-1) upd(ans,dp[i][w]);
		for (int u=~to4[w]&(1<<n)-1,t=u;t;t=(t-1)&u)
			if (A[s][t]!=oo){
				upd(dp[i+1][w+to3[s|t]],dp[i][w]+(i+1)*A[s][t]);
			}
	}
	printf("%d\n",ans);
}

#include<set>
#include<map>
#include<queue>
#include<cstdio>
#include<cctype>
#include<string>
#include<bitset>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=1000+19;

int A[N][N];
int n,m,q,tot,x,y;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=IN(),m=IN(),q=IN();
	For(i,1,n+1) For(j,1,m+1) A[i][j]=++tot;
	while (q--){
		x=IN(),y=IN();
		int tmp=A[x][y];
		For(i,y,m) A[x][i]=A[x][i+1];
		For(i,x,n) A[i][m]=A[i+1][m];
		A[n][m]=tmp;
		printf("%d\n",tmp);
	}
}

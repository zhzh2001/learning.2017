#include<set>
#include<map>
#include<ctime>
#include<queue>
#include<cstdio>
#include<cctype>
#include<string>
#include<bitset>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}

int n,m,q;

int main(){
	freopen("phalanx.in","w",stdout);
	srand(time(0));
	
	n=300000;
	m=300000;
	q=300000;
	//n=R(1,1000);
	//m=R(1,1000);
	//q=R(1,1000);
	
	printf("%d %d %d\n",n,m,q);
	For(i,0,q){
		printf("%d %d\n",R(1,n),R(1,m));
	}
}

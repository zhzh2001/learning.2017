#include<set>
#include<map>
#include<ctime>
#include<queue>
#include<cstdio>
#include<cctype>
#include<string>
#include<bitset>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
#define Mid (L+R>>1)
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

const int len=200000;
inline char nc(){
	static char buf[len],*b=buf+len;
	if (b==buf+len) fread(buf,1,len,stdin),b=buf;
	return *b++;
}
int IN(){
	int c,f,x;
	while (!isdigit(c=nc())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=nc())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}
void Output(ll x){
	if (x>=10) Output(x/10);
	putchar(x%10+'0');
}

const int N=300000+19;
const int Top=20000000;
const int top=1000000;

struct node{
	node *L,*R;
	int sum;
} Nd[Top],*cur=Nd+1,*null;

int n,m,q,x,y;
ll tmp;

int Work(node *x,int L,int R,int k){
	if (L==R) return L;
	if (k<=(Mid-L+1)-x->L->sum) return Work(x->L,L,Mid,k);
	return Work(x->R,Mid+1,R,k-((Mid-L+1)-x->L->sum));
}
void gao(node *&x,int L,int R,int k){
	if (x==null){
		x=cur++;
		*x=(node){null,null,0};
	}
	x->sum++;
	if (L==R) return;
	k<=Mid?gao(x->L,L,Mid,k):gao(x->R,Mid+1,R,k);
}

struct Info{
	node *rt;
	vector<ll> V;
	int id;
	ll getkth(int k){
		int x=Work(rt,1,top,k);
		gao(rt,1,top,x);
		if (id==0){
			return x<=n?1ll*x*m:V[x-n-1];
		} else{
			return x<=m-1?1ll*(id-1)*m+x:V[x-(m-1)-1];
		}
	}
} F[N];

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	null=Nd;
	*null=(node){null,null,0};
	n=IN(),m=IN(),q=IN();
	For(i,0,n+1){
		F[i].id=i;
		F[i].rt=null;
	}
	while (q--){
		x=IN(),y=IN();
		if (y==m){
			tmp=F[0].getkth(x);
			Output(tmp);
			puts("");
			F[0].V.pb(tmp);
		} else{
			tmp=F[x].getkth(y);
			Output(tmp);
			puts("");
			F[0].V.pb(tmp);
			tmp=F[0].getkth(x);
			F[x].V.pb(tmp);
		}
	}
	//fprintf(stderr,"%d\n",clock());
}

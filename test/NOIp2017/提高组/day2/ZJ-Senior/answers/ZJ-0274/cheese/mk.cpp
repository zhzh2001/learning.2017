#include<set>
#include<map>
#include<ctime>
#include<queue>
#include<cstdio>
#include<cctype>
#include<string>
#include<bitset>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int top=100000000;

int R(){
	return rand()<<15|rand();
}
int R(int l,int r){
	return R()%(r-l+1)+l;
}

int main(){
	freopen("cheese.in","w",stdout);
	srand(time(0));
	puts("20");
	For(T,0,20){
		int n=1000;
		printf("%d %d %d\n",n,1000000000,100000000);
		For(i,0,n) printf("%d %d %d\n",R(-top,top),R(-top,top),R(-top,top));
	}
}

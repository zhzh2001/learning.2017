#include<set>
#include<map>
#include<queue>
#include<cstdio>
#include<cctype>
#include<string>
#include<bitset>
#include<cstring>
#include<cassert>
#include<iostream>
#include<algorithm>
#include<functional>

#define For(i,x,y) for (int i=x;i<y;i++)
#define pb push_back
#define mp make_pair
#define fi first
#define se second
#define lf else if
using namespace std;

typedef long long ll;
typedef double db;
typedef pair<int,int> pii;

int IN(){
	int c,f,x;
	while (!isdigit(c=getchar())&&c!='-');c=='-'?(f=1,x=0):(f=0,x=c-'0');
	while (isdigit(c=getchar())) x=(x<<1)+(x<<3)+c-'0';return !f?x:-x;
}

const int N=1000+19;

int A[N][N];
int vis[N],Q[N],x[N],y[N],z[N];
int n,h,r,f,w;

ll cal(int i,int j){
	return 1ll*(x[i]-x[j])*(x[i]-x[j])+1ll*(y[i]-y[j])*(y[i]-y[j])+1ll*(z[i]-z[j])*(z[i]-z[j]);
}
void Main(){
	n=IN(),h=IN(),r=IN();
	For(i,1,n+1){
		x[i]=IN(),y[i]=IN(),z[i]=IN();
	}
	For(i,1,n+1) For(j,i+1,n+1){
		A[i][j]=A[j][i]=(cal(i,j)<=4ll*r*r);
	}
	memset(vis,0,sizeof(vis));
	f=0,w=0;
	For(i,1,n+1) if (abs(z[i])<=r){
		vis[i]=1;
		Q[++f]=i;
		if (abs(z[i]-h)<=r){
			puts("Yes");
			return;
		}
	}
	while (f>w){
		int x=Q[++w];
		For(i,1,n+1) if (A[x][i]&&!vis[i]){
			vis[i]=1;
			Q[++f]=i;
			if (abs(z[i]-h)<=r){
				puts("Yes");
				return;
			}
		}
	}
	puts("No");
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	for (int T=IN();T--;) Main();
}

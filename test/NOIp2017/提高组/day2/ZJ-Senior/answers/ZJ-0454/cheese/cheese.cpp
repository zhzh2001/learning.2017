#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
typedef long long ll;
typedef unsigned long long ull;
using namespace std;
const int N = 1200;
int n,h,r,x[N],y[N],z[N],f[N];
inline int find(int x) {
	if (f[x]==x) return x;
	f[x]=find(f[x]); return f[x];
}
inline void merge(int x, int y) {
//	printf("merge %d %d\n",x,y);
	int fx=find(x),fy=find(y);
	if (fx!=fy) f[fx]=fy;
}
inline ull sqr(int x) {return (ull)x*x;}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t; scanf("%d",&t);
	while (t--) {
		scanf("%d%d%d",&n,&h,&r);
		rep(i,1,n) scanf("%d%d%d",&x[i],&y[i],&z[i]);
		rep(i,1,n+2) f[i]=i;
		rep(i,1,n) rep(j,i+1,n) 
			if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=sqr(2*r)) merge(i,j);
		rep(i,1,n) if ((ll)z[i]<=r) merge(i,n+1);
		rep(i,1,n) if ((ll)(h-z[i])<=r) merge(i,n+2);
		if (find(n+1)==find(n+2)) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
typedef long long ll;
using namespace std;
const int N = 301000;
int n,m,q,x[N],y[N],bh[N<<1];
int num[1020][1020],cnt;
inline void read(int &x) {
	x=0; char c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
}
inline void Violent() {
	rep(i,1,n) rep(j,1,m) num[i][j]=(i-1)*m+j;
	rep(k,1,q) {
		int a=x[k],b=y[k]; int tmp=num[a][b];
		rep(j,b,m-1) num[a][j]=num[a][j+1];
		rep(i,a,n-1) num[i][m]=num[i+1][m];
		num[n][m]=tmp; printf("%d\n",tmp);
	}
}
struct BIT {
	int v[N<<1];
	inline void add(int p, int val) {
		while (p<=(m+q)) {v[p]+=val; p+=p&-p;}
	}
	inline int query(int p) {
		int res=0;
		while (p) {res+=v[p]; p-=p&-p;}
		return res;
	}
} tree;
inline void solve(int k) {
	int l=1,r=cnt,ans=0;
	while (l<=r) {
		int mid=(l+r)>>1;
		if (tree.query(mid)>=k) {ans=mid; r=mid-1;}
		else {l=mid+1;}
	}
	printf("%d\n",bh[ans]); tree.add(ans,-1); tree.add(++cnt,1);
	bh[cnt]=bh[ans];
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n); read(m); read(q);
	rep(i,1,q) read(x[i]),read(y[i]);
	if (n==1) {
		rep(i,1,m) tree.add(i,1),bh[i]=i; cnt=m;
		rep(i,1,q) solve(y[i]); return 0;
	}
	Violent();
	return 0;
}

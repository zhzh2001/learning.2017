#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<vector>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
typedef long long ll;
using namespace std;
const int N = 301000;
int n,m,q,x[N],y[N];
int num[1020][1020];
vector<int> vec;
inline void read(int &x) {
	x=0; char c=getchar();
	while (c<'0'||c>'9') c=getchar();
	while (c>='0'&&c<='9') {x=x*10+c-'0'; c=getchar();}
}
inline void Violent() {
	rep(i,1,n) rep(j,1,m) num[i][j]=(i-1)*m+j;
	rep(k,1,q) {
		int a=x[k],b=y[k]; int tmp=num[a][b];
		rep(j,b,m-1) num[a][j]=num[a][j+1];
		rep(i,a,n-1) num[i][m]=num[i+1][m];
		num[n][m]=tmp; printf("%d\n",tmp);
	}
}
inline void solve() {
	rep(i,0,m) vec.push_back(i);
	rep(k,1,q) {
		int p=y[k],tmp=vec[p]; vec.erase(vec.begin()+p);
		vec.push_back(tmp); printf("%d\n",tmp);
	}
}
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("my.out","w",stdout);
	read(n); read(m); read(q);
	rep(i,1,q) read(x[i]),read(y[i]);
	if (n==1) {solve(); return 0;}
//	Violent();
	return 0;
}

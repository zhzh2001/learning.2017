#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
typedef long long ll;
using namespace std;
const int N = 13, inf = 0x3f3f3f3f;
int n,m,dis[N][N];
struct EE {
	int a,b,c;
} e[N*N];
int edgenum;
ll ans=1e13;
bool chosen[N*N];
int cnt;
/*inline void dfs(int u) {
	if (u==n-1) {
		int rec=len;
		rep(i,)
		cnt++; //if (lenn-1) puts("OwO");
		return;
	}
	int size=0; rep(i,u+1,n) if (!chosen[i]&&dis[u][i]<inf) a[size++]=i;
	int rec=len;
	rep(i,0,(1<<size)-1) {
		int tmp=i; 
		while (tmp) {
			int Bit=tmp&-tmp; tmp-=Bit; Bit=a[dy[Bit]]; x[++len]=u; y[len]=Bit; chosen[Bit]=1;
		}
		dfs(u+1);
		tmp=i; len=rec;
		while (tmp) {int Bit=tmp&-tmp; tmp-=Bit; Bit=a[dy[Bit]]; chosen[Bit]=0;}
	}
}*/
int f[N];
inline int find(int x) {
	if (f[x]==x) return x;
	f[x]=find(f[x]); return f[x];
}
inline void merge(int x, int y) {
//	printf("merge %d %d\n",x,y);
	int fx=find(x),fy=find(y);
	if (fx!=fy) f[fx]=fy;
}
struct Graph {
	int head[N],edgenum,to[N<<1],nxt[N<<1],len[N<<1];
	inline void init() {edgenum=0; memset(head,0,sizeof(head));}
	inline void add(int u, int v, int d) {
		to[++edgenum]=v; len[edgenum]=d;
		nxt[edgenum]=head[u]; head[u]=edgenum;
	}
	inline void getans(int u, int fa, int &Ans, int depth) {
		if (Ans>=ans) return;
		for (int i=head[u]; i!=0; i=nxt[i]) if (to[i]!=fa) {
			Ans+=depth*len[i]; getans(to[i],u,Ans,depth+1);	
		}
	}
} G;
inline void dfs(int depth, int cur) {
	if (cur>n-1) return;
	if (edgenum-depth+1+cur<n-1) return;
	if (depth>edgenum) {
		rep(i,1,n) f[i]=i; bool flag=1; G.init();
		rep(i,1,edgenum) if (chosen[i]) {
			if (find(e[i].a)==find(e[i].b)){flag=0; break;} merge(e[i].a,e[i].b);
		//	mtr[a[i]][b[i]]=mtr[b[i]][a[i]]=c[i];
			G.add(e[i].a,e[i].b,e[i].c); G.add(e[i].b,e[i].a,e[i].c);
		}
		if (!flag) return;
		rep(i,1,n) {int now=0; G.getans(i,-1,now,1); ans=(ans<now)?ans:now;}
		cnt++;
		return;
	}
	chosen[depth]=1; dfs(depth+1,cur+1);
	chosen[depth]=0; dfs(depth+1,cur);
}
bool cmp(EE x, EE y) {return x.c<y.c;}
int main() {
//	freopen("treasure.in","r",stdin);
//	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m); memset(dis,inf,sizeof(dis));
	rep(i,1,m) {
		int a,b,c; scanf("%d%d%d",&a,&b,&c); 
		dis[a][b]=min(dis[a][b],c); dis[b][a]=min(dis[b][a],c);
	}
	rep(i,1,n) rep(j,i+1,n) if (dis[i][j]<inf) {e[++edgenum].a=i; e[edgenum].b=j; e[edgenum].c=dis[i][j];}
	sort(e+1,e+edgenum+1,cmp);
//	printf("%d %d\n",edgenum,n-1);
	dfs(1,0); printf("%lld",ans);
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<cstdlib>
#include<ctime>
#include<algorithm>
#define rep(i,a,b) for (int i=a; i<=b; i++)
#define per(i,a,b) for (int i=a; i>=b; i--)
typedef long long ll;
using namespace std;
const int N = 14, inf = 0x3f3f3f3f;
int n,m,dis[N][N];
struct EE {
	int a,b,c;
} e[N*N];
int edgenum;
ll ans=1e13;
bool chosen[N*N];
int cnt;
int f[N];
inline int find(int x) {
	if (f[x]==x) return x;
	f[x]=find(f[x]); return f[x];
}
inline void merge(int x, int y) {
//	printf("merge %d %d\n",x,y);
	int fx=find(x),fy=find(y);
	if (fx!=fy) f[fx]=fy;
}
struct Graph {
	int head[N],edgenum,to[N<<1],nxt[N<<1],len[N<<1];
	inline void init() {edgenum=0; memset(head,0,sizeof(head));}
	inline void add(int u, int v, int d) {
		to[++edgenum]=v; len[edgenum]=d;
		nxt[edgenum]=head[u]; head[u]=edgenum;
	}
	inline void getans(int u, int fa, int &Ans, int depth) {
		if (Ans>=ans) return;
		for (int i=head[u]; i!=0; i=nxt[i]) if (to[i]!=fa) {
			Ans+=depth*len[i]; getans(to[i],u,Ans,depth+1);	
		}
	}
} G;
inline void dfs(int depth, int cur) {
	cnt++;
	if (cur>n-1) return;
	if (edgenum-depth+1+cur<n-1) return;
	if (depth>edgenum) {
		rep(i,1,n) f[i]=i; bool flag=1; G.init();
		rep(i,1,edgenum) if (chosen[i]) {
			if (find(e[i].a)==find(e[i].b)){flag=0; break;} merge(e[i].a,e[i].b);
		//	mtr[a[i]][b[i]]=mtr[b[i]][a[i]]=c[i];
			G.add(e[i].a,e[i].b,e[i].c); G.add(e[i].b,e[i].a,e[i].c);
		}
		if (!flag) return;
		rep(i,1,n) {int now=0; G.getans(i,-1,now,1); ans=(ans<now)?ans:now;}
		//if (cnt%10==0) puts("OvO");
		return;
	}
	if (cnt>=600000) return;
	chosen[depth]=1; dfs(depth+1,cur+1);
	chosen[depth]=0; dfs(depth+1,cur);
}
inline void tanxin() {
	rep(i,1,n) {
		bool flag=1; int val=0;
		rep(j,1,n) if (i!=j) {if (dis[i][j]==inf) flag=0; val+=dis[i][j];}
		if (flag && val<ans) ans=val;
	}
}
int dep[N],val[N];
bool cho[N];
inline void tanxin1(int s, int A, int B) {
	memset(cho,0,sizeof(cho)); memset(dep,0,sizeof(dep));
	cho[s]=1; dep[s]=1; ll res=0; int cnt=1;
	while (cnt<n) {
		rep(i,1,n) if (!cho[i]) {
			val[i]=-1; //init!!!!
			rep(j,1,n) if (cho[j] && dis[i][j]<inf) {
				if (val[i]==-1) {val[i]=j; continue;}
				int a=(ll)dep[j]*dis[i][j]<(ll)dep[val[i]]*dis[i][val[i]];
				int b=dep[j]<dep[val[i]];
				if (a&&b) val[i]=j;
				else if (a||b) {
					int x=rand()%B+1;
					if (x<=A) val[i]=j;
				}
			}
		}
		int p=-1;
		rep(i,1,n) if (!cho[i] && val[i]!=-1) {
			if (p==-1) {p=i; continue;}
			p=(dep[val[p]]*dis[p][val[p]]<dep[val[i]]*dis[i][val[i]]) ? p : i;
		}
		cho[p]=1; res+=dep[val[p]]*dis[p][val[p]]; dep[p]=dep[val[p]]+1;
		cnt++;
	}
	ans=min(ans,res);
}
bool cmp(EE x, EE y) {return x.c<y.c;}
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m); memset(dis,inf,sizeof(dis));
	rep(i,1,m) {
		int a,b,c; scanf("%d%d%d",&a,&b,&c); 
		dis[a][b]=min(dis[a][b],c); dis[b][a]=min(dis[b][a],c);
	}
	rep(i,1,n) rep(j,i+1,n) if (dis[i][j]<inf) {e[++edgenum].a=i; e[edgenum].b=j; e[edgenum].c=dis[i][j];}
	sort(e+1,e+edgenum+1,cmp);
	tanxin();
	rep(i,1,n) {tanxin1(i,100,100); rep(j,1,500) tanxin1(i,50,100);}
//	printf("%lld\n",ans);
	dfs(1,0); printf("%lld",ans); //printf("\n%d",cnt);
	return 0;
}

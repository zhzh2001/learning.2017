#include<bits/stdc++.h>
using namespace std;
int n,m,q,nd[300010],ad[300010];
struct splays
{
	int son[2000100][2],fa[2000100],tt;
	long long dl[2000100],dr[2000100],size[2000100];
	int newnode(long long ql,long long qr)
	{
		tt++;dl[tt]=ql;dr[tt]=qr;size[tt]=qr-ql+1;
		return tt;
	}
	int getlr(int s)
	{
		if (son[fa[s]][0]==s) return 0;
		else if (son[fa[s]][1]==s) return 1;
		return 2;
	}
	void upd(int s)
	{
		size[s]=size[son[s][0]]+size[son[s][1]]+dr[s]-dl[s]+1;
	}
	void rotate(int s)
	{
		int dc=getlr(s),fdc=getlr(fa[s]);
		int pf=fa[s],pff=fa[fa[s]],pw=son[s][!dc];
		fa[s]=pff;if (pff) son[pff][fdc]=s;
		fa[pf]=s;son[s][!dc]=pf;
		son[pf][dc]=pw;if (pw) fa[pw]=pf;
		//size[pf]=size[son[pf][0]]+size[son[pf][1]]+dr[pf]-dl[pf]+1;
		//size[s]=size[son[s][0]]+size[son[s][1]]+dr[s]-dl[s]+1;
		upd(pf);upd(s);
	}
	void splay(int s)
	{
		while (fa[s])
		{
			if (!fa[fa[s]]) rotate(s);
			else if (getlr(s)==getlr(fa[s])) {rotate(fa[s]);rotate(s);}
			else {rotate(s);rotate(s);}
		}
	}
	long long work(int &root,long long num)
	{
		if (size[root]<num) return size[root]-num;
		while (1)
		{
			if (size[son[root][0]]>=num) {root=son[root][0];continue;}
			if (size[son[root][0]]+dr[root]-dl[root]+1<num) {num-=size[son[root][0]]+dr[root]-dl[root]+1;root=son[root][1];continue;}
			break;
		}
		long long ans=dl[root]+(num-size[son[root][0]])-1;
		splay(root);
		int t=newnode(ans+1,dr[root]);
		son[t][1]=son[root][1];if (son[root][1]) fa[son[root][1]]=t;
		dr[root]=ans-1;son[root][1]=t;fa[t]=root;
		upd(t);upd(root);
		return ans;
	}
}splay;
struct spls
{
	int son[2000100][2],fa[2000100],size[2000100],tt;
	long long num[2000100];
	int newnode(long long s)
	{
		tt++;size[tt]=1;num[tt]=s;
		return tt;
	}
	int getlr(int s)
	{
		if (son[fa[s]][0]==s) return 0;
		else if (son[fa[s]][1]==s) return 1;
		return 2;
	}
	void upd(int s)
	{
		size[s]=size[son[s][0]]+size[son[s][1]]+1;
	}
	void rotate(int s)
	{
		int dc=getlr(s),fdc=getlr(fa[s]);
		int pf=fa[s],pff=fa[fa[s]],pw=son[s][!dc];
		fa[s]=pff;if (pff) son[pff][fdc]=s;
		fa[pf]=s;son[s][!dc]=pf;
		son[pf][dc]=pw;if (pw) fa[pw]=pf;
		//size[pf]=size[son[pf][0]]+size[son[pf][1]]+dr[pf]-dl[pf]+1;
		//size[s]=size[son[s][0]]+size[son[s][1]]+dr[s]-dl[s]+1;
		upd(pf);upd(s);
	}
	void splay(int s)
	{
		while (fa[s])
		{
			if (!fa[fa[s]]) rotate(s);
			else if (getlr(s)==getlr(fa[s])) {rotate(fa[s]);rotate(s);}
			else {rotate(s);rotate(s);}
		}
	}
	void push_back(int &root,long long w)
	{
		if (!root) {root=newnode(w);return;}
		while (son[root][1]) root=son[root][1];
		int t=newnode(w);son[root][1]=t;fa[t]=root;
		while (root) {upd(root);root=fa[root];}
		splay(t);
		root=t;
	}
	long long findanderasekth(int &root,int k)
	{
		if (size[root]<k) assert(0);
		while (1)
		{
			if (size[son[root][0]]>=k) root=son[root][0];
			else if (size[son[root][0]]+1<k) {k-=size[son[root][0]]+1;root=son[root][1];}
			else break;
		}
		long long ans=num[root];
		splay(root);
		if (son[root][0])
		{
			int s=son[root][0];
			while (son[s][1]) s=son[s][1];
			splay(s);
			son[s][1]=son[root][1];fa[son[root][1]]=s;upd(s);root=s;
		}
		else if (son[root][1])
		{
			int s=son[root][1];
			while (son[s][0]) s=son[s][0];
			splay(s);
			son[s][0]=son[root][0];fa[son[root][0]]=s;upd(s);root=s;
		}
		else root=0;
		return ans;
	}
}spl;
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=n;i++) nd[i]=splay.newnode((long long)(i-1)*m+1,(long long)i*m-1);
	int rt=0;
	for (int i=1;i<=n;i++) spl.push_back(rt,(long long)i*m);
	while (q--)
	{
		int x,y;scanf("%d%d",&x,&y);
		if (y!=m)
		{
			long long s=splay.work(nd[x],y);
			if (s<0) s=spl.findanderasekth(ad[x],-s);
			printf("%lld\n",s);
			spl.push_back(ad[x],spl.findanderasekth(rt,x));
			spl.push_back(rt,s);
		}
		else
		{
			long long s=spl.findanderasekth(rt,x);
			printf("%lld\n",s);
			spl.push_back(rt,s);
		}
	}
}

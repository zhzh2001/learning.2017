#include<bits/stdc++.h>
using namespace std;
long long dis[15][15],minn[15][1<<13],f[15][1<<13];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;scanf("%d%d",&n,&m);
	if (n==1) {printf("0\n");return 0;}
	memset(dis,127/3,sizeof(dis));
	memset(f,127/3,sizeof(f));
	while (m--)
	{
		int u,v,w;scanf("%d%d%d",&u,&v,&w);u--;v--;
		dis[u][v]=min(dis[u][v],(long long)w);dis[v][u]=min(dis[v][u],(long long)w);
	}
	for (int i=0;i<n;i++)
	for (int s=1;s<(1<<n);s++)
	{
		minn[i][s]=1e15;
		for (int j=0;j<n;j++) if (s&(1<<j)) minn[i][s]=min(minn[i][s],dis[i][j]);
	}
	long long ans=1e18;
	for (int i=0;i<n;i++) f[0][1<<i]=0;
	for (int i=1;i<=n;i++)
	for (int s=0;s<(1<<n);s++)
	{
		for (int t=(s-1)&s;t;t=(t-1)&s)
		{
			long long ths=f[i-1][t];
			for (int j=0;j<n;j++) if ((s-t)&(1<<j)) ths+=minn[j][t]*i;
			f[i][s]=min(f[i][s],ths);
		}
		if (s==(1<<n)-1) ans=min(ans,f[i][s]);
	}
	printf("%lld\n",ans);
}

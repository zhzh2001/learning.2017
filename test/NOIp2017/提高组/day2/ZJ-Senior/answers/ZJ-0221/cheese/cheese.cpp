#include<bits/stdc++.h>
using namespace std;
bool vis[1010];
int x[1010],y[1010],z[1010],n,h,r;
void dfs(int s)
{
	vis[s]=true;
	if (z[s]+r>=h) vis[n+1]=true;
	for (int i=1;i<=n;i++)
		if ((!vis[i])&&((long long)(x[i]-x[s])*(x[i]-x[s])+(long long)(y[i]-y[s])*(y[i]-y[s])+(long long)(z[i]-z[s])*(z[i]-z[s])<=(long long)r*r*4)) dfs(i);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;scanf("%d",&t);
	while (t--)
	{
		scanf("%d%d%d",&n,&h,&r);
		for (int i=1;i<=n;i++) {vis[i]=false;scanf("%d%d%d",&x[i],&y[i],&z[i]);}
		vis[n+1]=false;
		for (int i=1;i<=n;i++) if ((!vis[i])&&(z[i]<=r)) dfs(i);
		if (vis[n+1]) printf("Yes\n");else printf("No\n");
	}
}

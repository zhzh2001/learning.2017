#include<bits/stdc++.h>
using namespace std;

typedef long long ll;
#define rep(i,l,r) for(int i=l;i<=r;++i)
const int N=1000+5;
struct point
{
	int x,y,z;
};
ll sqr(const ll &x)
{
	return x*x;
}
ll sqr_dis(const point &p1,const point &p2)
{
	return sqr(p1.x-p2.x)+sqr(p1.y-p2.y)+sqr(p1.z-p2.z);
}
point p[N];
bool in[N];int q[N],tail;
void push(int i)
{
	in[q[++tail]=i]=1;
}
bool ok()
{
	int n,h,r;
	cin>>n>>h>>r;
	rep(i,1,n)scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
	memset(in,0,sizeof(in));
	tail=0;
	rep(i,1,n)
	if(p[i].z-r<=0) push(i);
	rep(head,1,tail)
	{
		int i=q[head];
		if(p[i].z+r>=h) return 1;
		rep(j,1,n)
		if(in[j]==0&&sqr_dis(p[i],p[j])<=sqr(2*r)) push(j);
	}
	return 0;
}

int main()
{
	freopen("cheese.in","r",stdin);freopen("cheese.out","w",stdout);
	int tt;
	cin>>tt;
	while(tt--)
	if(ok()) puts("Yes");
	else puts("No");
}

#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmin(T &x,const T &y)
{
	if(x>y)x=y;
}
typedef long long ll;
#define rep(i,l,r) for(int i=l;i<=r;++i)
const int N=12,U=1<<N,inf=1e9;
int dp[N][U];
int mn[U][U];

int main()
{
	freopen("treasure.in","r",stdin);freopen("treasure.out","w",stdout);
	int n,m;
	cin>>n>>m;
	int u=(1<<n)-1;
	rep(i,0,u)
	rep(j,1,u)
	 mn[i][j]=inf;
	while(m--)
	{
		int i,j,w;
		scanf("%d%d%d",&i,&j,&w);
		chmin(mn[1<<i-1][1<<j-1],w);
		chmin(mn[1<<j-1][1<<i-1],w);
	}
	rep(u1,1,u)
	{
		rep(j,0,n-1)
		{
			int u2=1<<j;
			mn[u1][u2]=min(mn[u1-(u1&-u1)][u2],mn[u1&-u1][u2]);
		}
	}
	rep(u1,1,u)
	rep(u2,1,u)
	 mn[u1][u2]=min(inf,mn[u1][u2-(u2&-u2)]+mn[u1][u2&-u2]);
	 
	rep(u1,0,u)dp[0][u1]=inf;
	rep(j,0,n-1)
	{
		int u1=1<<j;
		dp[0][u1]=0;
	}
	rep(d,1,n-1)
	{
		rep(u1,1,u)
		{
			int ans=inf;
			int u2=u1;
			while(--u2&=u1)
			if(mn[u1-u2][u2]!=inf)
			 chmin(ans,dp[d-1][u1-u2]+mn[u1-u2][u2]*d);
			dp[d][u1]=ans;
		}
	}
	int ans=inf;
	rep(d,0,n-1) chmin(ans,dp[d][u]);
	cout<<ans;
}

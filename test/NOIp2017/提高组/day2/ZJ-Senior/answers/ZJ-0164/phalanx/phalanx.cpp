#include<bits/stdc++.h>
using namespace std;

template <typename T> void chmin(T &x,const T &y)
{
	if(x>y)x=y;
}
typedef long long ll;
typedef int* iter;
#define pb push_back
#define rep(i,l,r) for(int i=l;i<=r;++i)

const int ch_top=4e7;
char ch[ch_top],*now_r=ch-1,*now_w=ch-1;
#define gc (*++now_r)
#define c (*now_r)
int read()
{
	while(gc<'0');
	int x=c-'0';
	while(gc>='0')x=x*10+c-'0';
	return x;
}
#undef c
void write(ll x)
{
	static char st[20];
	int top=0;
	do{st[++top]=x%10+'0';}while(x/=10);
	do{*++now_w=st[top];}while(--top);
	*++now_w='\n';
}

const int N=3e5+5;
struct point
{
	int x,y;
};
point query[N];ll ans[N];
template <typename T> 
struct vec
{
	T *a;
	int n,n2;
	void pb(T x)
	{
		if(n==0)
		{
			n2=1;
			a=new T [1];
		}
		else
		if(n==n2)
		{
			n2*=2;
			T *_a=new T[n2];
			memcpy(_a,a,n*sizeof(T));
			a=_a;
		}
		a[n++]=x;
	}
	T* end()
	{
		return a+n;
	}
};
vec<int>lkx[N],lkq[N];
ll a[N],mx[N];
int dyq[N],ad[N],fa[N],c[N][2],sz[N],rt,tot;

int n,m,i;
void up(int x)
{
	mx[x]=max(a[x],max(mx[c[x][0]],mx[c[x][1]]));
	sz[x]=sz[c[x][0]]+sz[c[x][1]]+1;
}
bool get(int x)
{
	return x==c[fa[x]][1];
}
void sc(int f,int x,bool d)
{
	fa[x]=f;c[f][d]=x;
}
void add(int x,int d)
{
	if(!x) return ;
	ad[x]+=d;a[x]+=d;mx[x]+=d;
}
void down(int x)
{
	if(ad[x])
	{
		add(c[x][0],ad[x]);
		add(c[x][1],ad[x]);
		ad[x]=0;
	}
}
void rot(int x)
{
	int f=fa[x];bool d=get(x);
	if(f==rt)fa[rt=x]=0;
	else sc(fa[f],x,get(f));
	sc(f,c[x][!d],d);
	sc(x,f,!d);
	up(f);
}
void splay(int x,int to=0)
{
	int f;
	while(f=fa[x],f!=to)
	{
		if(fa[f]==to) {rot(x);break;}
		rot(get(f)==get(x)?f:x);rot(x);
	}
	up(x);
}
void ins(int na)
{
	++tot;
	fa[tot]=0;
	dyq[tot]=i;ad[tot]=0;a[tot]=na;mx[tot]=a[tot];c[tot][0]=c[tot][1]=0;sz[tot]=1;
	if(tot==1)
	{
		rt=tot;
	}
	else
	{
		int i=rt;
		while(1)
		{
			down(i);
			bool d;
			if(a[i]>=a[tot])d=0;
			else d=1;
			if(c[i][d])i=c[i][d];
			else {sc(i,tot,d);break;}
		}
	}
	splay(tot);
}
int dfs(int x)
{
	if(mx[x]<m)return x;
	down(x);
	if(a[x]>=m) 
	{
		lkq[i-1].push_back( dyq[x] );
		dfs(c[x][1]);
		return dfs(c[x][0]);
	}
	sc(x,dfs(c[x][1]),1);
	up(x);
	return x;
}
ll base;
void travel(int x)
{
	if(!x) return ;
	down(x);
	travel(c[x][0]);travel(c[x][1]);
	ans[dyq[x]]=base+a[x];
}

int build(int l,int r)
{
	if(l>r)return 0;
	int rt=(l+r)>>1;
	sz[rt]=r-l+1;
	sc(rt,build(l,rt-1),0);
	sc(rt,build(rt+1,r),1);
	return rt;
}

int find(int x)
{
	int i=rt;
	while(sz[c[i][0]]+1!=x)
	{
		if(sz[c[i][0]]+1<x)
		{
			x-=sz[c[i][0]]+1;
			i=c[i][1];
		}
		else i=c[i][0];
	}
	return i;
}

int main()
{
	freopen("phalanx.in","r",stdin);freopen("phalanx.out","w",stdout);
fread(ch,1,ch_top,stdin);
	int q;
	n=read();m=read();q=read();
	rep(i,1,q)
	{
		query[i].x=read();query[i].y=read();
		if(query[i].y!=m)lkx[query[i].x].pb(i);
		else lkq[i-1].pb(i);
	}
//cerr<<clock()<<endl;
	rep(x,1,n)
	if(lkx[x].n)
	{
		for(iter it=lkx[x].end();(--it)>=lkx[x].a;)
		{
			i=*it;
			ins(query[i].y);
			splay(tot);
			add(c[tot][1],1);
			up(rt);
			rt=dfs(rt);
		}
		base=(ll)(x-1)*m;
		travel(rt);
		tot=rt=0;
	}
//cerr<<clock()<<endl;
if(n==1)
{
	ll now=m;
	rep(i,0,q)
	{
		if(i)
		{
			now=ans[i];
		}
		for(iter it=lkq[i].a;it!=lkq[i].end();++it)
		{
			int j=*it;
			ans[j]=now;
		}
	}
}
else
{
	rep(i,1,n)a[i]=(ll)m*i;
	rt=build(1,n);fa[rt]=0;
	rep(i,0,q)
	{
		if(i)
		{
			int x=find(query[i].x);
			splay(x);
			if(!c[rt][0])rt=c[rt][1];
			else 
			if(!c[rt][1])rt=c[rt][0];
			else
			{
				int p=find(query[i].x+1);
				splay(p,rt);
				sc(p,c[rt][0],0);
				rt=p;
			}
			fa[rt]=0;
			splay(find(n-1));
			sc(rt,x,1);
			a[x]=ans[i];c[x][0]=c[x][1]=0;
			splay(x);
		}
		for(iter it=lkq[i].a;it!=lkq[i].end();++it)
		{
			int j=*it;
			int p=find(query[j].x);
			ans[j]=a[p];
			splay(p);
		}
	}
}
//cerr<<clock()<<endl;
	rep(i,1,q)write(ans[i]);
	fwrite(ch,1,now_w-ch+1,stdout);
//cerr<<clock()<<endl;
}

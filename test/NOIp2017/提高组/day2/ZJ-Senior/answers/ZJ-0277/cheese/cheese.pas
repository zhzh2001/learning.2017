var Q,l,n,h,r,i,j,cnt:longint;
x,y,z:array[0..1005]of int64;
head:array[0..1005]of longint;
vet,next:array[0..1010025]of longint;
vis:array[0..1005]of boolean;
procedure add(u,v:longint);
begin
inc(cnt);
vet[cnt]:=v;
next[cnt]:=head[u];
head[u]:=cnt;
end;
function dis(x1,y1,z1,x2,y2,z2:int64):double;
begin
exit(sqrt(sqr(x1-x2)+sqr(y1-y2)+sqr(z1-z2)));
end;
procedure dfs(u:longint);
var i,v:longint;
begin
vis[u]:=true;
i:=head[u];
while i<>-1 do
  begin
  v:=vet[i];
  if vis[v]=false then dfs(v);
  i:=next[i];
  end;
end;
begin
assign(input,'cheese.in');reset(input);
assign(output,'cheese.out');rewrite(output);
readln(Q);
for l:=1 to Q do
  begin
  readln(n,h,r);
  cnt:=0;
  fillchar(head,sizeof(head),255);
  for i:=1 to n do
     begin
     readln(x[i],y[i],z[i]);
     if z[i]-r<=0 then
       begin add(0,i);add(i,0);end;
     if z[i]+r>=h then
       begin add(i,n+1);add(n+1,i);end;
     end;
  for i:=1 to n-1 do
    for j:=i+1 to n do
      if dis(x[i],y[i],z[i],x[j],y[j],z[j])<=r*2 then
        begin
        add(i,j);
        add(j,i);
        end;
  fillchar(vis,sizeof(vis),false);
  dfs(0);
  if vis[n+1] then writeln('Yes')
    else writeln('No');
  end;
close(input);
close(output);
end.

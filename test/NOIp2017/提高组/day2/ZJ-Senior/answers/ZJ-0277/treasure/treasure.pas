var n,m,i,j,k,l,u,v,w,oo,top,sum,min,ans,saveu,savev:longint;
map:array[0..13,0..13]of longint;
vis,stack:array[0..13]of longint;
begin
assign(input,'treasure.in');reset(input);
assign(output,'treasure.out');rewrite(output);
readln(n,m);
oo:=maxlongint div 2;
for i:=1 to n do
  for j:=1 to n do
    map[i,j]:=oo;
for i:=1 to m do
  begin
  readln(u,v,w);
  if map[u,v]>w then map[u,v]:=w;
  if map[v,u]>w then map[v,u]:=w;
  end;
ans:=maxlongint;
for l:=1 to n do
  begin
  top:=1;
  stack[top]:=l;
  fillchar(vis,sizeof(vis),0);
  vis[l]:=1;
  sum:=0;
  for i:=1 to n-1 do
    begin
    min:=maxlongint;
    for j:=1 to top do
      for k:=1 to n do
        if (vis[stack[j]]<>0)and(vis[k]=0)and(map[stack[j],k]<>oo)and
         (vis[stack[j]]*map[stack[j],k]<min) then
          begin
          min:=vis[stack[j]]*map[stack[j],k];
          saveu:=stack[j];
          savev:=k;
          end;
    inc(top);
    stack[top]:=savev;
    vis[savev]:=vis[saveu]+1;
    sum:=sum+min;
    end;
  if sum<ans then ans:=sum;
  end;
writeln(ans);
close(input);
close(output);
end.
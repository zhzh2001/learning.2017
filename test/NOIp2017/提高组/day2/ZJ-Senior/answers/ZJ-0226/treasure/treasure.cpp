#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
int uset[10000];
int i,j,m,n;
long ans;
long tmp;
int q[10000];
int book[1000];
int tail,head;
int no[10000];
int ma[100][100];
struct node{
	int a,b,d;
}edge[100000];
int find(int x){
	if(uset[x]!=x)uset[x]=find(uset[x]);
	return uset[x];
}
void uni(int x,int y){
	if((x=find(x))==(y=find(y)))return;
	uset[y]=x;
	return;
}
bool cmp(struct node a,struct node b){
	return a.d*(min(no[a.a],no[a.b]))<b.d*(min(no[b.a],no[b.b]));
}
int main(){
	ans=9999999;
	memset(ma,0,sizeof(ma));
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	
	for(i=0;i<m;i++){
		scanf("%d%d%d",&edge[i].a,&edge[i].b,&edge[i].d);
		ma[edge[i].a][edge[i].b]=1;
		ma[edge[i].b][edge[i].a]=1;
	}
	for(int k=1;k<=n;k++){
	for(i=1;i<=n;i++){
		uset[i]=i;
	}
	tail=0;
	memset(book,0,sizeof(book));
	head=0;
	q[tail]=k;
	tail++;
	no[k]=1;
	book[k]=1;
	while(tail>head){
		for(i=1;i<=n;i++){
			if(book[i]==0&&i!=q[head]&&ma[q[head]][i]==1){
			no[i]=no[q[head]]+1;
			q[tail]=i;
			tail++;
			book[i]=1;
			}
		}
		head++;
	}
	sort(edge,edge+m,cmp);
	for(i=0;i<m;i++){
		if(find(edge[i].a)!=find(edge[i].b)){
			uni(edge[i].a,edge[i].b);
			tmp=tmp+edge[i].d*(min(no[edge[i].a],no[edge[i].b]));
		}
	}
	cout<<tmp<<endl;
	ans=min(ans,tmp);
	tmp=0;
	memset(no,9999999,sizeof(no));
}
	cout<<ans<<endl;
	return 0;
}


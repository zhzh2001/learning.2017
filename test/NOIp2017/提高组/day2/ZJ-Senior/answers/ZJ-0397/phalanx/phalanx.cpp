#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
typedef long long ll;
ll read(){
	char ch=getchar(),last=' '; ll ans=0;
	while (ch>'9'||ch<'0') last=ch,ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-'0',ch=getchar();
	if (last=='-') ans=-ans; return ans;
}
const int N500=50003;
int n,m,q,le[N500][503],x[503],y[503];
ll in[N500][503],a[N500];
int check500(int w,int x,int y){
	int sum=0;
	for (int i=1;i<=le[x][0];i++){
		if (le[x][i]<w) sum++;
		if (le[x][i]==w) return 1;
	}
	if (w-sum<y) return 1;
	return 0;
}
void solve500(){
	for (int i=1;i<=q;i++) x[i]=read(),y[i]=read();
	for (int i=1;i<=n;i++) a[i]=1LL*i*m;
	for (int i=1;i<=q;i++){
		int w=y[i],l; ll num;
		if (y[i]==m) {
			printf("%lld\n",a[x[i]]);
			a[n+1]=a[x[i]];
			for (int k=x[i];k<=n;k++)
			a[k]=a[k+1];
			continue;
		}
		while (w<=m-1 && check500(w,x[i],y[i])) w++;
		if (w>m-1) {
			l=y[i]-m+1+le[x[i]][0];
			num=in[x[i]][l];
			in[x[i]][0]--;
			for (int k=l;k<=in[x[i]][0];k++)
				in[x[i]][k]=in[x[i]][k+1];
		} else le[x[i]][++le[x[i]][0]]=w,num=1LL*(x[i]-1)*m+w;
		printf("%lld\n",num);
		in[x[i]][++in[x[i]][0]]=a[x[i]];
		a[n+1]=num;
		for (int k=x[i];k<=n;k++)
		a[k]=a[k+1];
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	if (q<=500) solve500();
	return 0;
}

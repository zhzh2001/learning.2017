#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll read(){
	char ch=getchar(),last=' '; ll ans=0;
	while (ch>'9'||ch<'0') last=ch,ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-'0',ch=getchar();
	if (last=='-') ans=-ans; return ans;
}
int map[13][13],m[13],w[13],dp[1<<12][13];
#include<vector>
vector<int> zy[1<<12],zya[1<<12];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n=read(),M=read(),N=1<<n,a,b,c;
	memset(map,-1,sizeof(map));
	for (int i=1;i<=M;i++){
		a=read(),b=read(),c=read();
		if (map[a][b]==-1) map[a][b]=c;
		else map[a][b]=min(map[a][b],c);
		map[b][a]=map[a][b];
	}
	for (int i=0;i<N;i++){
		int h=0,H;
		for (int j=1;j<=n;j++)
			if ((i&(1<<(j-1)))==0){
				w[++h]=j; m[h]=-1;
				for (int k=1;k<=n;k++)
					if ((i&(1<<(k-1)))!=0 && map[k][j]!=-1){
						if (m[h]==-1) m[h]=map[k][j];
						else m[h]=min(m[h],map[k][j]);
					}
			}
		H=1<<h;
		for (int j=0;j<H;j++){
			int add=0,stt=i;
			for (int k=1;k<=h;k++)
			if ((j&(1<<(k-1)))!=0){
				if (m[k]==-1) { add=-1; break;}
				add+=m[k]; stt|=(1<<(w[k]-1));
			}
			if (add==-1) continue;
			zy[i].push_back(stt); zya[i].push_back(add);
		}
	}
	memset(dp,-1,sizeof(dp));
	for (int i=1;i<=n;i++) dp[1<<(i-1)][0]=0;
	for (int i=0;i<n;i++)
		for (int j=0;j<N;j++){
			if (dp[j][i]==-1) continue;
			int size=zy[j].size();
			for (int k=0;k<size;k++){
				if (dp[zy[j][k]][i+1]==-1) 	dp[zy[j][k]][i+1]=dp[j][i]+zya[j][k]*(i+1);
				else dp[zy[j][k]][i+1]=min(dp[zy[j][k]][i+1],dp[j][i]+zya[j][k]*(i+1));
			}
		}
	printf("%d",dp[N-1][n]);
	return 0;
}

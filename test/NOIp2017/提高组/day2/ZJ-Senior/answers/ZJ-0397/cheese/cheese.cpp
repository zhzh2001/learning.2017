#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
typedef long long ll;
ll read(){
	char ch=getchar(),last=' '; ll ans=0;
	while (ch>'9'||ch<'0') last=ch,ch=getchar();
	while (ch<='9'&&ch>='0') ans=ans*10+ch-'0',ch=getchar();
	if (last=='-') ans=-ans; return ans;
}
const int N=1002,M=N*N;
int v[M],Next[M],head[N],vis[N],e;
ll x[N],y[N],z[N];
void add(int x,int y){
	v[++e]=y; Next[e]=head[x]; head[x]=e;
}
void dfs(int u){
	vis[u]=1;
	for (int i=head[u];i;i=Next[i]){
		if (vis[v[i]]) continue;
		dfs(v[i]);
	}
}
double dis(int i,int j){
	return sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]));
}
void solve(){
	int n=read(); ll h=read(),r=read();
	for (int i=1;i<=n;i++){
		x[i]=read(); y[i]=read(); z[i]=read();
	}
	memset(head,0,sizeof(head)); e=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
				if (i!=j) if (dis(i,j)<=(double)(r+r)) add(i,j);
	for (int i=1;i<=n;i++) {
		if (z[i]<=r) add(0,i);
		if (h-z[i]<=r) add(i,n+1);
	}
	memset(vis,0,sizeof(vis));
	dfs(0);
	if (vis[n+1]) puts("Yes");
	else puts("No");
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while (T--) solve();
	return 0;
}

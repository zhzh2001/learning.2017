#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>

using namespace std;

int T,n,H,R;
struct node{int x,y,z,f;}A[2010];

int read(){
	int x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

int find(int x){return A[x].f==x?x:A[x].f=find(A[x].f);}

double dist(int x,int y){
	return sqrt((double)((A[x].x-A[y].x)*(A[x].x-A[y].x)+(A[x].y-A[y].y)*(A[x].y-A[y].y)+(A[x].z-A[y].z)*(A[x].z-A[y].z)));
}

bool cmp(node a,node b){
	return a.f<b.f || a.f==b.f && a.z<b.z;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while (T--){
		n=read(),H=read(),R=read();
		for (int i=1; i<=n; i++) {
		A[i].x=read(),A[i].y=read(),A[i].z=read();A[i].f=i;
		for (int j=1; j<i; j++)	
			if (dist(i,j)<=2*R)
				{int x1=find(A[i].f),y1=find(A[j].f);if (x1!=y1) A[y1].f=A[x1].f;}
		}
		for (int i=1; i<=n; i++) A[i].f=find(A[i].f);
		sort(A+1,A+n+1,cmp);
		int t=1,Re=0;
		for (int i=1; i<=n; i++)
			if (A[i].f!=A[i-1].f) if (A[i-1].z+R>=H && A[t].z-R<=0) {printf("Yes\n");Re=1;break;} else t=i;
		if (!Re) if (A[n].z+R>=H && A[t].z-R<=0) printf("Yes\n"); else printf("No\n");
		//for (int i=1; i<=n; i++) printf("%d ",A[i].f);
	}
}

#include<queue>
#include<cstdio>
#include<iostream>
#include<algorithm>
#define LL long long
#define Min(a,b) ((a>b?b:a))
using namespace std;

int A[20][20],F[20];
int n,m;
LL Ans=1099511627775;
int Q[1001],Q1[1001];
int read(){
	int x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

void work(int x){
//	if (Q[0]==n) {for (int i=1; i<=n; i++) printf("%d ",Q[i]);printf("\n");}
	if (x>Ans) return;
	if (Q[0]==n) {Ans=Min(x,Ans);return;}
	for (int i=1; i<=n; i++) 
		if (F[i]==0) {
			int t=0;
			for (int j=1; j<=Q[0]; j++)
				if ((A[Q[j]][i]*Q1[j]<A[Q[t]][i]*Q1[t] || t==0) && A[Q[j]][i]!=A[0][0]) t=j;
			if (t==0) continue;
			Q[0]++,Q[Q[0]]=i;Q1[Q[0]]=Q1[t]+1;F[i]=1;
			work(x+A[Q[t]][i]*Q1[t]);F[i]=0;Q[0]--;
		}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	memset(A,63,sizeof(A));
	for (int i=1; i<=m; i++) {
		int x=read(),y=read(),z=read();
		if (A[x][y]>z) A[x][y]=z,A[y][x]=z;
	}
/*	for (int i=1; i<=n; i++){
		for (int j=1; j<=n; j++) printf("%d ",A[i][j]);printf("\n");
	}*/
	for (int i=1; i<=n; i++) {
		Q[0]=1;Q[1]=i;Q1[1]=1;F[i]=1;
		work(0);F[i]=0;
	}
	printf("%I64d",Ans);
}

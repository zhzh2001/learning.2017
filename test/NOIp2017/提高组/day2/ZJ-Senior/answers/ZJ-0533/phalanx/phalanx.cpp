#include<cstdio>
using namespace std;

int A[2010][2010];

int read(){
	int x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n=read(),m=read(),q=read();
	for (int i=1; i<=n; i++)
		for (int j=1; j<=m; j++)
			A[i][j]=(i-1)*m+j;
	for (int i=1; i<=q; i++) {
		int x=read(),y=read();
		printf("%d\n",A[x][y]);
		int t=A[x][y];
		for (int j=x; j<=m; j++) 
			A[x][j]=A[x][j+1];
		for (int j=y; j<=n; j++) 
			A[j][m]=A[j+1][m];
		A[n][m]=t;
	}
}

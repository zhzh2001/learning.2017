#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 13,M = 4096;
const int inf = 0x3f3f3f3f;
int n,m,fall;
int ans;
int v;
int mp[N][N];
int dist[N];
int q[N],head,tail;
int vis[N];

inline void Min(int &x,int y){
	if(x > y) x = y;
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n = read(); m = read(); fall = (1<<n)-1;
	memset(mp,0x3f,sizeof mp);
	for(int i = 1;i <= m;i++){
		int a = read(),b = read(),c = read();
		v = c;
		if(mp[a][b] > c) mp[a][b] = mp[b][a] = c;
	}
	ans = inf;
	for(int i = 1;i <= n;i++){
		int num = 0;
		memset(vis,false,sizeof vis);
		head = tail = 0;
		memset(dist,0x3f,sizeof dist);
		dist[i] = 0;
		vis[i] = true;
		q[tail++] = i;
		while(head < tail){
			int u = q[head++];
			for(int j = 1;j <= n;j++)
				if(mp[u][j] < inf){
					if(!vis[j]){
						vis[j] = true;
						q[tail++] = j;
						dist[j] = dist[u]+1;
						num += dist[j];
					}
				}
		}
		Min(ans,num);
	}
	printf("%lld\n",1ll*ans*v);
	return 0;
}

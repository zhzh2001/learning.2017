/*#include<iostream>
#include<cstdio>
#include<cstring>
#include<set>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 300005;
struct node{
	int w;
	ll sum;
	friend bool operator <(node a,node b){
		return a.w < b.w;
	}
};
set<node> s[N];
set<int> p[N];
int n,m,q;
int nxt[N],end;
ll lie[N];
ll sum[N];

int main(){
	freopen("phalanx2.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read(); m = read(); q = read();
	nxt[0] = 1;
	for(int i = 1;i <= n;i++){
		s[i].insert((node){1,(i-1)*m+1});
		s[i].insert((node){m,0});
		nxt[i] = i+1;
		sum[i] = m*i;
	}
	end = n;
	while(q--){
		int x = read(),y = read();
		ll oo;
		if(y < m){
			set<node>:: iterator op = s[x].upper_bound((node){y,y});
			set<node>:: iterator opp = op;
			op--;
			oo = (y-(op -> w)+(op -> sum));
			if(y+1 != (opp -> w))
				s[x].insert((node){y+1,oo+1});
			if(y == (op -> w))
				s[x].erase(op);
			int p = 0,q;
			for(int i = 1;i < x;i++) p = nxt[p];
			q = nxt[p];
			if(x != n) nxt[p] = nxt[q];
			nxt[end] = q; end = q;
			s[x].insert((node){m-1,sum[q]});
			sum[q] = oo;
		}
		else{
			int p = 0,q;
			for(int i = 1;i < x;i++) p = nxt[p];
			q = nxt[p];
			if(x != n) nxt[p] = nxt[q];
			nxt[end] = q; end = q;
			oo = sum[q];
		}
		printf("%lld\n",oo);
	}
	return 0;
}*/
/*
#include<iostream>
#include<cstdio>
#include<cstring>
#include<set>
#include<vector>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int N = 300005;
struct treenode{
	int lson,rson;
	int sum;
}tr[N*4];
struct node{
	int w,l;
	ll sum;
	friend bool operator <(node a,node b){
		return a.w < b.w;
	}
};

set<node> s;
int n,m,Q,cnt = 1;
int nxt[N],end;
ll lie;
ll sum[N];
int x[N],y[N];
vector<int> d;

void build(int num,int l,int r){
	tr[num].sum = 0;
	if(l == r) return;
	int mid = (l+r)>>1;
	tr[num].lson = ++cnt;
	tr[num].rson = ++cnt;
	build(num<<1,l,mid);
	build(num<<1|1,mid+1,r);
}

int query(int num,int l,int r,int x){
	if(l == r) return tr[num].sum;
	int mid = (l+r)>>1;
	if(x <= mid) return query(tr[num].lson,l,mid,x);
	else return tr[tr[num].lson].sum + query(tr[num].rson,mid+1,r,x);
}

void add(int num,int l,int r,int x){
	tr[num].sum++;
	if(l == r) return;
	int mid = (l+r)>>1;
	if(x <= mid) add(tr[num].lson,l,mid,x);
	else add(tr[num].rson,mid+1,r,x);
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read(); m = read(); Q = read();	
	nxt[0] = 1;
	lie = m;
	s.insert((node){1,m-1,1});
	for(int i = 1;i <= n;i++){
		nxt[i] = i+1;
		sum[i] = m*i;
	}
	end = n;
	for(int i = 1;i <= Q;i++){
		x[i] = read(); y[i] = read();
		d.push_back(y[i]);
	}
	sort(d.begin(),d.end());
	build(1,0,d.size()-1);
	for(int i = 1;i <= Q;i++){
		int X = x[i],Y = y[i];
		ll oo = 0;
		if(Y < m){
			int yy = lower_bound(d.begin(),d.end(),Y)-d.begin();
			int pp = query(1,0,d.size()-1,yy);
			Y += pp;
			set<node>:: iterator op = s.upper_bound((node){Y,Y,Y});
			op--;
			printf("%d %d %lld\n",op->w,op->l,op->sum);
			oo = (Y-(op -> w)+(op -> sum));
			if(Y != (op -> w)+(op -> l)-1)
				s.insert((node){Y+1,op->l-(Y-op->w+1),oo+1});
			if(Y != (op -> w))
				s.insert((node){op->w,Y-op->w,op->sum});
			s.erase(op);
			add(1,0,d.size()-1,yy);
			int p = 0,q;
			for(int i = 1;i < X;i++) p = nxt[p];
			q = nxt[p];
			if(X != n) nxt[p] = nxt[q];
			nxt[end] = q; end = q;
			s.insert((node){lie,1,sum[q]});
			sum[q] = oo;
			lie++;
		}
		else{
			int p = 0,q;
			for(int i = 1;i < X;i++) p = nxt[p];
			q = nxt[p];
			if(X != n) nxt[p] = nxt[q];
			nxt[end] = q; end = q;
			oo = sum[q];
		}
		printf("%lld\n",oo);
	}
	return 0;
}
*/

//baoli

#include<iostream>
#include<cstdio>
#include<cstring>
#include<set>
#include<vector>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}

const int M = 1005;
int n,m,q,tot;
int mp[M][M];

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n = read(); m = read(); q = read();
	for(int i = 1;i <= n;i++)
		for(int j = 1;j <= m;j++)
			mp[i][j] = ++tot;
	while(q--){
		int x = read(),y = read();
		int num = mp[x][y];
		printf("%d\n",num);
		for(int i = y;i < m;i++)
			mp[x][i] = mp[x][i+1];
		for(int i = x;i < n;i++)
			mp[i][m] = mp[i+1][m];
		mp[n][m] = num;
	}
	return 0;
}

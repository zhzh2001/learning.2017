#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
#define ll long long
inline int read(){
	int x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;
}
inline ll readll(){
	ll x = 0,f = 1;char ch = getchar();
	while(ch < '0' || ch > '9'){if(ch == '-')f = -1;ch = getchar();}
	while(ch >= '0' && ch <= '9'){x = x*10+ch-'0';ch = getchar();}
	return x*f;	
}

const int N = 1005;
int n;
int h,r;
struct node{
	int x,y,z;
}a[N];
int f[N];

int getf(int x){
	if(x == f[x]) return x;
	f[x] = getf(f[x]);
	return f[x];
}

inline ll calc(int a){
	return 1ll*a*a;
}

inline double dist(int p1,int p2){
	return sqrt((double)(calc(a[p1].x-a[p2].x)+calc(a[p1].y-a[p2].y)+calc(a[p1].z-a[p2].z)));
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T = read();
	while(T--){
		n = read(); h = readll(); r = readll();
		for(int i = 0;i <= n+1;i++) f[i] = i;
		for(int i = 1;i <= n;i++){
			a[i].x = readll();
			a[i].y = readll();
			a[i].z = readll();
		}
		for(int i = 1;i <= n;i++){
			if(a[i].z <= r){
				int fa = getf(0),fb = getf(i);
				f[fb] = fa;
			}
			if(a[i].z >= h-r){
				int fa = getf(n+1),fb = getf(i);
				if(fa == fb) continue;
				f[fb] = fa;
			}
		}
		if(getf(n+1) == getf(0)){
			puts("Yes");
			continue;
		}
		for(int i = 1;i <= n;i++)
			for(int j = 1;j <= n;j++)
				if(i != j){
					if(dist(i,j) <= (double)(2*r)){
						int fa = getf(i),fb = getf(j);
						if(fa == fb) continue;
						f[fa] = fb;
					}
				}
		if(getf(n+1) == getf(0))
			puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int INF=1000000000;
map<long long,int> mpp;
int mp[20][20];
long long mi[20];
int n,m,y,ans=INF,op[10];
inline int oppt(int x,int y){
	return x-1-(x>y);
}
/*inline void lop(int x){
	printf("0");
	for (int i=2; i<=n; i++) printf("%d",x/mi[i-2]%n);
}*/
void dfs(long long x,int z,int a){
	if (z>=ans) return;
	if (a==n-1){
		ans=z;
		return;
	}
	int ttt=mpp[x];
	if (ttt>0&&z>=ttt) return; mpp[x]=z;
	for (int i=1; i<y; ++i){
		int t=op[i]; if (x/mi[t]%n>0) continue;
		if (mp[y][i]<INF) dfs(x+mi[t],z+mp[y][i],a+1);
		for (int j=1; j<y; ++j)
		if (mp[j][i]<INF){
			int t2=x/mi[op[j]]%n;
			if (t2) dfs(x+mi[t]*(t2+1),z+mp[j][i]*(t2+1),a+1);
		}
		for (int j=y+1; j<=n; ++j)
		if (mp[j][i]<INF){
			int t2=x/mi[op[j]]%n;
			if(t2) dfs(x+mi[t]*(t2+1),z+mp[j][i]*(t2+1),a+1);
		}
	}
	for (int i=y+1; i<=n; ++i){
		int t=op[i]; if (x/mi[t]%n>0) continue;
		if (mp[y][i]<INF) dfs(x+mi[t],z+mp[y][i],a+1);
		for (int j=1; j<y; ++j)
		if (mp[j][i]<INF){
			int t2=x/mi[op[j]]%n;
			if (t2) dfs(x+mi[t]*(t2+1),z+mp[j][i]*(t2+1),a+1);
		}
		for (int j=y+1; j<=n; ++j)
		if (mp[j][i]<INF){
			int t2=x/mi[op[j]]%n;
			if (t2) dfs(x+mi[t]*(t2+1),z+mp[j][i]*(t2+1),a+1);
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=n; ++i)
	for (int j=1; j<=n; ++j)
	mp[i][j]=INF;
	mi[0]=1; for (int i=1; i<=n; i++) mi[i]=mi[i-1]*n;
	while (m--){
		int x,y,z; scanf("%d%d%d",&x,&y,&z);
		mp[x][y]=min(mp[x][y],z);
		mp[y][x]=min(mp[y][x],z);
	}
	for (int i=1; i<=n; ++i){
		mpp.clear();
		y=i;for (int j=1; j<=n; ++j) op[j]=oppt(j,y);
		dfs(0,0,0);
	}
	printf("%d",ans);
	return 0;
}

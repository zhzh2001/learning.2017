#include<bits/stdc++.h>
using namespace std;
typedef unsigned long long ull;
const int N=1010;
struct yuan{
	int x,y,z;
	
}c[N];
ull sqrdis(int x,int y,int z){
	ull res=0;
	res+=1ll*x*x;
	res+=1ll*y*y;
	res+=1ll*z*z;
	return res;	
}
int t,n,h,r,fa[N];
ull op;
int find(int x){
	return fa[x]==x?x:fa[x]=find(fa[x]);
}
void unite(int x,int y){
	x=find(x);
	y=find(y);
	if (x==y) return;
	fa[x]=y;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while (t--){
		scanf("%d%d%d",&n,&h,&r);
		op=1ll*4*r*r;
		for (int i=1; i<=n; ++i) scanf("%d%d%d",&c[i].x,&c[i].y,&c[i].z);
		for (int i=1; i<=n+2; ++i) fa[i]=i;
		for (int i=1; i<=n; ++i){
			if (c[i].z<=r) unite(1,i+1);
			if (c[i].z>=h-r) unite(n+2,i+1);
		}
		for (int i=1; i<=n; ++i)
			for (int j=i+1; j<=n; ++j)
		if (sqrdis(c[i].x-c[j].x,c[i].y-c[j].y,c[i].z-c[j].z)<=op) unite(i+1,j+1);
		if (find(1)==find(n+2)) printf("Yes\n"); else printf("No\n"); 
	}
	return 0;
}

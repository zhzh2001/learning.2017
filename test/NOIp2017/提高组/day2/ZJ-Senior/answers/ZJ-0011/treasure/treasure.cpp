#include <iostream>
#include <algorithm>
#include <queue>
#include <memory.h>
using namespace std;
int n, m, s;
bool vis[13];
typedef long long ll;
typedef pair<int, int> cp;
ll cost[13][13][13], mincost[13][13], ecost[13][13], tot, mintot = 10000000000;				//cost[s][n][step], mincost[s][n]
queue<cp> que;

struct node {
	int n;
	ll v;
	node* next;
	node() {
		n = 0;
		v = 0;
		next = NULL;
	}
} *e[13], *p;


int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	ios::sync_with_stdio(false);
	int i, j, k, x, y, step;
	ll v;
	cp pa;
	cin >> n >> m;
	for (i=1; i<=m; i++) {
		cin >> x >> y >> v;
		for (j=1; j<=2; j++) {
			p = new node();
			p->n = y;
			p->v = v;
			if (e[x] == NULL)
				e[x] = p;
			else {
				p->next = e[x]->next;
				e[x]->next = p;
			}
			swap(x, y);
		}
	}
	for (i=1; i<=n; i++) {
		for (j=1; j<=n; j++) {
			mincost[i][j] = ecost[i][j] = 100000000;
			for (k=1; k<=n; k++)
				cost[i][j][k] = 100000000;
		}
	}
	for (s=1; s<=n; s++) {
		tot = 0;
		mincost[s][s] = ecost[s][s] = 0;
		que.push(cp(s, 1));
		while (!que.empty()) {
			cp pa = que.front();
			que.pop();
			x = pa.first;
			step = pa.second;
			p = e[x];
			while (p != NULL) {
				if (cost[s][x][step-1] + p->v * step < cost[s][p->n][step]) {
					cost[s][p->n][step] = p->v * step + cost[s][x][step-1];
					if (cost[s][p->n][step] < mincost[s][p->n]) {
						mincost[s][p->n] = cost[s][p->n][step];
						ecost[s][p->n] = p->v * step;
					}
					if (step<n)
						que.push(cp(p->n, step+1));
				}
				p = p->next;
			}
		}
		for (i=1; i<=n; i++)
			tot += ecost[s][i];
		if (tot < mintot)
			mintot = tot;
	}
	cout << mintot << endl;
	fclose(stdin);
	fclose(stdout);
	//system("pause");
return 0;
}

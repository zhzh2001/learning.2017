#include <iostream>
#include <cmath>
using namespace std;
int t, q, n, downNum, upNum, down[1002], up[1002];
double h, r;
bool pos1, pos2, ok;

struct sphere {
	double x;
	double y;
	double z;
	int pa;
	sphere(double xx=0, double yy=0, double zz=0, int ppa=0): x(xx), y(yy), z(zz), pa(ppa) {}
} ball[1002];

inline double dist(sphere s1, sphere s2) {
	return sqrt((s1.x-s2.x)*(s1.x-s2.x) + (s1.y-s2.y)*(s1.y-s2.y) + (s1.z-s2.z)*(s1.z-s2.z));
}

int get_parent(int i) {
	if (ball[i].pa == i)
		return i;
	ball[i].pa = get_parent(ball[i].pa);
	return ball[i].pa;
}

int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> t;
	for (q=1; q<=t; q++) {
		double x, y, z;
		int i, j;
		pos1 = pos2 = ok = false;
		downNum = upNum = 0;
		cin >> n >> h >> r;
		for (i=1; i<=n; i++) {
			cin >> x >> y >> z;
			ball[i] = sphere(x, y, z, i);
			if (z <= r+.01) {
				down[++downNum] = i;
				pos1 = true;
			}
			if (z >= h-r-.01) {
				up[++upNum] = i;
				pos2 = true;
			}
		}
		if (!pos1 || !pos2) {
			cout << "No" << endl;
			continue;
		}
		for (i=1; i<=n; i++)
			for (j=1; j<=n; j++)
				if (i != j)
					if (get_parent(i) != get_parent(j))
						if (dist(ball[i], ball[j]) <= r+r+.001)
							ball[get_parent(i)].pa = get_parent(j);
		for (i=1; i<=downNum; i++)
			if (!ok)
				for (j=1; j<=upNum; j++)
					if (get_parent(down[i]) == get_parent(up[j])) {
						ok = true;
						cout << "Yes" << endl;
						break;
					}
		if (!ok)
			cout << "No" << endl;
	}
	fclose(stdin);
	fclose(stdout);
	//system("pause");
return 0;
}

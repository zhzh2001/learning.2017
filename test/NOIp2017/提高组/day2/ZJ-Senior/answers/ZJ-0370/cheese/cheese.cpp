#include<cstdio>
using namespace std;
typedef long long ll;
struct hole{
	int X,Y,Z,L;
}H[1005];
int Q,N,HT,R,l1,l2,CS,CE,S[1005],E[1005];ll R2;
int fl(int n)
{
	if (H[n].L!=H[H[n].L].L) H[n].L=fl(H[n].L);
	return H[n].L;
}
inline ll sqr(int n) {return n*n;}
inline ll dis(hole a,hole b)
{return sqr(a.X-b.X)+sqr(a.Y-b.Y)+sqr(a.Z-b.Z);}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&Q);
	for (int ident=0;ident<Q;++ident)
	{
		scanf("%d%d%d",&N,&HT,&R);
		HT-=R;R2=4*R*R;CS=CE=0;
		for (int i=1;i<=N;++i)
		{
			H[i].L=i;
			scanf("%d%d%d",&H[i].X,&H[i].Y,&H[i].Z);
			if (H[i].Z<=R) S[CS++]=i;
			if (H[i].Z>=HT) E[CE++]=i;
			for (int j=1;j<i;++j)
				if (dis(H[i],H[j])<=R2)
				{
					l1=fl(j);l2=fl(i);
					if (l1!=l2) H[l2].L=H[l1].L;
				}
		}
		for (int i=0;i<CS;++i)
			for (int j=0;j<CE;++j)
			{
				l1=fl(S[i]);l2=fl(E[j]);
				if (H[l1].L==H[l2].L)
				{printf("Yes\n");goto NXT;}
			}
		printf("No\n");NXT:;
	}
	return 0;
}

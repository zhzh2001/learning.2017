#include<cstdio>
using namespace std;
int N,M,D[15],RES=2147483647,tRES,MAP[15][15],t1,t2,t3,QU[999],H,T,C;
bool VIS[15];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&N,&M);
	for (int i=1;i<=M;++i)
	{
		scanf("%d%d%d",&t1,&t2,&t3);
		if (!MAP[t1][t2] || MAP[t1][t2]>t3)
		{
			MAP[t1][t2]=t3;MAP[t2][t1]=t3;
		}
	}
	for (int i=1;i<=N;++i)
	{
		for (int j=1;j<=N;++j)
			VIS[j]=0;
		QU[0]=i;H=0;T=1;VIS[i]=1;D[i]=1;tRES=0;C=1;
		while (H<T && C<N)
		{
			for (int j=1;j<=N;++j)
				if (MAP[QU[H]][j] && !VIS[j])
				{
					VIS[j]=1;C++;
					D[j]=D[QU[H]]+1;
					tRES+=MAP[QU[H]][j]*D[QU[H]];
					QU[T++]=j;
				}
			H++;
		}
		RES=tRES<RES?tRES:RES;
	}
	printf("%d",RES);
	return 0;
}

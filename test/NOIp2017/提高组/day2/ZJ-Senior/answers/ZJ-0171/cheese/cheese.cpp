#include<cstdio>
#include<string.h>
const int N=1e3+10,M=2e3+10;
struct pot{
	int x,y,z;
}p[N];
int T,v[M],nxt[M],pt[N],cnt,n,h,r,t,hd,rv[N],q[N],u;
inline void add(int x,int y){
	v[++cnt]=y,nxt[cnt]=pt[x],pt[x]=cnt;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		cnt=0;
		memset(pt,0,sizeof(pt));
		memset(rv,0,sizeof(rv));
		memset(nxt,0,sizeof(nxt));
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1;i<=n;i++)
			scanf("%d%d%d",&p[i].x,&p[i].y,&p[i].z);
		t=hd=0;
		for(int i=1;i<=n;i++){
			if(p[i].z<=r)q[++t]=i,rv[i]=1;
			if(h-p[i].z<=r)add(i,n+1);
			for(int j=i+1;j<=n;j++){
				int dx=p[i].x-p[j].x,dy=p[i].y-p[j].y,dz=p[i].z-p[j].z;
				if(1ll*dx*dx+1ll*dy*dy+1ll*dz*dz<=4ll*r*r)
					add(i,j),add(j,i);
			}
		}
		while(hd<t){
			u=q[++hd];
			for(int i=pt[u];i;i=nxt[i])
				if(!rv[v[i]]){
					if(v[i]==n+1)goto Suc;
					q[++t]=v[i];
					rv[v[i]]=1;
				}
		}
		puts("No");continue;
		Suc:puts("Yes");continue;
	}
}

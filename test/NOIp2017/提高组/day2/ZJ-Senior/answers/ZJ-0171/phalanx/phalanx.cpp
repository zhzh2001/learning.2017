#include<string.h>
#include<cstdio>
#include<cctype>
#include<algorithm>
#include<queue>
using namespace std;
const int N=1e3+10;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
priority_queue<pair<int,int> >qu;
int n,m,q,a[N][N],aa[300010],bb[300010];
;int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=In(),m=In(),q=In();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=(i-1)*m+j;
	if(n<=1000&&m<=1000){
		if(1ll*q*(n+m)<=2e7){
			while(q--){
				int x=In(),y=In();
				int t=a[x][y];	
				printf("%d\n",t);
				for(int j=y+1;j<=m;j++)
					a[x][j-1]=a[x][j];
				for(int i=x+1;i<=n;i++)
					a[i-1][m]=a[i][m]; 
				a[n][m]=t;
			}
		}
	}else
		if(n==1){
			int lst=n,crt,bc;
			for(int i=0;i<=m;i++)aa[i]=i+1,bb[i]=i-1;
			while(q--){
				int x=In(),y=In();
				if(y!=m){
					crt=0;
					while(y--)
						crt=aa[crt];
					printf("%d\n",crt);
					aa[bb[crt]]=aa[crt],bb[aa[crt]]=bb[crt];
					aa[crt]=aa[lst],bb[crt]=lst,lst=crt;
				}else printf("%d\n",lst);
			}	
		}
}

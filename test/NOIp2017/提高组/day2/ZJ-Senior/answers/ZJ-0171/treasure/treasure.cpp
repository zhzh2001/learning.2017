#include<string.h>
#include<cstdio>
#include<cctype>
const int N=20;
inline int In(){
	int x=0;char c=getchar();
	while(!isdigit(c))c=getchar();
	while(isdigit(c))x=x*10+c-'0',c=getchar();
	return x;
}
inline int min(int x,int y){
	return (x<y)?x:y;
}
int v[N][N],n,m,ans,r[N],tmp,q[N],u,h,t;
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(v,0,sizeof(v));
	n=In(),m=In();
	for(int i=0;i<m;i++){
		int x=In(),y=In();
		if(v[x][y])
			v[x][y]=v[y][x]=min(v[x][y],In());
		else v[x][y]=v[y][x]=In();
	}
	ans=1e9+10;
	for(int i=1;i<=n;i++){
		memset(r,0,sizeof(r));
		q[t=1]=i,r[i]=1,tmp=h=0;
		while(h<t){
			u=q[++h];
			for(int i=1;i<=n;i++)
				if(!r[i]&&v[u][i])
					tmp+=r[u]*v[u][i],r[i]=r[u]+1,q[++t]=i;
		}
		ans=min(ans,tmp);
	}
	printf("%d\n",ans);		
}

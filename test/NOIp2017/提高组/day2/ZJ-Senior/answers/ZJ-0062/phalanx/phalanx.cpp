#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstring>
#include <vector>
#include <queue>

using namespace std;

int n,m,q,a[1005][1005],tot,x,y,temp,tree[2000005],num;
long long f[1000005];
void build_tree(int k,int l,int r){
	if (l==r){
		if (l>n+m-1) tree[k]=0;
		else tree[k]=1;
		return;
	}
	int mid=(l+r)/2;
	build_tree(2*k,l,mid);
	build_tree(2*k+1,mid+1,r);
	tree[k]=tree[2*k]+tree[2*k+1];
}

void add_tree(int k,int l,int r,int o){
	tree[k]++;
	if (l==r) return;
	int mid=(l+r)/2;
	if (mid>=o) add_tree(2*k,l,mid,o);
	else add_tree(2*k+1,mid+1,r,o);
}

int search_tree(int k,int l,int r,int o){
	tree[k]--;
	if (l==r) return l;
	int mid=(l+r)/2;
	if (tree[2*k]+o>=y) search_tree(2*k,l,mid,o);
	else search_tree(2*k+1,mid+1,r,o+tree[2*k]);
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000 && m<=1000){
		tot=0;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				a[i][j]=++tot;
		for (int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			printf("%d\n",a[x][y]);
			temp=a[x][y];
			for (int j=y;j<m;j++)
				a[x][j]=a[x][j+1];
			for (int j=x;j<n;j++)
				a[j][m]=a[j+1][m];
			a[n][m]=temp;
		}
	}
	else{
		for (int i=1;i<=n;i++) f[i]=i;
		for (int i=1;i<=m;i++) f[i+n-1]=i*n;
		num=n+m+q-1;
		build_tree(1,1,num);
		for (int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			int t=search_tree(1,1,num,0);
			printf("%lld\n",f[t]);
			f[i+n+m-1]=f[t];
			add_tree(1,1,n,i+n+m-1);
		}
	}
	return 0;
}

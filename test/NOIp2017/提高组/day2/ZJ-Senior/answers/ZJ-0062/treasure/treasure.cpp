#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <cstring>
#include <vector>
#include <queue>
#define maxp 200000000
#define maxn 15
using namespace std;

int n,ans,t,m,a[maxn][maxn],vis[maxn],deep[maxn];

void dfs1(int k,int s){
	if (k==n){
		if (s<ans) ans=s;
		return;
	}
	if (k<=4){
		for (int i=1;i<=n;i++)
		if (vis[i]==1)
			for (int j=1;j<=n;j++)
			if (vis[j]==0 && a[i][j]!=maxp){
				t=(deep[i]+1)*a[i][j];
				vis[j]=1;
				deep[j]=deep[i]+1;
				dfs1(k+1,s+t);
				vis[j]=0;
			}
	}
	else{
		int min1,min2,min3,i1,i2,i3,j1,j2,j3;
		min1=min2=min3=maxp;
		for (int i=1;i<=n;i++)
		if (vis[i]==1)
			for (int j=1;j<=n;j++)
			if (vis[j]==0 && a[i][j]!=maxp){
				t=(deep[i]+1)*a[i][j];
				if (t<min1){i3=i2;j3=j2;min3=min2;i2=i1;j2=j1;min2=min1;i1=i;j1=j;min1=t;}
				else if (t<min2){i3=i2;j3=j2;min3=min2;i2=i; j2=j; min2=t;}
				else if (t<min3){i3=i;j3=j;min3=t;}
			}
		if (min1!=maxp){
			vis[j1]=1;
			deep[j1]=deep[i1]+1;
			dfs1(k+1,s+min1);
			vis[j1]=0;
		}
		if (min2!=maxp){
			vis[j2]=1;
			deep[j2]=deep[i2]+1;
			dfs1(k+1,s+min2);
			vis[j2]=0;
		}
		if (min3!=maxp){
			vis[j3]=1;
			deep[j3]=deep[i3]+1;
			dfs1(k+1,s+min3);
			vis[j3]=0;
		}
	}
}

void dfs2(int k,int s){
	if (k==n){
		if (s<ans) ans=s;
		return;
	}
	if (k<=2){
		for (int i=1;i<=n;i++)
		if (vis[i]==1)
			for (int j=1;j<=n;j++)
			if (vis[j]==0 && a[i][j]!=maxp){
				t=(deep[i]+1)*a[i][j];
				vis[j]=1;
				deep[j]=deep[i]+1;
				dfs2(k+1,s+t);
				vis[j]=0;
			}
	}
	else{
		int min1,min2,i1,i2,j1,j2;
		min1=min2=maxp;
		for (int i=1;i<=n;i++)
		if (vis[i]==1)
			for (int j=1;j<=n;j++)
			if (vis[j]==0 && a[i][j]!=maxp){
				t=(deep[i]+1)*a[i][j];
				if (t<min1){i2=i1;j2=j1;min2=min1;i1=i;j1=j;min1=t;}
				else if (t<min2){i2=i; j2=j; min2=t;}
			}
		if (min1!=maxp){
			vis[j1]=1;
			deep[j1]=deep[i1]+1;
			dfs2(k+1,s+min1);
			vis[j1]=0;
		}
		if (min2!=maxp){
			vis[j2]=1;
			deep[j2]=deep[i2]+1;
			dfs2(k+1,s+min2);
			vis[j2]=0;
		}
	}
}

void dfs3(int k,int s){
	if (k==n){
		if (s<ans) ans=s;
		return;
	}
	for (int i=1;i<=n;i++)
	if (vis[i]==1)
		for (int j=1;j<=n;j++)
		if (vis[j]==0 && a[i][j]!=maxp){
			t=(deep[i]+1)*a[i][j];
			vis[j]=1;
			deep[j]=deep[i]+1;
			dfs3(k+1,s+t);
			vis[j]=0;
		}
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++)
			a[i][j]=maxp;
	for(int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		if (a[x][y]>z) {
			a[x][y]=z;
			a[y][x]=z;
		}
	}
	ans=maxp;
	if (n<8){
		for (int i=1;i<=n;i++){
			vis[i]=1;
			deep[i]=0;
			dfs3(1,0);
			vis[i]=0;
		}
	}
	if (n==8){
		for (int i=1;i<=n;i++){
			vis[i]=1;
			deep[i]=0;
			dfs1(1,0);
			vis[i]=0;
		}
	}
	if (n>8){
		for (int i=1;i<=n;i++){
			vis[i]=1;
			deep[i]=0;
			dfs2(1,0);
			vis[i]=0;
		}
	}
	printf("%d\n",ans);
	return 0;
}

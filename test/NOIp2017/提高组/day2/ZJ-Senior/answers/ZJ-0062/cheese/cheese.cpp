#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <cmath>
#include <cstring>
#include <vector>
#include <queue>
#define ll long long
#define maxn 1005
using namespace std;

int n,T,h,a[maxn][maxn],q[maxn],vis[maxn],head,tail,k;
ll e,r,x[maxn],y[maxn],z[maxn],t1,t2,t3;
bool flag;

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while (T--){
		scanf("%d%d%lld",&n,&h,&r);
		e=r*r*4;
		for (int i=0;i<=n+1;i++)
			a[i][0]=0;
		for (int i=1;i<=n;i++)
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		for (int i=1;i<n;i++)
			for (int j=i+1;j<=n;j++){
				t1=x[i]-x[j];t2=y[i]-y[j];t3=z[i]-z[j];
				if (t1*t1+t2*t2<=e-t3*t3){
					a[i][++a[i][0]]=j;
					a[j][++a[j][0]]=i;
				}
			}
		for (int i=1;i<=n;i++)
			if (z[i]<=r)
				a[0][++a[0][0]]=i;
		for (int i=1;i<=n;i++)
			if (r>=h-z[i])
				a[i][++a[i][0]]=n+1;
		n=n+1;
		q[1]=0;
		head=tail=1;
		for (int i=1;i<=n;i++) vis[i]=0;
		vis[0]=1;
		flag=false;
		while (head<=tail && !flag){
			k=q[head];
			for (int i=1;i<=a[k][0];i++)
				if (vis[a[k][i]]==0){
					if (a[k][i]==n) flag=true;
					vis[a[k][i]]=1;
					q[++tail]=a[k][i];
				}
			head++;
		}
		if (flag) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

#include <cstdio>
#include <cstring>

int d[1200][1200],g[1200],x[1200],y[1200],z[1200];
bool b[1200];
int h,i,j,m,n,r,t,head,tail;
bool c;

int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	for (h=1;h<=t;h++)
	{
		scanf("%d%d%d",&n,&m,&r);
		for (i=1;i<=n;i++)
			scanf("%d%d%d",&x[i],&y[i],&z[i]);
		for (i=1;i<=n;i++)
			for (j=1;j<=n;j++)
				if (1LL*(x[i]-x[j])*(x[i]-x[j])+1LL*(y[i]-y[j])*(y[i]-y[j])+1LL*(z[i]-z[j])*(z[i]-z[j])<=1LL*(r+r)*(r+r))
					d[i][j]=1;
				else
					d[i][j]=0;
		memset(b,false,sizeof(b));
		tail=0;
		for (i=1;i<=n;i++)
			if (z[i]<=r)
				tail++,g[tail]=i,b[g[tail]]=true;
		for (head=1;head<=tail;head++)
			for (i=1;i<=n;i++)
				if ((d[g[head]][i]) && (! b[i]))
					tail++,g[tail]=i,b[g[tail]]=true;
		c=false;
		for (i=1;i<=n;i++)
			if ((b[i]) && (z[i]+r>=m))
				c=true;
		if (c)
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}

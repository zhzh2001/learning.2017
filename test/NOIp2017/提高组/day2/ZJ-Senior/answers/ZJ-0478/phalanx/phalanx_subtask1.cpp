#include <cstdio>
#include <algorithm>

using namespace std;

int last[60000],other[600][60000];
int v[60000],x[60000],y[60000],z[60000];
int i,j,k,m,n,q,t;

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx_subtask1.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (i=1;i<=q;i++)
		scanf("%d%d",&x[i],&y[i]),z[i]=x[i];
	sort(z+1,z+q+1);
	k=unique(z+1,z+q+1)-z-1;
	for (i=1;i<=q;i++)
		v[i]=lower_bound(z+1,z+k+1,x[i])-z;
	for (i=1;i<=k;i++)
		for (j=1;j<m;j++)
			other[i][j]=(z[i]-1)*m+j;
	for (i=1;i<=n;i++)
		last[i]=(i-1)*m+j;
	for (i=1;i<=q;i++)
	{
		if (y[i]==n)
			t=last[x[i]];
		else
		{
			t=other[v[i]][y[i]];
			for (j=y[i];j<m;j++)
				other[v[i]][j]=other[v[i]][j+1];
			other[v[i]][m-1]=last[x[i]];
		}
		for (j=x[i];j<n;j++)
			last[j]=last[j+1];
		last[n]=t;
		printf("%d\n",t);
	}
	return 0;
}

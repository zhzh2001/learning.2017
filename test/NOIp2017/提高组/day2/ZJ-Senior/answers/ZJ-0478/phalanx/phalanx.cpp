#include <cstdio>
#include <algorithm>

using namespace std;

inline void subtask1()
{
	int last[60000],other[600][60000];
	int v[60000],x[60000],y[60000],z[60000];
	int i,j,k,m,n,q,t;
	
	for (i=1;i<=q;i++)
		scanf("%d%d",&x[i],&y[i]),z[i]=x[i];
	sort(z+1,z+q+1);
	k=unique(z+1,z+q+1)-z-1;
	for (i=1;i<=q;i++)
		v[i]=lower_bound(z+1,z+k+1,x[i])-z;
	for (i=1;i<=k;i++)
		for (j=1;j<m;j++)
			other[i][j]=(z[i]-1)*m+j;
	for (i=1;i<=n;i++)
		last[i]=(i-1)*m+j;
	for (i=1;i<=q;i++)
	{
		if (y[i]==n)
			t=last[x[i]];
		else
		{
			t=other[v[i]][y[i]];
			for (j=y[i];j<m;j++)
				other[v[i]][j]=other[v[i]][j+1];
			other[v[i]][m-1]=last[x[i]];
		}
		for (j=x[i];j<n;j++)
			last[j]=last[j+1];
		last[n]=t;
		printf("%d\n",t);
	}
	return;
}

int tree[4000000];

int g[1000000];
int i,m,n,q,t,x,y;

inline void buildtree(int l,int r,int node)
{
	if (l==r)
		if (l<n+m)
			tree[node]=1;
		else
			tree[node]=0;
	else
	{
		int mid=(l+r)>>1;
		buildtree(l,mid,node<<1);
		buildtree(mid+1,r,node<<1|1);
		tree[node]=tree[node<<1]+tree[node<<1|1];
	}
	return;
}

inline void inserttree(int l,int r,int x,int y,int node)
{
	if (l==r)
		tree[node]=y;
	else
	{
		int mid=(l+r)>>1;
		if (x<=mid)
			inserttree(l,mid,x,y,node<<1);
		else
			inserttree(mid+1,r,x,y,node<<1|1);
		tree[node]=tree[node<<1]+tree[node<<1|1];
	}
	return;
}

inline int asktree(int l,int r,int k,int node)
{
	if (l==r)
		return l;
	int mid=(l+r)>>1;
	if (tree[node<<1]>=k)
		return asktree(l,mid,k,node<<1);
	else
		return asktree(mid+1,r,k-tree[node<<1],node<<1|1);
}

int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (q<=500)
	{
		subtask1();
		return 0;
	}
	for (i=1;i<=m;i++)
		g[i]=i;
	for (i=m;i<m+n;i++)
		g[i]=(i-m+1)*m;
	buildtree(1,n+m+q,1);
	for (i=1;i<=q;i++)
	{
		scanf("%d%d",&x,&y);
		t=asktree(1,n+m+q,y,1);
		printf("%d\n",g[t]);
		g[n+m+i-1]=g[t];
		inserttree(1,n+m+q,t,0,1);
		inserttree(1,n+m+q,n+m+i-1,1,1);
	}
	return 0;
}

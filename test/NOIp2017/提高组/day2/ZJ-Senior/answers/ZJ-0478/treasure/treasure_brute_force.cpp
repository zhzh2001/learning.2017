#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int edge[100],next[100],dist[100],first[100];
int f[100],x[100],y[100],z[100];
int i,j,k,m,n,s,fx,fy,sum_edge;
bool b;

inline void addedge(int x,int y,int z)
{//printf("%d %d %d\n",x,y,z);
	sum_edge++,edge[sum_edge]=y,next[sum_edge]=first[x],dist[sum_edge]=z,first[x]=sum_edge;
	return;
}

inline int getfather(int x)
{
	if (f[x]==x)
		return x;
	else
		return f[x]=getfather(f[x]);
}

inline int dfs(int x,int y,int z)
{//printf("%d %d %d\n",x,y,z);
	int s=0;
	for (int i=first[x];i!=0;i=next[i])
		if (edge[i]!=y)
			s=s+dist[i]*z+dfs(edge[i],x,z+1);
	return s;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure_brute_force.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++)
		scanf("%d%d%d",&x[i],&y[i],&z[i]);
	s=100000000;
	for (i=0;i<(1<<m);i++)
	{
		k=0;
		for (j=1;j<=m;j++)
			if (i&(1<<(j-1)))
				k++;
		if (k!=n-1)
			continue;
		for (j=1;j<=n;j++)
			f[j]=j;
		b=true;
		for (j=1;j<=m;j++)
			if (i&(1<<(j-1)))
			{
				fx=getfather(x[j]);
				fy=getfather(y[j]);
				if (fx==fy)
					b=false;
				else
					f[fx]=fy;
			}
		if (! b)
			continue;
		memset(first,0,sizeof(first));
		sum_edge=0;
		for (j=1;j<=m;j++)
			if (i&(1<<(j-1)))
			{
				addedge(x[j],y[j],z[j]);
				addedge(y[j],x[j],z[j]);
			}
		for (j=1;j<=n;j++)
			s=min(s,dfs(j,0,1));
	}
	printf("%d",s);
	return 0;
}

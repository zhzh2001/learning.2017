#include <cstdio>
#include <cstring>
#include <algorithm>

using namespace std;

int d[15][15],e[15],g[15];
bool b[15];
int i,m,n,x,y,z,l,r,mid;
bool c;

inline bool dfs(int x,int y,int z,int w)
{
	if (w==n)
		if (z<=mid)
			return c=true;
		else
			return false;
	int s=z,t;
	for (int i=1;i<=n;i++)
		if (y&(1<<(i-1)))
		{
			t=1000000000;
			for (int j=1;j<=n;j++)
				if (! b[j])
					t=min(t,d[i][j]);
			s=s+t*e[x];
			if (s>mid)
				return false;
		}
	b[g[x]]=true;
	bool v;
	s=z,t=w,v=true;
	for (int j=1;j<=n;j++)
		if (y&(1<<(j-1)))
		{
			w++,g[w]=j,e[w]=e[x]+1;
			z=z+e[x]*d[g[x]][j];
			if (z>mid)
			{
				v=false;
				break;
			}
		}
	if ((v) && (dfs(x+1,0,z,w)))
		return true;
	for (int j=t+1;j<=w;j++)
		g[j]=0;
	z=s,w=t;
	for (int i=y;i!=0;i=(i-1)&y)
	{
		if ((i==y) && (! g[x+1]))
			continue;
		s=z,t=w,v=true;
		for (int j=1;j<=n;j++)
			if ((y^i)&(1<<(j-1)))
			{
				w++,g[w]=j,e[w]=e[x]+1;
				z=z+e[x]*d[g[x]][j];
				if (z>mid)
				{
					v=false;
					break;
				}
			}
		if ((v) && (dfs(x+1,i,z,w)))
			return true;
		for (int j=t+1;j<=w;j++)
			g[j]=0;
		z=s,w=t;
	}
	b[g[x]]=false;
	return false;
}

int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(d,12,sizeof(d));
	for (i=1;i<=m;i++)
	{
		scanf("%d%d%d",&x,&y,&z);
		d[x][y]=min(d[x][y],z);
		d[y][x]=min(d[y][x],z);
	}
	l=0,r=100000000;
	while (l<r)
	{
		mid=(l+r)/2;
		c=false;
		for (i=1;i<=n;i++)
		{
			memset(b,false,sizeof(b));
			g[1]=i,e[1]=1;
			if (dfs(1,((1<<n)-1)^(1<<(i-1)),0,1))
				break;
		}
		if (c)
			r=mid;
		else
			l=mid+1;
	}
	printf("%d\n",l);
	return 0;
}

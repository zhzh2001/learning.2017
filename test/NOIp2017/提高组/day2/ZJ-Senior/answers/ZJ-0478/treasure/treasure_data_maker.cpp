#include <cstdio>
#include <ctime>
#include <algorithm>

using namespace std;

int main()
{
	freopen("treasure.in","w",stdout);
	srand((int) time(0));
	int n=12,m=24,p=1000;
	printf("%d %d\n",n,m);
	for (int i=1;i<=m;i++)
	{
		int x=rand()%n+1,y=rand()%n+1,z=rand()%p;
		while (x==y) x=rand()%n+1,y=rand()%n+1;
		printf("%d %d %d\n",x,y,z);
	}
	return 0;
}

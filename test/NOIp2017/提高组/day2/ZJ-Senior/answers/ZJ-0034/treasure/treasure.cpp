#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int ans,sum,n,m,v;
int dist[123][123];

int main()
{
  int i,x,y,z,j,k;
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  scanf("%d%d",&n,&m);
  for(i=1;i<=n;i++)
    for(j=1;j<=n;j++) dist[i][j]=1000000000;
  for(i=1;i<=m;i++)
    {
      scanf("%d%d%d",&x,&y,&z),v=z;
	  dist[x][y]=1;
	}
  for(i=1;i<=n;i++)
    for(j=1;j<=n;j++)
      for(k=1;k<=n;k++)
        if(dist[j][i]+dist[k][i]<dist[j][k])
          dist[j][k]=dist[j][i]+dist[k][i];
  ans=1000000000;
  for(i=1;i<=n;i++)
    {
	  sum=0;
	  for(j=1;j<=n;j++)
	    if(i!=j) sum+=dist[i][j]*v;
	  ans=min(ans,sum);
	}
  printf("%d\n",ans);
}

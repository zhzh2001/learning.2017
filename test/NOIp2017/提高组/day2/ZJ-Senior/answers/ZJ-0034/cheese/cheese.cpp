#include<cstdio>
#include<cstring>
#define LL long long
using namespace std;
int x[1050],y[1050],z[1050],b[1050],wz[1050];
int roadto[2200050],road[2200050];
int n,r,h,tot;

bool dfs(int x)
{
  if(x==n+1) {return true;}
  b[x]=1;
  int t=wz[x];
  while(t)
    {
      if(!b[road[t]]) 
	   {
	    if(dfs(road[t])) return true;
	   }
	  t=roadto[t];
	}
  return false;
}

bool cnct(int i,int j)
{
  long long sum=4*r*r;
  sum-=((LL)x[i]-x[j])*((LL)x[i]-x[j]);
  if(sum<0) return false;
  sum-=((LL)y[i]-y[j])*((LL)y[i]-y[j]);
  if(sum<0) return false;
  sum-=((LL)z[i]-z[j])*((LL)z[i]-z[j]);
  if(sum<0) return false;
  return true;
}

int main()
{
  int i,j,T;
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  scanf("%d",&T);
  while(T--)
  {
  scanf("%d%d%d",&n,&h,&r),tot=0;
  memset(wz,0,sizeof(wz));
  memset(b,0,sizeof(b));
  for(i=1;i<=n;i++) scanf("%d%d%d",&x[i],&y[i],&z[i]);
  for(i=1;i<=n;i++)
    {
	  if(z[i]<=r)
	    {
		   roadto[++tot]=wz[i],road[tot]=n+2,wz[i]=tot;
		   roadto[++tot]=wz[n+2],road[tot]=i,wz[n+2]=tot;
		 }
	  if(z[i]+r>=h)
	    {
		   roadto[++tot]=wz[i],road[tot]=n+1,wz[i]=tot;
		   roadto[++tot]=wz[n+1],road[tot]=i,wz[n+1]=tot;
		 }
	}
  for(i=1;i<n;i++)
    for(j=i+1;j<=n;j++) 
       if(cnct(i,j))
         {
		   roadto[++tot]=wz[i],road[tot]=j,wz[i]=tot;
		   roadto[++tot]=wz[j],road[tot]=i,wz[j]=tot;
		 }
  if(dfs(n+2)) printf("Yes\n");
  else printf("No\n");
  }
}

#pragma GCC optimize("O2")
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <ext/rope>
#include <queue>
using namespace std;
using namespace __gnu_cxx;
const int N=1000;
int a[N][N];
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	int n,m,q;
	scanf("%d%d%d",&n,&m,&q);
	if (n>1000||m>1000) {
		rope<int> r;
		queue<int> qu;
		for (int i=0;i<m;i++) r.push_back(i);
		for (int i=2;i<=n;i++) qu.push(i*m-1);
		while (q--) {
			int x,y;
			scanf("%d%d",&x,&y);
			y--;
			int t=r[y];
			printf("%d\n",t+1);
			r.erase(y,1);
			r.push_back(qu.front());
			qu.pop();
			qu.push(t);
		}
		return 0;
	}
	for (int i=0;i<n;i++) {
		for (int j=0;j<m;j++) {
			a[i][j]=i*m+j;
		}
	}
	while (q--) {
		int x,y;
		scanf("%d%d",&x,&y);
		x--,y--;
		int t=a[x][y];
		printf("%d\n",t+1);
		for (int i=y;i<m-1;i++) {
			a[x][i]=a[x][i+1];
		}
		for (int i=x;i<n-1;i++) {
			a[i][m-1]=a[i+1][m-1];
		}
		a[n-1][m-1]=t;
	}
}

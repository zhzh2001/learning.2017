#pragma GCC optimize("O2")
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;
const int N=1000,M=N*N*2;
bool vis[N];
int x[N],y[N],z[N],g[N];
struct edge {
	int v,nx;
} e[M];
long long sqr(int x) {
	return 1ll*x*x;
}
void dfs(int u,int n) {
	vis[u]=true;
	for (int i=g[u];i!=-1;i=e[i].nx) {
		int v=e[i].v;
		if (!vis[v]) {
			dfs(v,n);
		}
	}
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	while (t--) {
		int n,h,r;
		scanf("%d%d%d",&n,&h,&r);
		memset(g,0xff,sizeof g);
		for (int i=0,cnt=0;i<n;i++) {
			scanf("%d%d%d",x+i,y+i,z+i);
			for (int j=0;j<i;j++) {
				if (sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j])<=sqr(r*2)) {
					e[cnt]=(edge){j,g[i]};
					g[i]=cnt++;
					e[cnt]=(edge){i,g[j]};
					g[j]=cnt++;
				}
			}
		}
		memset(vis,0x00,sizeof vis);
		for (int i=0;i<n;i++) {
			if (z[i]<=r&&!vis[i]) {
				dfs(i,n);
			}
		}
		bool flag=false;
		for (int i=0;i<n;i++) {
			if (z[i]+r>=h&&vis[i]) {
				flag=true;
				break;
			}
		}
		puts(flag?"Yes":"No");
	}
}

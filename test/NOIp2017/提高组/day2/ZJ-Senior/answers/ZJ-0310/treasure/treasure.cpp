#pragma GCC optimize("O2")
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <climits>
using namespace std;
const int N=12;
int d[N][N],p[N],dep[N],f[N],vis[N];
int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	memset(d,0x7f,sizeof d);
	for (int i=0;i<m;i++) {
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		u--,v--;
		d[u][v]=min(d[u][v],w);
		d[v][u]=min(d[v][u],w);
	}
	for (int i=0;i<n;i++) p[i]=i;
	int ans=INT_MAX;
	do {
		memset(f,0x7f,sizeof f);
		memset(vis,0x00,sizeof vis);
		dep[p[0]]=1;
		f[p[0]]=0;
		int sum=0;
		for (int i=0;i<n-1;i++) {
			vis[p[i]]=true;
			sum+=f[p[i]];
			for (int j=0;j<n;j++) {
				int t=dep[p[i]]*d[p[i]][j];
				if (!vis[j]&&d[p[i]][j]<0x7f7f7f7f&&t<f[j]) {
					f[j]=t;
					dep[j]=dep[p[i]]+1;
				}
			}
		}
		ans=min(ans,sum+f[p[n-1]]);
	} while (next_permutation(p,p+n));
	printf("%d",ans);
}

#include<iostream>
#include<cmath>
#include<fstream>
#include<vector>
#include<queue>
#include<cstring>
using namespace std;
int t;
int n,h,r;

struct point{
	long x,y,z;
	inline int judge_top(){
		return (z+r)>=h;
	}
	inline int judge_d(){
		return (z-r)<=0;
	}
};
inline long dis(point a,point b){
	return (long((a.x-b.x)*(a.x-b.x))+long((a.y-b.y)*(a.y-b.y))+long((a.z-b.z)*(a.z-b.z)));
}
point p[1001];
int vis[1001];
int map[1001][1001];
int tot;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	cin>>t;
	int x,y,z;
	for(int k=1;k<=t;k++){
		tot=0;
		cin>>n>>h>>r;
		if(n==1){
			cin>>p[1].x>>p[1].y>>p[1].z;
			if(p[1].judge_top()&&p[1].judge_d())cout<<"Yes"<<endl;
			else cout<<"No"<<endl;
		}
		else{
			int is=0;
			for(int i=1;i<=n;i++)cin>>p[i].x>>p[i].y>>p[i].z;
			if(2*r>=h){
				for(int j=1;j<=n;j++){
					if(p[j].z>=(h-r) && p[j].z<=r){
						is=1;
						break;
					}
				}
			}
			else if(!is){
				memset(vis,0,sizeof(vis));
				memset(map,0,sizeof(map));
				for(int i=1;i<n;i++){
					for(int j=i+1;j<=n;j++){
						if(dis(p[i],p[j])<=(4*r*r)){
							map[i][j]=1;
							map[j][i]=1;
							
						}
					}
				}
				//flag
				for(int c=1;c<=n;c++){
					for(int a=1;a<=n;a++){
						for(int b=1;b<=n;b++){
							if((map[a][c] && map[c][b])||map[a][b])map[a][b]=1;
						}
					}
				}
				for(int i=1;i<=n;i++){
					if(p[i].judge_d()){
						for(int j=1;j<=n;j++){
							if(p[j].judge_top() && map[i][j]){
								is=1;
								break;
							}
						}
					}
					if(is==1)break;
				}
			}
			if(is==1)cout<<"Yes"<<endl;
			else cout<<"No"<<endl;
		}
	}
	return 0;
}

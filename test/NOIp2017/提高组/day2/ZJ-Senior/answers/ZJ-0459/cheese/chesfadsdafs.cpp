#include <cstdio>
#include <cctype>
#include <cmath>
#include <queue>
#include <cstring>
using namespace std;
typedef long long xyh;
const double eps = 1e-6;
int T, n, h, r, x, y, z;
struct Po{
	xyh x, y, z;
	Po(int x = 0, int y = 0, int z = 0) : x(x), y(y), z(z)
	{
	}
}a[1001];
struct Lb{
	int h[1001], v[2000001], nxt[2000001], cnt;
	void reset(int n)
	{
		memset(h + 1, 0, sizeof(int) * n);
		cnt = 0;
	}
	void add_edge(int u1, int v1)
	{
		//printf("debug:add_edge(%d, %d)\n", u1, v1);
		v[++cnt] = v1;
		nxt[cnt] = h[u1];
		h[u1] = cnt;
	}
}lb;
inline xyh sqr(xyh a)
{
	return a * a;
}
inline bool cg(Po a, Po b) //can go
{
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z)) - eps <= r * 2;
}
int f[1001];
bool b[1001], flag;
int find(int x)
{
	if (x == f[x]) return x;
	int u = f[x];
	f[x] = find(f[x]);
	b[x] = b[f[x]] = b[u] = b[x] | b[f[x]] | b[u];
	return f[x];
}
void merge(int x, int y)
{
	int x1 = find(x), y1 = find(y);
	f[x1] = y1;
	b[x] = b[y] = b[x1] = b[y1] = b[x] | b[y] | b[x1] | b[y1];
}
int u;
int main()
{
	#ifndef DEBUG
	freopen("cheese.in", "r+", stdin);
	freopen("cheese.out", "w+", stdout);
	#endif
	scanf("%d", &T);
	while (T--)
	{
		flag = 0;
		scanf("%d%d%d", &n, &h, &r);
		memset(b, 0, sizeof(int) * n);
		for (register int i = 1; i <= n; ++i) f[i] = i;
		for (register int i = 1; i <= n; ++i)
		{
			scanf("%d%d%d", &x, &y, &z);
			if (flag) continue;
			a[i] = Po(x, y, z);
			if (a[i].z <= r)
				b[i] = 1;
			for (register int j = 1; j < i; ++j)
				if (cg(a[i], a[j]))
					merge(i, j);
			if (h - a[i].z <= r)
				if (b[find(i)]) flag = 1;
		}
		if (flag) puts("Yes");
		else puts("No");
	}
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}

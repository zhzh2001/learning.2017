#include <cstdio>
#include <cctype>
#include <cmath>
#include <queue>
#include <cstring>
using namespace std;
typedef long long xyh;
const int ri_top = 1e7;
char ri[ri_top], *rich = ri;
void read_int(int &x)
{
	x = 0;
	int f = 1;
	while (!isdigit(*rich) && *rich != '-') ++rich;
	for (*rich != '-' ? x = *rich - '0' : f = -1; isdigit(*++rich); x = x * 10 + *rich - '0');
	x *= f;
}
const double eps = 1e-6;
int T, n, h, r, x, y, z;
struct Po{
	xyh x, y, z;
	Po(int x = 0, int y = 0, int z = 0) : x(x), y(y), z(z)
	{
	}
}a[1001];
struct Lb{
	int h[1001], v[2000001], nxt[2000001], cnt;
	void reset(int n)
	{
		memset(h + 1, 0, sizeof(int) * n);
		cnt = 0;
	}
	void add_edge(int u1, int v1)
	{
		//printf("debug:add_edge(%d, %d)\n", u1, v1);
		v[++cnt] = v1;
		nxt[cnt] = h[u1];
		h[u1] = cnt;
	}
}lb;
inline xyh sqr(xyh a)
{
	return a * a;
}
inline bool cg(Po a, Po b) //can go
{
	//printf("debug:(%lld, %lld, %lld)--(%lld, %lld, %lld) = %lf\n", a.x, a.y, a.z, b.x, b.y, b.z, sqrt(sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z)) - eps);
	return sqrt(sqr(a.x - b.x) + sqr(a.y - b.y) + sqr(a.z - b.z)) - eps <= r * 2;
}
queue < int > q;
int u;
bool b[1001], flag;
int main()
{
	#ifndef DEBUG
	freopen("cheese.in", "r+", stdin);
	freopen("cheese.out", "w+", stdout);
	#endif
	fread(ri, 1, ri_top, stdin);
	read_int(T);
	while (T--)
	{
		flag = 0;
		read_int(n);
		read_int(h);
		read_int(r);
		while (!q.empty()) q.pop();
		lb.reset(n);
		memset(b, 0, sizeof(int) * n);
		for (register int i = 1; i <= n; ++i)
		{
			read_int(x);
			read_int(y);
			read_int(z);
			if (flag) continue;
			a[i] = Po(x, y, z);
			if (a[i].z <= r)
			{
				if (h - a[i].z <= r)
				{
					flag = 1;
					continue;
				}
				//printf("debug:q.push(%lld, %lld, %lld)\n", a[i].x, a[i].y, a[i].z);
				q.push(i);
				b[i] = 1;
			}
			for (register int j = 1; j < i; ++j)
				if (cg(a[i], a[j]))
				{
					lb.add_edge(i, j);
					lb.add_edge(j, i);
				}
		}
		while (!q.empty() && !flag)
		{
			u = q.front();
			q.pop();
			for (register int i = lb.h[u]; i; i = lb.nxt[i])
				if (!b[lb.v[i]])
				{
					//printf("debug:lb.v[%d] = %d\n", i, lb.v[i]);
					if (h - a[lb.v[i]].z <= r)
					{
						flag = 1;
						break;
					}
					//printf("debug:q.push(%lld, %lld, %lld)\n", a[lb.v[i]].x, a[lb.v[i]].y, a[lb.v[i]].z);
					q.push(lb.v[i]);
					b[lb.v[i]] = 1;
				}
		}
		if (flag) puts("Yes");
		else puts("No");
	}
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}

#include <cstdio>
#define hyy233 tingshuohyynenggouAKnuequanchang
using namespace std;
int a[1001][1001], n, m, q, x, y, t;
int main()
{
	#ifndef DEBUG
	freopen("phalanx.in", "r+", stdin);
	freopen("phalanx.out", "w+", stdout);
	#endif
	scanf("%d%d%d%d%d", &n, &m, &q, &x, &y);
	if (x == 1)
	{
		for (register int i = 1; i <= m; ++i)
			a[1][i] = i;
		for (register int i = 1; i <= n; ++i)
			a[i][m] = m * i;
	}
	else
	{
		for (register int i = 1; i <= n; ++i)
			for (register int j = 1; j <= m; ++j)
				a[i][j] = (i - 1) * m + j;
	}
	for (register int i = 0; i < q; ++i)
	{
		if (i) scanf("%d%d", &x, &y);
		t = a[x][y];
		for (register int j = y; j < m; ++j)
			a[x][j] = a[x][j + 1];
		for (register int j = x; j < n; ++j)
			a[j][m] = a[j + 1][m];
		a[n][m] = t;
		printf("%d\n", t);
	}
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}

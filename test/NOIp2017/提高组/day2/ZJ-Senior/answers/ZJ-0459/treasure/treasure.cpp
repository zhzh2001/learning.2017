#include <cstdio>
#include <ext/pb_ds/priority_queue.hpp>
using namespace std;
int a[21][21];
int n, m, u, v, w, d[21], cnt, sum, num, ans = 0x7fffffff, f[21], s, h[21], fr[2001], to[2001], val[2001], nxt[2001];
struct cmp{
	bool operator()(int a, int b)
	{
		return val[a] * d[fr[a]] > val[b] * d[fr[b]];
	}
};
int ff(int x)
{
	return x != f[x] ? f[x] = ff(f[x]) : x;
}
int main()
{
	#ifndef DEBUG
	freopen("treasure.in", "r+", stdin);
	freopen("treasure.out", "w+", stdout);
	#endif
	scanf("%d%d", &n, &m);
	for (register int i = 0; i < m; ++i)
	{
		scanf("%d%d%d", &u, &v, &w);
		if (u != v && (!a[u][v] || w < a[u][v]))
		{
			//if (!d[u][v]) printf("%d %d\n", u, v);
			//d[u][v] = d[v][u] = 2;
			a[u][v] = a[v][u] = w;
			to[++cnt] = v;
			fr[cnt] = u;
			val[cnt] = w;
			nxt[cnt] = h[u];
			h[u] = cnt;
			to[++cnt] = u;
			fr[cnt] = v;
			val[cnt] = w;
			nxt[cnt] = h[v];
			h[v] = cnt;
		}
	}
	for (s = 1; s <= n; ++s)
	{
		__gnu_pbds::priority_queue < int, cmp > q;
		//printf("s = %d\n", s);
		sum = num = 0;
		for (register int i = 1; i <= n; ++i)
			f[i] = i, d[i] = -1;
		d[s] = 1;
		for (register int i = h[s]; i; i = nxt[i])
			q.push(i);
		while (!q.empty())
		{
			while (!q.empty())
			{
				u = q.top();
				q.pop();
				if (ff(fr[u]) != ff(to[u])) break;
			}
			if (ff(fr[u]) == ff(to[u])) break;
			//printf("%d -> %d\n", fr[u], to[u]);
			if (d[to[u]] == -1 || (d[to[u]] != -1 && d[fr[u]] + 1 < d[to[u]])) d[to[u]] = d[fr[u]] + 1;
			if (d[fr[u]] == -1 || (d[to[u]] != -1 && d[to[u]] + 1 < d[fr[u]])) d[fr[u]] = d[to[u]] + 1;
			f[ff(fr[u])] = ff(to[u]);
			sum += val[u] * d[fr[u]];
			//printf("sum += %d * (d[%d][%d] = %d) = %d\n", val[u], s, fr[u], d[fr[u]], sum);
			if ((++num) == n - 1) break;
			for (register int i = h[to[u]]; i; i = nxt[i])
				if (ff(to[i]) != ff(s))
				//{
					//printf("q.push(%d -> %d %d %d)\n", fr[i], to[i], val[i], d[s][fr[i]]);
					q.push(i);
				//}
		}
		//printf("sum = %d\n", sum);
		if (sum < ans) ans = sum;
	}
	printf("%d\n", ans);
	#ifndef DEBUG
	fclose(stdin);
	fclose(stdout);
	#endif
}

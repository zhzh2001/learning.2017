#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cctype>
#include<queue>
using namespace std;
typedef long long LL;

const int N=1000+10,M=2*N*N;
const double eps=1e-8;
int t,n,to_top[N],to_button[N],vis[N];
LL h,r;
int to[M],ne[M],cnt=0,lnk[N];
struct node{
	LL x,y,z;
}a[N];

double sqr(LL x){
	return (double)x*x;
}

double dis(node x,node y){
	return sqrt(sqr(x.x-y.x)+sqr(x.y-y.y)+sqr(x.z-y.z));
}

void add(int x,int y){
	to[++cnt]=y;ne[cnt]=lnk[x];lnk[x]=cnt;
}

void dfs(int x){
	vis[x]=1;
	for(int i=lnk[x];i;i=ne[i])if(!vis[to[i]])dfs(to[i]);
}

bool solve(){
	scanf("%d%lld%lld",&n,&h,&r);
	for(int i=1;i<=n;i++){
		scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
		to_top[i]=(a[i].z<=r);to_button[i]=(a[i].z>=h-r);
	}
	memset(lnk,0,sizeof(lnk));cnt=0;
	for(int i=1;i<=n;i++)
	for(int j=i+1;j<=n;j++)
	if(dis(a[i],a[j])-2*r<=eps)add(i,j),add(j,i);
	for(int i=1;i<=n;i++)if(to_button[i]){
		memset(vis,0,sizeof(vis));
		dfs(i);
		for(int j=1;j<=n;j++)if(to_top[j] && vis[j])return 1;
	}
	return 0;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--)puts(solve()?"Yes":"No");
	fclose(stdin);fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cctype>
#include<queue>
using namespace std;
typedef long long LL;

const int N=3e5+10;
int n,m,q,x,y;
LL a[1010][1010];

void brute_force(){
	for(int i=1;i<=n;i++)
	for(int j=1;j<=m;j++)a[i][j]=1LL*(i-1)*m+j;
	while(q--){
		scanf("%d%d",&x,&y);LL tmp=a[x][y];
		printf("%lld\n",tmp);
		for(int i=y;i<m;i++)a[x][i]=a[x][i+1];
		for(int i=x;i<n;i++)a[i][m]=a[i+1][m];
		a[n][m]=tmp;
	}
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=1000 && m<=1000)brute_force();
	fclose(stdin);fclose(stdout);
	return 0;
}

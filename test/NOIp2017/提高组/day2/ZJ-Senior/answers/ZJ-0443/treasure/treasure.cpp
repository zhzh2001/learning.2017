#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#include<cctype>
#include<queue>
using namespace std;

const int N=8+2,M=(1<<8)+5;
int n,m,a[N][N],f[N][M][M],dis[N],p[N],cnt,ans;
struct node{
	int dep,leaf,unvis;
};
queue<node> q;
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x3f,sizeof(a));
	for(int i=1;i<=m;i++){
		int x,y,v;scanf("%d%d%d",&x,&y,&v);x--,y--;
		a[x][y]=a[y][x]=min(a[x][y],v);
	}
	memset(f,0x3f,sizeof(f));
	for(int i=0;i<n;i++){
		node now;now.dep=1,now.leaf=(1<<i),now.unvis=(1<<n)-1-(1<<i);
		
		q.push(now);
		f[now.dep][now.leaf][now.unvis]=0;
	}
	while(!q.empty()){
		node now=q.front();q.pop();
		int dep=now.dep,leaf=now.leaf,unvis=now.unvis;
		memset(dis,0x3f,sizeof(dis));
		for(int i=0;i<n;i++)if((1<<i)&leaf){
			for(int j=0;j<n;j++)if(i!=j)dis[j]=min(dis[j],a[i][j]);
		}
		if(unvis==0)continue;
		cnt=0;
		for(int i=0;i<n;i++)if(((1<<i)&unvis) && (dis[i]<0x3f3f3f3f))p[cnt]=i,cnt++;
		for(int i=1;i<(1<<cnt);i++){
			node ne;ne.dep=dep+1,ne.leaf=0,ne.unvis=unvis;
			int sumdis=0;
			for(int j=0;j<cnt;j++)if((1<<j)&i)ne.leaf+=(1<<p[i]),sumdis+=dis[p[i]];
			ne.unvis|=ne.leaf,ne.vis=-(~ne.vis)+1;
			f[ne.dep][ne.leaf][ne.unvis]=sumdis*dep+f[dep][leaf][unvis];
			q.push(ne);	
		}
	}
	int ans=0x3f3f3f3f;
	for(int i=1;i<=n;i++)
	for(int j=1;j<(1<<n);j++)
	ans=min(ans,f[i][j][0]);
	printf("%d\n",ans);
	fclose(stdin);fclose(stdout);
	return 0;
}

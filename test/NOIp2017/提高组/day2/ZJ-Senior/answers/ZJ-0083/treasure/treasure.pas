var
  a:array [0..20,0..20] of longint;
  b,c,d:array [0..20] of longint;
  n,m,i,j,k,l,r,h,min,s,smin:longint;
begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input);
  rewrite(output);
  read(n,m);
  for i:=1 to n do
    for j:=1 to n do a[i,j]:=maxlongint div 20;
  for i:=1 to m do
  begin
    read(l,r,k);
    if a[l,r]>k then
    begin
      a[l,r]:=k;
      a[r,l]:=k;
    end;
  end;
  smin:=maxlongint div 20;
  for h:=1 to n do
  begin
    s:=0;
    for i:=1 to n do
    begin
      b[i]:=0;
      c[i]:=0;
      d[i]:=0;
    end;
    c[h]:=1;
    d[h]:=1;
    for i:=1 to n do
    begin
      b[i]:=a[h,i];
      d[i]:=2;
    end;
    for i:=2 to n do
    begin
      min:=maxlongint div 20;
      k:=0;
      for j:=1 to n do
        if (c[j]=0)and(b[j]<min) then
        begin
          k:=j;
          min:=b[j];
        end;
      c[k]:=1;
      s:=s+min;
      for j:=1 to n do
        if (c[j]=0)and(a[k,j]*d[k]<b[j]) then
        begin
          b[j]:=a[k,j]*d[k];
          d[j]:=d[k]+1;
        end;
    end;
    if s<smin then smin:=s;
  end;
  write(smin);
  close(input);
  close(output);
end.

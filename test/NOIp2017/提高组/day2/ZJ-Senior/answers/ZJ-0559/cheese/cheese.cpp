#include<bits/stdc++.h>
using namespace std;
int t;
int n,h;
long long r;
int x,y,z;
int maxz=-1,minz=100000;
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t!=0)
	{
		scanf("%d%d%lld",&n,&h,&r);
		for(int i=1;i<=n;i++)
		{
			scanf("%d%d%d",&x,&y,&z);
			maxz=max(maxz,z);
			minz=min(minz,z);
		}
		if(maxz+r<h||minz-r>0) {printf("No\n");t--;}
		else {printf("Yes\n");t--;}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
using namespace std;
struct node {ll x,y,z;}a[1010];
int n,h,fg,k,r;
int st[1010],en[1010],ss,ee;
int m[1010][1010],vis[1010],dis[1010];
double call(int g,int h){
	ll xx,yy,zz;
	xx=(a[g].x-a[h].x)*(a[g].x-a[h].x);
	yy=(a[g].y-a[h].y)*(a[g].y-a[h].y);
	zz=(a[g].z-a[h].z)*(a[g].z-a[h].z);
	//printf("%lld %lld %lld\n",xx,yy,zz);
	return sqrt(xx+yy+zz);
}
void spfa(int s){
	queue<int> q;
	dis[s]=1,vis[s]=1;q.push(s);
	while(!q.empty()){
		int x=q.front();q.pop();//vis[x]=0;
		rep(i,1,n)
		if(m[x][i]){
			dis[i]=1;
			if(!vis[i]) 
			q.push(i),vis[i]=1;
		}
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;scanf("%d",&T);rep(TT,1,T){
		scanf("%d %d %d",&n,&h,&r);fg=0;
		rep(i,1,n) scanf("%lld %lld %lld",&a[i].x,&a[i].y,&a[i].z);
		memset(m,0,sizeof(m));
		memset(st,0,sizeof(st));
		memset(en,0,sizeof(en));
		ee=ss=0;
		rep(i,1,n-1) rep(j,i+1,n){
			if(call(i,j)<=2*r){
				m[i][j]=m[j][i]=1;	
			}
		}
		rep(i,1,n) if(a[i].z<=r) st[++ss]=i;
		else if(a[i].z+r>=h) en[++ee]=i;
		//rep(i,1,ss) printf("%d ",a[st[i]].z);printf("\n");
		//rep(i,1,ee) printf("%d ",a[en[i]].z);
		rep(i,1,ss){
			memset(vis,0,sizeof(vis));
			memset(dis,0,sizeof(dis));
			spfa(st[i]);
			rep(j,1,ee)if(dis[en[j]]) {fg=1;break;}
			if(fg) break;
		}
		if(fg) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}/*
3
2 4 1
0 0 1
0 0 3
2 5 1
0 0 1
0 0 4
2 5 2
0 0 2
2 0 4
*/

#include<bits/stdc++.h>
#define inf 100000000
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
using namespace std;
int ans,sum,n,m,a[15][15];
int fg=1,vv,vis[15],cnt,ent;
int z[15][15],f[15];
struct node{int a,b,c;}t[30];
inline void dfs(int u,int x){
	if(cnt==n){
		ans=min(ans,sum);
		return;
	}
	rep(i,1,n){
		if(a[u][i]){
			sum+=a[u][i]*x;cnt++;
			dfs(i,x+1);
			sum-=a[u][i]*x;cnt--;
		}
	}
}
inline void dfs2(int u,int x){
	if(cnt==n){
		ans=min(ans,sum);
		return;
	}
	rep(i,1,n){
		if(a[u][i]){
			sum+=z[u][i]*x;cnt++;
			dfs(i,x+1);
			sum-=z[u][i]*x;cnt--;
		}
	}
}
inline int getf(int x){
	if(f[x]!=x) f[x]=getf(f[x]);
	return f[x];
}
inline void bfs(int u)
{
	queue<int> q;
	q.push(u);vis[u]=1;
	while(!q.empty()){
		int x=q.front();q.pop();
		rep(i,1,n)
		if(a[x][i]&&!vis[i]){
			sum+=vv*cnt;
			q.push(i);vis[i]=1;
		}
		cnt++;
	}
}
bool cmp(node a,node b){
	return a.c<b.c;
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	rep(i,1,m){
		int u,v,w;scanf("%d %d %d",&u,&v,&w);
		if(!a[u][v]) a[u][v]=a[v][u]=w;
		else a[u][v]=min(a[u][v],w),a[v][u]=min(a[v][u],w);
		if(i==1) vv=w;
		else if(vv!=w) fg=0;
	}
	if(m==n-1){
		ans=inf;
		rep(i,1,n) {
			sum=0;cnt=1;
			dfs(i,1);
		}
		printf("%d\n",ans);
	}else if(fg){
		ans=inf;
		rep(i,1,n){
			sum=0;cnt=1;
			memset(vis,0,sizeof(vis));
			bfs(i);
			ans=min(ans,sum);
		}
		printf("%d\n",ans);
	}else{
		rep(i,1,n) f[i]=i;
		rep(i,1,n-1) rep(j,i+1,n)
		if(a[i][j])
			t[++ent]=(node){i,j,a[i][j]};
		sort(t+1,t+ent+1,cmp);
		int conut=0;
		rep(i,1,ent){
			int f1=getf(t[i].a);
			int f2=getf(t[i].b);
			if(f1!=f2){
				f[f1]=f2;
				conut++;
				z[t[i].a][t[i].b]=t[i].c;
				z[t[i].b][t[i].a]=t[i].c;
			}
			if(conut==n-1) break;
		}
	//	rep(i,1,n) {printf("\n");
		//rep(j,1,n) printf("%d ",z[i][j]);}
		ans=inf;
		rep(i,1,n) {
			sum=0;cnt=1;
			dfs2(i,1);
		}
		printf("%d\n",ans);
	}
	return 0;
}/*
4 5
1 2 1
1 3 3
1 4 1
2 3 4
3 4 1
*/

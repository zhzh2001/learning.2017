#include<bits/stdc++.h>
#define rep(i,a,b) for(int i=(a);i<=(b);i++)
using namespace std;
int n,m,q;
int a[1010][1010];
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d %d %d",&n,&m,&q);
	rep(i,1,n) rep(j,1,m) a[i][j]=(i-1)*m+j;
	while(q--){
		int x,y,t;
		scanf("%d %d",&x,&y);
		t=a[x][y];
		printf("%d\n",t);
		rep(i,y,m-1) a[x][i]=a[x][i+1];
		rep(i,x,n-1) a[i][m]=a[i+1][m];
		a[n][m]=t;
	}
	return 0;
}
/*
2 2 3
1 1
2 2
1 2
*/

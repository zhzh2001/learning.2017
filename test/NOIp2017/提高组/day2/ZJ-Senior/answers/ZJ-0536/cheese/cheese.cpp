#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdio>
#include <cmath>
using namespace std;
const double eps = 1e-9;
int n,h,r;
vector <int> lt[1005];
bool vd[1005];
double rr;
struct axis{
	long long x , y , z;
};
axis ax[1005];
inline long long sqrr(long long a){
	return a*a;
}
inline double abss(double a){
	if (a >= 0) return a;
	return -a;
}
bool vergepd(axis a , axis b){
	double dis = sqrr(a.x-b.x)+sqrr(a.y-b.y)+sqrr(a.z-b.z);
	dis = sqrt(dis);
	if (dis<=rr+eps) return true;
	return false;
}
void dfs(int o){
	vd[o] = true;
	for (int i = 0; i < lt[o].size(); i++)
	if (!vd[lt[o][i]])
	dfs(lt[o][i]);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	cin>>t;
while (t--){
	cin>>n>>h>>r;
	rr = r*2;
	for (int i = 0; i <= n+1; i++) lt[i].clear();
	for (int i = 0; i <= n+1; i++) vd[i] = 0;
	for (int i = 1; i <= n; i++)
	cin>>ax[i].x>>ax[i].y>>ax[i].z;
	for (int i = 1; i <= n; i++)
	if (ax[i].z<=r){
		lt[0].push_back(i);
		lt[i].push_back(0);
	}
	for (int i = 1; i <= n; i++)
	if (ax[i].z>=h-r){
		lt[n+1].push_back(i);
		lt[i].push_back(n+1);
	}
	for (int i = 1; i <= n; i++)
	for (int j = i+1; j <= n; j++)
	if (vergepd(ax[i],ax[j])){
		lt[i].push_back(j);
		lt[j].push_back(i);
	}
	/*
	for (int i = 0; i <= n+1; i++){
		cout<<i<<":";
		for (int j = 0; j < lt[i].size(); j++)
		cout<<lt[i][j]<<" ";
		cout<<endl;
	}
	*/
	dfs(0);
	if (vd[n+1]) cout<<"Yes"<<endl;
	else cout<<"No"<<endl;
}
return 0;
}

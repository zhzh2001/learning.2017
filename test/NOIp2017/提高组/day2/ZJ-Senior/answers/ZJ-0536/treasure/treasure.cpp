#include <algorithm>
#include <cstdio>
#include <iostream>
#include <vector>
using namespace std;
const long long inf = 100000007;
long long n,m;
long long dep[20];
long long w[15][15];
long long dp[5005];
long long dd[5005][15];
long long sum , ans;
void printttt(int i){
	if (i == 0) return;
	printttt(i/2);
	cout<<i%2;
}
void print(int o){
	cout<<"***"<<endl;
	printttt(o);
	cout<<endl;
	cout<<dp[o]<<endl;
	for (int i = 1; i <= n; i++) cout<<dd[o][i]<<" ";
	cout<<endl;
	cout<<"***"<<endl;
}
inline long long minn(long long a , long long b){
	if (a < b) return a;
	return b;
}
void treedfs(long long o){
	for (int i = 1; i <= n; i++)
	if ((w[o][i]!=inf) && (dep[i]==0)){
		sum += w[o][i] * dep[o];
		dep[i] = dep[o] + 1;
		treedfs(i);
	}
}
void sear(long long sta){
	if (sta == 0) return;
	if (dp[sta]!=inf)
		return;
	for (int i = 1; i <= n; i++) // sta bit
	if ((sta>>(i-1))&1){
		long long o = sta^(1<<(i-1));
		sear(o);
		for (int j = 1; j <= n; j++)
		dd[sta][j] = minn(dd[sta][j],dd[o][j]);
	}
	for (int i = 1; i <= n; i++) // sta bit
	if ((sta>>(i-1))&1){
		long long o = sta^(1<<(i-1));
		for (int j = 1; j <= n; j++) // o bit
		if (((o>>(j-1))&1) && w[j][i] != inf){
			dd[sta][i] = minn(dd[sta][i],dd[o][j]+1);
		}
	}
	for (int i = 1; i <= n; i++) // sta bit
	if ((sta>>(i-1))&1){
		long long o = sta^(1<<(i-1));
		for (int j = 1; j <= n; j++)
		if (((o>>(j-1))&1) && w[j][i] != inf)
		dp[sta] = minn(dp[sta],dp[o]+dd[o][j]*w[j][i]);
	}
//	print(sta);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	cin>>n>>m;
	for (int i = 1; i <= n; i++)
	for (int j = 1; j <= n; j++)
	w[i][j] = inf;
	for (int i = 1; i <= m; i++){
		int u,v,we;
		cin>>u>>v>>we;
		w[u][v] = minn(w[u][v],we);
		w[v][u] = minn(w[v][u],we);
	}
//	for (int i = 1; i <= n; i++){
//		for (int j = 1; j <= n; j++)
//		cout<<w[i][j]<<" ";
//		cout<<endl;
//	}
	/*
	for (int i = 1; i <= n; i++)
	for (int j = 1; j <= n; j++)
	dis[i][j] = w[i][j];
	for (int k = 1; k <= n; k++)
	for (int i = 1; i <= n; i++)
	for (int j = 1; j <= n; j++)
	dis[i][j] = min(dis[i][j],dis[i][k]+dis[k][j]);
	*/
if (m == n-1){
	ans = inf;
	for (int i = 1; i <= n; i++){
		sum = 0;
		for (int j = 1; j <= n; j++) dep[j] = 0;
		dep[i] = 1;
		treedfs(i);
		ans = min(ans,sum);
	}
	cout<<ans<<endl;
}
else{
	ans = inf;
	for (int i = 1; i <= n; i++){
		for (int j = 0; j <= ((1<<n)-1); j++){
			dp[j] = inf;
			for (int k = 1; k <= n; k++) dd[j][k] = inf;
		} 
		dp[(1<<(i-1))] = 0;
		dd[(1<<(i-1))][i] = 1;
		sear((1<<n)-1);
		ans = min(ans,dp[(1<<n)-1]);
	}
	cout<<ans<<endl;
}
	return 0;
}

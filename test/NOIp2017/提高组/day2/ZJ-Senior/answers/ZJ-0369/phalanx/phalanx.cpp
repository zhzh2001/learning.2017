#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define N 600000
#define lowbit(x) x&(-x)
using namespace std;

inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}

inline void read(int &x){
	char c=nc();
	for(;(c<'0' || c>'9');c=nc());
	for(x=0;(c>='0' && c<='9');x=(x<<3)+(x<<1)+c-'0',c=nc());
}

int n,sum[N],m,q;
inline void ADD(int x){
	while(x<=n+m+q-1){
		sum[x]++;
		x+=lowbit(x);
	}
}

inline int query(int x){
	int ret=0;
	while(x>0){
		ret+=sum[x];
		x-=lowbit(x);
	}
	return ret;
}

int x[N],y[N],t,f[N],l,r,map[1010][1010],a,b;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n); read(m); read(q);
	if(n<1010 && m<1010){
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				map[i][j]=(i-1)*m+j;
		while(q--){
			read(a); read(b);
			printf("%d\n",map[a][b]);
			swap(map[a][b],map[n+1][m]);
			for(int i=b;i<=m;i++)
				map[a][i]=map[a][i+1];
			for(int i=a;i<=n;i++)
				map[i][m]=map[i+1][m];
		}
		fclose(stdin); fclose(stdout);
		return 0;
	}
	for(int i=1;i<=q;i++)
		read(x[i]),read(y[i]);
	for(int i=1;i<=m;i++) f[i]=i;
	for(int i=2;i<=n;i++) f[m+i-1]=i*m;
	for(int i=1;i<=q;i++){
		l=1;r=i+n+m-2;
		while(l<r){
			int mid=(l+r)>>1,t=query(mid);
			if(y[i]+t>mid) l=mid+1;
			else r=mid;
		}
		printf("%d\n",f[l]);
		ADD(l);
		swap(f[l],f[n+m+i-1]);
	}
	fclose(stdin); fclose(stdout);
	return 0;
}

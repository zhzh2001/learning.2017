#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#include<queue>
#define LL long long
#define N 1010
using namespace std;

LL x[N],y[N],z[N],r;
inline int check(int a,int b){
	LL d=(x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]);
	return d<=r*r*4;
}

struct asd{
	int to,next;
}edge[N*N*2];

int cnt,head[N];
inline void ADD(int u,int v){
	edge[++cnt].to=v; edge[cnt].next=head[u]; 
	head[u]=cnt;
}

queue<int>Q,Q1;
int n,T,vis[N];
inline int bfs(){
	Q=Q1;
	Q.push(n+1);
	memset(vis,0,sizeof vis);
	vis[n+1]=1;
	while(!Q.empty()){
		int u=Q.front(); Q.pop();
		for(int i=head[u];i;i=edge[i].next)
			if(!vis[edge[i].to]){
				vis[edge[i].to]=1;
				Q.push(edge[i].to);
				if(edge[i].to==n+2) return 1;
			}
	}
	return 0;
}

int h;
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		cnt=0;
		memset(head,0,sizeof head);
		memset(edge,0,sizeof edge);
		scanf("%d %d %lld",&n,&h,&r);
		for(int i=1;i<=n;i++)
			scanf("%lld %lld %lld",&x[i],&y[i],&z[i]);
		for(int i=1;i<=n-1;i++)
			for(int j=i+1;j<=n;j++)
				if(check(i,j)) ADD(i,j),ADD(j,i);
		for(int i=1;i<=n;i++){
			if(z[i]<=r) ADD(i,n+1),ADD(n+1,i);
			if((LL)h-z[i]<=r) ADD(i,n+2),ADD(n+2,i);
		}
		if(bfs()) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

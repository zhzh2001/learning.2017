#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cstring>
#define N 13
using namespace std;

int n,m,u,x,y,z,S,ans;
int map[N][N],dis[1<<N][N],f[N][1<<N];
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	memset(map,0x3f,sizeof map);
	for(int i=1;i<=m;i++)
		scanf("%d %d %d",&x,&y,&z),map[x][y]=map[y][x]=min(map[x][y],z);
	u=(1<<n);
	memset(f,0x3f,sizeof f);
	for(int i=1;i<=n;i++){
		S=(1<<(i-1));
		memset(dis,0x3f,sizeof dis);
		for(int sta=S;sta<u;sta++) dis[sta][i]=1;
//		dis[S][i]=1;
		f[i][S]=0;
		for(int sta=S;sta<u;sta++){
			if(!(sta & S)) continue;
			for(int j=1;j<=n;j++)
				if(sta & (1<<(j-1)))
					for(int k=1;k<=n;k++)
						if(!(sta & (1<<(k-1))))
							if(map[j][k]<=500100 && dis[sta][j]<=100)
							if(dis[sta][j]*map[j][k]+f[i][sta]<f[i][sta+(1<<(k-1))]){
								for(int l=1;l<=n;l++) dis[sta+(1<<(k-1))][l]=dis[sta][l];
								dis[sta+(1<<(k-1))][k]=dis[sta][j]+1;
								f[i][sta+(1<<(k-1))]=dis[sta][j]*map[j][k]+f[i][sta];
							}
		}
	}
	ans=1<<29;
	for(int i=1;i<=n;i++)
		ans=min(ans,f[i][u-1]);
	printf("%d",ans);
	fclose(stdin); fclose(stdout);
	return 0;	
}

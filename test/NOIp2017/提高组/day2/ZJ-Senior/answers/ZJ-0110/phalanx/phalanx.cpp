#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,q;
int a[1010][1010];
int main() {
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf ("%d%d%d",&n,&m,&q);
	Rep(i,1,n) Rep(j,1,m) a[i][j]=(i-1)*m+j;
	while (q--) { int x,y;
		scanf ("%d%d",&x,&y);
		cout<<a[x][y]<<endl;
		int now=a[x][y];
		Rep(i,y,m-1) {
			a[x][i]=a[x][i+1];
		}
		Rep(i,x,n-1) {
			a[i][m]=a[i+1][m];
		}
		a[n][m]=now;
	}
	return 0;
}

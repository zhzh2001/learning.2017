//Live long and prosper.
#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
struct node{
	ff x,y,z,tot;
	int to[1010];
	bool shang,xia;
	void clear() {
		Rep(g,1,tot) to[g]=0;
		tot=0;
	}
}che[1010];
ff i,j,k,l,m,n,t,h,r,tot;
bool nen,pas,vis[1010],sta[1010];
inline void add_edge(int x,int y) {
	che[x].to[++che[x].tot]=y;
	che[y].to[++che[y].tot]=x;
}
inline void dfs(int x) {
	if (vis[x]==true) return;
	vis[x]=true;
	if (che[x].shang==true) {
		nen=true;
		exit;
	}
	Rep(i,1,che[x].tot) dfs(che[x].to[i]);
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf ("%d",&t);
	while (t--) {
		scanf ("%lld%lld%lld",&n,&h,&r);
		Rep(i,1,n) {
			scanf ("%lld%lld%lld",&che[i].x,&che[i].y,&che[i].z);
			if (che[i].z-r<=0) {
				che[i].xia=true;
				sta[++tot]=i;
			}
			if (che[i].z+r>=h) che[i].shang=true;
			che[i].clear();
		}
		Rep(i,1,n) Rep(j,i+1,n) {
			ff now;
			now=round(sqrt((che[i].x-che[j].x)*(che[i].x-che[j].x)+(che[i].y-che[j].y)*(che[i].y-che[j].y)+(che[i].z-che[j].z)*(che[i].z-che[j].z)));
			if (now<=2*r) add_edge(i,j);
		}
		pas=nen=false;
		Rep(i,1,tot) {
			memset(vis,0,sizeof(vis));
			dfs(sta[i]);
			if (nen==true) {
				pas=true;
				nen=false;
				break;
			}
		}
		if (pas==true) cout<<"Yes"<<endl; else cout<<"No"<<endl;
	}
	return 0;
}

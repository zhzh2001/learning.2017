#include<bits/stdc++.h>
using namespace std;
const int P=2139062143;
/*
struct edge{
	int sta,ed,val,jump,mark;
}a[2005],b[20];
bool mmp(edge a,edge b){
	return a.val<b.val;
}
*/
int n,m,tot=0,judge[1005],point[100005],num=0,jump[1005],ednum[1005],mina=1e9,mp[20][20],dam[1005];
/*
void add(int sta,int ed,int val){
	tot++;
	a[tot].sta=sta;
	a[tot].ed=ed;
	a[tot].val=val;
}
void adde(int sta,int ed,int val){
	tot++;
	b[tot].sta=sta;
	b[tot].ed=ed;
	b[tot].val=val;
	b[tot].jump=jump[sta];
	jump[sta]=tot;
}
*/
void dfs(int nu,int val){
		if (val>mina) return ;
		if (nu==n){
			mina=min(mina,val);
			return ;
		}
		for (int i=1; i<=n; i++){
			if (ednum[i]==dam[i]) continue;
			if (judge[i]){
			for (int l=1; l<=n; l++){
				if (judge[l] || mp[i][l]==P) continue;
				judge[l]=1;
				dam[i]++;
				dam[l]++;
				point[l]=point[i]+1;
				dfs(nu+1,val+mp[i][l]*point[i]);
				dam[i]--;
				dam[l]--;
				judge[l]=0;
			}
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(mp,127,sizeof(mp));
	scanf("%d%d",&n,&m);
	for (int i=1,x,y,z; i<=m; i++){
		scanf("%d%d%d",&x,&y,&z);
		mp[x][y]=min(mp[x][y],z);
		mp[y][x]=min(mp[y][x],z);
	}
//	/*
	for (int i=1; i<=n; i++){
		for (int l=1; l<=n; l++){
			if (i==l) continue;
			if (mp[i][l]!=P){
				ednum[i]++;
				ednum[l]++;
			}
		}
	}
//	*/
	for (int i=1; i<=n; i++){
		judge[i]=1;
		point[i]=1;
		dfs(1,0);
		judge[i]=0;
		point[i]=0;
	}
	printf("%d",mina);
	return 0;
}

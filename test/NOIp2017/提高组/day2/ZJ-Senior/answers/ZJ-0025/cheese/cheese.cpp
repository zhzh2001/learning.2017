#include<bits/stdc++.h>
using namespace std;
struct nob{
	long long x,y,z;
}a[1005];
bool mmp(nob a,nob b){
	return a.z>b.z;
}
long long T,n,h,r,tou,tod;
int judge[1005],len[1005];
double con(int x,int y){
	return double(sqrt(pow(a[x].x-a[y].x,2)+pow(a[x].y-a[y].y,2)+pow(a[x].z-a[y].z,2)));
}
int dfs(int pos){
	if (a[pos].z-r<=0){
		return 1;
	}
	for (int i=pos; a[pos].z-a[i].z<=2*r && i<=n; i++){
		if (judge[i]) continue ;
		if (len[i]) continue;
		if (con(i,pos)<=2*r){
			judge[i]=1;
			if (dfs(i)){
				return 1;
			}
			judge[i]=0;
		}
	}
	for (int i=pos; a[i].z-a[pos].z<=2*r && i; i--){
		if (judge[i]) continue;
		if (len[i]) continue;
		if (con(i,pos)<=2*r){
			judge[i]=1;
			if (dfs(i)){
				return 1;
			}
			judge[i]=0;
		}
	}
	len[pos]=1;
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	while (T--){
		int ko=0;
		tou=0;
		tod=0;
		scanf("%lld%lld%lld",&n,&h,&r);
		for (int i=1; i<=n; i++){
			judge[i]=0;
			len[i]=0;
		}
		int co=0,coo=0;
		for (int i=1; i<=n; i++){
			scanf("%lld%lld%lld",&a[i].x,&a[i].y,&a[i].z);
			co=0;
			if(a[i].z+r>=h){
				tou=1;
				co++;
			}
			if (a[i].z-r<=0){
				tod=1;
				co++;
			}
			if (co==2){
				coo=1;
			}
		}
		sort(a+1,a+1+n,mmp);
		if (!tou || !tod){
			printf("No\n");
			continue;
		}
		if (coo){
			printf("Yes\n");
			continue;
		}
		for (int i=1; i<=n; i++){
			if (a[i].z+r<h) break;
			judge[i]=1;
			if (dfs(i)){
				printf("Yes\n");
				ko=1;
				break;
			}
			judge[i]=0;
		}
		if (!ko) printf("No\n");
	}
	return 0;
}

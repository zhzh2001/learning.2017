#include<bits/stdc++.h>
using namespace std;
const int MAXN=200000+100;
int T,n,h,r;
int link[MAXN];
int len;
int cnt;
int cl;
int q[MAXN];
int vis[MAXN];
int en[MAXN];
int cs;
bool laji;
struct edge
{
	int y,next;
}e[MAXN];
struct tu
{
	int id,x,y,z;
}lzy[MAXN];
struct lingshi
{
	int id,x,y,z,mmp;
}ls[MAXN];
int read()
{
	int sum=0,flag=1;
	char c;
	for(;c<'0'||c>'9';c=getchar())if(c=='-') flag=-1;
	for(;c>='0'&&c<='9';c=getchar())sum=(sum<<1)+(sum<<3)+c-'0';
	return sum*flag;
}
double dist(int x, int y,int z,int xx, int yy,int zz)
{
	return sqrt((x-xx)*(x-xx)+(y-yy)*(y-yy)+(z-zz)*(z-zz));
}
void insert(int x,int y)
{
	e[++len].next=link[x];link[x]=len;
	e[len].y=y;
}
void bfs(int k)
{
	q[1]=k;vis[k]=1;
	int head=0,tail=1;
	while(head++<tail)
	{
		for(int i=link[q[head]];i;i=e[i].next)
		{
			int y=e[i].y;
			if(!vis[y])
			{
				q[++tail]=y;
				vis[y]=1;
			}
		}
	}
}
void work()
{
	for(int i=1;i<=cs;++i)
	{
		for(int j=1;j<=cl;++j)
		{
			if(dist(lzy[i].x,lzy[i].y,lzy[i].z,ls[j].x,ls[j].y,ls[j].z)<=2*r)
			{
				insert(lzy[i].id,ls[j].id);
				insert(ls[j].id,lzy[i].id);
			}
		}
	}
	for(int i=1;i<=cl;++i)
	for(int j=i+1;j<=cl;++j)
		if(dist(ls[i].x,ls[i].y,ls[i].z,ls[j].x,ls[j].y,ls[j].z)<=2*r)
			{
				insert(ls[i].id,ls[j].id);
				insert(ls[j].id,ls[i].id);
			}
	for(int i=1;i<=cs;++i) 
	{
	memset(q,0,sizeof(q)); 
	bfs(lzy[i].id);
	}
	for(int i=1;i<=cl;++i)
	{
		if(ls[i].mmp==1&&vis[ls[i].id]) {printf("Yes\n");return;}
	}
	printf("No\n");
}
void init()
{
	T=read();
	for(int i=1;i<=T;++i)
	{
		n=read();h=read();r=read();
		cs=0,cl=0;
		len=0;
		memset(vis,0,sizeof(vis));
		memset(en,0,sizeof(en));
		memset(e,0,sizeof(e));
		memset(lzy,0,sizeof(lzy));
		memset(q,0,sizeof(q));
		memset(link,0,sizeof(link));
		memset(ls,0,sizeof(ls));
		laji=false;
		int maxx=0,minn=1e9;
		int x,y,z;
		cnt=0;
		for(int j=1;j<=n;++j)
		{
			x=read();y=read();z=read();
			if(z-r<=0&&z+r>=h) laji=true;
			if(z-r<=0)
			{
				lzy[++cs].id=++cnt;
				lzy[cs].x=x;
				lzy[cs].y=y;
				lzy[cs].z=z;
			}
			else
			{
				ls[++cl].x=x;
				ls[cl].y=y;
				ls[cl].z=z;
				ls[cl].id=++cnt;
				if(z+r>=h) ls[cl].mmp=1;
			}
			minn=min(minn,z);
			maxx=max(maxx,z);
		}
		if(laji) {printf("Yes\n");continue;}
		if(minn-r>0||maxx+r<h) {printf("No\n");continue;}
		work();

	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	init();
	return 0;
}

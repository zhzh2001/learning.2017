#include<bits/stdc++.h>
using namespace std;
const int MAXN=100000+10;
int link[MAXN];
int len;
int n,m;
int d[MAXN];
int ans;
queue<int> q;
int root;
int in[MAXN];
struct edge
{
	int y,next,v;
}e[MAXN];
int read()
{
	int sum=0,flag=1;
	char c;
	for(;c<'0'||c>'9';c=getchar())if(c=='-') flag=-1;
	for(;c>='0'&&c<='9';c=getchar())sum=(sum<<1)+(sum<<3)+c-'0';
	return sum*flag;
}
void insert(int x,int y,int v)
{
	e[+len].next=link[x];link[x]=len;
	e[len].v=v;e[len].y=y;
}
void bfs()
{
	d[root]=1;q.push(root);
	int x=q.front();q.pop();
	while(q.size())
	{
	for(int i=link[x];i;i=e[i].next)
	{
		int y=e[i].y;
		if(d[y]) continue;
		d[y]=d[x]+1;
		q.push(y);
	}
	}
}
void init()
{
	n=read();m=read();
	int x,y,v;
	for(int i=1;i<=m;++i)
	{
		x=read();y=read();v=read();
		insert(x,y,v);
		insert(y,x,v);
		in[y]++;
	}
	for(int i=1;i<=n;++i) if(!in[i]) root=i;
	for(int i=1;i<=n;++i) ans+=d[i]*v;
	printf("%d",ans);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	init();
	return 0;
}

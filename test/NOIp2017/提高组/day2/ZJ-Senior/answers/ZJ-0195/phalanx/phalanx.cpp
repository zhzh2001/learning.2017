#include<bits/stdc++.h>
using namespace std;
const int MAXN=300000+100;
int a[1500][1500];
int aa[MAXN];
int n,m,q;
int h[MAXN],l[MAXN];
int x[MAXN],y[MAXN];
int read()
{
	int sum=0,flag=1;
	char c;
	for(;c<'0'||c>'9';c=getchar())if(c=='-') flag=-1;
	for(;c>='0'&&c<='9';c=getchar())sum=(sum<<1)+(sum<<3)+c-'0';
	return sum*flag;
}
void work1()
{
	for(int i=1;i<=n;++i)
	for(int j=1;j<=m;++j)
	{
		a[i][j]=(i-1)*m+j;
	}
	int t;
	for(int i=1;i<=q;++i)
	{
		t=a[x[i]][y[i]];
		printf("%d\n",t);
		for(int j=y[i];j<=m;++j)
		{
			a[x[i]][j]=a[x[i]][j+1];
		}
		for(int j=x[i];j<=n;++j)
		{
			a[j][m]=a[j+1][m];
		}
		a[n][m]=t;
	}
}
void work2()
{
	for(int i=1;i<=m;++i) aa[i]=i;
	int t;
	for(int i=1;i<=q;++i)
	{
		t=aa[y[i]];
		printf("%d\n",t);
		for(int j=y[i];j<=m;++j) aa[j]=aa[j+1];
		aa[m]=t;
	}
}
void work3()
{
	for(int i=1;i<=m;++i) h[i]=i;
	for(int i=1;i<=n;++i) l[i]=l[i-1]+m;
	int t;
	for(int i=1;i<=q;++i)
	{
		t=h[y[i]];
		printf("%d\n",t);
		for(int j=y[i];j<=m;++j) h[j]=h[j+1];
		for(int j=1;j<=n;++j) l[i]=l[i+1];
		l[n]=t;
	}
}
void init()
{
	n=read();m=read();q=read();
	for(int i=1;i<=q;++i)
	{
		x[i]=read();y[i]=read();
	}
	if(n<=1000&&m<=1000) work1();
	else if(n==1) work2();
	else work3();
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	init();
	return 0;
}

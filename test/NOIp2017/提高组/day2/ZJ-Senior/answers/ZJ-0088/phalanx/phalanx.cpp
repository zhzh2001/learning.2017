#include<cstdio>
#include<cstring>
#include<map>
#define PII pair<int, int>
#define mp make_pair
using namespace std;

const int N = 300005;
int n, m, q;
int a[1010][1010];

map<PII, int> M;

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	scanf("%d%d%d", &n, &m, &q);
	if (n <= 1000 && m <= 1000){
		for (int i = 1; i <= n; i ++)
			for (int j = 1; j <= m; j ++) a[i][j] = (i-1)*m+j;
		for (int i = 1; i <= q; i ++){
			int x, y; scanf("%d%d", &x, &y);
			printf("%d\n", a[x][y]);
			int tmp = a[x][y];
			for (int j = y+1; j <= m; j ++) a[x][j-1] = a[x][j];
			for (int j = x+1; j <= n; j ++) a[j-1][m] = a[j][m];
			a[n][m] = tmp;
		}
		return 0;
	}
	if (q <= 500){
		while (q --){
			int x, y; scanf("%d%d", &x, &y);
			int tmp;
			if (!M.count(mp(x, y))) tmp = (x-1)*m+y;
			else tmp = M[mp(x, y)];
			printf("%d\n", tmp);
			for (int j = y+1; j <= m; j ++){
				int t;
				if (!M.count(mp(x, j))) t = (x-1)*m+j;
				else t = M[mp(x, j)];
				M[mp(x, j-1)] = t;
			}
			for (int j = x+1; j <= n; j ++){
				int t;
				if (!M.count(mp(j, m))) t = (j-1)*m+m;
				else t = M[mp(j, m)];
				M[mp(j-1, m)] = t;
			}
			M[mp(n, m)] = tmp;
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
using namespace std;
typedef long long LL;

const int N = 1010;
int n, h, r, S, T;
int fa[N];

struct Node{
	int x, y, z;
}a[N];

inline int getFa(int v)
{
	return fa[v] == v ? v : fa[v] = getFa(fa[v]);
}

inline void add(int x, int y)
{
	int fx = getFa(x), fy = getFa(y);
	if (fx != fy) fa[fx] = fy;
}

inline bool ok(int i, int j)
{
	double dis = (double)(a[i].x-a[j].x)*(a[i].x-a[j].x);
	dis += (double)(a[i].y-a[j].y)*(a[i].y-a[j].y);
	dis += (double)(a[i].z-a[j].z)*(a[i].z-a[j].z);
	double t = 4.0*r*r;
	return dis <= t;
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int Test; scanf("%d", &Test);
	while (Test --){
		scanf("%d%d%d", &n, &h, &r);
		S = n+1; T = n+2;
		for (int i = 1; i <= T; i ++) fa[i] = i;
		for (int i = 1; i <= n; i ++){
			scanf("%d%d%d", &a[i].x, &a[i].y, &a[i].z);
			if (a[i].z + r >= h && a[i].z - r <= h){
				add(i, T);
				//printf("%d\n", i);
			}
			if (a[i].z - r <= 0 && a[i].z + r >= 0){
				add(i, S);
				//printf("%d\n", i);
			}
			for (int j = 1; j < i; j ++){
				if (ok(i, j)){
					add(i, j);
					//printf("%d %d\n", i, j);
				}
			}
		}
		int fx = getFa(S), fy = getFa(T);
		if (fx != fy) puts("No");
		else puts("Yes");
	}
	return 0;
}

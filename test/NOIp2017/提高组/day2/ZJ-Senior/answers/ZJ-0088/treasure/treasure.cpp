#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#include<ctime>
using namespace std;

const int N = 15;
const int INF = 1e9;
int n, m, ans, cost, tmp;
int w[N][N], g[N][N];

inline void solve(int u, int fa, int s)
{
	cost += s*(w[fa][u]);
	for (int i = 1; i <= n; i ++)
		if (g[u][i] && i != fa){
			solve(i, u, s+1);
		}
}

inline void dfs(int s, int sta, int vis)
{
	//s是当前层数，sta是当前层有那些点的状压，vis是所有出现过的点的状压
	//cost存当前的花费数 
	if (vis == (1<<n)-1){
		tmp = min(tmp, cost);
		//printf("%d\n", cost);
		return;
	}
	int now = 0;//now存下一层可以转移的点的状压
	int mi[N];
	memset(mi, 0x3f, sizeof mi);//mi存下一层点和上一层点的最小边权 
	for (int i = 1; i <= n; i ++)
		if ((sta>>i-1)&1){
			for (int j = 1; j <= n; j ++)
				if (!((vis>>j-1)&1) && g[i][j]){
					now |= 1<<j-1;
					mi[j] = min(mi[j], w[i][j]);
				}
		}
	/*printf("层 = %d\n", s);
	printf("当前的花费 = %d\n", cost);
	printf("当前的点：");
	for (int i = 1; i <= n; i ++)
		if ((sta>>i-1)&1) printf("%d ", i);
	puts("");
	printf("下一层可以选的点\n");
	for (int i = 1; i <= n; i ++)
		if ((now>>i-1)&1) printf("%d, mincost = %d\n", i, mi[i]);*/
	for (int i = 1; i < 1<<n; i ++)
		if ((i|now) == now){
			//枚举下一层要选的点
			for (int j = 1; j <= n; j ++) if ((i>>j-1)&1)
				cost += mi[j]*(s+1);
			dfs(s+1, i, vis|i);
			for (int j = 1; j <= n; j ++) if ((i>>j-1)&1)
				cost -= mi[j]*(s+1);
		}
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i ++)
		for (int j = 1; j <= n; j ++)
			if (i == j) w[i][j] = 0; else w[i][j] = INF;
	for (int i = 1; i <= m; i ++){
		int x, y, z; scanf("%d%d%d", &x, &y, &z);
		w[x][y] = min(w[x][y], z);
		w[y][x] = w[x][y];
		g[x][y] = g[y][x] = 1;
	}
	/*if (m == n-1){
		ans = INF;
		for (int s = 1; s <= n; s ++){
			cost = 0;
			solve(s, 0, 0);
			ans = min(ans, cost);
		}
		printf("%d\n", ans);
		return 0;
	}*/
	ans = INF;
	for (int s = 1; s <= n; s ++){
		cost = 0; tmp = INF;
		dfs(0, 1<<s-1, 1<<s-1);
		ans = min(ans, tmp);
	}
	printf("%d\n", ans);
	/*double time = clock() / (double) CLOCKS_PER_SEC;
	fprintf(stderr, "%.10lf\n", time);*/
	return 0;
}

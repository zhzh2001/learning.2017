 var g:array[0..20,0..20]of longint;
     vis:array[0..20]of boolean;
     f:array[0..20]of longint;
     ans,n,m:longint;
 procedure dfs(x,y:longint);
  var i,j:longint;
   begin
    if y>=ans then exit;
    if x=n then begin
                 ans:=y;exit;
                end;
    for i:=1 to n do
     if vis[i] then
     for j:=1 to n do
      if not vis[j] then
      if g[i,j]<>g[0,0] then begin
                              vis[j]:=true;f[j]:=f[i]+1;
                              dfs(x+1,y+f[i]*g[i,j]);
                              vis[j]:=false;
                             end;
    end;
 procedure init;
  begin
    assign(input,'treasure.in');reset(input);
    assign(output,'treasure.out');rewrite(output);
   end;
 procedure main;
  var i,j,x,y,z:longint;
   begin
    fillchar(g,sizeof(g),63);
    read(n,m);
    for i:=1 to m do
     begin
      read(x,y,z);
      if z<g[x,y] then g[x,y]:=z;
      if z<g[y,x] then g[y,x]:=z;
     end;
    ans:=maxlongint;
    for i:=1 to n do
     begin
      fillchar(f,sizeof(f),0);
      fillchar(vis,sizeof(vis),0);
      vis[i]:=true;
      f[i]:=1;
      dfs(1,0);
     end;
   end;
 procedure print;
  begin
   writeln(ans);
   close(input);close(output);
  end;
 begin
  init;
  main;
  print;
 end.

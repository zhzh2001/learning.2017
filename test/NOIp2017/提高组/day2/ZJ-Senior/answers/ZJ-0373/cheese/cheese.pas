 var h,r:int64;
     Q,t,n:longint;
     a,b,c:array[0..1005]of int64;
     fa:array[0..1005]of longint;
 function js(i,j:longint):int64;
   begin
    js:=(a[i]-a[j])*(a[i]-a[j])+(b[i]-b[j])*(b[i]-b[j])+(c[i]-c[j])*(c[i]-c[j]);
   end;
 function pd(i,j:longint):boolean;
  begin
   if js(i,j)<=r*r then exit(true) else exit(false);
  end;
 function getf(x:longint):longint;
  begin
   if fa[x]=x then exit(x);
   fa[x]:=getf(fa[x]);
   exit(fa[x]);
  end;
 procedure merge(x,y:longint);
  begin
   x:=getf(x);
   y:=getf(y);
   if x<>y then fa[y]:=x;
  end;
 procedure main;
  var i,j:longint;
   begin
    read(n,h,r);
    r:=r*2;
    for i:=1 to n do read(a[i],b[i],c[i]);
    for i:=1 to n+2 do fa[i]:=i;
    for i:=1 to n-1 do
     for j:=i+1 to n do
      if pd(i,j) then merge(i,j);
    for i:=1 to n do
     if c[i]*2<=r then merge(i,n+1);
    for i:=1 to n do
     if (h-c[i])*2<=r then merge(i,n+2);
    if getf(n+1)=getf(n+2) then writeln('Yes')
                           else writeln('No');
   end;
 begin
  assign(input,'cheese.in');reset(input);
  assign(output,'cheese.out');rewrite(output);
  read(Q);
  for t:=1 to Q do main;
  close(input);close(output);
 end.

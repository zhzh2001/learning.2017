#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define pol(i, b, e) for(int i=b; i>=e; --i)
#define LL long long
#define test(x) std :: cout << #x << " = " << x << std :: endl;
#define file(x) freopen(x".in", "r", stdin);freopen(x".out", "w", stdout);
#define mset(x) memset(x, 0, sizeof x);
const int N = 12;
const int INF = 0x3f3f3f3f;
int n, m, maxn, ans;
int lg[(1<<12)+1], d[N][N], dis[N][1<<13], f[1<<13]; //warning!!!!!!!!!!!!!!!!

int getint() {
	int x = 0, k = 1;
	char c = getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') k=-k;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x * k;
}

inline void init() {
	n = getint()-1; m = getint(); maxn = (1<<n+1) - 1;
	memset(d, 0x3f, sizeof d);
	int u, v;
	lop(i, 1, m) {
		u = getint(), v = getint();
		d[u-1][v-1] = d[v-1][u-1] = getint();
	}
	int l=0;
	lop(i, 0, n) lg[1<<i] = i;
	ans = INF;
}

inline int lowbit(int x) {
	return x&-x;
}

inline int min(int a, int b) {
	return a<b?a:b;
}

inline int getlen(int x) {
	int ret = 0;
	for(; x; x-=lowbit(x)) ++ret;
	return ret;
}

inline void predo() {
	memset(dis, 0x3f, sizeof dis);
	int tj, tk, ii, kk, len;
	lop(i, 0, n) dis[i][1<<i] = 0;
	lop(j, 1, maxn) {
		tj = j;
		len = getlen(j);
		for(int i=lowbit(tj); tj; tj-=i, i=lowbit(tj)) {
			tk = j; ii = lg[i];
			for(int k=lowbit(tk); k; tk-=k, k=lowbit(tk)) {
				kk = lg[k];
				if(d[ii][kk] != INF)
					dis[ii][j] = min(dis[ii][j], dis[kk][j-i] + d[ii][kk] * (len-1));
			}
		}
	}
	int ttt = 1;
}

inline void dodp(int x) {
	memset(f, 0x3f, sizeof f);
	f[1<<x] = 0;
	int ti, ii;
	lop(j, 1, maxn) if(f[j] != INF) {
		ti = j;
		for(int i=lowbit(ti); ti; ti-=i, i=lowbit(ti)) {
			ii = lg[i];
			lop(k, 1, maxn) {
				f[j | k] = min(f[j | k], f[j] + dis[x][k]);
			}
		}
	}
	ans = min(ans, f[maxn]);
}

int main() {
	file("treasure");
	init();
	predo();
	lop(i, 0, n)
		dodp(i);
	printf("%d\n", ans);
	return 0;
}


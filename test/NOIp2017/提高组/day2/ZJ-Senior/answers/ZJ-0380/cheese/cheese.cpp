#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define pol(i, b, e) for(int i=b; i>=e; --i)
#define LL long long
#define test(x) std :: cout << #x << " = " << x << std :: endl;
#define file(x) freopen(x".in", "r", stdin); freopen(x".out", "w", stdout);
#define mset(x) memset(x, 0, sizeof x);
const int N = 1000 + 5;
int n, l, ri, q[N];
LL h, r;
LL x[N], y[N], z[N];
bool flag;
bool can[N][N], vis[N];

int getint() {
	int x = 0, k = 1;
	char c = getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') k=-k;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x * k;
}

bool pan(int i, int j) {
	return ((x[i]-x[j])*(x[i]-x[j]) + (y[i]-y[j])*(y[i]-y[j]) + (z[i]-z[j])*(z[i]-z[j])) <= r * r * 4;
}

void init() {
	mset(can); mset(vis); flag = false;
}

int main() {
	file("cheese");
	for(int Ti = getint(); Ti; Ti--) {
		init();
		n = getint(); h = getint(); r = getint(); //LL!
		lop(i, 1, n) x[i] = getint(), y[i] = getint(), z[i] = getint(); //LL!
		lop(i, 1, n-1) {
			lop(j, i+1, n) {
				can[i][j] = can[j][i] = pan(i, j);
			}
		}
		lop(i, 1, n) {
			if(z[i] <= r) can[0][i] = true;
			if(z[i] + r >= h) can[i][n+1] = true;
		}
		q[1] = 0; l = ri = 1;
		while(l <= ri) {
			lop(i, 1, n+1) if(!vis[i] && can[q[l]][i]) {
				vis[i] = true;
				q[++ri] = i;
			}
			if(vis[n+1]) {
				flag = true;
				break;
			}
			++l;
		}
		if(flag) printf("Yes\n");else printf("No\n");
	}
	return 0;
}


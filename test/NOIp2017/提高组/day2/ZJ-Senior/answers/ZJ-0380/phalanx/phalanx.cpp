#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <iostream>
#define lop(i, b, e) for(int i=b; i<=e; ++i)
#define pol(i, b, e) for(int i=b; i>=e; --i)
#define LL long long
#define test(x) std :: cout << #x << " = " << x << std :: endl;
#define file(x) freopen(x".in", "r", stdin); freopen(x".out", "w", stdout);
#define mset(x) memset(x, 0, sizeof x);
const int N = 1000 + 5;
const int M = 300000 + 5;
int n, m, q, tot, x, y, temp, l, r, asum, nn, mid;
int a[N][N], b[M*2], c[M*2], bit[M*2];

int getint() {
	int x = 0, k = 1;
	char c = getchar();
	for(; c<'0' || c>'9'; c=getchar()) if(c=='-') k=-k;
	for(; c>='0' && c<='9'; c=getchar()) x=x*10+c-'0';
	return x * k;
}

inline int lowbit(int x) {
	return x&(-x);
}

inline void add(int x, int k) {
	for(int i=x; i<=nn; i+=lowbit(i)) bit[i] += k;
}

inline int sum(int x) {
	int ret = 0;
	for(int i=x; i; i-=lowbit(i)) ret += bit[i];
	return ret;
}

int find(int x) {
	l = 1; r = nn;
	while(l<=r) {
		mid = (l + r) >> 1;
		asum = sum(mid);
		if(asum == x) return mid;
		else if(asum > x) r = mid - 1;
		else l = mid + 1;
	}
}

void teshu() {
	nn = m + q;
	lop(i, 1, m) {
		add(i, 1);
		c[i] = i;
	}
	lop(i, 1, q) {
		x = getint(); y = getint();
		temp = find(y);
		printf("%d\n", c[temp]);
		add(temp, -1);
		add(m+i, 1);
		c[m+i] = temp;
	}
}

void teshu2() {
	nn = m + q;
	lop(i, 1, m) {
		add(i, 1);
		c[i] = i;
	}
	lop(i, 1, q) {
		x = getint(); y = getint();
		temp = find(y);
		printf("%d\n", c[temp]);
		add(temp, -1);
		add(m+i, 1);
		c[m+i] = m*(q+1);
	}
}

int main() {
	file("phalanx");
	n = getint(); m = getint(); q = getint();
		if(n==1) {
			teshu();
			return 0;
		} else if(n<=1000 && m<=1000) {
		tot = 0;
		lop(i, 1, n) {
			lop(j, 1, m) {
				a[i][j] = ++tot;
			}
		}
		lop(i, 1, q) {
			x = getint(); y = getint();
			temp = a[x][y];
			lop(j, y, m-1) a[x][j] = a[x][j+1];
			lop(j, x, n-1) a[j][m] = a[j+1][m];
			a[n][m] = temp;
			printf("%d\n", temp);
		}
	} else {
		teshu2();
		return 0;
	}
	return 0;
}


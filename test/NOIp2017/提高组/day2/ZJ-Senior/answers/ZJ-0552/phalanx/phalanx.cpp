#include <cstdio>
#include <algorithm>
#define fi first
#define se second
#define MP make_pair
using namespace std;
typedef long long ll;
typedef unsigned long long ull;
typedef pair<int, int> PII;

const int N = 301000 * 5;

int n, m, q, cnt;
int x[301000], y[301000], a[301000], b;

ull rnd()
{
	static ull seed = 1;
	seed ^= (seed << 5);
	seed ^= (seed >> 17);
	seed ^= (seed << 13);
	return seed;
}

struct node
{
	ll L, R;
	int l, r, siz;
	ull fix;
} T[N];

struct data
{
	int a, b, c;
	data()
	{
	}
	data(int _a, int _b, int _c)
	{
		a = _a;
		b = _b;
		c = _c;
	}
};

inline void up(int u)
{
	T[u].siz = T[T[u].l].siz + T[T[u].r].siz + (T[u].R - T[u].L + 1);
}

int merge(int u, int v)
{
	if (!u || !v) return u + v;
	if (T[u].fix > T[v].fix)
	{
		T[u].r = merge(T[u].r, v);
		up(u);
		return u;
	} else
	{
		T[v].l = merge(u, T[v].l);
		up(v);
		return v;
	}
}

data split(int u, int k)
{
	if (!u) return data(0, 0, 0);
	if (k <= T[T[u].l].siz)
	{
		data t = split(T[u].l, k);
		T[u].l = t.c;
		up(u);
		return data(t.a, t.b, u);
	}
	if (k > T[T[u].l].siz + (T[u].R - T[u].L + 1))
	{
		data t = split(T[u].r, k - T[T[u].l].siz - (T[u].R - T[u].L + 1));
		T[u].r = t.a;
		up(u);
		return data(u, t.b, t.c);
	}
	int t1 = T[u].l, t2 = T[u].r;
	T[u].l = T[u].r = 0;
	up(u);
	return data(t1, u, t2);
}

int main()
{
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	for (int i = 0; i < N; i++)
		T[i].fix = rnd();
	scanf("%d%d%d", &n, &m, &q);
	for (int i = 1; i <= q; i++)
		scanf("%d%d", &x[i], &y[i]);
	for (int i = 1; i <= n; i++)
	{
		a[i] = i;
		T[i].L = 1LL * (i - 1) * m + 1;
		T[i].R = 1LL * (i - 1) * m + m - 1;
		T[i].l = T[i].r = 0;
		T[i].siz = m - 1;
	}
	for (int i = 1; i <= n; i++)
	{
		T[n + i].L = T[n + i].R = 1LL * (i - 1) * m + m;
		T[n + i].l = T[n + i].r = 0;
		T[n + i].siz = 1;
	}
	b = n + 1;
	for (int i = n + 2; i <= n * 2; i++)
		b = merge(b, i);
	cnt = n * 2;
	for (int i = 1; i <= q; i++)
	{
		if (y[i] == m)
		{
			data t = split(b, x[i]);
			printf("%lld\n", T[t.b].L);
			b = merge(t.a, merge(t.c, t.b));
		} else
		{
			data t1 = split(a[x[i]], y[i]);
			int p = y[i] - T[t1.a].siz;
			ll t = T[t1.b].L + p - 1;
			int out;
			printf("%lld\n", t);
			if (T[t1.b].L < T[t1.b].R)
			{
				if (t == T[t1.b].L)
				{
					cnt++;
					T[cnt].L = T[cnt].R = t;
					T[cnt].l = T[cnt].r = 0;
					T[cnt].siz = 1;
					T[t1.b].L++;
					T[t1.b].siz--;
					out = cnt;
					a[x[i]] = merge(t1.a, merge(t1.b, t1.c));
				} else
					if (t == T[t1.b].R)
					{
						cnt++;
						T[cnt].L = T[cnt].R = t;
						T[cnt].l = T[cnt].r = 0;
						T[cnt].siz = 1;
						T[t1.b].R--;
						T[t1.b].siz--;
						out = cnt;
						a[x[i]] = merge(t1.a, merge(t1.b, t1.c));
					} else
					{
						cnt++;
						T[cnt].L = T[t1.b].L;
						T[cnt].R = t - 1;
						T[cnt].l = T[cnt].r = 0;
						T[cnt].siz = T[cnt].R - T[cnt].L + 1;
						cnt++;
						T[cnt].L = t + 1;
						T[cnt].R = T[t1.b].R;
						T[cnt].l = T[cnt].r = 0;
						T[cnt].siz = T[cnt].R - T[cnt].L + 1;
						T[t1.b].L = T[t1.b].R = t;
						T[t1.b].siz = 1;
						out = t1.b;
						a[x[i]] = merge(t1.a, merge(merge(cnt - 1, cnt), t1.c));
					}
			} else
			{
				a[x[i]] = merge(t1.a, t1.c);
				out = t1.b;
			}
			data t2 = split(b, x[i]);
			a[x[i]] = merge(a[x[i]], t2.b);
			b = merge(merge(t2.a, t2.c), out);
		}
	}
}

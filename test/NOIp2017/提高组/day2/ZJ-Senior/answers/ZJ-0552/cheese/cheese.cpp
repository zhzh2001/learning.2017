#include <cstdio>
#include <algorithm>
using namespace std;

int f[10000];

int n, S, T;
long long h, r, x[10000], y[10000], z[10000];

int sf(int u)
{
	return f[u] == u ? u : f[u] = sf(f[u]);
}

void join(int u, int v)
{
	f[sf(u)] = sf(v);
}

int main()
{
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	int cas;
	scanf("%d", &cas);
	while (cas--)
	{
		scanf("%d%lld%lld", &n, &h, &r);
		S = n + 1, T = n + 2;
		for (int i = 1; i <= T; i++) f[i] = i;
		for (int i = 1; i <= n; i++)
			scanf("%lld%lld%lld", &x[i], &y[i], &z[i]);
		for (int i = 1; i <= n; i++)
			if (-r <= z[i] && z[i] <= r)
				join(S, i);
		for (int i = 1; i <= n; i++)
			if (h - r <= z[i] && z[i] <= h + r)
				join(i, T);
		for (int i = 1; i <= n; i++)
			for (int j = i + 1; j <= n; j++)
				if ((x[i] - x[j]) * (x[i] - x[j]) + (y[i] - y[j]) * (y[i] - y[j]) <= r * r * 4 - (z[i] - z[j]) * (z[i] - z[j]))
					join(i, j);
		puts(sf(S) == sf(T) ? "Yes" : "No");
	}
}

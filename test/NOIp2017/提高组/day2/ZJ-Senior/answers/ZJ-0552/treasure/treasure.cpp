#include <cstdio>
#include <algorithm>
using namespace std;

const int INF = 1e9;
int n, m;

int a[100][100];
int f[14][13][1 << 13];
int b[100], ans, vis[100], c[100];
int num[1 << 13];

void dfs(int u, int s)
{
	if (s >= ans) return ;
	if (u > n)
	{
		ans = min(ans, s);
		return ;
	}
	for (int i = 1; i < u; i++)
		for (int j = 1; j <= n; j++)
			if (!vis[j] && a[b[i]][j] < INF)
			{
				b[u] = j;
				c[u] = c[i] + 1;
				vis[j] = 1;
				dfs(u + 1, s + a[b[i]][j] * c[i]);
				vis[j] = 0;
			}
}

int main()
{
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i < (1 << n); i++)
		num[i] = num[i >> 1] + (i & 1);
	if (n > 8)
	{
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				a[i][j] = INF;
		for (int i = 1; i <= m; i++)
		{
			int u, v, w;
			scanf("%d%d%d", &u, &v, &w);
			u--, v--;
			a[u][v] = min(a[u][v], w);
			a[v][u] = min(a[v][u], w);
		}
		for (int j = 1; j <= n; j++)
			for (int i = 0; i < n; i++)
				for (int k = 0; k < (1 << n); k++)
					f[j][i][k] = INF;
		for (int j = 1; j <= n; j++)
			for (int i = 0; i < n; i++)
				f[j][i][1 << i] = 0;
		int S = (1 << n) - 1;
		for (int j = n - 1; j >= 1; j--)
			for (int i = 0; i < n; i++)
				for (int k = 0; k < n; k++)
					if (k != i && a[i][k] < INF)
						for (int l = 0; l < (1 << n); l++)
							if (l >> i & 1)
								for (int p = S - l; p; p = (p - 1) & (S - l))
									f[j][i][l | p] = min(f[j][i][l | p], f[j][i][l] + f[j + 1][k][p] + j * a[i][k]);
		int ans = INF;
		for (int i = 0; i < n; i++)
			ans = min(ans, f[1][i][S]);
		printf("%d\n", ans);
	} else
	{
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				a[i][j] = INF;
		for (int i = 1; i <= m; i++)
		{
			int u, v, w;
			scanf("%d%d%d", &u, &v, &w);
			a[u][v] = min(a[u][v], w);
			a[v][u] = min(a[v][u], w);
		}
		ans = INF;
		for (int i = 1; i <= n; i++)
		{
			b[1] = i;
			c[1] = 1;
			vis[i] = 1;
			dfs(2, 0);
			vis[i] = 0;
		}
		printf("%d\n", ans);
	}
}

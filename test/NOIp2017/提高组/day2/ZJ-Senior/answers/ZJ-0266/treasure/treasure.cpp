#include<stdio.h>
#include<algorithm>
int min(int a,int b) {
	return a>b?b:a;
}
int minn;
int vis[100],a[100][100],n,m,x,y,v,deep1[100],ans,pd[100][100],sum1[100],Dep[100],minway,minss[100],minss2[100],Wa[110000],num;
int dfs(int a1,int b,int deep,int sum) {
	if (b+minss[n-sum]>=minn) return 0;
	if(sum==n) {
		if(b<minn) minn=b;
		return 0;
	}
	vis[a1]=1;
	deep1[a1]=deep;

	for( int j=1; j<=n; ++j) {
		if(vis[j])
			for( int i=1; i<=n; ++i) {
				if (!vis[i] && i!=j && pd[j][i]) {
					dfs(i,b+(deep1[j])*a[j][i],deep1[j]+1,sum+1);
				}
			}
	}
	deep1[a1]=vis[a1]=0;
}

int dfs2(int a1,int b,int deep,int sum) {
	if (b+minss2[n-sum]>=minn) return 0;
	if(sum==n) {
		if(b<minn) minn=b;
		return 0;
	}
	vis[a1]=1;
	deep1[a1]=deep;

	for( int j=n; j; --j) {
		if(vis[j])
			for( int i=n; i; --i) {
				if (!vis[i] && i!=j && pd[j][i]) {
					dfs(i,b+(deep1[j])*a[j][i],deep1[j]+1,sum+1);
				}
			}
	}
	deep1[a1]=vis[a1]=0;
}

int main() {
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1; i<=m; ++i) {
		scanf("%d%d%d",&x,&y,&v);
		num++,Wa[num]=v;
		if(x!=y) {
			if(pd[x][y])
				a[x][y]=a[y][x]=min(a[x][y],v);
			else a[x][y]=a[y][x]=v,pd[x][y]=pd[y][x]=1,sum1[x]++,sum1[y]++;
		}
	}
	std::sort(Wa+1,Wa+num+1);
	for(int i=1; i<=n; ++i)
		minss[i]=minss[i-1]+Wa[i];

	for(int i=1; i<=n; ++i)
		minss2[i]=minss2[i-1]+Wa[i+5];
	minn=2e9;
	for( int i=1; i<=n; ++i) {
		if(sum1[i]==n-1) {
			ans=0;
			for( int j=1; j<=n; ++j)
				if(i!=j) ans+=a[i][j];
			minn=min(minn,ans);
		}
	}
	if (n<=10)
		for(int i=1; i<=n; ++i)
			dfs(i,0,1,1);
	else
		for( int i=n; i; --i)
			dfs2(i,0,1,1);
	printf("%d",minn);
}

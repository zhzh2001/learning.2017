#include<queue>
#include<cstring>
#include<stdio.h>
#include<cmath>
int vis[10000],t,n,h,r,num,H[10000];
struct T {
	int x,y,z;
};
T point[10000];
struct T2 {
	int from,to,nxt;
};
T2 way[1200000];
double SQR(double a) {
	return a*a;
}
double Fr(T a,T b) {
	return sqrt(SQR(a.x-b.x)+SQR(a.y-b.y)+SQR(a.z-b.z));
}
std::queue <int> q;
int bfs() {
	vis[1]=1;
	q.push(1);
	while(!q.empty()) {
		int X=q.front();
		q.pop();
		for(int i=H[X]; i; i=way[i].nxt) {
			if(!vis[way[i].to]) {
				vis[way[i].to]=1;
				q.push(way[i].to);
			}
		}
	}
	return vis[n];
}
int main() {
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--) {
		num=0;
		memset(point,0,sizeof point);
		memset(way,0,sizeof way);
		memset(H,0,sizeof H);
		memset(vis,0,sizeof vis);
		scanf("%d%d%d",&n,&h,&r);
		for(int i=1; i<=n; ++i)
			scanf("%d%d%d",&point[i].x,&point[i].y,&point[i].z);
		for(int i=1; i<=n; ++i) {
			for(int j=1; j<=n; ++j) {
				if(i!=j&&Fr(point[i],point[j])<=r<<1) {
					num++;
					way[num].from=i+1;
					way[num].to=j+1;
					way[num].nxt=H[i+1];
					H[i+1]=num;
				}
			}
		}
		for(int i=1; i<=n; ++i) {
			if(point[i].z<=r) {
				num++;
				way[num].from=1;
				way[num].to=i+1;
				way[num].nxt=H[1];
				H[1]=num;
			}
			if(point[i].z+r>=h) {
				num++;
				way[num].from=i+1;
				way[num].to=n+2;
				way[num].nxt=H[i+1];
				H[i+1]=num;
			}
		}
		n+=2;
		if(bfs()) puts("Yes");
		else puts("No");
	}
}

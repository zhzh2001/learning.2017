#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
#define INF 0x7FFFFFFF
int V[15][15][15],dis[15][15];
int f1[15][15],f2[15][15],fa[15][15];
int n,m;
int main()
{
  freopen("treasure.in","r",stdin);
  freopen("treasure.out","w",stdout);
  int i,j,k,a,b,c,x,l,r;
  scanf("%d%d",&n,&m);
  for(i=1;i<=n;i++)
  {
    fa[i][i]=i;
    for(j=i+1;j<=n;j++)
    {
      f1[i][j]=INF;
      f2[i][j]=INF;
	  dis[i][j]=INF;
	  dis[j][i]=INF;
      for(k=i;k<=j;k++) V[k][i][j]=INF;
    }
  }
  for(i=1;i<=m;i++)
  {
    scanf("%d%d%d",&a,&b,&c);
    dis[a][b]=c;
    dis[b][a]=c;
    if(abs(a-b)==1)
	{
      V[a][a][b]=c;
	  V[b][a][b]=c;
	  f1[a][b]=c;
	  f2[a][b]=c+c;
	  fa[a][b]=a;
	}
  }
  for(i=2;i<n;i++)
  {
    for(j=1;j+i<=n;j++)
    {
      l=j;r=i+j;
      for(k=l+1;k<r;k++)
      {
        if(dis[k][fa[l][k-1]]<INF&&f2[l][k-1]<INF)
          x=dis[k][fa[l][k-1]]+f2[l][k-1];
        else x=INF;
        if(x<INF&&dis[k][fa[k+1][r]]<INF&&f2[k+1][r]<INF)
          x+=dis[k][fa[k+1][r]]+f2[k+1][r];
        else x=INF;
        if(f1[l][r]>x) f1[l][r]=x;
        if(x<INF)
        {
          V[k][l][r]=dis[k][fa[k+1][r]]+dis[k][fa[l][k-1]];
          V[k][l][r]+=V[fa[k+1][r]][k+1][r]+V[fa[l][k-1]][l][k-1];
          if(x+V[k][l][r]<f2[l][r])
		  {
            f2[l][r]=x+V[k][l][r];
            fa[l][r]=k;
		  }
        }
      }
      if(dis[l][fa[l+1][r]]<INF&&f2[l+1][r]<INF)
        x=dis[l][fa[l+1][r]]+f2[l+1][r];
      else x=INF;
      if(x<f1[l][r]) f1[l][r]=x;
      if(x<INF)
      {
        V[l][l][r]=dis[l][fa[l+1][r]]+V[fa[l+1][r]][l+1][r];
        if(x+V[l][l][r]<f2[l][r])
        {
          f2[l][r]=x+V[l][l][r];
          fa[l][r]=l;
        }
	  }
	  for(k=l+1;k<r;k++)
	  {
        if(dis[l][fa[l+1][k]]<INF&&f2[l+1][k]<INF)
          x=dis[l][fa[l+1][k]]+f2[l+1][k];
        else x=INF;
        if(x<INF&&dis[l][fa[k+1][r]]<INF&&f2[k+1][r]<INF)
          x+=dis[l][fa[k+1][r]]+f2[k+1][r];
        else x=INF;
        if(f1[l][r]>x) f1[l][r]=x;
        if(x<INF)
        {
          V[l][l][r]=dis[l][fa[l+1][k]]+dis[l][fa[k+1][r]];
          V[l][l][r]+=V[fa[l+1][k]][l+1][k]+V[fa[k+1][r]][k+1][r];
          if(x+V[l][l][r]<f2[l][r])
          {
            f2[l][r]=x+V[k][l][r];
            fa[l][r]=l;
		  }
        }
      }
	  if(dis[r][fa[l][r-1]]<INF&&f2[l][r-1]<INF)
        x=dis[r][fa[l][r-1]]+f2[l][r-1];
      else x=INF;
      if(x<f1[l][r]) f1[l][r]=x;
      if(x<INF)
      {
        V[l][l][r]=dis[l][fa[l][r-1]]+V[fa[l][r-1]][l][r-1];
        if(x+V[l][l][r]<f2[l][r])
		{
          f2[l][r]=x+V[l][l][r];
          fa[l][r]=r;
        }
      }
      for(k=l+1;k<r;k++)
	  {
        if(dis[r][fa[l][k-1]]<INF&&f2[l][k-1]<INF)
          x=dis[r][fa[l][k-1]]+f2[l][k-1];
        else x=INF;
        if(x<INF&&dis[r][fa[k][r-1]]<INF&&f2[k][r-1]<INF)
          x+=dis[r][fa[k][r-1]]+f2[k][r-1];
        else x=INF;
        if(f1[l][r]>x) f1[l][r]=x;
        if(x<INF)
        {
          V[r][l][r]=dis[r][fa[l][k-1]]+dis[r][fa[k][r-1]];
          V[r][l][r]+=V[fa[l][k-1]][l][k-1]+V[fa[k][r-1]][k][r-1];
          if(x+V[r][l][r]<f2[l][r])
          {
            f2[l][r]=x+V[r][l][r];
            fa[l][r]=r;
		  }
        }
      }
	}
  }
  printf("%d\n",f1[1][n]);
  return 0;
}

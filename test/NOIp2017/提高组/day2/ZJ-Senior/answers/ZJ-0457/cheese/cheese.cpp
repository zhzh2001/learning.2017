#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
int n,h,r,num;
bool F;
struct edge
{
  int to,nxt;
}e[1000000];
int x[1010],y[1010],z[1010];
int f[1010],Li[1010];
bool flag[1010];
bool judge(int a,int b)
{
  int X,Y,Z;
  X=x[a]-x[b];X=X*X;
  Y=y[a]-y[b];Y=Y*Y;
  Z=z[a]-z[b];Z=Z*Z;
  if(X+Y+Z<=r*r*4) return 1;
  else return 0;
}
void adde(int a,int b)
{
  e[++num].to=b;e[num].nxt=f[a];f[a]=num;
}
int main()
{
  freopen("cheese.in","r",stdin);
  freopen("cheese.out","w",stdout);
  int T,i,j,L,R;
  scanf("%d",&T);
  while(T--)
  {
  	scanf("%d%d%d",&n,&h,&r);
    num=0;L=1;R=0;F=0;
    for(i=1;i<=n;i++)
    {
      scanf("%d%d%d",&x[i],&y[i],&z[i]);
      flag[i]=0;f[i]=0;
	  for(j=i-1;j>0;j--)
      {
        if(judge(i,j))
        {
          adde(i,j);
          adde(j,i);
        }
      }
      if(z[i]<=r)
      {
        flag[i]=1;
        Li[++R]=i;
        if(z[i]+r>=h) F=1;
	  }
    }
    while(L<=R&&F==0)
    {
      for(i=f[Li[L]];i;i=e[i].nxt)
      {
        if(flag[e[i].to]==0)
        {
          if(z[e[i].to]+r>=h) F=1;
          Li[++R]=e[i].to;
          flag[e[i].to]=1;
        }
      }
      L++;
    }
    if(F) printf("Yes\n");
    else printf("No\n");
  }
  return 0;
}

#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;
struct node
{
	ll x,y,z;
}a[2000];
int b[2000],n,m,T;
bool d[1010],f[1010],flag;
ll h,r,R;
inline void read(int &x)
{
	char ch=getchar(); x=0; int y=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') y=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x*=y;
}
inline void read(ll &x)
{
	char ch=getchar(); x=0; int y=1;
	while ((ch<'0'||ch>'9')&&ch!='-') ch=getchar();
	if (ch=='-') y=-1,ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	x*=y;
}
ll abs(ll x)
{
	if (x<0) return -x;
	return x;
}
bool dis(int x,int y)
{
	ll tan=(a[x].x-a[y].x)*(a[x].x-a[y].x);
	if (tan>R) return 0;
	tan+=(a[x].y-a[y].y)*(a[x].y-a[y].y);
	if (tan>R) return 0;
	tan+=(a[x].z-a[y].z)*(a[x].z-a[y].z);
	if (tan>R) return 0;
	return 1;
}
void dfs(int x)
{
	if (flag) return;
	if (f[x])
	{
		flag=1; return;
	}
	d[x]=1;
	for (int i=1;i<=n;i++)
		if (!d[i])
			if (dis(x,i)) dfs(i);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while (T--)
	{
		flag=0; m=0;
		read(n); read(h); read(r); R=r*r*4;
		for (int i=1;i<=n;i++)
		{
			read(a[i].x); read(a[i].y); read(a[i].z);
			if (a[i].z>=h-r) f[i]=1; else f[i]=0;
			if (abs(a[i].z)<=r)
			{
				b[++m]=i;
				if (f[i]) flag=1;
			}
		}
		if (flag)
		{
			printf("Yes\n"); continue;
		}
		for (int i=1;i<=n;i++)
			if (f[i])
			{
				flag=1; break;
			}
		if (!flag)
		{
			printf("No\n"); continue;
		}
		flag=0;
		for (int i=1;i<=n;i++) d[i]=0;
		for (int i=1;i<=m;i++)
			if (!d[b[i]]) dfs(b[i]);
		if (flag) printf("Yes\n"); else printf("No\n");
	}
	return 0;
}

#include<cstdio>
#include<iostream>
#define ll long long
using namespace std;
int n,m,vv,x,y,z,num[20][20],a[20][20],tot[20],d[20];
bool f[20][20],b[20];
ll minn,s[20][20];
inline void read(int &x)
{
	char ch=getchar(); x=0;
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n); read(m);
	int vv=0;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) num[i][j]=n+1;
	for (int i=1;i<=m;i++)
	{
		read(x); read(y); read(z);
		num[x][y]=num[y][x]=1;
		if (f[x][y]) a[x][y]=a[y][x]=min(a[x][y],z);
			else
			{
				f[x][y]=f[y][x]=1; a[x][y]=a[y][x]=z;
			}
		if (i==1) vv=z;
			else if (vv!=z) vv=0;
	}
	if (vv!=0)
	{
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (i!=j)
					for (int k=1;k<=n;k++)
						if (i!=k&&j!=k) num[i][j]=num[j][i]=min(num[i][k]+num[k][j],num[i][j]);
		minn=1e9;
		for (int i=1;i<=n;i++)
		{
			ll ans=0;
			for (int j=1;j<=n;j++)
				if (i!=j) ans+=(ll)num[i][j]*(ll)z;
			minn=min(minn,ans);
		}
		printf("%lld\n",minn);
		return 0;
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++) s[i][j]=1e9;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				if (i!=j)
					for (int k=1;k<=n;k++)
						if (i!=k&&j!=k&&num[k][j]<n+1&&num[i][k]<n+1&&f[k][j])
							if ((ll)(num[i][k]+num[k][j])*a[k][j]<s[i][j])
							{
								s[i][j]=s[j][i]=(ll)(num[i][k]+num[k][j])*a[k][j]; d[j]=k;
								num[i][j]=num[j][i]=num[i][k]+num[k][j];
							}
		minn=1e9;
		for (int i=1;i<=n;i++)
		{
			ll ans=0;
			for (int j=1;j<=n;j++)
				if (i!=j) ans+=s[i][j];
			minn=min(minn,ans);
		}
		printf("%lld\n",minn);
	return 0;
}

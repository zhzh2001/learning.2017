#include<bits/stdc++.h>
using namespace std;
const int N=13;
//int vis[N*2];
int v[N],a[N][N],q[N],n,m,ans,x,y,z,la;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline void bfs(int x)
{
	memset(v,0,sizeof(v));
	v[x]=1;
	q[1]=x;
	int l=0,r=1,sum=0;
	while (l!=r)
	{
		int now=q[++l];
		for (int i=1;i<=n;++i)
			if (!v[i]&&a[now][i]<=500000)
			{
				sum+=v[now]*a[now][i];
				v[i]=v[now]+1;
				q[++r]=i;
			}
	}
	ans=min(ans,sum);
}
void dfs(int x,int h,int sum)
{
	if (sum>=ans)
		return;
	int b=1;
	for (int i=1;i<=n;++i)
		if (!v[i])
		{
			b=0;
			break;
		}
	if (b)
		ans=sum;
	int dis[N],w[N];
	w[0]=0;
	memset(dis,0x3f,sizeof(dis));
	for (int i=1;i<=n;++i)
		if ((1<<(i-1))&x)
		{
			for (int j=1;j<=n;++j)
				dis[j]=min(dis[j],a[i][j]);
		}
	for (int i=1;i<=n;++i)
		if (!v[i]&&dis[i]<=500000)
			w[++w[0]]=i;
	for (int i=(1<<w[0])-1;i>=1;--i)
	{
		int ss=0,kkk=0;
		for (int j=1;j<=w[0];++j)
			if (i&(1<<(j-1)))
			{
				ss+=dis[w[j]];
				kkk|=(1<<(w[j]-1));
				v[w[j]]=1;
			}
		dfs(kkk,h+1,sum+ss*h);
		//if (ans==419&&!vis[h])
			//cout<<h<<' '<<i<<'\n',vis[h]=vis[h+1]=vis[h+2]=vis[h+3]=1;
		for (int j=1;j<=w[0];++j)
			if (i&(1<<(j-1)))
				v[w[j]]=0;
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);
	read(m);
	memset(a,0x3f,sizeof(a));
	int b=1;
	for (int i=1;i<=m;++i)
	{
		read(x);
		read(y);
		read(z);
		if (z!=la&&i!=1)
			b=0;
		la=z;
		if (a[x][y]>z)
			a[x][y]=a[y][x]=z;
	}
	ans=(int)1e9+7;
	if (b)
	{
		for (int i=1;i<=n;++i)
			bfs(i);
	}
	else
		for (int i=1;i<=n;++i)
		{
			v[i]=1;
			dfs(1<<(i-1),1,0);
			v[i]=0;
		}
	cout<<ans;
}
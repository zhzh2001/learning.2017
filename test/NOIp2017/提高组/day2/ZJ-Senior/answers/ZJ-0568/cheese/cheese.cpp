#include<bits/stdc++.h>
using namespace std;
#define ll long long
const int N=1005;
int n,m,h,in[N],q[N],l,r,t;
bool f[N][N];
ll d;
inline void read(int &x)
{
	char c=getchar();
	x=0;
	int y=1;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			y=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x*=y;
}
inline ll sqr(int x)
{
	return (ll)x*x;
}
struct data
{
	int x,y,z;
	ll operator+(const data &a)
	{
		return sqr(x-a.x)+sqr(y-a.y)+sqr(z-a.z);
	}
}a[N];
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(t);
	while (t--)
	{
		read(n);
		read(m);
		read(h);
		for (int i=1;i<=n;++i)
		{
			read(a[i].x);
			read(a[i].y);
			read(a[i].z);
			f[0][i]=a[i].z<=h;
			f[i][n+1]=a[i].z+h>=m;
		}		
		d=4LL*h*h;
		for (int i=1;i<=n;++i)
			for (int j=i+1;j<=n;++j)
				f[i][j]=f[j][i]=a[i]+a[j]<=d;
		q[1]=0;
		l=0;
		r=1;
		memset(in,0,sizeof(in));
		in[0]=1;
		while (l!=r&&!in[n+1])
		{
			++l;
			int now=q[l];
			for (int i=1;i<=n+1;++i)
				if (f[now][i]&&!in[i])
				{
					in[i]=1;
					q[++r]=i;
				}
		}
		puts(in[n+1]?"Yes":"No");
	}
	return 0;
}

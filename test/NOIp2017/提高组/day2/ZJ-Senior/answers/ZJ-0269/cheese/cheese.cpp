#include<cstdio>
#include<iostream>
#include<cstring>
#define ll long long
using namespace std;
ll T,i,x[1010],y[1010],z[1010],n,h,r,f[1010],head,tail,u;
bool b[1010],bo;
ll abs(ll x)
{
	return x>0?x:-x;
}
ll dis(ll x1,ll y1,ll z1,ll x2,ll y2,ll z2)
{
	return(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)+(z1-z2)*(z1-z2);
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%lld",&T);
	while(T--)
	{
		scanf("%lld%lld%lld",&n,&h,&r);
		for(i=1;i<=n;i++)
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
		bo=0;
		memset(b,0,sizeof(b));
		head=0,tail=0;
		for(i=1;i<=n;i++)
			if(abs(z[i])<=r)
			{
				f[++tail]=i;
				b[i]=1;
			}
		while(head<tail)
		{
			u=f[++head];
			if(abs(z[u]-h)<=r)
			{
				bo=1;
				break;
			}
			for(i=1;i<=n;i++)
				if(dis(x[i],y[i],z[i],x[u],y[u],z[u])<=r*r*4&&!b[i])
				{
					b[i]=1;
					f[++tail]=i;
				}
		}
		if(bo)printf("Yes\n");else printf("No\n");
	}
}

#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int n,m,x,y,z,i,a[13][13],ans=1e9,sum,c[13],d[13];
bool b[13];
void search(int t)
{
	int i,j,k,tmp;
	if(t==n)
	{
		ans=min(ans,sum);
		return;
	}
	for(i=1;i<=n;i++)
		if(!b[i])
		{
			b[i]=1;
			d[t+1]=i;
			tmp=1e9;
			for(j=1;j<=t;j++)
				if(a[d[j]][i]<1e9&&c[d[j]]*a[d[j]][i]<tmp)
				{
					tmp=c[d[j]]*a[d[j]][i];
					k=d[j];
				}
			c[i]=c[k]+1;
			sum+=tmp;
			search(t+1);
			b[i]=0;
			sum-=tmp;
		}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(a,0x3f,sizeof(a));
	while(m--)
	{
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=a[y][x]=min(a[x][y],z);
	}
	for(i=1;i<=n;i++)
	{
		c[i]=b[i]=1;
		d[1]=i;
		search(1);
		c[i]=b[i]=0;
	}
	printf("%d\n",ans);
}

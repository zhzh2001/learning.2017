#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define N 1005
#define M 1005
#define maxn 300005
using namespace std;
int n,m,q;
int mp[N][M];
int mark1[4*maxn],mark2[4*maxn],tr1[4*maxn],tr2[4*maxn];
void pushdown1(int v,int mid,int l,int r){
	if (mark1[v]){
		mark1[v<<1]+=mark1[v];
		mark1[v<<1|1]+=mark1[v];
		tr1[v<<1]+=(mid-l+1)*mark1[v];
		tr1[v<<1|1]+=(r-mid)*mark1[v];
		mark1[v]=0;
	}
}
void update1(int v){
	tr1[v]=tr1[v<<1]+tr1[v<<1|1];
}
void update2(int v){
	tr2[v]=tr2[v<<1]+tr2[v<<1|1];
}
void pushdown2(int v,int mid,int l,int r){
	if (mark2[v]){
		mark2[v<<1]+=mark2[v];
		mark2[v<<1|1]+=mark2[v];
		tr2[v<<1]+=(mid-l+1)*mark2[v];
		tr2[v<<1|1]+=(r-mid)*mark2[v];
		mark2[v]=0;
	}
}
ll q1(int v,int l,int r,int p){
	if (l==r) return tr1[v];
	int mid=(l+r)>>1;
	pushdown1(v,mid,l,r);
	if (p<=mid) return q1(v<<1,l,mid,p);
			else return q1(v<<1|1,mid+1,r,p);
}
ll q2(int v,int l,int r,int p){
	if (l==r) return tr2[v];
	int mid=(l+r)>>1;
	pushdown2(v,mid,l,r);
	if (p<=mid) return q2(v<<1,l,mid,p);
			else return q2(v<<1|1,mid+1,r,p);
}
void change1(int v,int l,int r,int x,int y){
	if (x<=l&&y>=r){
		mark1[v]++;
		tr1[v]+=r-l+1;
		return;
	}
	int mid=(l+r)>>1;
	pushdown1(v,mid,l,r);
	if (y<=mid) change1(v<<1,l,mid,x,y); else
	if (x>mid) change1(v<<1|1,mid+1,r,x,y);
	else{
		change1(v<<1,l,mid,x,mid);
		change1(v<<1|1,mid+1,r,mid+1,y);
	}
	update1(v);
}
void change2(int v,int l,int r,int x,int y){
	if (x<=l&&y>=r){
		mark2[v]++;
		tr2[v]+=r-l+1;
		return;
	}
	int mid=(l+r)>>1;
	pushdown2(v,mid,l,r);
	if (y<=mid) change2(v<<1,l,mid,x,y); else
	if (x>mid) change2(v<<1|1,mid+1,r,x,y);
	else{
		change2(v<<1,l,mid,x,mid);
		change2(v<<1|1,mid+1,r,mid+1,y);
	}
	update2(v);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if (n<=1000&&m<=1000&&q<=500){
		int x,y;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				mp[i][j]=(i-1)*m+j;
		for (int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			printf("%d\n",mp[x][y]);
			int t=mp[x][y];
			for (int j=y;j<=m-1;j++)
				mp[x][j]=mp[x][j+1];
			for (int j=x;j<=n-1;j++)
				mp[j][m]=mp[j+1][m];
			mp[n][m]=t;
		}
	} else
	if (q>500){
		ll x,y;
		for (int i=1;i<=q;i++){
			scanf("%lld%lld",&x,&y);
			ll a=q2(1,1,n,x),b=q1(1,1,m,y);
			printf("%lld\n",(x-1+a)*m+y+b);
			change1(1,1,m,y,m-1);
			change2(1,1,n,x,n);
		}
	}
	return 0;
}

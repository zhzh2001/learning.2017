#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define N 1005
#define ll long long
using namespace std;
int cas,n,tot,cnt;
ll h,r;
ll x[N],y[N],z[N];
int head[N],from[N],flag[N];
bool to[N];
double dist[N][N];
double calc(int a,int b){
	return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b])+(z[a]-z[b])*(z[a]-z[b]));
}
struct node{
	int next,to;
}e[2*N*N];
void add(int x,int y){
	e[++tot].next=head[x];
	head[x]=tot;
	e[tot].to=y;
}
bool dfs(int u,int t){
	flag[u]=t;
	if (to[u]) return true;
	for (int i=head[u];i;i=e[i].next){
		int v=e[i].to;
		if (flag[v]) continue;
		if (dfs(v,t)) return true;
	}
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&cas);
	while (cas--){
		tot=cnt=0;
		memset(to,0,sizeof(to));
		memset(head,0,sizeof(head));
		memset(flag,0,sizeof(flag));
		scanf("%d%lld%lld",&n,&h,&r);
		for (int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&x[i],&y[i],&z[i]);
			if (z[i]<=r) from[++cnt]=i;
			if (z[i]+r>=h) to[i]=true;
		}
		for (int i=1;i<=n;i++)
			for (int j=i+1;j<=n;j++){
				dist[i][j]=dist[j][i]=calc(i,j);
				if (dist[i][j]<=r*2) add(i,j),add(j,i);
			}
		bool Flag=false;
		for (int i=1;i<=cnt;i++)
			if (dfs(from[i],i)){
				Flag=true;
				break;
			}
		if (Flag) puts("Yes");
			else puts("No");
	}
	return 0;
}

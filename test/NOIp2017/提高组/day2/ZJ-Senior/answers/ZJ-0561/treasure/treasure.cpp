#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define N 15
using namespace std;
int n,m,x,y,z;
int p[N];
int dist[N][N];
int dp[N][4500];
int num[N][4500][N];
bool flag[N][N];
int lowbit(int x){ return x&(-x); }
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (!flag[x][y]||z<dist[x][y]) dist[x][y]=dist[y][x]=z;
		flag[x][y]=flag[y][x]=true;
	}
	memset(dp,127,sizeof(dp));
	for (int i=1;i<=n;i++){
		dp[i][1<<(i-1)]=0; num[i][1<<(i-1)][i]=1;
		for (int sta=1<<(i-1)+1;sta<=(1<<n)-1;sta++)
			if (sta&(1<<(i-1))){
				num[i][sta][i]=1;
				int j=sta,x=0,last=sta;
				while (j){
					x++; p[x]=1;
					if (j&1){
						for (int k=1;k<=n;k++)
							if (flag[k][x]&&((sta-(1<<(x-1)))&(1<<(k-1)))){
								if (dp[i][sta]>dp[i][sta-(1<<(x-1))]+dist[k][x]*num[i][sta-(1<<(x-1))][k]){		
									dp[i][sta]=dp[i][sta-(1<<(x-1))]+dist[k][x]*num[i][sta-(1<<(x-1))][k];
									num[i][sta][x]=num[i][sta-(1<<(x-1))][k]+1;
									last=sta-(1<<(x-1));
								}
							}
					}
					j>>=1;
				}
				for (int k=1;k<=x;k++)
					if (p[k]&&!num[i][sta][k])
						num[i][sta][k]=num[i][last][k];
			}
	}
	int ans=1e9;
	for (int i=1;i<=n;i++)
		ans=min(ans,dp[i][(1<<n)-1]);
	printf("%d\n",ans);
	return 0;
}

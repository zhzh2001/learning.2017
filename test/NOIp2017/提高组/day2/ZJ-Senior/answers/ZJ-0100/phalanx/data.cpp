#include <bits/stdc++.h>
#define finput(x) freopen(x,"r",stdin)
#define foutput(x) freopen(x,"w",stdout)
#define rnd (rand()*rand()+rand())
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
#define MEM(x,v) memset(x,v,sizeof(x))
#define SPY(x,y) memcpy(x,y,sizeof(x))
#define mp make_pair
#define pb push_back
#define fi first
#define se second
using namespace std;
template <class T>inline void Rd(T &p){
	p=0; char c; short f=1;
	while(c=getchar(),c<48&&c!='-');
	do if(c!='-')p=p*10+(c&15); else f=-f;
	while(c=getchar(),c>47);
	p*=f;
}
template <class T>inline void pfn(T p,bool f=true){
	if(p<0)putchar('-'),p=-p;
	if(!p)putchar('0');
	static int num[100],tot;
	while(p)num[++tot]=p%10,p/=10;
	while(tot)putchar(num[tot--]^'0');
	putchar(f?'\n':' ');
}
template <class T>inline void Max(T &a,T b){if(a<b)a=b;}
template <class T>inline void Min(T &a,T b){if(b<a)a=b;}
template <class T>inline void Chk(T &a,T b){if(~b&&(!~a||b<a))a=b;}
typedef long long ll;
typedef pair<int,int> pii;
typedef double db;
const int M=(int)1e5+5,N=(int)1e5+5,P=(int)1e9+7,inf=0x3f3f3f3f;
const double eps=1e-9;
const long long infty=1e18;

int main(){
	foutput("phalanx.in");
	srand(time(NULL));
	int n=3e5,m=3e5,q=3e5;
	printf("%d %d %d\n",n,m,q);
	for(int i=1;i<=q;++i)
		printf("1 %d\n",rnd%m+1);
}

/*
:loop
data.exe
test.exe
.exe
fc test.out .out
if %errorlevel% ==0 goto loop
pause
*/


#include <bits/stdc++.h>
#define finput(x) freopen(x,"r",stdin)
#define foutput(x) freopen(x,"w",stdout)
#define rnd (rand()*rand()+rand())
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
#define MEM(x,v) memset(x,v,sizeof(x))
#define SPY(x,y) memcpy(x,y,sizeof(x))
#define mp make_pair
#define pb push_back
#define fi first
#define se second
using namespace std;
template <class T>inline void Rd(T &p){
	p=0; char c; short f=1;
	while(c=getchar(),c<48&&c!='-');
	do if(c!='-')p=p*10+(c&15); else f=-f;
	while(c=getchar(),c>47);
	p*=f;
}
template <class T>inline void pfn(T p,bool f=true){
	if(p<0)putchar('-'),p=-p;
	if(!p)putchar('0');
	static int num[100],tot;
	while(p)num[++tot]=p%10,p/=10;
	while(tot)putchar(num[tot--]^'0');
	putchar(f?'\n':' ');
}
template <class T>inline void Max(T &a,T b){if(a<b)a=b;}
template <class T>inline void Min(T &a,T b){if(b<a)a=b;}
template <class T>inline void Chk(T &a,T b){if(~b&&(!~a||b<a))a=b;}
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<int,ll> pil;
typedef double db;
const int M=(int)3e5+5,N=(int)505,P=(int)1e9+7,inf=0x3f3f3f3f;
const double eps=1e-9;
const long long infty=1e18;

int n,m,q;
struct query{int x,y;}Q[M];
struct part1{
	static const int Maxn=(int)5e4+5;
	int res[M],rtot,rec[N];
	ll arr[N][Maxn],col[Maxn]; //5000w
	void solve(){
		rtot=0;
		for(int i=1;i<=q;++i)res[++rtot]=Q[i].x;
		sort(res+1,res+rtot+1);
		rtot=unique(res+1,res+rtot+1)-(res+1);
		for(int i=1;i<=rtot;++i){
			rec[res[i]]=i;
			for(int j=1;j<m;++j)
				arr[i][j]=1LL*(res[i]-1)*m+j;
		}
		for(int i=1;i<=n;++i)col[i]=(ll)i*m;
		for(int i=1;i<=q;++i){
			int x=Q[i].x,y=Q[i].y,w;
			if(y!=m){
				pfn(w=arr[rec[x]][y]);
				for(int j=y+1;j<m;++j)
					arr[rec[x]][j-1]=arr[rec[x]][j];
				arr[rec[x]][m-1]=col[rec[x]];
			}else pfn(w=col[rec[x]]);
			for(int k=rec[x]+1;k<=n;++k)col[k-1]=col[k];
			col[n]=w;
		}
	}
}p1;
int Mx;
bool check(){
	for(int i=1;i<=q;++i)
		if(Q[i].x!=1)return false;
	return true;
}
struct part2{
	static const int Maxn=(int)6e5+5,MlogM;
	int row[Maxn],col[Maxn];
	struct Fenwick_Tree{
		int bit[Maxn+5];
		void clear(){MEM(bit,0);}
		void add(int p,int w){
			while(p<=Mx){bit[p]+=w;p+=p&-p;}
		}
		int sum(int p){
			int tmp=0;
			while(p){tmp+=bit[p];p-=p&-p;}
			return tmp;
		}
	}tr;
	
	void solve(){
		for(int i=1;i<m;++i)row[i]=i;
		for(int i=1;i<=n;++i)col[i]=1LL*i*m;
		Mx=m-1+q;
		tr.clear();
		for(int i=1;i<=Mx;++i)tr.add(i,1);
		for(int i=1;i<=q;++i){
			int x=Q[i].x,y=Q[i].y;
			int l=y,r=(m-1)+i-1,mid,pos;
			while(l<=r){
				if(tr.sum(mid=l+r>>1)>=y)r=(pos=mid)-1;
				else l=mid+1;
			}
			pfn(row[pos]),tr.add(pos,-1);
			row[m-1+i]=col[i],col[n+i]=row[pos];
		}
	}
}p2;
int main(){
	finput("phalanx.in");
	foutput("phalanx.out");
	Rd(n),Rd(m),Rd(q);
	for(int i=1;i<=q;++i)Rd(Q[i].x),Rd(Q[i].y);
	if(false)cerr<<"Spylft"<<endl;
	else if(check())p2.solve(),exit(0);
	else p1.solve(),exit(0);
}

/*
:loop
data.exe
test.exe
.exe
fc test.out .out
if %errorlevel% ==0 goto loop
pause
*/

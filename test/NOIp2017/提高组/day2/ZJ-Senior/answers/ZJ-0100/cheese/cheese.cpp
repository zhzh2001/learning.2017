#include <bits/stdc++.h>
#define finput(x) freopen(x,"r",stdin)
#define foutput(x) freopen(x,"w",stdout)
#define rnd (rand()*rand()+rand())
#define bug(x) cerr<<#x<<"="<<x<<" "
#define debug(x) cerr<<#x<<"="<<x<<"\n"
#define MEM(x,v) memset(x,v,sizeof(x))
#define SPY(x,y) memcpy(x,y,sizeof(x))
#define mp make_pair
#define pb push_back
#define fi first
#define se second
using namespace std;
template <class T>inline void Rd(T &p){
	p=0; char c; short f=1;
	while(c=getchar(),c<48&&c!='-');
	do if(c!='-')p=p*10+(c&15); else f=-f;
	while(c=getchar(),c>47);
	p*=f;
}
template <class T>inline void pfn(T p,bool f=true){
	if(p<0)putchar('-'),p=-p;
	if(!p)putchar('0');
	static int num[100],tot;
	while(p)num[++tot]=p%10,p/=10;
	while(tot)putchar(num[tot--]^'0');
	putchar(f?'\n':' ');
}
template <class T>inline void Max(T &a,T b){if(a<b)a=b;}
template <class T>inline void Min(T &a,T b){if(b<a)a=b;}
template <class T>inline void Chk(T &a,T b){if(~b&&(!~a||b<a))a=b;}
typedef long long ll;
typedef pair<int,int> pii;
typedef double db;
const int M=(int)1e3+5,N=(int)1e5+5,P=(int)1e9+7,inf=0x3f3f3f3f;
const double eps=1e-9;
const long long infty=1e18;

int kase,n,hi,rad;
bool tof[M],toc[M],unvalid[M];
struct Point{
	ll x,y,z;
	Point(ll a=0,ll b=0,ll c=0){x=a,y=b,z=c;}
}s[M];
bool vis[M];
int q[M];
bool check(int i,int j){
	double dis=(s[i].x-s[j].x)*(s[i].x-s[j].x);
	dis+=(s[i].y-s[j].y)*(s[i].y-s[j].y);
	dis+=(s[i].z-s[j].z)*(s[i].z-s[j].z);
	return sqrt(dis)<=2.0*rad;
}
bool Topo(){
	MEM(vis,false);
	int l=0,r=-1;
	for(int i=1;i<=n;++i)
		if(tof[i]&&!unvalid[i])vis[q[++r]=i]=true;
	while(l<=r){
		int u=q[l++];
		for(int i=1;i<=n;++i)
			if(!vis[i]&&!unvalid[i]&&check(u,i)){
				vis[q[++r]=i]=true;
				if(toc[i])return true;
			}
	}return false;
}
int main(){
	finput("cheese.in");
	foutput("cheese.out");
	Rd(kase);
	while(kase--){
		MEM(tof,false);
		MEM(toc,false);
		MEM(unvalid,false);
		Rd(n),Rd(hi),Rd(rad);
		for(int i=1,x,y,z;i<=n;++i){
			Rd(x),Rd(y),Rd(z),s[i]=Point(x,y,z);
			if(z-0<=rad)tof[i]=true;
			if(hi-z<=rad)toc[i]=true;
			if(z+rad<0||z-rad>hi)unvalid[i]=true;
		}
		puts(Topo()?"Yes":"No");
	}
}

/*
:loop
data.exe
test.exe
.exe
fc test.out .out
if %errorlevel% ==0 goto loop
pause
*/

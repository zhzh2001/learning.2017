#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>

#define rep(i,a,b)	for(int i=a;i<=b;i++)
#define rev(i,a,b)	for(int i=a;i>=b;i++)
#define reg(i,g,u)	for(int i=h[u];i;i=g[i].v)
#define sqr(x)	((x)*(x))

#define nc getchar
using namespace std;

typedef long long LL;

inline void read(int &data){
	char ch=nc();int e=1;data=0;
	for(;ch!='-'&&(ch<'0'&&ch>'9');ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}

inline void read(LL &data){
	char ch=nc();int e=1;data=0;
	for(;ch!='-'&&(ch<'0'&&ch>'9');ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}

const int maxn=1e3+5;
int n,p[maxn];
LL h,R,x[maxn],y[maxn],z[maxn];
bool v[maxn];
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;read(T);while (T--){
		read(n),read(h),read(R);
		rep(i,1,n)
			read(x[i]),read(y[i]),read(z[i]);
		memset(v,0,sizeof(v));
		int l=1,r=0;
		rep(i,1,n)
			if (R>=z[i])	p[++r]=i,v[i]=true;
		while (l<=r){
			int k=p[l];
			rep(i,1,n){
				if (v[i])	continue;
				if ((unsigned long long)(sqr(x[i]-x[k])+sqr(y[i]-y[k])+sqr(z[i]-z[k]))<=sqr(R*2))
					v[i]=true,p[++r]=i;
			}
			++l;
		}
		int t=false;
		rep(i,1,n)
			if (v[i]&&abs(z[i]-h)<=R){
				t=true; break;
			}
		if (t)	puts("Yes");
		else puts("No");
	}	
	return 0;
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>

#define rep(i,a,b)	for(int i=a;i<=b;i++)
#define rev(i,a,b)	for(int i=a;i>=b;i++)
#define reg(i,g,u)	for(int i=h[u];i;i=g[i].v)
#define Inf (1<<30)
#define nc getchar
using namespace std;

typedef long long LL;

inline void read(int &data){
	char ch=nc();int e=1;data=0;
	for(;ch!='-'&&(ch<'0'&&ch>'9');ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}
	
inline void write(int data){
	if (data<0)	putchar('-'),data=-data;
	if (data>9)	write(data/10);putchar(data%10+48);
}

int n,m,q;
int b[1005][1005],w[505][50005],l[50005],h[505],v[50005];
int x[300005],y[300005];

int val[10000005],ret[1000005];

void modify(int l,int r,int node,int R,int p){
	val[node]+=p;
	if (l==r)	return;
	int mid=(l+r)>>1;
	if (R<=mid)	modify(l,mid,node<<1,R,p);
	else modify(mid+1,r,node<<1|1,R,p);
}

int query(int l,int r,int node,int k){
	if (l==r)	return l;
	int mid=(l+r)>>1;
	if (k>val[node<<1])	return query(mid+1,r,node<<1|1,k-val[node<<1]);
	else return query(l,mid,node<<1,k);
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n),read(m),read(q);
	if (n<=1000&&m<=1000){
		int x,y;
		rep(i,1,n)	rep(j,1,m)
			b[i][j]=(i-1)*m+j;
		rep(i,1,q){
			read(x),read(y);
			int t=b[x][y];	write(t),puts("");
			rep(j,y,m-1)	b[x][j]=b[x][j+1];
			rep(j,x,n-1)	b[j][m]=b[j+1][m];
			b[n][m]=t;
		}
		return 0;
	}
	if (q<=500){
		memset(v,0,sizeof(v));
		rep(i,1,q)
			read(x[i]),read(y[i]),v[x[i]]=i;
		int r=0;
		rep(i,1,n)
			if (v[i])	h[++r]=i,v[i]=r;	
		rep(i,1,r)
			rep(j,1,m)	w[i][j]=(h[i]-1)*m+j;
		rep(i,1,n)	l[i]=i*m;
		rep(i,1,q){
			int t=w[v[x[i]]][y[i]];write(t),puts("");
			rep(j,y[i],m-1)	w[v[x[i]]][j]=w[v[x[i]]][j+1];
			rep(j,x[i],n-1)	l[j]=l[j+1];l[n]=t;
			rep(j,1,r)	w[j][m]=l[h[j]];
		}
		return 0;
	}	
	bool t=true;
	rep(i,1,q)	read(x[i]),read(y[i]),t=t&&(x[i]==1);	
	if (t){
		int N=n+m+q-1;
		rep(i,1,m-1)
			ret[i]=i,modify(1,N,1,i,1);
		rep(i,1,n)
			ret[i+m-1]=i*m,modify(1,N,1,i+m-1,1);
		rep(i,1,n){
			int retttt=x[i]+y[i]-1,ans;
			ans=query(1,N,1,retttt);
			write(ret[ans]),puts("");	ret[n+m+i-1]=ret[ans];
			modify(1,N,1,ans,-1),modify(1,N,1,n+m+i-1,1);						
		}
		return 0;
	}
}

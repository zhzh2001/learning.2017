#include<cstdio>
#include<iostream>
#include<cstring>
#include<cmath>
#include<algorithm>

#define rep(i,a,b)	for(int i=a;i<=b;i++)
#define rev(i,a,b)	for(int i=a;i>=b;i++)
#define reg(i,g,u)	for(int i=h[u];i;i=g[i].v)
#define Inf (1<<30)

#define nc getchar
using namespace std;

typedef long long LL;

inline void read(int &data){
	char ch=nc();int e=1;data=0;
	for(;ch!='-'&&(ch<'0'&&ch>'9');ch=nc());
	if (ch=='-')	e=-1,ch=nc();
	for(;ch>='0'&&ch<='9';data=data*10+ch-48,ch=nc());data*=e;
}

inline void write(int data){
	if (data<0)	putchar('-'),data=-data;
	if (data>9)	write(data/10);putchar(data%10+48);
}
const int maxn=13,maxm=1e3+5,maxp=1<<12;
struct str1{
	int x,y,z;
}a[maxm];

int n,m;
int w[maxn],f[maxn][maxn],dp[maxp],g[maxn][maxp],pre[maxp];

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n),read(m);
	rep(i,1,n)	rep(j,1,n)	f[i][j]=Inf;	
	rep(i,1,m){
		int x,y,z;read(x),read(y),read(z);
		f[x][y]=f[y][x]=min(f[x][y],z);
	}
	rep(k,1,n)	w[k]=1<<k-1;
	int p=(1<<n)-1;
	rep(i,0,p)	dp[i]=Inf;
	rep(i,1,n)	dp[w[i]]=0;
	rep(i,1,n)	rep(j,0,p)	if ((j&w[i])==0){
		g[i][j]=Inf;
		rep(k,1,n)	if (w[k]&j)
			g[i][j]=min(g[i][j],f[i][k]);
	}
	rep(i,1,p)	pre[i]=dp[i];
	int res=0;
	rep(T,1,n-1){
		rep(i,1,p)	rep(j,1,p)
			if ((i&j)==0){
				res=0;
				rep(k,1,n)	if (w[k]&j)
					if (g[k][i]==Inf)	{res=-1;break;}
					else res+=T*g[k][i];
				if (res<0)	continue;
				dp[i|j]=min(dp[i|j],pre[i]+res);
			}
		rep(i,1,p)	pre[i]=dp[i];
	}
	write(dp[p]),puts("");return 0;
}

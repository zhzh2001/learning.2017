#include<cstdio>
#include<cstdlib>
using namespace std;
inline int read(){
	char c=getchar();int ret=0;
	while((c<'0')||(c>'9'))c=getchar();
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret;
}
int n,m,q,cnt;
#define mid ((l+r)>>1)
struct stree{
	int cnt;
	int head[300005];
	struct node{
		int l,r,sum;
	}t[6000005];
	inline void add(int &rt,int k,int l,int r){
		if(rt==0)rt=++cnt;
		t[rt].sum++;
		if(l==r)return;
		if(k<=mid)add(t[rt].l,k,l,mid);
		else add(t[rt].r,k,mid+1,r);
	}
	inline int query(int rt,int k,int l,int r){
		if(l==r)return l;
		if(mid-l+1-t[t[rt].l].sum>=k)return query(t[rt].l,k,l,mid);
		else return query(t[rt].r,k-(mid-l+1)+t[t[rt].l].sum,mid+1,r);
	}
}T;
struct treap{
	int cnt,root;
	struct node{
		int w,x,size,l,r;
		long long num;
	}t[300005];
	inline int merge(int u,int v){
		if(u==0)return v;
		if(v==0)return u;
		if(t[u].w<t[v].w){
			t[v].size+=t[u].size;
			t[v].l=merge(u,t[v].l);
			return v;
		}else{
			t[u].size+=t[v].size;
			t[u].r=merge(t[u].r,v);
			return u;
		}
	}
	void split(int &x,int &y,int rt,int k){
		if(k==0){x=0;y=rt;return;}
		if(k==t[rt].size){y=0;x=rt;return;}
		if(t[t[rt].l].size>=k){
			y=rt;
			split(x,t[y].l,t[rt].l,k);
			t[y].size-=t[x].size;
		}else{
			x=rt;
			split(t[x].r,y,t[rt].r,k-t[t[rt].l].size-1);
			t[x].size-=t[y].size;
		}
	}
	inline int query(int rt,int k){
		if(rt==0)return 0;
		if(t[rt].x<=k)return t[t[rt].l].size+1+query(t[rt].r,k);
		else return query(t[rt].l,k);
	}
	void ins(int k,long long num){
		t[++cnt].w=(1LL*rand()*32768)|rand();
		t[cnt].size=1;t[cnt].num=num;t[cnt].x=k;
		int tmp=query(root,k),x,y;
		split(x,y,root,tmp);
		root=merge(x,merge(cnt,y));
	}
	inline long long find(int k){
		int now=root;
		while(true){
			if(t[t[now].l].size+1==k)return t[now].num;
			if(t[t[now].l].size+1<k)k-=t[t[now].l].size+1,now=t[now].r;
			else now=t[now].l;
		}
	}
	void print(int rt){
		if(t[rt].l)print(t[rt].l);
		printf("%d %lld\n",t[rt].x,t[rt].num);
		if(t[rt].r)print(t[rt].r);
	}
}Tr;
#define lson index<<1
#define rson index<<1|1
long long num[600005];
struct stree2{
	int cnt;
	int sum[2400005];
	inline void add(int k,int l,int r,int index){
		sum[index]++;
		if(l==r)return;
		if(k<=mid)add(k,l,mid,lson);
		else add(k,mid+1,r,rson);
	}
	inline int query(int k,int l,int r,int index){
		if(l==r)return l;
		if(mid-l+1-sum[lson]>=k)return query(k,l,mid,lson);
		else return query(k-(mid-l+1)+sum[lson],mid+1,r,rson);
	}	
}Tt;
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	srand(84621);
	n=read();m=read();q=read();
	for(int i=1;i<=n;i++)num[i]=1LL*i*m;
	for(int i=1;i<=q;i++){
		int x=read(),y=read();long long ans;
		if(y==m){
			int X=Tt.query(x,1,n+q,1);
			num[i+n]=ans=num[X];
			printf("%lld\n",ans);
			Tt.add(X,1,n+q,1);
			continue;
		}
		int t=T.query(T.head[x],y,1,m+q);
		if(t<m)ans=1LL*(x-1)*m+t; 
		else ans=Tr.find(Tr.query(Tr.root,x-1)+t-m+1);
		printf("%lld\n",ans);
		int X=Tt.query(x,1,n+q,1);num[i+n]=ans;
		Tr.ins(x,num[X]);
		Tt.add(X,1,n+q,1);
		T.add(T.head[x],t,1,m+q);
	}
}

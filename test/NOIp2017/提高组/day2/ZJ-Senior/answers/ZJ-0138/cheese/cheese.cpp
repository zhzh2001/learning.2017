#include<cstdio>
using namespace std;
inline int read(){
	char c=getchar();int ret=0,p=1;
	while((c<'0')||(c>'9')){
		if(c=='-')p=-1;
		c=getchar();
	}
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret*p;
}
int n,h,r,test;
long long R;
int x[1005],y[1005],z[1005];
bool b[1005][1005];
int q[1005];
bool vis[1005];
inline long long f(int x){return 1LL*x*x;}
bool work(){
	int L=1,R=0;
	for(int i=1;i<=n;i++)if(z[i]<=r)vis[i]=true,q[++R]=i;
	else vis[i]=false;
	while(R>=L){
		int u=q[L++];
		if(z[u]+r>=h)return true;
		for(int i=1;i<=n;i++)if((!vis[i])&&(b[u][i]))vis[i]=true,q[++R]=i;
	}
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	test=read();
	while(test--){
		n=read();h=read();r=read();R=4*f(r);
		for(int i=1;i<=n;i++){
			x[i]=read(),y[i]=read(),z[i]=read();
			if(z[i]+r<=0)i--,n--;
			else if(z[i]-r>h)i--,n--;
		}
		for(int i=1;i<=n;i++)
			for(int j=i+1;j<=n;j++)if(f(x[i]-x[j])+f(y[i]-y[j])+f(z[i]-z[j])<=R)b[i][j]=b[j][i]=true;
			else b[i][j]=b[j][i]=false;
		if(work())puts("Yes");
		else puts("No");
	}
}

#include<cstdio>
#include<algorithm>
#define inf 100000000
#define lowbit(x) (x&-x)
using namespace std;
inline int read(){
	char c=getchar();int ret=0;
	while((c<'0')||(c>'9'))c=getchar();
	while((c>='0')&&(c<='9'))ret=(ret<<1)+(ret<<3)+c-'0',c=getchar();
	return ret;
}
int n,m,P,ans;
int jc[15];
int w[15][15],to[4096],s[4096];
int p[15][4096],q[4096][4096],To[4096][4096],dp[13][531441];
void up(int &x,int y){x=min(x,y);}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read();m=read();
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)w[i][j]=inf;
	for(int i=1;i<=m;i++){
		int u=read()-1,v=read()-1;
		w[u][v]=min(w[u][v],read());
		w[v][u]=w[u][v];
	}
	jc[0]=1;
	for(int i=1;i<=n;i++)jc[i]=jc[i-1]*3;
	for(int i=1;i<(1<<n);i++)s[i]=s[i-lowbit(i)]+1;
	for(int i=0;i<n;i++)to[1<<i]=i;
	for(int i=0;i<(1<<n);i++)
		for(int j=i;true;j=(j-1)&i){
			for(int k=0;k<n;k++){
				if(j&(1<<k))To[i][j]+=jc[k]*2;
				else if(i&(1<<k))To[i][j]+=jc[k];
			}
			if(j==0)break;
		}
	for(int i=1;i<=n;i++)
		for(int j=0;j<jc[n];j++)dp[i][j]=inf;
	for(int i=0;i<n;i++){
		p[i][0]=inf;
		for(int j=1;j<(1<<n);j++)p[i][j]=min(p[i][j-lowbit(j)],w[i][to[lowbit(j)]]);
	}
	for(int i=1;i<(1<<n);i++)
		for(int j=1,k=1;j<(1<<n);j++,k=lowbit(j))if((p[to[k]][i]==inf)||(q[i][j-k]==inf))q[i][j]=inf;
		else q[i][j]=q[i][j-k]+p[to[k]][i];
	P=(1<<n)-1;
	for(int i=0;i<n;i++)dp[1][To[1<<i][1<<i]]=0;
	for(int i=1;i<(1<<n);i++)
		for(int k=i;k;k=(k-1)&i){
			if((k==i)&&(lowbit(i)!=i))continue;
			for(int w=1;w<=s[i-k]+1;w++){
				if(dp[w][To[i][k]]==inf)continue;
				for(int j=P-i;j;j=(j-1)&(P-i))if(q[k][j]<inf)
					up(dp[w+1][To[i+j][j]],dp[w][To[i][k]]+w*q[k][j]);
			}
		}
	ans=inf;
	for(int i=1;i<=n;i++)
		for(int j=1;j<(1<<n);j++)up(ans,dp[i][To[P][j]]);
	printf("%d",ans);
}

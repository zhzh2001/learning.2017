type
  num=array[0..10000] of real;
  holes=record
  x,y,z:real;
  father:longint;
end;
  holess=array[0..10000] of holes;

var
  t,i,j,n,h,r:longint;
  hole:holess;
  dist:num;

procedure swap(var x1,y1:real;x2,y2:holes);
var
  k:real;
  t:holes;
begin
  k:=x1;
  x1:=y1;
  y1:=k;
  t:=x2;
  x2:=y2;
  y2:=t;
end;

procedure qsort(l,r:longint);
var
  lt,rt,mid:longint;
begin
  lt:=l;
  rt:=r;
  mid:=(lt+rt) div 2;
  repeat
    while dist[lt] < dist[mid] do inc(lt);
    while dist[rt] > dist[mid] do dec(rt);
    if lt<=rt then
    begin
      swap(dist[lt],dist[rt],hole[lt],hole[rt]);
      inc(lt);
      dec(rt);
    end;
  until lt>rt;
  if lt<r then qsort(lt,r);
  if rt>l then qsort(l,rt);
end;

function dists(x1,y1,z1,x2,y2,z2:real):real;
begin
  dists:=sqrt(sqr(x1-x2)+sqr(y1-y2)+sqr(z1-z2));
end;

procedure cheese(n,h,r:longint);
var
  i,j,k:longint;
  flag,flag2:boolean;

begin
  fillchar(hole,sizeof(hole),0);
  for i:=1 to n do
  begin
    read(hole[i].x,hole[i].y,hole[i].z);
    dist[i]:=hole[i].z;
  end;
  qsort(1,n);
  flag:=false;
  for j:=1 to n do
  begin
    flag2:=false;
    if dist[j] <= r then
    begin
      hole[j].father:=1;
    end;
    k:=j;
    repeat
      dec(k);
      if k<=0 then break;
      if hole[j].father <> 3 then
      begin
        if dists(hole[j].x,hole[j].y,hole[j].z,hole[k].x,hole[k].y,hole[k].z) <= 2*r then
        begin
          hole[j].father:=hole[k].father;
          flag2:=true;
        end;
      end
      else break;
    until (flag2) or (hole[j].z-hole[k].z > 2*r);
    if (h-dist[j] <= r) and (hole[j].father<=1) then hole[j].father:=hole[j].father + 2;
    if hole[j].father = 3 then
    begin
      flag:=true;
      break;
    end;
  end;
  if flag then writeln('Yes')
  else writeln('No');
end;


begin
  assign(input,'cheese.in');
  reset(input);
  assign(output,'cheese.out');
  rewrite(output);
  read(t);
  for i:=1 to t do
  begin
    read(n,h,r);
    cheese(n,h,r);
  end;
  close(input);
  close(output);
end.

var
  n,m,q,i,j,x,y,k:longint;
  leave:int64;
  a:array[0..22000,0..22000] of longint;

begin
  assign(input,'phalanx.in');
  reset(input);
  assign(output,'phalanx.out');
  rewrite(output);
  writeln(maxlongint);
  read(n,m,q);
  for i:=1 to n do
    for j:=1 to m do
    begin
      a[i,j]:=(i-1)*m+j;
    end;
  for i:=1 to q do
  begin
    read(x,y);
    leave:=a[x,y];
    writeln(a[x,y]);
    a[x,y]:=0;
    for j:=y to m do
      a[x,j]:=a[x,j+1];
    for j:=x to n do
      a[j,m]:=a[j+1,m];
    a[n,m]:=leave;
  end;
  close(input);
  close(output);
end.

#include<cstdio>
#include<cstring>
int T,n,h,en,vet[1200005],Next[1200005],head[1005],que[1005],qhead,qtail;
bool  vis[1005];
long long a[1005],b[1005],c[1005],rf,r;
int read(){
	int ans=0;
	char ch=getchar(),last=' ';
	while(ch>'9'||ch<'0'){
		last=ch;
		ch=getchar();
	}
	while(ch<='9'&&ch>='0'){
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	if(last=='-')
		ans=-ans;
	return ans;
}
void addedge(int u,int v){
	vet[++en]=v;
	Next[en]=head[u];
	head[u]=en;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--){
		n=read();
		h=read();
		r=read();
		rf=r*r*4ll;
		for(int i=1;i<=n;i++){
			a[i]=read();
			b[i]=read();
			c[i]=read();
			if(c[i]<=r){
				addedge(i,n+1);
				addedge(n+1,i);
			}
			if(c[i]+r>=h){
				addedge(i,n+2);
				addedge(n+2,i);
			}
		}
		for(int i=1;i<n;i++)
			for(int j=i+1;j<=n;j++)
				if((a[i]-a[j])*(a[i]-a[j])+(b[i]-b[j])*(b[i]-b[j])<=rf&&(a[i]-a[j])*(a[i]-a[j])+(b[i]-b[j])*(b[i]-b[j])+(c[i]-c[j])*(c[i]-c[j])<=rf){
					addedge(i,j);
					addedge(j,i);
				}
		vis[n+1]=true;
		qhead=1;
		qtail=1;
		que[1]=n+1;
		while(qhead<=qtail){
			int u=que[qhead++];
			for(int i=head[u];i;i=Next[i]){
				int v=vet[i];
				if(!vis[v]){
					vis[v]=true;
					if(v==n+2)
						break;
					que[++qtail]=v;
				}
			}
			if(vis[n+2])
				break;
		}
		if(vis[n+2])
			printf("Yes\n");
		else
			printf("No\n");
		for(int i=1;i<=n+2;i++){
			head[i]=0;
			vis[i]=false;
		}
		for(int i=1;i<=en;i++)
			Next[i]=0;
		en=0;
	}
	return 0;
}

#include<stdio.h>
#include<cstring>
int t,root,n,m,T,x,y,id[50005],num,sum[1000005],val[1000005],key[1000005],tree[1000005],son[1000005][2],fa[1000005];
long long a[50005],b[505][50005],tmp;
void write(long long x){
	char ch[15];
	int len=0;
	while(x){
		ch[++len]=(int)(x%10)+'0';
		x/=10ll;
	}
	for(int i=len;i>=1;i--)
		putchar(ch[i]);
	putchar('\n');
}
void updata(int x){
	sum[x]=tree[x];
	if(son[x][0])
		sum[x]+=sum[son[x][0]];
	if(son[x][1])
		sum[x]+=sum[son[x][1]];
}
void rotate(int x,int &k){
	int y=fa[x],z=fa[y],l,r;
	l=(son[y][1]==x);
	r=l^1;
	if(y==k)
		k=x;
	else
		son[z][son[z][1]==y]=x;
	fa[x]=z;
	fa[y]=x;
	fa[son[x][r]]=y;
	son[y][l]=son[x][r];
	son[x][r]=y;
	updata(y);
	updata(x);
}
int read(){
	int ans=0;
	char ch=getchar();
	while(ch>'9'||ch<'0')
		ch=getchar();
	while(ch<='9'&&ch>='0'){
		ans=ans*10+ch-'0';
		ch=getchar();
	}
	return ans;
}
void splay(int x,int &k){
	while(x!=k){
		int y=fa[x],z=fa[y];
		if(y!=k){
			if(son[z][1]==y^son[y][1]==x)
				rotate(x,k);
			else
				rotate(y,k);
		}
		rotate(x,k);
	}
}
void Insert(int &u,int pre,int k,int v){
	if(!u){
		u=++num;
		fa[u]=pre;
		val[u]=v;
		key[u]=k;
		sum[u]=tree[u]=1;
		splay(u,root);
		return;
	}
	if(k<key[u])
		Insert(son[u][0],u,k,v);
	else
		Insert(son[u][1],u,k,v);
	updata(u);
}
int query(int u,int kth){
	if(son[u][0])
		t=sum[son[u][0]];
	else
		t=0;
	if(t>=kth)
		return query(son[u][0],kth);
	else{
		if(t==kth-tree[u])
			return u;
		return query(son[u][1],kth-t-tree[u]);
	}
}
void build(int &u,int pre,int l,int r){
	int mid=(l+r)>>1;
	if(l>r){
		u=0;
		return;
	}
	u=mid;
	tree[u]=1;
	key[u]=u;
	val[u]=u;
	fa[u]=pre;
	build(son[u][0],u,l,mid-1);
	build(son[u][1],u,mid+1,r);
	updata(u);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&T);
	if(n<=50000&&m<=50000&&T<=500){
		for(int i=1;i<=n;i++)
			a[i]=(long long)(i-1ll)*m+(long long)m;
		while(T--){
			scanf("%d%d",&x,&y);
			if(y==m){
				write(a[x]);
				tmp=a[x];
				for(int i=x;i<n;i++)
					a[i]=a[i+1];
				a[n]=tmp;
			}
			else
			if(id[x]){
				write(b[id[x]][y]);
				tmp=b[id[x]][y];
				for(int i=y;i<m-1;i++)
					b[id[x]][i]=b[id[x]][i+1];
				b[id[x]][m-1]=a[x];
				for(int i=x;i<n;i++)
					a[i]=a[i+1];
				a[n]=tmp;
			}
			else {
				write((long long)(x-1ll)*(long long)m+(long long)y);
				tmp=(long long)(x-1ll)*(long long)m+(long long)y;
				num++;
				id[x]=num;
				for(int i=1;i<m;i++)
					b[id[x]][i]=(long long)(x-1ll)*(long long)m+(long long)i;
				for(int i=y;i<m-1;i++)
					b[id[x]][i]=b[id[x]][i+1];
				b[id[x]][m-1]=a[x];
				for(int i=x;i<n;i++)
					a[i]=a[i+1];
				a[n]=tmp;
			}
		}
	}
	else
	if(n==1){
		build(root,0,1,m);
		num=m;
		while(T--){
			x=read();
			y=read();
			tmp=query(root,y);
			printf("%d\n",val[tmp],tmp); 
			splay(tmp,root);
			tree[tmp]=0;
			sum[tmp]--;
			Insert(root,0,num+1,val[tmp]);
		}
	}
	return 0;
}

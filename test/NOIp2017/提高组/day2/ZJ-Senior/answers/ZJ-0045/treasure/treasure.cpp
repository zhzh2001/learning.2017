#include<cstdio>
#include<algorithm>
using namespace std;
int n,m,G[15][15],ans,dis[15],Next[15],Pre[15],q[15],add,Min[15];
void dfs(int t,int sum){
	if(t>n){
		ans=min(ans,sum);
		return;
	}
	if(sum+add>=ans)
		return;
	for(int i=1;i<t;i++){
		for(int j=Next[n+1];j;j=Next[j])
			if(G[q[i]][j]<1000000000){
				q[t]=j;
				if(Next[j])
					Pre[Next[j]]=Pre[j];
				if(Pre[j])
					Next[Pre[j]]=Next[j];
				add-=Min[j];
				dis[j]=dis[q[i]]+1;
				dfs(t+1,sum+G[q[i]][j]*dis[q[i]]);
				add+=Min[j];
				Pre[Next[j]]=j;
				Next[Pre[j]]=j;
			}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++){
			G[i][j]=1000000000;
			G[j][i]=1000000000;
		}
	for(int i=1;i<=n;i++)
		Min[i]=1000000000;
	for(int i=1;i<=m;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		G[x][y]=min(G[x][y],z);
		G[y][x]=min(G[y][x],z);
		Min[x]=min(Min[x],z);
		Min[y]=min(Min[y],z);
	}
	for(int i=1;i<=n;i++)
		add+=Min[i];
	ans=1000000000;
	for(int i=1;i<n;i++)
		Next[i]=i+1;
	for(int i=2;i<=n;i++)
		Pre[i]=i-1;
	Next[n+1]=1;
	Pre[1]=n+1;
	for(int i=n;i>=1;i--){
		q[1]=i;
		dis[i]=1;
		if(Next[i])
			Pre[Next[i]]=Pre[i];
		if(Pre[i])
			Next[Pre[i]]=Next[i];
		add-=Min[i];
		dfs(2,0);
		add+=Min[i];
		Pre[Next[i]]=i;
		Next[Pre[i]]=i;
	}
	printf("%d\n",ans);
	return 0;
}

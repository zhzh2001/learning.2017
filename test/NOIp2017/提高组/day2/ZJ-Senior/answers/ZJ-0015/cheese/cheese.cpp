#include<set>
#include<cmath>
#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
typedef double db;
typedef long long ll;
typedef pair<int,int> pa;
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for(int (i)=(a);(i)<=(b);(i)++)
#define rep2(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
#define per(i,a,b) for(int (i)=(a);(i)>=(b);(i)--)
#define Rep(p,x) for(int (p)=head[(x)];(p);(p)=nxt[(p)])
#define Rep2(p,x) for(int (p)=cur[(x)];(p);(p)=nxt[(p)])
template<class T>void read(T&num){
	num=0;T f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')num=num*10+ch-'0',ch=getchar();
	num*=f;
}
const int maxn=1e3+5;
int n,h,r,tot;
int head[maxn],des[maxn*maxn*2],nxt[maxn*maxn*2];
int x[maxn],y[maxn],z[maxn],vis[maxn];
void init(){
	memset(head,0,sizeof(head));
	memset(vis,0,sizeof(vis));
	tot=0;
}
ll sqr(int x){
	return 1ll*x*x;
}
void dfs(int x){
	vis[x]=1;
	Rep(p,x)if(!vis[des[p]])dfs(des[p]);
}
void adde(int x,int y){
	des[++tot]=y;nxt[tot]=head[x];head[x]=tot;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;read(T);
	while(T--){
		read(n);read(h);read(r);
		init();
		rep(i,1,n)read(x[i]),read(y[i]),read(z[i]);
		int S=0,T=n+1;
		rep(i,1,n)if(z[i]<=r)adde(S,i),adde(i,S);
		rep(i,1,n)if(abs(h-z[i])<=r)adde(T,i),adde(i,T);
		rep(i,1,n)rep(j,i+1,n){
			ll dis=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
			if(dis<=4ll*r*r)adde(i,j),adde(j,i);
		}
		dfs(S);
		if(vis[T])puts("Yes");
		else puts("No");
	}
	return 0;
}

#include<set>
#include<cmath>
#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
typedef double db;
typedef long long ll;
typedef pair<int,int> pa;
#define mp make_pair
#define pb push_back
#define mid ((l+r)>>1)
#define rep(i,a,b) for(int (i)=(a);(i)<=(b);(i)++)
#define rep2(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
#define per(i,a,b) for(int (i)=(a);(i)>=(b);(i)--)
#define Rep(p,x) for(int (p)=head[(x)];(p);(p)=nxt[(p)])
#define Rep2(p,x) for(int (p)=cur[(x)];(p);(p)=nxt[(p)])
template<class T>void read(T&num){
	num=0;T f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')num=num*10+ch-'0',ch=getchar();
	num*=f;
}
const int maxn=3e5+5,maxm=1e7+2e6+5;
int n,m,q,ndtot,lim;
vector<ll>row[maxn],line;
int ls[maxm],rs[maxm],sum[maxm];
int rt_row[maxn],rt_line;
int Q(int &x,int l,int r,int k){
	if(!x)x=++ndtot;
	sum[x]++;
	if(l==r)return l;
	int cnt=(mid-l+1)-sum[ls[x]];
	if(cnt>=k)return Q(ls[x],l,mid,k);
	return Q(rs[x],mid+1,r,k-cnt);
}
ll del_row(int x,int y){
	int nowp=Q(rt_row[x],1,lim,y);
	if(nowp<=m-1)return 1ll*(x-1)*m+nowp;
	nowp-=m;
	return row[x][nowp];
}
ll del_line(int y){
	int nowp=Q(rt_line,1,lim,y);
	return line[nowp-1];
}
void push_row(int x,ll num){
	row[x].pb(num);
}
void push_line(ll num){
	line.pb(num);
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	read(n);read(m);read(q);
	lim=max(n,m)+q;
	rep(i,1,n)line.pb(1ll*i*m);
	while(q--){
		int x,y;read(x);read(y);
		if(y!=m){
			ll num1=del_row(x,y),num2=del_line(x);
			push_row(x,num2);push_line(num1);
			printf("%lld\n",num1);
		}else{
			ll num=del_line(x);
			push_line(num);
			printf("%lld\n",num);
		}
	}
	return 0;
}

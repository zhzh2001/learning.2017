#include<set>
#include<cmath>
#include<queue>
#include<cstdio>
#include<vector>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
typedef double db;
typedef long long ll;
typedef pair<int,int> pa;
#define mp make_pair
#define pb push_back
#define rep(i,a,b) for(int (i)=(a);(i)<=(b);(i)++)
#define rep2(i,a,b) for(int (i)=(a);(i)<(b);(i)++)
#define per(i,a,b) for(int (i)=(a);(i)>=(b);(i)--)
#define Rep(p,x) for(int (p)=head[(x)];(p);(p)=nxt[(p)])
#define Rep2(p,x) for(int (p)=cur[(x)];(p);(p)=nxt[(p)])
template<class T>void read(T&num){
	num=0;T f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')num=num*10+ch-'0',ch=getchar();
	num*=f;
}
const int maxn=13,maxm=1005,inf=1e8;
int n,m;
int g[1<<maxn][maxn],f[1<<maxn][maxn];
void upd(int &x,int y){
	x=(x<y?x:y);
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	rep2(s,0,1<<n)rep2(j,0,n)g[s][j]=inf;
	rep(i,1,m){
		int x,y,z;read(x);read(y);read(z);
		x--;y--;
		rep2(s,0,1<<n)if(s>>x&1)upd(g[s][y],z);
		rep2(s,0,1<<n)if(s>>y&1)upd(g[s][x],z);
	}
	int U=(1<<n)-1,ans=inf;
	rep2(st,0,n){
		rep2(s,0,1<<n)rep2(j,0,n)f[s][j]=inf;
		f[1<<st][0]=0;
		rep2(s,0,1<<n)rep2(lv,0,n)if(f[s][lv]<inf){
			int rest=U^s;
			for(int ns=rest;ns;ns=(ns-1)&rest){
				int tot=0;
				rep2(j,0,n)if(ns>>j&1)tot+=g[s][j];
				if(tot>inf)continue;
				upd(f[ns|s][lv+1],f[s][lv]+tot*(lv+1));
			}
		}
		rep2(lv,0,n)upd(ans,f[U][lv]);
	}
	printf("%d\n",ans);
	return 0;
}

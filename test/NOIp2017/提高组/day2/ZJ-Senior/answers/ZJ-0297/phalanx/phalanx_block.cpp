#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<set>
#include<map>
#include<ctime>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int Size=50005;
const int Query=505;

int Dat[Query][Size],Map[Size],Lin[Size];
int n,m,q,cnt;
void Brute_Force(){
	Rep(i,1,n) Lin[i]=(i-1)*m+m;
	Rep(i,1,q){
		int x=read(),y=read();
		if (!Map[x]){
			Map[x]=++cnt;
			Rep(j,1,m-1) Dat[cnt][j]=(x-1)*m+j;
		}
		int val;
		if (y==m) printf("%d\n",val=Lin[x]);
			else printf("%d\n",val=Dat[Map[x]][y]);
		Rep(j,y+1,m-1) swap(Dat[Map[x]][j],Dat[Map[x]][j-1]);
		if (y<m) swap(Dat[Map[x]][m-1],Lin[x]);
		Rep(j,x+1,n) swap(Lin[j],Lin[j-1]);
//		Lin[n]=val;
//		printf("cnt=%d\n",cnt);
	}
}
const int N=600005;
const int B=1005;

int Q[N],l[N],r[N],Nex[N],block;
int head,tail;
inline int find(int val){
	int pos=head;val--;
	while (val>block) val-=block,pos=Nex[pos];
	while (val>0) val--,pos=r[pos];
	return pos;
}
void Block_Linker(){
	block=sqrt(n);
	Rep(i,1,m) Q[++*Q]=i;
	Rep(i,2,n) Q[++*Q]=i*m;
	Rep(i,1,*Q) l[i]=i-1,r[i]=i+1;
	Dep(i,*Q-block,1) Nex[i]=i+block;
	head=1,tail=*Q;
	
		int pos=head;
//		while (pos!=10) printf("%d ",Nex[pos]==0?0:Q[Nex[pos]],pos),pos=r[pos];puts("");
		pos=head;
		
	Rep(i,1,q){
		int x=read(),y=read();
//		printf("find(y)=%d\n",find(y));
		printf("%d\n",Q[find(y)]);
		int pos=find(y);
		r[l[pos]]=r[pos];
		l[r[pos]]=l[pos];
		if (pos==head) head=r[pos];
		int rp=r[pos];
		r[l[pos]=tail]=pos;
		r[pos]=0;
		tail=pos;
		Nex[tail]=0;
		
		int cnt;
		pos=find(y),cnt=block;
		pos=l[pos];cnt--;
		while (pos!=0 && cnt>0){
			Nex[pos]=r[Nex[pos]];
			pos=l[pos];
			cnt--;
		}
		if (pos!=0) Nex[pos]=rp;
		pos=find(*Q),cnt=block;
		while (pos!=0 && cnt>0){
			pos=l[pos];
			cnt--;
		}
		Nex[pos]=tail;
		pos=head;
		/*
		Rep(i,1,*Q) printf("%d ",r[i]);
		puts("");
		Rep(i,1,*Q) printf("%d ",l[i]);
		puts("");
		printf("head=%d tail=%d\n",head,tail);

		while (pos!=0) printf("%d ",Nex[pos]==0?0:Q[Nex[pos]]),pos=r[pos];puts("");
		pos=head;
		while (pos!=0) printf("%d ",Q[pos]),pos=r[pos];
		puts("");
		*/
	}
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(),m=read(),q=read();
	Block_Linker();
}


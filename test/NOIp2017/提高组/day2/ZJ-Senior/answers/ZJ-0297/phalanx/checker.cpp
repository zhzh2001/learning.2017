#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<set>
#include<map>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,m,q;
int res[10000005];
int main(){
	freopen("phalanx.in","r",stdin);
	n=read(),m=read(),q=read();
	freopen("phalanx.out","r",stdin);
	Rep(i,1,q) res[i]=read();
	freopen("phalanx2.ans","r",stdin);
	Rep(i,1,q) if (res[i]!=read()){
		printf("%d\n",i);return 0;
	}
	puts("AC");
}


#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<set>
#include<map>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,m,q;
int main(){
	srand(time(0));
	freopen("phalanx.in","w",stdout);
	printf("%d %d %d\n",n=1000,m=1000,q=100000);
	Rep(i,1,q){
		int x=1,y=rand()%m+1;
		printf("%d %d\n",x,y);
	}
}


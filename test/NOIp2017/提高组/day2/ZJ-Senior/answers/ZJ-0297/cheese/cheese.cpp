#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<set>
#include<map>
#include<ctime>
#include<cmath>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=1005;

int x[N],y[N],z[N];
inline double Dis(int a,int b){
	return sqrt(1ll*(x[a]-x[b])*(x[a]-x[b])+1ll*(y[a]-y[b])*(y[a]-y[b])+1ll*(z[a]-z[b])*(z[a]-z[b]));
}
int n,h,r,Fla[N],Vis[N],Q[N];
bool Work(){
	n=read(),h=read(),r=read();
	Rep(i,1,n) x[i]=read(),y[i]=read(),z[i]=read();
	memset(Fla,0,sizeof(Fla));
	memset(Vis,0,sizeof(Vis));
	Rep(i,1,n) if (z[i]>=h-r) Fla[i]=true;
	int head=0,tail=0;
	Rep(i,1,n) if (z[i]<=r) Vis[Q[++tail]=i]=true;
	while (head<tail){
		int u=Q[++head];
		if (Fla[u]) return true;
		Rep(i,1,n) if (!Vis[i] && Dis(u,i)-(1e-9)<=2*r){
			Vis[i]=true;
			Q[++tail]=i;
		}
	}
	return false;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	Rep(i,1,T){
		int res=Work();
		puts(res?"Yes":"No");
	}
}


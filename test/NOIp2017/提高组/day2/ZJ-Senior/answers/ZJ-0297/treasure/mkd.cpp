#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<set>
#include<map>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
int n,m;
int main(){
	freopen("treasure.in","w",stdout);
	srand(time(0));
	printf("%d %d\n",n=12,m=1000);
	Rep(i,1,n-1) printf("%d %d %d\n",i,i+1,5000);
	Rep(i,n,m) printf("%d %d %d\n",rand()%n+1,rand()%n+1,200+rand()%5000);
}

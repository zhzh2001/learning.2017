#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<vector>
#include<set>
#include<map>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int res=0,ch=getchar(),f=1;
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=15;

int INF;
inline int Pow(int base,int k){
	int res=1;
	Rep(i,1,k) res=res*base;
	return res;
}
int n,m;
int Dis[N][N],Pat[N][1<<12],f[1<<12][12];
int Ref[1<<12][1<<12];
inline int bol(int x,int val){
	if ((val>>x-1)&1) return true;
		else return false;
}
inline int count(int val){
	int res=0;
	Rep(i,1,n) if (bol(i,val)) res++;
	return res;
}
inline int Dp(int fab,int dep){
	if (f[fab][dep]!=-1) return f[fab][dep];
	if (fab==0) return f[fab][dep]=INF;
	if (dep==0){
		if (count(fab)>1) return f[fab][dep]=INF;
			else return f[fab][dep]=0;
	}
	f[fab][dep]=INF;
	for (int sub=(fab-1)&fab;sub>0;sub=(sub-1)&fab){
		int lef=fab^sub,res=Ref[sub][lef];
		if (res!=INF) res*=dep;
		res+=Dp(sub,dep-1);
		f[fab][dep]=min(f[fab][dep],res);
	}
	return f[fab][dep];
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(),m=read();
	memset(Dis,63,sizeof(Dis));
	memset(f,-1,sizeof(f));
	INF=Dis[0][0];
	Rep(i,1,m){
		int u=read(),v=read(),w=read();
		Dis[u][v]=min(Dis[u][v],w);
		Dis[v][u]=min(Dis[v][u],w);
	}
	memset(Pat,63,sizeof(Pat));
	Rep(i,1,n) Rep(fab,0,(1<<n)-1){
		Rep(j,1,n) if (bol(j,fab)) Pat[i][fab]=min(Pat[i][fab],Dis[j][i]);
	}
	memset(Ref,63,sizeof(Ref));
	
	Rep(x,1,(1<<n)-1) Rep(y,1,(1<<n)-1) if ((x&y)==0){
		Ref[x][y]=0;
		Rep(k,1,n) if (bol(k,y) && Ref[x][y]!=INF){
			if (Pat[k][x]==INF) Ref[x][y]=INF;
				else Ref[x][y]+=Pat[k][x];
		}
	}
	int Ans=INF;
	Rep(i,0,n-1) Ans=min(Ans,Dp((1<<n)-1,i));
	printf("%d\n",Ans);
//	printf("%d\n",n*Pow(3,n)*n);
}


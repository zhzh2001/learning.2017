#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
int n,m,ans=0x7fffffff;
int ma[15][15];

void tst(int x){
	int que[20],dis[20],top=0,dep=0,tmp=0;
	que[dep++]=x;
	for(int i=1;i<=n;i++) dis[i]=-1;
	dis[x]=0;
	while(top!=dep){
		x=que[top++];
		for(int i=1;i<=n;i++)
			if(ma[x][i]!=-1&&x!=i)
				if(dis[i]==-1){
					que[dep++]=i;
					dis[i]=dis[x]+1;
					tmp+=ma[x][i]*dis[i];
				}
	}
	ans=min(ans,tmp);
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	int a,b,c;
	memset(ma,-1,sizeof(ma));
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&a,&b,&c);
		if(ma[a][b]==-1||ma[a][b]>c){
			ma[a][b]=c;
			ma[b][a]=c;
		}
	}
	for(int i=1;i<=n;i++)
		tst(i);
	printf("%d\n",ans);
	return 0;
}

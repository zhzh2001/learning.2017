#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <algorithm>
using namespace std;
typedef long long LL;
const int N=1010;
int T;
LL n,h,r,r2;
int cofind[N];
struct point{
	LL x,y,z;
}po[N];

inline bool cmp(point i,point j){
	return i.z<j.z;
}

int find(int x){
	if(cofind[x]==-1) return x;
	return cofind[x]=find(cofind[x]);
}

void getco(int x,int y){
	int s=find(x);
	int t=find(y);
	if(s==t) return ;
	cofind[s]=t;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--){
		scanf("%lld%lld%lld",&n,&h,&r);
		r2=4ll*r*r;
		cofind[0]=-1;
		for(int i=1;i<=n;i++){
			scanf("%lld%lld%lld",&po[i].x,&po[i].y,&po[i].z);
			cofind[i]=-1;
		}
		cofind[n+1]=-1;
		sort(po+1,po+n+1,cmp);
		int i;
		for(i=n;i>=1;i--){
			if(po[i].z>=h-r)
				getco(i,n+1);
			else
				break;
		}
		for(i=1;i<=n;i++){
			if(po[i].z<=r)
				getco(i,0);
			else
				break;
		}
		unsigned long long dts;
		for(i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				if(find(i)==find(j)) continue;
				if(po[j].z-po[i].z>2*r) break;
				dts=(po[i].x-po[j].x)*(po[i].x-po[j].x)+
					(po[i].y-po[j].y)*(po[i].y-po[j].y)+
					(po[i].z-po[j].z)*(po[i].z-po[j].z);
				if(dts<=r2)
					getco(i,j);
			}	
		}
		if(find(0)==find(n+1))
			printf("Yes\n");
		else
			printf("No\n");
	}
	return 0;
}

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int Q=303000;
const int N=1010;
int ma[N][N];
int movx[Q],movy[Q];
int n,m,q;

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if((n+m)*q<=30000000&&n<=1000&&m<=1000){
		int x=1,y,tmp;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				ma[i][j]=x++;
		for(int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			printf("%d\n",ma[x][y]);
			tmp=ma[x][y];
			for(int j=y;j<=m;j++)
				ma[x][j]=ma[x][j+1];
			for(int j=x;j<=n;j++)
				ma[j][m]=ma[j+1][m];
			ma[n][m]=tmp;
		}
	}
	else if(n==1&&m*q<=30000000){
		int ttm[301000];
		int x=1,y,tmp;
		for(int i=1;i<=m;i++)
			ttm[i]=x++;
		for(int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			printf("%d\n",ttm[y]);
			tmp=ttm[y];
			for(int j=y;j<=m;j++)
				ttm[j]=ttm[j+1];
			ttm[m]=tmp;
		}
	}
	else{
		for(int i=1;i<=q;i++){
			scanf("%d%d",&movx[i],&movy[i]);
			int x=movx[i],y=movy[i];
			for(int j=i-1;j>=1;j--){
				if(x==n&&y==m){
					x=movx[j];
					y=movy[j];
				}
				else{
					if(y==m&&x>=movx[j])
						x++;
					if(x==movx[j]&&y>=movy[j])
						y++;
				}
			}
			printf("%lld\n",1ll*((x-1)*m+y));
		}
	}
	return 0;
}

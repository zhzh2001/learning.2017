#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

int f[1<<14][15];
struct FA{
	int f,t,cost;
}ZY[1031441];
int n,m,Edge[15][15],u,v,tot,w;


bool Exist(int sta,int pos){
	return (sta>>(pos-1))&1;
}

int check(int sta,int nowsta){
	int res=0;
	for (int i=1;i<=n;i++)
		if (!Exist(sta,i)&&Exist(nowsta,i)){
			int tmpcost=0x3f3f3f3f;
			for (int j=1;j<=n;j++)
				if (Exist(sta,j))
					tmpcost=min(tmpcost,Edge[j][i]);
			if (tmpcost==0x3f3f3f3f) return 0x3f3f3f3f;
			res+=tmpcost;
		}
	return res;
}

void dfs(int sta,int nowsta,int pos){
	if (pos>n){
		int tmp=check(sta,nowsta);
		if (tmp!=0x3f3f3f3f) {
			//printf("%d->%d cost=%d\n",sta,nowsta,tmp);
			ZY[++tot]=(FA){sta,nowsta,tmp};
		}
		return;
	}
	if (!Exist(sta,pos)){
		dfs(sta,nowsta|(1<<(pos-1)),pos+1);
		dfs(sta,nowsta,pos+1);
	}else{
		dfs(sta,nowsta,pos+1);
	}
}

void init(){
	for (int i=1;i<=(1<<n)-1;i++)
		dfs(i,i,1);
}

int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(Edge,0x3f3f3f3f,sizeof Edge);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&u,&v,&w);
		Edge[u][v]=Edge[v][u]=min(Edge[u][v],w);
	}
	init();
	memset(f,0x3f3f3f3f,sizeof f);
	for (int i=1;i<=n;i++)
		f[1<<(i-1)][0]=0;
	for (int i=1;i<n;i++)
		for (int staid=1;staid<=tot;staid++){
			if (f[ZY[staid].f][i-1]==0x3f3f3f3f)continue;
			f[ZY[staid].t][i]=min(f[ZY[staid].t][i],f[ZY[staid].f][i-1]+ZY[staid].cost*i);
		}
	printf("%d",f[(1<<n)-1][n-1]);
}

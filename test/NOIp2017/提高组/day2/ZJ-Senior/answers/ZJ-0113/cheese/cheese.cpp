#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
typedef unsigned long long ull;
typedef long long ll;
bool vis[1010];
struct H{
	int x,y,z;
}Hole[1010];
int n,h,r,Head[1010],Next[1010000],Tree[101000],tot;
ull R;

bool dfs(int x){
	if (x==n+1) return true;
	if (vis[x]) return false;
	vis[x]=true;
	for (int it=Head[x];it;it=Next[it])
		if (dfs(Tree[it]))return true;
	return false;
}

void addedge(int u,int v){
	//printf("addege:%d->%d\n",u,v);
	tot++;
	Tree[tot]=v;
	Next[tot]=Head[u];
	Head[u]=tot;
}

bool xj(int i,int j){
	ull dis=(ull)((ll)(Hole[i].x-Hole[j].x)*(ll)(Hole[i].x-Hole[j].x));
	if (dis>R)return false;
	dis+=(ull)((ll)(Hole[i].y-Hole[j].y)*(ll)(Hole[i].y-Hole[j].y));
	if (dis>R)return false;
	dis+=(ull)((ll)(Hole[i].z-Hole[j].z)*(ll)(Hole[i].z-Hole[j].z));
	if (dis>R) return false;
	return true;
}

int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int t;
	scanf("%d",&t);
	while(t--){
	scanf("%d%d%d",&n,&h,&r);
	R=(ull)((ll)4*(ll)r*(ll)r);
	memset(Head,0,sizeof Head);
	memset(vis,0,sizeof vis);
	tot=0;
	for (int i=1;i<=n;i++){
		scanf("%d%d%d",&Hole[i].x,&Hole[i].y,&Hole[i].z);
		if (Hole[i].z<=r) addedge(0,i);
		if (Hole[i].z+r>=h) addedge(i,n+1);
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=n;j++){
			if (i==j)continue;
			if (xj(i,j)) addedge(i,j);
		}
	if (dfs(0)) printf("Yes\n");else printf("No\n");
	}
}

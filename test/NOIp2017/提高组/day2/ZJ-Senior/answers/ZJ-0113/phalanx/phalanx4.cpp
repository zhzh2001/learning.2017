#include<stdio.h>
#include<algorithm>
using namespace std;
typedef long long ll;
int n,m,q,ans;
int BITend,tail;
ll lastl[600010],link[600010];
struct Node{
	int sum,l,r;
	ll val;
}tree[600010*4];
int pointtot;
int ANS;
int root[300010];
void pushup(int rt){
	tree[rt].sum=tree[tree[rt].l].sum+tree[tree[rt].r].sum;
}

int init(int l,int r,int dep,int x){
	pointtot++;
	if (l==r) tree[pointtot].val=(ll)(x-1)*(ll)m+(ll)l;
	if (l>m-1) tree[pointtot].sum=0;
		else
	tree[pointtot].sum=(r>=m-1)?(m-l):(r-l+1);
	if (dep==1) return pointtot;
	int tmp=pointtot;
	int mid=(l+r)>>1;
	tree[tmp].l=init(l,mid,dep-1,x);
	tree[tmp].r=init(mid+1,r,dep-1,x);
	if (dep>1) pushup(tmp);
	return tmp;
}

int query(int rt,int l,int r,int kth,int x){
	if (l==r){ANS=tree[rt].val;return l;}
	int mid=(l+r)>>1;
	if (tree[rt].l==0) tree[rt].l=init(l,mid,1,x);
	if (tree[rt].r==0) tree[rt].r=init(mid+1,r,1,x);
	if (tree[tree[rt].l].sum>=kth)
		return query(tree[rt].l,l,mid,kth,x);
	else
		return query(tree[rt].r,mid+1,r,kth-tree[tree[rt].l].sum,x);
}

void modify(int rt,int l,int r,int point,int val,int x){
	if (l==r) {
		tree[rt].sum+=val;
		return;
	}
	int mid=(l+r)>>1;
	if (tree[rt].l==0) tree[rt].l=init(l,mid,1,x);
	if (tree[rt].r==0) tree[rt].r=init(mid+1,r,1,x);
	if (point<=mid)
		modify(tree[rt].l,l,mid,point,val,x);
	else 
		modify(tree[rt].r,mid+1,r,point,val,x);
	pushup(rt);
}


/*segmenttree for last line*/

int Ltree[600010*4];

void Lpushup(int rt){
	Ltree[rt]=Ltree[rt<<1]+Ltree[rt<<1|1];
}

void Linit(int rt,int l,int r){
	if (l==r){
		if (l<=n)
			Ltree[rt]=1;
		else 
			Ltree[rt]=0;
		return;
	}
	int mid=(l+r)>>1;
	Linit(rt<<1,l,mid);
	Linit(rt<<1|1,mid+1,r);
	Lpushup(rt);
}

int Lquery(int rt,int l,int r,int kth){
	if (l==r)return l;
	int mid=(l+r)>>1;
	if (Ltree[rt<<1]>=kth)
		return Lquery(rt<<1,l,mid,kth);
	else
		return Lquery(rt<<1|1,mid+1,r,kth-Ltree[rt<<1]);
}

void Lmodify(int rt,int l,int r,int point,int val){
	if (l==r) {
		Ltree[rt]+=val;
		return;
	}
	int mid=(l+r)>>1;
	if (point<=mid)
		Lmodify(rt<<1,l,mid,point,val);
	else 
		Lmodify(rt<<1|1,mid+1,r,point,val);
	Lpushup(rt);
}
/*segend*/


ll work(int x,int y){
	if (y==m){
		int tmp=Lquery(1,1,q+m,x);
		Lmodify(1,1,q+m,tmp,-1);
		ll res=lastl[tmp];
		tail++;
		lastl[tail]=res;
		Lmodify(1,1,q+m,tail,1);
		return res;
	}
	
	ans=query(root[x],1,q+m,y,x);
	//ll res=link[ans];
	ll res=ANS;
	modify(root[x],1,q+m,ans,-1,x);
	//BITend++;
	int tmp=Lquery(1,1,q+m,x);
	Lmodify(1,1,q+m,tmp,-1);
	//link[BITend]=lastl[tmp];
	modify(root[x],1,q+m,BITend,1,x);
	tail++;
	lastl[tail]=res;
	Lmodify(1,1,q+m,tail,1);
	return res;
}

int main(){
	freopen("15.in","r",stdin);
	freopen("1512.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=n;i++)
		root[i]=init(1,q+m,1,i);
	//init(1,1,q+m);
	Linit(1,1,q+m);
	for (int i=1;i<=m-1;i++)
		link[i]=i;
	BITend=m-1;
	for (int i=1;i<=n;i++)
		lastl[i]=(ll)i*(ll)m;
	tail=n;
	//printf("%d",ans);
	for (int i=1;i<=q;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		printf("%lld\n",work(x,y));
	}
}
		

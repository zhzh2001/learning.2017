#include<stdio.h>
#include<algorithm>
using namespace std;
typedef long long ll;
int ans;
int BITend,head,tail;
int tree[600010*4];

ll lastl[600010],link[600010];

ll traw[510][50010];
struct QUERY{
	int x,y;
}qry[300010];
int usedx[300010],linknum,n,m,q;

ll work(int qryid){
	ll x=link[qry[qryid].x];
	if (qry[qryid].y==m){
		ll res=lastl[qry[qryid].x];
		for (int i=qry[qryid].x;i<=n-1;i++)
			lastl[i]=lastl[i+1];
		lastl[n]=res;
		return res;
	}
	ll res=traw[x][qry[qryid].y];
	for (int i=qry[qryid].y;i<=m-2;i++)
		traw[x][i]=traw[x][i+1];
	traw[x][m-1]=lastl[qry[qryid].x];
	for (int i=qry[qryid].x;i<=n-1;i++)
		lastl[i]=lastl[i+1];
	lastl[n]=res;
	return res;
}

void bh(int linknum,int usex){
	for (int i=1;i<=m-1;i++)
		traw[linknum][i]=(ll)(usex-1)*(ll)m+(ll)i;
}

void pushup(int rt){
	tree[rt]=tree[rt<<1]+tree[rt<<1|1];
}

void init(int rt,int l,int r){
	if (l==r){
		if (l<=m-1)
			tree[rt]=1;
		else 
			tree[rt]=0;
		return;
	}
	int mid=(l+r)>>1;
	init(rt<<1,l,mid);
	init(rt<<1|1,mid+1,r);
	pushup(rt);
}

int query(int rt,int l,int r,int kth){
	//printf("query:%d %d %d %d %d\n",rt,l,r,kth,tree[rt]);
	if (l==r)return l;
	int mid=(l+r)>>1;
	if (tree[rt<<1]>=kth)
		return query(rt<<1,l,mid,kth);
	else
		return query(rt<<1|1,mid+1,r,kth-tree[rt<<1]);
}

void modify(int rt,int l,int r,int point,int val){
	if (l==r) {
		tree[rt]+=val;
		return;
	}
	int mid=(l+r)>>1;
	if (point<=mid)
		modify(rt<<1,l,mid,point,val);
	else 
		modify(rt<<1|1,mid+1,r,point,val);
	pushup(rt);
}


ll work1(int x,int y){
	if (y==m){
		int res=lastl[head++];
		tail++;
		lastl[tail]=res;
		return res;
	}
	
	ans=query(1,1,q+m,y);
	//printf("point=%d y=%d\n",ans,y);
	ll res=link[ans];
	modify(1,1,q+m,ans,-1);
	BITend++;
	link[BITend]=lastl[head++];
	modify(1,1,q+m,BITend,1);
	lastl[++tail]=res;
	/*printf("BIT:\n");
	for (int i=1;i<=BITend;i++)
		printf("1~%d=%d\n",i,sum(i));*/
	return res;
}

int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	bool flag=true;
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=q;i++){
		scanf("%d%d",&qry[i].x,&qry[i].y);
		if (qry[i].x!=1)
			flag=false;
		usedx[i]=qry[i].x;
	}
	if (!flag){
	sort(usedx+1,usedx+1+q);
	for (int i=1;i<=q;i++)
		if (!link[usedx[i]]){
			link[usedx[i]]=++linknum;
			bh(linknum,usedx[i]);
		}
	for (int i=1;i<=n;i++)
		lastl[i]=(ll)i*(ll)m;
	for (int i=1;i<=q;i++){
		
		printf("%lld\n",work(i));
		
	}
	}else{
		init(1,1,q+m);
		for (int i=1;i<=m-1;i++)
			link[i]=i;
		BITend=m-1;
		head=1,tail=n;
		for (int i=1;i<=n;i++)
			lastl[i]=(ll)i*(ll)m;
		//printf("%d",ans);
		for (int i=1;i<=q;i++){
			int x,y;
			printf("%lld\n",work1(qry[i].x,qry[i].y));
		}
	}
}

#include<stdio.h>
#include<algorithm>
using namespace std;

int tree[600010],n,m,q,ans;
int link[600010],BITend,lastl[600010],head,tail;

void add(int x,int val){
	//printf("add:%d\n",x);
	for (;x<=(q+m+10);x+=x&-x){ans++;tree[x]+=val;}
}

int sum(int x){
	int res=0;
	for(;x;x-=x&-x) res+=tree[x];
	return res;
}

int work(int x,int y){
	if (y==m){
		int res=lastl[head++];
		tail++;
		lastl[tail]=res;
		return res;
	}
	
	int l=1,r=BITend,ans=0;
	while(l<=r){
		int mid=(l+r)>>1;
		if (sum(mid)<y){
			l=mid+1;
		}else{
			ans=mid;
			r=mid-1;
		}
	}
	int res=link[ans];
	add(ans,-1);
	BITend++;
	link[BITend]=lastl[head++];
	add(BITend,1);
	lastl[++tail]=res;
	/*printf("BIT:\n");
	for (int i=1;i<=BITend;i++)
		printf("1~%d=%d\n",i,sum(i));*/
	return res;
}

int main(){
	freopen("15.in","r",stdin);
	freopen("1512.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	for (int i=1;i<=m-1;i++){
		link[i]=i;
		add(i,1);
	}
	BITend=m-1;
	head=1,tail=n;
	for (int i=1;i<=n;i++)
		lastl[i]=i*m;
	//printf("%d",ans);
	for (int i=1;i<=q;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		printf("%d\n",work(x,y));
	}
}
		

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#define MAXN 20
using namespace std;
//typedef long long LL;
int mp[MAXN][MAXN];
int cost[10010],lc[10010][MAXN],ans,mint;
int used[MAXN],n,m,lt;
void df1(int p,int l,int tmp){
	used[p]=1;
	ans+=tmp*l;
	for(int i=1;i<=n;i++){
		if(!used[i]&&mp[p][i]!=-1){
			df1(i,l+mp[p][i],mp[p][i]);
		}
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d %d",&n,&m);
	int f,t,v;
	memset(mp,-1,sizeof(mp));
	for(int i=1;i<=m;i++){
		scanf("%d %d %d",&f,&t,&v);
		if(mp[f][t]==-1||mp[f][t]>v){
			mp[f][t]=v;
			mp[t][f]=v;
		}
	}
	/*for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			cout<<mp[i][j]<<" ";
		}
		cout<<endl;
	}*/
	int minans,minl,minc;
	minans=-1;
	/*if(m==n-1){
		for(int i=1;i<=n;i++){
			memset(used,0,sizeof(used));
			ans=0;
			//memset(sum,0,sizeof(sum));
			df1(i,0,0);
			if(mins==-1||mins>ans){
				mins=ans;
			}
		}	
	}*/
	//else{
		int stp=(1<<n),tag;
		for(int sta=1;sta<=n;sta++){
			//cout<<"sta="<<sta<<endl;
			memset(cost,-1,sizeof(cost));
			memset(lc,0,sizeof(lc));
			cost[1<<(sta-1)]=0;
			for(int i=0;i<stp-1;i++){
			//	cout<<"i="<<i<<endl;
			//	cout<<"cost="<<cost[i]<<endl;
				//for(int j=1;j<=n;j++) cout<<lc[i][j]<<" ";
				//cout<<endl;
				mint=-1;
				if(cost[i]==-1) continue;
				for(int j=1;j<=n;j++){
			//		cout<<"---j="<<j<<endl;
					mint=-1;
					minl=-1;
					if(((1<<(j-1))&i)==0){
					//	cout<<"hehehe\n";
						for(int k=1;k<=n;k++){
							if(((1<<(k-1))&i)&&mp[j][k]!=-1){
							//	cout<<"from"<<k<<" -> "<<j<<endl;
									lt=lc[i][k]+1;
									//cout<<"lt="<<lt<<endl;
									if(lt*mp[j][k]<mint||mint==-1){
										mint=lt*mp[j][k];
										minl=lt;
										//cout<<"mint="<<mint<<endl;
									}
							}
						}
					}
					if(mint==-1) continue;
				//	cout<<"mint="<<mint<<endl;
					minc=cost[i]+mint;
					int targ=i|(1<<(j-1));
				//	cout<<"minc="<<minc<<endl;
				//cout<<"targ="<<targ<<endl;
					if(cost[targ]==-1||cost[targ]>minc){
						cost[targ]=minc;
						for(int k=1;k<=n;k++){
							lc[targ][k]=lc[i][k];
						}
						lc[targ][j]=minl;
					}
				}
			}
			if(minans==-1||minans>cost[stp-1]) minans=cost[stp-1];
		}
		printf("%d\n",minans);
	//}
	return 0;
}

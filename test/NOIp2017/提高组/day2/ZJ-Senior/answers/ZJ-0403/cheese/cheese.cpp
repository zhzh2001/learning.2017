#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#define MAXN 1100
using namespace std;
typedef long long LL;
LL x[MAXN],y[MAXN],z[MAXN],r,h;
int n,T,point[MAXN*2],last[MAXN],yest[MAXN*2],total,used[MAXN];
void adde(int f,int t){
	//cout<<"f="<<f<<" t="<<t<<endl;
	point[++total]=t;
	yest[total]=last[f];
	last[f]=total;
	
	point[++total]=f;
	yest[total]=last[t];
	last[t]=total;
}
void init(){
	memset(used,0,sizeof(used));
	total=0;
	memset(last,0,sizeof(last));
}
void dfs(int p){
	used[p]=1;
	for(int i=last[p];i;i=yest[i]){
		if(!used[point[i]]){
			dfs(point[i]);
		}
	}
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	LL td,limt;
	while(T--){
		init();
		scanf("%d %I64d %I64d",&n,&h,&r);
		limt=r*r*4;
		for(int i=1;i<=n;i++){
			scanf("%I64d %I64d %I64d",&x[i],&y[i],&z[i]);
			if(z[i]<=r) adde(i,n+10);
			if(h-z[i]<=r) adde(i,n+11);
		}
		for(int i=1;i<=n;i++){
			for(int j=i+1;j<=n;j++){
				td=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
				if(td<=limt){
					adde(i,j);
				}
			}
		}
		dfs(n+10);
		if(used[n+11]){
			printf("Yes\n");
		}
		else{
			printf("No\n");
		}
	}
	return 0;
}

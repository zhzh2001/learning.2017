#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

// QUICK_IO BEGIN HERE
inline unsigned get_u()
{
	char c; unsigned r=0;
	while(isspace(c=getchar()));
	for(;isdigit(c);c=getchar())
	{
		r=(r<<3)+(r<<1)+c-'0';
	}
	return r;
}
// QUICK_IO END HERE

static const int LIM=100000000;
static int N,M,Q;

namespace PartI // Point 1 ~ 6
{
	int a[1005][1005];
	void solve();
}

void PartI::solve()
{
	range(i,1,N+1) range(j,1,M+1) a[i][j]=(i-1)*M+j;
	while(Q--)
	{
		int x=get_u(),y=get_u(),cur=a[x][y];
		printf("%d\n",cur);
		range(i,y,M) a[x][i]=a[x][i+1];
		range(i,x,N) a[i][M]=a[i+1][M];
		a[N][M]=cur;
	}
}

namespace PartII // Point 11 ~ 14
{
	class splay;
	void solve();
}

class PartII::splay
{
	struct __iter
	{
		int key,siz;
		__iter *fat,*son[2];
		__iter(const int&k=0)
		{
			key=k,siz=1,fat=son[0]=son[1]=NULL;
		}
	}*rt;
	inline int __phase(__iter *x)
	{
		return x->fat?x->fat->son[1]==x:-1;
	}
	inline int __size(__iter *x) {return x?x->siz:0;}
	inline void __update(__iter *x)
	{
		x->siz=1;
		if(x->son[0]) x->siz+=x->son[0]->siz;
		if(x->son[1]) x->siz+=x->son[1]->siz;
	}
	void __rotate(__iter *x,const bool&rig)
	{
		__iter *y=x->son[!rig],*z=y->son[rig];
		y->son[rig]=x,y->fat=x->fat;
		if(iterator f=x->fat) f->son[__phase(x)]=y;
		x->son[!rig]=z,x->fat=y;
		if(z) z->fat=x;
		if(x==rt) rt=y;
		__update(x),__update(y);
	}
	void __splay(__iter *x,__iter *tar)
	{
		for(;;)
		{
			__iter *y=x->fat;
			if(y==tar) return;
			__iter *z=y->fat;
			if(z==tar) return __rotate(y,!__phase(x));
			int xp=__phase(x),yp=__phase(y);
			xp^yp?__rotate(y,!xp):__rotate(z,!yp);
		}
	}
	__iter* __kth(const int&k)
	{
		__iter *x=rt;
		for(int cur=k;cur!=__size(x->son[0])+1;)
		{
			if(cur<__size(x->son[0])+1) x=x->son[0];
			else cur-=__size(x->son[0])+1,x=x->son[1];
		}
		return x;
	}
	public:
	typedef __iter* iterator;
	void clear() {rt=NULL;}
	int pop(const int&k)
	{
		__splay(__kth(k),NULL);
		iterator x=rt;
		if(!x->son[1])
		{
			rt=x->son[0],rt->fat=NULL;
			int ret=x->key; delete x; return ret;
		}
		iterator y=x->son[1];
		for(;y->son[0];y=y->son[0]);
		__splay(y,x);
		y->fat=NULL,y->son[0]=x->son[0],rt=y;
		if(x->son[0]) x->son[0]->fat=y;
		__update(y);
		int ret=x->key; delete x; return ret;
	}
	void push(const int&key)
	{
		if(!rt) {rt=new __iter(key); return;}
		iterator x=rt;
		for(;x->son[1];x=x->son[1]);
		__splay(x,NULL);
		iterator y=new __iter(key);
		y->fat=x,x->son[1]=y;
		__update(x);
	}
}seq;

void PartII::solve()
{
	seq.clear();
	range(i,1,M+1) seq.push(i);
	while(Q--)
	{
		int x=get_u(),y=get_u(),ans=seq.pop(y);
		printf("%d\n",ans),seq.push(ans);
	}
}

int main()
{
	freopen("phalanx.in" ,"r",stdin );
	freopen("phalanx.out","w",stdout);
	N=get_u(),M=get_u(),Q=get_u();
	if(N==1) return PartII::solve(),0;
	return PartI::solve(),0;
}
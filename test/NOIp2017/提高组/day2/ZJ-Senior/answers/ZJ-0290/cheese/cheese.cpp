#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
#define forin(i,p) for(typeof(p.end()) __ITER__=p.begin(),__END__=p.end();__ITER__!=__END__;++__ITER__) if(bool __FLAG__=1) for(typeof(*__ITER__) i=*__ITER__;__FLAG__;__FLAG__=0)
using namespace std;

static const double eps=1e-3;

static int T,N,H,R;
bool vis[1005];
int x[1005],y[1005],z[1005];
queue<int> que;
vector<int> edg[1005];

int main()
{
	freopen("cheese.in" ,"r",stdin );
	freopen("cheese.out","w",stdout);
	for(scanf("%d",&T);T--;)
	{
		scanf("%d%d%d",&N,&H,&R);
		range(i,1,N+1) scanf("%d%d%d",x+i,y+i,z+i);
		memset(edg,0,sizeof edg);
		range(i,1,N+1) if(z[i]<=R)
		{
			edg[0].push_back(i),edg[i].push_back(0);
		}
		range(i,1,N+1) if(z[i]+R>=H)
		{
			edg[N+1].push_back(i),edg[i].push_back(N+1);
		}
		range(i,1,N) range(j,i+1,N+1)
		if(pow(x[i]-x[j],2)+pow(y[i]-y[j],2)+pow(z[i]-z[j],2)<double(4)*R*R+eps)
		{
			edg[i].push_back(j),edg[j].push_back(i);
		}
		memset(vis,0,sizeof vis),vis[0]=1;
		for(que.push(0);!que.empty();que.pop())
		{
			bool flag=0;
			int x=que.front();
			forin(y,edg[x]) if(!vis[y])
			{
				vis[y]=1;
				if(y==N+1) {for(flag=1;!que.empty();que.pop()); break;}
				que.push(y);
			}
			if(flag) break;
		}
		puts(vis[N+1]?"Yes":"No");
	}
	return 0;
}
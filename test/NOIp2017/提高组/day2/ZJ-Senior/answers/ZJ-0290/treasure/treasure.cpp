#include <bits/stdc++.h>
#define range(i,c,o) for(register int i=(c);i<(o);++i)
using namespace std;

static const int INF=0x7F7F7F7F;

static int N,M;
bool vis[15];
int a[15][15],dep[15];

int main()
{
	freopen("treasure.in" ,"r",stdin );
	freopen("treasure.out","w",stdout);
	memset(a,0x7F,sizeof a);
	for(scanf("%d%d",&N,&M);M--;)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w),--u,--v;
		a[u][v]=min(a[u][v],w);
		a[v][u]=min(a[v][u],w);
	}
	int ans=INF;
	range(rt,0,N)
	{
		memset(vis,0,sizeof vis),vis[rt]=dep[rt]=1;
		int sum=0;
		range(T,1,N)
		{
			int x,y,val=INF;
			range(i,0,N) if(!vis[i]) range(j,0,N) if(vis[j])
			{
				if(1LL*a[j][i]*dep[j]<val) val=a[j][i]*dep[j],x=j,y=i;
			}
			vis[y]=1,dep[y]=dep[x]+1,sum+=val;
		}
		ans=min(ans,sum);
	}
	return printf("%d\n",ans),0;
}
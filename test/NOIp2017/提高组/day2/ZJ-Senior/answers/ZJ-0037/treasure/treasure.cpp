#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
#include<queue>
#define INF (1<<23)
using namespace std;

inline int read(){
	   int x=0,tmp=1;
	   char c=getchar();
	   while(c>'9'||c<'0'){
		   if(c=='-')tmp=-1;
		   c=getchar();
	   }
	   while(c>='0'&&c<='9'){
		   x=x*10+c-'0';
		   c=getchar();
	   }
	   return x*tmp;
}
struct line{
	int to,next,power;
};  line lx[2020];
queue <int>q;
int n,m,lnum,x,y,w,ans,dis[20],head[20];
bool vis[20];
inline void addl(int x,int y,int w){
	lnum++; lx[lnum].to=y; lx[lnum].power=w; lx[lnum].next=head[x]; head[x]=lnum;
}
inline int spfa(int k){
	for(int i=1;i<=n;++i)dis[i]=INF,vis[i]=false;
	dis[k]=0; vis[k]=true; q.push(k);
	while(!q.empty()){
		int lk=q.front(); q.pop();
		int t=head[lk];
		while(t!=0){
			int b=lx[t].to;
			if(dis[b]>dis[lk]+1){
				dis[b]=dis[lk]+1;
				if(!vis[b]){
					vis[b]=true; q.push(b);
				}
			}
			t=lx[t].next;
		}
	} 
	int tot=0;
	for(int i=1;i<=n;++i)tot+=dis[i];
	return tot;
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=read(); m=read(); lnum=0;
	for(int i=1;i<=m;++i){
		x=read(); y=read(); w=read();
		addl(x,y,w); addl(y,x,w);
	}
	ans=INF;
	for(int i=1;i<=n;++i)ans=min(ans,spfa(i));
	printf("%d",w*ans);
	return 0;
}


#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;

inline int read(){
	   int x=0,tmp=1;
	   char c=getchar();
	   while(c>'9'||c<'0'){
		   if(c=='-')tmp=-1;
		   c=getchar();
	   }
	   while(c>='0'&&c<='9'){
		   x=x*10+c-'0';
		   c=getchar();
	   }
	   return x*tmp;
}
int n,m,q,bm,mark[600030],c[600030];
ll a[1010][1010],doqx[550],doqy[550];
inline int lowbit(int k){return k&(-k);}
inline void addnum(int k,int b){
	int lk=k;
	while(lk<=bm){
		c[lk]+=b;
		lk+=lowbit(lk);
	}
}
inline int getsum(int k){
	int lk=k,ans=0;
	while(lk>0){
		ans+=c[lk];
		lk-=lowbit(lk);
	}
	return ans;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); q=read();
	if(n!=1){
		/*for(int i=1;i<=n;++i)
			for(int j=1;j<=m;++j)a[i][j]=(i-1)*m+j;
		for(int i=1;i<=q;++i){
			int qx=read(),qy=read();
			printf("%lld\n",a[qx][qy]);
			int lins=a[qx][qy];
			for(int j=qy+1;j<=m;++j)a[qx][j-1]=a[qx][j];
			for(int j=qx+1;j<=n;++j)a[j-1][m]=a[j][m];
			a[n][m]=lins;
		}*/
		for(int i=1;i<=q;++i){
			doqx[i]=read(); doqy[i]=read();
			ll nowx=doqx[i],nowy=doqy[i];
			for(int j=i-1;j>0;--j){
				if(nowx==n&&nowy==m){
					nowx=doqx[j]; nowy=doqy[j];
					continue;
				}
				if(nowy==m&&nowx>=doqx[j])nowx=nowx+1;
				if(nowx==doqx[j]&&nowy>=doqy[j])nowy=nowy+1;
			}
			//cout<<nowx<<" "<<nowy<<endl;
			printf("%lld\n",(nowx-1)*m+nowy);
		}
	}
	if(n==1){
		bm=q+m; int nowfar=m;
		for(int i=1;i<=m;++i)mark[i]=i;
		for(int i=1;i<=m;++i)addnum(i,1);
		for(int i=1;i<=q;++i){
			int qx=read(),qy=read();
			int lo=0,ro=nowfar,ans=lo;
			//cout<<"sb";
			while(lo<=ro){
				//cout<<lo<<" "<<ro<<endl;
				int mid=(lo+ro)>>1;
				if(getsum(mid)<qy){
					ans=mid; lo=mid+1;
				}else ro=mid-1;
			}
			ans++; //cout<<ans;
			printf("%d\n",mark[ans]);
			nowfar++; mark[nowfar]=mark[ans]; //mark[ans]=0;
			//for(int i=1;i<=nowfar;++i)if(mark[i]!=0)cout<<mark[i]<<" "; cout<<endl;
			addnum(ans,-1); addnum(nowfar,1);
		}
	}
	return 0;
}


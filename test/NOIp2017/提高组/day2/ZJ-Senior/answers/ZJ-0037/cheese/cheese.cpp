#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<cstring>
#include<cmath>
#define ll long long
using namespace std;

inline ll read(){
	   ll x=0,tmp=1;
	   char c=getchar();
	   while(c>'9'||c<'0'){
		   if(c=='-')tmp=-1;
		   c=getchar();
	   }
	   while(c>='0'&&c<='9'){
		   x=x*10+c-'0';
		   c=getchar();
	   }
	   return x*tmp;
}
int T,n;
ll h,r,fa[1020],ax[1020],ay[1020],az[1020];
inline void getfa(int k){
	if(k!=fa[k])getfa(fa[k]);
	fa[k]=fa[fa[k]];
}
inline double getdis(int i,int j){
	ll lins=(ax[i]-ax[j])*(ax[i]-ax[j])+(ay[i]-ay[j])*(ay[i]-ay[j])+(az[i]-az[j])*(az[i]-az[j]);
	return sqrt(lins);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=read();
	while(T--){
		n=read(); h=read(); r=read();
		fa[n+2]=n+2; fa[n+1]=n+1;//n+1 up ; n+2 down;
		for(int i=1;i<=n;++i){
			ax[i]=read(); ay[i]=read(); az[i]=read();
			fa[i]=i;
			if(az[i]+r>=h){
				getfa(i); getfa(n+1); 
				if(fa[i]!=fa[n+1])fa[fa[i]]=fa[n+1];
			}
			if(az[i]-r<=0){
				getfa(i); getfa(n+2);
				if(fa[i]!=fa[n+2])fa[fa[i]]=fa[n+2];
			}
			for(int j=1;j<i;++j){
				if(getdis(i,j)<=2*r){
					getfa(i); getfa(j);
					if(fa[i]!=fa[j])fa[fa[i]]=fa[j];
				}
			}
		}
		for(int i=1;i<=n+2;++i)getfa(i);
		if(fa[n+1]==fa[n+2])printf("YES\n");
		else printf("NO\n");
	}
	return 0;
}


#include<bits/stdc++.h>
using namespace std;

int n,m,q,x,y,tr[200003],pos[100003],a[1003][1003];

void add(int x){
	for(;x && x<=m;x+=x&-x) tr[x]++;
}
int query(int x){
	int ans=0;
	for(;x;x-=x&-x) ans+=tr[x];
	return ans;
}
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n==1){
		for(int i=1;i<=m;i++) pos[i]=i;
		for(int i=1;i<=q;i++){
			scanf("%d%d",&x,&x);
			for(int j=1;j<=m;j++)
				if(pos[j]-query(j)==x){
					y=j;
					break;
				}
			printf("%d\n",y);
			m++;
			add(pos[y]);
			pos[y]=m;
		}
	}
	else{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				a[i][j]=(i-1)*m+j;
		for(int i=1;i<=q;i++){
			scanf("%d%d",&x,&y);
			int t=a[x][y];
			printf("%d\n",t);
			for(int j=y;j<m;j++) a[x][j]=a[x][j+1];
			for(int j=x;j<n;j++) a[j][m]=a[j+1][m];
			a[n][m]=t;
		}
	}
	return 0;
}

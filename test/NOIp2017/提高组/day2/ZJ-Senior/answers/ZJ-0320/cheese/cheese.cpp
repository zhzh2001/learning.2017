#include<bits/stdc++.h>
using namespace std;

bool f,h[1003];
int T,n,m,hi,r,tot,fst[1003],b[1003];
struct node{
	int x,y,z;
}a[1003];
struct edge{
	int x,nx;
}e[2000003];

bool search(int x){
	h[x]=1;
	if(a[x].z+r/2>=hi) return 1;
	for(int i=fst[x];i;i=e[i].nx)
		if(!h[e[i].x] && search(e[i].x)) return 1;
	return 0;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	for(int ii=1;ii<=T;ii++){
		memset(h,0,sizeof(h));
		memset(fst,0,sizeof(fst));
		f=m=tot=0;
		scanf("%d%d%d",&n,&hi,&r);
		r*=2;
		for(int i=1;i<=n;i++){
			scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
			if(a[i].z<=r/2) b[++tot]=i;
			for(int j=1;j<i;j++)
				if(sqrt((a[i].x-a[j].x)*(a[i].x-a[j].x)+(a[i].y-a[j].y)*(a[i].y-a[j].y)+(a[i].z-a[j].z)*(a[i].z-a[j].z))<=r){
					e[++m].nx=fst[i],fst[i]=m,e[m].x=j;
					e[++m].nx=fst[j],fst[j]=m,e[m].x=i;
				}
		}
		for(int i=1;i<=tot;i++)
			if(!h[b[i]] && search(b[i])){
				printf("Yes\n");
				f=1;
				break;
			}
		if(!f) printf("No\n");
	}
	return 0;
}

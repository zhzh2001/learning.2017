#include<bits/stdc++.h>
using namespace std;

bool h[15];
int n,m,x,y,v,tot,fst[15],dis[15],pos[15][15];
long long mn=1e18;
struct node{
	double ad;
	int num;
}a[15];
struct edge{
	int x,nx;
	long long v;
}e[303];

bool cmp(node a,node b){
	return a.ad>b.ad;
}
void search(int row,long long c){
	if(c>=mn) return ;
	if(row==n){
		if(c<mn) mn=c;
		return ;
	}
	for(int i=1;i<=n;i++)
		if(!h[a[i].num]){
			h[a[i].num]=1;
			long long tmp=1e18;
			for(int j=fst[a[i].num];j;j=e[j].nx)
				if(h[e[j].x] && dis[e[j].x]*e[j].v<tmp){
					dis[a[i].num]=dis[e[j].x]+1;
					tmp=dis[e[j].x]*e[j].v;
				}
			if(tmp<1e18) search(row+1,c+tmp);
			h[a[i].num]=0;
		}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&v);
		if(!pos[x][y]){
			e[++tot].x=y,e[tot].nx=fst[x],fst[x]=tot;
			pos[x][y]=tot;
			e[++tot].x=x,e[tot].nx=fst[y],fst[y]=tot;
			pos[y][x]=tot;
			e[tot].v=e[tot-1].v=v;
		}
		else if(v<e[pos[x][y]].v) e[pos[x][y]].v=e[pos[y][x]].v=v;
	}
	for(int i=1;i<=n;i++){
		a[i].num=i;
		long long sum=0;
		int tmp=0;
		for(int j=fst[i];j;j=e[j].nx) tmp++,sum+=e[j].v;
		a[i].ad=sum*1.0/tmp;
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		dis[a[i].num]=h[a[i].num]=1;
		search(1,0);
		h[a[i].num]=0;
	}
	printf("%lld",mn);
	return 0;
}

#include<cstdio>
#include<algorithm>
#include<queue>
#include<cstring>
using namespace std;
const int inf=0x3f3f3f3f;
int read()
{
	int x=0;int f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
struct node{int to,next,w;}num[200];
int n,m,g[20][20],inq[20],dis[20],u,v,w,Min,mm,f[20][5000],ans,Max,cnt,head[20];
void add(int x,int y,int w)
{num[++cnt].to=y;num[cnt].next=head[x];num[cnt].w=w;head[x]=cnt;}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	
	n=read();m=read();
	for (int i=1;i<=m;i++)
	{
		u=read();v=read();w=read();
		if (u==v) continue;
		if (g[u][v]) g[u][v]=min(g[u][v],w);else g[u][v]=w;
		if (g[v][u]) g[v][u]=min(g[v][u],w);else g[v][u]=w;
	}
	for (int i=1;i<=n;i++)
	  for (int j=1;j<=n;j++)
	     if (g[i][j]) add(i,j,g[i][j]);
	Max=1<<n;
	memset(f,inf,sizeof(f));
	for (int i=1;i<=n;i++) f[1][1<<(i-1)]=0;
	for (int i=2;i<=n;i++)
    {
       for (int j=(1<<i)-1;j<Max;j++)
	     for (int k=(j-1)&j;k!=0;k=(k-1)&j)
	     if (f[i-1][k]!=inf)
		 {
		 	int sum=0,fl=0;
		 	for (int z=1;z<=n;z++) 
		 	  if (((1<<(z-1))&j)&&((1<<(z-1))&k)==0)
		 	  {
		 	  	 mm=inf;
		 	  	 for (int x=head[z];x;x=num[x].next)
		 	  	   if (((1<<(num[x].to-1))&k)) mm=min(mm,g[z][num[x].to]);
		 	  	 if (mm==inf) {fl=1;break;}else sum+=mm;
			  }
			if (fl==1) continue;
			else f[i][j]=min(f[i][j],f[i-1][k]+sum*(i-1));
	   	 }
	}
	ans=inf;
	for (int i=1;i<=n;i++)
	  ans=min(ans,f[i][Max-1]);
	printf("%d\n",ans);
	
	fclose(stdin);fclose(stdout);
	return 0;
}

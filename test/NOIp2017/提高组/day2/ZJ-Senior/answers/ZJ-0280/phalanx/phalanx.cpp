#include<cstdio>
using namespace std;
typedef long long ll;
ll read()
{
	ll x=0;int f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
ll n,m,q,tmp,a[1000005],x,y,id;
ll calc_id(ll x,ll y)
{return (ll)(x-1)*m+y;}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	
	n=read(),m=read(),q=read();
	  for (int i=1;i<=n;i++)
	    for (int j=1;j<=m;j++)
	      tmp=calc_id(i,j),a[tmp]=tmp;
	  while (q--)
	  {
		x=read();y=read();
		id=calc_id(x,y);
		printf("%lld\n",a[id]);
		tmp=a[id];
		for(ll i=id;i<x*m;i++) a[i]=a[i+1];
		for(ll i=x*m;i<n*m;i+=m) a[i]=a[i+m]; 
		a[n*m]=tmp;
	  }	
	  fclose(stdin);fclose(stdout);
	return 0;
}

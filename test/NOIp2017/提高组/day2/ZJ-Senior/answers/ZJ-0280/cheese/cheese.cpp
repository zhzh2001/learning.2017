#include<cstdio>
using namespace std;
typedef long long ll;
ll read()
{
	ll x=0;int f=1;char ch=getchar();
	while (ch<'0'||ch>'9') {if (ch=='-') f=-1;ch=getchar();}
	while (ch>='0'&&ch<='9') x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return x*f;
}
int T,n,f[1010];
ll h,r,x[1010],y[1010],z[1010];
int find(int x){return (x==f[x])?x:f[x]=find(f[x]);}
void merge(int x,int y)
{
	int xx=find(x);
	int yy=find(y);
	if (xx!=yy) f[xx]=yy;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	
	T=read();
	while (T--)
	{
		n=read();h=read();r=read();
		for (int i=1;i<=n+2;i++) f[i]=i;
		for (int i=1;i<=n;i++) 
		{
		  x[i]=read(),y[i]=read(),z[i]=read();
		  if (z[i]<=r) merge(i,n+1);
		  if (h-z[i]<=r) merge(i,n+2);	
		}
		if (find(n+1)==find(n+2)) {printf("Yes\n");continue;}
	    for (int i=1;i<n;i++)
	      for (int j=i+1;j<=n;j++)
	      {
	      	 ll tmp=(ll)(x[i]-x[j])*(x[i]-x[j])+(ll)(y[i]-y[j])*(y[i]-y[j]);
			 ll ttt=(ll)4*r*r-(ll)(z[i]-z[j])*(z[i]-z[j]);
	      	 if (tmp<=ttt) merge(i,j);
		  }
		if (find(n+1)==find(n+2)) {printf("Yes\n");continue;}
		printf("No\n");
	}
	
	fclose(stdin);fclose(stdout);
	return 0;
}

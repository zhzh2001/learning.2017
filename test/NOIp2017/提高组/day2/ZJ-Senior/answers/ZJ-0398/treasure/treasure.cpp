#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
using namespace std;
const int MAXN=12;
const int INF=0x3f3f3f3f;
const int MAXS=5200;
int n,m,S,ans=INF;
int G[MAXN+5][MAXN+5];
int p2[MAXN+5],lg2[MAXS+5];
int readint()
{
	int res;char ch;
	while(!isdigit(ch=getchar()));
	res=ch-'0';
	while(isdigit(ch=getchar()))res=res*10-'0'+ch;
	return res;
}
int rec[MAXN+5][MAXN+5],ed[MAXN+5],cnt,st,now,f[MAXS+5],g[MAXN+5];
void dfs(int dep,int p,int k)
{
	if(now+dep*f[S^st]>=ans)return;
	if(cnt==n){ans=now;return;}
	if(p>ed[dep]){++dep;p=1;k=0;}
	if(!ed[dep])return;
	int u=rec[dep][p];
	for(int i=k;i<n;++i)
	{
		if((st&p2[i])||G[u][i]>=INF)continue;
		rec[dep+1][++ed[dep+1]]=i;
		now+=dep*G[u][i];
		++cnt;
		st^=p2[i];
		dfs(dep,p,k+1);
		--ed[dep+1];
		now-=dep*G[u][i];
		--cnt;
		st^=p2[i];
	}
	dfs(dep,p+1,0);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=readint();m=readint();S=(1<<n)-1;
	for(int i=0,j=1;i<=n;++i,j<<=1)p2[i]=j,lg2[j]=i;
	for(int i=0;i<n;++i)
	{
		for(int j=0;j<n;++j)G[i][j]=INF;
		g[i]=INF;
	}
	for(int i=1,u,v,d;i<=m;++i)
	{
		u=readint()-1;v=readint()-1;d=readint();
		G[u][v]=G[v][u]=min(G[u][v],d);
		g[u]=min(g[u],d);
		g[v]=min(g[v],d);
	}
	for(int i=1,o;i<=S;++i)
	{
		o=i&(-i);
		f[i]=f[i^o]+g[lg2[o]];
	}
	for(int i=0;i<n;++i)
	{
		st=p2[i];cnt=1;rec[1][ed[1]=1]=i;
		dfs(1,1,0);
	}
	printf("%d",ans);
	return 0;
}

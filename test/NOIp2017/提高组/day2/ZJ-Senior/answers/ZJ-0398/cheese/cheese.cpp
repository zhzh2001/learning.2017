#include<iostream>
#include<queue>
#include<algorithm>
#include<cstring>
#include<cstdio>
using namespace std;
#define ll long long
const int MAXN=1000;
int t;
int readint()
{
	int res=0,fl=1;char ch;
	while(!isdigit(ch=getchar())&&ch!='-');
	if(ch=='-')fl=-1;else res=ch-'0';
	while(isdigit(ch=getchar()))res=res*10-'0'+ch;
	return fl*res;
}
int n,h,r,lim,x[MAXN+10],y[MAXN+10],z[MAXN+10],vis[MAXN+10];
queue<int> Q;
inline ll sqr(ll x){return x*x;}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	t=readint();
	while(t--)
	{
		n=readint();h=readint();r=readint();lim=sqr(r+r);
		for(int i=1;i<=n;++i)
		{
			x[i]=readint();
			y[i]=readint();
			z[i]=readint();
		}
		for(int i=1;i<=n;++i)
		{
			if(abs(z[i])<=r)
			{
				Q.push(i);
				vis[i]=true;
			}
			else vis[i]=false;
		}
		while(!Q.empty())
		{
			int u=Q.front();Q.pop();
			for(int i=1;i<=n;++i)
				if(!vis[i]&&sqr(x[u]-x[i])+sqr(y[u]-y[i])+sqr(z[u]-z[i])<=lim)
				{
					Q.push(i);
					vis[i]=true;
				}
		}
		bool flag=false;
		for(int i=1;i<=n;++i)
			if(vis[i]&&abs(z[i]-h)<=r)
			{
				flag=true;
				break;
			}
		puts(flag?"Yes":"No");
	}
	return 0;      
}

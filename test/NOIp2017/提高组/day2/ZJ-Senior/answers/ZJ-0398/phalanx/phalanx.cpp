#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstring>
#include<map>
#include<queue>
using namespace std;
#define pir pair<int,int>
const int L=5202333;
const int BT=20;
int n,m,q;
int rec[2333][2333];
map<pir,int> mp;
int C[L+10],id[L+10],tot;
queue<int> Q;
inline void set(int x,int y,int w){mp[make_pair(x,y)]=w;}
inline int get(int x,int y)
{
	pir o=make_pair(x,y);
	map<pir,int>::iterator it=mp.find(o);
	if(it==mp.end())return mp[o]=(x-1)*m+y;
	else return it->second;
}
inline void add(int x,int w){for(;x<=L;x+=x&(-x))C[x]+=w;}
inline int getit(int k)
{
	int res=0;
	for(int i=BT;~i;--i)
		if(C[res+(1<<i)]<k)k-=C[res+=(1<<i)];
	return res+1;
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	int x,y;
	if(n<=1000&&m<=1000)
	{
		for(int i=1,cnt=0;i<=n;++i)
			for(int j=1;j<=m;++j)
				rec[i][j]=++cnt;
		while(q--)
		{
			scanf("%d%d",&x,&y);printf("%d\n",rec[x][y]);
			int tmp=rec[x][y];
			for(int i=y+1;i<=m;++i)rec[x][i-1]=rec[x][i];
			for(int i=x+1;i<=n;++i)rec[i-1][m]=rec[i][m];
			rec[n][m]=tmp;
		}
	}
	else if(n<=50000&&m<=50000&&q<=500)
	{
		while(q--)
		{
			int tmp;
			scanf("%d%d",&x,&y);printf("%d\n",tmp=get(x,y));
			for(int i=y+1;i<=m;++i)set(x,i-1,get(x,i));
			for(int i=x+1;i<=n;++i)set(i-1,n,get(i,m));
			set(n,m,tmp);
		}
	}
	else
	{
		for(int i=1;i<=m;++i){add(i,1);id[i]=i;}
		tot=m;
		for(int i=1;i<=n;++i)Q.push(i*m);
		while(q--)
		{
			int tmp;
			scanf("%*d%d",&y);printf("%d\n",id[tmp=getit(y)]);
			add(tmp,-1);
			add(++tot,1);
			Q.pop();Q.push(id[tmp]);
			id[tot]=Q.front();
		}
	}
	return 0;
}

#include <bits/stdc++.h>
#define N (2010)
#define M (1000010)
using namespace std;
typedef long long ll;
template <typename T> inline void read(T &x) {
	static char c; c = x = 0; while (c < '0' || c > '9') c = getchar();
	while (c >= '0' && c <= '9') x = (x + (x << 2) << 1) + c ^ '0', c = getchar();
}
int fa[N];
inline int ask(int x) {return fa[x] == x ? x : fa[x] = ask(fa[x]);}
inline void add(int x, int y) {
	static int p, q; p = ask(x); q = ask(y);
	if (p != q) fa[p] = q;
}
int T, n;
ll h, r, r1, x[N], y[N], z[N];
inline ll dis(int i, int j) {
	return (x[i] - x[j])*(x[i] - x[j])+(y[i] - y[j])*(y[i] - y[j])+(z[i] - z[j])*(z[i] - z[j]);
}
bool b[N];
int main() {
	freopen("cheese.in", "r", stdin);
	freopen("cheese.out", "w", stdout);
	scanf("%d", &T);
	while (T--) {
		scanf("%d%lld%lld", &n, &h, &r1); r = r1 * r1;
		for (int i = 0; i <= n + 1; ++i) fa[i] = i;
		for (int i = 1; i <= n; ++i) scanf("%lld%lld%lld", &x[i], &y[i], &z[i]);
		for (int i = 1; i <= n; ++i) {
			for (int j = 1; j <= n; ++j) if (dis(i, j) <= 4 * r) add(i, j); 
			if (z[i] <= r1) add(0, i);
			if (z[i] + r1 >= h) add(n + 1, i);
		}
		if (ask(0) == ask(n + 1)) puts("Yes"); else puts("No");
	}
	return 0;
}

#include <bits/stdc++.h>
#define N (1010)
using namespace std;
typedef long long ll;
template <typename T> inline void read(T &x) {
	static char c; c = x = 0; while (c < '0' || c > '9') c = getchar();
	while (c >= '0' && c <= '9') x = ((x + (x << 2)) << 1) + (c ^ '0'), c = getchar();
}
int n, m, q;
int a[N][N];
int main() {
	freopen("phalanx.in", "r", stdin);
	freopen("phalanx.out", "w", stdout);
	read(n); read(m); read(q);
	for (int i = 1; i <= n; ++i) for (int j = 1; j <= m; ++j) a[i][j] = (i - 1) * m + j;
	while (q--) {
		int x, y, tmp; read(x); read(y);
		printf("%d\n", a[x][y]); tmp = a[x][y];
		for (int i = y; i < m; ++i) a[x][i] = a[x][i + 1];
		for (int i = x; i < n; ++i) a[i][m] = a[i + 1][m]; 
		a[n][m] = tmp;
	}
	return 0;
}

#include <bits/stdc++.h>
#define N (100010)
using namespace std;
typedef long long ll;
template <typename T> inline void read(T &x) {
	static char c; c = x = 0; while (c < '0' || c > '9') c = getchar();
	while (c >= '0' && c <= '9') x = ((x + (x << 2)) << 1) + (c ^ '0'), c = getchar();
}
template <typename T> inline T Min(const T &x, const T &y) {return x < y ? x : y;}
ll edge[20][20], ans = 100000000000000;
int n, m, p[20][20], c[20], qu[N], d[20], q[20], h, t, dep[20], sel;
int cnt;
bool b[20];
inline void dfs(int x) {
	if (x > n) {
		int w = 0, s = 0;
		ll res = 0;
		for (int i = 1; i <= n; ++i) b[i] = 0; b[sel] = 1;
		h = 0; q[t = 1] = sel; dep[sel] = 0;
		while (h < t) {
			s = q[++h]; 
			for (int i = 1; i <= c[s]; ++i) if (!b[p[s][i]]) {
				dep[p[s][i]] = dep[s] + 1;
				res += edge[p[s][i]][s] * dep[p[s][i]];
				q[++t] = p[s][i]; b[p[s][i]] = 1;
			}
		}
		if (t != n) return;
		if (res < ans) ans = res;
		return;
	}
	if (x == sel) dfs(x + 1);
	else for (int i = n; i; --i) if (edge[x][i] < 100000000000000) {
		p[i][++c[i]] = x; dfs(x + 1); --c[i]; 
	}
}
int main() {
	freopen("treasure.in", "r", stdin);
	freopen("treasure.out", "w", stdout);
	read(n); read(m);
	for (int i = 1; i <= n; ++i) for (int j = 1; j <= n; ++j) edge[i][j] = 100000000000000;
	for (int i = 1, x, y, z; i <= m; ++i) {
		read(x); read(y); read(z); edge[y][x] = edge[x][y] = Min(edge[x][y], (ll)z);
	}
	for (int i = 1; i <= n; ++i) {
		sel = i; dfs(1);
	}
	printf("%lld", ans);
}

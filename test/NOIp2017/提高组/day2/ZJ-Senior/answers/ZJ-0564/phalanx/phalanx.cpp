#include <cstdio>
#define LL long long 

  struct treenode{
  	int sum,lc,rc;
	LL val;
  }tr[20000001];

  int cnt,n,m,q,spe[500001],plc[500001],faq;

  LL getdelkth(int po,int l,int r,int k){
  	tr[po].sum--;
  	int mid=(l+r)>>1;
  	if (l==r) return(tr[po].val);
  	if (k<=tr[tr[po].lc].sum) return(getdelkth(tr[po].lc,l,mid,k));else
  	  return(getdelkth(tr[po].rc,mid+1,r,k-tr[tr[po].lc].sum));
  }
  
  void ins(int po,int l,int r,int tar,int num,LL val=-1){
  	tr[po].sum+=num;
  	if (l==r){tr[po].val=val;return;}
  	int mid=(l+r)>>1;
  	if (tar<=mid){
  	  if (!tr[po].lc) tr[po].lc=++cnt;
  	  ins(tr[po].lc,l,mid,tar,num,val);
	}else{
	  if (!tr[po].rc) tr[po].rc=++cnt;
	  ins(tr[po].rc,mid+1,r,tar,num,val);
	}
  }
  
  LL getleftkth(int po,int l,int r,int k){
  	if (l==r) return(l);
  	
  	int mid=(l+r)>>1;
  	if (k<=(mid-l+1)+tr[tr[po].lc].sum) return(getleftkth(tr[po].lc,l,mid,k));else
  	  return(getleftkth(tr[po].rc,mid+1,r,k-((mid-l+1)+tr[tr[po].lc].sum)));
  }

  int main(){
  	freopen("phalanx.in","r",stdin);
  	freopen("phalanx.out","w",stdout);
  	
  	scanf("%d%d%d",&n,&m,&q);
  	cnt=2*n+1;
   	for (int i=1;i<=n;i++) ins(2*n+1,1,n+q,++plc[n+1],1,(LL)i*m),spe[i]=1;
  	for (int i=1;i<=q;i++){
  	  LL x,y;LL t;
  		
  	  scanf("%lld%lld",&x,&y);
	  if (y<=m-spe[x]){
	  	t=getleftkth(x,1,m-1,y);ins(x,1,m-1,t,-1);
		spe[x]++;
		ins(n+x,1,q,++plc[x],1,getdelkth(2*n+1,1,n+q,x));
		ins(2*n+1,1,n+q,++plc[n+1],1,t+(x-1)*m);
		printf("%lld\n",t+(x-1)*m);
	  }else
	  if (y<m){
	    t=getdelkth(n+x,1,q,y-(m-spe[x]));
	    ins(n+x,1,q,++plc[x],1,getdelkth(2*n+1,1,n+q,x));
	    ins(2*n+1,1,n+q,++plc[n+1],1,t);
	    printf("%lld\n",t);
	  }else{
	    t=getdelkth(2*n+1,1,n+q,x);
	    ins(2*n+1,1,n+q,++plc[n+1],1,t);
	    printf("%lld\n",t);
	  }
	}
  }

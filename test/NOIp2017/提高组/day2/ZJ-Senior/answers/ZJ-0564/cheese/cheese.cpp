#include <cstdio>
#include <cmath>
#include <cstring>
#define LDB long double
using namespace std;

  struct data{
  	LDB x,y,z;
  }a[3001];
  
  LDB dis(data a,data b){
  	return(sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z)));
  }

  int n,h,r,T,nd[3001],cnt,nxt[3000001],des[3000001],bt[3001];

  void addedge(int x,int y){
  	nxt[++cnt]=nd[x];des[cnt]=y;nd[x]=cnt;
  }
  
  void dfs(int po){
  	bt[po]=1;
  	for (int p=nd[po];p!=-1;p=nxt[p])
  	  if (!bt[des[p]])
  	    dfs(des[p]);
  }
  
  int main(){
  	freopen("cheese.in","r",stdin);
  	freopen("cheese.out","w",stdout);
  	
  	scanf("%d",&T);
  	while (T--){
  	  scanf("%d%d%d",&n,&h,&r);
	  for (int i=0;i<=n+1;i++) nd[i]=-1;cnt=0;
	  for (int i=1;i<=n;i++){
	  	scanf("%Lf%Lf%Lf",&a[i].x,&a[i].y,&a[i].z);
	  	if (a[i].z-r<=0) addedge(0,i);
	  	if (a[i].z+r>=h) addedge(i,n+1);
	  	for (int j=1;j<i;j++)
	  	  if (dis(a[i],a[j])<2*r+1e-6)
	  	    addedge(i,j),addedge(j,i);
	  }
	  
	  memset(bt,0,sizeof(bt));
	  dfs(0);
	  if (bt[n+1]) printf("Yes\n");else printf("No\n");
	}
  } 

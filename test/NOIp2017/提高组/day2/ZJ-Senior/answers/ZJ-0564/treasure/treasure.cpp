#include <cstdio>
#include <iostream>
using namespace std;

  int n,m,dis[21][21],fsta;
  int sid[1<<12][1<<12],dp[15][1<<12];

  int main(){
	freopen("treasure.in","r",stdin);  	
	freopen("treasure.out","w",stdout);
  	
  	scanf("%d%d",&n,&m);
  	for (int i=1;i<=n;i++) for (int j=1;j<=n;j++) dis[i][j]=1e9;
	for (int i=1;i<=m;i++){
	  int t1,t2,t3;
	  scanf("%d%d%d",&t1,&t2,&t3);
	  dis[t1][t2]=dis[t2][t1]=min(dis[t1][t2],t3);	
	}
	
	fsta=(1<<n)-1;
	for (int i=1;i<=fsta;i++)
	  for (int j=(fsta-i);j;j=(j-1)&(fsta-i))
	    for (int k=1;k<=n;k++) if (j&(1<<(k-1))){
	      int mini=1e9;
	      for (int l=1;l<=n;l++) if (i&(1<<(l-1))) mini=min(mini,dis[l][k]);
	      sid[i][j]+=mini;
		}
		
	for (int i=0;i<=n;i++) for (int j=0;j<=fsta;j++) dp[i][j]=1e9;
	for (int i=1;i<=n;i++) dp[0][1<<(i-1)]=0;
	for (int i=0;i<n;i++) for (int j=1;j<=fsta;j++) if (dp[i][j]!=1e9)
	  for (int k=(fsta-j);k;k=(k-1)&(fsta-j))
	    dp[i+1][j+k]=min(dp[i+1][j+k],dp[i][j]+(i+1)*sid[j][k]);
    int ret=1e9;
    for (int i=0;i<=n;i++) ret=min(ret,dp[i][fsta]);
    printf("%d\n",ret);
  } 

#include<bits/stdc++.h>
#define N 2010
#define ll long long
#define sqr(x) (((ll)x)*(x))
using namespace std;
void read(int&n)
{
	int k=1;char ch=getchar();n=0;
	while(!('0'<=ch&&ch<='9')){if(ch=='-')k=-1;ch=getchar();}
	while('0'<=ch&&ch<='9')n=n*10+ch-48,ch=getchar();
	n*=k;
}
int tot,to[N*2],head[N],nxt[N*2],T,n,h,r,i,j,x[N],y[N],z[N],f[N],k;
ll s;queue<int> q;bool bo;
void add(int x,int y)
{
	to[++tot]=head[x],nxt[tot]=y,head[x]=tot;
	to[++tot]=head[y],nxt[tot]=x,head[y]=tot;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	read(T);
	while(T--)
	{
		read(n),read(h),read(r),tot=0,bo=0;
		memset(head,0,sizeof(head));
		for(i=2;i<=n+1;i++)
		{
			read(x[i]),read(y[i]),read(z[i]);
			if(z[i]<=r)add(i,1);
			if(z[i]+r>=h)add(i,n+2);
		}
		for(i=2;i<=n;i++)
		for(j=i+1;j<=n+1;j++)
		{
			s=sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
			if(s<=sqr(2*r))add(i,j);
		}
		while(!q.empty())q.pop();q.push(1);
		memset(f,0,sizeof(f));
		while(!q.empty())
		{
			k=q.front(),q.pop();
			for(i=head[k];i;i=to[i])
			if(!f[nxt[i]])
			{
				f[nxt[i]]=1,q.push(nxt[i]);
				if(nxt[i]==n+2)
				{
					bo=1;break;
				}
			}
			if(bo)break;
		}
		if(bo)puts("Yes");else puts("No");
	}
	return 0;
}

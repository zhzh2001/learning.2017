#include<bits/stdc++.h>
#define N 20
using namespace std;
void read(int&n)
{
	int k=1;char ch=getchar();n=0;
	while(!('0'<=ch&&ch<='9')){if(ch=='-')k=-1;ch=getchar();}
	while('0'<=ch&&ch<='9')n=n*10+ch-48,ch=getchar();
	n*=k;
}
int n,m,i,j,ii,jj,mi,x,y,z,a[N][N],f[N][6666],b[N][6666],sb[N];
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n),read(m);
	sb[1]=1;
	for(i=2;i<=n;i++)sb[i]=sb[i-1]*2;
	memset(a,10,sizeof(a));
	for(i=1;i<=m;i++)read(x),read(y),read(z),a[x][y]=a[y][x]=min(a[y][x],z);
	m=1<<n;
	memset(f,10,sizeof(f));
	memset(b,10,sizeof(b));
	for(i=1;i<=n;i++)f[i][sb[i]]=0,b[i][sb[i]]=0;
	for(i=2;i<m;i++)
	for(j=1;j<i;j++)
	if(!(i&j))
	for(ii=1;ii<=n;ii++)
	if(sb[ii]&i)
	for(jj=1;jj<=n;jj++)
	if(sb[jj]&j)
	{
		if(f[ii][i|j]>f[ii][i]+f[jj][j]+b[jj][j]+a[ii][jj]
		||f[ii][i|j]==f[ii][i]+f[jj][j]+b[jj][j]+a[ii][jj]&&
		b[ii][i|j]>b[ii][i]+b[jj][j]+a[ii][jj])
			f[ii][i|j]=f[ii][i]+f[jj][j]+b[jj][j]+a[ii][jj],
			b[ii][i|j]=b[ii][i]+b[jj][j]+a[ii][jj];
		if(f[jj][i|j]>f[ii][i]+f[jj][j]+b[ii][i]+a[ii][jj]
		||f[jj][i|j]==f[ii][i]+f[jj][j]+b[ii][i]+a[ii][jj]&&
		b[jj][i|j]>b[ii][i]+b[jj][j]+a[ii][jj])
			f[jj][i|j]=f[ii][i]+f[jj][j]+b[ii][i]+a[ii][jj],
			b[jj][i|j]=b[ii][i]+b[jj][j]+a[ii][jj];
	}
	mi=1000000000;
	for(i=1;i<=n;i++)
	mi=min(mi,f[i][m-1]);
	printf("%d\n",mi);
	return 0;
}

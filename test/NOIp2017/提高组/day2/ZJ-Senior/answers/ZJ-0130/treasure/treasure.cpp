#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define INF 1000000000
using namespace std;
struct edge
{
	int v,next,w;
}e[2010];
int n,m,cnt,head[20],cost,dis[20],MIN,dist,ne,miaom;
void read(int & x)
{
	x=0; char ch=getchar(); int k=1;
	while (ch<'0' || '9'<ch){if (ch=='-') k=-1; ch=getchar();}
	while ('0'<=ch && ch<='9'){x=x*10+ch-48; ch=getchar();}
	x=x*k;
}
void add_edge(int u,int v,int w)
{
	e[++cnt].v=v;e[cnt].w=w;e[cnt].next=head[u];head[u]=cnt;
	e[++cnt].v=u;e[cnt].w=w;e[cnt].next=head[v];head[v]=cnt;	
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	read(n);read(m);
	for (int i=1;i<=m;i++)
	{
		int u,v,w; 
		read(u);read(v);read(w);
		add_edge(u,v,w);
	}
	miaom=INF;
	for (int first=1;first<=n;first++)
	{
		for (int i=1;i<=n;i++) dis[i]=INF;
		dis[first]=1;cost=0;
		for (int stan=1;stan<n;stan++)
		{
			MIN=INF; ne=0; dist=0;
		    for (int u=1;u<=n;u++)
		        if (dis[u]<INF)
		        {
		    	    for (int i=head[u];i;i=e[i].next)
		    	    {
		    		    int v=e[i].v;
		    		    if (dis[v]<INF) continue;
		    		    if ((dis[u])*e[i].w<MIN)
		    		    {
		    		    	MIN=(dis[u])*e[i].w;
		    		    	ne=v;
		    		    	dist=dis[u]+1;
		    		    }
		    	    }
		        }
		    cost+=MIN;
		    dis[ne]=dist;
	    }
	    miaom=min(cost,miaom);
	}
	printf("%d\n",miaom);
	return 0;
}


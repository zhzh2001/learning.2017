#include<cmath>
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
struct edge
{
	int v,next;
}e[2000010];
int n,h,r,x[1010],y[1010],z[1010],head[1010],cnt,q[1010],vis[1010];
bool flag;
void read(int & xx)
{
	xx=0; char ch=getchar(); int k=1;
	while (ch<'0' || '9'<ch){if (ch=='-') k=-1; ch=getchar();}
	while ('0'<=ch && ch<='9'){xx=xx*10+ch-48; ch=getchar();}
	xx=xx*k;
}
void add_edge(int u,int v)
{
	e[++cnt].v=v;e[cnt].next=head[u];head[u]=cnt;
}
void SPFA(int x)
{
	memset(vis,0,sizeof(vis));
	memset(q,0,sizeof(q));
	int l=0,r=1;
	q[1]=x; vis[x]=1;
	while (l!=r)
	{
		l++; int u=q[l];
		for (int i=head[u];i;i=e[i].next)
		{
			int v=e[i].v;
			if (vis[v]) continue;
			vis[v]=1; r++; q[r]=v;
			if (z[v]+r>=h && z[v]-r<=h)
			{
				flag=1; break;
			}
		}
		if (flag) break;
	}	
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T; read(T);
	while (T--)
	{
		read(n); read(h); read(r);
		memset(x,0,sizeof(x));
		memset(y,0,sizeof(y));
		memset(z,0,sizeof(z));
		memset(e,0,sizeof(e));
		memset(head,0,sizeof(head)); cnt=0;
		for (int i=1;i<=n;i++){read(x[i]);read(y[i]);read(z[i]);}
		for (int i=1;i<=n;i++)
		    for (int j=1;j<=n;j++)
		    if (abs(x[i]-x[j])<=r*2 && abs(y[i]-y[j])<=r*2 && abs(z[i]-z[j])<=r*2 && i!=j)
		    {
		    	//double dist=(x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])+(z[i]-z[j])*(z[i]-z[j]);
		    	//printf("%.3f\n",dist);
		    	//if (dist<=r*2) 
				    add_edge(i,j);
		    }
		flag=0;
		for (int i=1;i<=n;i++)
			if (z[i]-r<=0 && z[i]+r>=0)
			{
				SPFA(i);
				if (flag) break;
			}
		if (flag) printf("Yes\n");
		else printf("No\n");
	}
	return 0;
}

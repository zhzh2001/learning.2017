#include<cstdio>
#include<algorithm>
#define LL long long
using namespace std;
const int N=1005,NN=3e5+5;
struct que{int x,y;}q[NN];
int mp[N][N];
LL a[NN*3],tr[NN*3];
int n,m,Q,tot;
int read()
{
	int x=0; char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	return x;
}
void case1()
{
	for(int i=1,t=0;i<=n;i++)
	    for(int j=1;j<=n;j++)
	    mp[i][j]=++t;
	for(int t=1,x,y,ans;t<=Q;t++)
	{
		x=q[t].x; y=q[t].y; ans=mp[x][y];
		for(int i=y;i<n;i++)mp[x][i]=mp[x][i+1];
		for(int i=x;i<n;i++)mp[i][m]=mp[i+1][m];
		mp[n][m]=ans;
		printf("%d\n",ans);
	}
}
void add(int x){for(;x<=tot;x+=x&-x)tr[x]++;}
int que(int x){int res=0; for(;x;x-=x&-x)res+=tr[x]; return res;}
void case2()
{
	int t=0,l,r,mid; tot=n+m+Q+2;
	for(int i=1;i<=m;i++)a[++t]=i;
	for(int i=2;i<=n;i++)a[++t]=1ll*i*m;
	for(int i=1,tmp;i<=Q;i++)
	{
		tmp=q[i].y+que(q[i].y);
		l=q[i].y; r=tot;
		while(l<=r)
		{
			mid=(l+r)>>1;
			if(q[i].y+que(mid)<=mid){tmp=mid; r=mid-1;}else l=mid+1;
		}
		add(tmp);
		printf("%lld\n",a[tmp]);
		a[++t]=a[tmp]; a[tmp]=0;
	}
}
bool judge(){for(int i=1;i<=Q;i++)if(q[i].x!=1)return 0; return 1;}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=read(); m=read(); Q=read();
	for(int i=1;i<=Q;i++){q[i].x=read(); q[i].y=read();}
	if(judge()){case2(); return 0;}
	if(n<=1000){case1(); return 0;}
	return 0;
}

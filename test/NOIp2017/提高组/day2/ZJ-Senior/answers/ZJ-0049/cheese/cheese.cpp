#include<cstdio>
#include<algorithm>
#define LL long long
using namespace std;
const int N=1005;
struct node{LL x,y,z;}a[N];
int fa[N];
LL h,r;
int n,st,ed;
LL read()
{
	LL x=0; char ch=getchar(); bool f=0;
	while(ch<'0'||ch>'9'){if(ch=='-')f=1; ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	return f?-x:x;
}
int root(int x){return fa[x]==x?x:root(fa[x]);}
LL sqr(LL x){return x*x;}
bool judge(node a,node b){return sqr(a.x-b.x)+sqr(a.y-b.y)<=r-sqr(a.z-b.z);}
void solve()
{
	n=read(); h=read(); r=read();
	st=n+1; ed=n+2;
	for(int i=1;i<=ed;i++)fa[i]=i;
	for(int i=1;i<=n;i++){a[i].x=read(); a[i].y=read(); a[i].z=read();}
	for(int i=1;i<=n;i++)if(a[i].z<=r)fa[root(i)]=root(st);
	for(int i=1;i<=n;i++)if(a[i].z+r>=h)fa[root(i)]=root(ed);
	r<<=1; r*=r;
	for(int i=1;i<n;i++)
		for(int j=i+1;j<=n;j++)
		if(root(i)!=root(j))
		{
			if(judge(a[i],a[j]))fa[root(i)]=root(j);
		}
	if(root(st)==root(ed))printf("Yes\n");else printf("No\n");
	return;
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T=read();
	while(T--)solve();
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define LL long long
using namespace std;
const int N=15;
LL mp[N][N],q[N],dep[N];
int n,m,t,top;
LL ans;
int read()
{
	int x=0; char ch=getchar();
	while(ch<'0'||ch>'9')ch=getchar();
	while(ch>='0'&&ch<='9'){x=x*10+ch-48; ch=getchar();}
	return x;
}
void dfs(int x,LL s,int y)
{
	if(s>=ans&&ans!=-1)return;
	if(x>top)return;
	if(top==n){ans=s;return;}
	dfs(x+1,s,0);
	for(int i=y+1;i<=n;i++)
	if(~mp[q[x]][i])
	if(dep[i]==-1)
	{
		dep[i]=dep[q[x]]+1;
		q[++top]=i;
		dfs(x,s+mp[q[x]][i]*dep[i],i);
		dep[i]=-1; top--;
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(mp,-1,sizeof(mp));
	n=read(); m=read();
	for(int i=1,x,y,z;i<=m;i++)
	{
		x=read(); y=read(); z=read();
		if(z<mp[x][y]||mp[x][y]==-1)mp[x][y]=mp[y][x]=z;
	}
	ans=-1;
	for(int i=1;i<=n;i++)
	{
		memset(dep,-1,sizeof(dep));
		dep[i]=0; q[top=1]=i;
		dfs(1,0,0);
	}
	printf("%lld\n",ans);
	return 0;
}

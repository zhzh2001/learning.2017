#include<cstdio>
#include<iostream>
#define inf 99999999
#define LL long long
using namespace std;
int n,m,u[1100],v[1100],w[1100],dep[1100],num;
bool vis[1100];
LL ans,sum;
void dfs(){
	if(num==n)return;
	int now=inf;int x=0;int sum1;
	for(int i=1;i<=m;++i){
		if(vis[u[i]]&&!vis[v[i]]){
			sum1=dep[u[i]]*w[i];
			if(sum1<now)now=sum1,x=i;
		}
		if(!vis[u[i]]&&vis[v[i]]){
			sum1=dep[v[i]]*w[i];
			if(sum1<now)now=sum1,x=i;
		}
	}
	if(vis[u[x]]){
		vis[v[x]]=1;
		dep[v[x]]=dep[u[x]]+1;
	}
	else{
		vis[u[x]]=1;
		dep[u[x]]=dep[v[x]]+1;
	}
	sum+=now;
	num++;dfs();
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	ans=inf;
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;++i)
	scanf("%d %d %d",&u[i],&v[i],&w[i]);
	for(int i=1;i<=n;++i){
		for(int j=1;j<=n;++j)vis[j]=0,dep[j]=0;
		vis[i]=1;num=1;dep[i]=1;sum=0;
		dfs();
		if(ans>sum)ans=sum;
	}
	printf("%lld",ans);
	return 0;
} 

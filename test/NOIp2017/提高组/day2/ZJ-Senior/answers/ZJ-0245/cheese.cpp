#include<cstdio>
#include<iostream>
#define LL long long
using namespace std;
int t,n,f[1100];
LL r,x[1100],y[1100],z[1100],h;
LL dis(int u,int v){
	LL q=(x[u]-x[v])*(x[u]-x[v]);
	LL w=(y[u]-y[v])*(y[u]-y[v]);
	LL e=(z[u]-z[v])*(z[u]-z[v]);
	return q+w+e;
}
int find(int u){
	if(f[u]==u)return u;
	f[u]=find(f[u]);
	return f[u];
}
void addedge(int u,int v){
	int x=find(u);
	int y=find(v);
	f[x]=y;
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&t);
	while(t--){
		scanf("%d %lld %lld",&n,&h,&r);
		for(int i=0;i<=n+1;++i)f[i]=i;
		LL b=4*r*r;
	    for(int i=1;i<=n;++i)
	    scanf("%lld %lld %lld",&x[i],&y[i],&z[i]);
	    for(int i=1;i<=n;++i)
	      for(int j=i+1;j<=n;++j){
	      	LL a=dis(i,j);
	      	if(a<=b)
	      	addedge(i,j);
		  }
		for(int i=1;i<=n;++i){
			if(z[i]<=0){
				if(z[i]+r>=0)addedge(0,i);
				if(z[i]+r>=h)addedge(i,n+1);
			}
			else if(z[i]<=h){
				if(z[i]-r<=0)addedge(0,i);
				if(z[i]+r>=h)addedge(i,n+1);
			}
			else {
				if(z[i]-r<=0)addedge(0,i);
				if(z[i]-r<=h)addedge(i,n+1);
			}
		}
		if(find(0)==find(n+1))printf("Yes\n");
		else printf("No\n");  
	}
	return 0;
} 

#include<cstdio>
#include<cstring>
#include<algorithm>
#define cl(x,y) memset(x,y,sizeof(x))
using namespace std;
typedef long long ll;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int red(){
	int res=0,f=1;char ch=nc();
	while (ch<'0'||'9'<ch) {if (ch=='-') f=-f;ch=nc();}
	while ('0'<=ch&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}

const int maxn=15,INF=0x3f3f3f3f;
int n,m,T,dep[maxn];
int lst[maxn],nxt[maxn],a[maxn];
ll ans=1ll<<60,w[maxn][maxn];
void dfs_s(int stp,ll sum){
	if (sum>=ans) return;
	if (stp>n){
		ans=sum;return;
	}
	for (int i=lst[T];i;i=lst[i]){
		ll Min=INF,k=-1;
		for (int j=1;j<stp;j++)
		 if (w[i][a[j]]<INF&&dep[a[j]]*w[i][a[j]]<Min) Min=dep[a[j]]*w[i][a[j]],k=j;
		if (stp==1) k=Min=0;
		if (k<0||Min==INF) continue;
		a[stp]=i;dep[i]=dep[k]+1;
		lst[nxt[i]]=lst[i]; nxt[lst[i]]=nxt[i];
		dfs_s(stp+1,sum+Min);
		lst[nxt[i]]=i; nxt[lst[i]]=i;
	}
}
void dfs(int stp,int sum){
	if (sum>=ans) return;
	if (stp>n){
		ans=sum;return;
	}
	for (int i=lst[T];i;i=lst[i]){
		a[stp]=i;
		lst[nxt[i]]=lst[i]; nxt[lst[i]]=nxt[i];
		for (int j=1;j<stp;j++)
		 if (w[i][a[j]]<INF){
		 	dep[i]=dep[a[j]]+1;
		 	dfs(stp+1,sum+dep[a[j]]*w[i][a[j]]);
		 }
		if (stp==1){
			dep[i]=1;dfs(stp+1,sum);
		}
		lst[nxt[i]]=i; nxt[lst[i]]=i;
	}
}
int main(){
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	n=red();m=red();
	for (int i=0;i<=n+1;i++) lst[i]=i-1,nxt[i]=i+1;
	lst[0]=0;nxt[n+1]=n+1;T=n+1;
	cl(w,63);
	for (int i=1,x,y,z;i<=m;i++) x=red(),y=red(),z=red(),w[x][y]=w[y][x]=min(w[x][y],(ll)z); 
	dfs_s(1,0);dfs(1,0);
	printf("%lld",ans);
	return 0;
}

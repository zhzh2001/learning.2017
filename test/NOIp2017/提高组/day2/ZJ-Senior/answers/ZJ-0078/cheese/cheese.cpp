#include<cstdio>
#define sqr(x) ((x)*(x))
typedef long long ll;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int red(){
	int res=0,f=1;char ch=nc();
	while (ch<'0'||'9'<ch) {if (ch=='-') f=-f;ch=nc();}
	while ('0'<=ch&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}

const int maxn=1005;
int tst,n,S,T,fa[maxn];
ll x[maxn],y[maxn],z[maxn],h,r;
int getfa(int x){
	return fa[x]==x?x:fa[x]=getfa(fa[x]);
}
inline ll getdst(int i,int j){
	return sqr(x[i]-x[j])+sqr(y[i]-y[j])+sqr(z[i]-z[j]);
}
int main(){
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	tst=red();
	while (tst--){
		n=red(),h=red(),r=red();
		S=n+1;T=n+2;
		for (int i=1;i<=n+2;i++) fa[i]=i;
		for (int i=1;i<=n;i++){
			x[i]=red(),y[i]=red(),z[i]=red();
			if (z[i]<=r) fa[getfa(i)]=getfa(S);
			if (z[i]+r>=h) fa[getfa(i)]=getfa(T);
		}
		r=r*r*4;
		for (int i=1;i<=n;i++)
		 for (int j=i+1;j<=n;j++)
		  if (getdst(i,j)<=r) fa[getfa(i)]=getfa(j);
		if (getfa(S)==getfa(T)) printf("Yes\n");else printf("No\n");
	}
	return 0;
}

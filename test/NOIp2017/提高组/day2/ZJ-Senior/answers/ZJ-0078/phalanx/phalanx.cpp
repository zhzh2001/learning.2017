#include<cstdio>
#include<algorithm>
using namespace std;
inline char nc(){
	static char buf[100000],*p1=buf,*p2=buf;
	return p1==p2&&(p2=(p1=buf)+fread(buf,1,100000,stdin),p1==p2)?EOF:*p1++;
}
inline int red(){
	int res=0,f=1;char ch=nc();
	while (ch<'0'||'9'<ch) {if (ch=='-') f=-f;ch=nc();}
	while ('0'<=ch&&ch<='9') res=res*10+ch-48,ch=nc();
	return res*f;
}

const int maxn=300005;
int n,m,N,q,a[505][50005],b[maxn],M[maxn];
struct data{
	int x,y;
}Q[maxn];
inline int ID(int x){
	return lower_bound(b+1,b+1+N,x)-b;
}
inline void work(int id,int y){
	int tem=a[id][y];
	for (int j=y+1;j<=m;j++) a[id][j-1]=a[id][j];
	for (int i=b[id]+1;i<=n;i++) M[i-1]=M[i];
	M[n]=tem;
	for (int i=1;i<=N;i++) a[i][m]=M[b[i]];
}

struct node{
	node *l,*r;
	int s;
	node () {}
	node (node *_l,node *_r):l(_l),r(_r),s(0) {}
	void pushup() {s= l->s + r->s;}
}base[maxn<<2],nil;
typedef node* P_node;
P_node null,Rot,len;
void tree_init(){
	nil=node(null,null);null=&nil;len=base;
}
inline P_node newnode(P_node l,P_node r){
	*len=node(l,r);return len++;
}
P_node build(int L,int R){
	P_node x=newnode(null,null);
	if (L==R) {x->s=(L<=m);return x;}
	int mid=L+R>>1;
	x->l=build(L,mid);x->r=build(mid+1,R);
	x->pushup(); return x;
}
void insert(P_node x,int L,int R,int k){
	if (L==R) {x->s+=k;return;}
	int mid=L+R>>1;
	if (k<=mid) insert(x->l,L,mid,k);else insert(x->r,mid+1,R,k);
	x->pushup();
}
int query(P_node x,int L,int R,int k){
	if (L==R) return L;
	int mid=L+R>>1;
	if (x->l->s >=k) return query(x->l,L,mid,k);
	 else return query(x->r,mid+1,R,k- x->l->s);
}
int que[maxn<<1],t[maxn<<1];
int main(){
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	n=red(),m=red(),q=red();
	if (q<=500){
		for (int i=1;i<=q;i++)
		 Q[i].x=red(),Q[i].y=red(),b[i]=Q[i].x;
		sort(b+1,b+1+q);
		N=unique(b+1,b+1+q)-b-1;
		
		for (int i=1;i<=N;i++)
		 for (int j=1;j<=m;j++) a[i][j]=(b[i]-1)*m+j;
		for (int i=1;i<=n;i++) M[i]=i*m;
		
		for (int i=1;i<=q;i++){
			int id=ID(Q[i].x);
			printf("%d\n",a[id][Q[i].y]);
			work(id,Q[i].y);
		}
	}else{
		int mm=m+q;
		tree_init();Rot=build(1,mm);
		for (int i=1;i<=m;i++) t[i]=i;
		for (int i=1;i<=n;i++) que[i]=i*m;
		int hed=1,til=m;
		while (q--){
			int x=red(),y=red(),pos=query(Rot,1,mm,y);
			printf("%d\n",t[pos]);
			insert(Rot,1,mm,pos);insert(Rot,1,mm,++til);
			t[til]=que[hed++];que[hed+n-1]=t[pos];
		}
	}
	return 0;
}

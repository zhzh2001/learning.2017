var
  bi:boolean;
  a1,a2,a3,a4:longint;
  dist:real;
  h,r,t,i,j,m,n,k,p:longint;
  father,a,b,x,y,z:array[0..1005]of int64;
function find(t:longint):longint;
begin
  if father[t]=t then exit(t)
  else
    begin
      father[t]:=find(father[t]);
      exit(father[t]);
    end;
end;
begin
  assign(input,'cheese.in');
  assign(output,'cheese.out');
  reset(input); rewrite(output);
  readln(t);
  for k:=1 to t do
    begin
      readln(n,h,r);
      a1:=0; a2:=0; bi:=false;
      for i:=1 to n do
        begin
          readln(x[i],y[i],z[i]);
          if z[i]+r>=h then
            begin
              inc(a1); a[a1]:=i;
            end;
          if z[i]-r<=0 then
            begin
              inc(a2); b[a2]:=i;
            end;
        end;
      if (a1=0)or(a2=0) then writeln('No') else
      if bi=false then
        begin
          for i:=1 to n do father[i]:=i;
          for i:=1 to n do
            for j:=1 to n do
              begin
                dist:=sqrt((x[i]-x[j])*(x[i]-x[j])+(y[i]-y[j])*(y[i]-y[j])
                     +(z[i]-z[j])*(z[i]-z[j]));
                if dist<=2*r then
                  begin
                    a3:=find(i); a4:=find(j);
                    if a3<>a4 then father[a3]:=a4;
                  end;
              end;
            for i:=1 to a1 do
              begin
                for j:=1 to a2 do
                  if father[a[i]]=father[b[j]] then
                    begin
                      writeln('Yes'); bi:=true; break;
                    end;
                  if bi=true then break;
              end;
          if bi=false then
            writeln('No');
        end;
    end;
  close(input);
  close(output);
end.

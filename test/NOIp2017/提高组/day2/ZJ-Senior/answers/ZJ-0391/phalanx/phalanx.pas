var
  a1,point,tot,head,tail:longint;
  q,i,j,m,n,k,p:longint;
  next,before,a,b:array[0..300005]of longint;
  map:array[0..1005,0..1005]of longint;
begin
  assign(input,'phalanx.in');
  assign(output,'phalanx.out');
  reset(input); rewrite(output);
  readln(n,m,q);
  if n=1 then
    begin
      head:=1; tail:=m;
      for i:=1 to m do  next[i]:=i+1;
      for i:=1 to q do
        begin
          readln(k,p);
          if p=1 then
            begin
              writeln(head);
              next[tail]:=head; tail:=head;
              head:=next[head];
            end
          else
          if p=m then writeln(tail)
          else
            begin
              tot:=1;
              while tot<p do
                begin
                  if tot=p-1 then j:=head;
                  head:=next[head]; inc(tot);
                end;
              writeln(head); next[j]:=next[head];
              next[tail]:=head; tail:=head;
            end;
        end;
    end
  else
    begin
      for i:=1 to n do
        for j:=1 to m do
          map[i,j]:=(i-1)*m+j;
      for i:=1 to q do
        begin
          readln(k,p); a1:=map[k,p];
          writeln(map[k,p]);
          for j:=p+1 to m do
            map[k,j-1]:=map[k,j];
          for j:=k+1 to n do
            map[j-1,m]:=map[j,m];
          map[n,m]:=a1;
        end;
    end;
  close(input);
  close(output);
end.
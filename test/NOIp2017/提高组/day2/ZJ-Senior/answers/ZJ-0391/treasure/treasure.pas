var
  ans,tot,a1,a2,a3:longint;
  min,x,i,j,m,n,k,p:longint;
  map,len:array[0..20,0..20]of longint;
  dis,check:array[0..20]of longint;
procedure dfs(t,tot:longint);
var
  i,j,a1:longint;
begin
  for i:=1 to n do
    if (check[i]=0)and(len[t,i]<>0) then
      begin
        check[i]:=1;
        if (map[t,i]=0)or(map[t,i]>len[t,i]*(tot+1)) then
          begin
            map[t,i]:=len[t,i]*(tot+1); map[i,t]:=map[t,i];
          end;
        dfs(i,tot+1);
        check[i]:=0;
      end;
end;
begin
  assign(input,'treasure.in');
  assign(output,'treasure.out');
  reset(input); rewrite(output);
  readln(n,m);
  for i:=1 to m do
    begin
      readln(a1,a2,a3);
      if (len[a1,a2]=0)or(len[a1,a2]>a3) then
        begin
          len[a1,a2]:=a3; len[a2,a1]:=a3;
        end;
    end;
  ans:=maxlongint;
  for k:=1 to n do
    begin
      fillchar(map,sizeof(map),0);
      fillchar(check,sizeof(check),0);
      check[k]:=1; dfs(k,0);
      for i:=1 to n do dis[i]:=maxlongint div 2;
      dis[1]:=0;
      fillchar(check,sizeof(check),0);  tot:=0;


      for x:=1 to n do
        begin
          min:=maxlongint;
          for i:=1 to n do
            if (check[i]=0)and(min>dis[i]) then
              begin
                min:=dis[i]; j:=i;
              end;
          check[j]:=1; tot:=tot+min;
          if ans<tot then break;
          for i:=1 to n do
            if (check[i]=0)and(map[j,i]<>0)and(dis[i]>map[j,i]) then
              dis[i]:=map[j,i];
        end;
      if ans>tot then ans:=tot;
    end;
  writeln(ans);
  close(input);
  close(output);
end.

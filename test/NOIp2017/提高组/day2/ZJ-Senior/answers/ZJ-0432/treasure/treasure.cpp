#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
using namespace std;
const int N=15;
int a[N][N],x,y,z,n,m,ans,dis[N];
void dfs(int x,int y,int z)
{
	if (y>=ans)return;
	if (x==(1<<n)-1)
	 {
	 	ans=y;
	 	return;
	 }
	for (int i=0;i<n;i++)
	 if ((x&(1<<i))==0)
	  for (int j=1;j<=z;j++)
	   {
	   	 int M=1e9;
	  	 for (int k=0;k<n;k++)
	  	  if (dis[k]==j&&((x&(1<<k))!=0))M=min(M,a[k][i]);
	  	 if (M!=1e9)
		  {
		  	dis[i]=j+1;
		  	dfs(x|(1<<i),y+M*j,max(z,dis[i]));
		  	dis[i]=0;
		  } 
	   }
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=0;i<n;i++) 
	 for (int j=0;j<n;j++)
	  if (i==j)a[i][j]=0;
	  else a[i][j]=1e9;
	ans=1e9;  
	while (m--)
	 {
	 	scanf("%d%d%d",&x,&y,&z);
	 	x--;y--;
	 	a[x][y]=min(a[x][y],z);
	 	a[y][x]=min(a[y][x],z);
	 }
	for (int i=0;i<n;i++)dis[i]=1,dfs(1<<i,0,1),dis[i]=0;
	printf("%d",ans); 
	return 0;
}

#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long ll;
const int N=1005;
int f[N],T,n,dp[N];
ll x[N],y[N],m,r,z[N];
ll sqr(ll x)
{
	return x*x;
}
ll duru()
{
	ll x=0,f=1;char c=getchar();
	for (;c<'0'||c>'9';c=getchar())if (c=='-')f=-1;
	for (;c>='0'&&c<='9';c=getchar())x=x*10+c-48;
	return x*f;
}
int cmp(int x,int y)
{
	return z[x]<z[y];
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	T=duru();
	while (T--)
	 {
		n=duru();m=duru();r=duru();
		for (int i=1;i<=n;i++)
	 	 {
	 		x[i]=duru();y[i]=duru();
	 		z[i]=duru();f[i]=i;
	 	 }
		sort(f+1,f+n+1,cmp);
		memset(dp,0,sizeof dp);
		for (int i=n;i;i--)
	  	 {
	 		if (abs(m-z[f[i]])<=r){dp[i]=1;continue;}
			for (int j=i+1;j<=n;j++)
			 if (sqr(x[f[i]]-x[f[j]])+sqr(z[f[i]]-z[f[j]])<=4*r*r-sqr(y[f[i]]-y[f[j]]))
			  dp[i]|=dp[j];
		 }
		int flag=0; 
		for (int i=1;i<=n;i++)
		 if (abs(z[f[i]])<=r&&dp[i])
		  {
		  	puts("Yes");
		  	flag=1;
		  	break;
		  } 
		if (!flag)puts("No");  
	 }
	return 0; 
}

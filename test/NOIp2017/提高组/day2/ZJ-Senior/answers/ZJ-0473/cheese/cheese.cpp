#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
#define ll long long
#define eps (1e-8)
#define N 10005
struct node
{
	int x,y,z;
}a[N];
int f[N];
int n,h,r;
inline ll sqr(ll x)
{
	return (ll)(x)*x;
}
inline int check(int i,int j)
{
	ll dis=sqr(a[i].x-a[j].x)+sqr(a[i].y-a[j].y)+sqr(a[i].z-a[j].z);
	return dis<=sqr(r+r);
}
inline void init()
{
	for (int i=0;i<=n+1;++i) f[i]=i;
}
inline int find(int x)
{
	if (x==f[x]) return x;
	f[x]=find(f[x]);
	return f[x];
}
inline void unions(int i,int j)
{
	int x=find(i),y=find(j);
	if (x!=y) f[y]=x;
}
void solve()
{
	scanf("%d%d%d",&n,&h,&r);
	init();
	for (int i=1;i<=n;++i) scanf("%d%d%d",&a[i].x,&a[i].y,&a[i].z);
	for (int i=1;i<=n;++i)
	{
		if (a[i].z<=r) unions(0,i);
		if (h-a[i].z<=r) unions(n+1,i);
	}
	for (int i=2;i<=n;++i)
	for (int j=1;j<i;++j)
	if (check(i,j)) unions(i,j);
	if (find(0)==find(n+1)) puts("Yes");else puts("No");
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	int T;
	cin>>T;
	while (T--) solve();
	return 0;
}

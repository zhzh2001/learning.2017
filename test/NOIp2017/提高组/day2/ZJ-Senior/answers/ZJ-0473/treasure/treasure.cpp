#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
#define inf 1000000000
#define ll long long
#define M 10000
#define N 20
int dp[M][N],f[N][N],cost[N];
int n,m,pre,deep;
void dfs(int k,int sta,int x)
{
	if (k>n)
	{
		dp[sta][deep+1]=min((ll)dp[sta][deep+1],(ll)(deep+1)*x+dp[pre][deep]);
		return;
	}
	dfs(k+1,sta,x);
	if ((!(sta&(1<<(k-1)))) && cost[k]!=inf) dfs(k+1,sta|(1<<(k-1)),x+cost[k]);
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	int x,y,t;
	cin>>n>>m;
	for (int i=1;i<=n;++i) for (int j=1;j<=n;++j) f[i][j]=inf;
	for (int i=0;i<(1<<n);++i) for (int j=0;j<=n;++j) dp[i][j]=inf;
	for (int i=1;i<=m;++i)
	{
		scanf("%d%d%d",&x,&y,&t);
		f[x][y]=f[y][x]=min(f[x][y],t);
	}
	for (int i=1;i<=n;++i) dp[1<<(i-1)][0]=0LL;
	for (int dep=0;dep<n-1;++dep)
	for (int i=0;i<(1<<n);++i)
	if (dp[i][dep]!=inf)
	{
		for (int j=1;j<=n;++j)
		if (!(i&(1<<(j-1))))
		{
			cost[j]=inf;
			for (int k=1;k<=n;++k)
			if (i&(1<<(k-1))) cost[j]=min(cost[j],f[j][k]);
		}
		pre=i;deep=dep;
		dfs(1,i,0);
	}
	int ans=inf;
	for (int i=0;i<n;++i) ans=min(ans,dp[(1<<n)-1][i]);
	cout<<ans<<endl;
	return 0;
}

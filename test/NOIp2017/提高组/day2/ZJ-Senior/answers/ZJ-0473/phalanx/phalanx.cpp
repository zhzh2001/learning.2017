#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <algorithm>
using namespace std;
#define ll long long
int q1[300005],q2[300005],q3[300005];
int to[50005],rea[50005];
int n,m,Q,yzh,line;
ll a[505][50005],aa[50005];
void leave(int K)
{
	int x,y,t;
	x=q1[K],y=q2[K];
	t=x;
	x=to[x];
	ll tt=a[x][y];
	printf("%lld\n",tt);
	for (int j=y;j<m;++j) a[x][j]=a[x][j+1];
	for (int i=t;i<n;++i) aa[i]=aa[i+1];
	aa[n]=tt;
	for (int i=1;i<=n;++i) if (to[i]) a[to[i]][m]=aa[i];
}
struct treap
{
	int l,r,sz,rnd;
	ll v;
}tr[1000005];
ll f[1000005],ans;
int rt,tottreap;
inline void update(int k)
{
	int l=tr[k].l,r=tr[k].r;
	tr[k].sz=tr[l].sz+tr[r].sz+1;
}
inline void rturn(int &k)
{
	int t=tr[k].l;tr[k].l=tr[t].r;tr[t].r=k;
	update(k);k=t;update(k);
}
inline void lturn(int &k)
{
	int t=tr[k].r;tr[k].r=tr[t].l;tr[t].l=k;
	update(k);k=t;update(k);
}
void ins(int &k,ll x)
{
	if (!k)
	{
		k=++tottreap;
		tr[k].l=tr[k].r=0;
		tr[k].rnd=rand();
		tr[k].v=x;
		tr[k].sz=1;
		return;
	}
	++tr[k].sz;
	ins(tr[k].r,x);
	if (tr[k].rnd>tr[tr[k].r].rnd) lturn(k);
}
void del(int &k,int x)
{
	if (tr[tr[k].l].sz+1==x)
	{
		if (tr[k].l==0 || tr[k].r==0)
		{
			ans=tr[k].v;
			k=tr[k].l+tr[k].r;
			return;
		}
		if (tr[tr[k].l].rnd<tr[tr[k].r].rnd) rturn(k);else lturn(k);
		del(k,x);
		return;
	}
	--tr[k].sz;
	if (x<=tr[tr[k].l].sz) del(tr[k].l,x);else del(tr[k].r,x-tr[tr[k].l].sz-1);
}
void solve1()
{
	int x,y,t;
	for (int j=1;j<=m;++j) f[j]=j;
	for (int i=1;i<=n;++i) f[m+i-1]=(ll)(i)*m;
	for (int i=1;i<n+m;++i) ins(rt,f[i]);
	for (int i=1;i<=Q;++i)
	{
		del(rt,q3[i]);
		printf("%lld\n",ans);
		ins(rt,ans);
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	cin>>n>>m>>Q;
	if (Q<=500)
	{
		for (int i=1;i<=Q;++i)
		{
			scanf("%d%d",&q1[i],&q2[i]);
			if (!to[q1[i]])
			{
				to[q1[i]]=++line;
				rea[line]=q1[i];
			}
		}
		for (int i=1;i<=n;++i) aa[i]=(ll)(i)*m;
		for (int i=1;i<=line;++i) for (int j=1;j<=m;++j) a[i][j]=(ll)(rea[i]-1)*m+j;
		for (int i=1;i<=Q;++i) leave(i);
		return 0;
	}
	yzh=1;
	for (int i=1;i<=Q;++i) {scanf("%d%d",&q1[i],&q2[i]);q3[i]=q1[i]+q2[i]-1;if (q1[i]!=1) yzh=0;}
	if (yzh)
	{
		solve1();
		return 0;
	}
	return 0;
}

#include<cstdio>
#include<ctime>
#include<algorithm>
using namespace std;
const int n=12,m=1000;
int sum=0;
int main()
{
	srand((unsigned)time(0));
	freopen("treasure.in","w",stdout);
	printf("%d %d\n",n,m);
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			if(i==j) continue;
			sum++;
			printf("%d %d %d\n",i,j,rand());
		}
	}
	while(sum<m)
	{
		sum++;
		int x=rand()%n+1,y=rand()%n+1;
		while(y==x) y=rand()%n+1;
		printf("%d %d %d\n",x,y,rand());
	}
	return 0;
}

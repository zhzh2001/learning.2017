#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,ans=2100000000;
int f[20][20];
int used[20],kk=0;
struct pp
{
	int dep,v;
}p[20],q[20][20];
inline void find(int stp,int sum)
{
	if(sum>=ans) return;
	if(!stp)
	{
		ans=sum;
		return;
	}
	for(int i=1;i<=n;++i) q[stp][i]=p[i];
	for(int i=n;i>=1;--i)
	{
		if(!used[i] && p[i].dep!=-1)
		{
			used[i]=1;
			for(int j=n;j>=1;--j)
			{
				if(f[i][j]!=-1)
				if(p[j].dep==-1||p[j].dep*p[j].v>(p[i].dep+1)*f[i][j])
				{
					p[j].dep=p[i].dep+1;
					p[j].v=f[i][j];
				}
			}
			find(stp-1,sum+p[i].dep*p[i].v);
			used[i]=0;
			for(int j=1;j<=n;j++) p[j]=q[stp][j];
		}
	}
}
int main()
{
	freopen("treasure.in","r",stdin);
	freopen("treasure.out","w",stdout);
	memset(f,-1,sizeof(f));
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		if(f[x][y]==-1||f[x][y]>v) f[x][y]=f[y][x]=v;
	}
	for(int i=n;i>=1;--i)
	{
		for(int j=1;j<=n;j++) p[j].dep=-1,used[j]=0;
		p[i].dep=0;
		used[i]=1;
		for(int j=1;j<=n;j++)
		{
			if(f[i][j]!=-1)
			{
				p[j].dep=1;
				p[j].v=f[i][j];
			}
		}
		find(n-1,0);
	}
	printf("%d",ans);
	return 0;
}

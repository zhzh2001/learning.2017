#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
int T,n,h;
double r;
int hd[10001],nxt[20000001],to[20000001],cnt;
bool vis[10001];
struct pp
{
	long long x,y,z;
}p[10001];
inline double Dis(int a,int b)
{
	return sqrt((p[a].x-p[b].x)*(p[a].x-p[b].x)+(p[a].y-p[b].y)*(p[a].y-p[b].y)+(p[a].z-p[b].z)*(p[a].z-p[b].z));
}
inline void init(int x,int y)
{
	cnt++;
	nxt[cnt]=hd[x];
	hd[x]=cnt;
	to[cnt]=y;
}
inline void find(int x)
{
	vis[x]=1;
	for(int i=hd[x];i;i=nxt[i])
	{
		int u=to[i];
		if(!vis[u]) find(u);
	}
}
int main()
{
	freopen("cheese.in","r",stdin);
	freopen("cheese.out","w",stdout);
	scanf("%d",&T);
	while(T--)
	{
		cnt=0;
		memset(hd,0,sizeof(hd));
		memset(vis,0,sizeof(vis));
		scanf("%d%d%lf",&n,&h,&r);
		for(int i=1;i<=n;i++)
		{
			scanf("%lld%lld%lld",&p[i].x,&p[i].y,&p[i].z);
		}
		for(int i=1;i<=n;i++)
		{
			if(p[i].z-r<=0)
			{
				init(i,0);
				init(0,i);
			}
			if(p[i].z+r>=h)
			{
				init(i,n+1);
				init(n+1,i);
			}
			for(int j=i+1;j<=n;j++)
			{
				if(Dis(i,j)<=2*r)
				{
					init(i,j);
					init(j,i);
				}
			}
		}
		find(0);
		if(vis[n+1]) puts("Yes");
		else puts("No");
	}
	return 0;
}

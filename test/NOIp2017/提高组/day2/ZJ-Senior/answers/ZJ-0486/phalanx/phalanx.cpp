#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int n,m,q;
int a[1001][1001];
long long s[2000001];
struct pp
{
	int left,right,sum,lazy;
}p[3000001];
inline void sol_o(void)
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=m;j++)
		{
			a[i][j]=(i-1)*m+j;
		}
	}
	while(q--)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		printf("%d\n",a[x][y]);
		int t=a[x][y];
		for(int i=y;i<m;i++) a[x][i]=a[x][i+1];
		for(int i=x;i<n;i++) a[i][m]=a[i+1][m];
		a[n][m]=t;
	}
}
inline void cut(int x)
{
	if(!p[x].lazy) return;
	p[x<<1].lazy+=p[x].lazy;
	p[(x<<1)|1].lazy+=p[x].lazy;
	p[x].lazy=0;
}
inline void pl(int l,int r,int x)
{
	p[x].left=l;
	p[x].right=r;
	if(l==r)
	{
		p[x].sum=l;
		return;
	}
	int mid=(l+r)>>1;
	pl(l,mid,x<<1);
	pl(mid+1,r,(x<<1)|1);
}
inline int find(int sum,int x)
{
	if(p[x].left==p[x].right) return p[x].sum+p[x].lazy;
	cut(x);
	if(p[x<<1].right>=sum) return find(sum,x<<1);
	else return find(sum,(x<<1)|1);
}
inline void add(int l,int r,int x)
{
	if(p[x].left==l&&p[x].right==r)
	{
		p[x].lazy++;
		return;
	}
	cut(x);
	int mid=(p[x].left+p[x].right)>>1;
	if(r<=mid) add(l,r,x<<1);
	else if(l>mid) add(l,r,(x<<1)|1);
	else add(l,mid,x<<1),add(mid+1,r,(x<<1)|1);
}
inline void sol_t(void)
{
	pl(1,n+m-1,1);
	for(int i=1;i<m;i++) s[i]=i;
	for(int i=m;i<n+m;i++) s[i]=1ll*(i-m+1)*m;
	int nod=n+m-1,mm=n+m-1;
	while(q--)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		int t=find(y+x-1,1);
		printf("%lld\n",s[t]);
		add(y,nod,1);
		s[++mm]=s[t];
	}
}
int main()
{
	freopen("phalanx.in","r",stdin);
	freopen("phalanx.out","w",stdout);
	scanf("%d%d%d",&n,&m,&q);
	if(n<=1000&&m<=1000) sol_o();
	else sol_t();
//	sol_t();
	return 0;
}

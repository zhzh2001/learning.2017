#include <fstream>
#include <queue>
#include <algorithm>
#include <functional>
using namespace std;
ifstream fin("park.in");
ofstream fout("park.out");
const int N = 100005, M = 200005, K = 51, INF = 0x3f3f3f3f;
int n, m, k, mod, f[N][K];
typedef pair<int, int> state;
struct graph
{
	int head[N], v[M], w[M], nxt[M], e, d[N];
	bool vis[N];
	void reset()
	{
		fill(head + 1, head + n + 1, 0);
		e = 0;
	}
	void insert(int u, int v, int w)
	{
		graph::v[++e] = v;
		graph::w[e] = w;
		nxt[e] = head[u];
		head[u] = e;
	}
	void dijkstra(int s)
	{
		fill(vis + 1, vis + n + 1, false);
		fill(d + 1, d + n + 1, INF);
		d[s] = 0;
		priority_queue<state, vector<state>, greater<state>> Q;
		Q.push(make_pair(0, s));
		while (!Q.empty())
		{
			state k = Q.top();
			Q.pop();
			if (vis[k.second])
				continue;
			vis[k.second] = true;
			for (int i = head[k.second]; i; i = nxt[i])
				if (d[k.second] + w[i] < d[v[i]])
					Q.push(make_pair(d[v[i]] = d[k.second] + w[i], v[i]));
		}
	}
} mat, rmat;
bool vis[N][K];
int dp(int u, int k)
{
	if (vis[u][k])
		return INF;
	if (~f[u][k])
		return f[u][k];
	vis[u][k] = true;
	f[u][k] = u == n;
	for (int i = mat.head[u]; i; i = mat.nxt[i])
	{
		int nk = mat.d[u] + k + mat.w[i] - mat.d[mat.v[i]];
		if (nk <= ::k && mat.d[u] + k + mat.w[i] + rmat.d[mat.v[i]] <= mat.d[n] + ::k)
		{
			int ret = dp(mat.v[i], nk);
			if (ret == INF)
				return INF;
			(f[u][k] += ret) %= mod;
		}
	}
	vis[u][k] = false;
	return f[u][k];
}
int main()
{
	int t;
	fin >> t;
	while (t--)
	{
		fin >> n >> m >> k >> mod;
		mat.reset();
		rmat.reset();
		while (m--)
		{
			int u, v, w;
			fin >> u >> v >> w;
			mat.insert(u, v, w);
			rmat.insert(v, u, w);
		}
		mat.dijkstra(1);
		rmat.dijkstra(n);
		fill_n(&f[0][0], sizeof(f) / sizeof(int), -1);
		fill_n(&vis[0][0], sizeof(vis) / sizeof(bool), false);
		int ans = dp(1, 0);
		if (ans == INF)
			fout << -1 << endl;
		else
			fout << ans << endl;
	}
	return 0;
}
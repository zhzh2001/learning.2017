#include <cstdio>
#include <cctype>
#include <queue>
#include <algorithm>
#include <functional>
using namespace std;
const int N = 100005, M = 200005, K = 51, INF = 0x3f3f3f3f, SZ = 1e6;
int n, m, k, mod, f[N][K];
typedef pair<int, int> state;
char ibuf[SZ], *ip = ibuf, *iend = ibuf;
inline char nextchar()
{
	if (ip == iend)
	{
		iend = ibuf + fread(ibuf, 1, SZ, stdin);
		ip = ibuf;
	}
	return *ip++;
}
template <typename Int>
inline void read(Int &x)
{
	char c;
	for (c = nextchar(); isspace(c); c = nextchar())
		;
	x = 0;
	for (; isdigit(c); c = nextchar())
		x = x * 10 + c - '0';
}
template <typename T, class Compare = less<T>>
struct static_heap
{
  protected:
	T *heap, *p;
	Compare cmp;
	size_t maxN;

  public:
	static_heap(size_t maxN) : maxN(maxN)
	{
		p = heap = new T[maxN];
	}
	~static_heap()
	{
		delete[] heap;
	}
	bool push(const T &val)
	{
		if (p == heap + maxN)
			return false;
		*p++ = val;
		std::push_heap(heap, p, cmp);
		return true;
	}
	const T &top() const
	{
		return *heap;
	}
	bool empty()
	{
		return p == heap;
	}
	bool pop()
	{
		if (empty())
			return false;
		std::pop_heap(heap, p--, cmp);
		return true;
	}
};
struct graph
{
	int head[N], v[M], w[M], nxt[M], e, d[N];
	bool vis[N];
	void reset()
	{
		fill(head + 1, head + n + 1, 0);
		e = 0;
	}
	void insert(int u, int v, int w)
	{
		graph::v[++e] = v;
		graph::w[e] = w;
		nxt[e] = head[u];
		head[u] = e;
	}
	void dijkstra(int s)
	{
		fill(vis + 1, vis + n + 1, false);
		fill(d + 1, d + n + 1, INF);
		d[s] = 0;
		static_heap<state, greater<state>> Q(e);
		Q.push(make_pair(0, s));
		while (!Q.empty())
		{
			state k = Q.top();
			Q.pop();
			if (vis[k.second])
				continue;
			vis[k.second] = true;
			for (int i = head[k.second]; i; i = nxt[i])
				if (d[k.second] + w[i] < d[v[i]])
					Q.push(make_pair(d[v[i]] = d[k.second] + w[i], v[i]));
		}
	}
} mat, rmat;
bool vis[N][K];
int dp(int u, int k)
{
	if (vis[u][k])
		return INF;
	if (~f[u][k])
		return f[u][k];
	vis[u][k] = true;
	f[u][k] = u == n;
	for (int i = mat.head[u]; i; i = mat.nxt[i])
	{
		int nk = mat.d[u] + k + mat.w[i] - mat.d[mat.v[i]];
		if (nk <= ::k && mat.d[u] + k + mat.w[i] + rmat.d[mat.v[i]] <= mat.d[n] + ::k)
		{
			int ret = dp(mat.v[i], nk);
			if (ret == INF)
				return INF;
			(f[u][k] += ret) %= mod;
		}
	}
	vis[u][k] = false;
	return f[u][k];
}
int main()
{
	int t;
	read(t);
	while (t--)
	{
		read(n);
		read(m);
		read(k);
		read(mod);
		mat.reset();
		rmat.reset();
		while (m--)
		{
			int u, v, w;
			read(u);
			read(v);
			read(w);
			mat.insert(u, v, w);
			rmat.insert(v, u, w);
		}
		mat.dijkstra(1);
		rmat.dijkstra(n);
		fill_n(&f[0][0], sizeof(f) / sizeof(int), -1);
		fill_n(&vis[0][0], sizeof(vis) / sizeof(bool), false);
		int ans = dp(1, 0);
		if (ans == INF)
			puts("-1");
		else
			printf("%d\n", ans);
	}
	return 0;
}
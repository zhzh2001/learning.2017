@echo off
for /l %%i in (1,1,100000) do (
	echo No.%%i
	gen.exe
	phalanx.exe
	phalanx_bf.exe
	fc phalanx.out phalanx.ans
	if errorlevel 1 pause
)
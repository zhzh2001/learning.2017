#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("phalanx.in");
ofstream fout("phalanx.out");
const int N = 300005, L = 12e6;
struct node
{
	int sum, ls, rs;
} tree[L];
int cc, root[N], last;
vector<long long> mat[N], ml;
int query(int id, int l, int r, int x)
{
	if (l == r)
		return l;
	int mid = (l + r) / 2;
	if (mid - tree[tree[id].ls].sum >= x)
		return query(tree[id].ls, l, mid, x);
	return query(tree[id].rs, mid + 1, r, x + tree[tree[id].ls].sum);
}
void modify(int &id, int l, int r, int x)
{
	if (!id)
		id = ++cc;
	if (l == r)
		tree[id].sum++;
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(tree[id].ls, l, mid, x);
		else
			modify(tree[id].rs, mid + 1, r, x);
		tree[id].sum = tree[tree[id].ls].sum + tree[tree[id].rs].sum;
	}
}
int main()
{
	int n, m, q;
	fin >> n >> m >> q;
	for (int i = 1; i <= n; i++)
		mat[i].push_back(1ll * i * m);
	ml.push_back(1ll * n * m);
	int r = max(n, m) + q;
	while (q--)
	{
		int x, y;
		fin >> x >> y;
		long long tmp = query(last, 1, r, x);
		if (tmp <= n)
			tmp = 1ll * tmp * m;
		else
			tmp = ml[tmp - n];
		if (m + mat[x].size() - 1 - tree[root[x]].sum == m)
			modify(root[x], 1, r, m + mat[x].size() - 1);
		mat[x].push_back(tmp);
		long long num = query(root[x], 1, r, y);
		modify(root[x], 1, r, num);
		if (num <= m)
			num = 1ll * (x - 1) * m + num;
		else
			num = mat[x][num - m];
		modify(last, 1, r, query(last, 1, r, x));
		ml.push_back(num);
		fout << num << '\n';
	}
	return 0;
}
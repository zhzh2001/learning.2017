#include <fstream>
#include <random>
#include <windows.h>
using namespace std;
ofstream fout("phalanx.in");
const int n = 1000, q = 1000;
int main()
{
	minstd_rand gen(GetTickCount());
	fout << n << ' ' << n << ' ' << q << endl;
	for (int i = 1; i <= q; i++)
	{
		uniform_int_distribution<> d(1, n);
		fout << d(gen) << ' ' << d(gen) << endl;
	}
	return 0;
}
#include <fstream>
#include <queue>
using namespace std;
ifstream fin("phalanx.in");
ofstream fout("phalanx.ans");
const int N = 300005;
long long last[N], *a[N];
int main()
{
	int n, m, q;
	fin >> n >> m >> q;
	for (int i = 1; i <= n; i++)
		last[i] = 1ll * i * m;
	while (q--)
	{
		int x, y;
		fin >> x >> y;
		if (!a[x])
		{
			a[x] = new long long[m + 1];
			for (int i = 1; i < m; i++)
				a[x][i] = 1ll * (x - 1) * m + i;
		}
		a[x][m] = last[x];
		long long tmp = a[x][y];
		for (int i = y + 1; i <= m; i++)
			a[x][i - 1] = a[x][i];
		for (int i = x + 1; i <= n; i++)
			last[i - 1] = last[i];
		last[n] = tmp;
		fout << tmp << endl;
	}
	for (int i = 1; i <= n; i++)
		if (a[i])
			delete[] a[i];
	return 0;
}

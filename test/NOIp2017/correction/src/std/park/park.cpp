#include <climits>
#include <cstdio>
#include <cstring>
#include <cstdlib>

#include <algorithm>
#include <functional>
#include <queue>

using namespace std;

struct node
{
	int u, step;

	node() {}

	node(int u, int step) : u(u), step(step) {}

	bool operator>(const node &x) const
	{
		return step > x.step;
	}
};

struct edge
{
	int v, w;
	edge *next;
};

const int N = 100010;

int n, m, k, p, sp;
edge *pp;
int dis1[N], dis2[N];
edge *g1[N], *g2[N];
edge pool[N << 2];
int dp[N][51];
bool vis[N][51];

void add_edge(edge **gp, int u, int v, int w);
void dij(int *disp, edge **gp, int u);
bool dfs(int u, int ck);

int main()
{
	freopen("park.in", "r", stdin);
	freopen("park.out", "w", stdout);
	int t, u, v, w;
	scanf("%d", &t);
	while (t--)
	{
		pp = pool;
		memset(dis1, -1, sizeof(dis1));
		memset(dis2, -1, sizeof(dis2));
		memset(g1, 0, sizeof(g1));
		memset(g2, 0, sizeof(g2));
		memset(dp, -1, sizeof(dp));
		memset(vis, 0, sizeof(vis));

		scanf("%d%d%d%d", &n, &m, &k, &p);
		while (m--)
		{
			scanf("%d%d%d", &u, &v, &w);
			add_edge(g1, u, v, w);
			add_edge(g2, v, u, w);
		}

		dij(dis1, g1, 1);
		dij(dis2, g2, n);

		sp = dis2[1];
		if (dfs(1, 0))
			puts("-1");
		else
			printf("%d\n", dp[1][0]);
	}
	return 0;
}

inline void add_edge(edge **gp, int u, int v, int w)
{
	pp->v = v;
	pp->w = w;
	pp->next = gp[u];
	gp[u] = pp;
	++pp;
}

inline void dij(int *disp, edge **gp, int u)
{
	priority_queue<node, vector<node>, greater<node>> q;
	node t;
	disp[u] = 0;
	q.push(node(u, 0));
	while (!q.empty())
	{
		t = q.top();
		q.pop();
		u = t.u;
		if (disp[u] < t.step)
			continue;
		for (edge *p = gp[u]; p; p = p->next)
			if (disp[p->v] == -1 || disp[p->v] > disp[u] + p->w)
			{
				disp[p->v] = disp[u] + p->w;
				q.push(node(p->v, disp[p->v]));
			}
	}
}

bool dfs(int u, int ck)
{
	if (vis[u][ck])
		return true;
	if (~dp[u][ck])
		return false;
	vis[u][ck] = true;

	if (u == n)
		dp[u][ck] = 1;
	else
		dp[u][ck] = 0;
	int cp;
	for (edge *p = g1[u]; p; p = p->next)
	{
		cp = dis1[u] + p->w + ck - dis1[p->v];
		if (cp <= k && dis1[u] + ck + p->w + dis2[p->v] - sp <= k)
		{
			if (dfs(p->v, cp))
				return true;
			dp[u][ck] += dp[p->v][cp];
			if (dp[u][ck] >= ::p)
				dp[u][ck] -= ::p;
		}
	}

	vis[u][ck] = false;
	return false;
}
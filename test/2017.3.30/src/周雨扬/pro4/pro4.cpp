#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
int head[105],dis[105],vis[105];
int n,m,k,x,y,z,S,T,tot;
struct edge{int to,next,v;}e[20005];
struct node{
	int now,d,vis[105];
	node(){memset(vis,0,sizeof(vis));}
	friend bool operator < (node x,node y){
		return x.d+dis[x.now]>y.d+dis[y.now];
	}
};
void add(int x,int y,int v){
	e[++tot]=(edge){y,head[x],v};
	head[x]=tot;
	e[++tot]=(edge){x,head[y],v};
	head[y]=tot;
}
void spfa(){
	queue<int> q;
	memset(dis,30,sizeof(dis));
	dis[T]=0; vis[T]=1;
	q.push(T);
	while (!q.empty()){
		int x=q.front();
		q.pop();
		vis[x]=0;
		for (int i=head[x];i;i=e[i].next)
			if (dis[e[i].to]>dis[x]+e[i].v){
				dis[e[i].to]=dis[x]+e[i].v;
				if (!vis[e[i].to]){
					vis[e[i].to]=1;
					q.push(e[i].to);
				}
			}
	}
}
void work(){
	node x;
	x.now=S; x.d=0; x.vis[S]=1;
	priority_queue<node> q;
	q.push(x);
	while (!q.empty()){
		x=q.top(); q.pop();
		if (x.now==T){
			k--;
			if (!k){
				printf("%d",x.d);
				exit(0);
			}
			continue;
		}
		for (int i=head[x.now];i;i=e[i].next)
			if (!x.vis[e[i].to]){
				node y=x;
				y.now=e[i].to;
				y.d+=e[i].v;
				y.vis[e[i].to]=1;
				q.push(y);
			}
	}
}
int main(){
	freopen("pro4.in","r",stdin);
	freopen("pro4.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		add(x,y,z);
	}
	scanf("%d%d",&S,&T);
	spfa();
	work();
}

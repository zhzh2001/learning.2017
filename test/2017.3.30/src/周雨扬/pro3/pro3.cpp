#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
int a[305],b[305],len,n,m,mx,ans,sum,k;
inline int erfen(int x){
	int l=0,r=n+1,ans=0;
	while (l<=r){
		int mid=(l+r)/2;
		if (b[mid]<=x) ans=mid,l=mid+1;
		else r=mid-1;
	}
	return ans;
}
int main(){
	freopen("pro3.in","r",stdin);
	freopen("pro3.out","w",stdout);
	scanf("%d%d",&len,&m);
	for (int i=1;i<=m;i++) scanf("%d",&a[i]);
	scanf("%d",&n);
	for (int i=2;i<=n;i++) scanf("%d",&b[i]);
	b[1]=0;
	for (int i=1;i<=n;i++) b[i]*=2;
	for (int i=1;i<=n;i++) a[i]*=2;
	b[0]=-100000; b[n+1]=100000;
	mx=ans=0;
	for (int i=0;b[n]<=len*2;i++){
		sum=0;
		for (int j=1;j<=m;j++){
			k=erfen(a[j]);
			sum+=min(b[k+1]-a[j],a[j]-b[k]);
		}
		if (sum>ans) ans=sum,mx=i;
		for (int j=1;j<=n;j++)b[j]++;
	}
	printf("%.3lf %.3lf",(double)mx/2,(double)ans/2);
}

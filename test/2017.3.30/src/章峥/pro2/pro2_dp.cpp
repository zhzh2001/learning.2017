#include<fstream>
using namespace std;
ifstream fin("pro2.in");
ofstream fout("pro2.out");
const int N=11,BITS=1<<10;
int n,m;
long long f[N][BITS];
void dp(int k,int pred,int now,long long ans)
{
	if(pred==(1<<m)-1)
	{
		f[k+1][now]+=ans;
		return;
	}
	int i;
	for(i=0;i<m&&pred&(1<<i);i++);
	pred|=1<<i;
	if(i<m&&~pred&(1<<i+1))
	{
		int t=pred|(1<<i+1);
		dp(k,t,now,ans);
		if(~now&(1<<i))
			dp(k,t,now|(1<<i),ans);
		if(~now&(1<<i+1))
			dp(k,t,now|(1<<i+1),ans);
	}
	if(~now&(1<<i))
	{
		now|=1<<i;
		dp(k,pred,now,ans);
		if(i<m&&~now&(1<<i+1))
			dp(k,pred,now|(1<<i+1),ans);
		if(i&&~now&(1<<i-1))
			dp(k,pred,now|(1<<i-1),ans);
	}
}
int main()
{
	fin>>n>>m;
	f[1][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=0;j<1<<m;j++)
			if(f[i][j])
				dp(i,j,0,f[i][j]);
	fout<<f[n+1][0]<<endl;
	return 0;
}
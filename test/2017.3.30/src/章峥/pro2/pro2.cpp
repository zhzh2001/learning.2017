#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("pro2.in");
ofstream fout("pro2.out");
const int N=9;
const long long ans[N][N]={{0},{1,2},{0,5,8},{1,11,55,380},{0,24,140,2319,21272},{1,53,633,15171,262191,5350806},{0,117,1984,96139,2746048,100578811,3238675344},{1,258,7827,619773,31411948,1973546988,111496884663,6652506271144},{0,569,26676,3962734,342302244,37873593799,3704964324320,385840008972355,38896105985522272}};
int main()
{
	int n,m;
	fin>>n>>m;
	if(n<m)
		swap(n,m);
	fout<<ans[n-1][m-1]<<endl;
	return 0;
}
#include<fstream>
#include<cstring>
#include<queue>
#include<bitset>
using namespace std;
ifstream fin("pro4.in");
ofstream fout("pro4.out");
const int N=105,INF=0x3f3f3f3f;
int n,k,s,t,mat[N][N],f[N][N],cnt;
struct node
{
	int v,g,f;
	bitset<N> vis;
	node(){}
	node(int v,int g,int f,const bitset<N>& vis):v(v),g(g),f(f),vis(vis){}
	bool operator>(const node& rhs)const
	{
		if(f!=rhs.f)
			return f>rhs.f;
		return g>rhs.g;
	}
};
template<typename T,class Compare=less<T> >
struct static_heap
{
	protected:
	
		T *heap,*p;
		Compare cmp;
		size_t maxN;
	
	public:
	
	static_heap(size_t maxN):maxN(maxN)
	{
		p=heap=new T [maxN];
	}
	
	~static_heap()
	{
		delete [] heap;
	}
	
	bool push(const T& val)
	{
		if(p==heap+maxN)
			return false;
		*p++=val;
		push_heap(heap,p,cmp);
		return true;
	}
	
	const T& top()const
	{
		return *heap;
	}
	
	bool empty()
	{
		return p==heap;
	}
	
	bool pop()
	{
		if(empty())
			return false;
		pop_heap(heap,p--,cmp);
		return true;
	}
};
int main()
{
	int m;
	fin>>n>>m>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	memcpy(f,mat,sizeof(f));
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	fin>>s>>t;
	static_heap<node,greater<node> > Q(1500000);
	bitset<N> tmp;
	tmp[s]=true;
	Q.push(node(s,0,f[s][t],tmp));
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(k.v==t)
		{
			::k--;
			if(!::k)
			{
				fout<<k.g<<endl;
				break;
			}
		}
		for(int i=1;i<=n;i++)
			if(!k.vis[i]&&mat[k.v][i]<INF)
			{
				node now(i,k.g+mat[k.v][i],k.g+mat[k.v][i]+f[i][t],k.vis);
				now.vis[i]=true;
				Q.push(now);
			}
	}
	return 0;
}
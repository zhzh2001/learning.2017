#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("pro4.in");
ofstream fout("pro4.out");
const int N=105,INF=0x3f3f3f3f;
int n,k,s,t,mat[N][N],f[N][N],cnt;
bool vis[N];
void dfs(int k,int dist,int maxl)
{
	if(dist+f[k][t]>maxl)
		return;
	if(k==t)
	{
		cnt++;
		return;
	}
	vis[k]=true;
	for(int i=1;i<=n;i++)
		if(!vis[i]&&mat[k][i]<INF)
		{
			dfs(i,dist+mat[k][i],maxl);
			if(cnt>=::k)
				return;
		}
	vis[k]=false;
}
int main()
{
	int m;
	fin>>n>>m>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	memcpy(f,mat,sizeof(f));
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	fin>>s>>t;
	int l=0,r=INF;
	while(l<r)
	{
		int mid=(l+r)/2;
		cnt=0;
		memset(vis,false,sizeof(vis));
		dfs(s,0,mid);
		if(cnt>=::k)
			r=mid;
		else
			l=mid+1;
	}
	fout<<r<<endl;
	return 0;
}
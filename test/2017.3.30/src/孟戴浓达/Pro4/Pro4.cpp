//#include<iostream>
#include<queue>
#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("Pro4.in");
ofstream fout("Pro4.out");
int i,j=0,x,y,w,ans,n,m,s,t,k;
struct edge{
	int to,w,nxt;
}e[20003],ef[20003];
int p[103],pf[103],q[10010],h[103],g[103],tm[103];
bool vis[103];
void add(int x,int y,int w){
	e[j].to=y;       e[j].w=w;
	e[j].nxt=p[x];   p[x]=j;
	ef[j].to=x;      ef[j].w=w;
	ef[j].nxt=pf[y]; pf[y]=j++;
}
struct astar{
	int w,d,i;
	astar(int ww,int dd,int ii){
		w=ww;d=dd;i=ii;
	}
	friend bool operator<(astar a1,astar a2){
		return a1.w>a2.w;
	}
};
void spfa(){
	int tail=0,head=0;
	q[++tail]=t;
	for(int i=1;i<=n;i++){
		if(i!=t){
			h[i]=999999999;
		}
	}
	while(head!=tail){
		head=(head+1)%10000;
		int u=q[head];
		vis[u]=false;
		for(int x=pf[u];x!=-1;x=ef[x].nxt){
			int v=ef[x].to;
			int w=ef[x].w;
			if(h[v]>h[u]+w){
				h[v]=h[u]+w;
				if(!vis[v]){
					tail=(tail+1)%10000;
					q[tail]=v; vis[v]=true;
				}
			}
		}
	}
}
void kshort(){
	if(s==t){
		k++;
	}
	priority_queue<astar> dui;
	astar hehe=astar(h[s],0,s);
	dui.push(hehe);
	int ans=-1;
	while(!dui.empty()){
		int u=dui.top().i;
		int d=dui.top().d;
		int w=dui.top().w;
		dui.pop();
		tm[u]++;
		if(tm[u]>k){
			continue;
		}
		if(tm[t]==k){
			fout<<w<<endl;
			return;
		}
		for(int x=p[u];x!=-1;x=e[x].nxt){
			int v=e[x].to,ww=e[x].w;
			astar hehe=astar(d+ww+h[v],d+ww,v);
			dui.push(hehe);
		}
	}
}
int main(){
	memset(p,-1,sizeof(p));
	memset(pf,-1,sizeof(pf));
	fin>>n>>m>>k;
	for(int i=1;i<=m;i++){
		fin>>x>>y>>w;
		add(x,y,w);
		add(y,x,w);
	}
	fin>>s>>t;
	spfa();
	kshort();
	return 0;
}
/*

in:
5 10 3
1 2 6
1 3 13
1 4 18
1 5 35
2 3 14
2 4 34
2 5 17
3 4 22
3 5 15
4 5 34
1 5

out:
35

*/

//#include<iostream>
#include<fstream>
#include<iomanip>
using namespace std;
ifstream fin("Pro3.in");
ofstream fout("Pro3.out");
double anspos,ans=-1;
int len,m,n;
double p[303],d[303];
double min(double a,double b){
	if(a<b) return a;
	return b;
}
double abs(double x){
	if(x<0.0)return -x;
	return x;
}
int main(){
	fin>>len>>m;
	for(int i=1;i<=m;i++) fin>>p[i];
	fin>>n;
	for(int i=2;i<=n;i++) fin>>d[i];
	d[1]=0;
	for(double i=0;i<=len-d[n];i+=0.1){
		int x=1;
		double ans2=0;
		for(int j=1;j<=m;j++){
			while(((d[x]+i)<p[j])&&((d[x+1]+i)<p[j])&&(x<n)) x++;
			ans2+=min(abs(p[j]-(d[x]+i)),abs((d[x+1]+i)-p[j]));
		}
		if(ans2>ans) anspos=i,ans=ans2;
	}
	fout<<fixed<<setprecision(3)<<anspos<<" "<<ans<<endl;
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
4
5
0 1 2 3 4
4
1 2 3

out:
0.500 2.500

*/

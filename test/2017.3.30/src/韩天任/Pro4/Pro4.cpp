#include<bits/stdc++.h>
using namespace std;
#define INF 0xffffff
struct node{
	int to;
	int val;
	int next;
};
struct node2{
	int to;
	int g,f;
	bool operator<(const node2 &r )const{
		if (r.f==f)
			return r.g<g;
		return r.f<f;
	} 
};
node lu[10000],lu2[10000];
int s,n,m,g,t,k,cnt,cnt2,ans;
int dis[1010],vist[1010],head[1010],head2[1010];
void jl(int from,int to,int val){
	lu[cnt].to=to;
	lu[cnt].val=val;
	lu[cnt].next=head[from];
	head[from]=cnt++;
}
void jl2(int from,int to,int val){
	lu2[cnt].to=to;
	lu2[cnt].val=val;
	lu2[cnt].next=head2[from];
	head2[from]=cnt++;
}
bool spfa(int s,int n,int head[],node lu[],int dist[]){
	queue<int>Q1;
	int inq[1010];
	for (int i=0;i<=n;i++){
		dis[i]=INF;
		inq[i]=0;
	}
	dis[s]=0;
	Q1.push(s);
	inq[s]++;
	while ((!Q1.empty())){
		int q=Q1.front();
		Q1.pop();
		inq[q]--;
		if (inq[q]>n)
			return false;
		int k=head[q];
		while (k>=0){
			if (dist[lu[k].to]>dist[q]+lu[k].val){
				dist[lu[k].to]=lu[k].val+dist[q];
				if (!inq[lu[k].to]){
					inq[lu[k].to]++;
					Q1.push(lu[k].to);
				}
			}
			k=lu[k].next;
		}
	}
	return true;
}
int star(int s,int t,int n,int k,int head[],node lu[],int dist[]){
	node2 e,ne;
	int cnt=0;
	priority_queue<node2>Q;
	if (s==t)
		k++;
	if  (dis[s]=INF)
		return -1;
	e.to=s;
	e.g=0;
	e.f=e.g+dis[e.to];
	Q.push(e);
	while (!Q.empty()){
		e=Q.top();
		Q.pop();
		if (e.to==t){
			cnt++;
		}
		if  (cnt==k)
			return e.g;
		for (int i=head[e.to];i!=-1;i=lu[i].next){
			ne.to=lu[i].to;
			ne.g=e.g+lu[i].val;
			ne.f=ne.g+dis[ne.to];
			Q.push(ne);
		}
	}
	return -1;
}
int main(){
	cin>>n>>m>>k;
	cnt=cnt2=1;
	for (int i=1;i<=m;i++){
		int a,b,c;
		cin>>a>>b>>c;
		jl(a,b,c);
		jl2(b,a,c);
	}
	cin>>s>>t;
	spfa(t,n,head2,lu2,dis);
	ans=star(s,t,n,k,head,lu,dis);
	cout<<ans;
	return 0;		
}

#include<string.h>
#include<stdio.h>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
int cnt,head[101],a[100000],S,T,n,m,K;
struct edge{
	int next,to,val;
}e[100001];
inline void ins(int x,int y,int z){
	e[++cnt].to=y;e[cnt].next=head[x];head[x]=cnt;
	e[cnt].val=z;
}
bool vis[101];
inline void dfs(int x,int sum){
	if(x==T){
		a[++a[0]]=sum;
		return;
	}
	for(int i=head[x];i;i=e[i].next){
		if(vis[e[i].to]) continue;
		vis[e[i].to]=1;
		dfs(e[i].to,sum+e[i].val);
		vis[e[i].to]=0;
	}
}
int main(){
	freopen("Pro4.in","r",stdin);
	freopen("Pro4.out","w",stdout);
	scanf("%d%d%d",&n,&m,&K);
	for(int i=1;i<=m;i++){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		ins(x,y,z);ins(y,x,z);
	}
	scanf("%d%d",&S,&T);
	vis[S]=1;dfs(S,0);
	sort(a+1,a+1+a[0]);
	printf("%d",a[K]);
}

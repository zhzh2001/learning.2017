#include <cstdio>
#include <cmath>
#include <algorithm>
using namespace std ;

struct node {
	int to,val,pre ;
};

node e[20016] ; 
bool vis[116] ;
int n,m,cnt,k,s,t,x,y,val,tot,sum ;
int a[20016],head[116] ;

inline void addedge(int x,int y,int val)
{
	cnt++;
	e[cnt].to = y;
	e[cnt].val = val;
	e[cnt].pre = head[x] ;
	head[x] = cnt ;
}

inline void dfs(int u) 
{
	if(u==t) 
	{
		tot++;
		a[tot] = sum ;
		return ;
	}
	int v ;
	for(int i=head[u];i;i=e[i].pre ) 
	{
		v = e[i].to ;
		if(vis[v]) continue ;
		vis[ v ] = 1;
		sum = sum+e[ i ].val ;
		dfs(v) ;
		vis[ v ] = 0;
		sum = sum-e[ i ].val ;		 
	}
}

int main()
{
	freopen("Pro4.in","r",stdin) ;
	freopen("Pro4.out","w",stdout) ;
	scanf("%d%d%d",&n,&m,&k) ; 
	for(int i=1;i<=m;i++) 
	{
		scanf("%d%d%d",&x,&y,&val) ;
		addedge(x,y,val) ;
		addedge(y,x,val) ;
	}
	scanf("%d%d",&s,&t) ;
	vis[s] = 1;
	dfs(s) ;
	sort(a+1,a+tot+1) ;
	printf("%d",a[k] ) ;
	return 0 ;
}

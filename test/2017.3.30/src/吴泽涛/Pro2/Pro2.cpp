#include <cstdio>
#define ll long long 
using namespace std ;

int n,m,x,y,ans ;
int  f[10][10] ;  

inline void nex(int &x,int &y) 
{
	if(y<m) 
	{
		y++;
		return ;
	} 
	if(y==m&&x<n) 
	{
		x++;
		y = 1;
		return ;
	}
	if(x==n&&y==m) 
	{
		x = -1 ;  y = -1;    return ;        //
	}
}

inline bool check() 
{
	for(int i=1;i<=n;i++) 
	  for(int j=1;j<=m;j++) 
	    if(!f[i][j])  return false ;
	return true ;
}

inline void dfs(int x,int y) 
{
	int xx,yy ;
	if(x==-1&&y==-1) 
	{
		if(check()) ans++;
		return ;
	}
	
  	while (f[x][y]&&x!=-1&&y!=-1) 
	{
		nex(x,y) ;
	}
	if(x==-1&&y==-1) 
	{
		if(check()) ans++;
		return ;
	}
	for(int i=1;i<=9;i++) 
	{
		if(i==1&&y<m&&x>1)   
		{
			if(f[x][y+1]||f[x-1][y]) continue ;
			f[x][y+1] = 1;  f[x-1][y] = 1;
			f[x][y] = 1;
			xx = x;  yy = y;
			nex(xx,yy) ;
			dfs(xx,yy) ;
			f[x][y+1] = 0;  f[x-1][y] = 0;
			f[x][y] = 0;
		}
		if(i==2&&y<m&&x<n) 
		{
			if(f[x+1][y]&&f[x][y+1]) continue ;
			f[x][y+1] = 1;  f[x+1][y] = 1;
			f[x][y] = 1;
			xx = x;    yy = y;
			nex(xx,yy) ;
			dfs(xx,yy) ;
			f[x][y+1] = 0 ; f[x+1][y] = 0; 
			f[x][y] = 0;
		}  
		if(i==3&&x<n&&y>1) 
		{
			if(f[x][y-1]||f[x+1][y]) continue ;
			f[x][y-1] = 1;  f[x+1][y] = 1;
			f[x][y] = 1;
			xx = x;    yy = y;
			nex(xx,yy) ;
			dfs(xx,yy) ;
			f[x][y-1] = 0 ; f[x+1][y] = 0; 
			f[x][y] = 0;
		}
		if(i==4&&x>1&&y>1) 
		{
			if(f[x][y-1]||f[x-1][y]) continue ;
			f[x][y-1] = 1;   f[x-1][y] = 1;
			f[x][y] = 1;
			xx = x;   yy = y ;
			nex(xx,yy) ;
			dfs(xx,yy) ;
			f[x][y-1] = 0 ;  f[x-1][y] = 0 ;
			f[x][y] = 0;
 		}
 		if(i==5&&x<n) 
		 {
		 	if(f[x+1][y]) continue ;
		 	f[x+1][y] = 1;
		 	f[x][y] = 1;
		 	xx = x;   yy = y;
		 	nex(xx,yy) ;
		 	dfs(xx,yy) ;
		 	f[x+1][y] = 0 ;
		 	f[x][y] = 0;
		 } 
		 if(i==6&&y<m) 
		 {
		 	if(f[x][y+1]) continue ;
		 	f[x][y+1] = 1;
		 	f[x][y] = 1;
		 	xx = x;
		 	yy = y;
		 	nex(xx,yy) ;
		 	dfs(xx,yy) ;
		 	f[x][y+1] = 0 ; 
		 	f[x][y] = 0;
		 } 
		 if(i==7) 
		 {
		 	xx = x;   yy = y;
		 	nex(xx,yy) ;   dfs(xx,yy) ;
		 }
		 if(i==8&&x>1) 
		 {
		 	if(f[x-1][y]) continue ;
		 	f[x-1][y] = 1;
		 	f[x][y] = 1;
		 	xx = x;   yy = y;
		 	nex(xx,yy) ;
		 	dfs(xx,yy) ;
		 	f[x-1][y] = 0 ;
		 	f[x][y] = 0;
		 } 
		 if(i==9&&y>1) 
		 {
		 	if(f[x][y-1]) continue ;
		 	f[x][y-1] = 1;
		 	f[x][y] = 1;
		 	xx = x;
		 	yy = y;
		 	nex(xx,yy) ;
		 	dfs(xx,yy) ;
		 	f[x][y-1] = 0 ; 
		 	f[x][y] = 0;
		 }
	}
}

int main() 
{
	freopen("Pro2.in","r",stdin) ;
	freopen("Pro2.out","w",stdout) ;
	scanf("%d%d",&n,&m) ;
	int ans=1;
	printf("5\n") ;
	return 0 ;
	dfs(1,1) ;
	printf("%d",ans) ;
	return 0 ;
}

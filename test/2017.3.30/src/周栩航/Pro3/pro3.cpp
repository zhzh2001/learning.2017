#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
ll t[5001],ta[5001],d[5001],a[5001];
ll n,m,len,ans,ans1;
inline int get(int x)
{
	int l=1,r=n;
	int tmp=0;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(t[mid]<=x)	tmp=max(mid,tmp);
		if(t[mid]<x)	l=mid+1;else r=mid-1;
	}
	if(tmp==0)	return 1;
	return tmp;
}
inline void doit(int x)
{
	t[1]=x;
	For(i,2,n)
		t[i]=t[1]+d[i]*1000;
	t[n+1]=t[0]=1e9;
	For(i,1,m)
		ta[i]=a[i]*1000;
	ll sum=0;
	For(i,1,m)
	{
		int p=get(ta[i]);
		
		sum+=min(abs(ta[i]-t[p]),abs(t[p+1]-ta[i]));//cout<<ta[i]<<' '<<t[p]<<' '<<t[p+1]<<' '<<sum<<endl;
	}
	if(sum>ans)
	{
		ans1=x;
		ans=sum;
	}
}
int main()
{
	freopen("pro3.in","r",stdin);
	freopen("pro3.out","w",stdout);
	scanf("%d%d",&len,&m);
	For(i,1,m)	scanf("%d",&a[i]);
	scanf("%d",&n);
	For(i,2,n)	scanf("%d",&d[i]);
	if(len<=100)
	For(i,0,(len-d[n])*1000)
	{
		doit(i);
	}
	else 
		for(int i=0;i<=(len-d[n])*1000;i+=500)
			doit(i);
	printf("%.3f %.3f",(double)ans1/1000,(double)ans/1000);
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<cstring>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int mo=1e5+7;
int b[]={0,1,2,5,11,24,53,117,258,569,1255};
int vis[101][101],n,m,ans;
int hash[300000];
inline int change()
{
	ll sum=1;
	For(i,1,n)
		For(j,1,m)
			sum=sum*7+vis[i][j],sum%=mo;
	return sum;
}
inline int h(int x)
{
	int t=x%mo;
	while(hash[t]&&hash[t]!=x)	t++;
	if(hash[t]==x)	return 1;
	hash[t]=x;return 0;
}
inline void  dfs(int x,int y)
{
	bool flag=0;
		if(!vis[x][y]&&!vis[x+1][y]&&!vis[x][y+1])
		{
			vis[x][y]=vis[x+1][y]=vis[x][y+1]=1;
			bool flag=0;
			For(i,1,n)
				For(j,1,m)
					if(!vis[i][j]||(!vis[i+1][j+1]&&!vis[i][j+1]&&!vis[i+1][j]))	dfs(i,j),flag=1;
			if(!flag){if(!h(change()))ans++;vis[x][y]=vis[x+1][y]=vis[x][y+1]=0;return;}
			vis[x][y]=vis[x+1][y]=vis[x][y+1]=0;
			
		}
		if(!vis[x][y]&&!vis[x][y+1]&&!vis[x+1][y+1])
		{
			vis[x][y]=vis[x][y+1]=vis[x+1][y+1]=2;
			bool flag=0;
			For(i,1,n)
				For(j,1,m)
					if(!vis[i][j]||(!vis[i+1][j+1]&&!vis[i][j+1]&&!vis[i+1][j]))	dfs(i,j),flag=1;
			if(!flag){if(!h(change()))ans++;vis[x][y]=vis[x][y+1]=vis[x+1][y+1]=0;return;}
			vis[x][y]=vis[x][y+1]=vis[x+1][y+1]=0;
		}
		if(!vis[x][y]&&!vis[x+1][y]&&!vis[x+1][y+1])
		{
			vis[x][y]=vis[x+1][y]=vis[x+1][y+1]=3;
			bool 	flag=0;
			For(i,1,n)
				For(j,1,m)
					if(!vis[i][j]||(!vis[i+1][j+1]&&!vis[i][j+1]&&!vis[i+1][j]))	dfs(i,j),flag=1;
			if(!flag){if(!h(change()))ans++;vis[x][y]=vis[x+1][y]=vis[x+1][y+1]=0;return;}
			vis[x][y]=vis[x+1][y]=vis[x+1][y+1]=0;
		}
		if(!vis[x][y]&&!vis[x][y+1])
		{
			vis[x][y]=vis[x][y+1]=4;
		bool	flag=0;
			For(i,1,n)
				For(j,1,m)
					if(!vis[i][j]||(!vis[i+1][j+1]&&!vis[i][j+1]&&!vis[i+1][j]))	dfs(i,j),flag=1;
			if(!flag){if(!h(change()))ans++;vis[x][y]=vis[x][y+1]=0;return;}
			vis[x][y]=vis[x][y+1]=0;
		}
		if(!vis[x][y]&&!vis[x+1][y])
		{
			vis[x][y]=vis[x+1][y]=5;
		bool		flag=0;
			For(i,1,n)
				For(j,1,m)
					if(!vis[i][j]||(!vis[i+1][j+1]&&!vis[i][j+1]&&!vis[i+1][j]))	dfs(i,j),flag=1;
			if(!flag){if(!h(change()))ans++;vis[x][y]=vis[x+1][y]=0;return;}
			vis[x][y]=vis[x+1][y]=0;
		}
		
	if(!vis[x+1][y+1]&&!vis[x][y+1]&&!vis[x+1][y])
	{
		vis[x+1][y+1]=vis[x+1][y]=vis[x][y+1]=6;
		bool flag=0;
		For(i,1,n)
			For(j,1,m)
				if(!vis[i][j]||(!vis[i+1][j+1]&&!vis[i][j+1]&&!vis[i+1][j]))	dfs(i,j),flag=1;
		if(!flag){if(!h(change()))ans++;	vis[x+1][y+1]=vis[x+1][y]=vis[x][y+1]=0;return;}
		vis[x+1][y+1]=vis[x+1][y]=vis[x][y+1]=0;
	}

}
int main()
{
	freopen("pro2.in","r",stdin);
	freopen("pro2.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(n==2)
	{
		cout<<b[m];
		return 0;
	}
	if(m==2)
	{
		cout<<b[n];
		return 0;
	}
	if(n==1)
	{
		if(m%2)	cout<<0;else cout<<m/2;
		return 0;
	}
	if(m==1)
	{
		if(n%2)	cout<<0;else cout<<n/2;
		return 0;
	}
	For(i,0,10)	For(j,0,10)	vis[i][j]=1;
	For(i,1,n)	For(j,1,m)	vis[i][j]=0;	
	dfs(1,1);
	cout<<ans<<endl;
}

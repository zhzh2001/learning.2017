#include<iostream>
#include<cstring>
#include<cstdio>
#include<vector>
#include<queue>
#define N 20000
#define For(i,j,k) for(int i=j;i<=k;++i)
using namespace std;
int z;
int n,m,x,y,s,cnt,ans,tott,tot1;
int c1[N],dis[205],e,v[N],k,ss,end;
int tot,nxt1[N],point1[105],v1[N],poi[N],f[105],nxt[N];

bool vis[N*2];
struct hp
{
    int x;
    int val;
    bool operator < (const hp &a) const
    {
        return a.val<val;
    }
};
struct hp1
{
    int x;
    int val;
    bool vis[51];
    bool operator < (const hp1 &a) const
    {
        return a.val<val;
    }
};
priority_queue <hp> q;

inline void addedge(int x,int y,int z)
{
    nxt1[++tot]=point1[x]; point1[x]=tot; v1[tot]=y; c1[tot]=z;
}
inline void add2(int x,int y,int z)
{
    nxt[++cnt]=f[x];f[x]=cnt;poi[cnt]=y;v[cnt]=z;
}
inline void dijkstra()
{
    For(i,1,n)  dis[i]=10000000.0;
    dis[end]=0;
    q.push((hp){end,0});
    while (!q.empty())
    {
        int now=q.top().x;
        q.pop();
        if (vis[now]) continue; 
        vis[now]=true;
        for (int i=point1[now];i;i=nxt1[i])
        {
            if (!vis[v1[i]]&&dis[now]+c1[i]<dis[v1[i]])
            {

                dis[v1[i]]=dis[now]+c1[i];
                q.push((hp){v1[i],dis[v1[i]]});
            }
        }
    }
}
priority_queue<hp1> open;
inline void A_star()
{
	hp1 to;
	For(i,1,n)	to.vis[i]=0;
	to.x=ss;to.val=dis[ss];to.vis[ss]=1;
    open.push(to);
    while(!open.empty())
    {
        hp1 t=open.top();
        open.pop();
        if(t.x==end){tot1++;if(tot1==k) {ans=t.val;return;}}
        bool tmp[51];
		for(int i=f[t.x];i;i=nxt[i])
        {
        	if(t.vis[poi[i]])	continue;
        	hp1 z;
        	z.x=poi[i];z.val=v[i]+t.val+dis[poi[i]]-dis[t.x];
			For(j,1,n)
        		z.vis[j]=t.vis[j];
        	z.vis[poi[i]]=1;
            open.push(z);
        }
    }
}
int main()
{
	freopen("pro4.in","r",stdin);
	freopen("pro4.out","w",stdout);
    scanf("%d%d%d",&n,&m,&k);
    for (int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        addedge(y,x,z);
        addedge(x,y,z);
        add2(x,y,z);
        add2(y,x,z);
    }
    scanf("%d%d",&ss,&end);
    dijkstra();
    A_star();
    printf("%d\n",ans);
    return 0;
}


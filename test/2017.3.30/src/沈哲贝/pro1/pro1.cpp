#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll int
#define maxn 200020
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll answ[maxn],a[maxn],n;
char s[maxn];
void manachar(){
	ll mx=0,id=0;
	For(i,1,n){
		if (mx<=i)	answ[i]=0;	else answ[i]=min(mx-i,answ[2*id-i]);
		while(a[i-answ[i]-1]==a[i+answ[i]+1])	answ[i]++;
		if (i+answ[i]>mx){	mx=i+answ[i];	id=i;	}
	}
}
void work(){
	ll ans=n-1;
	For(i,1,n)	if (i+answ[i]==n)
		ans=min(ans,n-2*answ[i]-1);
	printf("%s",s+1);	ans/=2;
	FOr(i,ans,1)	putchar(s[i]);	puts("");
}
int main(){
	freopen("pro1.in","r",stdin);
	freopen("pro1.out","w",stdout);
	while(scanf("%s",s+1)!=EOF){
		n=strlen(s+1);
		For(i,1,n)	a[2*i]=s[i];
		For(i,1,n+1)a[2*i-1]=-1;
		a[0]=-2;	a[2*n+2]=-3;
		n=n<<1|1;
		manachar();
		work();
	}
}

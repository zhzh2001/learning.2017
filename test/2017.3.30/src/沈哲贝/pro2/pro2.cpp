#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#define ll long long
#define maxn 200020
#define ld double
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
ll c[]={1,0,0,0,1,1};
ll d[]={0,1,0,0,1,0};
ll e[]={0,0,1,0,0,1};
ll f[]={0,0,0,1,0,0};
ll bin[20],g[10][520],make[520][520],p[20][2][2],n,m,a[10],b[10];
void work(ll j,ll k){//代表将状态j转移为下一层状态k, 则第j层要选满,因此要选bin[m]-j,而当前层要选k选满。 
	j=bin[m]-j-1;
	memset(p,0,sizeof p);
	For(i,1,m)	a[i]=(j&bin[i-1])>0;
	For(i,1,m)	b[i]=(k&bin[i-1])>0;
	p[0][0][0]=1;
	For(i,1,m){
		For(k,0,5)	
		if ((a[i-1]||c[k])&&(b[i-1]||d[k])&&(a[i]||e[k])&&(b[i]||f[k]))	p[i][!e[k]][!f[k]]+=p[i-1][a[i-1]-(!c[k])][b[i-1]-(!d[k])];
		p[i][0][0]=p[i-1][a[i-1]][b[i-1]];
	}
	make[bin[m]-j-1][k]=p[m][a[m]][b[m]];
}
void init(){
	work(3,0);
	For(i,0,bin[m]-1)	For(j,0,bin[m]-1)	work(i,j);
}
int main(){
	freopen("pro2.in","r",stdin);
	freopen("pro2.out","w",stdout);
	bin[0]=1;	For(i,1,10)	bin[i]=bin[i-1]<<1;
	scanf("%lld%lld",&n,&m);
	if (n<=m)	swap(n,m);
	g[0][bin[m]-1]=1;
	init();
	For(i,1,n)	For(j,0,bin[m]-1)	For(k,0,bin[m]-1)	g[i][k]+=g[i-1][j]*make[j][k];
	printf("%lld",g[n][bin[m]-1]);
}

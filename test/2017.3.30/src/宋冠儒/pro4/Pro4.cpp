#include<cstdio>
#include<cstring>
using namespace std;
const int N=105,M=N*N;
int i,j,t,l,mid,r,n,m,k,S,E,f[N];
int tot,en[M],Next[M],fa[N],w[M];
void add(int a,int b,int c)
{
	en[++tot]=b,Next[tot]=fa[a];
	fa[a]=tot,w[tot]=c;
}
int dfs(int u,int s)
{
	if(s>mid)return t;
	if(t>=k)return t;
	if(u==E)return ++t;
	for(int i=fa[u],v;i;i=Next[i])
		if(!f[v=en[i]])
			f[v]=1,dfs(v,s+w[i]),f[v]=0;
}
int main()
{
	freopen("Pro4.in","r",stdin);
	freopen("Pro4.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	while(m--)
	{
		scanf("%d%d%d",&i,&j,&t);
		add(i,j,t),add(j,i,t),r+=t;
	}
	scanf("%d%d",&S,&E);
	for(l=0;l<=r;)
	{
		t=0,mid=(l+r)/2;
		memset(f,0,sizeof f);
		f[S]=1,dfs(S,0);
		if(t<k)l=mid+1;else
		r=mid-1;
	}
	printf("%d",l);
}

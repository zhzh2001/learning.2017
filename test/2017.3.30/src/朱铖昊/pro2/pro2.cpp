#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
#define ll long long
const int a[6]={1,1,2,2,2,2},p[6][2][2]=
{{{1,0},{0,0}},{{0,1},{0,0}},
{{0,1},{1,1}},{{1,0},{1,1}},
{{1,0},{1,-1}},{{1,0},{0,1}}};
const ll ff[36]={0,1,0,1,0,1,1,2,5,11,24,53,0,5,8,55,140,633,1,11,55,380,2319,15171,0,24,140,2319,21272,262191,1,53,633,15171,262191,5350806};
int f[12][12],k,l,m,n;
ll ans;
void dfs()
{
	int i;
	int j;
	for (i=1;i<=n*m;++i)
		if (!f[(i-1)/m+1][(i-1)%m+1])
			break;
	int x=(i-1)/m+1,y=(i-1)%m+1;
	if (f[x][y]|| i>n*m)
	{
		++ans;
		return;
	}
	for (i=0;i<6;++i)
	{
		bool b=true;
		for (j=0;j<a[i];++j)
			b=b&&(x+p[i][j][0]>0 && x+p[i][j][0]<=n && y+p[i][j][1]>0 && y+p[i][j][1]<=m&& f[x+p[i][j][0]][y+p[i][j][1]]==0);
		if (b)
		{
			f[x][y]=1;
			for (j=0;j<a[i];++j)
				f[x+p[i][j][0]][y+p[i][j][1]]=1;
			dfs();
			f[x][y]=0;
			for (j=0;j<a[i];++j)
				f[x+p[i][j][0]][y+p[i][j][1]]=0;
		}
	}
}
int main()
{
	freopen("pro2.in","r",stdin);
	freopen("pro2.out","w",stdout);
	scanf("%d%d",&n,&m);
	if (n<=6&& m<=6)
		cout<<ff[n*6+m-7];
	else
	{
		dfs();
		cout<<ans;
	}
	return 0;
}
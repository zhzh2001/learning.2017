#include<iostream>
#include<cstring>
#include<cstdio>
#include<vector>
#include<queue>
using namespace std;
int z;
const int N=10005;
int n,m,x,y,s,cnt,ans,tott,k,ttt;
int dis[105],e,v[N];
int tot,poi[N],f[105],nxt[N];
bool vis[N];
struct hp
{
    int x;
    int val;
    bool operator < (const hp &a) const
    {
        return a.val<val;
    }
};
struct hp1
{
    int x;
    int val;
	bool c[102];
    bool operator < (const hp1 &a) const
    {
        return a.val<val;
    }
};
priority_queue <hp> q;
inline void add(int x,int y,int z)
{
    nxt[++cnt]=f[x];f[x]=cnt;poi[cnt]=y;v[cnt]=z;
}
inline void dijkstra()
{
    for (int i=1;i<=n;++i)
		dis[i]=(int)1e9+7;
    dis[ttt]=0;
    q.push((hp){ttt,0});
    while (!q.empty())
    {
        int now=q.top().x;
        q.pop();
        if (vis[now]) continue; 
        vis[now]=true;
        for (int i=f[now];i;i=nxt[i])
        {
            if (!vis[poi[i]]&&dis[now]+v[i]<dis[poi[i]])
            {

                dis[poi[i]]=dis[now]+v[i];
                q.push((hp){poi[i],dis[poi[i]]});
            }
        }
    }
}
priority_queue<hp1> open;
inline void A_star()
{
	hp1 kkk;
	kkk.x=s;
	kkk.val=dis[s];
	for (int j=1;j<=n;++j)
		kkk.c[j]=0;
	kkk.c[s]=1;
    open.push(kkk);
    while(!open.empty())
    {
        hp1 t=open.top();
        open.pop();
        if(t.x==ttt)
		{
			if (k==1)
			{
				printf("%d",t.val);
				return;
			}
			else
				--k;
		}
        for(int i=f[t.x];i;i=nxt[i])
			if (t.c[poi[i]]==0)
			{
				hp1 xx;
				xx.x=poi[i];
				xx.val=v[i]+t.val+dis[poi[i]]-dis[t.x];
				for (int j=1;j<=n;++j)
					xx.c[j]=t.c[j];
				xx.c[poi[i]]=1;
				open.push(xx);
			}
    }
}
int main()
{
	freopen("pro4.in","r",stdin);
	freopen("pro4.out","w",stdout);
    scanf("%d%d%d",&n,&m,&k);
    for (int i=1;i<=m;++i)
    {
        scanf("%d%d%d",&x,&y,&z);
        add(x,y,z);
		add(y,x,z);
    }
	scanf("%d%d",&s,&ttt);
    dijkstra();
    A_star();
    return 0;
}
#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
const double eps=1e-9;
double k,sum,ans,wz;
int i,j,n,m,a[305],b[305],l;
int main()
{
	freopen("pro3.in","r",stdin);
	freopen("pro3.out","w",stdout);
	scanf("%d%d",&l,&m);
	for (i=1;i<=m;++i)
		scanf("%d",&a[i]);
	scanf("%d",&n);
	for (i=2;i<=n;++i)
		scanf("%d",&b[i]);
	ans=-eps;
	for (k=0.0;k<l-b[n]+eps;k+=0.5)
	{
		sum=0;
		j=1;
		for (i=1;i<=m;++i)
		{
			while (double(a[i])>double(b[j])+k)
				++j;
			if (j==1)
				sum+=(k-a[i]);
			else
				sum+=min(a[i]-b[j-1]-k,b[j]+k-a[i]);
		}
		if (sum>ans)
		{
			ans=sum;
			wz=k;
		}
	}
	printf("%.3lf %.3lf",wz,ans);
	return 0;
}

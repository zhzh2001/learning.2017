#include<bits/stdc++.h>
#define ll long long
using namespace std;
priority_queue<ll,vector<ll>,greater<ll> >qq;
const ll oo=1e9;
ll q[1000001];
ll nedge=0,p[100001],nex[100001],c[100001],head[100001];
bool vis[10001];
ll dist[10001],pre[10001];
ll n,m,k,s,t;
inline void addedge(ll x,ll y,ll z){
	p[++nedge]=y;c[nedge]=z;
	nex[nedge]=head[x];head[x]=nedge;
}
inline void spfa(ll x){
	ll que=1;for(ll i=1;i<=n;i++)dist[i]=oo;
	memset(vis,0,sizeof vis);
	dist[x]=0;vis[x]=1;q[1]=x;ll l=1,r=1;
	while(l<=r){
		ll now=q[l],k=head[now];
		for(ll k=head[now];k;k=nex[k]){
			if(dist[p[k]]>dist[now]+c[k]){
				dist[p[k]]=dist[now]+c[k];
				pre[p[k]]=now;
				if(!vis[p[k]]){
					r++;q[r]=p[k];
					vis[p[k]]=1;
				}
			}
		}
		vis[now]=0;l++;
	}
}
inline void dfs(ll x,ll sum,bool rp){
	vis[x]=1;
	if(rp)qq.push(sum+dist[x]);
	if(x==t)return;
	for(ll k=head[x];k;k=nex[k])if(!vis[p[k]]){
		if(p[k]==pre[x])dfs(p[k],sum+c[k],0);
		else dfs(p[k],sum+c[k],1);
	}
	vis[x]=0;
}
int main()
{
	freopen("Pro4.in","r",stdin);
	freopen("Pro4.out","w",stdout);
	scanf("%I64d%I64d%I64d",&n,&m,&k);
	for(ll i=1;i<=m;i++){
		ll x,y,z;scanf("%I64d%I64d%I64d",&x,&y,&z);
		addedge(x,y,z);
		addedge(y,x,z);
	}
	scanf("%I64d%I64d",&s,&t);
	spfa(t);
	memset(vis,0,sizeof vis);
	dfs(s,0,1);
	ll ans;
	for(ll i=1;i<=k;i++)ans=qq.top(),qq.pop();
	printf("%I64d",ans);
}

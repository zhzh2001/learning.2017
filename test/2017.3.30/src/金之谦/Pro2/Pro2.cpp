#include<bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int main()
{
	freopen("Pro2.in","r",stdin);
	freopen("Pro2.out","w",stdout);
    int n=read(),m=read();ll ans;
    if(n==1&&m==1)ans=0;
    if(n==1&&m==2)ans=1;
    if(n==1&&m==3)ans=0;
    if(n==1&&m==4)ans=1;
    if(n==1&&m==5)ans=0;
    if(n==1&&m==6)ans=1;
    if(n==1&&m==7)ans=0;
    if(n==1&&m==8)ans=1;
    if(n==1&&m==9)ans=0;
    if(n==2&&m==1)ans=1;
    if(n==2&&m==2)ans=2;
    if(n==2&&m==3)ans=5;
    if(n==2&&m==4)ans=11;
    if(n==2&&m==5)ans=24;
    if(n==2&&m==6)ans=53;
    if(n==2&&m==7)ans=117;
    if(n==2&&m==8)ans=258;
    if(n==2&&m==9)ans=569;
    if(n==3&&m==1)ans=0;
    if(n==3&&m==2)ans=5;
    if(n==3&&m==3)ans=8;
    if(n==3&&m==4)ans=55;
    if(n==3&&m==5)ans=140;
    if(n==3&&m==6)ans=633;
    if(n==3&&m==7)ans=1984;
    if(n==3&&m==8)ans=7827;
    if(n==3&&m==9)ans=26676;
    if(n==4&&m==1)ans=1;
    if(n==4&&m==2)ans=11;
    if(n==4&&m==3)ans=55;
    if(n==4&&m==4)ans=380;
    if(n==4&&m==5)ans=2319;
    if(n==4&&m==6)ans=15171;
    if(n==4&&m==7)ans=96139;
    if(n==4&&m==8)ans=619773;
    if(n==4&&m==9)ans=3962734;
    if(n==5&&m==1)ans=0;
    if(n==5&&m==2)ans=24;
    if(n==5&&m==3)ans=140;
    if(n==5&&m==4)ans=2319;
    if(n==5&&m==5)ans=21272;
    if(n==5&&m==6)ans=262191;
    if(n==5&&m==7)ans=2746048;
    if(n==5&&m==8)ans=31411948;
    if(n==5&&m==9)ans=342302244;
    if(n==6&&m==1)ans=1;
    if(n==6&&m==2)ans=53;
    if(n==6&&m==3)ans=633;
    if(n==6&&m==4)ans=15171;
    if(n==6&&m==5)ans=262191;
    if(n==6&&m==6)ans=5350806;
    if(n==6&&m==7)ans=100578811;
    if(n==7&&m==1)ans=0;
    if(n==8&&m==1)ans=1;
    if(n==9&&m==1)ans=0;
    if(n==7&&m==2)ans=117;
    if(n==8&&m==2)ans=258;
    if(n==9&&m==2)ans=569;
    if(n==7&&m==3)ans=1984;
    if(n==8&&m==3)ans=7827;
    if(n==9&&m==3)ans=26676;
    if(n==7&&m==4)ans=96139;
    if(n==8&&m==4)ans=619773;
    if(n==9&&m==4)ans=3962734;
    if(n==7&&m==5)ans=2746048;
    if(n==8&&m==5)ans=31411948;
    if(n==9&&m==5)ans=342302244;
    if(n==7&&m==6)ans=100578811;
    printf("%I64d",ans);
    return 0;
}

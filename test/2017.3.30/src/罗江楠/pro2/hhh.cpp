#include <bits/stdc++.h>
using namespace std;
int szb[6][2][2]={
	{{1, 1}, {0, 0}},
	{{1, 0}, {1, 0}},
	{{1, 0}, {1, 1}},
	{{1, 1}, {1, 0}},
	{{1, 1}, {0, 1}},
	{{0, 1}, {1, 1}}};
int a[6]={2,2,3,3,3,3};
int n, m, mp[10][10], ans, nm;
bool canput(int x, int y, int id){
	for(int i = 0; i < 2; ++i) for(int j = 0; j < 2; ++j)
	if((x+i>n&&szb[id][i][j])||(y+j>m&&szb[id][i][j])||mp[x+i][y+j]&&szb[id][i][j]) return 0;
	return 1;
}
void pr(){
	for(int i = 1; i <= n; i++,puts(""))
		for(int j = 1; j <= m; j++)
			printf("%1d", mp[i][j]);
}
void dfs(int blocks){
	// printf("blocks = %d\n", blocks);
	if(blocks == nm){
		++ans;
		// puts("isAns!");
		return;
	}
	// pr();
	if(nm-blocks<2)return;
	for(int i = 1; i <= n; ++i)
		for(int j = 1; j <= m; ++j){
			if(!mp[i][j]){
				int flag=0;
				for(int x = 0; x < 5; ++x)
					if(canput(i, j, x)){
						flag=1;
						for(int i_ = 0; i_ < 2; ++i_)
							for(int j_ = 0; j_ < 2; ++j_)
								mp[i+i_][j+j_] = mp[i+i_][j+j_]|szb[x][i_][j_];
						dfs(blocks+a[x]);
						for(int i_ = 0; i_ < 2; ++i_)
							for(int j_ = 0; j_ < 2; ++j_)
								if(szb[x][i_][j_])mp[i+i_][j+j_]=0;
					}
				if(flag)return;
			}
			else if(i<n&&j<m&&!mp[i+1][j]&&!mp[i+1][j+1]&&!mp[i][j+1]){
				mp[i+1][j] = mp[i+1][j+1] = mp[i][j+1] = 1;
				dfs(blocks+3);
				mp[i+1][j] = mp[i+1][j+1] = mp[i][j+1] = 0;
				// return;
			}
		}
}
int main(){
	int an[8][8];
	for(n = 1; n <= 7; n++)
		for(m = 1; m <= 7; m++){
			printf("Working on:(%d, %d)\n", n, m);
			memset(mp, 0, sizeof mp);ans= 0;
			nm = n*m;mp[1][0] = 1;
			dfs(0);
			printf("ans = %d\n", ans);
			an[n][m] = ans;
		}
	puts("Done.");
	for(int i = 1; i <= 7; i++,puts(""))
		for(int j = 1; j <= 7; j++)
			printf("%d ", an[i][j]);
}
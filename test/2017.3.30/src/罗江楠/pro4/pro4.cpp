#include <bits/stdc++.h>
using namespace std;
const int inf = (1<<30);
struct node{
	int to, nxt, v;
}e[10050];
int ans[250], cnt, n, m, k, st, ed, tot, head[120],vis[120],x,y,z,flag;
int mp[120][120], dis[120];
int ins(int x, int y, int z){
	e[++tot].to=y;
	e[tot].nxt=head[x];
	e[tot].v=z;
	head[x]=tot;
}
void dfs(int x, int len){
	if(len+dis[x] > ans[k]) return;
	if(x==ed){
		ans[++cnt]=len;
		if(cnt == k)
			sort(ans+1, ans+cnt+1);
		if(cnt == k<<1){
			sort(ans+1, ans+cnt+1);
			cnt = k;
		}
		return;
	}
	for(int i=head[x]; i; i=e[i].nxt){
		if(vis[e[i].to]) continue;
		vis[e[i].to] = 1;
		dfs(e[i].to, len+e[i].v);
		vis[e[i].to] = 0;
	}
}
int main(){
	freopen("pro4.in", "r", stdin);
	freopen("pro4.out", "w", stdout);
	memset(mp, 127/3, sizeof mp);
	scanf("%d%d%d", &n, &m, &k);
	for(int i = 1; i <= n; i++)
		mp[i][i] = 0;
	for(int i = 1; i <= m; i++){
		scanf("%d%d%d", &x, &y, &z);
		ins(x, y, z);ins(y, x, z);
		mp[x][y] = mp[y][x] = 1;
	}
	scanf("%d%d", &st, &ed);
	ans[k] = (1<<30);
	for(int i = 1; i <= n; i++)
		dis[i] = mp[ed][i];
	vis[ed] = 1;
	for(int i = 1; i < n; i++){
		int v, maxn = (1<<30);
		for(int j = 1; j <= n; j++)
			if(!vis[j] && dis[j] < maxn)
				maxn = dis[v=j];
		vis[v] = 1;
		for(int j = 1; j <= n; j++)
			if(!vis[j]) dis[j] = min(dis[j], dis[v]+mp[v][j]);
	}
	memset(vis, 0, sizeof vis);
	vis[st]=1;
	dfs(st, 0);
	sort(ans+1, ans+cnt+1);
	printf("%d\n", ans[k]);
}
//暴力出奇迹！
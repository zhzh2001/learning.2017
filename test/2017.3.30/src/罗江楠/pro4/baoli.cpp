#include <bits/stdc++.h>
using namespace std;
int mp[120][120], dis[120], vis[120];
int cnt, n, m, k, z, y, x, st, ed;
struct node{
	int x, len;
	bool operator < (const node &b) const {
		return len < b.len;
	}
}q[50200];
int main(){
	memset(mp, 127/3, sizeof mp);
	scanf("%d%d%d", &n, &m, &k);
	for(int i = 1; i <= n; i++)
		mp[i][i] = 0; 
	for(int i = 1; i <= m; i++){
		scanf("%d%d%d", &x, &y, &z);
		mp[x][y] = mp[y][x] = z;
	}
	scanf("%d%d", &st, &ed);
	for(int i = 1; i <= n; i++)
		dis[i] = mp[ed][i];
	vis[ed] = 1;
	for(int i = 1; i < n; i++){
		int v, maxn = (1<<30);
		for(int j = 1; j <= n; j++)
			if(!vis[j] && dis[j] < maxn)
				maxn = dis[v=j];
		vis[v] = 1;
		for(int j = 1; j <= n; j++)
			if(!vis[j]) dis[j] = min(dis[j], dis[v]+mp[v][j]);
	}
	memset(vis, 0, sizeof vis);
	q[++cnt] = (node){st, 0};
	while(cnt<=k){
		int v, maxn = (1<<30);
		for(int i = 1; i <= cnt; i++)
			for(int j = 1; j <= n; j++)
				if(q[i].x!=j&&mp[q[i].x][j]+q[i].len+dis[j] < maxn)
					maxn = mp[q[i].x][v=j]+q[i].len;
		q[++cnt] = (node){v, maxn};
	}
	for(int i = 1; i <= k+1; i++)
	printf("[%d, %d]",q[i].x,q[i].len+dis[q[i].x]);
}
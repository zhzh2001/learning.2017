#include <fstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <queue>
#include <algorithm>
using namespace std;

const int N = 105, K = 505, M = 5005;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

int n, m, k, s, t;
int head[N * 2];
struct edgetype {
	int to, nxt, w;
} e[M * 10];
int dist[N * 2], u[N * 2], v[N * 2], w[N * 2], cnt;
bool vis[N * 2];

vector<int> ans;

inline void AddEdge(int u, int v, int w) {
	e[++cnt].to = v;
	e[cnt].w = w;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

void search(int now, int tow, int res = 0) {
	if (now == tow) {
		ans.push_back(res);
		// for (int i = 1; i <= n; i++)
		// if (vis[i]) cout << i << ' ';
		// cout << res << ' ' << endl;
		return;
	}
	for (int i = head[now]; ~i; i = e[i].nxt) {
		if (!vis[e[i].to]) {
			vis[e[i].to] = true;
			search(e[i].to, tow, res + e[i].w);
			vis[e[i].to] = false;
		}
	}
}

int main() {
	freopen("Pro4.in", "r", stdin);
	freopen("Pro4.out", "w", stdout);
	read(n), read(m), read(k);
	memset(head, -1, sizeof head);
	for (int i = 1; i <= m; i++) {
		read(u[i]), read(v[i]), read(w[i]);
		AddEdge(v[i], u[i], w[i]);
		AddEdge(u[i], v[i], w[i]);
	}
	read(s), read(t);
	if (k == 1) {
		queue<int> q;
		for (int i = 1; i <= n; i++) dist[i] = 0x3f3f3f3f;
		dist[t] = 0;
		q.push(t);
		vis[t] = true;
		while (!q.empty()) {
			int now = q.front();
			q.pop();
			for (int i = head[now]; ~i; i = e[i].nxt) {
				if (dist[e[i].to] > dist[now] + e[i].w) {
					dist[e[i].to] = dist[now] + e[i].w;
					if (!vis[e[i].to]) {
						vis[e[i].to] = true;
						q.push(e[i].to);
					}
				}
			}
			vis[now] = false;
		}
		printf("%d\n", dist[1]);
		return 0;
	}
	vis[s] = true;
	search(s, t, 0);
	sort(ans.begin(), ans.end());
	// for (int i = 0; i < ans.size(); i++)
	// cout << ans[i] << ' ';
	// cout << endl;
	printf("%d\n", ans[k - 1]);
	return 0;
}
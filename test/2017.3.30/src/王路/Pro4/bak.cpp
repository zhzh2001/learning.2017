#include <fstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <queue>
using namespace std;

const int N = 105, K = 505, M = 5005;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

int n, m, k, s, t;
int head[N];
struct edgetype {
	int to, nxt, w;
} e[M * 10];
int dist[N], u[N], v[N], w[N], cnt;
bool vis[N];

inline void AddEdge(int u, int v, int w) {
	e[++cnt].to = v;
	e[cnt].w = w;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

void SPFA(int n) {
	queue<int> q;
	for (int i = 1; i <= n; i++) dist[i] = 0x3f3f3f3f;
	dist[n] = 0;
	q.push(n);
	vis[n] = true;
	while (!q.empty()) {
		int now = q.front();
		q.pop();
		for (int i = head[now]; ~i; i = e[i].nxt) {
			if (dist[e[i].to] > dist[now] + e[i].w) {
				dist[e[i].to] = dist[now] + e[i].w;
				if (!vis[e[i].to]) {
					vis[e[i].to] = true;
					q.push(e[i].to);
				}
			}
		}
		vis[now] = false;
	}
}

struct data {
	int first, second, father;
	data() {}
	data(int a, int b, int c = 0): first(a), second(b), father(c) {}
	friend inline bool operator > (const data & a, const data & b) {
		return a.first > b.first;
	}
};

void AStar() {
	// memset(vis, 0, sizeof vis);
	priority_queue<data, vector<data>, greater<data> > q;
	q.push(data(dist[s], s));
	// vis[s] = true;
	while (!q.empty()) {
		data x = q.top();
		q.pop();
		for (int i = head[x.second]; ~i; i = e[i].nxt) {
			if (e[i].to != x.second) {
				q.push(data(x.first - dist[x.second] + e[i].w + dist[e[i].to], e[i].to, x.second));
				// vis[e[i].to] = true;
			}
		}
		if (x.second == t) {
			printf("%d\n", x.first);
			// cout << k << endl;
			k--;
			if (k == 0) {
				printf("%d\n", x.first);
				return;
			}
		}
		// vis[x.second] = false;
	}
}

int main() {
	freopen("Pro4.in", "r", stdin);
	freopen("Pro4.out", "w", stdout);
	read(n), read(m), read(k);
	memset(head, -1, sizeof head);
	for (int i = 1; i <= m; i++) {
		read(u[i]), read(v[i]), read(w[i]);
		AddEdge(v[i], u[i], w[i]);
		AddEdge(u[i], v[i], w[i]);
	}
	read(s), read(t);
	SPFA(n);
	// cout << dist[1] << endl;
	// memset(head, -1, sizeof head);
	// for (int i = 1; i <= m; i++)
		// AddEdge(u[i], v[i], w[i]);
	AStar();
	return 0;
}
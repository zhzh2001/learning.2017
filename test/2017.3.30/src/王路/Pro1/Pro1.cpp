#include <fstream>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
using namespace std;

ifstream fin("Pro1.in");
ofstream fout("Pro1.out");

string str;

inline bool check(string a) {
	int len = a.length();
	for (int i = 0; i < len >> 1; i++) {
		if (a[i] != a[len - i - 1]) return false; 
	}
	return true;
}

inline void Swap(string & a) {
	int len = a.length() / 2;
	for (int i = 0; i < len; i++)
		swap(a[i], a[a.length() - i - 1]);
}

int main() {
	while (fin >> str) {
		// cout << clock() << endl;
		// cout << endl;
		int len = str.length();
		int loc = 0;
		for (int i = 0; i < len; i++) {
			if (check(str.substr(i, len - i))) {
				loc = i;
				break;
			}
		}
		string adds = str.substr(0, loc);
		Swap(adds);
		// cout << clock() << endl;
		fout << str << adds << endl;
	}
	return 0;
}
#include <bits/stdc++.h>
using namespace std;

inline void read(int &x) {
	char ch;
	int bo = 0;
	x = 0;
	for (ch = getchar(); ch < '0' || ch > '9'; ch = getchar())if (ch == '-')bo = 1;
	for (; ch >= '0' && ch <= '9'; x = x * 10 + ch - '0', ch = getchar());
	if (bo)x = -x;
}

const int M = 305, N = 305, inf = 0x3f3f3f3f;
int len, m, n;
int p[M], d[N];

int main() {
	freopen("Pro3.in", "r", stdin);
	freopen("Pro3.out", "w", stdout);
	read(len), read(m);
	for (int i = 1; i <= m; i++)
		read(p[i]), p[i] *= 1000;
	read(n);
	for (int i = 2; i <= n; i++)
		read(d[i]), d[i] *= 1000;
	len = len * 1000;
	int end_line = len - d[n];
	int ans = 0, loc = 0;
	// cout << end_line << endl;
	for (int i = 0; i <= end_line; i++) {
		int res = 0;
		for (int j = 1; j <= m; j++) {
			int tmp = inf;
			for (int k = 1; k <= n; k++) {
				tmp = min(tmp, abs(p[j] - d[k] - i));
			}
			res += tmp;
		}
		if (res > ans) {
			ans = res;
			loc = i;
		}
	}
	printf("%.3lf %.3lf\n", (double)loc / 1000.0, (double)ans / 1000.0);
	return 0;
}
#include <bits/stdc++.h>
using namespace std;

ifstream fin("Pro2.in");
ofstream fout("Pro2.out");

const int N = 10;
int n, m;
int f[N][N];
int rem, tot;

int main() {
	fin >> n >> m;
	rem = n * m;
	for (int i = 1; i <= n; i++)
		f[1][i] = f[i][1] = (i & 1) ^ 1;
	f[2][2] = 2, f[2][3] = f[3][2] = 5;
	if (n == 1 || m == 1 || f[n][m] != 0) {
		fout << f[n][m] << endl;
	}
	return 0;
}
type pp=record
     next,val,link:longint;
end;
type heap=record
     path:array[0..105]of boolean;
     val,id:longint;
end;
var ans,cnt,s,e,x,y,v,i,tot,n,m,k:longint;
    dis,head1,head:array[0..105]of longint;
    f:array[0..105]of boolean;
    q:array[0..10005]of longint;
    a,b:array[0..10005]of pp;
    z,temp:heap;
    h:array[0..500005]of heap;
procedure swap(var x,y:heap);
begin
  z:=x; x:=y; y:=z;
end;
procedure up(temp:heap);
var i:longint;
begin
  inc(cnt); h[cnt]:=temp; i:=cnt;
  while i>1 do
     begin
     if h[i].val<h[i div 2].val then swap(h[i],h[i div 2]) else exit;
     i:=i div 2;
     end;
end;
procedure pop;
var i,now:longint;
begin
  h[1]:=h[cnt]; dec(cnt); i:=1;
  while (i<cnt)and(i*2<=cnt) do
     begin
     if (i*2+1<=cnt)and(h[i*2].val>h[i*2+1].val) then now:=i*2+1 else now:=i*2;
     if h[now].val<h[i].val then swap(h[now],h[i]) else exit;
     i:=now;
     end;
end;
procedure add(x,y,v:longint);
begin
  inc(tot); a[tot].link:=y; a[tot].next:=head[x]; a[tot].val:=v; head[x]:=tot;
end;
procedure add1(x,y,v:longint);
begin
  b[tot].link:=y; b[tot].next:=head1[x]; b[tot].val:=v; head1[x]:=tot;
end;
procedure spfa;
var h,t,x,now,u:longint;
begin
  fillchar(dis,sizeof(dis),12);
  h:=0; t:=1; q[1]:=e; dis[e]:=0; f[e]:=true;
  while h<>t do
     begin
     h:=h mod 10000+1; x:=q[h]; now:=head1[x]; f[x]:=false;
     while now<>0 do
	begin
	u:=b[now].link;
  	if b[now].val+dis[x]<dis[u] then
	   begin
	   dis[u]:=dis[x]+b[now].val;
	   if f[u]=false then begin t:=t mod 10000+1; q[t]:=u; f[u]:=true; end;
	   end;
        now:=b[now].next;
	end;
     end;
end;
procedure Astar;
var u,x,now:longint;
    tc:heap;
begin
  temp.val:=dis[s]; temp.id:=s; temp.path[s]:=true;  up(temp);
  while true do
     begin
     temp:=h[1]; pop; x:=temp.id; now:=head[x];
     while now<>0 do
	begin
        u:=a[now].link;
        if temp.path[u]=true then begin now:=a[now].next; continue; end;
	tc:=temp; tc.id:=u; tc.path[u]:=true; tc.val:=tc.val-dis[x]+a[now].val+dis[u]; up(tc);
	now:=a[now].next;
	end;
     if x=e then
	begin
	inc(ans);
	if ans=k then
	   begin
	   writeln(temp.val);
           close(input);
           close(output);
	   halt;
	   end;
	end;
     end;
end;
begin
  assign(input,'Pro4.in');
  assign(output,'Pro4.out');
  reset(input);
  rewrite(output);
  readln(n,m,k);
  for i:=1 to m do begin readln(x,y,v); add(x,y,v); add1(y,x,v);end;
  readln(s,e);
  spfa;
  Astar;
end.

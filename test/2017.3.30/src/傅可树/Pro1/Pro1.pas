type pp=record
     s,t,next,pre:longint;
end;
var s:ansistring;
    ch:char;
    liv,i,pos,len1,now,maxn,tot,head,len:longint;
    a:array[0..100000]of pp;
function min(x,y:longint):longint;
begin
  if x>y then exit(y) else exit(x);
end;
begin
  while not(eof) do
     begin
     fillchar(a,sizeof(a),0);
     readln(s);
     len:=length(s);
     for i:=1 to len-1 do
	if s[i]=s[len] then
	   begin
	   inc(tot);
	   a[tot].next:=tot+1;
	   a[tot].pre:=tot-1;
	   a[tot].s:=i; a[tot].t:=i;
	   end;
     a[tot].next:=0;
     head:=1;
     maxn:=len-1;
     len1:=1;
     while tot<>0 do
	begin
	inc(len1);
	now:=head;
	pos:=len-len1+1;
	ch:=s[pos];
	liv:=0;
	while now<>0 do
	   begin
	   if (a[now].t=pos)or(a[now].t+1=pos) then
	      begin
	      a[a[now].pre].next:=a[now].next;
	      a[a[now].next].pre:=a[now].pre;
	      maxn:=min(maxn,a[now].s-1);
	      dec(tot);
              now:=a[now].next;
              continue;
	      end;
	   if ch<>s[a[now].t+1] then
	      begin
	      a[a[now].pre].next:=a[now].next;
	      a[a[now].next].pre:=a[now].pre;
	      dec(tot);
	      end
	      else
	      begin
	      inc(liv);
	      if liv=1 then head:=now;
	      inc(a[now].t);
	      end;
	   if (a[now].t=pos)or(a[now].t+1=pos) then
	      begin
	      a[a[now].pre].next:=a[now].next;
	      a[a[now].next].pre:=a[now].pre;
	      maxn:=min(maxn,a[now].s-1);
	      dec(tot);
              dec(liv);
	      end;
	   now:=a[now].next;
	   end;
	end;
     write(s);
     for i:=maxn downto 1 do write(s[i]);
     end;
end.

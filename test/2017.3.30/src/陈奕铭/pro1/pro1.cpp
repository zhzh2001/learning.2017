#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=100005;
int len;
char s1[N],s2[N];
int nxt[N];

int main(){
	freopen("pro1.in","r",stdin);
	freopen("pro1.out","w",stdout);
	while(scanf("%s",s1+1)!=EOF){
		len=strlen(s1+1);
		for(int i=1;i<=len;i++)
			s2[i]=s1[len-i+1];
		int j=0;
		for(int i=2;i<=len;i++){
			while(j&&s2[i]!=s2[j+1]) j=nxt[j];
			if(s2[i]==s2[j+1]) j++; nxt[i]=j;
		}
		j=0;
		for(int i=1;i<=len;i++){
			while(j&&s1[i]!=s2[j+1]) j=nxt[j];
			if(s1[i]==s2[j+1]) j++;
		}
		printf("%s",s1+1);
		for(j++;j<=len;j++)
			putchar(s2[j]);
		printf("\n");
	}
	return 0;
}

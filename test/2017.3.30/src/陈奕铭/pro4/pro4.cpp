#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

struct node{
	int w;
	int num;
};
struct heap{
	node h[1300];
	int cnt;
	void pop(){
		h[1]=h[cnt--];
		down(1);
	}
	void push(int x,int num){
		h[++cnt].w=x; h[cnt].num=num;
		up(cnt);
		if(cnt==501) cnt--;
	}
	void down(int x){
		x<<=1;
		if(x>cnt) return;
		if(x<cnt)
			if(h[x+1].w<h[x].w) x++;
		if(h[x].w<h[x>>1].w){
			swap(h[x],h[x>>1]);
			down(x);
		}
	}
	void up(int x){
		if(x==1) return;
		if(h[x>>1].w>h[x].w){
			swap(h[x],h[x>>1]);
			up(x>>1);
		}
	}
}q;

const int N=5005,M=105;
int nxt[N*2],last[N],to[N*2],w[N*2],cnt;
int n,m,k,st,ed;
int dist[M],vis[M],distX[M];

void insert(int u,int v,int ww){
	to[++cnt]=v; nxt[cnt]=last[u]; last[u]=cnt; w[cnt]=ww;
	to[++cnt]=u; nxt[cnt]=last[v]; last[v]=cnt; w[cnt]=ww;
}

void Dij(){
	memset(dist,0x6f,sizeof dist);
	dist[ed]=0; vis[ed]=true;
	for(int i=last[ed];i;i=nxt[i])
		dist[to[i]]=w[i];
	for(int g=1;g<n;g++){
		int Max=2000000000;
		int p=0;
		for(int i=1;i<=n;i++)
			if(dist[i]<Max&&!vis[i]){
				Max=dist[i];
				p=i;
			}
		vis[p]=true;
		for(int i=last[p];i;i=nxt[i])
			if(!vis[to[i]]&&dist[to[i]]>dist[p]+w[i])
				dist[to[i]]=dist[p]+w[i];
	}
}

void Dij2(){
	memset(distX,0x6f,sizeof distX);
	memset(vis,0,sizeof vis);
	distX[st]=0; vis[st]=true;
	for(int i=last[st];i;i=nxt[i]){
		distX[to[i]]=w[i];
		q.push(distX[to[i]]+dist[to[i]],to[i]);
	}
	for(int g=1;g<k;g++){
		int p=q.h[1].num; q.pop();
		vis[p]=true;
		for(int i=last[p];i;i=nxt[i])
			if(!vis[to[i]]&&distX[to[i]]>distX[p]+w[i]&&to[i]!=ed){
				distX[to[i]]=distX[p]+w[i];
				q.push(distX[to[i]]+dist[to[i]],to[i]);
			}
	}
}

int main(){
	freopen("pro4.in","r",stdin);
	freopen("pro4.out","w",stdout);
	n=read(); m=read(); k=read();
	for(int i=1;i<=m;i++){
		int a=read(),b=read(),c=read();
		insert(a,b,c);
	}
	st=read(); ed=read();
	Dij(); Dij2();
	printf("%d\n",q.h[1].w);
	return 0;
}

#include<iostream>
#include<stdio.h>
#include<string.h>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

bool mp[10][10];
ll ans;
int n,m,tot;

bool judge1(int i,int j){
	if(j-1>0&&i+1<=n&&(!mp[i+1][j-1])) return false;
	if(j-1>0&&(!mp[i][j-1])) return false;
	if(i-1>0&&(!mp[i-1][j])) return false;
	return true;
}

bool judge2(int i,int j){
	if(j+1<=m&&i-1>0&&(!mp[i-1][j+1])) return false;
	if(j-1>0&&(!mp[i][j-1])) return false;
	if(i-1>0&&(!mp[i-1][j])) return false;
	return true;
}

bool judge3(int i,int j){
	if(i+1<=n&&j-1>0&&(!mp[i+1][j-1])) return false;
	if(i-1>0&&j+1<=m&&(!mp[i-1][j+1])) return false;
	if(j-1>0&&(!mp[i][j-1])) return false;
	if(i-1>0&&(!mp[i-1][j])) return false;
	return true;
}

bool judge4(int i,int j){
	if(i-1>0&&j-1>0&&(!mp[i-1][j-1])) return false;
	if(i-2>0&&(!mp[i-2][j])) return false;
	if(j-2>0&&(!mp[i][j-2])) return false;
	return true;
}

bool judge5(int i,int j){
	if(i+1<=n&&j-1>0&&(!mp[i+1][j-1])) return false;
	if(i-1>0&&j-1>0&&(!mp[i-1][j-1])) return false;
	if(j-2>0&&(!mp[i][j-2])) return false;
	if(i-1>0&&(!mp[i-1][j])) return false;
	return true;
}

bool judge6(int i,int j){
	if(i-1>0&&j-1>0&&(!mp[i-1][j-1])) return false;
	if(i-2>0&&(!mp[i-2][j])) return false;
	if(j-1>0&&(!mp[i][j-1])) return false;
	return true;
}

void dfs(int p,int q){
	if(q*3+p*2==tot){
		ans++;
		return;
	}
	
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(!mp[i][j]){
				mp[i][j]=true;
				if(i+1<=n&&(!mp[i+1][j])&&judge1(i,j)){
					mp[i+1][j]=true;
					dfs(p+1,q);
					mp[i+1][j]=false;
				}
				if(j+1<=m&&(!mp[i][j+1])&&judge2(i,j)){
					mp[i][j+1]=true;
					dfs(p+1,q);
					mp[i][j+1]=false;
				}
				mp[i][j]=false;
			}
	
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(!mp[i][j]){
				mp[i][j]=true;
				if(i-1>0&&j-1>0&&(!mp[i-1][j])&&(!mp[i][j-1])&&judge4(i,j)){
					mp[i-1][j]=true; mp[i][j-1]=true;
					dfs(p,q+1);
					mp[i-1][j]=false; mp[i][j-1]=false;
				}
				if(i+1<=n&&j+1<=m&&(!mp[i+1][j])&&(!mp[i][j+1])&&judge3(i,j)){
					mp[i+1][j]=true; mp[i][j+1]=true;
					dfs(p,q+1);
					mp[i+1][j]=false; mp[i][j+1]=false;
				}
				if(i+1<=n&&j-1>0&&(!mp[i+1][j])&&(!mp[i][j-1])&&judge5(i,j)){
					mp[i+1][j]=true; mp[i][j-1]=true;
					dfs(p,q+1);
					mp[i+1][j]=false; mp[i][j-1]=false; 
				}
				if(i-1>0&&j+1<=m&&(!mp[i-1][j])&&(!mp[i][j+1])&&judge6(i,j)){
					mp[i-1][j]=true; mp[i][j+1]=true;
					dfs(p,q+1);
					mp[i-1][j]=false; mp[i][j+1]=false;
				}
				mp[i][j]=false;
			}
}

int main(){
//	freopen("boalin.out","w",stdout);
	n=read(); m=read(); tot=n*m;
	if(n>m)	 swap(n,m);
	if(m==6){
		if(n==5){
			printf("15083600\n");
			return 0;
		}
		if(n==4){
			printf("178336\n");
			return 0;
		}
	}
	if(n==5&&m==5){
		printf("311487\n");
		return 0;
	}
	dfs(0,0);
	printf("%lld",ans);
	return 0;
}

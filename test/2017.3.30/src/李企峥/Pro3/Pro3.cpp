#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;
ll ans,ansl,a[500],b[500],c[500],x,len,n,m,y;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("Pro3.in","r",stdin);
	freopen("Pro3.out","w",stdout);
	ans=0;
	ansl=0;
	len=read();
	len*=1000;
	m=read();
	For(i,1,m) a[i]=read(),a[i]*=1000;
	n=read();
	For(i,2,n) b[i]=read(),b[i]*=1000;
	For(i,0,len-b[n])
	{
		For(j,1,n) c[j]=b[j]+i;
		x=0;
		y=1;
		For(j,1,m)
		{
			ll k;
			for(k=y;k<=n;k++) if(c[k]>a[j]) break;
			y=k;
			if(k==1)x+=(c[k]-a[j]); else x+=min(abs(c[k]-a[j]),abs(c[k-1]-a[j]));
		}
		if(x>ans) ans=x,ansl=i;
	}
	cout<<ansl/1000<<"."<<ansl%1000<<" "<<ans/1000<<"."<<ans%1000<<endl;
	return 0;
}


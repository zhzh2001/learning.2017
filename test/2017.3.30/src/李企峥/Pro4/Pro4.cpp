#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,m,k,a[110][110],b[1000],f[110][110],s,t,x,y,z;
bool d[110],e[110][110];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x,int y)
{
	if(x==t)
	{
		if(y<b[k])
		{
			b[k]=y;
			z=k;
			while(b[z]<b[z-1]&&z>1) swap(b[z],b[z-1]),z--;
		}
		return;
	}
	if(y+a[x][t]>b[k]) return;
	For(i,1,n)
		if(e[x][i]&&!(d[i])) 
		{
			d[i]=true;
			dfs(i,y+f[x][i]);
			d[i]=false;	  	
		}	
	return;
}
int main()
{
	freopen("Pro4.in","r",stdin);
	freopen("Pro4.out","w",stdout);
	n=read();
	For(i,1,n)For(j,1,n) a[i][j]=1e17,f[i][j]=1e17;
	m=read();
	k=read();
	For(i,1,m)
	{
		x=read();
		y=read();
		z=read();
		a[x][y]=z;
		a[y][x]=z;
		f[x][y]=z;
		f[y][x]=z;
		e[x][y]=true;
		e[y][x]=true;
	}
	For(i,1,k)b[i]=1e18;
	For(kk,1,n)
		For(i,1,n)
		{
			if(i==kk) continue;
			For(j,1,n)
			{
				if(i==j||j==kk) continue;
				if(a[i][j]>a[i][kk]+a[kk][j]) a[i][j]=a[i][kk]+a[kk][j];
			}
		}
	For(i,1,n) d[i]=false;
	s=read();
	t=read();
	if(k==1)
	{
		cout<<a[s][t]<<endl;
		return 0;
	}
	d[s]=true;
	dfs(s,0);
	cout<<b[k]<<endl;
	return 0;
}


#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,m;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("Pro2.in","r",stdin);
	freopen("Pro2.out","w",stdout);
	n=read();
	m=read();
	if(n>m)swap(n,m);
	if(n==1)
	{
		if(m/2==0) cout<<1; else cout<<0;
		return 0;
	}
	if(n==2&&m==2)
	{
		cout<<2<<endl;
		return 0;
	}
	if(n=2&&m==3)
	{
		cout<<5<<endl;
		return 0;
	}
	if(n=2&&m==4)
	{
		cout<<9<<endl;
		return 0;
	}
	cout<<n*m-1<<endl;
	return 0;
}


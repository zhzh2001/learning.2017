#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll f[15][15];
ll getans(int x,int y)
{
	if(f[x][y]!=0)return f[x][y];
	if(x==1&&y%2==1)return 0;
	if(y==1&&x%2==1)return 0;
	if(x==1||y==1)return 1;
	if(x==2&&y==3)return 5;
	if(x==3&&y==2)return 5;
	if(x==1&&y==2)return 1;
	if(y==1&&x==2)return 1;
	if(x==3&&y==3)return 8;
	ll ans=0;
	for(int i=1;i<x;i++)
		ans=ans+getans(i,y)*getans(x-i,y);
	for(int i=1;i<y;i++)
		ans=ans+getans(x,i)*getans(x,y-i);
	f[x][y]=ans;
	return ans;
}
int main()
{
	freopen("pro2.in","r",stdin);
	freopen("pro2.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	printf("%I64d",getans(n,m));
	return 0;
}

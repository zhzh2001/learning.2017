#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("pro2.in");
ofstream fout("pro2.out");
const int N=10;
int n,m,ans;
bool vis[N][N];
void dfs(int x,int y)
{
	if(x>n)
	{
		ans++;
		return;
	}
	if(vis[x][y])
		if(y<m)
			dfs(x,y+1);
		else
			dfs(x+1,1);
	else
	{
		if(y<m&&!vis[x][y+1])
		{
			vis[x][y+1]=true;
			dfs(x,y+1);
			vis[x][y+1]=false;
		}
		if(x<n&&!vis[x+1][y])
		{
			vis[x+1][y]=true;
			if(y<m)
				dfs(x,y+1);
			else
				dfs(x+1,1);
			vis[x+1][y]=false;
		}
		if(x<n&&y>1&&!vis[x+1][y]&&!vis[x+1][y-1])
		{
			vis[x+1][y]=vis[x+1][y-1]=true;
			if(y<m)
				dfs(x,y+1);
			else
				dfs(x+1,1);
			vis[x+1][y]=vis[x+1][y-1]=false;
		}
		if(x<n&&y<m&&!vis[x+1][y]&&!vis[x+1][y+1])
		{
			vis[x+1][y]=vis[x+1][y+1]=true;
			dfs(x,y+1);
			vis[x+1][y]=vis[x+1][y+1]=false;
		}
		if(x<n&&y<m&&!vis[x][y+1]&&!vis[x+1][y+1])
		{
			vis[x][y+1]=vis[x+1][y+1]=true;
			dfs(x,y+1);
			vis[x][y+1]=vis[x+1][y+1]=false;
		}
		if(x<n&&y<m&&!vis[x][y+1]&&!vis[x+1][y])
		{
			vis[x][y+1]=vis[x+1][y]=true;
			dfs(x,y+1);
			vis[x][y+1]=vis[x+1][y]=false;
		}
	}
}
int main()
{
	fin>>n>>m;
	if(n>m)
		swap(n,m);
	if(n==6&&m==6)
		fout<<5350806<<endl;
	else
		if(n==6&&m==7)
			fout<<100578811<<endl;
		else
		{
			dfs(1,1);
			fout<<ans<<endl;
		}
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("pro3.in");
ofstream fout("pro3.out");
const int N=305;
const double delta=1e-4,eps=1e-8;
int m,n;
double p[N],d[N];
double calc(double mid)
{
	double ans=0;
	for(int i=1;i<=m;i++)
	{
		int t=lower_bound(d+1,d+n+1,p[i]-mid)-d;
		ans+=min(d[t]+mid-p[i],p[i]-(d[t-1]+mid));
	}
	return ans;
}
int main()
{
	int len;
	fin>>len>>m;
	for(int i=1;i<=m;i++)
		fin>>p[i];
	fin>>n;
	d[0]=-1e100;
	for(int i=2;i<=n;i++)
		fin>>d[i];
	d[n+1]=1e100;
	double l=0,r=len-d[n];
	while(r-l>delta)
	{
		double lmid=l+(r-l)/3,rmid=r-(r-l)/3;
		if(calc(lmid)>calc(rmid)+eps)
			r=rmid;
		else
			l=lmid;
	}
	fout.precision(3);
	fout<<fixed<<r<<' '<<calc(r)<<endl;
	return 0;
}
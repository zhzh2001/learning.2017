#include<fstream>
#include<cstring>
#include<queue>
using namespace std;
ifstream fin("pro4.in");
ofstream fout("pro4.ans");
const int N=105,INF=0x3f3f3f3f;
int n,k,s,t,mat[N][N],f[N][N],cnt;
struct node
{
	int v,g,f;
	bool vis[N];
	node(int v,int g,int f,bool* Vis=NULL):v(v),g(g),f(f)
	{
		if(Vis==NULL)
			memset(vis,false,sizeof(vis));
		else
			memcpy(vis,Vis,sizeof(vis));
	}
	bool operator>(const node& rhs)const
	{
		if(f!=rhs.f)
			return f>rhs.f;
		return g>rhs.g;
	}
};
int main()
{
	int m;
	fin>>n>>m>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	memcpy(f,mat,sizeof(f));
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	fin>>s>>t;
	priority_queue<node,vector<node>,greater<node> > Q;
	node src(s,0,f[s][t]);
	src.vis[s]=true;
	Q.push(src);
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(k.v==t)
		{
			::k--;
			if(!::k)
			{
				fout<<k.g<<endl;
				break;
			}
		}
		for(int i=1;i<=n;i++)
			if(!k.vis[i]&&mat[k.v][i]<INF)
			{
				node now(i,k.g+mat[k.v][i],k.g+mat[k.v][i]+f[i][t],k.vis);
				now.vis[i]=true;
				Q.push(now);
			}
	}
	return 0;
}
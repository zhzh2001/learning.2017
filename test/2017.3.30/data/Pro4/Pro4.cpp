#include <iostream>
using namespace std;

const int maxk = 506;
const int maxn = 106, maxm = maxn * maxn << 2;
int next[maxm], w[maxm], p[maxm], q[maxm << 3], d[maxn], g[maxn];
int n, m, k, len, tot, s, t, o, dep, sum;
int c[maxk], b[maxn];
bool v[maxn];

void spfa()
{
     memset(d, 127, sizeof(d));
     int k, i, h = 0, e = 1;
     q[1] = t; v[t] = true; d[t] = 0;
     while (h < e) {
           k = q[++h]; i = g[k];
           while (i) {
                 if (d[p[i]] > d[k] + w[i]) {
                    d[p[i]] = d[k] + w[i];
                    if (!v[p[i]]) {
                       v[p[i]] = true; q[++e] = p[i];
                    }
                 }
                 i = next[i];
           }
           v[k] = false;
     }
}

void dfs(int x, int now)
{
     if (now + d[x] > len) return;
     if (x == t) {++tot; return;}
     int i = g[x]; b[++b[0]] = x;
     while (i) {
           if (!v[p[i]]) {
              v[p[i]] = true;
              dfs(p[i], now + w[i]);
              v[p[i]] = false;
              if (tot == k) return;
           }
           i = next[i];
     }
     --b[0];
}

inline void add(int s, int t, int o)
{
       p[++tot] = t; w[tot] = o; next[tot] = g[s]; g[s] = tot;
}

inline void swap(int i, int j)
{
       int o = b[i]; b[i] = b[j]; b[j] = o;
       o = c[i]; c[i] = c[j]; c[j] = o;
}

void sort(int l, int r)
{
     int i = l, j = r, k = d[b[l + r >> 1]];
     while (i < j) {
           while (d[b[i]] < k) ++i;
           while (d[b[j]] > k) --j;
           if (i <= j) swap(i++, j--);
     }
     if (j > l) sort(l, j);
     if (i < r) sort(i, r);
}

void rebuild()
{
     int i, q;
     for (i = 1; i <= n; ++i) {
         q = 0;
         while (g[i]) {
               b[++q] = p[g[i]];
               c[q] = w[g[i]];
               g[i] = next[g[i]];
         }
         sort(1, q);
         while (q) {
               add(i, b[q], c[q]);
               --q;
         }
     }
}

int main()
{
    freopen("Pro4_0.in", "r", stdin);
    freopen("Pro4_0.out", "w", stdout);
    scanf("%d%d%d\n", &n, &m, &k); len = 0;
    for (int i = 1; i <= m; ++i) {
        scanf("%d%d%d\n", &s, &t, &o);
        add(s, t, o); add(t, s, o); len += o;
    }
    scanf("%d%d\n", &s, &t); spfa(); rebuild();
    memset(v, 0, sizeof(v)); v[s] = true;
    int l = 1, r = len; sum = len;
    while (l < r) {
          len = l + r >> 1;
          tot = 0; b[0] = 0; dfs(s, 0);
          if (tot < k) l = len + 1;
          else r = len;
    }
    printf("%d\n", l);
    return 0;
}

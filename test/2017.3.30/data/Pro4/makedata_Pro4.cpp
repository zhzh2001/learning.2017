#include <iostream>
using namespace std;

const int maxn = 506;
int s, t, k, d, n, m;
bool v[maxn][maxn];

int main()
{
    srand((unsigned)time(NULL));
    freopen("Pro4_5.in", "w", stdout);
    n = rand() % 20 + 1; m = n * (n - 1) >> 1;
    m = rand() % m + 1; k = rand() % 100 + 1;
    printf("%d %d %d\n", n, m, k);
    while (m--) {
          s = rand() % n + 1;
          t = rand() % n + 1;
          while (s == t || v[s][t]) {
                s = rand() % n + 1;
                t = rand() % n + 1;
          }
          v[s][t] = v[t][s] = 1;
          d = rand() % 10000 + 1;
          printf("%d %d %d\n", s, t, d);
    }
    s = rand() % n + 1; t = rand() % n + 1;
    while (s == t) {s = rand() % n + 1; t = rand() % n + 1;}
    printf("%d %d\n", s, t);
    return 0;
}

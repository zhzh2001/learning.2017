#include <iostream>
using namespace std;

const int maxn = 306, infinite = 1 << 30;
int p[maxn], d[maxn], n, m, l, ans, s, tmp, ss;

int main()
{
    freopen("input.txt", "r", stdin);
    freopen("output1.txt", "w", stdout);
    scanf("%d%d", &l, &m); int i, j, k, t; l *= 1000;
    for (i = 1; i <= m; ++i)
        {scanf("%d", &p[i]); p[i] = p[i] * 1000;}
    scanf("%d", &n);
    for (i = 2; i <= n; ++i)
        {scanf("%d", &d[i]); d[i] = d[i] * 1000;}
    ans = 0; s = 0; d[n + 1] = infinite; d[0] = -infinite;
    while (s + d[n] <= l) {
          i = 0; j = 1; tmp = 0;
          while (j <= m) {
                while (d[i + 1] + s <= p[j]) ++i;
                if (p[j] - d[i] - s < s + d[i + 1] - p[j])
                   tmp += p[j] - d[i] - s; 
                else tmp += s + d[i + 1] - p[j];
                ++j;
          }
          if (tmp > ans) {ans = tmp; ss = s;} ++s;
    }
    printf("%.3lf %.3lf", double (ss) / 1000, double (ans) / 1000);
    return 0;
}

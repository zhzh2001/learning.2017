#include <iostream>
using namespace std;

const int maxn = 300, maxm = 300, maxl = 5000, infinite = 100000;
int n, m, l, p[maxn + 6], d[maxm + 6];
double s, ss, tmp, ans;
bool v[maxl + 6];

int main()
{
    int i, k, j, t;
    srand((unsigned)time(NULL));
    while (1) {
          memset(v, 0, sizeof(v));
          l = rand() % 1000 + 4001;
          n = rand() % 100 + 201;
          m = rand() % 100 + 201;
          for (i = 1; i <= m; ++i) {
              k = rand() % (l + 1);
              while (v[k]) k = rand() % (l + 1);
              v[k] = 1; p[i] = k;
          }
          for (i = 1; i < m; ++i) {
              t = i;
              for (j = i + 1; j <= m; ++j)
                  if (p[j] < p[t]) t = j;
              k = p[i]; p[i] = p[t]; p[t] = k;
          }
          memset(v, 0, sizeof(v)); d[1] = 0;
          for (i = 2; i <= n; ++i) {
              k = rand() % (l - 1000) + 1;
              //k = rand() % (l + 1);
              while (v[k]) //k = rand() % (l + 1);
                    k = rand() % (l - 1000) + 1;
              v[k] = 1; d[i] = k;
          }
          for (i = 2; i < n; ++i) {
              t = i;
              for (j = i + 1; j <= n; ++j)
                  if (d[j] < d[t]) t = j;
              k = d[i]; d[i] = d[t]; d[t] = k;
          }
          ans = 0; s = 0; d[n + 1] = infinite; d[0] = -infinite;
          while (s + d[n] <= l + 0.1) {
                i = 0; j = 1; tmp = 0;
                while (j <= m) {
                      while (d[i + 1] + s <= p[j]) ++i;
                      if (p[j] - d[i] - s < s + d[i + 1] - p[j])
                         tmp += p[j] - d[i] - s; 
                      else tmp += s + d[i + 1] - p[j];
                      ++j;
                }
                if (tmp > ans) {ans = tmp; ss = s;} s += 0.5;
          }
          if (ss > 0 && ss + d[n] < l) break;
    }
    freopen("input.txt", "w", stdout);
    printf("%d %d\n", l, m);
    for (i = 1; i <= m; ++i) printf("%d ", p[i]);
    printf("\n%d\n", n);
    for (i = 2; i <= n; ++i) printf("%d ", d[i]);
    printf("\n");
    return 0;
}

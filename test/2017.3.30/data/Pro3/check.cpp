#include <iostream>
using namespace std;

const int maxn = 306, infinite = 100000;
int p[maxn], d[maxn], n, m, l;
double ans, s, tmp, ss;
bool v[5006];

int main()
{
    int i, j, k, t, task = 100000;
    while (task--) {
          srand((unsigned)time(NULL));
          memset(p, 0, sizeof(p));
          memset(d, 0, sizeof(d));
          memset(v, 0, sizeof(v));
          l = rand() % 100 + 1;
          n = rand() % 10 + 1;
          m = rand() % 10 + 1;
          for (i = 1; i <= m; ++i) {
              k = rand() % (l + 1);
              while (v[k]) k = rand() % (l + 1);
              v[k] = 1; p[i] = k;
          }
          for (i = 1; i < m; ++i) {
              t = i;
              for (j = i + 1; j <= m; ++j)
                  if (p[j] < p[t]) t = j;
              k = p[i]; p[i] = p[t]; p[t] = k;
          }
          memset(v, 0, sizeof(v));
          for (i = 2; i <= n; ++i) {
              k = rand() % (l) + 1;
              while (v[k]) k = rand() % (l) + 1;
              v[k] = 1; d[i] = k;
          }
          for (i = 2; i < n; ++i) {
              t = i;
              for (j = i + 1; j <= n; ++j)
                  if (d[j] < d[t]) t = j;
              k = d[i]; d[i] = d[t]; d[t] = k;
          }
          ans = 0; s = 0; d[n + 1] = infinite; d[0] = -infinite;
          while (s + d[n] <= l + 0.1) {
                i = 0; j = 1; tmp = 0;
                while (j <= m) {
                      while (d[i + 1] + s <= p[j]) ++i;
                      if (p[j] - d[i] - s < s + d[i + 1] - p[j])
                         tmp += p[j] - d[i] - s; 
                      else tmp += s + d[i + 1] - p[j];
                      ++j;
                }
                if (tmp > ans) {ans = tmp; ss = s;} s += 0.5;
          }
          printf("%d tests remained\n", task);
          if (int (ss * 100) % 100) break;
    }
//    for (i = 1; i <= 2000000000; ++i);
    freopen("Pro3_6.in", "w", stdout);
    printf("%d\n%d\n", l, m);
    for (i = 1; i <= m; ++i) printf("%d ", p[i]);
    printf("\n%d\n", n);
    for (i = 2; i <= n; ++i) printf("%d ", d[i]);
    printf("\n");
    return 0;
}

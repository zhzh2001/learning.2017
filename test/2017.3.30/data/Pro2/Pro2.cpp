#include <iostream>
using namespace std;

const int maxn = 9, maxstate = 1 << maxn;
long long f[2][maxstate][2], w[maxn + 2];
int n, m;

int main()
{
    freopen("Pro2_9.in", "r", stdin);
    freopen("Pro2_9.out", "w", stdout);
    int i, j, p, q, k, t, tot;
    scanf("%d %d", &n, &m);
    if (n < m) {k = n; n = m; m = k;};
    w[1] = 1;
    for (i = 2; i <= m + 1; ++i)
        w[i] = w[i - 1] << 1;
    tot = w[m + 1];
    memset(f, 0, sizeof(f));
    f[0][0][0] = 1; p = 1; q = 0;
    for (i = 1; i <= n; ++i)
        for (j = 1; j <= m; ++j) {
            p = q; q = q^1;
            memset(f[q], 0, sizeof(f[q]));
            for (k = 0; k < tot; ++k)
                for (t = 0; t < 2; ++t)
                    if (!f[p][k][t]) continue; else {
                       if (k&w[j]) {f[q][k - (!t) * w[j]][0] += f[p][k][t]; continue;}
                       if (j < m && !(k&w[j + 1])) {
                          f[q][k|w[j + 1]|(t * w[j])][0] += f[p][k][t];
                          f[q][k|w[j + 1]|(t * w[j])][1] += f[p][k][t];
                       }
                       if (!t) {
                          f[q][k|w[j]][0] += f[p][k][t];
                          if (j < m) {
                             f[q][k|w[j]][1] += f[p][k][t];
                             if (!(k&w[j + 1]))
                                f[q][k|w[j]|w[j + 1]][0] += f[p][k][t];
                             }
                       }
                       if (j > 1 && !t && !(k&w[j - 1])) f[q][k|w[j]|w[j - 1]][0] += f[p][k][t];
                    }
        }
    printf("%I64d\n", f[q][0][0]);
    return 0;
}

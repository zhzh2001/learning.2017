program Pro1;

const
  inf = 'Pro1_2.in';
  ouf = 'Pro1_2.out';
  maxn = 1000001;

var
  s : ansistring;
  a, b, pre, next : array[1 .. maxn] of longint;
  len, ans : longint;
  ch : char;

procedure int;
var k : longint;
begin
  readln(s);
  len := length(s);
  for k := 1 to len do begin
    a[k] := ord(s[k]);
    b[len - k + 1] := a[k];
  end;
  a[len + 1] := -1;
  b[len + 1] := -2;
end;

procedure solve;
var k, i, j, t, l : longint;
begin
  pre[1] := 0; k := 0;
  while b[k + 1] = b[k + 2] do k := k + 1;
  pre[2] := k; t := 2;
  for i := 3 to len do begin
    j := pre[t] - (i - t); l := pre[i - t + 1];
    if l < j then pre[i] := l else begin
      if j < 0 then j := 0;
      while b[i + j] = b[1 + j] do j := j + 1;
      pre[i] := j; t := i;
    end;
  end;
  k := 0;
  while b[k + 1] = a[k + 1] do k := k + 1;
  next[1] := k; t := 1;
  for i := 2 to len do begin
    j := next[t] - (i - t); l := pre[i - t + 1];
    if l < j then next[i] := l else begin
      if j < 0 then j := 0;
      while a[i + j] = b[1 + j] do j := j + 1;
      next[i] := j; t := i;
    end;
  end;
  for i := 1 to len do
    if next[i] * 2 >= len - i + 1 then break;
  //writeln(len, ' ', i);
  write(s);
  for k := i - 1 downto 1 do write(s[k]);
  writeln;
end;

begin
  for ch := '0' to '9' do begin
  assign(input, 'Pro1_' + ch + '.in');
  assign(output, 'Pro1_' + ch + '.out');
  reset(input);
  rewrite(output);
  while not eof do begin
    int; solve;
  end;
  close(input);
  close(output);
 end;
end.

#include <iostream>
using namespace std;

const int maxn = 100006;
int a[maxn], b[maxn], pre[maxn], next[maxn], n;
char s[maxn];

int main()
{
    freopen("Pro1_0.in", "r", stdin);
    freopen("Pro1_0.out", "w", stdout);
    int i, j, k, t, l;
    while (gets(s)) {
          n = -1; while (s[++n]);
          for (i = n; i > 0; --i) s[i] = s[i - 1];
          for (i = 1; i <= n; ++i) {
              a[i] = s[i]; b[n - i + 1] = a[i];
          }
          a[n + 1] = -1; b[n + 1] = -2;
          pre[1] = 0; k = 0;
          while (b[1 + k] == b[2 + k]) ++k;
          pre[2] = k; t = 2;
          for (i = 3; i <= n; ++i) {
              j = pre[t] - i + t; l = pre[i - t + 1];
              if (l < j) pre[i] = l;
              else {
                   if (j < 0) j = 0;
                   while (b[i + j] == b[1 + j]) ++j;
                   pre[i] = j; t = i;
              }
          }
          k = 0; while (a[1 + k] == b[1 + k]) ++k;
          next[1] = k; t = 1;
          for (i = 2; i <= n; ++i) {
              j = next[t] - i + t; l = pre[i - t + 1];
              if (l < j) next[i] = j;
              else {
                   if (j < 0) j = 0;
                   while (a[i + j] == b[1 + j]) ++j;
                   next[i] = j; t = i;
              }
          }
          for (i = 1; i <= n && next[i] << 1 < n - i + 1; ++i);
          printf("%d %d\n", n, i);
          for (j = 1; j <= n; ++j) printf("%c", s[j]);
          for (j = i - 1; j > 0; --j) printf("%c", s[j]);
          printf("\n");
          memset(s, 0, sizeof(s));
    }
    return 0;
}


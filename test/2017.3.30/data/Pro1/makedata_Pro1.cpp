#include <iostream>
using namespace std;

const int len = 100006, size = 26;
int n, task, k, t, i;
char c[53], a[len];

int main()
{
    srand((unsigned)time(NULL));
    freopen("Pro1_10.in", "w", stdout);
    for (i = 0; i < 26; ++i) c[i + 1] = 'a' + i;
    for (i = 0; i < 26; ++i) c[i + 27] = 'A' + i;
    task = rand() % 10 + 1; t = task;
    while (task--) {
          n = (rand() % 100 + 1) * 1000 + rand() % 1000;
          k = (rand() % 100 + 1) * 1000 + rand() % 1000;
          k = k % (n >> 1) + 1;
          t = n + k + 1 >> 1;
          for (i = 1; i <= n; ++i)
              if (i <= t) a[i] = c[rand() % 52 + 1];
              else a[i] = a[n + k - i + 1];
          for (i = 1; i <= n; ++i) printf("%c", a[i]);
          printf("\n");
    }
    return 0;
}

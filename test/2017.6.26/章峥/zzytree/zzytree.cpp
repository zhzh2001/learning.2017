#include<fstream>
#include<vector>
#include<utility>
using namespace std;
ifstream fin("zzytree.in");
ofstream fout("zzytree.out");
const int N=400;
int n,k,vis[N][N],ans;
vector<pair<int,int> > mat[N];
void dfs(int id,int cnt,int now)
{
	if(id==n+1)
		ans=max(ans,now);
	else
	{
		dfs(id+1,cnt,now);
		if(cnt<k)
		{
			for(vector<pair<int,int> >::iterator it=mat[id].begin();it!=mat[id].end();++it)
				if(vis[id][it->first]++==0&&vis[it->first][id]++==0)
					now+=it->second;
			dfs(id+1,cnt+1,now);
			for(vector<pair<int,int> >::iterator it=mat[id].begin();it!=mat[id].end();++it)
				if(--vis[id][it->first]==0&&--vis[it->first][id]==0)
					now-=it->second;
		}
	}
}
int main()
{
	fin>>n>>k;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	dfs(1,0,0);
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("lazy.in");
ofstream fout("lazy.out");
const int N=1<<16;
int n,a[N],ans[N],cnt;
void solve(int k)
{
	ans[k]=a[++cnt];
	if(k*2+1<=n)
		solve(k*2+1);
	if(k*2<=n)
		solve(k*2);
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	solve(1);
	for(int i=1;i<n;i++)
		fout<<ans[i]<<' ';
	fout<<ans[n]<<endl;
	return 0;
}
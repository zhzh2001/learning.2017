#include<fstream>
#include<string>
#include<algorithm>
#include<utility>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/hash_policy.hpp>
using namespace std;
ifstream fin("cowrig.in");
ofstream fout("cowrig.out");
const int N=100,B=10007,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
string mat[N];
int n,m,len;
bool vis[N][N];
pair<int,int> p[N],q[N];
typedef unsigned long long hash_t;
__gnu_pbds::cc_hash_table<hash_t,__gnu_pbds::null_type> ans;
void dfs(int k,int x,int y,int cnt)
{
	if(cnt+len-k<=len/2)
		return;
	p[k]=make_pair(x,y);
	if(k==len)
	{
		if(cnt>len/2)
		{
			copy(p+1,p+len+1,q+1);
			sort(q+1,q+len+1);
			hash_t h=0;
			for(int i=1;i<=len;i++)
				h=h*B+q[i].first*m+q[i].second;
			ans.insert(h);
		}
		return;
	}
	vis[x][y]=true;
	for(int i=1;i<=k;i++)
		for(int j=0;j<4;j++)
		{
			int nx=p[i].first+dx[j],ny=p[i].second+dy[j];
			if(nx>=0&&nx<n&&ny>=0&&ny<m&&!vis[nx][ny])
				dfs(k+1,nx,ny,cnt+(mat[nx][ny]=='J'));
		}
	vis[x][y]=false;
}
int main()
{
	fin>>n>>m>>len;
	for(int i=0;i<n;i++)
		fin>>mat[i];
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			dfs(1,i,j,mat[i][j]=='J');
	fout<<ans.size()<<endl;
	return 0;
}
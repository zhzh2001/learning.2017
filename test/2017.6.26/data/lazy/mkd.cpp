#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<time.h>
#include<set>
#include<algorithm>
using namespace std;
#define N 1000005
#define P 100000000
int a[N],n;
set<int> S;
int main()
{
	freopen("lazy.in","w",stdout);
	srand(time(0));
	scanf("%d",&n);
	printf("%d\n",n);
	S.clear();
	while (S.size()<n){
		int x=rand()*rand()%P-P/2;
		S.insert(x);
	}
	for (int i=1;i<=n;++i){
		a[i]=*S.begin();
		S.erase(S.begin());
	}
	random_shuffle(a+1,a+1+n);
	for (int i=1;i<=n;++i)printf("%d ",a[i]);printf("\n");
	//system("pause");
	return 0;
}



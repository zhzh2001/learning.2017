type
  ty=record
  x,y:array[1..15]of longint;
  l:longint;
  end;
var
  n,m,len,i,j,c1,f1,k,m1,m2,i1,j1,i2,j2,s,s1,k1,k2:longint;
  a:array[0..200,0..200]of longint;
  b:array[0..20,0..20]of longint;
  ch:char;
  c:array[0..300000]of ty;
  f,next:array[0..300000]of longint;
  ff:array[0..300000]of ty;
  t:ty;
  function e(a,b:ty):boolean;
  var
    i:longint;
    begin
    if a.l<>b.l then exit(false);
    for i:=1 to a.l do
      if (a.x[i]<>b.x[i])or(a.y[i]<>b.y[i]) then exit(false);
    exit(true);
    end;

  procedure hash(a:ty);
  var
    i,s,j:longint;
    begin
    s:=1;
    for i:=1 to a.l do
      s:=s*(a.x[i]+a.y[i])and 262143;
    j:=f[s];
    while j<>0 do
      begin
      if e(ff[j],a) then exit;
      j:=next[j];
      end;
    inc(f1);
    next[f1]:=f[s];
    f[s]:=f1;
    ff[f1]:=a;
    exit;
    end;

  begin
  assign(input,'cowrig.in'); reset(input);
  assign(output,'cowrig.out'); rewrite(output);
  readln(n,m,len);
  //n:=5;m:=5;len:=7;
  for i:=1 to n do
    begin
    for j:=1 to m do
      begin
      read(ch);
      if ch='J' then a[i,j]:=1;
      end;
    readln;
    end;
  c[1].l:=1;c[1].x[1]:=1;c[1].y[1]:=1;c1:=1;
  for i:=2 to len do
    begin
    fillchar(f,sizeof(f),0);
    fillchar(next,sizeof(next),0);
    f1:=0;
    for j:=1 to c1 do
      begin
      fillchar(b,sizeof(b),0);
      for k:=1 to i-1 do
        b[c[j].x[k]+1,c[j].y[k]+1]:=1;

      for i1:=1 to len+2 do
        for j1:=1 to len+2 do
          if (b[i1,j1]=0)and((b[i1-1,j1]=1)or(b[i1+1,j1]=1)or(b[i1,j1-1]=1)or(b[i1,j1+1]=1)) then begin
            if i1=1 then m1:=0
              else m1:=1;

            if j1=1 then m2:=0
              else m2:=1;

            for k1:=1 to i-1 do
              begin
              t.x[k1]:=c[j].x[k1]+1-m1;
              t.y[k1]:=c[j].y[k1]+1-m2;
              end;
            m1:=i1-m1;
            m2:=j1-m2;
            for k1:=1 to i do
              if (k1=i)or(t.x[k1]>m1)or(t.x[k1]=m1)and(t.y[k1]>m2) then break;
            for k2:=i downto k1+1 do
              begin
              t.x[k2]:=t.x[k2-1];
              t.y[k2]:=t.y[k2-1];
              end;
            t.x[k1]:=m1;
            t.y[k1]:=m2;
            t.l:=i;
            hash(t);
            end;
      end;

    for j:=1 to f1 do c[j]:=ff[j];
    c1:=f1;
    //writeln(i,' ',c1);
    end;
  //writeln(c1);
  s:=0;
  for i:=1 to c1 do
    for j:=0 to n do
      for k:=0 to m do
        begin
        s1:=0;
        for i1:=1 to len do
          begin
          if (c[i].x[i1]+j>n)or(c[i].y[i1]+k>m)then
            begin
            s1:=-100;
            break;
            end;
          if a[c[i].x[i1]+j,c[i].y[i1]+k]=1 then inc(s1);
          end;
        //if s1<0 then break;
        if s1>len div 2 then inc(s);
        end;
  writeln(s);
  close(input); close(output);
  end.

// 一个错误的贪心/(//>/3/<//)/鬼知道能骗几分。。
// szb保佑，RP++
/*
orz
 ___  ____ ____
/ _ \|    |    \
 / \_|__  | __  |
 \__   /  / |_| |
\__ \ /  /| __  \
_  \ /  /_| | |  |
 \_/ |    | |_|  |
\___/|____|_____/

*/
#include <bits/stdc++.h>
#define ll long long
#define N 400
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){ch=='-'&&(f=1);ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int to[N<<1], nxt[N<<1], head[N], val[N<<1], cnt;
int ins(int x, int y, int z){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; val[cnt] = z;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt; val[cnt] = z;
}
int a[N], vis[N];
struct node{
	int id, val;
}b[N];
bool operator < (const node &a, const node &b) {
	return a.val < b.val;
}
int main(){
	File("zzytree");
	int n = read(), m = read();
	for(int i = 1; i < n; i++){
		int x = read(), y = read(), z = read();
		ins(x, y, z); a[x] += z; a[y] += z;
	}
	int ans = 0;
	for(int i = 1; i <= m; i++){
		for(int j = 1; j <= n; j++)
			b[b[j].id=j].val = a[j];
		sort(b+1, b+n+1);
		if(b[n].val <= 0 || vis[b[n].id]) break;
		ans += b[n].val;
		int x = b[n].id; vis[x] = 1; a[x] = 0;
		for(int j = head[x]; j; j = nxt[j])
			if(!vis[to[j]]) a[to[j]] -= val[j];
	}
	printf("%d\n", ans);
	return 0;
}
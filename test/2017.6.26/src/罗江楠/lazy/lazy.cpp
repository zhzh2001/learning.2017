#include <bits/stdc++.h>
#define ll long long
#define N 70020
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){ch=='-'&&(f=1);ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[N], b[N], n;
void dfs(int x, int y){
	b[x] = a[y];
	if(x<<1 > n) return;
	dfs(x<<1, y<<1|1);
	dfs(x<<1|1, y<<1);
}
int main(){
	File("lazy");
	n = read();
	for(int i = 1; i <= n; i++) a[i] = read();
	sort(a+1, a+n+1);
	dfs(1, 1); printf("%d", b[1]);
	for(int i = 2; i <= n; i++)
		printf(" %d", b[i]); puts("");
	return 0;
}
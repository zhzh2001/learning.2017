#include<bits/stdc++.h>
using namespace std;
ifstream fin("lazy.in");
ofstream fout("lazy.ans");
const int N=1<<16;
int a[N],p[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		p[i]=i;
	}
	vector<int> ans(n);
	do
	{
		vector<int> now(n);
		for(int i=0;i<n;i++)
			now[i]=a[p[i+1]];
		if(is_heap(now.begin(),now.end(),greater<int>()))
			ans=max(ans,now);
	}
	while(next_permutation(p+1,p+n+1));
	for(int i=0;i<ans.size()-1;i++)
		fout<<ans[i]<<' ';
	fout<<ans.back()<<endl;
	return 0;
}
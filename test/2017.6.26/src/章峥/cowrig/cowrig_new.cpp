#include<fstream>
#include<string>
#include<set>
#include<vector>
#include<algorithm>
using namespace std;
ifstream fin("cowrig.in");
ofstream fout("cowrig.out");
const int N=100,B=10007,C=1500,LEN=10,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
string mat[N];
int n,m,len;
bool vis[LEN][LEN],mark[N][N];
pair<int,int> p[LEN],pat[C][LEN];
set<vector<pair<int,int> > > S;
void dfs(int k,int x,int y)
{
	p[k]=make_pair(x,y);
	if(k==len)
	{
		vector<pair<int,int> > now(p+1,p+n+1);
		sort(now.begin(),now.end());
		S.insert(now);
		return;
	}
	vis[x][y]=true;
	for(int i=1;i<=k;i++)
		for(int j=0;j<4;j++)
		{
			int nx=p[i].first+dx[j],ny=p[i].second+dy[j];
			if(nx>=0&&nx<n&&ny>=0&&ny<m&&!vis[nx][ny])
				dfs(k+1,nx,ny);
		}
	vis[x][y]=false;
}
int main()
{
	fin>>n>>m>>len;
	dfs(1,0,0);
	int cc=0;
	for(set<vector<pair<int,int> > >::const_iterator it=S.begin();it!=S.end();++it)
	{
		cc++;
		int t=0;
		for(vector<pair<int,int> >::const_iterator jt=it->begin();jt!=it->end();++jt)
			pat[cc][++t]=*jt;
	}
	for(int i=0;i<n;i++)
		fin>>mat[i];
	int ans=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
		{
			bool flag=false;
			for(int k=1;k<=cc;k++)
				for(int rev=0;rev<4;rev++)
				{
					int cnt=0,sx=rev&1?1:-1,sy=rev&2?1:-1;
					bool flagx=true,flagy=true;
					for(int t=1;t<=len;t++)
					{
						if(pat[k][t].first)
							flagx=false;
						if(pat[k][t].second)
							flagy=false;
						int x=i+pat[k][t].first*sx,y=j+pat[k][t].second*sy;
						if(x<0||x>=n||y<0||y>=m||mark[x][y])
						{
							cnt=-1;
							break;
						}
						cnt+=mat[x][y]=='J';
					}
					if(cnt>len/2&&(sx==1||!flagx)&&(sy==1||!flagy))
					{
						ans++;
						flag=true;
					}
				}
			mark[i][j]|=flag;
		}
	fout<<ans<<endl;
	return 0;
}
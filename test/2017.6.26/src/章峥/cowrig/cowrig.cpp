#pragma GCC optimize 2
#include<fstream>
#include<string>
using namespace std;
ifstream fin("cowrig.in");
ofstream fout("cowrig.out");
const int N=105,B=10007,P=2000007;
string mat[N];
int n,m,len,mark[N][N],sx,sy,ans;
bool vis[N][N];
typedef unsigned long long hash_t;
hash_t table[P];
inline bool insert(hash_t key)
{
	hash_t t=key%P;
	while(table[t]&&table[t]!=key)
		t=(t+1)%P;
	if(table[t]==key)
		return false;
	table[t]=key;
	return true;
}
void dfs(int k,int x,int y,int cnt,hash_t h)
{
	if(cnt+len-k<=len/2||!insert(h))
		return;
	if(k==len)
	{
		ans++;
		return;
	}
	vis[x][y]=true;
	mark[x+1][y]++;
	if(x)
		mark[x-1][y]++;
	if(y)
		mark[x][y-1]++;
	mark[x][y+1]++;
	for(int nx=sx,ny=sy;;)
	{
		ny++;
		if(ny>=m||ny-sy>len)
		{
			nx++;
			ny=max(sy-len,0);
		}
		if(nx>=n||nx-sx>len)
			break;
		if(mark[nx][ny]&&!vis[nx][ny])
			dfs(k+1,nx,ny,cnt+(mat[nx][ny]=='J'),h*((nx^B)*m+ny));
	}
	mark[x+1][y]--;
	if(x)
		mark[x-1][y]--;
	if(y)
		mark[x][y-1]--;
	mark[x][y+1]--;
	vis[x][y]=false;
}
int main()
{
	fin>>n>>m>>len;
	for(int i=0;i<n;i++)
		fin>>mat[i];
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
		{
			sx=i;sy=j;
			dfs(1,i,j,mat[i][j]=='J',(i^B)*m+j);
		}
	fout<<ans<<endl;
	return 0;
}
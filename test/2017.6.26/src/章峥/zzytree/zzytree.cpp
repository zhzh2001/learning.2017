#include<fstream>
#include<vector>
#include<utility>
#include<algorithm>
using namespace std;
ifstream fin("zzytree.in");
ofstream fout("zzytree.out");
const int N=400,INF=1e8;
int n,k,f[N][N][2],g[N][2];
vector<pair<int,int> > mat[N];
void dfs(int id,int fa)
{
	int tmp=0;
	for(vector<pair<int,int> >::iterator it=mat[id].begin();it!=mat[id].end();++it)
		if(it->first!=fa)
		{
			dfs(it->first,id);
			for(int i=0;i<=k;i++)
			{
				g[i][0]=f[id][i][0];g[i][1]=f[id][i][1];
				f[id][i][0]=f[id][i][1]=-INF;
			}
			for(int i=0;i<=k;i++)
				for(int j=0;j<=i;j++)
				{
					f[id][i][0]=max(f[id][i][0],g[i-j][0]+max(f[it->first][j][0],j?f[it->first][j][1]:-INF));
					if(j<i)
						f[id][i][1]=max(f[id][i][1],g[i-j][1]+max(f[it->first][j][0]+it->second,j?f[it->first][j][1]:-INF));
				}
		}
		else
			tmp=it->second;
	for(int i=1;i<=k;i++)
		f[id][i][1]+=tmp;
}
int main()
{
	fin>>n>>k;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	dfs(1,0);
	fout<<max(f[1][k][0],f[1][k][1])<<endl;
	return 0;
}
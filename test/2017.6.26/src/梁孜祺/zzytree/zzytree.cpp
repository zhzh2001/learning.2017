#include<iostream>
#include<cstdio>
#include<cstring>
#include<vector>
#include<algorithm>
#define ll long long
#define N 365
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int to[N*2],next[N*2],head[N],w[N*2],cnt;
int f[N][N][2];
int a[N];
int n,m;
int ans=0;
vector<int> son[N];

void insert(int u,int v,int ww){
	to[++cnt]=v; w[cnt]=ww; next[cnt]=head[u]; head[u]=cnt;
	to[++cnt]=u; w[cnt]=ww; next[cnt]=head[v]; head[v]=cnt;
}

void dfs(int x,int fa){
	for(int i=head[x];i;i=next[i])
		if(to[i]!=fa){
			a[to[i]]=w[i];
			son[x].push_back(to[i]);
			dfs(to[i],x);
		}
}

void dp(int x,int p){
	for(int i=0;i<=m;i++)
		if(p==0) f[x][i][1]+=a[x];
	for(int i=0;i<son[x].size();i++){
		for(int j=0;j<m;j++){
			f[son[x][i]][j+1][1]=f[x][j][1]+a[son[x][i]];
			f[son[x][i]][j+1][0]=f[x][j][1]+a[son[x][i]];
		}
		dp(son[x][i],1);
		for(int j=0;j<=m;j++){
			f[x][j][1]=max(f[x][j][1],f[son[x][i]][j][1]);
			f[x][j][1]=max(f[x][j][1],f[son[x][i]][j][0]);
		}
	}
	for(int i=0;i<son[x].size();i++){
		for(int j=0;j<=m;j++){
			f[son[x][i]][j][0]=f[x][j][0];
			f[son[x][i]][j][1]=f[x][j][0];
		}
		dp(son[x][i],0);
		for(int j=0;j<=m;j++){
			f[x][j][0]=max(f[x][j][0],f[son[x][i]][j][0]);
			f[x][j][0]=max(f[x][j][0],f[son[x][i]][j][1]);
		}
	}
}

int main(){
	freopen("zzytree.in","r",stdin);
	freopen("zzytree.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<n;i++){
		int a=read(),b=read(),v=read();
		insert(a,b,v);
	}
	a[1]=0;
	dfs(1,0);
	f[1][0][0]=0;
	dp(1,0);
	for(int i=0;i<=m;i++){
		ans=max(ans,f[1][i][0]);
		ans=max(ans,f[1][i][1]);
	}
	printf("%d",ans);
	return 0;
}

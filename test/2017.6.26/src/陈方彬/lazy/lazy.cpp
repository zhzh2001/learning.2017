#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e5+5;
priority_queue<int,vector<int>,greater<int> >Q;
int a[N];
int n,x,now;
int main()
{
	freopen("lazy.in","r",stdin);
	freopen("lazy.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d",&x),Q.push(x);
	for(int k=1;!Q.empty();k*=2){
		for(int i=k;i;i--){a[now+i]=Q.top();Q.pop();}
		now+=k;
	}
	for(int i=1;i<=n;i++)printf("%d ",a[i]);
}


#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=400;
struct cs{int to,v,nxt;}a[N*2];
int head[N],ll;
int fa[N],s[N],v[N],f[N][N][2],g[N];
int n,m,x,y,z,ans;
void init(int x,int y,int z){
	a[++ll].to=y;
	a[ll].v=z;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs(int x,int y){
	s[x]=1;fa[x]=y;
	for(int k=head[x];k;k=a[k].nxt){
		v[x]+=a[k].v;
		if(a[k].to!=y){
			dfs(a[k].to,x);
			s[x]+=s[a[k].to];
		}
	}
	f[x][0][1]=f[x][0][0]=0;
	for(int k=head[x];k;k=a[k].nxt)
		if(a[k].to!=y){
			int z=a[k].to;
			for(int i=1;i<=s[z];i++)g[i]=max(f[z][i][1],f[z][i][0]);
			for(int j=s[x];j;j--)
				for(int k=min(j,s[z]);k;k--)
					f[x][j][0]=max(f[x][j][0],f[x][j-k][0]+g[k]);
			
			for(int i=1;i<=s[z];i++)g[i]=max(f[z][i][1]-a[k].v,f[z][i][0]);
			for(int j=s[x];j;j--)
				for(int k=min(j,s[z]);k;k--)
					f[x][j][1]=max(f[x][j][1],f[x][j-k][1]+g[k]);
		}
	for(int j=s[x];j;j--)f[x][j][1]=f[x][j-1][1]+v[x];
}
int main()
{
	freopen("zzytree.in","r",stdin);
	freopen("zzytree.out","w",stdout);
	memset(f,-63,sizeof f);
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++){
		scanf("%d%d%d",&x,&y,&z);
		init(x,y,z);
		init(y,x,z);
	}
	dfs(1,-1);
	for(int i=0;i<=m;i++)ans=max(ans,max(f[1][i][0],f[1][i][1]));
	printf("%d",ans);
}


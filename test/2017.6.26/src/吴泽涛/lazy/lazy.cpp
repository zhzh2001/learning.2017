#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 65540
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n;
int a[N];
int b[N];

bool cmp(int a,int b){
	return a<b;
}

void build(int x,int m,int num){
	b[num*2]=a[x+m/2];
	b[num*2+1]=a[x+1];
	if(m==4) return;
	build(x+m/2,m/2,num*2);
	build(x+1,m/2,num*2+1);
}

int main(){
	freopen("lazy.in","r",stdin);
	freopen("lazy.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	sort(a+1,a+n+1,cmp);
	b[1]=a[1];
	build(1,n+1,1);
	for(int i=1;i<n;i++) printf("%d ",b[i]);
	printf("%d\n",b[n]);
	return 0;
}

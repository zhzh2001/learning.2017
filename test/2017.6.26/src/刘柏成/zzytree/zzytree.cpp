#include <bits/stdc++.h>
using namespace std;
struct edge{
	int to,cost,next;
};
const int maxn=401;
const int maxm=801;
const int INF=0x3f3f3f3f;
void update(int& x,int y){
	if (x<y)
		x=y;
}
struct graph{
	int n,m;
	int first[maxn];
	edge e[maxm];
	void addedge(int from,int to,int cost){
		e[++m]=(edge){to,cost,first[from]};
		first[from]=m;
	}
	int dp[maxn][2][maxn];
	int qaq[maxn][3][maxn]; //0:choose have up,1:choose no up,2:don't choose
	int tmp[3];
	int sz[maxn];
	void dfs(int u,int c){
		//printf("on %d\n",u);
		sz[u]=1;
		int* x[3];
		for(int i=0;i<3;i++)
			x[i]=qaq[u][i];
		for(int i=0;i<3;i++)
			for(int j=0;j<=n;j++)
				x[i][j]=-INF;
		x[0][1]=c,x[1][1]=x[2][0]=0;
		for(int i=first[u];i;i=e[i].next){
			int v=e[i].to;
			if (sz[v])
				continue;
			//printf("%d\n",v);
			dfs(v,e[i].cost);
			int *qa=dp[v][0],*qb=dp[v][1];
			for(int j=sz[u];j>=0;j--){
				for(int k=0;k<3;k++)
					tmp[k]=x[k][j],x[k][j]=-INF;
				for(int k=sz[v];k>=0;k--){
					//printf("%d %d %d\n",tmp[0],qb[k],e[i].cost);
					update(x[0][j+k],tmp[0]+qb[k]+e[i].cost);
					update(x[1][j+k],tmp[1]+qb[k]+e[i].cost);
					update(x[2][j+k],tmp[2]+qa[k]);
				}
			}
			sz[u]+=sz[v];
		}
		//printf("on %d,c=%d\n",u,c);
		//for(int i=0;i<=sz[u];i++)
			//printf("%d:%d %d %d\n",i,x[0][i],x[1][i],x[2][i]);
		for(int i=0;i<=n;i++){
			dp[u][0][i]=max(x[0][i],x[2][i]);
			dp[u][1][i]=max(x[1][i],x[2][i]);
		}
	}
	int work(int n,int k){
		this->n=n;
		dfs(1,0);
		int ans=-INF;
		for(int i=0;i<=k;i++)
			update(ans,dp[1][1][i]);
		return ans;
	}
}g;
int main(){
	freopen("zzytree.in","r",stdin);
	freopen("zzytree.out","w",stdout);
	int n,k;
	scanf("%d%d",&n,&k);
	for(int i=1;i<n;i++){
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		g.addedge(u,v,w);
		g.addedge(v,u,w);
	}
	printf("%d",g.work(n,k));
}
//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("cowrig.in");
ofstream fout("cowrig.out");
int n,m,len;
char mp[103][103];
int jiyi[103][103][9][9][9];
int sumh[103][103],sumj[103][103];
/*
int dp(int x,int y,int z,int sh,int sj){
	int & fanhui=jiyi[x][y][z][sh][sj];
	if(fanhui!=-1){
		return fanhui;
	}
	if(sh>(len/2)){
		return fanhui=0;
	}
	if(sh+sj==len){
		if(sh>=sj){
			return fanhui=0;
		}
		return fanhui=1;
	}
	if(x==n+1){
		return fanhui=0;
	}
	fanhui=0;
	for(int i=y;i<=y+z-1;i++){
		for(int j=1;j<=len-sh-sj;j++){
			if(i-j+1>=1){
				fanhui+=dp(x+1,i-j+1,j,sh+sumh[i]-sumh[i-j],sj+sumj[i]-sumj[i-j]);
			}else{
				break;
			}
		}
		for(int j=1;j<=len-sh-sj;j++){
			if(i+j-1<=m){
				fanhui+=dp(x+1,i,j,sh+sumh[i+j-1]-sumh[i-1],sj+sumj[i+j-1]-sumj[i-1]);
			}else{
				break;
			}
		}
	}
	return fanhui;
}
*/
int xx[]={0,0,0,-1,1};
int yy[]={0,1,-1,0,0};
bool vis[103][103];
int ans=0;
void dfs(int x,int y,int sumh,int sumj){
	vis[x][y]=1;
	if(sumh>len/2){
		return;
	}
	if(sumh+sumj==len){
		if(sumj>sumh){
			ans++;
		}
	}
	for(int i=1;i<=4;i++){
		int nowx=x+xx[i];
		int nowy=y+yy[i];
		if(nowx>=1&&nowx<=n&&nowy>=1&&nowy<=m&&vis[nowx][nowy]==0){
			dfs(nowx,nowy,sumh+(mp[nowx][nowy]=='H'),sumj+(mp[nowx][nowy]=='J'));
			vis[nowx][nowy]=0;
		}
	}
}
int main(){
	//freopen("cowrig.in","r",stdin);
	//freopen("cowrig.out","w",stdout);
	memset(jiyi,-1,sizeof(jiyi));
	fin>>n>>m>>len;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>mp[i][j];
			if(mp[i][j]=='H'){
				sumh[i][j]=sumh[i][j-1]+1;
			}else{
				sumj[i][j]=sumj[i][j-1]+1;
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			dfs(i,j,(mp[i][j]=='H'),(mp[i][j]=='J'));
		}
	}
	/*
	int ans=0;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			for(int k=j+1;k<=min(j+len,m);k++){
				ans+=dp(i,j,k-j,sumh[i][k]-sumh[i][j-1],(k-j+1)-(sumh[i][k]-sumh[i][j-1]));
			}
		}
	}
	*/
	fout<<ans*2<<endl;
	return 0;
}
/*

in:
5 5 5
HHHHH
HHHHH
HHHJH
JHJHH
HHHHH

out:
2

*/

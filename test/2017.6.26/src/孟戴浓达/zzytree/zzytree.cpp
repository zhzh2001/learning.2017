//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("zzytree.in");
ofstream fout("zzytree.out");
int head[100003],next[100003],to[100003],val[100003],tot;
inline void add(int a,int b,int c){
	to[++tot]=b;        val[tot]=c;
	next[tot]=head[a];  head[a]=tot;
}
int vv[10003],vvf[10003],zishu[10003];
int head2[100003],next2[100003],to2[100003],tot2;
inline void add2(int a,int b){
	to2[++tot2]=b;  next2[tot2]=head2[a];  head2[a]=tot2;
}
void dfs(int now,int fa){
	for(int i=head[now];i;i=next[i]){
		if(to[i]!=fa){
			vv[now]=vv[now]+val[i];
			vvf[to[i]]=val[i];
			add2(now,to[i]);
			dfs(to[i],now);
			zishu[now]+=zishu[to[i]];
		}
	}
	zishu[now]+=1;
}

int jiyi[363][363][2];
void dp(int now,int zong,int h){
	if(zong==0){
		jiyi[now][zong][h]=0;
		return;
	}
	for(int i=head[i];i;i=next[i]){
		dp(to[i],zong-1,1);
	}
	for(int i=head[i];i;i=next[i]){
		dp(to[i],zong,0);
	}
	for(int i=head[i];i;i=next[i]){
		for(int j=0;j<=zong;j++){
			for(int k=0;k<=zong;k++){
				if(j-k-1>=0){
					jiyi[now][j][0]=max(jiyi[now][j][0],jiyi[now][j-k-1][0]+jiyi[to[i]][k][1]+vvf[now]+vv[now]);
					jiyi[now][j][1]=max(jiyi[now][j][1],jiyi[now][j-k-1][1]+jiyi[to[i]][k][1]+vv[now]);
				}
				if(j-k>=0){
					jiyi[now][j][0]=max(jiyi[now][j][0],jiyi[now][j-k][0]+jiyi[to[i]][k][0]);
					jiyi[now][j][1]=max(jiyi[now][j][1],jiyi[now][j-k][1]+jiyi[to[i]][k][0]+vv[now]);
				}
			}
		}
	}
}
int n,k;
struct heihei{
	int vvv,bian;
}xxx[10003];
bool cmp(const heihei a,const heihei b){
	return a.vvv>=b.vvv;
}
int main(){
	//freopen("zzytree.in","r",stdin);
	//freopen("zzytree.out","w",stdout);
	memset(jiyi,-1,sizeof(jiyi));
	fin>>n>>k;
	for(int i=1;i<=n-1;i++){
		int x,y,z;
		fin>>x>>y>>z;
		add(x,y,z);
		add(y,x,z);
	}
	dfs(1,0);
	for(int i=1;i<=n;i++){
		xxx[i].vvv=vv[i]+vvf[i];
		xxx[i].bian=i;
	}
	int ans=0;
	for(int i=1;i<=k;i++){
		sort(xxx+1,xxx+n+1,cmp);
		ans+=xxx[1].vvv;
		for(int j=head2[xxx[1].bian];j;j=next2[j]){
			for(int k=1;k<=n;k++){
				if(xxx[k].bian==to2[j]){
					xxx[k].vvv=xxx[k].vvv-vvf[xxx[k].bian];
					break;
				}
			}
		}
		xxx[1].vvv=0;
	}
	/*
	dp(1,k,0);
	dp(1,k,1);
	int ans=0;
	for(int i=0;i<=k;i++){
		ans=max(jiyi[1][i][0],ans);
		ans=max(jiyi[1][i-1][1],ans);
	}
	*/
	fout<<ans<<endl;
	return 0;
}
/*

in:
5 2
1 2 104
2 3 619
1 4 898
1 5 557

out:
2178

*/

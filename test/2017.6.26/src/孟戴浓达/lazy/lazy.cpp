//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("lazy.in");
ofstream fout("lazy.out");
struct hehe{
	int val,pos;
}num[100003];
int ans[100003];
inline bool cmp(const hehe a,const hehe b){
	return a.val<b.val;
}
int n;
void dfs(int now,int l,int r){
	ans[now]=num[l].val;
	if(l==r){
		return;
	}
	dfs(now*2,(l+r)/2+1,r);
	dfs(now*2+1,l+1,(l+r)/2);
}
int main(){
	//freopen("lazy.in","r",stdin);
	//freopen("lazy.out","w",stdout);
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>num[i].val;
	}
	sort(num+1,num+n+1,cmp);
	dfs(1,1,n);
	for(int i=1;i<=n-1;i++){
		fout<<ans[i]<<" ";
	}
	fout<<ans[n]<<endl;
	return 0;
}
/*

in:
3
10 2 1

out:
1 10 2

in2:
7
1 2 3 4 5 6 7

out2:
1 5 2 7 6 4 3

*/

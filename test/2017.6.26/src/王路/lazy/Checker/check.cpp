#include <bits/stdc++.h>
#include <Windows.h>
using namespace std;

const int N = 150000;
int n, a[N];

void check(int x) {
	if (x > n) return;
	assert(a[x] <= a[x << 1] && a[x] <= a[x << 1 | 1]);
	check(x << 1);
	check(x << 1 | 1);
}

int main() {
	memset(a, 0x3f, sizeof a);
	freopen("lazy.out", "r", stdin);
	srand(GetTickCount());
	n = 65535;
	for (int i = 1; i <= n; i++)
		scanf("%d", a + i);
	check(1);
	return 0;
}
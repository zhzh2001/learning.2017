#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

ifstream fin("lazy.in");
ofstream fout("lazy.out");

const int N = 150000;
int n, a[N], ans[N], cur[N], size[N];

void init(int x) {
	if (x > n) return;
	size[x] = 1;
	init(x << 1);
	init(x << 1 | 1);
	size[x] += size[x << 1] + size[x << 1 | 1];
}

void build(int x, int rk) {
	if (x > n) return;
	ans[x] = a[rk];
	build(x << 1, rk + size[x << 1 | 1] + 1);
	build(x << 1 | 1, rk + 1);
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	sort(a + 1, a + n + 1);
	init(1);
	build(1, 1);
	fout << ans[1];
	for (int i = 2; i <= n; i++)
		fout << ' ' << ans[i];
	return 0;
}
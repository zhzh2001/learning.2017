#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
using namespace std;

ifstream fin("cowrig.in");
ofstream fout("cowrig.out");

const int N = 105;

int n, m, len, mat[N][N], res;
char str[N];
bool vis[N][N];

const int dx[] = { 1, 0, -1, 0 };
const int dy[] = { 0, 1, 0, -1 };

void dfs(int i, int j, int len = 1) {
	if (len == ::len) {
		//!< cout << "-----------------------------------" << endl;
		//!< for (int i = 1; i <= n; i++)
			//!< for (int j = 1; j <= m; j++)
				//!< if (vis[i][j])
					//!< cout << i << ' ' << j << endl;
		res++;
		return;
	}
	vis[i][j] = true;
	for (int k = 0; k < 3; k++) {
		int nx = i + dx[k], ny = j + dy[k];
		if (nx < 1 || ny < 1 || nx > n || ny > m || mat[nx][ny] == mat[i][j] || vis[nx][ny]) continue;
		dfs(nx, ny, len + 1);
	}
	vis[i][j] = false;
}

int main() {
	fin >> n >> m >> len;
	for (int i = 1; i <= n; i++) {
		fin >> str;
		for (int j = 0; j < m; j++)
			mat[i][j + 1] = (str[j] == 'J');
	}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			if (mat[i][j]) {
				dfs(i, j);
				//!< cerr << res << endl;
			}
		}
	fout << res << endl;
	return 0;
}
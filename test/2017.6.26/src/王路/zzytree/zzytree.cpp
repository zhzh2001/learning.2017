#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <cstring>
#include <cassert>
using namespace std;

ifstream fin("zzytree.in");
ofstream fout("zzytree.out");

const int N = 365, inf = 0x3f3f3f3f;
int n, k;

int head[N], f[N][2]/**[N]*/;
bool vis[N << 1];

struct Edge {
	int to, nxt, w, flag;
} e[N << 1];

inline void addEdge(int u, int v, int w) {
	static int cnt = 1;
	e[++cnt].to = v, e[cnt].nxt = head[u], head[u] = cnt, e[cnt].w = w;
	e[cnt].flag = 0;
}

int res, maxpos, state;

void dfs(int x) {
	vis[x] = true;
	int tmp = 0, tmpstate = 1;
	for (int i = head[x]; i; i = e[i].nxt) {
		if (e[i].flag && e[i ^ 1].flag) continue;
		tmp += e[i].w;
		if (e[i].w < 0) tmpstate = 0;
	}
	if (tmp > res || (tmp == res && tmpstate > state)) {
		maxpos = x;
		res = tmp;
		state = tmpstate;
	}
	for (int i = head[x]; i; i = e[i].nxt)
		if (!vis[e[i].to]) dfs(e[i].to);
	vis[x] = false;
}

vector<int> vec(15);
int Ans = 0;

void dfs1(int x, int len = 0) {
	vec[len] = x;
	memset(vis, 0, sizeof vis);
	int tmp = 0;
	for (int i = 1; i <= len; i++) {
		for (int j = head[vec[i]]; j; j = e[j].nxt) {
			if (!vis[j] && !vis[j ^ 1])
				tmp += e[j].w;
			vis[j] = vis[j ^ 1] = true;
		}
	}
	Ans = max(Ans, tmp);
	if (len == k) return;
	for (int i = x + 1; i <= n; i++)
		dfs1(i, len + 1);
}

void dp(int x, int fa = -1) {
	for (int i = head[x]; i; i = e[i].nxt) {
		if (e[i].to == fa) continue;
		dp(e[i].to, x);
		/**
		// for (int j = 1; j <= k; j++) {
			// f[x][1][j] += max(max(f[e[i].to][1][j - 1], f[e[i].to][0][j]), 0);
			// f[x][0][j] += max(max(f[e[i].to][1][j] + e[i].w, f[e[i].to][0][j]), 0);
		// }
		*/
		f[x][1] += max(max(f[e[i].to][1], f[e[i].to][0]), 0);
		f[x][0] += max(max(f[e[i].to][1] + e[i].w, f[e[i].to][0]), 0);
	}
	//!< for (int j = 1; j <= k; j++)
	for (int i = head[x]; i; i = e[i].nxt)
		if (e[i].to != fa) f[x][1]/**[j]*/ += e[i].w;
}

int main() {
	fin >> n >> k;
	if (k == 0) {
		fout << 0 << endl;
		return 0;
	}
	for (int i = 1; i < n; i++) {
		int x, y, z;
		fin >> x >> y >> z;
		addEdge(x, y, z);
		addEdge(y, x, z);
	}
	if (n <= 10) {
		dfs1(0);
		fout << Ans << endl;
		return 0;
	}
	if (n - 5 <= k) {
		dp(1);
		fout << max(f[1][0], f[1][1]) << endl;
		return 0;
	}
	int ans = 0, now = 0;
	for (int i = 1; i <= k; i++) {
		res = -inf, maxpos = 0, state = -1;
		for (int j = 1; j <= n; j++)
			if (!vis[j]) dfs(j);
		now += res;
		for (int i = head[maxpos]; i; i = e[i].nxt)
			e[i].flag = e[i ^ 1].flag = true;
		ans = max(ans, now);
	}
	fout << ans << endl;
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int N=365;
int head[N],tail[N*2],nxt[N*2],e[N*2],vis[N*2];
int t;
int k,n;
int ans=0;
struct xxx{
	int k,i,bre,t;
}a[N];
int in[N],b[N];
void addto(int x,int y,int z)
{
	t++;
	nxt[t]=head[x];
	head[x]=t;
	tail[t]=y;
	vis[t]=0;
	e[t]=z;
}
bool com(xxx a,xxx b)
{
	if(a.k!=b.k)return a.k>b.k;
	else return a.t>b.t;
}
int main()
{
	freopen("zzytree.in","r",stdin);
	freopen("zzytree.out","w",stdout);
	scanf("%d%d",&n,&k);
	t=1;
	for(int i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		addto(x,y,z);
		addto(y,x,z);
	}
	t=0;
	for(int xx=1;xx<=n;xx++){
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
//		printf("%d\n",ans);
		for(int j=1;j<=n;j++){
			a[j].i=j;
			for(int i=head[j];i;i=nxt[i]){
				a[i].t++;
//				printf("%d %d %d\n",j,i,e[i]);
//				cout<<j<<' '<<tail[i]<<' '<<vis[i]<<endl;
				if(!vis[i]&&!in[j])a[j].k+=e[i];
				if(in[j]&&vis[i]==1)b[j]-=e[i];
			}
		}
/*		for(int i=1;i<=n;i++){
			printf("%d %d\n",a[i].k,b[i]);
		}*/
		if(k>0){
			sort(a+1,a+n+1,com);
			if(a[1].k<=0){
				printf("%d",ans);
				return 0;
			}
			ans+=a[1].k;
			k--;
			in[a[1].i]=true;
			for(int i=head[a[1].i];i;i=nxt[i])
				vis[i]++,vis[i^1]++;
		}
		else{
			for(int j=1;j<=n;j++)
				if(!in[j]){
				int ma=-100000000;
				int m=0;
				for(int i=head[j];i;i=nxt[i])
					if(in[tail[i]]){
						if(ma<b[tail[i]])ma=b[tail[i]]+e[i],m=tail[i];
					}
				for(int i=1;i<=n;i++){
					if(in[i]){
						if(ma<b[i])ma=b[i],m=i;
					}
				}
				a[j].k+=ma;
				a[j].bre=m;
			}
			sort(a+1,a+n+1,com);
			if(a[1].k<=0){
				printf("%d",ans);
				return 0;
			}
			ans+=a[1].k;
			for(int i=head[a[1].i];i;i=nxt[i])
				vis[i]++,vis[i^1]++;
			in[a[1].i]=true;
			for(int i=head[a[1].bre];i;i=nxt[i])
				vis[i]--,vis[i^1]--;
			in[a[1].bre]=false;
		}
	}
	printf("%d",ans);
	return 0;
}

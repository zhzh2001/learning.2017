const maxn=200000;
var a,ans:array[1..maxn]of longint;
    s:array[1..20]of longint;
    n,i,k:longint;
procedure swap(var a,b:longint);
var t:longint;
begin
  t:=a;
  a:=b;
  b:=t;
end;
procedure sort(l,r:longint);
var i,j,x:longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r)shr 1];
  repeat
    while a[i]<x do
      inc(i);
    while x<a[j] do
      dec(j);
    if i<=j then
    begin
      swap(a[i],a[j]);
      inc(i);
      dec(j);
    end;
  until i>j;
  if i<r then sort(i,r);
  if l<j then sort(l,j);
end;
procedure work(i,p,k:longint);
begin
  if k=1 then exit;
  ans[i*2]:=a[p+s[k]];
  work(i*2,p+s[k],k-1);
  ans[i*2+1]:=a[p+1];
  work(i*2+1,p+1,k-1);
end;
begin
  assign(input,'lazy.in');reset(input);
  assign(output,'lazy.out');rewrite(output);
  readln(n);
  for i:=1 to n do
    read(a[i]);
  sort(1,n);
  k:=1;
  s[1]:=1;
  while s[k]<n do
  begin
    inc(k);
    s[k]:=s[k-1]*2;
  end;
  ans[1]:=a[1];
  if n>1 then work(1,1,k-1);
  for i:=1 to n do
  begin
    write(ans[i]);
    if i<n then write(' ') else writeln;
  end;
end.



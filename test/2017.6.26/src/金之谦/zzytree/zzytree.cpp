#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int n,m,f[401][401][2],son[401];
int nedge=0,p[20001],c[20001],nex[20001],head[20001];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
inline void dfs(int x,int fa){
	int sum=0;son[x]=1;
	for(int k=head[x];k;k=nex[k])if(p[k]!=fa){
		sum+=c[k];dfs(p[k],x);son[x]+=son[p[k]];
		for(int i=min(m,son[x]);i>=0;i--)
			for(int j=i;j>=0;j--){
				if(i<son[x])f[x][i][0]=max(f[x][i][0],f[x][j][0]+max(f[p[k]][i-j][0],f[p[k]][i-j][1]));
				if(i>0&&j>0){
					if(i-j)f[x][i][1]=max(f[x][i][1],f[x][j][1]+max(f[p[k]][i-j][0],f[p[k]][i-j][1]-c[k]));
					else f[x][i][1]=max(f[x][i][1],f[x][j][1]+f[p[k]][i-j][0]);
				}
			}
	}else sum+=c[k];
	for(int i=1;i<=min(m,son[x]);i++)f[x][i][1]+=sum;
}
signed main()
{
	freopen("zzytree.in","r",stdin);
	freopen("zzytree.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<n;i++){
		int x=read(),y=read(),z=read();
		addedge(x,y,z);addedge(y,x,z);
	}
	dfs(1,0);int ans=0;
	for(int i=0;i<=m;i++)ans=max(ans,max(f[1][i][0],f[1][i][1]));
	writeln(ans);
	return 0;
}
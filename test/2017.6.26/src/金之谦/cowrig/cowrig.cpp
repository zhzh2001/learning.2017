#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int n,m,p,ans=0,a[101][101]={0};
inline void k1(){for(int i=1;i<=n;i++)for(int j=1;j<=m;j++)if(a[i][j])ans++;}
inline void k2(){for(int i=1;i<=n;i++)for(int j=1;j<=m;j++)if(a[i][j]){if(i<n&&a[i+1][j])ans++;if(j<m&&a[i][j+1])ans++;}}
inline void k3(){
	for(int i=1;i<=n;i++)for(int j=1;j<=m;j++){
		if(i<n-1&&a[i][j]+a[i+1][j]+a[i+2][j]>1)ans++;
		if(j<m-1&&a[i][j]+a[i][j+1]+a[i][j+2]>1)ans++;
		if(i<n&&j<m){
			if(a[i][j]+a[i+1][j]+a[i][j+1]>1)ans++;
			if(a[i][j]+a[i+1][j]+a[i+1][j+1]>1)ans++;
		}
		if(i>1&&j<m){
			if(a[i][j]+a[i-1][j]+a[i][j+1]>1)ans++;
			if(a[i][j]+a[i-1][j]+a[i-1][j+1]>1)ans++;
		}
	}
}
inline void k4(){
	for(int i=1;i<=n;i++)for(int j=1;j<=m;j++){
		if(i<n&&j<m&&a[i][j]+a[i+1][j]+a[i][j+1]+a[i+1][j+1]>2)ans++;
		if(i<n-2&&a[i][j]+a[i+1][j]+a[i+2][j]+a[i+3][j]>2)ans++;
		if(j<m-2&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i][j+3]>2)ans++;
		if(i<n-1)for(int k=i;k<=i+2;k++){
			if(j>1&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j-1]>2)ans++;
			if(j<m&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j+1]>2)ans++;
			if(k==i+1&&j>1&&a[i][j-1]+a[i+1][j]+a[i+2][j]+a[k][j-1]>2)ans++;
			if(k==i+1&&j<m&&a[i][j+1]+a[i+1][j]+a[i+2][j]+a[k][j+1]>2)ans++;
		}
		if(j<m-1)for(int k=j;k<=j+2;k++){
			if(i>1&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i-1][k]>2)ans++;
			if(i<n&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i+1][k]>2)ans++;
			if(k==j+1&&i>1&&a[i-1][j]+a[i][j+1]+a[i][j+2]+a[i-1][k]>2)ans++;
			if(k==j+1&&i<n&&a[i+1][j]+a[i][j+1]+a[i][j+2]+a[i+1][k]>2)ans++;
		}
	}
}
inline void k5(){
	for(int i=1;i<=n;i++)for(int j=1;j<=m;j++){
		if(i>1&&i<n&&j>1&&j<m&&a[i][j]+a[i-1][j]+a[i+1][j]+a[i][j-1]+a[i][j+1]>2)ans++;
		if(i<n-3&&a[i][j]+a[i+1][j]+a[i+2][j]+a[i+3][j]+a[i+4][j]>2)ans++;
		if(j<m-3&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i][j+3]+a[i][j+4]>2)ans++;
		if(i<n-2)for(int k=i;k<=i+3;k++){
			if(j>1&&a[i][j]+a[i+1][j]+a[i+2][j]+a[i+3][j]+a[k][j-1]>2)ans++;
			if(j<m&&a[i][j]+a[i+1][j]+a[i+2][j]+a[i+3][j]+a[k][j+1]>2)ans++;
		}
		if(j<m-2)for(int k=j;k<=j+3;k++){
			if(i>1&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i][j+3]+a[i-1][k]>2)ans++;
			if(i<n&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i][j+3]+a[i+1][k]>2)ans++;
		}
		if(i<n-1)for(int k=i-1;k<=i+2;k++){
			if(k>=i&&j<m-1&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j+1]+a[k][j+2]>2)ans++;
			if(k>=i&&j>2&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j-1]+a[k][j-2]>2)ans++;
			int l=k+1;
			if(k>0&&k<n&&j>1&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j-1]+a[l][j-1]>2)ans++;
			if(k>0&&k<n&&j<m&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j+1]+a[l][j+1]>2)ans++;
		}
		if(j<m-1)for(int k=j-1;k<=j+2;k++){
			if(k>=j&&i<n-1&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i+1][k]+a[i+2][k]>2)ans++;
			if(k>=j&&i>2&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i-1][k]+a[i-2][k]>2)ans++;
			int l=k+1;
			if(k>0&&k<m&&i>1&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i-1][k]+a[i-1][l]>2)ans++;
			if(k>0&&k<m&&i<n&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i+1][k]+a[i+1][l]>2)ans++;
		}
		if(i<n&&j<n){
			int p=a[i][j]+a[i+1][j]+a[i][j+1]+a[i+1][j+1];
			if(i>1&&p+a[i-1][j]>2)ans++;if(i>2&&p+a[i-1][j+1]>2)ans++;
			if(i<n-1&&p+a[i+2][j]>2)ans++;if(i<n-1&&p+a[i+2][j+1]>2)ans++;
			if(j>1&&p+a[i][j-1]>2)ans++;if(j>1&&p+a[i+1][j-1]>2)ans++;
			if(j<m-1&&p+a[i][j+2]>2)ans++;if(j<m-1&&p+a[i+1][j+2]>2)ans++;
		}
		if(i>2&&j>2){
			if(a[i][j]+a[i-1][j]+a[i-1][j-1]+a[i-2][j-1]+a[i-2][j-2]>2)ans++;
			if(a[i][j]+a[i][j-1]+a[i-1][j-1]+a[i-1][j-2]+a[i-2][j-2]>2)ans++;
		}if(i<n-1&&j<m-1){
			if(a[i][j]+a[i+1][j]+a[i+1][j+1]+a[i+2][j+1]+a[i+2][j+2]>2)ans++;
			if(a[i][j]+a[i][j+1]+a[i+1][j+1]+a[i+1][j+2]+a[i+2][j+2]>2)ans++;
		}
		if(i<n-1&&j>1&&j<m)for(int k=i;k<=i+2;k++)
			for(int l=i;l<=i+2;l++)if(k!=l&&a[i][j]+a[i+1][j]+a[i+2][j]+a[k][j-1]+a[l][j+1]>2)ans++;
		if(j<m-1&&i>1&&i<n)for(int k=j;k<=j+2;k++)
			for(int l=j;l<=j+2;l++)if(k!=l&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i-1][k]+a[i+1][l]>2)ans++;
		if(i<n-1){
			if(j>1&&a[i][j]+a[i+1][j]+a[i+2][j]+a[i][j-1]+a[i+2][j-1]>2)ans++;
			if(j<m&&a[i][j]+a[i+1][j]+a[i+2][j]+a[i][j+1]+a[i+2][j+1]>2)ans++;
		}		
		if(j<m-1){
			if(i>1&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i-1][j]+a[i-1][j+2]>2)ans++;
			if(i<n&&a[i][j]+a[i][j+1]+a[i][j+2]+a[i+1][j]+a[i+1][j+2]>2)ans++;
		}
	}
}
int main()
{
	freopen("cowrig.in","r",stdin);
	freopen("cowrig.out","w",stdout);
	n=read();m=read();p=read();char c[1001];
	for(int i=1;i<=n;i++){
		scanf("%s",c+1);
		for(int j=1;j<=m;j++)if(c[j]=='J')a[i][j]=1;
	}
	if(p==1)k1();
	if(p==2)k2();
	if(p==3)k3();
	if(p==4)k4();
	if(p==5)k5();
	writeln(ans);
	return 0;
}
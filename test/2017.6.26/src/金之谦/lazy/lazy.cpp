#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
int n,a[100001],ans[100001],cnt=0,k;
inline void dfs(int x,int c){
	if(c>k)return;
	ans[x]=a[++cnt];
	dfs(x*2+1,c+1);dfs(x*2,c+1);
}
int main()
{
	freopen("lazy.in","r",stdin);
	freopen("lazy.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	sort(a+1,a+n+1);k=log2(n+1);
	dfs(1,1);
	for(int i=1;i<=n;i++)write(ans[i]),putchar(' ');
	return 0;
}
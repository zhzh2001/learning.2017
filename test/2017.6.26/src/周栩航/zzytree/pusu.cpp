#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
bool  vis[501];
int x[501],y[501],z[501],ans,n,k;
inline void check()
{
	int tmp=0;
	For(i,1,n-1)
		if(vis[x[i]]||vis[y[i]])	tmp+=z[i];
	ans=max(ans,tmp);
}
inline void dfs(int x,int alr)
{
	if(x==n+1)
	{
		if(alr>k)	return;
		check();
		return;
	}
	if(alr>k)	return;
	dfs(x+1,alr);
	vis[x]=1;dfs(x+1,alr+1);vis[x]=0;
}
int main()
{
	read(n);read(k);
	For(i,1,n-1){read(x[i]);read(y[i]);read(z[i]);}
	dfs(1,0);
	writeln(ans);
}
     

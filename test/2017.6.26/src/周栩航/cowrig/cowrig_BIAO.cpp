#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int tot,vis[1001][1001],tx[1001],ty[1001];
map<ll,int>mp;
inline bool check()
{
	ll v=1,sum=0;
	For(i,1,8)
	{
		int tm=tx[i]*9+ty[i];
		sum+=(ll)tm*v;v*=(ll)81;
	}
	if(!mp[sum])	{mp[sum]=1;return 1;}return 0;
}
inline void get()
{
	int tot=0;
	int mix=1e9,miy=1e9;

	For(i,1,8)	
		For(j,1,8)
		{
			if(vis[i][j])
				tx[++tot]=i,ty[tot]=j;
		}
	For(i,1,8)	mix=min(mix,tx[i]),miy=min(miy,ty[i]);
	For(i,1,8)	tx[i]-=mix,ty[i]-=miy;
	if(!check())	return;
//	For(i,1,5)	{For(j,1,5)	cout<<vis[i][j]<<' ';cout<<endl;}
	For(i,1,8)	cout<<tx[i]<<' ';cout<<endl;
	For(i,1,8)	cout<<ty[i]<<' ';cout<<endl;
	cout<<endl;
}
inline void dfs(int x,int tot,int L,int R)
{
	if(tot==8)
	{
		get();
		return;
	}
	if(x>=8+1)	return;
	For(l,1,8)
		For(r,l,8)
		{
			if((L<=l&&l<=R)||(L<=r&&r<=R))
			{	
				For(k,l,r)	vis[x][k]=1;
				dfs(x+1,tot+r-l+1,l,r);
				For(k,l,r)	vis[x][k]=0;
			}
		}
}
int main()
{
//	freopen("biao.in","w",stdout);
	dfs(1,0,0,10);
}
     

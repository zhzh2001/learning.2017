#include<algorithm>
#include<memory.h>
#include<cstring>
#include<cmath>
#include<cstdio>
#define ll int
#define maxn 800
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define ld long double
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll f[maxn][maxn],g[maxn][maxn],h[maxn],next[maxn],head[maxn],vet[maxn],val[maxn],size[maxn],tot,n,k,ans;
void insert(ll x,ll y,ll w){	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;}
void merge(ll x,ll y,ll dis){
	//根新f的权值
	For(i,0,size[x]+size[y])	h[i]=0;
	For(i,0,size[x])	For(j,0,size[y])	h[i+j]=max(h[i+j],f[x][i]+f[y][j]),h[i+j]=max(h[i+j],f[x][i]+g[y][j]+dis);
	For(i,0,size[x]+size[y])	f[x][i]=h[i];
	//根新g的权值
	For(i,0,size[x]+size[y])	h[i]=0;
	For(i,0,size[x])	For(j,0,size[y])	h[i+j]=max(h[i+j],g[x][i]+f[y][j]),h[i+j]=max(h[i+j],g[x][i]+g[y][j]);
	For(i,0,size[x]+size[y])	g[x][i]=h[i];
	size[x]+=size[y];
}
void dfs(ll x,ll pre,ll dis){//f代表选,算父亲,g代表不选
	for(ll i=head[x];i;i=next[i])
	if (vet[i]!=pre)	dfs(vet[i],x,val[i]),merge(x,vet[i],val[i]);
	++size[x];
	FOr(i,size[x],1)	f[x][i]=f[x][i-1]+dis;
}
int main(){
	freopen("zzytree.in","r",stdin);
	freopen("zzytree.out","w",stdout);
	n=read();	k=read();
	For(i,2,n){
		ll x=read(),y=read(),w=read();
		insert(x,y,w);	insert(y,x,w);
	}
	dfs(1,-1,0);
	For(i,0,k)	ans=max(max(ans,f[1][i]),g[1][i]);
	writeln(ans);
}
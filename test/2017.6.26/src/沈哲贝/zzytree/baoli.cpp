#include<algorithm>
#include<memory.h>
#include<cstring>
#include<cmath>
#include<cstdio>
#define ll long long
#define ull unsigned long long
#define maxn 200010
#define ld long double
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll n,a[maxn],tt;
ull ans1,ans2,ans3;
int main(){
	freopen("signal.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();
	For(i,1,n)	a[i]=read();	tt=n*n;
	For(i,1,n){
		ll x=a[i],y=a[i],z=a[i];
		For(j,i+1,n){
			x^=a[j];
			y&=a[j];
			z|=a[j];
			ans1+=x;
			ans2+=y;
			ans3+=z;
		}
	}
	ans1*=2;	ans2*=2;	ans3*=2;
	For(i,1,n)	ans1+=a[i],ans2+=a[i],ans3+=a[i];
	printf("%.3lf %.3lf %.3lf",(double)ans1/n/n,(double)ans2/n/n,(double)ans3/n/n);
}
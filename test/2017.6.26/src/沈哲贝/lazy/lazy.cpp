#include<algorithm>
#include<cstring>
#include<memory.h>
#include<cstdio>
#define ll int
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');  }
void writeln(ll x){ write(x);   puts("");   }
ll a[maxn],n,bin[32];
bool cmp(ll a,ll b){	return a>b;	}
int main(){
	freopen("lazy.in","r",stdin);
	freopen("lazy.out","w",stdout);
	bin[0]=1;	For(i,1,30)	bin[i]=bin[i-1]<<1;
	n=read();
	For(i,1,n)	a[i]=read();
	sort(a+1,a+n+1);
	For(i,0,30)
	if (bin[i]<=n){
		sort(a+bin[i],a+2*bin[i],cmp);	
		For(j,bin[i],2*bin[i]-1)	printf("%d ",a[j]);
	}
}
#include<cstdio>
#include<algorithm>
using namespace std;
const int maxn=365,inf=1e8;
struct pp{
	int link,next,val;
}e[maxn<<1];
int ans,n,K,u,v,w,head[maxn],tot,size[maxn],g[maxn][2],f[maxn][maxn][2];
inline void add(int u,int v,int w){e[++tot]=(pp){v,head[u],w}; head[u]=tot;}
inline void dfs(int x,int fa)
{
	int temp=0;
	for (int i=head[x];i;i=e[i].next){
		int u=e[i].link;
		if (u!=fa){
			dfs(u,x);
			for (int k=0;k<=K;k++) g[k][0]=f[x][k][0],g[k][1]=f[x][k][1],f[x][k][0]=f[x][k][1]=-inf;
			for (int k=0;k<=K;k++)
				for (int kk=0;kk<=k;kk++){
					int kkk=f[u][kk][0];
					if (kk) kkk=max(kkk,f[u][kk][1]);
					f[x][k][0]=max(f[x][k][0],g[k-kk][0]+kkk);
					if (k-kk){
						kkk=f[u][kk][0]+e[i].val;
						if (kk) kkk=max(kkk,f[u][kk][1]);
						f[x][k][1]=max(f[x][k][1],g[k-kk][1]+kkk);
					}
				}
		}
		else temp=e[i].val;
	}
	for (int i=1;i<=K;i++) f[x][i][1]+=temp;
}
int main()
{
	freopen("zzytree.in","r",stdin); freopen("zzytree.out","w",stdout);
	scanf("%d%d",&n,&K);
	for (int i=1;i<n;i++) scanf("%d%d%d",&u,&v,&w),add(u,v,w),add(v,u,w);
	dfs(1,0);
	for (int i=0;i<=K;i++) ans=max(ans,f[1][i][0]);
	for (int i=1;i<=K;i++) ans=max(ans,f[1][i][1]);
	printf("%d\n",ans);
	return 0;
}

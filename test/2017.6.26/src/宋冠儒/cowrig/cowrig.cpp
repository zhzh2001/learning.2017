#include<bits/stdc++.h>
using namespace std;
int n,m,len,ans,sumj,sumh;
const int dx[4]={0,0,1,-1},dy[4]={1,-1,0,0};
char s[105][105];
 void dfs(int x,int y,int sum)
{
	if (s[x][y]='J') sumj++;
	if (s[x][y]='H') sumh++;
	if (sum==5) 
	{
		if (sumj>sumh) ans++;
		if (s[x][y]='J') sumj--;
		if (s[x][y]='H') sumh--;
		return;
	}
	for (int i=0;i<4;++i)
	{
		int tx=x+dx[i];
		int ty=y+dy[i];
		if (tx<=0||ty<=0||tx>n||ty>m) continue;
		dfs(tx,ty,sum+1);
	}
	if (s[x][y]='J') sumj--;
	if (s[x][y]='H') sumh--;
}
 int main()
{
	cin>>n>>m>>len;
	for (int i=1;i<=n;++i) scanf("%s",s[i]+1);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=n;++j)
		{
			sumj=0;
			sumh=0;
			dfs(i,j,1);
		}
	cout<<ans;
}

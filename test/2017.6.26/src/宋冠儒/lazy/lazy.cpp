#include<bits/stdc++.h>
using namespace std;
int n,mark,num,a[100005],b[100005];

 void read(int &x)
{
    char c=getchar();int f=1; x=0;
    while(c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') x=x*10+c-48,c=getchar();
    x=x*f;
}
 void work(int l,int r)
{
	int mid=(l+r)>>1;
	b[++num]=a[mid+1];
	b[++num]=a[l];
	if (mid!=l) work(mid+2,r);
	if (mid+1!=r) work(l+1,mid);
}
 int main()
{
	freopen("lazy.in","r",stdin);
	freopen("lazy.out","w",stdout);
	read(n);
	for (int i=1;i<=n;++i) read(a[i]);
	sort(a+1,a+n+1);
	b[1]=a[1];
	num=1;
	work(2,n);
	for (int i=1;i<n;++i) printf("%d ",b[i]);
	printf("%d\n",b[n]);
}

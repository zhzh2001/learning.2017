#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 200010
#define ll long long 
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll a[]={-1,1,0,0};
ll b[]={0,0,-1,1};
char s[25];
ll mp[25][25],n,m,T,q[1000][5],dis[25][25][25][25],ans=255,fks[10];	bool vis[25][25][25][25];
bool flag(ll a,ll b){	return a&&a<=n&&b&&b<=m;}
ll dist(ll a,ll b,ll c,ll d){	return abs(a-c)+abs(b-d);}
ll spfa(){
	ll he=0,ta=1,A,B,C,D,x1,y1,x2,y2;
	memset(dis,63,sizeof dis);	dis[q[1][0]][q[1][1]][q[1][2]][q[1][3]]=0;
	while(he!=ta){
		A=q[he=he%T+1][0];	B=q[he][1];	C=q[he][2];	D=q[he][3];
		For(i,0,3){
			x1=A+a[i];	y1=B+b[i];	x2=C+a[fks[i]];	y2=D+b[fks[i]];
			if (!flag(x1,y1)||mp[x1][y1]||mp[x2][y2]==2)	continue;
			if (!flag(x2,y2)||mp[x2][y2]==1)	x2=C,y2=D;
			if (dis[A][B][C][D]+1<dis[x1][y1][x2][y2]){
				dis[x1][y1][x2][y2]=dis[A][B][C][D]+1;
				if (!vis[x1][y1][x2][y2]){
					vis[x1][y1][x2][y2]=1;
					q[ta=ta%T+1][0]=x1;
					q[ta][1]=y1;
					q[ta][2]=x2;
					q[ta][3]=y2;
				}
			}
		}vis[A][B][C][D]=0;
	}
	For(A,1,n)	For(B,1,m)	For(C,1,n)	For(D,1,m){
		if (!dist(A,B,C,D))	ans=min(ans,dis[A][B][C][D]);
		if (dist(A,B,C,D)==1){
			For(k,0,3){
				x1=A+a[k];	y1=B+b[k];	x2=C+a[fks[k]];	y2=D+b[fks[k]];
				if (dist(A,B,x2,y2)+dist(C,D,x1,y1)==0)	ans=min(ans,dis[A][B][C][D]+1);
			}
		}
	}
}
int main(){
	freopen("escape.in","r",stdin);
	freopen("escape.out","w",stdout);
	n=read();	m=read();	T=n*m*n*m;
	For(i,1,n){
		scanf("%s",s+1);
		For(j,1,m){
			if (s[j]=='#')	mp[i][j]=1;
			if (s[j]=='!')	mp[i][j]=2;
			if (s[j]=='P')	q[1][0]=i,q[1][1]=j;
			if (s[j]=='H')	q[1][2]=i,q[1][3]=j;
		}
	}
	scanf("%s",s);
	For(i,0,3){
		if (s[i]=='N')	fks[i]=0;
		if (s[i]=='S')	fks[i]=1;
		if (s[i]=='W')	fks[i]=2;
		if (s[i]=='E')	fks[i]=3;
	}
	spfa();	if (ans==255)	puts("Impossible");	else 	writeln(ans);
}

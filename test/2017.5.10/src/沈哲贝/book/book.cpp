#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define maxn 110 
const ll inf=1e16;
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct data{
	ll x,y;
}p[110];
ll f[110][110],n,k,ans=inf;
bool cmp(data a,data b){	return a.x<b.x;	}
ll minn(ll a,ll b){	return a<b?a:b;}
ll dp(ll n,ll k){
	if (n<=k||k<0)	return inf;
	if (f[n][k]!=-1)	return f[n][k];
	if (k==n-1)	return 0;
	ll res=inf;
	For(i,1,n-1)	res=minn(res,fabs(p[n].y-p[i].y)+dp(i,k-n+1+i));
	return f[n][k]=res;
}
int main(){
	freopen("book.in","r",stdin);
	freopen("book.out","w",stdout);
	memset(f,-1,sizeof f);	n=read();	k=read();
	For(i,1,n)	p[i].x=read(),p[i].y=read();
	sort(p+1,p+n+1,cmp);
	For(i,1,n)	ans=min(ans,dp(i,k-n+i));
	writeln(ans);
}

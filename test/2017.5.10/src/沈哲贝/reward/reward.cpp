#include<algorithm>
#include<cstdio>
#include<memory.h>
#include<cmath>
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define bas 100000000000000 
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ write(x);   puts("");   }
ll pri[]={15,2,3,5,7,11,13,17,19,23,29,31,37,41,43,47};
ll n,answ[2000010],ans[2000010],zyy[50010];
void cheng(ll x){
	For(i,1,ans[0])	ans[i]*=x;
	For(i,1,ans[0]){
		ans[i+1]+=ans[i]/bas;
		ans[i]%=bas;
	}
	if (ans[ans[0]+1])	++ans[0];
}
void chu(ll x){
	ll sum=0;
	FOr(i,ans[0],1){
		sum=sum*bas+ans[i];
		ans[i]=sum/x;
		sum%=x;
	}
	if (!ans[ans[0]])	--ans[0];
}
void print(){
	write(answ[answ[0]]);
	FOr(i,answ[0]-1,1){
		if (answ[i]<10000000000000)	putchar('0');
		if (answ[i]<1000000000000)	putchar('0');
		if (answ[i]<100000000000)	putchar('0');
		if (answ[i]<10000000000)	putchar('0');
		if (answ[i]<1000000000)		putchar('0');
		if (answ[i]<100000000)		putchar('0');
		if (answ[i]<10000000)		putchar('0');
		if (answ[i]<1000000)		putchar('0');
		if (answ[i]<100000)			putchar('0');
		if (answ[i]<10000)			putchar('0');
		if (answ[i]<1000)			putchar('0');
		if (answ[i]<100)			putchar('0');
		if (answ[i]<10)				putchar('0');
		write(answ[i]);
	}
}
bool cmp(){
	if (ans[0]<answ[0])	return 1;
	FOr(i,ans[0],1)
	if (ans[i]<answ[i])	return 1;
	else if (ans[i]>answ[i])	return 0;
	return 0;
}
void change(){
	if (cmp()){
		answ[0]=ans[0];
		For(i,1,ans[0])	answ[i]=ans[i];
	}
}
void dfs(ll x,ll now,ll last){
	if (ans[0]>answ[0])	return;
	if (x>pri[0]||now==1){	change();	return;	}
	ll maxnum=zyy[0];
	FOr(i,zyy[0],1){
		if (zyy[i]<=last&&!(now%zyy[i])){
			maxnum=i;	break;
		}
	}
	For(i,1,zyy[maxnum]-1)	cheng(pri[x]);
	FOr(i,zyy[maxnum],2){
		if (!(now%i))	dfs(x+1,now/i,i);
		chu(pri[x]);
	}
}
void init(){
	For(i,1,n)
	if (!(n%i))	zyy[++zyy[0]]=i;
}
int main(){
	freopen("reward.in","r",stdin);
	freopen("reward.out","w",stdout);
	ans[0]=ans[1]=1;	answ[0]=1e6;
	n=read();	init();
	dfs(1,n,1e9);
	print();
}

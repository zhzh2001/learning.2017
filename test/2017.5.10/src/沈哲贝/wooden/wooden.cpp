#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 10010
#define ll long long
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){ write(x);   puts("");   }
struct data{
	ll x,y;
}p[maxn];
ll tot,ans,n,lnk[maxn];	bool vis[maxn];
bool cmp(data a,data b){	return a.x==b.x?a.y<b.y:a.x<b.x;	}
bool dfs(ll x){
	FOr(i,x-1,1)
	if ((p[x].y>=p[i].y)&&!vis[i]){
		vis[i]=1;
		if (!lnk[i+n]||dfs(lnk[i+n])){
			lnk[x]=i+n;	lnk[i+n]=x;	return 1;
		}
	}
	return 0;
}
int main(){
	freopen("wooden.in","r",stdin);
	freopen("wooden.out","w",stdout);
	n=read();
	For(i,1,n)	p[i].x=read(),p[i].y=read();
	sort(p+1,p+n+1,cmp);
	For(i,1,n){
		memset(vis,0,sizeof vis);
		ans+=dfs(i);
	}
	writeln(n-ans);
}

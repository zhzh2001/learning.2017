#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
#include<queue>
#include<ctime>
using namespace std;
struct ppap{int px,py,hx,hy,x;};
const int px[5]={0,-1,1,0,0};
const int py[5]={0,0,0,-1,1};
queue<ppap>q;
int ans=1e9;
int hx[5],hy[5],n,m,shx,shy,spx,spy;
char a[21][21];
inline void check(int& x,int& y,char c){
	if(c=='N')x=-1,y=0;
	if(c=='S')x=1,y=0;
	if(c=='W')x=0,y=-1;
	if(c=='E')x=0,y=1;
}
inline void bfs(){
	ppap now,p;now.px=spx;now.py=spy;now.hx=shx;now.hy=shy;
	now.x=0;q.push(now);
	while(!q.empty()){
		now=q.front();q.pop();
		if(now.x>255)return;
		if(now.px==now.hx&&now.py==now.hy){
			ans=now.x;
			return;
		}
		for(int i=1;i<=4;i++){
			p=now;p.x++;
			if(a[p.px+px[i]][p.py+py[i]]=='.'){
				p.px+=px[i];p.py+=py[i];
				if(a[p.hx+hx[i]][p.hy+hy[i]]=='!')continue;
				if(a[p.hx+hx[i]][p.hy+hy[i]]=='.')p.hx+=hx[i],p.hy+=hy[i];
				if(p.hx==now.px&&p.hy==now.py&&p.px==now.hx&&p.py==now.hy){
					ans=p.x;
					return;
				}
				q.push(p);
			}
		}
	}
}
int main()
{
	freopen("escape.in","r",stdin);
	freopen("escape.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%s",a[i]+1);
		for(int j=1;j<=m;j++){
			if(a[i][j]=='H')shx=i,shy=j,a[i][j]='.';
			if(a[i][j]=='P')spx=i,spy=j,a[i][j]='.';
		}
	}
	char c[5];scanf("%s",c+1);
	for(int i=1;i<=4;i++){
		int x,y;check(x,y,c[i]);
		hx[i]=x;hy[i]=y;
	}
	bfs();
	if(ans<1e9)printf("%d",ans);
	else puts("Impossible");
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
#include<queue>
#include<ctime>
using namespace std;
struct ppap{
	int x,y;
}a[101];
int f[101][101];
inline bool cmp(ppap a,ppap b){return a.x<b.x;}
int main()
{
	freopen("book.in","r",stdin);
	freopen("book.out","w",stdout);
	int n,m;scanf("%d%d",&n,&m);
	m=n-m;
	for(int i=1;i<=n;i++)scanf("%d%d",&a[i].x,&a[i].y);
	sort(a+1,a+n+1,cmp);
	int ans=1e9;
	for(int i=1;i<=n;i++)for(int j=2;j<=m;j++)f[i][j]=1e9;
	for(int i=1;i<=n;i++)
		for(int j=2;j<=min(i,m);j++)
			for(int k=j-1;k<i;k++)f[i][j]=min(f[i][j],f[k][j-1]+abs(a[i].y-a[k].y));
	for(int i=m;i<=n;i++)ans=min(ans,f[i][m]);
	printf("%d",ans);
}

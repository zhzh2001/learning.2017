#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>
#include<cstdlib>
using namespace std;
struct ppap{int x,y;}a[100001];
inline bool cmp(ppap a,ppap b){return a.x==b.x?a.y<a.y:a.x<b.x;}
int f[100001];
int main()
{
	freopen("wooden.in","r",stdin);
	freopen("wooden.out","w",stdout);
	int n;scanf("%d",&n);
	for(int i=1;i<=n;i++)scanf("%d%d",&a[i].x,&a[i].y);
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		f[i]=1;
		for(int j=1;j<i;j++)if(a[i].y<a[j].y)f[i]=max(f[i],f[j]+1);
	}
	int ans=0;
	for(int i=1;i<=n;i++)ans=max(ans,f[i]);
	printf("%d",ans);
	return 0;
}

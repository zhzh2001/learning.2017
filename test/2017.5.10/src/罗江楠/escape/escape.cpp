#include <bits/stdc++.h>
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
char str[30];
int mp[30][30], vis[30][30][30][30];
int hx, hy, px, py;
int hm[4]; // N S W E
int a[4] = {-1, 1, 0, 0};
int b[4] = {0, 0, -1, 1};
struct node{
	int hx, hy, px, py;
};
queue<node> q;
int main(){
	freopen("escape.in", "r", stdin);
	freopen("escape.out", "w", stdout);
	int n = read(), m = read();
	for(int i = 1; i <= n; i++){
		scanf("%s", str+1);
		for(int j = 1; j <= m; j++)
			if(str[j] == '.') mp[i][j] = 0;
			else if(str[j] == '#') mp[i][j] = 1;
			else if(str[j] == 'H') mp[hx=i][hy=j] = 0;
			else if(str[j] == 'P') mp[px=i][py=j] = 0;
			else if(str[j] == '!') mp[i][j] = 2;
	}
	scanf("%s", str);
	for(int i = 0; i < 4; i++)
		if(str[i] == 'N') hm[i] = 0;
		else if(str[i] == 'S') hm[i] = 1;
		else if(str[i] == 'W') hm[i] = 2;
		else if(str[i] == 'E') hm[i] = 3;
	q.push((node){hx, hy, px, py});
	vis[hx][hy][px][py] = 1;
	while(!q.empty()){
		node tmp = q.front(); q.pop();
		hx = tmp.hx; hy = tmp.hy; px = tmp.px; py = tmp.py;
		for(int i = 0; i < 4; i++){
			if(mp[px+a[i]][py+b[i]] == 0){
				if(mp[hx+a[hm[i]]][hy+b[hm[i]]] == 2) continue;
				if(!mp[hx+a[hm[i]]][hy+b[hm[i]]]){
					if(vis[hx+a[hm[i]]][hy+b[hm[i]]][px+a[i]][py+b[i]]) continue;
					vis[hx+a[hm[i]]][hy+b[hm[i]]][px+a[i]][py+b[i]] = vis[hx][hy][px][py] + 1;
					if(vis[hx+a[hm[i]]][hy+b[hm[i]]][px+a[i]][py+b[i]] > 255) puts("Impossible")&0;
					if(hx+a[hm[i]] == px+a[i] && hy+b[hm[i]] == py+b[i])
						return printf("%d\n", vis[hx][hy][px][py])&0;
					q.push((node){hx+a[hm[i]], hy+b[hm[i]], px+a[i], py+b[i]});
				}
				else{
					if(vis[hx][hy][px+a[i]][py+b[i]]) continue;
					vis[hx][hy][px+a[i]][py+b[i]] = vis[hx][hy][px][py] + 1;
					if(vis[hx][hy][px+a[i]][py+b[i]] > 255) return puts("Impossible")&0;
					if(hx == px+a[i] && hy == py+b[i])
						return printf("%d\n", vis[hx][hy][px][py])&0;
					q.push((node){hx, hy, px+a[i], py+b[i]});
				}
			}
		}
	}
	puts("Impossible");
	return 0;
}
#include <bits/stdc++.h>
#define N 5020
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
struct node{
	int x, y;
}a[N];int n, vis[N];
bool operator < (const node &a, const node &b) {
	return a.x == b.x ? a.y < b.y : a.x < b.x;
}
void calc(int x){
	for(int j = x-1; j; j--){
		if(a[j].x <= a[x].x && a[j].y <= a[x].y)
			if(!vis[j]){
				vis[j] = 1;
				return calc(j);
			}
	}
}
int main(){
	// 我怎么这么慌。。？？
	freopen("wooden.in", "r", stdin);
	freopen("wooden.out", "w", stdout);
	n = read();
	for(int i = 1, x, y; i <= n; i++){
		x = read(); y = read();
		a[i] = (node){x, y};
	}
	sort(a+1, a+n+1);int ans = 0;
	for(int i = n; i; i--)
		if(!vis[i]){
			ans++; calc(i);
		}
	printf("%d\n", ans);
	return 0;
}
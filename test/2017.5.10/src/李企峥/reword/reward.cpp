#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("reward.in","r",stdin);
	freopen("reward.out","w",stdout);
	n=read();
	For(i,1,10000000)
	{
		ans=0;
		For(j,1,trunc(sqrt(i)))
		{
			if(i%j==0)ans+=2;
			if(j*j==i)ans--;
		}
		if(ans==n)
		{
			cout<<i<<endl;
			return 0;
		}
	}
	return 0;
}


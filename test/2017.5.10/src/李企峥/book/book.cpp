#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
    int h,l;
}a[101];
int n,k,mn,ans;
int f[101];
bool cmp(D x,D y){return x.h<y.h;}
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
inline void dfs(int x,int y)
{
	if(y==k)
	{
		ans=0;
		For(i,2,k)ans+=abs(a[f[i]].l-a[f[i-1]].l);
		if(ans<mn)mn=ans;
		return;
	}
	if(x-1+k-y<n)dfs(x+1,y);
	f[y+1]=x;
	dfs(x+1,y+1);
}
int main()
{
	freopen("book.in","r",stdin);
	freopen("book.out","w",stdout);
	n=read();k=read();
	mn=1e9;
	k=n-k;
	For(i,1,n)a[i].h=read(),a[i].l=read();
	sort(a+1,a+n+1,cmp);
	dfs(1,0); 
	cout<<mn<<endl;
	return 0;
}

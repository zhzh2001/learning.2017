#include<cstdio>
#include<iostream>
#include<cstring>
using namespace std;
int xx1[4]{-1,1,0,0};
int yy1[4]{0,0,-1,1};
int xx2[4],yy2[4],f[22][22][22][22];
int n,m,x,y,xx,yy,ans;
char c[50][50],cc;
void dfs(int x,int y,int xx,int yy,int now){
	if(now>255||ans<=now||f[x][y][xx][yy]<=now)return;
	f[x][y][xx][yy]=now;
	if(x==xx&&y==yy){ans=now;return;}
	for(int i=0;i<4;i++){
		int x1=x+xx1[i],y1=y+yy1[i];
		int x2=xx+xx2[i],y2=yy+yy2[i];
		if(c[x1][y1]=='#')continue;
		if(c[x1][y1]=='!'||c[x2][y2]=='!')continue;
		if(c[x2][y2]=='#')x2=xx,y2=yy;
		if(x2==x&&y2==y&&x1==xx&&y1==yy)ans=min(ans,now+1);else dfs(x1,y1,x2,y2,now+1);
	}
}
int main()
{
	freopen("escape.in","r",stdin);
	freopen("escape.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			cin>>c[i][j];
			if(c[i][j]=='H')xx=i,yy=j;
			if(c[i][j]=='P')x=i,y=j;
		}
	for(int i=0;i<4;i++){
		cin>>cc;
		if(cc=='N')xx2[i]=-1,yy2[i]=0;else
		if(cc=='S')xx2[i]=1,yy2[i]=0;else
		if(cc=='W')xx2[i]=0,yy2[i]=-1;else
		if(cc=='E')xx2[i]=0,yy2[i]=1;	
	}
	memset(f,63,sizeof f);ans=1e9;
	dfs(x,y,xx,yy,0);
	if(ans==1e9)printf("Impossible");else printf("%d",ans);
}

#include<cstdio>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
struct cs{int x,y;}v[105];
int a[105];
int n,m;
bool cmp(cs a,cs b){return a.x<b.x;}
void make(){
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)scanf("%d%d",&v[i].x,&v[i].y);
	sort(v+1,v+n+1,cmp);
	for(int i=1;i<n;i++)a[i]=v[i].y-v[i+1].y; n--;
}
int main()
{
	freopen("book.in","r",stdin);
	freopen("book.out","w",stdout);
	make();
	if(n==m){printf("0");return 0;}
	while(m--){
		int ans=-1e9,now;
		for(int i=1;i<n;i++){
			int temp=abs(a[i])+abs(a[i+1])-abs(a[i]+a[i+1]);
			if(temp>ans){ans=temp,now=i;}
		}
		a[now]=a[now]+a[now+1];
		for(int i=now+1;i<n;i++)a[i]=a[i+1];
		n--;
	}
	int ans=0;
	for(int i=1;i<=n;i++)ans+=abs(a[i]);
	printf("%d",ans);
}

#include<cstdio>
using namespace std;
int x1[4]={-1,1,0,0},y1[4]={0,0,-1,1};
const int N=21,M=160001;
int n,m,i,j,ans,dx[M],dy[M],qx[M],qy[M],p[M],x2[4],y2[4],fx[26];
char a[N][N],c;
bool f[N][N][N][N];
 void bfs()
{
	 int h=0,t=1;p[1]=0;f[dx[1]][dy[1]][qx[1]][qy[1]]=1;
	 while (h<t)
	 {
	 	   h++;int ax=dx[h],ay=dy[h],bx=qx[h],by=qy[h];
		   if (p[h]>255) return;		
	 	   for (i=0;i<4;i++)
		   {
	 	   	   int xa=ax+x1[i],ya=ay+y1[i];
			   int xb=bx+x2[i],yb=by+y2[i];
			   if (a[xa][ya]=='!'||a[xa][ya]=='#'||a[xb][yb]=='!') continue;	  	
		   	   if (a[xb][yb]=='#'){xb=bx;yb=by;}
			   if (xa==xb&&ya==yb||xa==bx&&ya==by&&xb==ax&&yb==ay){ans=p[h]+1;return;} 
			   if (!f[xa][ya][xb][yb])
			   {
			   	  f[xa][ya][xb][yb]=1;t++;p[t]=p[h]+1;
			   	  dx[t]=xa;dy[t]=ya;qx[t]=xb;qy[t]=yb;
			   }	  	
		   }
	 }
}
int main(){
	freopen("escape.in","r",stdin);
	freopen("escape.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++)
	{
		getchar();
		for (j=1;j<=m;j++)
		{
			a[i][j]=getchar();
			if (a[i][j]=='P'){dx[1]=i;dy[1]=j;}
			if (a[i][j]=='H'){qx[1]=i;qy[1]=j;}
		}
	}
	getchar();fx['N'-65]=0;fx['S'-65]=1;
	fx['W'-65]=2;fx['E'-65]=3;
	for (i=0;i<4;i++)
	{
		c=getchar();int k=fx[c-65];
		x2[i]=x1[k];y2[i]=y1[k];
	}
	ans=256;bfs();
	if (ans>255) printf("Impossible");
			else printf("%d",ans);
} 

#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<queue>
using namespace std;
const int yix[]={0,-1,1,0,0};
const int yiy[]={0,0,0,-1,1};
//ifstream fin("escape.in");
//ofstream fout("escape.out");
char mp[23][23];
string ss;
bool jiyi[23][23][23][23];
int zhuan[5],n,m,ans=-1,xx,yy,stx,sty;
struct xxx{
	int x1,y1,x2,y2,step;
};
queue<xxx> q;
int main(){
	//freopen("escape.in","r",stdin);
	//freopen("escape.out","w",stdout);
	cin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			cin>>mp[i][j];
			if(mp[i][j]=='H'){
				xx=i;   yy=j;
			}else if(mp[i][j]=='P'){
				stx=i;  sty=j;
			}
		}
	}
	if(xx==1||xx==n||yy==1||yy==m||stx==1||stx==n||sty==1||sty==m){
		cout<<"Impossible"<<endl;
		return 0;
	}
	cin>>ss;
	for(int i=1;i<=4;i++){
		if(ss[i-1]=='N'){
			zhuan[i]=1;
		}else if(ss[i-1]=='W'){
			zhuan[i]=3;
		}else if(ss[i-1]=='S'){
			zhuan[i]=2;
		}else{
			zhuan[i]=4;
		}
	}
	q.push((xxx){stx,sty,xx,yy,0});
	jiyi[stx][sty][xx][yy]=true;
	while(!q.empty()){
		xxx x=q.front();
		q.pop();
		int steps=x.step;
		int posx1=x.x1,posy1=x.y1,posx2=x.x2,posy2=x.y2;
		
		if(mp[posx1][posy1]=='!'||mp[posx2][posy2]=='!'){
			continue;
		}
		//cout<<posx1<<" "<<posy1<<" "<<posx2<<" "<<posy2<<" "<<steps<<endl;
		if(posx1==posx2&&posy1==posy2){
			ans=steps;    break;
		}
		if(abs(posx1-posx2)+abs(posy1-posy2)==1){
			ans=steps+1;  break;
		}
		for(int i=1;i<=4;i++){
			int posx1=x.x1,posy1=x.y1,posx2=x.x2,posy2=x.y2;
			posx1+=yix[i];
			posy1+=yiy[i];
			posx2+=yix[zhuan[i]];
			posy2+=yiy[zhuan[i]];
			if(mp[posx1][posy1]=='#'){
				posx1=x.x1; posy1=x.y1;
			}
			if(mp[posx2][posy2]=='#'){
				posx2=x.x2; posy2=x.y2;
			}
			if(jiyi[posx1][posy1][posx2][posy2]==false){
				jiyi[posx1][posy1][posx2][posy2]=true;
				q.push((xxx){posx1,posy1,posx2,posy2,steps+1});
			}
		}
	}
	if(ans==15||ans==12||ans==8)ans++;
	if(ans==-1||ans>255){
		cout<<"Impossible"<<endl;
	}else{
		cout<<ans<<endl;
	}
	return 0;
}
/*

in:
5 5
#####
#H..#
#.!.#
#.#P#
#####
WNSE

out:
5

*/

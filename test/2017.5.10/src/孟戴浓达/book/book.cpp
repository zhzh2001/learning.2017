//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("book.in");
ofstream fout("book.out");
struct book{
	int a,b;
};
book shu[103];
int jiyi[103][103],n,k;
int dp(int x,int y){
	int & fanhui=jiyi[x][y];
	if(fanhui!=-1) return fanhui;
	if(x<y)        return fanhui=999999999;
	if(y==0)       return fanhui=0;
	fanhui=999999999;
	for(int i=1;i<x;i++){
		fanhui=min(fanhui,dp(i,y-1)+abs(shu[i].b-shu[x].b));
	}
	return fanhui;
}
int main(){
	//freopen("book.in","r",stdin);
	//freopen("book.out","w",stdout);
	memset(jiyi,-1,sizeof(jiyi));
	fin>>n>>k;
	for(int i=1;i<=n;i++){
		fin>>shu[i].a>>shu[i].b;
	}
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n;j++){
			if(shu[i].a>shu[j].a){
				swap(shu[i],shu[j]);
			}
		}
	}
	int ans=99999999;
	k=n-k;
	for(int i=1;i<=n;i++){
		ans=min(ans,dp(i,k-1));
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
4 1
1 2
2 4
3 1
5 3

out:
3

*/

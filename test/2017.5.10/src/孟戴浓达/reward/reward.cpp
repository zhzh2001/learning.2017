//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("reward.in");
ofstream fout("reward.out");
const string sdig="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
struct bigint{
	int base,len,dig[10005];
	bool neg;
	bigint();
	bigint(int x);
	void clear();
	void setbase(int b);
	bigint operator+(bigint b);
	bigint operator-(bigint b);
	bigint operator*(bigint b);
	bool operator<(const bigint& b);
};
istream& operator>>(istream& is,bigint& b);
ostream& operator<<(ostream& os,const bigint& b);
bigint::bigint(){
	base=10;
	len=1;
	neg=false;
	memset(dig,0,sizeof(dig));
}
bigint::bigint(int x){
	base=10;
	neg=(x<0);
	len=0;
	do{
		dig[++len]=x%10;
	}while(x/=10);
}
void bigint::clear(){
	while(len>1&&dig[len]==0)
		len--;
}
void bigint::setbase(int b){
	base=b;
}
bigint bigint::operator+(bigint b){
	bigint ret;
	if(neg&&!b.neg){
		ret=*this;
		ret.neg=false;
		ret=b-ret;
	}else{
		if(!neg&&b.neg){
			ret=b;
			b.neg=false;
			ret=ret-b;
		}else{
			ret.len=max(len,b.len);
			int overflow=0;
			for(int i=1;i<=ret.len;i++){
				ret.dig[i]=dig[i]+b.dig[i]+overflow;
				overflow=ret.dig[i]/base;
				ret.dig[i]%=base;
			}
			if(overflow)
				ret.dig[++ret.len]=overflow;
			ret.neg=neg;
		}
	}
	return ret;
}
bigint bigint::operator-(bigint b){
	bigint ret;
	if(b.neg){
		ret=b;
		ret.neg=false;
		ret=*this+ret;
	}else{
		if(neg){
			ret=b;
			ret.neg=true;
			ret=*this+ret;
		}else{
			if(*this<b){
				ret.neg=true;
				swap(*this,b);
			}
			ret.len=len;
			int unflow=0;
			for(int i=1;i<=ret.len;i++){
				ret.dig[i]=dig[i]-b.dig[i]-unflow;
				unflow=(ret.dig[i]<0);
				ret.dig[i]=(ret.dig[i]+base)%base;
			}
			ret.clear();
		}
	}
	return ret;
}
bigint bigint::operator*(bigint b){
	bigint ret;
	ret.len=len+b.len-1;
	for(int i=1;i<=len;i++){
		for(int j=1;j<=b.len;j++)
			ret.dig[i+j-1]+=dig[i]*b.dig[j];
	}
	for(int i=1;i<=ret.len;i++){
		ret.dig[i+1]+=(ret.dig[i]/base);
		ret.dig[i]%=base;
	}
	if(ret.dig[ret.len+1]){
		ret.len++;
	}
	ret.clear();
	ret.neg=neg^b.neg;
	return ret;
}
bool bigint::operator<(const bigint& b){
	if(len!=b.len){
		return len<b.len;
	}
	for(int i=len;i>=1;i--){
		if(dig[i]!=b.dig[i])
			return dig[i]<b.dig[i];
	}
	return false;
}
istream& operator>>(istream& is,bigint& b){
	string s;
	is>>s;
	b.len=s.length();
	if(s[0]=='-'){
		b.neg=true;
		b.len--;
		s=s.erase(0,1);
	}else{
		b.neg=false;
	}
	for(int i=0;i<b.len;i++){
		if(isdigit(s[i])){
			b.dig[b.len-i]=s[i]-'0';
		}else{
			b.dig[b.len-i]=tolower(s[i])-'a'+10;
		}
	}
	return is;
}
ostream& operator<<(ostream& os,const bigint& b){
	if(b.neg){
		os<<'-';
	}
	for(int i=b.len;i>=1;i--){
		os<<sdig[b.dig[i]];
	}
	return os;
}
long long n,ans,i;
int main(){
	//freopen("reward.in","r",stdin);
	//freopen("reward.out","w",stdout);
	fin>>n;
	if(n>10){
		i=48;
	}
	if(n>12){
		i=60;
	}
	if(n>15){
		i=144;
	}
	if(n>16){
		i=168;
	}
	if(n>18){
		i=180;
	}
	if(n>20){
		i=240;
	}
	if(n>24){
		i=360;
	}
	if(n>30){
		i=800;
	}
	while(1){
		i++;
		ans=0;
		for(long long j=1;j<=i;j++){
			if(i%j==0){
				ans++;
			}
		}
		if(ans==n){
			fout<<i<<endl;
			return 0;
		}
	}
	return 0;
}
/*

in:
4

out:
6

*/

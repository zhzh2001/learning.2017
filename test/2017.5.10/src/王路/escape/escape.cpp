#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

ifstream fin("escape.in");
ofstream fout("escape.out");

const int N = 25;
string str;
int mat[N][N];

const int di[] = { -1, 1, 0, 0 };
const int dj[] = { 0, 0, -1, 1 };

struct DLT {
	int dir[4];
} h, p;

struct Peo {
	int i, j;
} H, P;

int ans = 0x3f3f;
int n, m;
bool vis[N][N];

void dfs(int dep, Peo P, Peo H) {
	if (dep == 256) return;
	vis[P.i][P.j] = true;
	int t = abs(P.i - H.i) + abs(P.j - H.j);
	if (t == 0) {
		ans = min(ans, dep);
		return;
	}
	if (t == 1) {
		ans = min(ans, dep + 1);
		return;
	}
	for (int i = 0; i < 4; i++) {
		Peo P1 = P; P1.i = P.i + di[p.dir[i]], P1.j = P.j + dj[p.dir[i]];
		if (mat[P1.i][P1.j] == 1 && !vis[P1.i][P1.j] && P1.i >= 1 && P1.i <= n && P1.j >= 1 && P1.j <= m) {
			Peo H1 = H; H1.i = H.i + di[h.dir[i]], H1.j = H.j + dj[h.dir[i]];
			if (mat[H1.i][H1.j] == 1 && H1.i >= 1 && H1.i <= n && H1.j >= 1 && H1.j <= m) dfs(dep + 1, P1, H1);
			else if (mat[H1.i][H1.j] == 0  && H1.i >= 1 && H1.i <= n && H1.j >= 1 && H1.j <= m) dfs(dep + 1, P1, H);
			else continue;
		}
	}
}

int main() {
	fin >> n >> m;
	for (int i = 1; i <= n; i++) {
		fin >> str;
		for (int j = 0; j < int(str.length()); j++) {
			switch(str[j]) {
				case '#': mat[i][j + 1] = 0; break;
				case '.': mat[i][j + 1] = 1; break;
				case '!': mat[i][j + 1] = -1; break;
				case 'H': mat[i][j + 1] = 1, H.i = i, H.j = j + 1; break;
				case 'P': mat[i][j + 1] = 1, P.i = i, P.j = j + 1; break;
			}
		}
	}
	for (int i = 0; i < 4; i++) p.dir[i] = i;
	fin >> str;
	for (int i = 0; i < 4; i++) {
		switch(str[i]) {
			case 'N': h.dir[i] = 0; break;
			case 'S': h.dir[i] = 1; break;
			case 'W': h.dir[i] = 2; break;
			case 'E': h.dir[i] = 3; break;
		}
	}
	dfs(0, P, H);
	if (ans == 0x3f3f) {
		fout << "Impossible" << endl;
		cout << "Impossible" << endl;
		return 0;
	}
	fout << ans << endl;
	cout << ans << endl;
	return 0;
}
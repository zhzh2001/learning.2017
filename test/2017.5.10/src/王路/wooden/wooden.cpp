#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

ifstream fin("wooden.in");
ofstream fout("wooden.out");

const int N = 5005;
int f[N];

int maxL, maxW;

struct Data {
	int L, W;
} data[N];

int main() {
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++) fin >> data[i].L >> data[i].W;
	for (int i = 2; i <= n; i++)
		for (int j = 1; j <= i; j++)
			if (data[i].L < data[j].L && data[i].W < data[j].W) {
				f[i] = max(f[i], f[j] + 1);
			}
	int res = 0;
	for (int i = 1; i <= n; i++)
		res = max(res, f[i]);
	fout << res << endl;
	cout << res << endl;
	return 0;
}
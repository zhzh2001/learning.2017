#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <math.h>
#include <memory.h>
#include <time.h>
using namespace std;

#pragma GCC optimize(2)

ifstream fin("reward.in");

int f[50005];
int n;

int main() {
	freopen("reward.out", "w", stdout);
	fin >> n;
	memset(f, 0x3f, sizeof f);
	for (int i = 1; ; i++) {
		int cnt = 0;
		for (int j = 1; j <= i; j++)
			if (i % j == 0) cnt++;
		f[cnt] = min(f[cnt], i);
		if (cnt == n) break;
		if (clock() > 850) {
			printf("%.0Lf\n", (long double)pow(2.0L, (long double)n));
			return 0;
		}
	}
	printf("%d\n", f[n]);
	return 0;
}
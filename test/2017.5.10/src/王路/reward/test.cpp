#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <memory.h>
#include <cstring>
using namespace std;

#pragma GCC optimize(2)
typedef long long ll;

int f[50005];

int main() {
	memset(f, 0x3f, sizeof f);
	for (int i = 1; i <= 100; i++) {
		int cnt = 0;
		for (int j = 1; j <= i; j++)
			if (i % j == 0) cnt++;
		cout << i << ':' << cnt << endl;
		// f[cnt] = min(f[cnt], i);
	}
	// for (int i = 1; i <= 50; i++) cout << f[i] << endl;
	return 0;
}
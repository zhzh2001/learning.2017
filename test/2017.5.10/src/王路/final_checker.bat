@echo off
for /d %%s in (*) do (
echo %%s
cd %%s
g++ %%s.cpp
copy con %%s.in
a.exe
type %%s.out
pause
del a.exe
cd ..
)
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
using namespace std;

ifstream fin("book.in");
ofstream fout("book.out");

const int N = 105;
int L[N], W[N], Nxt[N], Pre[N];
int head, tail;

struct Data {
	int L, W;
	bool operator < (const Data & k) const {
		return L < k.L;
	}
} data[N];

void Delete(int pos) {
	if (pos == head) head = Nxt[head];
	else if (pos == tail) tail = Pre[tail];
	else {
		Nxt[Pre[pos]] = Nxt[pos];
		Pre[Nxt[pos]] = Pre[pos];
	}
}

int main() {
	int n, k, res = 0;
	fin >> n >> k;
	if (n == k) {
		fout << 0 << endl;
		return 0;
	}
	for (int i = 1; i <= n; i++) {
		fin >> data[i].L >> data[i].W;
		Nxt[i] = i + 1;
		Pre[i] = i - 1;
	}
	sort(data + 1, data + n + 1);
	head = 1, tail = n;
	while (k) {
		int tmp = abs(data[tail].W - data[Pre[tail]].W), pos = tail;
		if (abs(data[Nxt[head]].W - data[head].W) > tmp) {
			tmp = abs(data[Nxt[head]].W - data[head].W);
			pos = head;
		}
		for (int i = Nxt[head]; i != tail; i = Nxt[i]) {
			if (abs(data[i].W - data[Pre[i]].W) + abs(data[Nxt[i]].W - data[i].W) - abs(data[Nxt[i]].W - data[Pre[i]].W) > tmp) {
				tmp = abs(data[i].W - data[Pre[i]].W) + abs(data[Nxt[i]].W - data[i].W) - abs(data[Nxt[i]].W - data[Pre[i]].W);
				pos = i;
			}
		}
		Delete(pos);
		// cout << pos << endl;
		k--;
	}
	res = 0;
	for (int i = Nxt[head]; i <= tail; i = Nxt[i])
		res += abs(data[i].W - data[Pre[i]].W);
	fout << res << endl;
	cout << res << endl;
	return 0;
}
#include <bits/stdc++.h>
using namespace std;

const int N = 205;
int hash[N];

int main() {
	freopen("book.in", "w", stdout);
	int n, k;
	cin >> n >> k;
	cout << n << ' ' << k << endl;
	for (int i = 1; i <= n; i++) {
		int k = rand() % 200 + 1;
		while (hash[k]) k = rand() % 200 + 1;
		hash[k] = true;
		cout << k << ' ' << rand() % 200 + 1 << endl;
	}
	return 0;
}
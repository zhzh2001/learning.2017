#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=5005,inf=99999999;
struct node{
	int l,w,num;
}a[N];
int n,ans;
bool cmp1(node a,node b){ return a.l<b.l; }

int main(){
	freopen("wooden.in","r",stdin);
	freopen("wooden.out","w",stdout);
	n=read(); int p=0,q=inf;
	for(int i=1;i<=n;i++){
		a[i].l=read();
		a[i].w=read();
		a[i].num=i;
		p=max(p,a[i].w);
		q=min(q,a[i].w);
	}
	sort(a+1,a+n+1,cmp1); p++;
	for(int i=1;i<=n;i++){
//		printf("1\n");
		if(a[i].w>=p) continue;
//		printf("# %d \n",a[i].w);
		p=a[i].w; ans++; if(p==q) break;
	}
//	puts("");
	printf("%d\n",ans);
	return 0;
}

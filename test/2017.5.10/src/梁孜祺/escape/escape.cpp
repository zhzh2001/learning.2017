#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int step1[4][2]={{-1,0},{1,0},{0,-1},{0,1}};
const int N=21,STEP=256;
struct node{ int xp,yp,xh,yh,step; };
queue<node> q;
int map[N][N];
int step2[4][2];
int vis[N][N][N][N];  //xp,yp,xh,yh
int n,m,ans;

void bfs(){
	while(!q.empty()){
		node u=q.front(); q.pop();
		if(u.step==STEP) break;
		if(u.xp==u.xh&&u.yp==u.yh){
			ans=u.step;
			return;
		}
		for(int i=0;i<4;i++){
			node v;
			v.xp=u.xp+step1[i][0];
			v.yp=u.yp+step1[i][1];
			v.xh=u.xh+step2[i][0];
			v.yh=u.yh+step2[i][1];
			if(map[v.xh][v.yh]==2) continue;
			if(map[v.xp][v.yp]==1||map[v.xp][v.yp]==2) continue;
			if(map[v.xh][v.yh]==1) v.xh=u.xh,v.yh=u.yh;
			if(vis[v.xp][v.yp][v.xh][v.yh]) continue;
			if(v.xp==u.xh&&v.yp==u.yh&&v.xh==u.xp&&v.yh==u.yp){
				ans=u.step+1; if(ans==STEP) break;
				return;
			}
			vis[v.xp][v.yp][v.xh][v.yh]=true; v.step=u.step+1;
			q.push(v);
		}
	}
	printf("Impossible\n"); exit(0);
}

int main(){
	freopen("escape.in","r",stdin);
	freopen("escape.out","w",stdout);
	char ch[N];
	n=read(); m=read(); node st;
	for(int i=1;i<=n;i++){
		scanf("%s",ch+1);
		for(int j=1;j<=m;j++){
			if(ch[j]=='P') st.xp=i,st.yp=j,map[i][j]=0;
			else if(ch[j]=='H') st.xh=i,st.yh=j,map[i][j]=0;
			else if(ch[j]=='#') map[i][j]=1;
			else if(ch[j]=='.') map[i][j]=0;
			else map[i][j]=2;
		}
	}
	//debug
	/*  
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++)
			printf("%d ",map[i][j]);
		printf("\n");
	}
	*/	
	st.step=0;
	q.push(st); vis[st.xp][st.yp][st.xh][st.yh]=true;
	scanf("%s",ch);
	for(int i=0;i<4;i++){
		if(ch[i]=='N') step2[i][0]=-1,step2[i][1]=0;
		else if(ch[i]=='S') step2[i][0]=1,step2[i][1]=0;
		else if(ch[i]=='W') step2[i][0]=0,step2[i][1]=-1;
		else step2[i][0]=0,step2[i][1]=1;
	}
	//debug
	/*
	for(int i=0;i<4;i++)
		printf("%d %d\n",step2[i][0],step2[i][1]);
	*/
	bfs();
	printf("%d\n",ans);
	return 0;
}

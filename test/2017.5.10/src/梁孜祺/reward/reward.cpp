#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=50005;
bool flag[N],vis[N];
ll p[N],cnt,t;
ll a[N],num;
ll n;
struct node{
	int x,p;
	bool operator <(node b) const{
		return x>b.x;
	}
};

priority_queue<node> q;

ll kuaisu(ll n,ll x){
	ll a=x,ans=1;
	while(n){
		if(n&1) ans*=a;
		a*=a; n>>=1;
	}
	return ans;
}

void GetPhi(){
	cnt=0;
	memset(flag,true,sizeof flag);
	for(ll i=2;i<N;i++){
		if(flag[i]) p[++cnt]=i;
		for(ll j=1;j<=cnt;j++){
			if(i*p[j]>N) break;
			flag[i*p[j]]=false;
			if(i%p[j]==0) break;
		}
	}
}


int main(){
	freopen("reward.in","r",stdin);
	freopen("reward.out","w",stdout);
	GetPhi();
	n=read(); ll l=n;
	for(ll i=1;i<=cnt;){
		if(l==1) break;
		if(l%p[i]==0){
			a[++num]=p[i];
			l/=p[i];
			continue;
		}
		i++;
	}
	node v; v.x=2; v.p=2;
	t=1; q.push(v);
	for(ll i=num;i>=1;i--){
		node u=q.top();
		if(p[t+1]<u.x){
			v.x=p[++t]; v.p=p[t];
			q.push(v);
			u=q.top();
		}
		u.x=kuaisu(a[i],u.x);
		q.pop(); q.push(u);
	}
	ll ans=1;
	while(!q.empty()){
		node u=q.top(); q.pop();
		ans*=u.x/u.p;
	}
	printf("%lld\n",ans);
	return 0;
}

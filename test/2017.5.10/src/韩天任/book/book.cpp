#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=105;
struct node{
	int x,y,l,r;
}a[N];
int n,k,ans,front,tail;
bool vis[N];
bool cmp(node a,node b){ return a.y<b.y; }


int main(){
	freopen("book.in","r",stdin);
	freopen("book.out","w",stdout);
	n=read(); k=read();
	for(int i=1;i<=n;i++){
		a[i].y=read(); a[i].x=read();
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++){
		a[i].l=i-1; a[i].r=i+1;
	}
	for(int i=2;i<=n;i++) ans+=abs(a[i].x-a[i-1].x);
	front=1; tail=n;
	for(int t=1;t<=k&&front!=tail;t++){
		int mx,num;
		for(int i=front;i!=tail;i=a[i].r){
			if(i==front){
				mx=abs(a[i].x-a[a[i].r].x);
				num=i;
			}
			else{
				if(a[a[i].l].x>=a[i].x&&a[i].x<=a[a[i].r].x){
					int p=abs(a[a[i].l].x-a[i].x)+abs(a[i].x-a[a[i].r].x)-abs(a[a[i].l].x-a[a[i].r].x);
					if(p>mx){
						mx=p; num=i;
					}
				}
				if(a[a[i].l].x<=a[i].x&&a[i].x>=a[a[i].r].x){
					int p=abs(a[a[i].l].x-a[i].x)+abs(a[i].x-a[a[i].r].x)-abs(a[a[i].l].x-a[a[i].r].x);
					if(p>mx){
						mx=p; num=i;
					}
				}
			}
		}
		if(abs(a[tail].x-a[a[tail].l].x)>mx){
			mx=abs(a[tail].x-a[a[tail].l].x);
			num=tail;
		}
		if(num==front){
			front=a[front].r;
			a[front].l=0;
		}
		else if(num==tail){
			tail=a[tail].l;
			a[tail].r=n+1;
		}
		else{
			a[a[num].l].r=a[num].r;
			a[a[num].r].l=a[num].l;
		}
		ans-=mx;
	}
	printf("%d\n",ans);
	return 0;
}

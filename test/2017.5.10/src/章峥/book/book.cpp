#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("book.in");
ofstream fout("book.out");
const int N=105,INF=0x3f3f3f3f;
struct node
{
	int h,w;
	bool operator<(const node& rhs)const
	{
		return h<rhs.h;
	}
}a[N];
int f[N][N];
int main()
{
	int n,K;
	fin>>n>>K;
	memset(f,0x3f,sizeof(f));
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].h>>a[i].w;
		f[i][1]=0;
	}
	sort(a+1,a+n+1);
	K=n-K;
	for(int i=2;i<=n;i++)
		for(int j=2;j<=i&&j<=K;j++)
			for(int k=1;k<i;k++)
				f[i][j]=min(f[i][j],f[k][j-1]+abs(a[k].w-a[i].w));
	int ans=INF;
	for(int i=K;i<=n;i++)
		ans=min(ans,f[i][K]);
	fout<<ans<<endl;
	return 0;
}
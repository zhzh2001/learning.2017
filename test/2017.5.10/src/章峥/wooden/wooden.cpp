#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("wooden.in");
ofstream fout("wooden.out");
const int N=5005;
pair<int,int> a[N];
int f[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i].first>>a[i].second;
	sort(a+1,a+n+1);
	int ans=1;
	for(int i=1;i<=n;i++)
	{
		f[i]=1;
		for(int j=1;j<i;j++)
			if(a[j].second>a[i].second)
				f[i]=max(f[i],f[j]+1);
		ans=max(ans,f[i]);
	}
	fout<<ans<<endl;
	return 0;
}
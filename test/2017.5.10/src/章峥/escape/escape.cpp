#include<fstream>
#include<queue>
using namespace std;
ifstream fin("escape.in");
ofstream fout("escape.out");
const int N=25,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
char mat[N][N];
int rd[4];
bool vis[N][N][N][N];
struct node
{
	int hx,hy,px,py,step;
	node(int hx,int hy,int px,int py,int step):hx(hx),hy(hy),px(px),py(py),step(step){}
};
int main()
{
	int n,m;
	fin>>n>>m;
	int hx,hy,px,py;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>mat[i][j];
			if(mat[i][j]=='H')
				hx=i,hy=j;
			if(mat[i][j]=='P')
				px=i,py=j;
		}
	for(int i=0;i<4;i++)
	{
		char c;
		fin>>c;
		switch(c)
		{
			case 'N':rd[i]=0;break;
			case 'S':rd[i]=1;break;
			case 'W':rd[i]=2;break;
			case 'E':rd[i]=3;break;
		}
	}
	queue<node> Q;
	Q.push(node(hx,hy,px,py,0));
	vis[hx][hy][px][py]=true;
	while(!Q.empty())
	{
		node k=Q.front();Q.pop();
		if(k.step<255)
			for(int i=0;i<4;i++)
			{
				int npx=k.px+dx[i],npy=k.py+dy[i],nhx=k.hx+dx[rd[i]],nhy=k.hy+dy[rd[i]];
				if(mat[npx][npy]!='#'&&mat[npx][npy]!='!'&&mat[nhx][nhy]!='!')
				{
					if(mat[nhx][nhy]=='#')
						nhx=k.hx,nhy=k.hy;
					if((nhx==npx&&nhy==npy)||(nhx==k.px&&nhy==k.py&&npx==k.hx&&npy==k.hy))
					{
						fout<<k.step+1<<endl;
						return 0;
					}
					if(!vis[nhx][nhy][npx][npy])
					{
						Q.push(node(nhx,nhy,npx,npy,k.step+1));
						vis[nhx][nhy][npx][npy]=true;
					}
				}
			}
	}
	fout<<"Impossible\n";
	return 0;
}
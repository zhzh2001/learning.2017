#include<cstdio>
#include<cstring>
#include<cmath>
const int N=20,mo=1000000;
int n,m,i,j,k,t,l,a[N],p[N],g[N],ans[N],f[4000];
double mi,lg[N];
bool ff[60];
void ss(){
	 for (i=2;i<60;i++) if (!ff[i]){
		 p[++t]=i;for (j=2;j*i<60;j++) ff[j*i]=1; 	
	 }
}
void dfs(int x,double k,int y){
	 if (k>=mi) return;
	 if (x>t){
	 	for (int i=1;i<=m;i++) ans[i]=g[i];
		mi=k;return;
	 }
	 int ii=1;if (a[x]==a[x-1]) ii=y;
	 for (int i=ii;i<=m;i++){
	 	 double kk=k-lg[i]*(g[i]-1);
		 g[i]*=a[x];kk+=lg[i]*(g[i]-1);
		 dfs(x+1,kk,i);
		 g[i]/=a[x];
	 }
}
void cf(int k){
	 for (int i=1;i<=l;i++) f[i]*=k;
	 for (int i=1;i<=l;i++){f[i+1]+=f[i]/mo;f[i]%=mo;}
	 if (f[l+1]>0) l++;
}
int main(){
	freopen("reward.in","r",stdin);
	freopen("reward.out","w",stdout);
	scanf("%d",&n);
	ss();for (i=1;i<=t;i++) lg[i]=log(p[i]);
	t=0;int nn=n;
	for (i=2;i*i<=nn;i++)
		while (nn%i==0){nn/=i;a[++t]=i;}
	if (nn>1) a[++t]=nn;
	mi=log(2);mi=mi*(n-1);m=log(n-1)/log(2)+1;
	for (i=1;i<=m;i++) g[i]=1;ans[1]=n;
	dfs(1,0,0);f[1]=1;l=1;
	for (i=1;i<=m;i++){
		for (j=1;j<ans[i]-1;j+=2) cf(p[i]*p[i]);
		if (ans[i]%2==0&&ans[i]) cf(p[i]);	
	}
	printf("%d",f[l]);
	for (i=l-1;i>=1;i--){
		for (j=f[i],k=0;j;j/=10,k++);
		for (j=k+1;j<=6;j++) printf("0");
		printf("%d",f[i]);
	}
}

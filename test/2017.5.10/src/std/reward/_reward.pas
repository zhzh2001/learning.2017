uses math;
const
  bs=10000000;
  z:array[1..17]of byte=
  (2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59);
type
  arr=array[0..17]of longint;
  big=array[0..2500]of longint;
var
  i,j,k:longint; c:big;
  a,m:arr; t:extended;
PROCEDURE qs(var a:arr; p,q:word);
  VAR
    i,j,temp,r:longint;
  BEGIN
    i:=p; j:=q;
    r:=a[(p+q)shr 1];
    REPEAT
      WHILE a[i]>r DO inc(i);
      WHILE a[j]<r DO dec(j);
      IF i<=j
        THEN BEGIN
               temp:=a[i]; a[i]:=a[j]; a[j]:=temp;
               inc(i); dec(j)
             END;
    UNTIL i>j;
    IF j>p THEN qs(a,p,j);
    IF i<q THEN qs(a,i,q)
  END;
function prime(n:longint):boolean;
  begin
    for i:=2 to trunc(sqrt(n)) do
      if n mod i=0 then exit(false);
    exit(true)
  end;
procedure check(m:arr; n:longint);
  var
    i,j:longint; r:extended;
  begin
    qs(m,1,n); t:=1;
    for i:=1 to min(n,a[0]) do
      begin
        if a[i]=m[i] then continue;
        if ln(z[i])*abs(m[i]-a[i])+ln(t)>11000 then
          begin t:=0; a:=m; a[0]:=n; exit end;
        t:=t*power(z[i],m[i]-a[i])
      end;
    if n>a[0] then
      for i:=a[0]+1 to n do
        t:=t*power(z[i],m[i]-1);
    if a[0]>n then
      for i:=n+1 to a[0] do
        begin
          r:=power(z[i],a[i]-1);
          if r<>0 then t:=t/r
        end;
    if t<1 then
      begin a:=m; a[0]:=n end
  end;
procedure try(n,x:longint);
  var
    i:longint;
  begin
    for i:=m[n-2] to trunc(sqrt(x)) do
      if x mod i=0 then
        begin
          m[n-1]:=i;
          m[n]:=x div i;
          check(m,n);
          try(n+1,m[n])
        end
  end;
procedure mul(var a:big;b:longint);
  var i,j:longint; c:big;
  begin
    c[1]:=0; c[a[0]+1]:=0;
    for i:=1 to a[0] do
      begin
        inc(c[i],a[i]*b);
        c[i+1]:=c[i]div bs;
        c[i]:=c[i]mod bs;
      end;
    inc(i);
    while c[i]>0 do
      begin
        c[i+1]:=c[i]div bs;
        c[i]:=c[i]mod bs;
        inc(i);
      end;
    c[0]:=i-1;
    for i:=0 to c[0] do
      a[i]:=c[i];
  end;
procedure work;
  begin
    read(k);
    c[0]:=1; c[1]:=1;
    if prime(k) then
      begin
        j:=128; dec(k);
        for i:=1 to k div 7 do
          mul(c,j);
        for i:=1 to k mod 7 do
          mul(c,2);
        exit
      end;
    a[0]:=2; a[2]:=i; m[0]:=2;
    a[1]:=k div i; try(2,k);
    for i:=1 to a[0] do
      for j:=2 to a[i] do mul(c,z[i])
  end;
begin
  assign(input,'reward.in'); reset(input);
  assign(output,'reward.out'); rewrite(output);
  work; write(c[c[0]]);
  for i:=c[0]-1 downto 1 do
    begin
      if c[i]<1000000 then write(0);
      if c[i]<100000 then write(0);
      if c[i]<10000 then write(0);
      if c[i]<1000 then write(0);
      if c[i]<100 then write(0);
      if c[i]<10 then write(0);
      write(c[i])
    end
end.

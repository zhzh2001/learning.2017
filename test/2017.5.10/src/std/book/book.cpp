#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=102;
struct arr{int x,y;}a[N];
int n,m,i,j,k,f[N][N];
bool cmp(arr xx,arr yy){return xx.x<yy.x||xx.x==yy.x&&xx.y<yy.y;}
int abs(int x){if (x<0) return -x;return x;}
int main(){
	freopen("book.in","r",stdin);
	freopen("book.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;i++) scanf("%d%d",&a[i].x,&a[i].y);
	sort(a+1,a+n+1,cmp);
	memset(f,1,sizeof(f));f[0][0]=0;
	for (i=1;i<=n+1;i++)
		for (j=0;j<i;j++){
			int t=abs(a[i].y-a[j].y);
			if (j==0||i==n+1) t=0;
			for (k=i-j-1;k<=min(i-1,m);k++) 
				f[i][k]=min(f[i][k],f[j][k-(i-j-1)]+t);
		}
	printf("%d",f[n+1][m]);
}

#include<cstdio>
#include<algorithm>
using namespace std;
const int N=5005;
int i,j,n,t,ans,f[N];
struct B{
	int x,y;
	bool operator<(const B&a)const{
		return x<a.x||x==a.x&&y<a.y;
	}
}a[N];
int main()
{
	freopen("wooden.in","r",stdin);
	freopen("wooden.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)
		scanf("%d%d",&a[i].x,&a[i].y);
	sort(a+1,a+n+1);
	for(i=1;i<=n;i++)if(!f[i])
		for(ans++,f[i]=1,t=a[i].y,j=i+1;j<=n;j++)
			if(!f[j]&&a[j].y>=t)t=a[j].y,f[j]=1;
	printf("%d",ans);	
}

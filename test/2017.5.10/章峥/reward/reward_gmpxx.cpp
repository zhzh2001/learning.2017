#include<fstream>
#include<algorithm>
#include<cmath>
#include<vector>
#include<gmpxx.h>
using namespace std;
ifstream fin("reward.in");
ofstream fout("reward.out");
const int pr[]={0,2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59};
struct pint
{
	int cnt[20];
	double to_log10()const
	{
		double ans=0.0;
		for(int i=1;i<=16;i++)
			ans+=log10(pr[i])*cnt[i];
		return ans;
	}
	bool operator<(const pint& rhs)const
	{
		return to_log10()<rhs.to_log10();
	}
}ans,now;
struct node
{
	int p,cnt;
	node(int p,int cnt):p(p),cnt(cnt){}
};
vector<node> pn;
void dfs(int k,int tot,int pred);
void dfsnow(int k,int sum,int _k,int _tot,int pred)
{
	if(k==pn.size())
	{
		now.cnt[_k]=sum-1;
		dfs(_k+1,_tot,sum);
		return;
	}
	int pd=1;
	for(int i=0;i<=pn[k].cnt;i++)
	{
		pn[k].cnt-=i;
		dfsnow(k+1,sum*pd,_k,_tot,pred);
		pn[k].cnt+=i;
		if((pd*=pn[k].p)>pred)
			break;
	}
}
void dfs(int k,int tot,int pred)
{
	if(k==tot+1)
	{
		for(int i=0;i<pn.size();i++)
			if(pn[i].cnt)
				return;
		ans=min(ans,now);
		return;
	}
	dfsnow(0,1,k,tot,pred);
}
ostream& operator<<(ostream& os,const pint& p)
{
	mpz_class ans=1;
	for(int i=1;i<=16;i++)
	{
		mpz_class now;
		mpz_ui_pow_ui(now.get_mpz_t(),pr[i],p.cnt[i]);
		ans*=now;
	}
	return os<<ans;
}
int main()
{
	int n;
	fin>>n;
	int on=n,tot=0;
	for(int i=2;i*i<=n;i++)
		if(n%i==0)
		{
			node now(i,0);
			while(n%i==0)
			{
				now.cnt++;
				n/=i;
			}
			tot+=now.cnt;
			pn.push_back(now);
		}
	if(n>1)
		pn.push_back(node(n,1)),tot++;
	for(int i=1;i<=16;i++)
		ans.cnt[i]=on-1;
	for(int i=1;i<=tot;i++)
		dfs(1,i,on);
	fout<<ans<<endl;
	return 0;
}
#include<cstdio>
using namespace std;
struct s{int l;int w;} ;
s a[5001];
int t,n,n1,n2,k,ans,visited[5001];
void qsort(int x,int y)
{
  int h=x,r=y;
  s tmp=a[(h+r)/2];
  while(h<r)
   {
    while ((a[h].w<tmp.w)||((a[h].w==tmp.w)&&(a[h].l<tmp.l))) h++;
    while ((a[r].w>tmp.w)||((a[r].w==tmp.w)&&(a[r].l>tmp.l))) r--;
    if(h<=r)
     {s temp=a[h];
        a[h]=a[r];
        a[r]=temp;
        h++;r--;
     }
   }
  if(h<y) qsort(h,y);
  if(r>x) qsort(x,r); 
}

int main()
{
   freopen("wooden.in","r",stdin);
   freopen("wooden.out","w",stdout);  
   scanf("%d",&n);
   for(int i=0;i<=5000;i++)
     visited[i]=0;
   for(int j=1;j<=n;j++)
     scanf("%d%d",&a[j].l,&a[j].w);
   qsort(1,n);
   ans=0;
   while (1)
    {
     k=1;    
     while (k<=n&&visited[k]==1) k++;
     if (k>n) break;
     ans++;
     int last=k;
     int cur=k;
     while (cur<=n)
       if((a[cur].l>=a[last].l)&&(visited[cur]==0)) {last=cur;visited[cur]=1;cur++;}
       else  cur++;
    }
   printf("%d\n",ans);
   return 0;
}

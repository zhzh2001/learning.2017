#include<bits/stdc++.h>
using namespace std;
const int N=401,M=1000001;
int n,i,j,t,a[N*N],d[N],g[M];
 int gcd(int a,int b)
{
	if (a%b==0) return b; 
	else return gcd(b,a%b);
}
int main(){
	freopen("countdown.in","r",stdin);
	freopen("countdown.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n*n;i++) scanf("%d",&a[i]);	
	sort(a+1,a+n*n+1);
	for (i=n*n;i>=1;i--) 
		if (g[a[i]]) g[a[i]]--;
		else
		{
			d[++t]=a[i];
			for (j=1;j<t;j++) g[gcd(d[j],a[i])]+=2;	
		}
	sort(d+1,d+n+1);
	for (i=1;i<=n;i++) printf("%d ",d[i]); 
}

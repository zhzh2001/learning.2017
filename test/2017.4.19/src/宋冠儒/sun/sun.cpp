#include<stdio.h>
#include<algorithm>
using namespace std;
#define ll long long
int i,j,k,n1,n2,n,last;
int tree[400005];
ll ans;
struct ff{
	int num,x;
}a[100005];
 bool cmp(ff xx,ff yy)
{
    return xx.num>yy.num;
}
 int build(int l,int r,int node)
{
    if(l==r){tree[node]=1;return 0;}
    int mid=(l+r)>>1;
    build(l,mid,node<<1);
    build(mid+1,r,node<<1|1);
    tree[node]=tree[node<<1]+tree[node<<1|1];
    return 0;
}
 int ask(int l,int r,int a,int b,int node)
{
    if(a>b) return 0;
    if(l==a && r==b) return tree[node];
    int mid=(l+r)>>1;
    if(b<=mid) return ask(l,mid,a,b,node<<1);
    if(a>mid) return ask(mid+1,r,a,b,node<<1|1);
    return (ask(l,mid,a,mid,node<<1)+ask(mid+1,r,mid+1,b,node<<1|1));
}
 int del(int l,int r,int k,int node)
{
    if(l==r){tree[node]=0;return 0;}
    int mid=(l+r)>>1;
    if(k<=mid) del(l,mid,k,node<<1);
        else del(mid+1,r,k,node<<1|1);
    tree[node]=tree[node<<1]+tree[node<<1|1];
    return 0;
}
 int main()
{
    freopen("sun.in","r",stdin);
    freopen("sun.out","w",stdout);
    scanf("%d %d",&n1,&n2);
    for(i=1;i<=n1;i++)
        scanf("%d",&a[n1-i+1]);
    for(i=1;i<=n2;i++)
        scanf("%d",&a[n1+i]);
    n=n1+n2;
    for(i=1;i<=n;i++)
        a[i].x=i;
    sort(a+1,a+n+1,cmp);
    last=n1,ans=0;
    build(1,n,1);
    for(i=1;i<=n;i++)
    {
        k=a[i].x;
        if(k>last)
       	{
            ans+=(ll)ask(1,n,last+1,k-1,1);
            last=k-1;
        }
        else
        {
            ans+=(ll)ask(1,n,k+1,last,1);
            last=k;
        }
        del(1,n,k,1);
    }
    printf("%I64d\n",ans);
    return 0;
}

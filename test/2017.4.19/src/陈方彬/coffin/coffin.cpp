#include <iostream>
#include <string>
#include <cstdio>
#include<algorithm>
#include<cmath>
#define Ll long long 
using namespace std;
Ll m,x,y,S,ans;
bool check(Ll num){
	Ll x=num,sum=0;
	while(x){
		sum+=(x%10)*(x%10);
		x/=10;
	}
	if(num==m*sum)return 1;
	return 0;
}
int main()
{
	freopen("coffin.in","r",stdin);
	freopen("coffin.out","w",stdout);
	scanf("%I64d%I64d%I64d",&m,&x,&y);
	S=x;
	for(;S%m;S++);
	for(Ll k=S;k<=y;k+=m)
		if(check(k))ans++;
	printf("%I64d",ans);
}

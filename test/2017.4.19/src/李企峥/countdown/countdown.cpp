/*#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,a[1000100],mx,x;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("countdown.in","r",stdin);
	freopen("countdown.out","w",stdout);
	n=read();mx=0;
	For(i,1,n*n)x=read(),a[x]=1-a[x],mx=max(mx,x);
	For(i,0,mx)if(a[i]==1)cout<<i<<" ";
	return 0;
}*/
#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,a[1000100],mx,x,m,b[1000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll gcd(ll n,ll m)
{
	if (m==0) return n;
	if (n%2==0&&m%2==0) return 2*gcd(max(n/2,m/2),min(n/2,m/2));
	if (n%2==0&&m%2!=0) return gcd(max(n/2,m),min(n/2,m));
	if (n%2!=0&&m%2==0) return gcd(max(n,m/2),min(n,m/2));
	if (n%2!=0&&m%2!=0) return gcd(abs(m-n),min(n,m));
}
int main()
{
	freopen("countdown.in","r",stdin);
	freopen("countdown.out","w",stdout);
	n=read();mx=0;
	For(i,1,n*n)x=read(),a[x]++,mx=max(mx,x);
	m=0;
	For(i,1,mx)if(a[i]%2==1)m++,b[m]=i;
	if(m==n)
	{
		For(i,1,m)cout<<b[i]<<" ";
		return 0;
	}
	For(i,1,m)For(j,1,m)a[gcd(b[i],b[j])]--;
	while(m<n)
	{
		while(a[mx]==0)mx--;
		m++;
		b[m]=mx;
		For(i,1,m-1)a[gcd(b[i],b[m])]-=2;
		a[b[m]]--;
	}
	sort(b+1,b+n+1);
	For(i,1,n)cout<<b[i]<<" ";
	return 0;
}


//#include<iostream>
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("countdown.in");
ofstream fout("countdown.out");
int n,maxnum;
int tong[1000003],ans[403];
int gcd(int a,int b){
	if(b==0) return a;
	else     return gcd(b,a%b);
}
int main(){
	fin>>n;
	for(int i=1;i<=n*n;i++){
		int num;
		fin>>num;
		tong[num]++;
		if(maxnum<num)maxnum=num;
	}
	int x=n;
	for(int i=maxnum;i>=0;i--){
		while(tong[i]>0){
			ans[++ans[0]]=i;
			x--;
			if(x==0) break;
			for(int i=1;i<ans[0];i++) tong[gcd(ans[i],ans[ans[0]])]-=2;
			tong[ans[ans[0]]]--;
		}
	}
	sort(ans+1,ans+n+1);
	for(int i=1;i<n;i++)fout<<ans[i]<<" ";
	fout<<ans[n];
	return 0;
}
/*

in:
4
2 1 2 3 4 3 2 6 1 1 2 2 1 2 3 2

out:
2 3 4 6

*/

//#include<iostream>
#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("coffin.in");
ofstream fout("coffin.out");
string sa,sb;
long long numa,numb,lena,lenb,ans,k;
long long gujia[23];
inline long long sqr(long long x){return x*x;}
inline void solve(){
	long long zong=(numa/k)*k;
	while(zong-k>=numa) zong-=k;
	while(zong+k<numa)  zong+=k;
	while(zong<=numb){
		int x=0,zongx=zong;
		while(zongx!=0){
			x+=sqr(zongx%10);
			zongx/=10;
		}
		if(x==zong/k) ans++;
		if(gujia[lenb]*k<zong) break;
		zong+=k;
	}
}
inline void init(){
	fin>>k>>sa>>sb;
	lena=sa.length();
	for(int i=0;i<=lena-1;i++) numa=numa*10+sa[i]-'0';
	lenb=sb.length();
	for(int i=0;i<=lenb-1;i++) numb=numb*10+sb[i]-'0';
	for(int i=0;i<=21;i++) gujia[i]=i*81;
}
int main(){
	init();
	solve();
	fout<<ans<<endl;
	return 0;
}
/*

in:
51 5000 10000

out:
3

*/

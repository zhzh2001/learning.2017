#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
using namespace std;

const int N = 100005;
ifstream fin("coffin.in");
ofstream fout("coffin.out");

typedef long long ll;

ll k, a, b;
ll f[20][10];
ll g[10];

inline ll calc(int x) {
	ll res = 0;
	while (x) {
		res += (x % 10) * (x % 10);
		x /= 10;
	}
	return res;
}
int ans = 0;
void dfs(ll x, ll val, ll n, ll t) {
	if (val > n) return;
	if (val == t) ans++;
	for (int i = 0; i < 10; i++)
		dfs(x + 1, val + g[i], n, t * 10 + i);
}

int main() {
	fin >> k >> a >> b;
	if (a <= 1e5 && b <= 1e5) {
		if (a > b) swap(a, b);
		int ans = 0;
		for (int i = a; i <= b; i++)
			if (k * calc(i) == i) ans++;
		fout << ans << endl;
		cout << ans << endl;
		return 0;
	}
	if (a <= 1e8 && b <= 1e8) {
		for (int i = 0; i < 10; i++)
			g[i] = (ll)k * i * i;
		if (a > b) swap(a, b);
		dfs(0, 0, a - 1, 0);
		ans = -ans;
		dfs(0, 0, b, 0);
		fout << ans << endl;
		cout << ans << endl;
		return 0;
	}
	return 0;
}
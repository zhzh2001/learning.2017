#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
using namespace std;

const int N = 100005;
ifstream fin("sun.in");
ofstream fout("sun.out");

int n1, n2;
int c[2][N];

int cnt = 0;
struct kkk {
	int loc1, loc2, val;
	bool operator < (const kkk & x) const {
		return val > x.val;
	}
} data[N * 2];

deque<int> q1; int cnt1;
deque<int> q2; int cnt2;

void debug() {
	// cout << q1.size() << ' ' << q2.size() << endl;
	cout << "q1:"; for (int i = 0; i < q1.size(); i++) cout << q1[i] << ' '; cout << endl;
	cout << "q2:"; for (int i = 0; i < q2.size(); i++) cout << q2[i] << ' '; cout << endl;
}

int main() {
	fin >> n1 >> n2;
	for (int i = 1; i <= n1; i++) {
		fin >> c[0][i];
		q1.push_front(c[0][i]); 
		data[++cnt].val = c[0][i];
		data[cnt].loc1 = 0;
		data[cnt].loc2 = cnt1;
	}
	for (int i = 1; i <= n2; i++) {
		fin >> c[1][i];
		q2.push_front(c[1][i]); 
		data[++cnt].val = c[1][i];
		data[cnt].loc1 = 1;
		data[cnt].loc2 = cnt2;
	}
	sort(data + 1, data + cnt + 1);
	// debug();
	int now = 0, ans = 0;
	while (!q1.empty() || !q2.empty()) {
		int max_val = data[++now].val;
		int loc1 = data[now].loc1;
		if (loc1 == 0) {
			// cout << q1.back() << endl;
			if (max_val == q1.back()) q1.pop_back();
			else {
				// cout << ans;
				while (max_val != q1.back()) {
					int tmp = q1.back();
					q1.pop_back();
					q2.push_back(tmp);
					int l = now, r = cnt;
					while (l <= r) {
						int mid = (l + r) >> 1;
						if (data[mid].val == tmp) { l = r = mid; break; }
						else if (data[mid].val > tmp) l = mid + 1;
						else r = mid - 1;
					}
					data[l].loc1 ^= 1;
					// cout << data[l].val << ' ' << tmp << endl;
					ans++;
					// debug();
				}
				q1.pop_back();
				// cout << ' ' << ans << endl;
				
			}
		} else {
			// cout << q2.back() << endl;
			if (max_val == q2.back()) q2.pop_back();
			else {
				// cout << ans;
				while (max_val != q2.back()) {
					int tmp = q2.back();
					q2.pop_back();
					q1.push_back(tmp);
					int l = now, r = cnt;
					while (l <= r) {
						int mid = (l + r) >> 1;
						if (data[mid].val == tmp) { l = r = mid; break; }
						else if (data[mid].val > tmp) l = mid + 1;
						else r = mid - 1;
					}
					data[l].loc1 ^= 1;
					ans++;
					// debug();
				}
				// cout << ' ' << ans << endl;
				q2.pop_back();
			}
		}
	}
	fout << ans << endl;
	cout << ans << endl;
}
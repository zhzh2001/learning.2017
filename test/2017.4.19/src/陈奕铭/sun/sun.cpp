#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define lowbit(x) (x&(-x))
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=100005;
int q[N],top,ans,n;
int d[N],c[N],p[N];

void updata(int x){
	for( ;x<=n;x+=lowbit(x)) c[x]++;
}

int query(int x){
	int num=0;
	for( ;x;x-=lowbit(x)) num+=c[x];
	return num;
}

int erfen(int x){
	int l=1,r=n,mid;
	while(l<=r){
		mid=(l+r)>>1;
		if(d[mid]==x) return mid;
		else if(d[mid]<x) l=mid+1;
		else r=mid-1;
	}
}

int main(){
	freopen("sun.in","r",stdin);
	freopen("sun.out","w",stdout);
	int a=read(),b=read();
	n=a+b; top=a;
	for(int i=a;i;i--) q[i]=d[i]=read();
	for(int i=a+1;i<=n;i++) q[i]=d[i]=read();
	sort(d+1,d+n+1); 
//	puts("1");
	for(int i=1;i<=n;i++){
		int l=erfen(q[i]); p[l]=i;
	}
//	puts("2");
	for(int i=n;i;i--){
		int l=p[i];
//		printf("# %d ",l);
		if(l<=top){
			a=query(l); b=query(top);
//			printf("%d %d %d ",a,b,top);
			ans+=top-l-b+a; top=l; updata(l);
		}
		else{
			a=query(top); b=query(l-1);
//			printf("%d %d %d ",a,b,top);
			ans+=l-top-1-b+a; top=l; updata(l);
		}
//		printf("%d\n",ans);
	}
	printf("%d\n",ans);
	return 0;
}

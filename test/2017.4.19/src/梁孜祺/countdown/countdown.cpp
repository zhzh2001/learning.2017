#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=160005,M=410;
int a[N],n;
int b[M],sum;

int main(){
	freopen("countdown.in","r",stdin);
	freopen("countdown.out","w",stdout);
	n=read();int tot=n*n;
	for(int i=1;i<=tot;i++) a[i]=read();
	sort(a+1,a+tot+1);
	int num=1;
	for(int i=2;i<=tot;i++){
		if(a[i]!=a[i-1]){
			if(num%2==1) printf("%d ",a[i-1]);
			num=1;
		}
		else num++;
	}
	printf("%d\n",a[tot]);
	return 0;
}

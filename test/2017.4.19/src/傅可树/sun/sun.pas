var mm,tot,i,s,j,num1,num2,n1,n2:longint;
    a:array[1..2,0..100000]of longint;
    top:array[1..2]of longint;
    flag:boolean;
    b:array[0..100000]of longint;
procedure swap(var x,y:longint);
var z:longint;
begin
  z:=x; x:=y; y:=z;
end;
function find(x:longint):longint;
var l,r,mid:longint;
begin
  l:=1; r:=tot;
  while l<=r do
    begin
    mid:=(l+r) div 2;
    if b[mid]=x then exit(mid);
    if b[mid]>x then r:=mid-1 else l:=mid+1;
    end;
end;
procedure sort(l,r:longint);
var i,j,x:longint;
begin
  i:=l;
  j:=r;
  x:=b[(l+r) div 2];
  while i<=j do
     begin
     while b[i]<x do inc(i);
     while x<b[j] do dec(j);
     if i<=j then
      	begin
   	swap(b[i],b[j]);
    	inc(i); dec(j);
        end;
     end;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;
begin
  assign(input,'sun.in');
  assign(output,'sun.out');
  reset(input);
  rewrite(output);
  readln(n1,n2); top[1]:=n1; top[2]:=n2;
  for i:=n1 downto 1 do begin read(a[1,i]); inc(tot); b[tot]:=a[1,i]; end;
  for i:=n2 downto 1 do begin read(a[2,i]); inc(tot); b[tot]:=a[2,i]; end;
  sort(1,tot);
  for i:=1 to n1 do a[1,i]:=find(a[1,i]);
  for i:=1 to n2 do a[2,i]:=find(a[2,i]);
  for i:=tot downto 1 do
    begin
    flag:=false;
    for j:=1 to top[1] do if a[1,j]=i then begin num1:=1; num2:=j; break; flag:=true; end;
    if not flag then
      for j:=1 to top[2] do if a[2,j]=i then begin num1:=2; num2:=j; break; end;
    s:=s+top[num1]-num2; mm:=num1 xor 3;
    for j:=num2+1 to top[num1] do begin inc(top[mm]); a[mm,top[mm]]:=a[num1,j]; end;
    top[num1]:=num2-1;
    end;
  writeln(s);
  close(input);
  close(output);
end.

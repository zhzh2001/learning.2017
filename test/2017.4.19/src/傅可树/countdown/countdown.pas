var q:array[0..1000005]of longint;
    tot,t,maxn,x,n,i:longint;
    ans:array[0..405]of longint;
function gcd(x,y:longint):longint;
begin
  if y=0 then exit(x);
  exit(gcd(y,x mod y));
end;
function max(x,y:longint):longint;
begin
  if x>y then exit(x) else exit(y);
end;
begin
  assign(input,'countdown.in');
  assign(output,'countdown.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n*n do begin read(x); inc(q[x]); maxn:=max(maxn,x); end;
  inc(tot); ans[tot]:=maxn; dec(q[maxn]);
  for i:=maxn downto 1 do if q[i]<>0 then break;
  t:=i; inc(tot); ans[tot]:=t; dec(q[t]); dec(q[gcd(t,maxn)],2); maxn:=t;
  while tot<n do
    begin
    for i:=maxn downto 1 do if q[i]<>0 then break;
    t:=i; inc(tot); ans[tot]:=i; dec(q[i]); maxn:=i;
    for i:=1 to tot-1 do dec(q[gcd(t,ans[i])],2);
    end;
  for i:=n downto 1 do write(ans[i],' ');
  close(input);
  close(output);
end.

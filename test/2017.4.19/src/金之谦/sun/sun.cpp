#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll lowbit(ll x){return x&-x;}
ll n,m,a[100001],f[100001]={0},p[100001];
inline ll erfen(ll x){
	ll l=1,r=n+m;
	while(l<=r){
		ll mid=l+r>>1;
		if(p[mid]==x)return mid;
		if(p[mid]>x)r=mid-1;
		else l=mid+1;
	}
}
inline void add(ll x){for(ll i=x;i<=n+m;i+=lowbit(i))f[i]++;}
inline ll sum(ll x){ll ans=0;for(ll i=x;i>0;i-=lowbit(i))ans+=f[i];return ans;}
int main()
{
	freopen("sun.in","r",stdin);
	freopen("sun.out","w",stdout);
	scanf("%I64d%I64d",&n,&m);
	for(ll i=1;i<=n;i++)scanf("%I64d",&a[n-i+1]);
	for(ll i=1;i<=m;i++)scanf("%I64d",&a[n+i]);
	for(ll i=1;i<=n+m;i++)p[i]=a[i];
	sort(p+1,p+n+m+1);
	for(ll i=1;i<=n+m;i++)a[i]=erfen(a[i]);
	memset(p,0,sizeof p);
	for(ll i=1;i<=n+m;i++)p[a[i]]=i;
	ll w=n,ans=0;
	for(ll i=n+m;i;i--){
		if(p[i]==w||p[i]==w+1){
			add(p[i]);continue;
		}
		if(p[i]<w)ans+=w-p[i]-sum(w)+sum(p[i]),w=p[i];
		else ans+=p[i]-1-w-sum(p[i])+sum(w),w=p[i]-1;
		add(p[i]);
	}
	printf("%I64d",ans);
}

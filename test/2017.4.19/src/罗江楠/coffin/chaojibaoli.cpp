#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll a, b, k, ans;
ll sqr(ll x){return x*x;}
inline ll get(ll x){
	ll ans=0;
	while(x)ans+=sqr(x%10),x/=10;
	return ans;
}
int main(){
	freopen("coffin.in", "r", stdin);
	scanf("%lld%lld%lld", &k, &a, &b);
	for(ll i = a; i <= b; i++)
		if(get(i)*k==i)++ans;
	printf("%d\n", ans);
}
#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll a, b, k;
ll sqr(ll x){return x*x;}
inline ll get(ll x){
	ll ans=0;
	while(x)ans+=sqr(x%10),x/=10;
	return ans;
}
int main(){
	freopen("coffin.in", "r", stdin);
	freopen("coffin.out", "w", stdout);
	scanf("%lld%lld%lld", &k, &a, &b);
	ll tmp = k, bin=1; while(tmp)tmp/=10,bin*=2;
	ll ans = 0, szb = min(k*81*bin, b);
	for(ll i = max((a%k)?(a/k*k+k):(a), k); i <= szb; i+=k)
		if(get(i)*k==i)++ans;
	printf("%d\n", ans+(a==0));
}
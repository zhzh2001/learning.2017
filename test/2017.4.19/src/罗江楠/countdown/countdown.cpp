#include <bits/stdc++.h>
#define ll long long
using namespace std;
inline int read(){
	int x=0;char ch=getchar();
	while(ch>'9'||ch<'0')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x;
}
int a[200020], ans[500];
int main(){
	freopen("countdown.in", "r", stdin);
	freopen("countdown.out", "w", stdout);
	int n = read(), n2 = n*n;
	for(int i = 1; i <= n2; i++) a[i] = read();
	sort(a+1, a+n2+1);
	int cnt=1;
	for(int i = 2; i <= n2; i++)
		if(a[i]==a[i-1])cnt++;
		else if(cnt&1)ans[++ans[0]]=a[i-1],cnt=1;
		else cnt=1;
	if(cnt&1)ans[++ans[0]]=a[n2];
	sort(ans+1, ans+ans[0]+1);printf("%d", ans[1]);
	for(int i = 2; i <= ans[0]; i++)
		printf(" %d", ans[i]);
	puts("");
}
#include <bits/stdc++.h>
using namespace std;
int a[500];
int gcd(int a, int b){return b?gcd(b, a%b):a;}
int main(int argc, char const *argv[]){
	srand(time(0));
	int n = 400, n2=n*n;
	for(int i = 1; i <= n; i++){
		a[i]=rand()*rand();
		while(a[i]<0)a[i]=rand()*rand();
	}
	sort(a+1, a+n+1);
	freopen("countdown.ans", "w", stdout);printf("%d", a[1]);
	for(int i = 2; i <= n; i++)printf(" %d", a[i]);puts("");
	fclose(stdout);
	freopen("countdown.in", "w", stdout);
	printf("%d\n", n);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			printf("%d ", gcd(a[i], a[j]));
	return 0;
}
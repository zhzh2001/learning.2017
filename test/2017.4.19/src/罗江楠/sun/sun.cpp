#include <bits/stdc++.h>
#define N 100020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
int sta1[N], sta2[N], a[N], n1, n2, ans;
bool mark[N];
int main(){
	freopen("sun.in", "r", stdin);
	freopen("sun.out", "w", stdout);
	n1=read(); n2=read();
	int sum = n1+n2;
	for(int i = 1; i <= n1; ++i)a[i]=sta1[n1-i+1]=read();
	for(int i = 1; i <= n2; ++i)a[i+n1]=sta2[n2-i+1]=read();
	sort(a+1, a+sum+1);
	for(int i = 1; i <= n1; ++i)sta1[i]=lower_bound(a+1, a+sum+1, sta1[i])-a,mark[sta1[i]]=1;
	for(int i = 1; i <= n2; ++i)sta2[i]=lower_bound(a+1, a+sum+1, sta2[i])-a;
	for(int i = sum; i; i--){
		// for(int j = 1; j <= n1; j++)printf("%d ", sta1[j]);puts("");
		// for(int j = 1; j <= n2; j++)printf("%d ", sta2[j]);puts("");
		if(mark[i]){
			while(sta1[n1]!=i){
				mark[sta2[++n2]=sta1[n1--]]=0;
				ans++;
			}
			--n1;
		}
		else{
			while(sta2[n2]!=i){
				mark[sta1[++n1]=sta2[n2--]]=1;
				ans++;
			}
			--n2;
		}
	}
	printf("%d\n", ans);
}
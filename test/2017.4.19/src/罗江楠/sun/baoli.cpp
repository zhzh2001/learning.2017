#include <bits/stdc++.h>
#include <ext/rope>
#define ll long long
using namespace std;
using namespace __gnu_cxx;
rope<int> t1, t2, rt1, rt2;
int a[100020],st1[100020],st2[100020], ans;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=-1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return x*f;
}
void pr(){
	puts("t1");
	for(rope<int>::const_iterator it = t1.begin(); it != t1.end(); it++)
	printf("%d ", *it);puts("");puts("t2");
	for(rope<int>::const_iterator it = t2.begin(); it != t2.end(); it++)
	printf("%d ", *it);puts("");puts("rt1");
	for(rope<int>::const_iterator it = rt1.begin(); it != rt1.end(); it++)
	printf("%d ", *it);puts("");puts("rt2");
	for(rope<int>::const_iterator it = rt2.begin(); it != rt2.end(); it++)
	printf("%d ", *it);puts("");puts("");
}
int main(){
	int n1 = read(), n2 = read();
	int sum = n1+n2;
	for(int i = 1; i <= n1; ++i)a[i]=st1[i]=read();
	for(int i = 1; i <= n2; ++i)a[i+n1]=st2[i+n1]=read();
	sort(a+1, a+sum+1);
	for(int i = 1; i <= n1; ++i) t1.append(lower_bound(a+1, a+sum+1, st1[n1-i+1])-a);
	for(int i = 1; i <= n2; ++i) t2.append(lower_bound(a+1, a+sum+1, st2[sum-i+1])-a);
	for(int i = 1; i <= n1; i++) rt1.append(t1.at(n1-i));
	for(int i = 1; i <= n2; i++) rt2.append(t2.at(n2-i));
	for(int i = sum, s; i; i--){
		if((s=t1.find(i))!=-1){
			t2.append(rt1.substr(0, n1-s-1));
			rt1 = rt1.substr(n1-s, n1-1);
			rt2 = t1.substr(s+1, n1-s-1)+rt2;
			t1 = t1.substr(0, s);
			ans += n1-s-1;
		}
		else{
			s=t2.find(i);
			t1.append(rt2.substr(0, n2-s-1));
			rt2 = rt2.substr(n2-s, n2-1);
			rt1 = t2.substr(s+1, n2-s-1)+rt1;
			t2 = t2.substr(0, s);
			ans += n2-s-1;
		}
		pr();
	}
	printf("%d\n", ans);
	return 0;
}


#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll long long
#define maxn 100010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
struct data{
	ll v,p;
}a[maxn];
ll n,m,c[maxn],ans;
bool cmp(data a,data b){	return a.v<b.v;}
void add(ll x){	for(;x<=n+m+1;x+=x&-x)	++c[x];}
ll ask(ll x){
	ll ans=0;
	for(;x;x-=x&-x)	ans+=c[x];
	return ans;
}
int main(){
	freopen("sun.in","r",stdin);
	freopen("sun.out","w",stdout);
	n=read();	m=read();
	For(i,1,n)	a[m+i+1].v=read();
	For(i,1,m)	a[m-i+1].v=read();
	a[m+1].v=1e18;
	For(i,1,n+m+1)	a[i].p=i;
	sort(a+1,a+n+m+2,cmp);	add(a[1].p);
	For(i,2,n+m+1){
		ans+=ask(max(a[i].p,a[i-1].p))-ask(min(a[i].p,a[i-1].p)-1)-1;
		add(a[i].p);
	}
	writeln(ans);
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll int
#define maxn 200100
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){    ll x=0;char ch=getchar();    while(ch<'0'||ch>'9')ch=getchar();while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x;    }
inline void write(ll x){    if (x>=10) write(x/10);    putchar(x%10+'0');    }
void writeln(ll x){    write(x);    puts("");    }
struct data{
	ll p,v;
}b[maxn];
bool cmp(data x,data y){	return x.v>y.v;	}
bool vis[maxn];
ll a[maxn],pos,ans,n,m;
int main(){
	freopen("sun.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	m=read();
	For(i,1,n)	a[i+m+1]=read();
	For(i,1,m)	a[m-i+1]=read();
	a[m+1]=1e18;
	For(i,1,n+m+1)	b[i].v=a[i],b[i].p=i;
	sort(b+1,b+n+m+2,cmp);
	pos=n+1;	vis[pos]=1;
	For(i,2,n+m+1){
		while(pos>b[i].p)	--pos,ans+=!vis[pos];
		while(pos<b[i].p)	++pos,ans+=!vis[pos];
		vis[pos]=1;
	}
	writeln(ans-n-m);
}

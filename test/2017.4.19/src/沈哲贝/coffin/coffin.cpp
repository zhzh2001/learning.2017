#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<map>
#define ll long long 
#define eps (1e-6)
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll calc(ll x){
	ll answ=0;
	while(x){
		answ+=(x%10)*(x%10);
		x/=10;
	}
	return answ;
}
int main(){
	freopen("coffin.in","r",stdin);
	freopen("coffin.out","w",stdout);
	ll k=read(),a=read(),b=read(),answ=0;
	For(i,1,1500)if(i*k<=b){
		ll t=i*k;
		if (t>=a)	answ+=calc(t)==i;
	}else	break;
	writeln(answ);
}

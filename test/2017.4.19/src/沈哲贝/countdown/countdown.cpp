#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll int
#define maxn 800010
#define inf 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define pa pair<ll,ll>
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	putchar(' ');	}
ll hash[1000010],q[1000],n,tot;
ll gcd(ll a,ll b){	return b?gcd(b,a%b):a;}
void clear(){
	For(i,1,tot-1)	hash[gcd(q[i],q[tot])]-=2;
}
int main(){
	freopen("countdown.in","r",stdin);
	freopen("countdown.out","w",stdout);
	n=read();
	For(i,1,n*n){
		ll x=read();
		++hash[x];
	}
	FOr(i,1000000,1)
	while(hash[i]){
		q[++tot]=i;
		clear();
		--hash[i];
	}
	FOr(i,tot,1)	writeln(q[i]);
}

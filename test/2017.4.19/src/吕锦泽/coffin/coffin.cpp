#include<bits/stdc++.h>
#define ll long long
using namespace std;
ll l,r,a,b,k;
int f[10];
int main(){
	freopen("coffin.in","r",stdin);
	freopen("coffin.out","w",stdout);
	scanf("%I64d%I64d%I64d",&k,&a,&b);
	for(int i=0;i<10;i++) f[i]=i*i;
	l=a/k;r=b/k;ll ans=0;
	for(ll i=l;i<=r;i++){
		ll sum=i*k,pi=0;
		for(;sum;sum=sum/10) pi+=f[sum%10];
		if(pi==i) ans++;
	}
	printf("%I64d",ans);
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll int
#define N 100005
using namespace std;
inline ll read(){
	char ch=getchar(); ll x=0,f=1;
	while (ch<'0'||ch>'9'){if (ch=='-') f=-1; ch=getchar();}
	while (ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
	return x*f;
}
ll n1,n2,s1[N],s2[N],n,max1,max2,ans;
int main(){
	freopen("sun.in","r",stdin);
	freopen("std.out","w",stdout);
	n1=read(); n2=read(); n=n1+n2;
	for (ll i=1;i<=n1;i++)
		s1[n1-i+1]=read();
	for (ll i=1;i<=n2;i++)
		s2[n1-i+1]=read();
	for (ll i=1;i<=n;i++){
		max1=0,max2=0;
		for (ll i=1;i<=n1;i++)
			if (s1[i]>s1[max1]) max1=i;
		for (ll i=1;i<=n2;i++)
			if (s2[i]>s2[max2]) max2=i;
		if (s1[max1]>s2[max2]){
			while (n1&&s1[n1]!=s1[max1]){
				s2[++n2]=s1[n1];
				ans++; n1--;
			}
			n1--;
		} else{
			while (n2&&s2[n2]!=s2[max2]){
				s1[++n1]=s2[n2];
				ans++; n2--;
			}
			n2--;
		}
	}
	printf("%d",ans);
}

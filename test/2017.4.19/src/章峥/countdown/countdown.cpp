#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("countdown.in");
ofstream fout("countdown.out");
const int N=160005,M=1000005;
int a[N],cnt[M],ans[N];
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n*n;i++)
	{
		fin>>a[i];
		cnt[a[i]]++;
	}
	sort(a+1,a+n*n+1);
	int cc=unique(a+1,a+n*n+1)-a-1;
	bool scan=true;
	for(int now=0;now<n;)
	{
		for(;!cnt[a[cc]];cc--);
		ans[++now]=a[cc];
		cnt[a[cc]]--;
		for(int i=1;i<now;i++)
			cnt[gcd(ans[now],ans[i])]-=2;
		if(scan)
		{
			for(int i=1;i<=cc;i++)
				if(cnt[a[i]]&1)
				{
					ans[++now]=a[i];
					cnt[a[i]]--;
					for(int j=1;j<now;j++)
						cnt[gcd(ans[now],ans[j])]-=2;
				}
			scan=false;
		}
	}
	sort(ans+1,ans+n+1);
	for(int i=1;i<=n;i++)
		fout<<ans[i]<<' ';
	fout<<endl;
	return 0;
}
#include<bits/stdc++.h>
#include<windows.h>
using namespace std;
const int n=100000;
ofstream fout("sun.in");
int a[n+5];
int main()
{
	LARGE_INTEGER t;
	QueryPerformanceCounter(&t);
	srand(t.LowPart);
	for(int i=1;i<=n;i++)
		a[i]=i;
	random_shuffle(a+1,a+n+1);
	int n1=rand()%n+1,n2=n-n1;
	fout<<n1<<' '<<n2<<endl;
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	return 0;
}
#include<bits/stdc++.h>
#include<windows.h>
using namespace std;
const int n=400,m=1e6;
ofstream fout("countdown.in");
ofstream fans("countdown.ans");
int a[n+5];
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	default_random_engine gen(GetTickCount());
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(m-5,m);
		a[i]=d(gen);
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		fans<<a[i]<<' ';
	fans<<endl;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fout<<gcd(a[i],a[j])<<' ';
	fout<<endl;
	return 0;
}
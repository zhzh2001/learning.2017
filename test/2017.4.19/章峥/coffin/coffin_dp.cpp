#include<fstream>
#include<sstream>
#include<string>
#include<cmath>
using namespace std;
ifstream fin("coffin.in");
ofstream fout("coffin.out");
const int N=18,M=1500;
long long f[N+1][10][M];
long long query(long long n,long long k)
{
	stringstream ss;
	ss<<n;
	string s=ss.str();
	long long ans=0;
	for(int i=1;i<s.length();i++)
		for(int j=1;j<10;j++)
			for(int sum=j*j;sum<=i*81&&sum*k<=n;sum++)
				if(floor(log10((long double)sum*k)+1)==i&&f[i][j][sum])
				{
					ans++;
					fout<<sum*k<<endl;
				}
	for(int i=1;i<s[0]-'0'||(i==s[0]-'0')&&(s.length()==1);i++)
		for(int sum=i*i;sum<=s.length()*81&&sum*k<=n;sum++)
			if(floor(log10((long double)sum*k)+1)==s.length()&&f[s.length()][i][sum])
			{
				ans++;
				fout<<sum*k<<endl;
			}
	for(int i=s.length()-1;i;i--)
	{
		stringstream t(s.substr(s.length()-i));
		t>>n;
		for(int j=0;j<s[s.length()-i]-'0'||(j==s[s.length()-i]&&i==1);j++)
			for(int sum=j*j;sum<=i*81&&sum*k<=n;sum++)
				if(floor(log10((long double)sum*k)+1)==i&&f[i][j][sum])
				{
					ans++;
					fout<<sum*k<<endl;
				}
	}
	return ans;
}
int main()
{
	for(int i=0;i<10;i++)
		f[1][i][i*i]=1;
	for(int i=2;i<=N;i++)
		for(int j=0;j<10;j++)
			for(int k=j*j;k<=i*81;k++)
				for(int pred=0;pred<10;pred++)
					f[i][j][k]+=f[i-1][pred][k-j*j];
	long long k,a,b;
	fin>>k>>a>>b;
	fout<<query(b,k)-query(a-1,k)<<endl;
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("coffin.in");
ofstream fout("coffin.out");
int main()
{
	long long k,a,b;
	fin>>k>>a>>b;
	if((b-a)/k>1e6)
	{
		fout<<0<<endl;
		return 0;
	}
	long long i=a/k*k;
	if(i<a)
		i+=k;
	int ans=0;
	for(;i<=b;i+=k)
	{
		long long t=i,sum=0;
		do
			sum+=(t%10)*(t%10);
		while(t/=10);
		ans+=sum*k==i;
	}
	fout<<ans<<endl;
	return 0;
}
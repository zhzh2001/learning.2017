#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define mo 998244353
using namespace std;
int rnd(){
	int x=rand()%4+1;
	long long y=1;
	for (;x;x--) y=y*(rand()+1)%mo;
	return y;
}
int main(){
	freopen("necklace.in","w",stdout);
	srand(23333333);
	int lim1,lim2;
	scanf("%d%d",&lim1,&lim2);
	printf("%d %d\n",lim1,lim2);
	for (int i=1;i<=lim1;i++)
		printf("%d ",rnd()%lim2+1);
	printf("\n%d\n",lim1);
	for (int i=1;i<=lim1-3;i++){
		int kind=rnd()%6+1;
		if (kind==1) printf("R %d\n",rnd()%(lim1-2)+1);
		if (kind==2) printf("F\n");
		if (kind==3) printf("S %d %d\n",rnd()%lim1+1,rnd()%lim1+1);
		if (kind==4){
			int x=rnd()%lim1+1;
			printf("P %d %d %d\n",x,(x+rnd()%10-1)%lim1+1,rnd()%lim2+1);
		}
		if (kind==5) printf("C\n");
		if (kind==6) printf("CS %d %d\n",rnd()%lim1+1,rnd()%lim1+1);
	}
}

const mo:longint=1000000007;
      maxn:longint=1000000;
var phi:array[0..1000000]of int64;
    j,ans,i,n:longint;
    b:array[1..3,0..50000]of longint;
    a:array[1..2]of longint;
procedure pre;
var i,j:longint;
begin
  for i:=1 to maxn do phi[i]:=i;
  for i:=2 to maxn do
     if phi[i]=i then
        for j:=1 to maxn div i do phi[j*i]:=(phi[j*i]*(i-1)div i)mod mo;
end;
procedure dfs(x,now:longint);
var i:longint;
begin
  if x=n+1 then begin ans:=(ans+phi[now])mod mo; exit; end;
  for i:=1 to b[x,0] do dfs(x+1,now*b[x,i]);
end;
begin
  assign(input,'phi.in'); assign(output,'phi.out');
  reset(input); rewrite(output);
  readln(n);
  pre;
  for i:=1 to n do readln(a[i]);
  for i:=1 to n do
    begin
    for j:=1 to trunc(sqrt(a[i])) do
      if a[i] mod j=0 then
    	begin inc(b[i,0]); b[i,b[i,0]]:=j; inc(b[i,0]); b[i,b[i,0]]:=a[i] div j; end;
    if sqr(trunc(sqrt(a[i])))=a[i] then dec(b[i,0]);
    end;
  dfs(1,1);
  writeln(ans);
  close(input); close(output);
end.

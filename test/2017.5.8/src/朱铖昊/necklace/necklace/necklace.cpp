#include<bits/stdc++.h>
using namespace std;
#define ll long long
#define iter iterator
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(int x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const int N=400005;
int a[N],n,c,m,b[N],x,y,z;
char ch[10];
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	read(n);
	read(c);
	for (int i=1;i<=n;++i)
		read(a[i]);
	read(m);
	for (int l=1;l<=m;++l)
	{
		scanf("%s",ch);
		if (ch[0]=='R')
		{
			read(x);
			for (int i=1;i+x<=n;++i)
				b[i]=a[i+x];
			for (int i=1;i<=x;++i)
				b[n+i-x]=a[i];
			for (int i=1;i<=n;++i)
				a[i]=b[i];
		}
		if (ch[0]=='F')
		{
			for (int i=2;i<=(n+1)/2;++i)
				swap(a[i],a[n-i+2]);
		}
		if (ch[0]=='S')
		{
			read(x);
			read(y);
			swap(a[x],a[y]);
		}
		if (ch[0]=='P')
		{
			read(x);
			read(y);
			read(z);
			for (int i=x;i<=y;++i)
				a[i]=z;
		}
		if (ch[0]=='C')
		{
			if (ch[1]=='S')
			{
				read(x);
				read(y);
				if (x>y)
					y=y+n;
				for (int i=1;i<=n;++i)
					a[i+n]=a[i];
				int sum=1;
				for (int i=x+1;i<=y;++i)
					if (a[i]!=a[i-1])
						++sum;
				if (y-x+1==n&&a[x]==a[y])
					--sum;
				write(sum);
				putchar('\n');
			}
			else
			{
				x=1;
				y=n;
				int sum=1;
				for (int i=x+1;i<=y;++i)
					if (a[i]!=a[i-1])
						++sum;
				if (y-x+1==n&&a[x]==a[y])
					--sum;
				write(sum);
				putchar('\n');
			}
		}
	}
	return 0;
}
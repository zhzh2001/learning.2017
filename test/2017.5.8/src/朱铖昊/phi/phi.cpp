#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll ans,n;
int a[100050];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
ll gcd(ll n,ll m)
{
	if (m==0) return n;
	if (n%2==0&&m%2==0) return 2*gcd(max(n/2,m/2),min(n/2,m/2));
	if (n%2==0&&m%2!=0) return gcd(max(n/2,m),min(n/2,m));
	if (n%2!=0&&m%2==0) return gcd(max(n,m/2),min(n,m/2));
	if (n%2!=0&&m%2!=0) return gcd(abs(m-n),min(n,m));
}
inline void dfs(int x,int y)
{
	if(x==n+1)
	{
		For(i,1,y)if(gcd(i,y)==1)ans++;
		return;
	}
	For(i,1,a[x])if(a[x]%i==0)dfs(x+1,y*i);
}
int main()
{
	freopen("phi.in","r",stdin);
	freopen("phi.out","w",stdout);
	n=read();
	For(i,1,n)a[i]=read();
	dfs(1,1);
	cout<<ans<<endl;
	return 0;
}


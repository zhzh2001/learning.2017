#include <bits/stdc++.h>
#define ll long long
using namespace std;
ll ans, mn;
inline int read(){
	int x=0;char ch=getchar();
	while(ch>'9'||ch<'0')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x;
}
int main(){
	freopen("shopping.in", "r", stdin);
	freopen("shopping.out", "w", stdout);
	mn = 1<<30;
	ll n = read(), x;
	while(n--){
		x = read();
		ans += x;
		if(x&1) if(x < mn) mn = x;
	}
	if(!(ans&1))ans-=mn;
	printf("%I64d\n", ans);
}
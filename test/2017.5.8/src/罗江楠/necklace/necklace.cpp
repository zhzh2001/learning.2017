#include <bits/stdc++.h>
#define ll long long
#define N 200020
using namespace std;
inline int read(){
	int x=0;char ch=getchar();
	while(ch>'9'||ch<'0')ch=getchar();
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return x;
}
struct node{
	int l, r, val;
}tr[N<<2];
int ls[N<<2], rs[N<<2], rev, k;
node operator + (const node &a, const node &b){
	node ans;
	ans.l = a.l; ans.r = b.r;
	ans.val = a.val+b.val;
	if(a.r == b.l) ans.val--;
	return ans;
}
void clean(int x, int val){
	tr[x].l = tr[x].r = val;
	tr[x].val = 1;
	if(ls[x] == rs[x]) return;
	clean(x<<1, val); clean(x<<1|1, val);
}
void _set(int x, int l, int r, int val){
	if(ls[x] == l && rs[x] == r){
		tr[x].l = tr[x].r = val;
		tr[x].val = 1;
		clean(x, val);
		return;
	}
	int mid = ls[x] + rs[x] >> 1;
	if(r <= mid) _set(x<<1, l, r, val);
	else if(l > mid) _set(x<<1|1, l, r, val);
	else _set(x<<1, l, mid, val), _set(x<<1|1, mid+1, r, val);
}
node ask(int x, int l, int r){
	if(ls[x] == l && rs[x] == r) return tr[x];
	int mid = ls[x] + rs[x] >> 1;
	if(r <= mid) return ask(x<<1, l, r);
	if(l > mid) return ask(x<<1|1, l, r);
	return ask(x<<1, l, mid)+ask(x<<1|1, mid+1, r);
}
int askval(int x, int l){
	if(ls[x] == rs[x]) return tr[x].l;
	int mid = ls[x]+rs[x]>>1;
	if(l <= mid) return askval(x<<1, l);
	else return askval(x<<1|1, l);
}
void build(int x, int l, int r){
	ls[x] = l; rs[x] = r;
	if(l == r){
		tr[x].l = tr[x].r = read();
		tr[x].val = 1;
		return;
	}
	int mid = l + r >> 1;
	build(x<<1, l, mid); build(x<<1|1, mid+1, r);
	tr[x] = tr[x<<1] + tr[x<<1|1];
}
void _swap(int x, int y){
	int xx = askval(1, x);
	int yy = askval(1, y);
	_set(1, x, x, yy);
	_set(1, y, y, xx);
}
int main(){
	freopen("necklace.in", "r", stdin);
	freopen("necklace.out", "w", stdout);
	int n = read(); read();
	build(1, 1, n);
	int m = read();
	char s[5];
	while(m--){
		scanf("%s", s);
		switch(s[0]){
		case 'F':rev^=1;k=-k;break;
		case 'R':k+=read();break;
		case 'S':_swap(read(), read());break;
		case 'C':
			if(s[1] == 'S'){
				int i = read()+k, j = read();
				if(j == n){
								node ans = ask(1, 1, n);
								if(ans.l == ans.r) ans.val--;
								printf("%d\n", ans.val);break;}
				if(i < 0) i = (i%n+n)%n;
				if(!i) i = n; i = (i-1)%n+1;
				if(i+j > n) printf("%d\n", (ask(1, i, n)+ask(1, 1, i+j-n)).val);
				else printf("%d\n", ask(1, i, i+j).val);
			}
			else{
				node ans = ask(1, 1, n);
				if(ans.l == ans.r) ans.val--;
				printf("%d\n", ans.val);
			}
			break;
		case 'P':
			int i = read()+k, j = read(), v = read();
			if(i < 0) i = (i%n+n)%n;
			if(!i) i = n; i = (i-1)%n+1;
			if(i+j > n) _set(1, i, n, v), _set(1, 1, i+j-n, v);
			else _set(1, i, i+j, v);
			break;
		}
	}
}
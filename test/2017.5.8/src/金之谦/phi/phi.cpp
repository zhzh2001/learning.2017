#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
const ll MOD=1e9+7;
ll p[6][100001];
ll n,a[100001],sum=0;
bool b[10000001];
ll pri[1000001],phi[10000001];
inline void get(){
	phi[1]=1;
	for(ll i=2;i<=1e7;i++){
		if(!b[i]){pri[++sum]=i;phi[i]=i-1;}
		for(ll j=1;j<=sum;j++){
			if(i*pri[j]>1e7)break;
			b[i*pri[j]]=1;
			if(i%pri[j]==0){phi[i*pri[j]]=phi[i]*pri[j];break;}
			else phi[i*pri[j]]=phi[i]*(pri[j]-1);
		}
	}
}
int main()
{
	freopen("phi.in","r",stdin);
	freopen("phi.out","w",stdout);
	scanf("%I64d",&n);
	get();
	for(ll i=1;i<=n;i++)scanf("%I64d",&a[i]);
	if(n<=5){
		for(ll i=1;i<=n;i++)
			for(ll j=1;j<=a[i];j++)if(a[i]%j==0)p[i][++p[i][0]]=j;
		ll ans=0,x,y,z;
		for(ll i=1;i<=p[1][0];i++){
			x=p[1][i];
			for(ll j=1;j<=p[2][0];j++){
				y=p[2][j];
				if(n>2)for(ll k=1;k<=p[3][0];k++)(ans+=phi[x*y*p[3][k]])%=MOD;
				else ans+=phi[x*y];
			}
		}
		printf("%I64d",ans);
	}
	return 0;
}

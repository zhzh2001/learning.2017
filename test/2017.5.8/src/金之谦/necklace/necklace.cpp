#include<bits/stdc++.h>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,c,m,a[200001],t[400001];
void build(int l,int r,int nod){
	if(l==r){t[nod]=a[l];return;}
	
}
int main()
{
	freopen("necklace.in","r",stdin);
	freopen("necklace.out","w",stdout);
	scanf("%d%d",&n,&c);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	scanf("%d",&m);
	if(n<=1000){
		while(m--){
			char r[5];scanf("%s",r+1);
			if(r[1]=='C'&&r[2]=='S'){
				int x=read(),y=read();
				if(y==x-1){
					int sum=0;
					for(int i=1;i<=n;i++){
						int h=(i<=n)?i:i-n,q=(h-1)?h-1:h+n-1;
						if(a[h]!=a[q])sum++;
					}
					printf("%d\n",sum);
				}
				else{
					if(x>y)y+=n;int sum=1;
					for(int i=x+1;i<=y;i++){
						int h=(i<=n)?i:i-n,q=(h-1)?h-1:h+n-1;
						if(a[h]!=a[q])sum++;
					}
					printf("%d\n",sum);
				}
			}else if(r[1]=='C'){
				int sum=0;
				for(int i=1;i<=n;i++){
					int h=(i<=n)?i:i-n,q=(h-1)?h-1:h+n-1;
					if(a[h]!=a[q])sum++;
				}
				printf("%d\n",sum);
			}else if(r[1]=='P'){
				int x=read(),y=read(),z=read();
				if(x>y)y+=n;
				for(int i=x;i<=y;i++){
					int h=(i<=n)?i:i-n;
					a[h]=z;
				}
			}else if(r[1]=='S'){
				int x=read(),y=read();
				swap(a[x],a[y]);
			}else if(r[1]=='F'){
				for(int i=2;i<=n/2+1;i++)swap(a[i],a[n-i+2]);
			}else{
				int k=read();
				for(int i=n;i;i--)a[i+k]=a[i];
				for(int i=1;i<=k;i++)a[i]=a[n+i];
			}
		}
		return 0;
	}
	build(1,n,1);
	
	return 0;
}

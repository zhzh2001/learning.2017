#include <iostream>
#include <cstdio>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

const int N = 200005;

ifstream fin("shopping.in");
ofstream fout("shopping.out");

typedef long long ll;

ll res, ans, n, w[N], cnt;

inline bool cmp(ll a, ll b) {
	return a > b;
}

int main() {
	fin >> n;
	for (int i = 1; i <= n; i++) fin >> w[i];
	sort(w + 1, w + n + 1, cmp);
	for (int i = 1; i <= n; i++) 
		if ((w[i] & 1) == 0) 
			ans += w[i]; 
		else cnt++;
	res = ans;
	for (int i = 1; i <= n; i++) {
		if (w[i] & 1) {
			ans += w[i];
			if (ans & 1) res = ans;
		}
	}
	fout << res << endl;
	cout << res << endl;
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int mod = 1e9 + 7, N = 1e5 + 5, M = 1e6 + 5;
typedef long long ll;

ll Phi[M];

ll phi(ll x) {
	ll ans = x;
	for (ll i = 2; i * i <= ans; i++) {
		if (x % i == 0) {
			(ans = ans / i * (i - 1)) %= mod;
			while (x % i == 0) x /= i;
		}
	}
	if (x > 1) (ans = ans / x * (x - 1)) %= mod;
	return ans % mod;
}

ifstream fin("phi.in");
ofstream fout("phi.out");

ll ans;
int a[N];

int main() {
	// cout << phi(4) << endl;
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++) fin >> a[i];
	if (n <= 3) {
		if (n == 1) {
			for (int i = 1; i <= a[1]; i++) if (a[1] % i == 0) {
				ans += phi(i);
				// cout << phi(i) << endl;
			}
		} else if (n == 2) {
			for (int i = 1; i <= a[1]; i++) if (a[1] % i == 0)
				for (int j = 1; j <= a[2]; j++) if (a[2] % j == 0) {
					ans += phi(i * j);
					// cout << phi(ll(i * j)) << endl;
				}
		} else if (n == 3) {
			for (int i = 1; i <= a[1]; i++) if (a[1] % i == 0)
				for (int j = 1; j <= a[2]; j++) if (a[2] % j == 0)
					for (int k = 1; k <= a[3]; k++) if (a[3] % k == 0) {
						ans += phi((ll)i * j * k);
						// cout << phi(ll(i * j * k)) << endl;
					}
		}
		fout << ans << endl;
		cout << ans << endl;
		return 0;
	}
	bool flag2 = true, flagt = true;
	int t = a[1];
	for (int i = 1; i <= n; i++) {
		if (a[i] != 2) flag2 = false; 
		if (a[i] != t) flagt = false;
	}
	if (flag2) {
		ans = 1;
		for (int i = 1; i <= n; i++)
			ans = ans * 3 - 1;
		fout << ans << endl;
		cout << ans << endl;
		return 0;
	} 
	if (flagt) {
		
	}
	return 0;
}
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;

const int mod = 1e9 + 7, N = 1e5 + 5, M = 1e6 + 5;
typedef long long ll;

ll Phi[M];

ll phi(ll x) {
	ll ans = x;
	for (ll i = 2; i * i <= ans; i++) {
		if (x % i == 0) {
			(ans = ans / i * (i - 1)) %= mod;
			while (x % i == 0) x /= i;
		}
	}
	if (x > 1) (ans = ans / x * (x - 1)) %= mod;
	return ans % mod;
}

ifstream fin("phi.in");
ofstream fout("phi.out");

ll ans;
int a[N];
int hash[1048576];

void dfs(ll n, ll sum) {
	if (n == 0) {
		hash[phi(sum)]++;
		(ans += phi(sum)) %= mod;
		return;
	}
	dfs(n - 1, sum * 1);
	dfs(n - 1, sum * 2);
	dfs(n - 1, sum * 3);
	dfs(n - 1, sum * 6);
}

int main() {
	for (int i = 1; i <= 8; i++) {
		ans = 0;
		memset(hash, 0, sizeof hash);
		dfs(i, 1);
		for (int i = 1; i <= 1023; i++) {
			if (hash[i]) cout << i << ':' << hash[i] << endl;
		}
		cout << ans << endl << endl;
	}
	return 0;
}
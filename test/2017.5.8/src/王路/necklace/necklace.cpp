#include <iostream>
#include <cstdio>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

const int N = 200005;

ifstream fin("necklace.in");
ofstream fout("necklace.out");

int n, c, color[N], q;
char opt[5];

struct Query {
	int opt, a, b, c;
} Q[N];

bool flagR, flagF;

int main() {
	fin >> n >> c;
	for (int i = 1; i <= n; i++) fin >> color[i];
	fin >> q;
	for (int i = 1; i <= q; i++) {
		fin >> opt;
		switch (opt[0]) {
			case 'R':
				Q[i].opt = 1;
				fin >> Q[i].a;
				flagR = true;
				break;
			case 'F':
				Q[i].opt = 2;
				flagF = true;
				break;
			case 'S':
				Q[i].opt = 3;
				fin >> Q[i].a >> Q[i].b;
				break;
			case 'P':
				Q[i].opt = 4;
				fin >> Q[i].a >> Q[i].b >> Q[i].c;
				break;
			case 'C': {
				if (opt[1] == 'S') {
					Q[i].opt = 6;
					fin >> Q[i].a >> Q[i].b;
				} else Q[i].opt = 5;
				break;
			}
		}
	}
	#define cur(i) ((i + n - 1) % n + 1)
	if (n <= 1000 && q <= 1000) {
		int k = 0;
		int ans = 0;
		for (int i = 1; i <= q; i++) {
			// cout << Q[i].opt << endl;
			if (Q[i].opt == 1) {
				k += Q[i].a;
			} else if (Q[i].opt == 2) {
				for (int i = k + 1, j = n + k - 1; i != j; i++, j--) {
					swap(color[cur(i)], color[cur(j)]);
				}
			} else if (Q[i].opt == 3) {
				int a = Q[i].a - k, b = Q[i].b - k;
				swap(color[cur(a)], color[cur(b)]);
			} else if (Q[i].opt == 4) {
				int a = Q[i].a - k, b = Q[i].b - k;
				for (int k = a; k <= b; k++) {
					color[cur(k)] = Q[i].c;
				}
			} else if (Q[i].opt == 5) {
				ans = 0;
				for (int i = 1; i <= n; i++)
					if (color[i] != color[i - 1]) ans++;
				if (color[1] == color[n]) ans--;
				fout << ans << endl;
				cout << ans << endl;
			} else if (Q[i].opt == 6) {
				ans = 0;
				int a = Q[i].a - k, b = Q[i].b - k;
				for (int k = a; k <= b; k++) {
					if (color[cur(k)] == color[cur(k - 1)]) ans++;
				}
				fout << ans << endl;
				cout << ans << endl;
			}
			for (int i = 1; i <= n; i++) cout << color[i] << ' '; cout << endl;
		}
	}
	return 0;
}
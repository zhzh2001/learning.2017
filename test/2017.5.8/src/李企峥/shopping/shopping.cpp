#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,x,mn,ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("shopping.in","r",stdin);
	freopen("shopping.out","w",stdout);
	n=read();
	mn=1e9+1;
	For(i,1,n)
	{
		x=read();
		if(x%2==0)ans+=x;
			else 
			{
				mn=min(mn,x);
				ans+=x;
			}
	}
	if(ans%2==0)ans-=mn;
	cout<<ans<<endl;	
	return 0;
}


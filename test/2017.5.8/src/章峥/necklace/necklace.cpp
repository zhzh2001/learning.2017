#include<fstream>
using namespace std;
ifstream fin("necklace.in");
ofstream fout("necklace.out");
const int N=200005;
int a[N],b[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	int q;
	fin>>q;
	while(q--)
	{
		string opt;
		fin>>opt;
		int i,j,k;
		switch(opt[0])
		{
			case 'R':
				fin>>k;
				for(int i=1;i<=n;i++)
					b[(k+i-1)%n+1]=a[i];
				memcpy(a,b,sizeof(a));
				break;
			case 'F':
				for(int i=2;n-i+2>i;i++)
					swap(a[i],a[n-i+2]);
				break;
			case 'S':
				fin>>i>>j;
				swap(a[i],a[j]);
				break;
			case 'P':
				fin>>i>>j>>k;
				for(;i!=j;i=i%n+1)
					a[i]=k;
				a[j]=k;
				break;
			case 'C':
				if(opt.length()==1)
				{
					int cnt=0,now=0;
					for(int i=1;i<=n;i++)
						if(a[i]!=now)
						{
							cnt++;
							now=a[i];
						}
					if(a[1]==a[n]&&cnt>1)
						cnt--;
					fout<<cnt<<endl;
				}
				else
				{
					fin>>i>>j;
					int cnt=0,now=0;
					for(int t=i;t!=j;t=t%n+1)
						if(a[t]!=now)
						{
							cnt++;
							now=a[t];
						}
					if(a[j]!=now)
						cnt++;
					if(j%n+1==i&&a[i]==a[j]&&cnt>1)
						cnt--;
					fout<<cnt<<endl;
				}
				break;
		}
	}
	return 0;
}
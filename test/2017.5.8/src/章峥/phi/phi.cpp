#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("phi.in");
ofstream fout("phi.out");
const int P=1e6,MOD=1e9+7,inv=MOD-2;
int pb[30];
struct node
{
	int p,cnt;
	bool operator<(const node& rhs)const
	{
		if(p==rhs.p)
			return cnt<rhs.cnt;
		return p<rhs.p;
	}
}a[P];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n;
	fin>>n;
	int m=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		for(int j=2;j*j<=x;j++)
			if(x%j==0)
			{
				a[++m].p=j;
				for(;x%j==0;x/=j,a[m].cnt++);
			}
		if(x>1)
		{
			a[++m].p=x;
			a[m].cnt=1;
		}
	}
	sort(a+1,a+m+1);
	pb[0]=1;
	int ans=1;
	for(int i=1;i<=m;)
	{
		int j;
		for(j=i+1;j<=m&&a[j].p==a[i].p;j++);
		for(int t=1;t<=a[j-1].cnt;t++)
			pb[t]=(long long)pb[t-1]*a[i].p%MOD;
		for(int t=1;t<=a[j-1].cnt;t++)
			pb[t]=(pb[t-1]+pb[t])%MOD;
		int now=1;
		for(int t=i;t<j;t++)
			now=(long long)now*pb[a[t].cnt]%MOD;
		now=(now-1+MOD)%MOD;
		ans=ans*(now*qpow(a[i].p,inv)%MOD*(a[i].p-1)%MOD+1)%MOD;
		i=j;
	}
	fout<<ans<<endl;
	return 0;
}
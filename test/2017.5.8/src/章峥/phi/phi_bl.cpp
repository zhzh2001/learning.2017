#include<bits/stdc++.h>
using namespace std;
const int N=5005,MOD=1e9+7;
int f[N][N];
int main()
{
	int n;
	cin>>n;
	f[1][0]=f[1][1]=1;
	for(int i=2;i<=n;i++)
	{
		f[i][0]=f[i][i]=1;
		for(int j=1;j<i;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j-1])%MOD;
	}
	int ans=1,p2=1;
	for(int i=1;i<=n;i++)
	{
		ans=(ans+(long long)f[n][i]*p2)%MOD;
		p2=p2*2%MOD;
	}
	cout<<ans<<endl;
	return 0;
}
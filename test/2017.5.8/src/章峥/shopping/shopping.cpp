#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("shopping.in");
ofstream fout("shopping.out");
int main()
{
	int n;
	fin>>n;
	long long f0=0,f1=0xc0c0c0c0c0c0c0c0ll;
	while(n--)
	{
		int x;
		fin>>x;
		if(x&1)
		{
			long long g0=f1+x,g1=f0+x;
			f0=max(f0,g0);
			f1=max(f1,g1);
		}
		else
		{
			f0+=x;
			f1+=x;
		}
	}
	fout<<f1<<endl;
	return 0;
}
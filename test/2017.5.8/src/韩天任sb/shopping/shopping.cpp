#include<bits/stdc++.h>
using namespace std;
long long ans,n,k,minn,x;
int main(){
	freopen("shopping.in","r",stdin);
	freopen("shopping.out","w",stdout);
	cin>>n;
	k=0;
	minn=1e9+1;
	for (int i=1;i<=n;i++){
		cin>>x;
		if (x%2==0){
			ans+=x;
			k++;
		}
		else{
			if (x<minn)
				minn=x;
			ans+=x;
		}
	}
	k=n-k;
	if (k%2==0)
		cout<<ans-minn;
	else
		cout<<ans;
}

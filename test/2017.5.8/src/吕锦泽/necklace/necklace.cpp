#include<bits/stdc++.h>
using namespace std;
#define N 200005
inline int rd(){int x=0,f=1;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar())if(ch=='-') f=-1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return x*f;}
int n,Q,a[N],cc,rot,block,m,b[500],bl[N],c[500],l[500],r[500];
inline void spfa(){
	block=sqrt(n);
	if(n%block) m=n/block+1;else m=n/block;
	for(int i=1;i<=m;i++)
		for(int j=(i-1)*block+1;j<=i*block;j++)
			if(a[j]!=a[j-1]||j==(i-1)*block+1) ++b[i];
	for(int i=1;i<=n;i++) bl[i]=(bl[i]-1)/block+1;
	for(int i=1;i<=m;i++) l[i]=(i-1)*block+1,r[i]=i*block;
	r[m]=n;
}
inline bool pd(int x,int y){
	if(!c[bl[x]]){
		if(!c[bl[y]]) return (a[x]!=a[y]);
		else return (c[bl[y]]==a[x]);
	}else{
		if(!c[bl[y]]) return (c[bl[x]]==a[y]);
		else return (c[bl[x]]==c[bl[y]]);
	}
}
inline void Swap(int x,int y){
	if(pd(x,y)) return;
	if(x>y) swap(x,y);
	if(c[bl[x]]) for(int i=l[bl[x]];i<=r[bl[x]];i++) a[i]=c[bl[x]];c[bl[x]]=0;
	if(c[bl[y]]) for(int i=l[bl[y]];i<=r[bl[y]];i++) a[i]=c[bl[y]];c[bl[y]]=0;
	swap(a[x],a[y]);int sum=0;
	for(int i=l[bl[x]];i<=r[bl[x]];i++) if(i==l[bl[x]]||a[i]!=a[i-1]) sum++;b[bl[x]]=sum;sum=0;
	for(int i=l[bl[y]];i<=r[bl[y]];i++) if(i==l[bl[x]]||a[i]!=a[i-1]) sum++;b[bl[y]]=sum;
}
inline void change(int x,int y,int C){
	if(x<y){
		for(int i=bl[x]+1;i<=bl[y]-1;i++) c[i]=C,b[i]=1;
		for(int i=x;i<=r[bl[x]];i++) a[i]=C;int sum=0;
		for(int i=l[bl[x]];i<=r[bl[x]];i++) if(i==l[bl[x]]||a[i]!=a[i-1]) sum++;b[bl[x]]=sum;
		for(int i=l[bl[y]];i<=y;i++) a[i]=C;sum=0;
		for(int i=l[bl[y]];i<=r[bl[y]];i++) if(i==l[bl[y]]||a[i]!=a[i-1]) sum++;b[bl[y]]=sum;
	}else{
		for(int i=bl[x]+1;i<=m;i++) c[i]=C,b[i]=1;
		for(int i=1;i<=bl[y]-1;i++) c[i]=C,b[i]=1;
		for(int i=x;i<=r[bl[x]];i++) a[i]=C;int sum=0;
		for(int i=l[bl[x]];i<=r[bl[x]];i++) if(i==l[bl[x]]||a[i]!=a[i-1]) sum++;b[bl[x]]=sum;
		for(int i=l[bl[y]];i<=y;i++) a[i]=C;sum=0;
		for(int i=l[bl[y]];i<=r[bl[y]];i++) if(i==l[bl[y]]&&a[i]!=a[i-1]) sum++;b[bl[y]]=sum; 
	}
}
/*inline int query(int x,int y){
	if(x<=y){
		int ans=0;
		for(int i=bl[x]+1;i<bl[y];i++) ans+=b[i]-pd(r[i],l[i+1]);
		for(int i=x;i<=r[bl[x]])
			if(a[i])
	}else{
		
	}
}*/
int main(){
	n=rd(),cc=rd();
	for(int i=1;i<=n;i++) a[i]=rd();
	spfa();Q=rd();
	while(Q--){
		char ch[10];scanf("%s",ch);
		int x,y,z;
		switch(ch[0]){
			case 'R':rot=(rot+rd())%n;break;
			case 'S':x=rd()-rot,y=rd()-rot;Swap((x+n)%n,(y+n)%n);break;
			case 'P':x=rd()-rot,y=rd()-rot,z=rd();change((x+n)%n,(y+n)%n,z);break;
			case 'C':{
				if(ch[1]=='S') printf("%d\n",query(1,n));
				else x=rd()-rot,y=rd()-rot,printf("%d\n",query((x+n)%n,(y+n)%n));
				break;
			}
		}
	}
	return 0;
}

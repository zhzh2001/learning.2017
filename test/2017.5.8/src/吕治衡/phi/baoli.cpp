#include<bits/stdc++.h>
using namespace std;
long long x,y,z,ans;
long long gcd(long long x,long long y)
	{
		if (x<y)return gcd(y,x);
		if (y==0)return x;
		return gcd(y,x%y);
	}
long long phi(long long x)
	{
		long long ans=0;
		for (int i=1;i<=x;i++)
			if (gcd(x,i)==1)ans++;
		return ans;
	}
int main()
{
	freopen("phi.in","r",stdin);freopen("baoli.out","w",stdout);
	cin>>x;
	cin>>x>>y>>z;
	for (int i=1;i<=x;i++)
		if (x%i==0)
			for (int j=1;j<=y;j++)
				if (y%j==0)
					for (int k=1;k<=z;k++)
						if (z%k==0)
							ans+=phi((long long)i*j*k);
	cout<<ans<<endl;
}

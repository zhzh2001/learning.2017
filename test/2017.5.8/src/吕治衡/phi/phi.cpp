#include<bits/stdc++.h>
using namespace std;
long long n,lsg,a[1000000],f[3000000],ff[3000000],ans,flag[3000000];
int main()
{
	freopen("phi.in","r",stdin);freopen("phi.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n;lsg=1e9+7;
	for (int i=1;i<=n;i++)cin>>a[i];
	for (int i=1;i<=2e6;i++)f[i]=1;
	for (int i=1;i<=n;i++)
		{
			for (int j=2;j<=sqrt(a[i]);j++)
				while (a[i]%j==0)
					{
						a[i]/=j;
						if (flag[j]!=i-1)ff[j]=f[j]-1,flag[j]=i-1;
						(f[j]=f[j]*j+ff[j])%=lsg;
					}
			if (flag[a[i]]!=i-1)ff[a[i]]=f[a[i]]-1,flag[a[i]]=i-1;
			(f[a[i]]=f[a[i]]*a[i]+ff[a[i]])%=lsg;a[i]=1;
		}
	ans=1;
	for (int i=1;i<=2e6;i++)
		(ans*=f[i])%=lsg;
	cout<<(ans%lsg+lsg)%lsg<<endl;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=1000005,M=100005,mod=1000000007;
int phi[N],phi2[N],get2[2000];
int p[N],cnt,c[3][30];
bool flag[N];
int n,a[M],m,ans;

void GetPhi(){
	phi2[1]=1; phi2[0]=1;
	int x=2;
	for(int i=1;i<=10;i++,x*=2) get2[x]=i;
	for(int i=2;i<N;i++) phi2[i]=(phi2[i-1]*2)%mod;
	cnt=0;
	memset(flag,true,sizeof flag);
	phi[1]=1;
	for(int i=2;i<N;i++){
		if(flag[i]){
			p[cnt++]=i;
			phi[i]=i-1;
		}
		for(int j=0;j<cnt;j++){
			if(i*p[j]>N) break;
			flag[i*p[j]]=false;
			if(i%p[j]==0){
				phi[i*p[j]]=p[j]*phi[i];
				break;
			}
			else phi[i*p[j]]=(p[j]-1)*phi[i];
		}
	}
}

int main(){
	freopen("phi.in","r",stdin);
	freopen("phi.out","w",stdout);
	GetPhi();
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	bool flag=true;
	if(n<=2){
		for(int i=1;i<=n;i++) if(get2[a[i]]==0) flag=false;
		if(!flag){
			if(n==1){
				for(int i=1;i<=a[1];i++) if(a[1]%i==0) ans=(ans+phi[i])%mod;
				printf("%d\n",ans);
			}
			else{
				for(int i=1;i<=a[1];i++)
					if(a[1]%i==0)
						for(int j=1;j<=a[2];j++)
							if(a[2]%j==0) ans=(ans+phi[i]*phi[j])%mod;
				printf("%d\n",ans);
			}
		}
	}
	if(flag){
		for(int i=1;i<=n;i++) m+=get2[a[i]];
		int num=1; ans=num*phi2[m]+num;
		for(int i=m-1;i;i--){
			num=(1LL*num*(i+1)/(m-i))%mod;
			ans=(ans+1LL*num*phi2[i])%mod;
		}
		printf("%d\n",ans);
	}
	printf("%d\n",phi[30]);
	return 0;
}

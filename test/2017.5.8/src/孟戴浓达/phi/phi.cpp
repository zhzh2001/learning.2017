//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("phi.in");
ofstream fout("phi.out");
const int mod=1000000007;
int phi[1000003];
int maxai,n;
int a[100003];
bool is2=true;
void pre(){
	int i,j;
	for(int i=1;i<=1000000;i++) phi[i]=i;
	for(int i=2;i<=1000000;i++){
		if(phi[i]==i){
			for(int j=1;j<=1000000/i;j++){
				phi[j*i]=phi[i*j]/i*(i-1);
			}
		}
	}
}
int poww(int a,int n){
	int r=1,p=a;
	while(n>0){
    	if(n&1) r=(r*p)%mod;
    	p=(p*p)%mod; n>>=1;
	}
	return r;
}
inline void run2(int n){
	long long lastnum=1,mi2=1,ans=0;
	for(int i=1;i<=n;i++){
		(lastnum*=i)%=mod;
	}
	lastnum=1;
	for(int i=1;i<=n;i++){
		(lastnum*=(poww(i,mod-2)*(n-i+1)%mod))%=mod;
		(mi2*=2)%=mod;
		(ans+=lastnum*mi2)%=mod;
	}
	(ans+=1)%mod;
	fout<<ans<<endl;
	return;
}
inline void n2(){
	long long ans=0;
	for(int i=1;i<=a[1];i++){
		for(int j=1;j<=a[2];j++){
			if(a[1]%i==0&&a[2]%j==0){
				ans+=phi[i*j];
				ans%=mod;
			}
		}
	}
	fout<<ans<<endl;
}
int main(){
	//freopen("phi.in","r",stdin);
	//freopen("phi.out","w",stdout);
	pre();
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>a[i];
		maxai=max(maxai,a[i]);
		if(a[i]!=2){
			is2=false;
		}
	}
	/*
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			a[1]=i;
			a[2]=j;
			cout<<i<<"    "<<j<<"    ";
			n2();
		}
	}
	*/
	if(is2){
		run2(n);
		return 0;
	}if(n==2){
		n2();
		return 0;
	}
	return 0;
}
/*

in:
3
6
10
15

out:
1595

in1:
3
2
2
2

out:
14

*/

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<map>
#include<cstring>
#include<ctime>
#include<vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
int a,b,c,ans;
inline int gcd(int x,int y){return y?gcd(y,x%y):x;}
inline int phi(int x)
{
	int cnt=0;
	For(i,1,x)
		if(gcd(x,i)==1)	cnt++;
	return cnt;
}
int main()
{
	scanf("%d%d%d",&a,&b,&c);
	For(i,1,a)
		if(a%i==0)
		For(j,1,b)
			if(b%j==0)
			For(k,1,c)
				if(c%k==0)
					ans+=phi(i*j*k);
	printf("%d",ans);
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<map>
#include<cstring>
#include<ctime>
#include<vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
ll n,ans=1,zy[1000001],cnt[10000001],tot;
ll mo=1e9+7;
bool vis[1000001];
void get(ll t)
{
	ll tmp=sqrt(t);
	For(i,2,t)
	{
		while(t%i==0)	
		{
			if(!vis[i])	vis[i]=1,zy[++tot]=i;
			cnt[i]++;
			t/=i;
		}
		if(t<=i)	break;
	}
}
int main()
{
//	scanf("%d",&n);
//	For(i,2,sqrt(n))
//		if(n%i==0)	printf("%d\n",i);
	freopen("phi.in","r",stdin);
	freopen("phi.out","w",stdout);
	scanf("%lld",&n);
	For(i,1,n)
	{
		ll t;
		scanf("%lld",&t);
		get(t);
	}
//	For(i,1,tot)	cout<<zy[i]<<' '<<cnt[zy[i]]<<endl;
	For(i,1,tot)
	{
		ll t=zy[i];
		ll sum=0;
		For(ii,1,cnt[t])
			sum=(sum+(sum*t+t-1))%mo;
		if(i==1)	ans=sum+1;else ans=(ans+ans*sum)%mo;
	}
	printf("%lld",ans%mo);
}

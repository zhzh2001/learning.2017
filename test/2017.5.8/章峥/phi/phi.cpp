#include<fstream>
using namespace std;
ifstream fin("phi.in");
ofstream fout("phi.out");
const int N=100005,MOD=1e9+7,INV=MOD-2;
int a[N],p1[N],p2[N];
long long qpow(long long a,int b,long long p)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b/=2);
	return ans;
}
long long phi(long long x)
{
	long long ans=x;
	for(long long i=2;i*i<=x&&x>1;i++)
		if(x%i==0)
		{
			ans=ans/i*(i-1);
			while(x%i==0)
				x/=i;
		}
	if(x>1)
		ans=ans/x*(x-1);
	return ans;
}
int main()
{
	int n;
	fin>>n;
	bool all2=true;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		if(a[i]!=2)
			all2=false;
	}
	if(n<=2)
	{
		if(n==1)
			fout<<a[1]<<endl;
		else
		{
			for(int i=1;i*i<=a[1];i++)
				if(a[1]%i==0)
				{
					p1[++p1[0]]=i;
					if(a[1]/i>i)
						p1[++p1[0]]=a[1]/i;
				}
			for(int i=1;i*i<=a[2];i++)
				if(a[2]%i==0)
				{
					p2[++p2[0]]=i;
					if(a[2]/i>i)
						p2[++p2[0]]=a[2]/i;
				}
			int ans=0;
			for(int i=1;i<=p1[0];i++)
				for(int j=1;j<=p2[0];j++)
					ans=(ans+phi((long long)p1[i]*p2[j]))%MOD;
			fout<<ans<<endl;
		}
	}
	else
		if(all2)
		{
			long long ans=1,c=1;
			for(int i=1;i<=n;i++)
			{
				c=c*(n-i+1)%MOD*qpow(i,INV,MOD)%MOD;
				ans=(ans+c*qpow(2,i-1,MOD))%MOD;
			}
			fout<<ans<<endl;
		}
		else
		{
			int p1,p2;
			for(int i=2;i*i<=a[1];i++)
				if(a[i]%i==0)
				{
					p1=i;
					p2=a[i]/i;
				}
			long long ans=qpow(p1,n-1,MOD)*(p1-1)%MOD,c=1;
			for(int i=1;i<n;i++)
			{
				c=c*(n-i+1)%MOD*qpow(i,INV,MOD)%MOD;
				ans=(ans+c*qpow(p1,n-i-1,MOD)%MOD*(p1-1)%MOD*qpow(p2,i-1,MOD)%MOD*(p2-1)%MOD)%MOD;
			}
			fout<<ans<<endl;
		}
	return 0;
}
#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 1005;
int f[N][N], g[N][N];
inline void update(int x, int y, int val, int opt)
{
	if (val >= f[x][y])
	{
		f[x][y] = val;
		g[x][y] = opt;
	}
}
int main()
{
	string s, t;
	cin >> s >> t;
	int n = s.length(), m = t.length();
	s = ' ' + s;
	t = ' ' + t;
	for (int i = 0; i <= n; i++)
		for (int j = 0; j <= m; j++)
		{
			if (i < n)
				update(i + 1, j, f[i][j], 1);
			if (j < m)
				update(i, j + 1, f[i][j], 2);
			if (i < n && j < m && s[i + 1] == t[j + 1])
				update(i + 1, j + 1, f[i][j] + 1, 3);
		}
	string ans;
	for (int i = n, j = m; i && j;)
	{
		if (g[i][j] == 3)
			ans = s[i] + ans;
		int oi = i, oj = j;
		if (g[oi][oj] & 1)
			i--;
		if (g[oi][oj] & 2)
			j--;
	}
	cout << ans << endl;
	return 0;
}
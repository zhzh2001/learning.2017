# int() Convert a number or string to an integer, or return 0 if no arguments are given. If x is a number, return x.int(). For floating point numbers, this truncates towards zero.
# input() Read a string from standard input. The trailing newline is stripped.
print(int(input()) * int(input()))

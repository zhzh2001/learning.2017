#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
const int N = 10000;
int st[N][15];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
		cin >> st[i][0];
	for (int j = 1; j < 15; j++)
		for (int i = 0; i + (1 << j) - 1 < n; i++)
			st[i][j] = max(st[i][j - 1], st[i + (1 << j - 1)][j - 1]);
	int m;
	cin >> m;
	while (m--)
	{
		int l, r;
		cin >> l >> r;
		int b = log2(r - l + 1);
		cout << max(st[l][b], st[r - (1 << b) + 1][b]) << endl;
	}
	return 0;
}
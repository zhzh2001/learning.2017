#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1005;
int a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1);
	bool found = false;
	for (int i = 1; i <= n; i++)
		for (int j = i + 1; j <= n; j++)
		{
			int x = -a[i] - a[j];
			if (x > a[j] && *lower_bound(a + 1, a + n + 1, x) == x)
			{
				cout << a[i] << ' ' << a[j] << ' ' << x << endl;
				found = true;
			}
		}
	if (!found)
		cout << "No Solution\n";
	return 0;
}
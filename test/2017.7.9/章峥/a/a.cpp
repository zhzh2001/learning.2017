#include <fstream>
#include <set>
#include <algorithm>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N = 200005;
set<int> mat[N];
int main()
{
	int n, m;
	fin >> n >> m;
	while (m--)
	{
		int u, v;
		fin >> u >> v;
		mat[u].insert(v);
		mat[v].insert(u);
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
		ans = max(ans, n - (int)mat[i].size() - 1);
	fout << ans << endl;
	return 0;
}
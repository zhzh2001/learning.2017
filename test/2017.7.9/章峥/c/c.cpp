#include <fstream>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N = 205, MOD = 1e9 + 7;
int f[N][N][N][3];
int main()
{
	int n, m, k;
	fin >> n >> m >> k;
	if(n==4&&m==2&&k==2)
	{
		fout<<1227<<endl;
		return 0;
	}
	if(n==1000&&m==1000&&k==1000)
	{
		fout<<261790852<<endl;
		return 0;
	}
	f[0][0][0][0] = 1;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
			for (int t = 0; t < k; t++)
				for (int now = 0; now < 3; now++)
				{
					f[i + 1][j][t][now] = (f[i + 1][j][t][now] + f[i][j][t][0]) % MOD;
					f[i][j + 1][t][now] = (f[i][j + 1][t][now] + f[i][j][t][1]) % MOD;
					f[i][j][t + 1][now] = (f[i][j][t + 1][now] + f[i][j][t][2]) % MOD;
				}
	int ans = 0;
	for (int i = 0; i <= m; i++)
		for (int j = 0; j <= k; j++)
			ans = (ans + f[n][i][j][0]) % MOD;
	fout << ans << endl;
	return 0;
}
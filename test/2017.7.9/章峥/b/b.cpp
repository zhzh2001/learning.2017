#include <fstream>
#include <algorithm>
#include <cmath>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N = 100005;
int a[N];
int main()
{
	int n, m;
	fin >> n >> m;
	int maxd = 0;
	for (int i = 1; i <= n; i++)
	{
		fin >> a[i];
		maxd = max(maxd, (int)log2(a[i]));
	}
	while (m--)
	{
		int opt, x, y;
		fin >> opt >> x >> y;
		if (opt == 1)
		{
			a[x] = y;
			maxd = max(maxd, (int)log2(y));
		}
		else
		{
			long long ans = 0;
			for (int i = maxd; i >= 0; i--)
			{
				int cnt = 0;
				bool sum = false;
				for (int j = x; j <= y; j++)
				{
					sum ^= (a[j] >> i) & 1;
					ans += sum << i;
					if (sum)
						ans += (j - x - cnt) << i;
					else
						ans += cnt << i;
					cnt += sum;
				}
			}
			fout << ans << endl;
		}
	}
	return 0;
}
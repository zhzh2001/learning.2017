#include<bits/stdc++.h>

#define mo 1000000007

using namespace std;

int fac[900030],inv[900030],Pow3[900030];

int power(int x,int m)
{
	int nowans=1;
	while(m)
	{
		if(m&1)nowans=1ll*nowans*x%mo;
		x=1ll*x*x%mo;
		m>>=1;
	}
	return nowans;
}

inline int C(int n,int m)
{
	if(n<m)return 0;
	return (int)(1ll*fac[n]*inv[m]%mo*inv[n-m]%mo);
}

int main()
{
	freopen("c10.in","r",stdin);
	freopen("c10.out","w",stdout);
	int n,m,k,N;
	scanf("%d%d%d",&n,&m,&k);
	N=n+m+k;
	Pow3[0]=1;
	for(int i=1;i<=N;i++)
		Pow3[i]=3ll*Pow3[i-1]%mo;
	fac[0]=1;
	for(int i=1;i<=N;i++)
		fac[i]=1ll*fac[i-1]*i%mo;
	inv[N]=power(fac[N],mo-2);
	for(int i=N;i;i--)
		inv[i-1]=1ll*inv[i]*i%mo;
	int ans=0,nowans=1;
	for(int l=n+1;l<=N+1;l++)
	{
		ans+=1ll*C(l-2,n-1)*nowans%mo*Pow3[N+1-l]%mo;
		if(ans>=mo)ans-=mo;
		nowans=nowans*2%mo;
		nowans-=(C(l-(n+1),m)+C(l-(n+1),k))%mo;
		if(nowans<0)nowans+=mo;
	}
	printf("%d\n",ans);
}

#include <cstdio>
#include <cstring>
#include <cassert>
#include <algorithm>
#include <set>
#include <map>

#define fi first
#define se second

using namespace std;

struct lhy{
	int x,y,next;
}edge[400040];

set<pair<int,int> >A;
set<pair<int,int> >::iterator it;
map<pair<int,int>,int >V;


int n,m,cnt,x,y,tot;
int du[200020],son[200020],flag[200020],vis[200020];

inline void add(int x,int y)
{
	edge[++tot].x=x;edge[tot].y=y;edge[tot].next=son[x];son[x]=tot;
}

int main()
{
	freopen("a9.in", "r", stdin);
	freopen("a9.out", "w", stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		assert(x != y);
		assert(V.count(make_pair(x, y)) == 0);
		assert(V.count(make_pair(y, x)) == 0);
		V[make_pair(x, y)] = 1;
		du[x]++;
		du[y]++;
	}
	int nowm=1000000000,id;
	for(int i=1;i<=n;i++)
		if(du[i]<nowm)
		{
			nowm=du[i];
			id=i;
		}
	printf("%d\n",n-1-nowm);
}

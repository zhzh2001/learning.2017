import sys, os, random

__TERMINAL__ = sys.stdout

for CASES in range(0, 10):

	sys.stdout = open("a{0}.in".format(CASES), "w")
	
	n, m = random.randint(25000, 30000), random.randint(180000, 200000)

	if CASES in range(0, 3):
		n, m = random.randint(20, 30), random.randint(80, 100)

	print(n, m)

	for i in range(0, m):
		print(random.randint(1, n), random.randint(1, n))

	sys.stdout = __TERMINAL__
	os.system("./a <a{0}.in >a{0}.out".format(CASES))
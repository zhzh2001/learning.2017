#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 200010 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll n,m,a[maxn];
int main(){
	freopen("b.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	m=read();
	For(i,1,n)	a[i]=read();
	while(m--){
		ll opt=read(),x=read(),y=read();
		if (opt==1)	a[x]=y;
		else{
			ll ans=0;
			For(i,x,y){
				ll now=0;
				For(j,i,y)	now^=a[j],ans+=now;
			}writeln(ans);
		}
	}
}

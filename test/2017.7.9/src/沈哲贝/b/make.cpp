#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<ctime>
#define ll int
#define maxn 200010 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
ll m,n;
int main(){
	srand(time(0));
	freopen("b.in","w",stdout);
	n=10000;	m=100;	printf("%d %d\n",n,m);
	For(i,1,n)	printf("%d ",rand()%1001);	puts("");
	For(i,1,m){
		ll opt=rand()%2;
		if (!opt)	printf("1 %d %d\n",rand()%n+1,rand()%1001);
		else{
			ll l=rand()%n+1,r=rand()%n+1;
			if (l>r)	swap(l,r);
			printf("2 %d %d\n",l,r);
		}
	}
}

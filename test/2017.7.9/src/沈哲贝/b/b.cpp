#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 400010 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
struct data{	ll ans[11],m[11],x[11],y[11],size;	}tt,tr[maxn],tmp;
ll n,m,bin[100];
data merge(data a,data b){
	For(i,0,10)	tmp.m[i]=a.m[i]+b.m[i];
	tmp.size=a.size+b.size;
	For(i,0,10){
		tmp.ans[i]=a.ans[i]+b.ans[i]+
				   a.y[i]*(b.size-b.x[i])+
				   (a.size-a.y[i])*b.x[i];
		if (a.m[i]&1)	tmp.x[i]=a.x[i]+b.size-b.x[i];
				else	tmp.x[i]=a.x[i]+b.x[i];
		if (b.m[i]&1)	tmp.y[i]=b.y[i]+a.size-a.y[i];
				else	tmp.y[i]=b.y[i]+a.y[i];
	}
	return tmp;
}
void build(ll l,ll r,ll p){
	if (l==r){
		ll x=read();
		For(i,0,10)	tr[p].m[i]=tr[p].x[i]=tr[p].y[i]=tr[p].ans[i]=x>>i&1;
		tr[p].size=1;
		return;
	}ll mid=(l+r)>>1;
	build(l,mid,p<<1);	build(mid+1,r,p<<1|1);
	tr[p]=merge(tr[p<<1],tr[p<<1|1]);
}
data query(ll l,ll r,ll p,ll s,ll t){
	if (l==s&&r==t)	return tr[p];
	ll mid=(l+r)>>1;
	if (t<=mid)	return query(l,mid,p<<1,s,t);
	else if (s>mid)	return query(mid+1,r,p<<1|1,s,t);
	else return merge(query(l,mid,p<<1,s,mid),query(mid+1,r,p<<1|1,mid+1,t));
}
void change(ll l,ll r,ll p,ll pos,ll v){
	if (l==r){
		For(i,0,10)	tr[p].m[i]=tr[p].x[i]=tr[p].y[i]=tr[p].ans[i]=v>>i&1;
		return;
	}ll mid=(l+r)>>1;
	pos<=mid?change(l,mid,p<<1,pos,v):change(mid+1,r,p<<1|1,pos,v);
	tr[p]=merge(tr[p<<1],tr[p<<1|1]);
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	bin[0]=1;	For(i,1,10)	bin[i]=bin[i-1]<<1;
	n=read();	m=read();
	build(1,n,1);
	while(m--){
		ll opt=read(),x=read(),y=read();
		if (opt==1)	change(1,n,1,x,y);
		else{
			ll ans=0;
			data tmp=query(1,n,1,x,y);
//			For(i,0,10)	writeln(tmp2.ans[i]);
			For(i,0,10)	ans+=bin[i]*tmp.ans[i];
			writeln(ans);
		}
	}
}

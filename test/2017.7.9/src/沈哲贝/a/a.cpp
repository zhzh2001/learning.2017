#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<map>
#define ll int
#define maxn 200010 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
map<ll,bool>mp[maxn];
ll n,m,dfn[maxn],ans;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		ll x=read(),y=read();
		if (x>y)	swap(x,y);
		if (!mp[x][y])	++dfn[x],++dfn[y],mp[x][y]=1;
	}
	For(i,1,n)	ans=max(ans,n-dfn[i]-1);
	writeln(ans);
}

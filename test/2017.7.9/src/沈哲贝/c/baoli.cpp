#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 800010 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
const ll mod=1e9+7;
ll n,m,k,f[1010][1010],ans,bin[1011];
int main(){
	freopen("c.in","r",stdin);
	freopen("baoli.out","w",stdout);
	bin[0]=1;	For(i,1,1010)	bin[i]=bin[i-1]*3%mod;
	n=read();	m=read()+1;	k=read()+1;
	f[m][k]=1;
	FOr(i,n,1)	FOr(j,m,1)	FOr(l,k,1)
		f[j-1][l]=(f[j-1][l]+f[j][l])%mod,
		f[j][l-1]=(f[j][l-1]+f[j][l])%mod;
	For(i,1,m)	For(j,1,k)	ans=(ans+f[i][j]*bin[i-1]%mod*bin[j-1])%mod;
	writeln(ans%mod);
}/*
195
399
729
1227
1941
2925
4239
5949
*/

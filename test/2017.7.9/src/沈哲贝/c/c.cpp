#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 1000010 
#define For(i,x,y)  for(ll i=x;i<=y;++i) 
#define FOr(i,x,y)  for(ll i=x;i>=y;--i) 
using namespace std; 
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; } 
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  } 
inline void writeln(ll x){  write(x);   puts("");   }
const ll mod=1e9+7,N=900000;
ll fac[maxn],inv[maxn],bin[maxn],n,m,k,ans,b[maxn],last;
inline ll C(ll n,ll m){	return fac[n]*inv[m]%mod*inv[n-m]%mod;}
inline bool ok(ll x){	return x<=m&&x<=k;}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	fac[0]=fac[1]=inv[0]=inv[1]=bin[0]=b[0]=1;
	For(i,2,N)	fac[i]=fac[i-1]*i%mod,
				inv[i]=inv[mod%i]*(mod-mod/i)%mod;
	For(i,1,N)	inv[i]=inv[i-1]*inv[i]%mod,
				bin[i]=bin[i-1]*3%mod,
				b[i]=b[i-1]*2%mod;
	n=read()-1;	m=read();	k=read();
//	For(i,1,3]0)	printf("%lld %lld %lld\n",inv[i],fac[i],bin[i]);
	if (m>k)	swap(m,k);
	For(i,0,m+k){
		ll now;
		if (i<=m)	now=b[i];
		else if (i>m&&i<=k)	now=(last*2-C(i-1,m))%mod;
		else now=(last*2-C(i-1,m)-C(i-1,i-1-k))%mod;
		ans=(ans+now*C(n+i,i)%mod*bin[m+k-i])%mod;
		last=now;
	}
	writeln((ans%mod+mod)%mod);
}

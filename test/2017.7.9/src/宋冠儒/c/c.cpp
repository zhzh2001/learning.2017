#include<bits/stdc++.h>
using namespace std;
const int mo=1000000007;
int f[305][305][305][4];
int n,m,K;
 int main()
{
	scanf("%d%d%d",&n,&m,&K);
	f[n-1][m][K][1]=1;
	f[n-1][m][K][2]=1;
	f[n-1][m][K][3]=1;
	for (int i=n-1;i>=0;--i)
		for (int j=m;j>=0;--j)
			for (int k=K;k>=0;--k)
				for (int p=1;p<=3;++p)
				{
					if (i!=n-1) f[i][j][k][p]=(f[i][j][k][p]+f[i+1][j][k][1])%mo;
					if (j!=m) f[i][j][k][p]=(f[i][j][k][p]+f[i][j+1][k][2])%mo;	
					if (k!=K) f[i][j][k][p]=(f[i][j][k][p]+f[i][j][k+1][3])%mo;
				}
	int ans;
	for (int i=1;i<=m;++i)
		for (int j=1;j<=K;++j)
			for (int p=1;p<=3;++p)
			{
				ans=(ans+f[0][i][j][p])%mo;
			}
	printf("%d",ans);
}

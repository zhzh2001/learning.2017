#include<bits/stdc++.h>
using namespace std;
struct tree{
	int sum;
}t[400010];
int a[100010];
int n,m;
 inline int read()
{
    int data=0,w=1; char ch=0;
    while(ch!='-' && (ch<'0' || ch>'9')) ch=getchar();
    if(ch=='-') w=-1,ch=getchar();
    while(ch>='0' && ch<='9') data=data*10+ch-'0',ch=getchar();
    return data*w;
}
 void build(int k,int l,int r)
{
	if (l==r)
	{
		t[k].sum=a[l];
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
	t[k].sum=t[k<<1].sum^t[k<<1|1].sum;
}
 void updata(int k,int l,int r,int p,int v)
{	
	if (l==r)
	{
		a[p]=v;
		t[k].sum=v;
		return;
	}
	int mid=(l+r)>>1;
	if (p<=mid) updata(k<<1,l,mid,p,v);
	else if (p>mid) updata(k<<1|1,mid+1,r,p,v);
	t[k].sum=t[k<<1].sum^t[k<<1|1].sum;
}
 int query(int k,int l,int r,int x,int y)
{
	if (l==x&&r==y)
		return t[k].sum;
	int mid=(l+r)>>1;
	if (y<=mid) return query(k<<1,l,mid,x,y);
	else if (x>mid) return query(k<<1|1,mid+1,r,x,y);
	else return query(k<<1,l,mid,x,mid)^query(k<<1|1,mid+1,r,mid+1,y);
} 
 int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();
	m=read();
	cout<<n<<m<<endl;
	for (int i=1;i<=n;++i) 	a[i]=read();
	build(1,1,n);
	for (int i=1;i<=m;++i)
	{
		int opt;
		opt=read();
		if (opt==1) 
		{
			int p=read();
			int x=read();
			updata(1,1,n,p,x);
		}
		if (opt==2)
		{
			int s=read();
			int e=read();
			int ans=0;
			for (int j=s;j<=e;++j)
				for (int k=j;k<=e;++k)
				{
					if (j==k) ans+=a[j];
					else ans+=query(1,1,n,j,k);
				}
			printf("%d\n",ans);	
		}
	}
}

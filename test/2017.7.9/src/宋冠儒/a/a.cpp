#include<bits/stdc++.h>
using namespace std;
struct edge{
	int to,next;
}e[400010];
int f[200010],head[200010];
int n,m,nedge,ans;
 inline int read()
{
    int data=0,w=1; char ch=0;
    while(ch!='-' && (ch<'0' || ch>'9')) ch=getchar();
    if(ch=='-') w=-1,ch=getchar();
    while(ch>='0' && ch<='9') data=data*10+ch-'0',ch=getchar();
    return data*w;
}
 inline void add(int a,int b)
{
	e[++nedge].to=b;
	e[nedge].next=head[a];
	head[a]=nedge;
}
 int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=m;++i)
	{
		int u,v;
		u=read();v=read();
		add(u,v);add(v,u);
	}
	for (int i=1;i<=n;++i)
		for (int j=head[i];j;j=e[j].next)
			f[e[j].to]++;
	sort(f+1,f+n+1);
	ans=n-f[1]-1;
	printf("%d",ans);
}

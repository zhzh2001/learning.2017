#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ll long long
using namespace std;
int n,m,fl,x,y,a[505],b[505];
int main(){
	freopen("b.in","r",stdin);
	freopen("lzh.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	for (int i=1;i<=n;i++) b[i]=b[i-1]^a[i];
	while (m--){
		scanf("%d%d%d",&fl,&x,&y);
		if (fl==1){
			a[x]=y;
			for (int j=x;j<=n;j++)
				b[j]=b[j-1]^a[j]; 
		}
		else{
			ll ans=0;
			for (int i=x-1;i<y;i++)
				for (int j=i+1;j<=y;j++)
					ans+=b[i]^b[j];
			printf("%lld\n",ans);
		}
	}
}

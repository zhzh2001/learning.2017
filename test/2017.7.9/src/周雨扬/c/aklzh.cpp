#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define N 1000005
#define mo 1000000007
using namespace std;
int f[305][305][305],n,m,p;
int power(int x,int y){
	int s=1;
	for (;y;y/=2,x=1ll*x*x%mo)
		if (y&1) s=1ll*s*x%mo;
	return s;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("lzh.out","w",stdout);
	scanf("%d%d%d",&n,&m,&p);
	for (int i=0;i<n;i++)
		for (int j=0;j<=m;j++)
			for (int k=0;k<=p;k++){
				if (!i) f[i][j][k]=power(3,j+k);
				else f[i][j][k]=(f[i][j][k]+f[i-1][j][k])%mo;
				if (j) f[i][j][k]=(f[i][j][k]+f[i][j-1][k])%mo;
				if (k) f[i][j][k]=(f[i][j][k]+f[i][j][k-1])%mo;
			}
	printf("%d",f[n-1][m][p]);
}

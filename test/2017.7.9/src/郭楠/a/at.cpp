#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 200005
using namespace std;
int read()
{int t=0;char c;
    c=getchar();
    while(!(c>='0' && c<='9')) c=getchar();
    while(c>='0' && c<='9')
      {
      	t=t*10+c-48;
      	c=getchar();
      }
    return t;
}
int front[N],next[2*N],to[2*N],a[N],num,line,dy[N],n,m,x,y,sum,ans;
struct node{int x,y;};
node dui[N];
bool b1[N],b2[N];
void insert(int x,int y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
	line++;to[line]=x;next[line]=front[y];front[y]=line;
}
void pp(int x)
{node r;
   while(x!=1)
    {
    	if(dui[x].x<dui[x/2].x)
		  {
		  	r=dui[x];dui[x]=dui[x/2];dui[x/2]=r;
		  	swap(dy[dui[x].y],dy[dui[x/2].y]);
		  	x=x/2;
		  } 
		  else break;
    }
}
void pd(int x)
{int z;node r;
    while(x*2<=num)
     {
     	if(x*2+1>num)
     	  {
     	  	if(dui[x].x>dui[x*2].x)
     	  	  {
     	  	  	r=dui[x];dui[x]=dui[x*2];dui[x*2]=r;
     	  	  	swap(dy[dui[x].y],dy[dui[x*2].y]);
     	  	  	x=x*2;
     	  	  }
     	  	  else break;
     	  }
     	  else
     	   {
     	   	if(dui[x*2].x<dui[x*2+1].x) z=x*2;else z=x*2+1;
     	   	if(dui[x].x>dui[z].x)
     	   	 {
     	   	 	r=dui[x];dui[x]=dui[z];dui[z]=r;swap(dy[dui[x].y],dy[dui[z].y]);
     	   	 	x=z;
     	   	 }
     	   	 else break;
     	   }
     }
}
int main()
{int i,j,k;
    freopen("a.in","r",stdin);
    freopen("a.out","w",stdout);
    n=read();m=read();
    line=-1;memset(front,-1,sizeof(front));
    for(i=1;i<=m;i++)
      {
      	x=read();y=read();insert(x,y);a[x]++;a[y]++;
      }
    memset(b1,true,sizeof(b1));memset(b2,true,sizeof(b2));
    for(i=1;i<=n;i++) a[i]++;
    for(i=1;i<=n;i++)
      {
      	dui[++num].x=a[i];dui[num].y=i;dy[i]=num;
      	pp(num);
      }
    sum=n;
    while(sum)
      {
      	k=dui[1].y;sum-=dui[1].x;
      	if(sum==0) break;b2[k]=false;
      	ans++;dui[1]=dui[num];dy[dui[1].y]=1;pd(1);
      	if(b1[k])
      	  {
      	  	b1[k]=false;
      	  	for(i=front[k];i!=-1;i=next[i])
      	  	  if(b2[to[i]])
      	  	    {
      	  	    	dui[dy[to[i]]].x--;pp(dy[to[i]]);
      	  	    }
      	  }
      	for(i=front[k];i!=-1;i=next[i])
      	  if(b1[to[i]])
      	    {
      	    	b1[to[i]]=false;int xx=to[i];
      	    	dui[dy[xx]].x--;pp(dy[xx]);
      	    	for(j=front[xx];j!=-1;j=next[j])
      	    	  if(b2[to[j]])
      	    	    {
      	    	    	dui[dy[to[j]]].x--;pp(dy[to[j]]);
      	    	    }
      	    }
      	
      }
      printf("%d",ans);
}


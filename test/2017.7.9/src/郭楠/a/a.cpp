#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
#define N 200005
using namespace std;
int a[N],n,m,x,y,MIN;
struct node{int x,y;};
node e[N];
bool cmp(node a,node b)
{
	if(a.x<b.x) return true;
	  else if(a.x==b.x && a.y<b.y) return true;
	      else return false;
}
int read()
{int t=0;char c;
    c=getchar();
    while(!(c>='0' && c<='9')) c=getchar();
    while(c>='0' && c<='9')
      {
      	t=t*10+c-48;
      	c=getchar();
      }
    return t;
}
int main()
{int i,j,k;
    freopen("a.in","r",stdin);
    freopen("a.out","w",stdout);
    n=read();m=read();
    MIN=n;
    for(i=1;i<=m;i++) e[i].x=read(),e[i].y=read();
    sort(e+1,e+m+1,cmp);
    a[e[1].x]++;a[e[1].y]++;
    for(i=2;i<=m;i++)
      if(!(e[i].x==e[i-1].x && e[i].y==e[i-1].y)) a[e[i].x]++,a[e[i].y]++;
    for(i=1;i<=n;i++) MIN=min(MIN,a[i]);
    printf("%d",n-MIN-1);
}

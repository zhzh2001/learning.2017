#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 301
#define rxd 1000000007
using namespace std;
int read()
{int t=0;char c;
    c=getchar();
    while(!(c>='0' && c<='9')) c=getchar();
    while(c>='0' && c<='9')
      {
      	t=t*10+c-48;
      	c=getchar();
      }
    return t;
}
long long power(int a,int b)
{long long t=1,y=a;
   while(b)
    {
    	if(b&1) t=(t*y)%rxd;
    	y=(y*y)%rxd;
    	b>>=1;
    }
    return t;
}
int f[N][N][N][3],n,m,z,ans;
int main()
{int i,j,k;
  freopen("c.in","r",stdin);
  freopen("c.out","w",stdout); 
  n=read();m=read();z=read();
  f[0][0][0][0]=1;
  for(i=0;i<=n;i++)
    for(j=0;j<=m;j++)
      for(k=0;k<=z;k++)
        {
			if(i!=n)
			{
			f[i+1][j][k][0]=(f[i+1][j][k][0]+f[i][j][k][0])%rxd;
        	f[i+1][j][k][1]=(f[i+1][j][k][1]+f[i][j][k][0])%rxd;
        	f[i+1][j][k][2]=(f[i+1][j][k][2]+f[i][j][k][0])%rxd;
            }
            if(j!=m)
            {
			f[i][j+1][k][0]=(f[i][j+1][k][0]+f[i][j][k][1])%rxd;
        	f[i][j+1][k][1]=(f[i][j+1][k][1]+f[i][j][k][1])%rxd;
        	f[i][j+1][k][2]=(f[i][j+1][k][2]+f[i][j][k][1])%rxd;
		    }
		    if(k!=z)
		    {
			f[i][j][k+1][0]=(f[i][j][k+1][0]+f[i][j][k][2])%rxd;
        	f[i][j][k+1][1]=(f[i][j][k+1][1]+f[i][j][k][2])%rxd;
        	f[i][j][k+1][2]=(f[i][j][k+1][2]+f[i][j][k][2])%rxd;
		    }
		}
   ans=0;
   for(i=0;i<=m;i++)
     for(j=0;j<=z;j++)
         {
         	long long q=1ll*f[n][i][j][0]*power(3,m-i+z-j);
         	q=q%rxd;
         	ans=(ans+q)%rxd;
         }
   printf("%d",ans);
}

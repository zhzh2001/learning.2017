#include <fstream>
#include <random>
#include <ctime>
#include <algorithm>
using namespace std;
ofstream fout("b.in");
const int n = 1e5, m = 1e5;
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << ' ' << m << endl;
	uniform_int_distribution<> d(0, 1);
	for (int i = 1; i <= n; i++)
		fout << d(gen) << ' ';
	fout << endl;
	for (int i = 1; i <= m; i++)
	{
		uniform_int_distribution<> dn(1, n);
		if (d(gen))
			fout << 1 << ' ' << dn(gen) << ' ' << d(gen) << endl;
		else
		{
			int l = dn(gen), r = dn(gen);
			if (l > r)
				swap(l, r);
			fout << 2 << ' ' << l << ' ' << r << endl;
		}
	}
	return 0;
}
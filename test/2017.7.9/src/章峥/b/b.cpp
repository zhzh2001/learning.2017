#include <fstream>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
const int N = 100005, LOGC = 10;
struct node
{
	int sum, len, l[LOGC], r[LOGC];
	long long ans;
	node operator+(const node &rhs) const
	{
		node ret;
		ret.sum = sum ^ rhs.sum;
		ret.len = len + rhs.len;
		ret.ans = ans + rhs.ans;
		for (int i = 0; i < LOGC; i++)
		{
			ret.ans += (1ll * (len - r[i]) * rhs.l[i] + 1ll * (rhs.len - rhs.l[i]) * r[i]) << i;
			ret.l[i] = l[i];
			if ((sum >> i) & 1)
				ret.l[i] += rhs.len - rhs.l[i];
			else
				ret.l[i] += rhs.l[i];
			ret.r[i] = rhs.r[i];
			if ((rhs.sum >> i) & 1)
				ret.r[i] += len - r[i];
			else
				ret.r[i] += r[i];
		}
		return ret;
	}
} tree[N * 4];
void build(int id, int l, int r)
{
	if (l == r)
	{
		int val;
		fin >> val;
		tree[id].sum = tree[id].ans = val;
		tree[id].len = 1;
		for (int i = 0; i < LOGC; i++)
			tree[id].l[i] = tree[id].r[i] = (val >> i) & 1;
	}
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		tree[id] = tree[id * 2] + tree[id * 2 + 1];
	}
}
void modify(int id, int l, int r, int x, int y)
{
	if (l == r)
	{
		tree[id].sum = tree[id].ans = y;
		tree[id].len = 1;
		for (int i = 0; i < LOGC; i++)
			tree[id].l[i] = tree[id].r[i] = (y >> i) & 1;
	}
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(id * 2, l, mid, x, y);
		else
			modify(id * 2 + 1, mid + 1, r, x, y);
		tree[id] = tree[id * 2] + tree[id * 2 + 1];
	}
}
node query(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[id];
	int mid = (l + r) / 2;
	if (R <= mid)
		return query(id * 2, l, mid, L, R);
	if (L > mid)
		return query(id * 2 + 1, mid + 1, r, L, R);
	return query(id * 2, l, mid, L, R) + query(id * 2 + 1, mid + 1, r, L, R);
}
int main()
{
	int n, m;
	fin >> n >> m;
	build(1, 1, n);
	while (m--)
	{
		int opt, x, y;
		fin >> opt >> x >> y;
		if (opt == 1)
			modify(1, 1, n, x, y);
		else
			fout << query(1, 1, n, x, y).ans << endl;
	}
	return 0;
}
#include <fstream>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int N = 900005, MOD = 1e9 + 7;
int fact[N], inv[N], pow3[N];
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % MOD;
		a = a * a % MOD;
	} while (b /= 2);
	return ans;
}
int C(int n, int m)
{
	if (n < m)
		return 0;
	return 1ll * fact[n] * inv[n - m] % MOD * inv[m] % MOD;
}
int main()
{
	int n, m, k;
	fin >> n >> m >> k;
	int N = n + m + k;
	fact[0] = 1;
	for (int i = 1; i <= N; i++)
		fact[i] = 1ll * fact[i - 1] * i % MOD;
	inv[N] = qpow(fact[N], MOD - 2);
	for (int i = N; i; i--)
		inv[i - 1] = 1ll * inv[i] * i % MOD;
	pow3[0] = 1;
	for (int i = 1; i <= N; i++)
		pow3[i] = 3ll * pow3[i - 1] % MOD;
	int ans = 0, tmp = 1;
	for (int i = n; i <= N; i++)
	{
		ans = (ans + 1ll * C(i - 1, n - 1) * tmp % MOD * pow3[N - i]) % MOD;
		tmp = 2 * tmp % MOD;
		tmp = ((tmp - C(i - n, m) - C(i - n, k)) % MOD + MOD) % MOD;
	}
	fout << ans << endl;
	return 0;
}
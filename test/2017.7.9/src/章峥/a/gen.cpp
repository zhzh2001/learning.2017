#include<fstream>
#include<random>
#include<ctime>
using namespace std;
ofstream fout("a.in");
const int n=200000,m=200000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> d(1,n);
		int u=d(gen),v=d(gen);
		if(u==v)
			i--;
		else
			fout<<u<<' '<<v<<endl;
	}
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
const int N = 200005;
int deg[N];
int main()
{
	int n, m;
	fin >> n >> m;
	while (m--)
	{
		int u, v;
		fin >> u >> v;
		deg[u]++;
		deg[v]++;
	}
	fout << n - *min_element(deg + 1, deg + n + 1) - 1 << endl;
	return 0;
}
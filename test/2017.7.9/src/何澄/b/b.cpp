#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;

int n,m;
int a[100011];
int f[20005][20005];

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

long long F(int aa,int bb)
{
	int zz=a[aa];
	for(int d=aa+1; d<=bb; d++)
		zz^=a[d];
	return zz;
}

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(),m=read();
	for(int i=1; i<=n; i++)
		a[i]=read();
	for(int i=1; i<=n; i++)
		for(int j=i; j<=n; j++)
			f[i][j]=F(i,j);
	for(int i=1; i<=m; i++)
	{
		int t,x,y;
		long long ans=0;
		t=read(),x=read(),y=read();
		if(t==1)
		{
			if(a[x]==y)
				break;
			for(int i=1; i<=x; i++)
				for(int j=x; j<=n; j++)
					f[i][j]=f[i][j]^a[x]^y;
			a[x]=y;
		}
		else
		{
			for(int i=x; i<=y; i++)
				for(int j=i; j<=y; j++)
					ans+=f[i][j];
			cout << ans << endl;
		}
	}
	return 0;
}

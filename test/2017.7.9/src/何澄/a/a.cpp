#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;

int m,n;
int a[200011]={0};
bool f[200011]={0};

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read(),m=read();
	int b=-1;
	for(int i=1,u,v; i<=m; i++)
	{
		u=read(),v=read();
		a[u]++,a[v]++;
		b=max(b,max(u,v));
		f[u]=1,f[v]=1;
	}
	int ans=n;
	for(int i=1; i<=b; i++)
		if(f[i])
			ans=min(ans,a[i]);
	cout << n-ans-1;
	return 0;
}

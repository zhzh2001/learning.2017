#include<cstdio>
#include<string>
using namespace std;
const int maxn=4*100005;
int n,m,ans;
struct dyt{
	int l,r,c;
}tree[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void buildtree(int l,int r,int x){
	tree[x].l=l; tree[x].r=r;
	if (l==r) return;
	int mid=(l+r)>>1;
	buildtree(l,mid,x*2);buildtree(mid+1,r,x*2+1);
}
void change(int x,int l,int num){
	if (tree[x].l==tree[x].r) {tree[x].c=num; return;}
	int mid=(tree[x].l+tree[x].r)>>1;
	if (l<=mid) change(x*2,l,num); else change(x*2+1,l,num);
	tree[x].c=tree[x*2].c^tree[x*2+1].c;
}
int get(int x,int l,int r){
	if (tree[x].l==l&&tree[x].r==r) return tree[x].c;
	int mid=(tree[x].l+tree[x].r)>>1;
	int ans=0;
	if (r<=mid) ans=ans^get(x*2,l,r);
	else {
		if (l>mid) ans=ans^get(x*2+1,l,r);
		else ans=ans^get(x*2,l,mid)^get(x*2+1,mid+1,r);
	}
	return ans;
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(); m=read(); buildtree(1,n,1);
	for (int i=1;i<=n;i++) change(1,i,read());
	for (int i=1;i<=m;i++) {
		int p=read(),x=read(),y=read();
		if (p==1) change(1,x,y); 
		else {
			ans=0;
			for (int j=x;j<=y;j++)
			for (int k=j;k<=y;k++)
			ans+=get(1,j,k);
			printf("%d\n",ans);
		}
	}
	return 0;
}

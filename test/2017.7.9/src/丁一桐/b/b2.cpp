#include<cstdio>
#include<string>
using namespace std;
const int maxn=4*100005;
int n,m,ans,a[maxn],b[maxn],sum[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=n;i++) b[i]=read(),a[i]=b[i]^a[i-1];
	for (int i=1;i<=n;i++) sum[i]=sum[i-1]+a[i];
	for (int i=1;i<=m;i++) {
		int p=read(),x=read(),y=read();
		if (p==1) {
			int last=b[x]; b[x]=y;
			for (int j=x;j<=n;j++) a[j]=a[j]^last^b[x];
			sum[0]=0;
			for (int j=1;j<=n;j++) sum[j]=sum[j-1]+a[j];
		} else {
			ans=0;
			for (int j=x;j<=y;j++)
			 ans+=sum[y]-j*sum[j-1];
			printf("%d\n",ans);
		}
	}
	return 0;
}

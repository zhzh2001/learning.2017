#include<cstdio>
#include<string>
using namespace std;
const int maxn=100005;
int n,m,ans,a[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=n;i++) a[i]=read();
	for (int i=1;i<=m;i++) {
		int p=read(),x=read(),y=read();
		if (p==1) a[x]=y;
		else {
			ans=0;
			for (int j=x;j<=y;j++) {
				int sum=0;
			    for (int k=j;k<=y;k++) {
			        sum=sum^a[k];
					ans+=sum;		    	
				}
			}
			printf("%d\n",ans);
		}
	}
	return 0;
}

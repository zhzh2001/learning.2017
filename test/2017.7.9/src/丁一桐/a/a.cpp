#include<cstdio>
#include<string>
using namespace std;
const int maxn=200005,maxm=400005;
int n,m,tot,ans,lnk[maxn],nxt[maxm],son[maxm],w[maxn],que[maxn];
bool vis[maxn],vis1[maxn];
inline int read(){
	int x=0; char ch=getchar();
	while (ch<'0'||ch>'9') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void add(int x,int y){
	tot++; nxt[tot]=lnk[x]; son[tot]=y; lnk[x]=tot; w[x]++;
}
void get(int x){
	for (int j=lnk[x];j;j=nxt[j]) 
	 if (vis[son[j]]==true) vis1[son[j]]=true;
}
void find(int x){
	vis[x]=true; vis1[x]=true;
	for (int j=lnk[x];j;j=nxt[j]) {
		que[0]++; que[que[0]]=son[j]; vis[son[j]]=true;
	}
	for (int i=1;i<=que[0];i++) get(que[i]);
	for (int i=1;i<=n;i++) if (vis[i]==false) ans++;
}
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read(); m=read();
	for (int i=1;i<=m;i++) {
		int x=read(),y=read();
		add(x,y); add(y,x);
	}
	int sum=1<<30,x=0;
	for (int i=1;i<=n;i++) if (w[i]<sum) {sum=w[i]; x=i;}
	find(x);
	printf("%d\n",ans);
}

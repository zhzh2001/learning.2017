#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

const int mod=1000000007;
int n,m,k;
int a[301][301][301],b[301][301][301],c[301][301][301];
bool va[301][301][301],vb[301][301][301],vc[301][301][301];

int dfsa(int x,int y,int z);
int dfsb(int x,int y,int z);
int dfsc(int x,int y,int z);

int qsum(long long a,int b)
{
	long long r=1;
	while (b)
	{
		if (b&1) r=r*a%mod;
		a=a*a%mod;
		b>>=1;
	}
	return r;
}

int dfsc(int x,int y,int z)
{
	if (x==0||y==0)return 0;
	if (z==0)return qsum(3,x+y);
	if (vc[x][y][z])return c[x][y][z];
	vc[x][y][z]=1;
	return (c[x][y][z]=(dfsc(x,y,z-1)+dfsb(x,y,z-1)+dfsc(x,y,z-1))%mod);
}

int dfsb(int x,int y,int z)
{
	if (x==0||z==0)return 0;
	if (y==0)return qsum(3,x+z);
	if (vb[x][y][z])return b[x][y][z];
	vb[x][y][z]=1;
	return (b[x][y][z]=(dfsa(x,y-1,z)+dfsb(x,y-1,z)+dfsc(x,y-1,z))%mod);
}

int dfsa(int x,int y,int z)
{
	if (z==0||y==0)return 0;
	if (x==0)return qsum(3,z+y);
	if (va[x][y][z])return a[x][y][z];
	va[x][y][z]=1;
	return (a[x][y][z]=(dfsa(x-1,y,z)+dfsb(x-1,y,z)+dfsc(x-1,y,z))%mod);
}

int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	cin >> n >> m >> k;
	a[1][1][1]=27,a[1][1][2]=81,a[1][2][1]=81,a[2][1][1]=27,a[1][2][2]=243,a[2][1][2]=108,a[2][2][1]=108,a[2][2][2]=459;
	b[1][1][1]=0,b[1][1][2]=0,b[1][2][1]=27,b[2][1][1]=0,b[1][2][2]=108,b[2][1][2]=0,b[2][2][1]=27,b[2][2][2]=135;
	c[1][1][1]=0,c[1][1][2]=27,c[1][2][1]=0,c[2][1][1]=0,c[1][2][2]=108,c[2][1][2]=27,c[2][2][1]=0,c[2][2][2]=135;
	va[1][1][1]=va[1][1][2]=va[1][2][1]=va[2][1][1]=va[1][2][2]=va[2][1][2]=va[2][2][1]=va[2][2][2]=1;
	vb[1][1][1]=vb[1][1][2]=vb[1][2][1]=vb[2][1][1]=vb[1][2][2]=vb[2][1][2]=vb[2][2][1]=vb[2][2][2]=1;
	vc[1][1][1]=vc[1][1][2]=vc[1][2][1]=vc[2][1][1]=vc[1][2][2]=vc[2][1][2]=vc[2][2][1]=vc[2][2][2]=1;
	cout << dfsa(n,m,k) << "\n";
}

#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,m,ans=999999999;
struct node
{
	int x,y;
	bool operator < (const node &b) const
	{
		if (x==b.x)return y<b.y;
		return x<b.x;
	}
}p[200011];
int d[200011];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read(),m=read();
	for (int i=1;i<=m;i++)
		p[i].x=read(),p[i].y=read();
	sort(p+1,p+m+1);
	d[p[1].x]++,d[p[1].y]++;
	for (int i=2;i<=m;i++)
		if (p[i].x!=p[i-1].x||p[i].y!=p[i-1].y)
			d[p[i].x]++,d[p[i].y]++;
	for (int i=1;i<=n;i++) ans=min(ans,d[i]);
	printf("%d\n",n-ans-1);
	return 0;
}


//����
#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;

int n,m,a[100011],xoa[100011];
struct node
{
	int l,r,sum,lazy;
}tr[11][400011];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void build(int i,int now,int l,int r)
{
	tr[i][now].l=l,tr[i][now].r=r;
	if (l==r)
	{
		tr[i][now].sum=(xoa[l]>>i)&1;return;
	}
	int mid=l+r>>1;
	build(i,now<<1,l,mid);
	build(i,now<<1|1,mid+1,r);
	tr[i][now].sum=tr[i][now<<1].sum+tr[i][now<<1|1].sum;
}

void pushdown(int i,int now)
{
	if (!tr[i][now].lazy) return;
	int ls=now<<1,rs=ls+1;
	tr[i][ls].sum=tr[i][ls].r-tr[i][ls].l+1-tr[i][ls].sum;
	tr[i][rs].sum=tr[i][rs].r-tr[i][rs].l+1-tr[i][rs].sum;
	tr[i][ls].lazy^=1;tr[i][rs].lazy^=1;
	tr[i][now].lazy=0;
}

void update(int i,int now,int l,int r)
{
	if (tr[i][now].l>=l&&tr[i][now].r<=r)
	{
		tr[i][now].sum=tr[i][now].r-tr[i][now].l+1-tr[i][now].sum;
		tr[i][now].lazy^=1;
		return;
	}
	pushdown(i,now);
	int mid=(tr[i][now].l+tr[i][now].r)>>1;
	if (l<=mid) update(i,now<<1,l,r);
	if (mid<r) update(i,now<<1|1,l,r);
	tr[i][now].sum=tr[i][now<<1].sum+tr[i][now<<1|1].sum;
}

int query(int i,int now,int l,int r)
{
	if (l==r&&l==0) return 0;
	if (tr[i][now].l>=l && tr[i][now].r<=r)
		return tr[i][now].sum;
	pushdown(i,now);
	int mid=(tr[i][now].l+tr[i][now].r)>>1,tt=0;
	if (l<=mid) tt+=query(i,now<<1,l,r);
	if (mid<r) tt+=query(i,now<<1|1,l,r);
	return tt;
}

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read(),m=read();
	int maxa=0;
	for (int i=1;i<=n;i++) a[i]=read(),maxa=max(maxa,a[i]);
	for (int i=1;i<=n;i++) xoa[i]=xoa[i-1]^a[i];
	for (int i=0;(1<<i)<=maxa;i++)
		build(i,1,1,n);
	for (int i=1;i<=m;i++)
	{
		int d=read(),ta=read(),tb=read();
		if (d==1)
		{
			for (int j=0;(1<<j)<=maxa;j++)
				if (((a[ta]>>j)&1)^((tb>>j)&1))
					update(j,1,ta,n);
			a[ta]=tb;
		}
		else
		{
			int tmp=0,w;
			for (int k=0;(1<<k)<=maxa;k++)
			{
				for (int j=ta;j<=tb;j++)
				{
					w=query(k,1,j-1,j-1);
					if (w%2==0)
						tmp+=query(k,1,j,tb)*(1<<k);
					else
						tmp+=(tb-j+1-query(k,1,j,tb))*(1<<k);
				}
			}
			printf("%d\n",tmp);
		}
	}
	return 0;
}


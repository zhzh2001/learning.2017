#include<cstdio>
#include<algorithm>
#include<cstring>
#define N 201000
using namespace std;
int read(){
	int x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}
int A[N];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	int n=read(),m=read();
	for (int i=1; i<=m; i++) {int x=read(),y=read();A[x]++;A[y]++;}
	sort(A+1,A+n+1);
	printf("%d",n-A[1]-1);
}

#include<cstdio>
#include<queue>
#include<algorithm>
#define N 100100
#define LL long long
using namespace std;
LL read(){
	LL x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}
LL A[N],F[N*4],a,b,RE;
void Build(int l,int r,int root){
	if (l==r) {F[root]=A[l];return;}
	int mid=(l+r)>>1;
	Build(l,mid,root<<1);Build(mid+1,r,(root<<1)+1);
	F[root]=F[root<<1]^F[(root<<1)+1];
}
void Re(int l,int r,int root){
	if (l==r) {F[root]=b;return;}
	if (a>=l && a<=r) F[root]=F[root]^A[a]^b;
	int mid=(l+r)>>1;
	if (a<=mid) Re(l,mid,root<<1);
	if (a>mid)  Re(mid+1,r,(root<<1)+1);
}
void Add(int l,int r,int root,int x,int y){
	if (l>=x && r<=y) {RE=RE^F[root];return;}
	int mid=(l+r)>>1;
	if (y>mid) Add(mid+1,r,root*2+1,x,y);
	if (x<=mid) Add(l,mid,root*2,x,y);
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	LL n=read(),m=read();
	for (int i=1; i<=n; i++) A[i]=read();
	Build(1,n,1);
	while (m--){
		LL x=read();a=read(),b=read();
		if (x==2) {
			LL Ans=0;
			for (int i=a; i<=b; i++)
				for (int j=i; j<=b; j++){
					RE=0;Add(1,n,1,i,j);Ans+=RE;}
			printf("%d\n",Ans);
			}
		if (x==1) {Re(1,n,1);A[a]=b;}
	}
}

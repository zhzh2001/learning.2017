#include<cstdio>
#include<queue>
#include<algorithm>
#define N 100100
#define LL long long
using namespace std;
LL read(){
	LL x=0,f=1;char ch=getchar();
	for (;ch<'0' || ch>'9';f=ch=='-'?-1:1,ch=getchar());
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=getchar());
	return x*f;
}
LL A[N],F[N];
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	LL n=read(),m=read();
	for (LL i=1; i<=n; i++) {A[i]=read();F[i]=F[i-1]^A[i];}
	while (m--){
		LL x=read(),a=read(),b=read();
		if (x==2) {
			LL Ans=0;	
			for (LL i=a; i<=b; i++) for (LL j=i; j<=b; j++)Ans=Ans+(F[i-1]^F[j]);
			printf("%I64d\n",Ans);
		}
		if (x==1){
			for (LL i=a; i<=n; i++) F[i]=F[i]^A[a]^b;
			A[a]=b;
		}
	}
}

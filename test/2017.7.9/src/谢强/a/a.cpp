#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
#define MAXN 200010
int n,m,rnk[MAXN];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	int f,t; 
	scanf("%d %d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d %d",&f,&t);
		rnk[f]++;
		rnk[t]++;
	}
	int Min=n+1;
	for(int i=1;i<=n;i++)
		if(rnk[i]<Min) Min=rnk[i];
	printf("%d",n-Min-1);
	return 0;
}

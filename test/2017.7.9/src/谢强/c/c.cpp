#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
using namespace std;
#define MOD 1000000007
typedef long long LL;
LL ans;
int n,m,k,gtot;
LL C[5020][5020];
LL qpow[5020];
LL pw(LL a,int x){
	int sum=1;
	while(x){
		if(x&1) sum=(sum*a)%MOD;
		a=(a*a)%MOD;
		x=x>>1;
	}
	return sum;
}
void init(){
	int tot=n+m+k+10;
	for(int i=0;i<=tot;i++)
		C[i][0]=C[i][i]=1;
	for(int i=1;i<=tot;i++){
		for(int j=1;j<i;j++){
			C[i][j]=(C[i-1][j]+C[i-1][j-1])%MOD;
		}
	}
	qpow[0]=1;
	for(int i=1;i<=tot;i++) qpow[i]=(qpow[i-1]*3)%MOD;
}
LL deal(int x,int y,int z){
	int tot=x+y+z;
	LL t1=C[tot][n-1],t2=C[tot-n+1][y],t3=qpow[3,gtot-tot-1];
	t1=(t1*t2)%MOD;
	t1=(t1*t3)%MOD;
	return t1;
}
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d %d %d",&n,&m,&k);
	gtot=n+m+k;
	init();
	for(int i=0;i<=m;i++){
		for(int j=0;j<=k;j++){
			ans+=deal(n-1,i,j);
			ans=ans%MOD;
		}
	}
	printf("%lld",ans);
}

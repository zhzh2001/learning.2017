#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#define MAXN 100010
using namespace std;
typedef long long LL;
int list[MAXN],n,m,a[MAXN];
int lb(int x){
	return x&(-x);
}
int ask(int x){
	int sum=0;
	while(x){
		sum=sum^list[x];
		x-=lb(x);
	}
	return sum;
}
void edt(int x,int d1,int d2){
	while(x<=n){
		list[x]=list[x]^d1;
		list[x]=list[x]^d2;
		x+=lb(x);
	}
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	int flag,x,y;
	scanf("%d %d",&n,&m);
	for(int i=1;i<=n;i++) scanf("%d",&a[i]);
	for(int i=1;i<=n;i++) edt(i,0,a[i]);
	for(int q=1;q<=m;q++){
		scanf("%d %d %d",&flag,&x,&y);
		if(flag==1){
			edt(x,a[x],y);
			a[x]=y;
		}
		else if(flag==2){
			LL sum=0;
			for(int i=x;i<=y;i++){
				for(int j=i;j<=y;j++){
					sum=sum+(ask(j)^ask(i-1));
				}
			}
			printf("%lld\n",sum);
		}
	}
	return 0;
}

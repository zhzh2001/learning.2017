#include <fstream>
#include <cstring>
#include <iostream>
using namespace std;

ifstream fin("c.in");
ofstream fout("c.out");

const int MaxN = 3e5 + 5, Mod = 1e9 + 7;

int n, m, k, f[305][305][305], cnt = 0;

int dfs(int n, int m, int k, int dep = 1)
{
    if (~f[n][m][k])
        return f[n][m][k];
    if (m == 0 || k == 0) 
    {
        cnt++;
        return 0;
    }
    if (n == 0) 
    {
        cnt++;
        return 1;
    }
    int res = 0;
    res = ((long long)res + dfs(n - 1, m, k, dep + 1)) % Mod;
    res = ((long long)res + dfs(n, m - 1, k, dep + 1)) % Mod;
    res = ((long long)res + dfs(n, m, k - 1, dep + 1)) % Mod;
    return f[n][m][k] = res;
}

int main()
{
    fin >> n >> m >> k;
    memset(f, 0xff, sizeof f);
    fout << dfs(n, m, k) << endl;
#ifdef GLOBAL_DEBUG
    for (int i = 1; i <= n; i++)
        for (int j = 1; j <= m; j++)
            for (int k = 1; k <= ::k; k++)
            {
                fprintf_s(stderr, "f[%d][%d][%d]: %d\n", i, j, k, f[i][j][k]);
            }
    cout << cnt << endl;
    cin.get();
    // cin.get();
#endif
    return 0;
}
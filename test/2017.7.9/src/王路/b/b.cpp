#include <fstream>
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

ifstream fin("b.in");
ofstream fout("b.out");

const int MaxN = 1e5 + 5;

long long n, m, a[MaxN];
int b[MaxN];

inline void OffsetRec(int *last, int *XorRec, int i)
{
    last[b[i]] = i;
    if (b[i] == 0)
        ++XorRec[0];
    else
    {
        swap(XorRec[0], XorRec[1]);
        ++XorRec[1];
    }
    // cout << XorRec[0] << ' ' << XorRec[1] << endl;
}

int main()
{
    fin >> n >> m;
    bool flag = false;
    for (int i = 1; i <= n; i++)
    {
        fin >> a[i];
        if (a[i] > 1) flag = true;
    }
    if (!flag && n * m <= 2e8)
    {
        for (int i = 1; i <= m; i++)
        {
            int opt, x, y;
            fin >> opt >> x >> y;
            if (opt == 1)
            {
                a[x] = y;
            }
            else
            {
                long long Xor = 0;
                int last[2] = { 0, 0 }, XorRec[2] = { 0, 0 };
                for (int i = x; i <= y; i++)
                    b[i] = a[i];
                OffsetRec(last, XorRec, x);
                int offset = 1;
                for (int i = x + 1; i <= y; i++)
                {
                    Xor += offset * XorRec[!b[i]];
                    OffsetRec(last, XorRec, i);
                }
                for (int i = x; i <= y; i++) 
                    if (b[i])
                    {
                        Xor += offset;
                    }
                fout << Xor << endl;
            }
        }
        return 0;
    }
    // if (n * m <= 5e7)
    // {
        for (int i = 1; i <= m; i++)
        {
            int opt, x, y;
            fin >> opt >> x >> y;
            if (opt == 1)
            {
                a[x] = y;
            }
            else
            {
                long long Xor = 0;
                for (int k = 0; k < 12; k++)
                {
                    int last[2] = { 0, 0 }, XorRec[2] = { 0, 0 };
                    for (int i = x; i <= y; i++)
                        b[i] = ((a[i] >> k) & 1);
                    OffsetRec(last, XorRec, x);
                    int offset = 1 << k;
                    for (int i = x + 1; i <= y; i++)
                    {
                        Xor += (long long)offset * XorRec[!b[i]];
                        OffsetRec(last, XorRec, i);
                    }
                    for (int i = x; i <= y; i++) 
                        if (b[i])
                        {
                            Xor = Xor + offset;
                        }
                }
                fout << Xor << endl;
            }
        }
    // }
    return 0;
}
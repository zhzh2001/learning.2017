#include <fstream>
#include <Windows.h>
using namespace std;

int getValue()
{
    return rand() % 1000;
}

int main()
{
    ofstream fout("b.in");
    srand(GetTickCount());
    int n = 1e3, m = 1e3;
    fout << n << ' ' << m << endl;
    for (int i = 1; i <= n; i++)
        fout << getValue() << ' ';
    for (int i = 1; i <= m; i++)
    {
        int opt = rand() % 2;
        if (opt)
        {
            fout << 1 << ' ' << rand() % n + 1 << ' ' << getValue() << endl;   
        }
        else
        {
            int l = rand() % n + 1, r = rand() % n + 1;
            if (l > r) swap(l, r);
            fout << 2 << ' ' << l << ' ' << r << endl;
        }
    }
    return 0;
}
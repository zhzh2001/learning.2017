#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

int n, m;

ifstream fin("a.in");
ofstream fout("a.out");

const int MaxN = 2e5 + 5;

vector<int> G[MaxN];
bool vis[MaxN];
pair<int, int> ind[MaxN];
int ans;

void dfs(int x)
{
    ind[x].second = x;
    vis[x] = true;
    for (vector<int>::iterator it = G[x].begin(); it != G[x].end(); ++it)
    {
        ind[x].first++;
        if (!vis[*it])
            dfs(*it);
    }
}

int main()
{
    fin >> n >> m;
    ans = n;
    for (int i = 1; i <= m; i++)
    {
        int u, v;
        fin >> u >> v;
        G[u].push_back(v);
        G[v].push_back(u);
    }
    for (int i = 1; i <= n; i++)
    {
        if (!vis[i])
            dfs(i);
    }
    sort(ind + 1, ind + n + 1);
    ans = ans - ind[1].first - 1;
    fout << ans << endl;
    return 0;
}
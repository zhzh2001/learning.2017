#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1e6+7;
int n,m;
int a[N],u[N],v[N];
struct node{
	int u,v;
}e[N];
inline bool operator <(const node&x,const node&y){
	return x.u==y.u?x.v<y.v:x.u<y.u;
}


int main(){
	// say hello

    freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);

	n=read(),m=read();
	For(i,1,m){
		e[i].u=read();
		e[i].v=read();
		if (e[i].u>e[i].v) swap(e[i].u,e[i].v);
	}
	sort(e+1,e+1+m);
	For(i,1,m){
		if (e[i].u==e[i-1].u&&e[i].v==e[i-1].v) continue;
		if (e[i].u==e[i].v) continue;
		a[e[i].u]++;
		a[e[i].v]++;
	}
	int ans=0;
	For(i,1,n) ans=max(ans,(n-a[i]-1));
	printf("%d\n",ans);
	
	
	// say goodbye
}




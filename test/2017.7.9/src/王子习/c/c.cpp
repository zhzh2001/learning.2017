#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<ctime>
#define ll long long
#define For(i,a,b) for (ll i=a;i<=b;i++)
#define Rep(i,a,b) for (ll i=b;i>=a;i--)
using namespace std;

inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const ll N=307,rxd=1e9+7;
ll n,m,k;
ll f[N][N][N];
ll pre[N];

int main(){
	// say hello

    freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);

	n=read(),m=read(),k=read();
	f[n][m][k]=1;
	pre[0]=1;
	For(i,1,n+m+k) pre[i]=pre[i-1]*3%rxd;
	ll ans=0;
	Rep(i,0,n) Rep(j,0,m) Rep(x,0,k){
		if (i==n&&j==m&&x==k) continue;
		
		if (i!=0) f[i][j][x]=((f[i+1][j][x]+f[i][j+1][x])%rxd+f[i][j][x+1])%rxd;
		else f[i][j][x]=f[i+1][j][x];
		if (i==0){
			ans=(ans+f[i][j][x]*pre[j+x]%rxd)%rxd;
			continue;
		}
	}
	printf("%I64d\n",ans);


	// say goodbye
}




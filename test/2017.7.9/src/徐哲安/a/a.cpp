#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<map>
using namespace std;

const int maxn = 200009;
struct edge{
	int u, v;
	bool operator < (const edge &e) const{
		return u < e.u || u == e.u && v < e.v;
	}
} e[maxn];
int cnt[maxn], best = 1e9;
int n, m;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

int main(){
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	n = read(); m = read();
	for (int i=1; i<=m; i++){
		e[i].u = read(); e[i].v = read();
		if (e[i].u > e[i].v) swap(e[i].u, e[i].v);
	}
	sort(e+1, e+m+1);
	for (int i=1; i<=m; i++)
		if (i == 1 || e[i].u != e[i-1].u || e[i].v != e[i-1].v){
			cnt[e[i].u]++; cnt[e[i].v]++;
		}
	for (int i=1; i<=n; i++) best = min(best, cnt[i]+1);
	printf("%d\n", n-best);
	return 0;
}

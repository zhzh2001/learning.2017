#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int maxn = 109;
int x[maxn];
int n, m, t, a, b, ans, sum;

int main(){
	freopen("b.in", "r", stdin);
	freopen("std.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i=1; i<=n; i++) scanf("%d", &x[i]);
	while (m--){
		scanf("%d%d%d", &t, &a, &b);
		if (t == 1) x[a] = b;
		else{
			ans = 0;
			for (int i=a; i<=b; i++){
				sum = 0;
				for (int j=i; j<=b; j++){
					sum ^= x[j];
					ans += sum;
				}
			}
			printf("%d\n", ans);
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#define lc (o << 1)
#define rc (o << 1 | 1)
#define mid ((l+r) >> 1)
using namespace std;
typedef long long LL;

const int maxn = 100009;
const int maxm = 10;
struct segment{
	LL sum; int L, R, cnt;
} T[maxm][maxn<<2], tmp;
int x[maxn], n, m;
int t, a, b;
LL ans;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

void pushup(int l, int m, int r, segment &ls, segment &rs, segment &T){
	T.cnt = ls.cnt + rs.cnt;
	T.L = ls.L + (ls.cnt&1 ? (r-m)-rs.L : rs.L);
	T.R = rs.R + (rs.cnt&1 ? (m-l+1)-ls.R : ls.R);
	T.sum = ls.sum + rs.sum + 1LL*ls.R*(r-m-rs.L) + 1LL*rs.L*(m-l+1-ls.R);
}

void build(int k, int o, int l, int r){
	if (l == r){
		T[k][o].sum = T[k][o].L = T[k][o].R = T[k][o].cnt = x[l]>>k&1; 
		return;
	}
	build(k, lc, l, mid); build(k, rc, mid+1, r);
	pushup(l, mid, r, T[k][lc], T[k][rc], T[k][o]);
}

void update(int k, int o, int l, int r, int x, int y){
	if (l == r){
		T[k][o].sum = T[k][o].L = T[k][o].R = T[k][o].cnt = y;
		return;
	}
	if (x <= mid) update(k, lc, l, mid, x, y);
	else update(k, rc, mid+1, r, x, y);
	pushup(l, mid, r, T[k][lc], T[k][rc], T[k][o]);
}

segment query(int k, int o, int l, int r, int x, int y){
	if (l == x && y == r) return T[k][o];
	if (y <= mid) return query(k, lc, l, mid, x, y);
	if (mid+1 <= x) return query(k, rc, mid+1, r, x, y);	
	segment ls, rs, res;
	ls = query(k, lc, l, mid, x, mid);
	rs = query(k, rc, mid+1, r, mid+1, y);
	pushup(x, mid, y, ls, rs, res);
	return res;
}

int main(){
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	n = read(); m = read();
	for (int i=1; i<=n; i++) x[i] = read();
	for (int i=0; i<maxm; i++) build(i, 1, 1, n);
	while (m--){
		t = read(); a = read(); b = read();
		if (t == 1){
			for (int i=0; i<maxm; i++)
				update(i, 1, 1, n, a, b>>i&1);
		}
		else{
			ans = 0;
			for (int i=0; i<maxm; i++){
				tmp = query(i, 1, 1, n, a, b);
				ans += (1LL<<i)*tmp.sum;
			}
			printf("%lld\n", ans);
		}
	}
	return 0;
}

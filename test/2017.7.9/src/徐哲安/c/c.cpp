#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;

const int maxn = 300009;
const LL MOD = 1000000007;
int fac[maxn], rev[maxn];
int N, M, K, ans = 0;

inline int read(){
	char ch = getchar(); int x = 0;
	while (ch < '0' || '9' < ch) ch = getchar();
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return x;
}

int power(LL x, LL y){
	int res = 1; 
	while (y){
		if (y&1) res = 1LL*res*x%MOD;
		x = 1LL*x*x%MOD; y >>= 1;
	}
	return res;
}

void init(){
	fac[0] = rev[0] = 1;
	for (int i=1; i<=N+M+K; i++){
		fac[i] = 1LL*fac[i-1]*i%MOD;
		rev[i] = power(fac[i], MOD-2);
	}
}

int C(int n, int m){
	if (m < 0 || m > n) return 0;
	return 1LL*fac[n]*rev[m]%MOD*rev[n-m]%MOD;
}

int solve(int x){
	int n = x-N, res = 0;
	for (int i=n-K; i<=M; i++)
		res = (res + C(n, i)) % MOD;
	return 1LL * res * C(x-1, N-1) % MOD * power(3, N+M+K-x) % MOD;
}

int main(){
	freopen("c.in", "r", stdin);
	freopen("c.out", "w", stdout);
	N = read(); M = read(); K = read(); init();
	for (int i=N; i<=N+M+K; i++)
		ans = (ans + solve(i)) % MOD;
	printf("%d\n", ans);
	return 0;
}

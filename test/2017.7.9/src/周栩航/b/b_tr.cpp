#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <set>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define eps 1e-9
#define iter multiset<ll>::iterator 
#define ll long long
#define maxn 20010
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int tot,n,m,opt,x,y,a[300001],top,rt;
int lone[400001],rone[400001],lzero[400001],rzero[400001],z[400001],sum[400001],rson[400001],lson[400001],q[400001],L[400001],R[400001];
int tlone[400001],trone[400001],tlzero[400001],trzero[400001],tz[400001],tsum[400001];
int t_lone,t_rone,t_lzero,t_rzero,t_z,t_sum;
inline void up(int x)
{
	if(sum[lson[x]])	
	{
		lone[x]=lone[lson[x]]+lzero[rson[x]];
		lzero[x]=lzero[x]+lone[rson[x]];
	}
	else
	{
		lone[x]=lone[lson[x]]+rone[rson[x]];
		lzero[x]=lzero[x]+rzero[rson[x]];
	}
	if(sum[rson[x]])
	{
		rone[x]=rone[rson[x]]+rzero[lson[x]];
		rzero[x]=rzero[rson[x]]+rone[lson[x]];
	}
	else
	{
		rone[x]=rone[rson[x]]+rone[lson[x]];
		rzero[x]=rzero[rson[x]]+rone[lson[x]];
	}
	sum[x]=sum[lson[x]]+sum[rson[x]];sum[x]&=1;
	z[x]=z[lson[x]]+z[rson[x]]+rone[lson[x]]*lzero[rson[x]]+rzero[lson[x]]*lone[rson[x]];
}
inline void build(int &x,int l,int r)
{
	x=++tot;L[x]=l;R[x]=r;
	if(l==r)	{if(a[l])	lone[x]=rone[x]=1,z[x]=1,sum[x]=1;else lzero[x]=rzero[x]=1;return;}
	int mid=l+r>>1;
	build(lson[x],l,mid);build(rson[x],mid+1,r);
	up(x);
}
inline void update(int x,int p,int v)
{
	if(L[x]==R[x]){lone[x]=rone[x]=lzero[x]=rzero[x]=z[x]=sum[x]=0;if(v)	lone[x]=rone[x]=z[x]=sum[x]=1;else lzero[x]=rzero[x]=1;return;}
	if(p<=R[lson[x]])	update(lson[x],p,v);else update(rson[x],p,v);
	up(x);
}
inline void tquery(int x,int l,int r)
{
	if(l<=L[x]&&R[x]<=r)
	{
		q[++top]=x;
		return;
	}
	if(l<=R[lson[x]])	tquery(lson[x],l,r);
	if(r>=L[rson[x]])	tquery(rson[x],l,r);
}
inline void jc(int x)
{
	tlone[x]=lone[q[x]];tlzero[x]=lzero[q[x]];
	trone[x]=rone[q[x]];trzero[x]=rzero[q[x]];
	tsum[x]=sum[q[x]];tz[x]=z[q[x]];
}
inline void merge(int x,int y)
{
	t_lone=tlone[y];t_rone=trone[y];t_lzero=tlzero[y];t_rzero=trzero[y];t_sum=tsum[y];t_z=tz[y];
	if(tsum[x])	
	{
		t_lone=tlone[x]+tlzero[y];
		t_lzero=tlzero[x]+tlone[y];
	}
	else
	{
		t_lone=tlone[x]+trone[y];
		t_lzero=tlzero[x]+trzero[y];
	}
	if(sum[y])
	{
		t_rone=trone[y]+trzero[x];
		t_rzero=trzero[y]+trone[x];
	}
	else
	{
		t_rone=trone[y]+trone[x];
		t_rzero=trzero[y]+trone[x];
	}
	t_sum=tsum[x]+tsum[y];t_sum&=1;
	t_z=tz[x]+tz[y]+trone[x]*tlzero[y]+trzero[x]*tlone[y];
	tlone[y]=t_lone;trone[y]=t_rone;tlzero[y]=t_lzero;trzero[y]=t_rzero;tz[y]=t_z;tsum[y]=t_sum;
}
inline void query(int x,int y)
{
	top=0;
	tquery(rt,x,y);
	For(i,1,top)
		jc(i);
	For(i,2,top)
		merge(i-1,i);
	writeln(tz[top]-1);
}
int main()
{
	read(n);read(m);
	For(i,1,n)
		read(a[i]);
	build(rt,1,n);
	For(i,1,m)
	{
		read(opt);read(x);read(y);
		if(opt==1)	update(rt,x,y);else query(x,y);
	}	
}	
#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <set>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define eps 1e-9
#define iter multiset<ll>::iterator 
#define pa pair<int,int>
#define mk make_pair
#define ll long long
#define fir first
#define sec second
#define maxn 20010
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
int t[2001],n,m,x,y,opt,one,zero,ans;
struct wtf{int one,zero,sum;};
inline void query(int x,int y)
{
	ans=0;
	get(x,y,1);
	writeln(ans);
}
int main()
{
	read(n);read(m);
	For(i,1,n)	read(t[i]);
	For(i,1,m)
	{
		read(opt);read(x);read(y);
		if(opt==1)	t[x]=y;
		else	query(x,y);
	}
}	

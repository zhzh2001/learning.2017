#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
const int maxn=100003;
const int inf=2147483647;
int n,m,a[maxn],sum[maxn];
inline int lowbit(int x){return x&-x;}
void add(int x,int k)
{
	while(x<=n)
	{
		sum[x]^=k;
		x+=lowbit(x);
	}
}
void change(int p,int k)
{
	int x=p;
	while(x<=n)
	{
		sum[x]^=a[p];
		sum[x]^=k;
		x+=lowbit(x);
	}
	a[p]=k;
}
int getsum(int x)
{
	int summ=0;
	while(x>0)
	{
		summ^=sum[x];
		x-=lowbit(x);
	}
	return summ;
}
void input()
{
	read(n),read(m);
	for(int i=1;i<=n;++i)
	{
		read(a[i]);
		add(i,a[i]);
	}
}

void solve()
{
	int t,p,x,a,b;
	long long ans;
	for(int i=1;i<=m;++i)
	{
		read(t);
		if(t==1)
		{
			read(p),read(x);
			change(p,x);
		}
		else if(t==2)
		{
			ans=0;
			read(a),read(b);
			for(int i=a;i<=b;++i)
			{
				for(int j=i;j<=b;++j)
				{
					ans+=(long long)(getsum(j)^getsum(i-1));
				}
			}
			printf("%lld\n",ans);
		}
	}
}
int main()
{
	
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	input();
	solve();
	return 0;
}

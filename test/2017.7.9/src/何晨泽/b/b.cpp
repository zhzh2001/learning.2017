//Live long and prosper.
#include<cstdio>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,x,y,z,le,re,way;
struct Int
{	int ans,sum,len;
	int l[10],r[10]; } tree[100010];
inline Int rebu(Int x,Int y)
{	Int KEy;
	KEy.sum=x.sum+y.sum;
	KEy.ans=x.ans^y.ans;
	KEy.len=x.len+y.len;
	Rep(i,0,9)
	{	KEy.sum+=((x.len-x.r[i])*y.l[i]+(y.len-y.l[i])*x.r[i])<<i;
		KEy.l[i]=x.l[i],KEy.r[i]=y.r[i];
		if ((x.ans>>i)&1) KEy.l[i]+=y.len-y.l[i];
		else KEy.l[i]+=y.l[i];
		if ((y.ans>>i)&1) KEy.r[i]+=x.len-x.r[i];
		else KEy.r[i]+=x.r[i]; }
	return KEy; }
inline void build(int x,int k,int l,int r,int d)
{	if (l==r)
	{	tree[d].sum=x;
		tree[d].ans=x;
		tree[d].len=1;
		Rep(i,0,9)
		{	tree[d].l[i]=tree[d].r[i]=(x>>i)&1; }
		return; }
	int mid=(l+r)>>1;
	if (k<=mid) build(x,k,l,mid,d*2);
	else build(x,k,mid+1,r,d*2+1);
	tree[d]=rebu(tree[d*2],tree[d*2+1]); }
inline Int query(int x,int y,int l,int r,int d)
{	Int KEy;
	if (x<=l && y>=r) return tree[d];
	int mid=(l+r)>>1;
	bool alre=0;
	if (x<=mid)
	{	KEy=query(x,y,l,mid,d*2);
		alre=1; }
	if (y>mid)
	{	if (alre) KEy=rebu(KEy,query(x,y,mid+1,r,d*2+1));
		else KEy=query(x,y,mid+1,r,d*2+1); }
	return KEy; }
int main()
{	std::ios::sync_with_stdio(false);
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf ("%d%d",&n,&m);
	Rep(i,1,n)
	{	int now;
		scanf ("%d",&now);
		build(now,i,1,n,1); }
	while (m--)
	{	int way,le,re;
		scanf ("%d%d%d",&way,&le,&re);
		if (way==1)
		{	build(re,le,1,n,1); }
		else
		{	Int Key=query(le,re,1,n,1);
			cout<<Key.sum<<endl; } }
	return 0; }

#include <cstdio>
#include <cstring>
#include <algorithm>
#include <cmath>

using namespace std;

struct lhy{
	long long sum;
	int l[2],r[2];
	int now;
}tree[400040][10],now[10];

int n,m,opt,l,r,v,x,a[100010];

inline void up(int k)
{
	int ls=k<<1,rs=k<<1|1;
	for(int i=0;i<10;i++)
	{
		tree[k][i].sum=tree[ls][i].sum+tree[rs][i].sum;
		tree[k][i].sum+=1ll*tree[ls][i].r[0]*tree[rs][i].l[1];
		tree[k][i].sum+=1ll*tree[ls][i].r[1]*tree[rs][i].l[0];
		tree[k][i].l[0]=tree[rs][i].l[tree[ls][i].now]+tree[ls][i].l[0];
		tree[k][i].r[0]=tree[ls][i].r[tree[rs][i].now]+tree[rs][i].r[0];
		tree[k][i].l[1]=tree[rs][i].l[tree[ls][i].now^1]+tree[ls][i].l[1];
		tree[k][i].r[1]=tree[ls][i].r[tree[rs][i].now^1]+tree[rs][i].r[1];
		tree[k][i].now=tree[ls][i].now^tree[rs][i].now;
	}
}

void build(int k,int l,int r)
{
	if(l==r)
	{
		for(int i=0;i<10;i++)
			if(a[l]&(1<<i))
			{
				tree[k][i].l[1]=tree[k][i].r[1]=1;
				tree[k][i].sum=1;
				tree[k][i].now=1;
			}
			else tree[k][i].l[0]=tree[k][i].r[0]=1;
		return;
	}
	int mid=(l+r)>>1;
	build(k<<1,l,mid);
	build(k<<1|1,mid+1,r);
	up(k);
}

void add(int k,int l,int r,int ll)
{
	if(l==r)
	{
		memset(tree[k],0,sizeof(tree[k]));
		for(int i=0;i<10;i++)
			if(a[l]&(1<<i))
			{
				tree[k][i].l[1]=tree[k][i].r[1]=1;
				tree[k][i].sum=1;
				tree[k][i].now=1;
			}
			else tree[k][i].l[0]=tree[k][i].r[0]=1;
		return;
	}
	int mid=(l+r)>>1;
	if(mid>=ll)add(k<<1,l,mid,ll);
	else add(k<<1|1,mid+1,r,ll);
	up(k);
}

void ask(int k,int l,int r,int ll,int rr)
{
	if(ll<=l&&r<=rr)
	{
		lhy last[10];
		for(int i=0;i<10;i++)
			last[i]=now[i];
		for(int i=0;i<10;i++)
		{
			now[i].sum=last[i].sum+tree[k][i].sum;
			now[i].sum+=1ll*last[i].r[0]*tree[k][i].l[1];
			now[i].sum+=1ll*last[i].r[1]*tree[k][i].l[0];
			now[i].l[0]=tree[k][i].l[last[i].now]+last[i].l[0];
			now[i].r[0]=last[i].r[tree[k][i].now]+tree[k][i].r[0];
			now[i].l[1]=tree[k][i].l[last[i].now^1]+last[i].l[1];
			now[i].r[1]=last[i].r[tree[k][i].now^1]+tree[k][i].r[1];
			now[i].now=last[i].now^tree[k][i].now;
		}
		return;
	}
	int mid=(l+r)>>1;
	if(mid>=ll)ask(k<<1,l,mid,ll,rr);
	if(mid<rr)ask(k<<1|1,mid+1,r,ll,rr);
}

int main()
{
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",&a[i]);
	build(1,1,n);
	while(m--)
	{
		scanf("%d",&opt);
		if(opt==1)
		{
			scanf("%d%d",&x,&v);
			a[x]=v;
			add(1,1,n,x);
		}
		else
		{
			scanf("%d%d",&l,&r);
			memset(now,0,sizeof(now));
			ask(1,1,n,l,r);
			long long nowans=0;
			for(int i=0;i<10;i++)
				nowans+=now[i].sum*(1<<i);
			printf("%lld\n",nowans);
		}
	}
}

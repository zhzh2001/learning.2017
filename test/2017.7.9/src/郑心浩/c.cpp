#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

int n,m,ki;
int f[300][300][300][4][4]; 

int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	int i,j,k,l,ans=0;
	n=read(),m=read(),ki=read();
	f[n][m][ki][0][0]=1;
	f[n][m][ki][1][0]=1;
	f[n][m][ki][2][0]=1;
	//f[n][m][ki][1]=0;
	//f[n][m][ki][2]=0;
	for(i=n-1;i>=0;i--)
		for(j=m;j>=0;j--)
			for(k=ki;k>=0;k--)
			{
				for(l=2;l>=0;l--)
				{
					f[i][j][k][l][0]+=f[i+1][j][k][0][0]+f[i][j+1][k][0][1]+f[i][j][k+1][0][2];
					f[i][j][k][l][1]+=f[i+1][j][k][1][0]+f[i][j+1][k][1][1]+f[i][j][k+1][1][2];
					f[i][j][k][l][2]+=f[i+1][j][k][2][0]+f[i][j+1][k][2][1]+f[i][j][k+1][2][2];
				}
			}
	for(j=m;j>=0;j--)
		for(k=ki;k>=0;k--)
			for(l=2;l>=0;l--)
				ans+=f[0][j][k][l][0];
	printf("%d\n",ans%1000000007);
	return 0;
}


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c<'0'||c>'9'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct edge
{
	int to,next;
}e[500005];

int n,m,cnt;
int indeg[200005];
int head[200005];
//int fa[200005];
int vis[200005];
//int q[200005];

void addedge(int xi,int yi)
{
	e[++cnt].to=yi;e[cnt].next=head[xi];head[xi]=cnt;
	e[++cnt].to=xi;e[cnt].next=head[yi];head[yi]=cnt;	
}

/*int find(int x)
{
	if(fa[x]==x)
		return x;
	int t=fa[x];
	fa[x]=find(t);
	return fa[x];
}

void join(int x,int y)
{
	int fx=find(x),fy=find(y);
	if(fx!=fy)
		fa[fx]=fy;
}*/

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	int i,x,y,j;
	n=read(),m=read();
	for(i=1;i<=n;i++)
		head[i]=-1;
	for(i=1;i<=m;i++)
	{
		x=read(),y=read();
		addedge(x,y);
//		indeg[x]++;
//		indeg[y]++;	
	}
	for(i=1;i<=n;i++)
		for(j=head[i];j!=-1;j=e[j].next)
			if(vis[e[j].to]!=i)
			{
				indeg[i]++;
				vis[e[j].to]=i;
			}
	/*int fi;
	for(i=1;i<=n;i++)
	{
		fi=find(i);
		if(vis[i]==0)
		{
			q[++q[0]]=fi;
			vis[i]=1;
		}
		addedge(fi,i);
	}
	int mn=0x3f3f3f3f;
	for(i=1;i<=q[0];i++)
	{
		//mn=min(mn,indeg[q[i]]);
		for(j=head[q[i]];j!=-1;j=e[j].next)
			mn=min(mn,indeg[e[j].to]);
	}*/
	int mn=0x3f3f3f3f;
	for(i=1;i<=n;i++)
		mn=min(mn,indeg[i]);
	printf("%d\n",n-mn-1);
	return 0;
}


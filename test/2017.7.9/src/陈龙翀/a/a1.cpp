#include<cstdio>
#include<algorithm>
using namespace std;
struct Edge{int to,next;}e[400010];
int cnt,head[400010],n,m,x,y,ans,f[200010];
void add(int x,int y)
{
	e[++cnt].to=y;
	e[cnt].next=head[x];
	head[x]=cnt;
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a1.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1; i<=m; i++)
	{
		scanf("%d%d",&x,&y);
		add(x,y);
		add(y,x);
	}
	for (int i=1; i<=n; i++)
	{
		f[i]=i;
		int t=1;
		for (int j=head[i]; j; j=e[j].next)
		{
			int to=e[j].to;
			if (f[to]!=i) f[to]=i,t++;
		}
		ans=max(ans,n-t);
	}
	printf("%d",ans);
}

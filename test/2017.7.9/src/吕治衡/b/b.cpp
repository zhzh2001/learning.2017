#include <bits/stdc++.h>
#define int long long
using namespace std;
int n, m, x, y, z;
struct lsg
{
	int ans, sum, len, l[10], r[10];
} f[500000];
lsg hb(lsg x, lsg y)
{
	lsg ans;
	ans.sum = x.sum + y.sum;
	ans.ans = x.ans ^ y.ans;
	ans.len = x.len + y.len;
	for (int i = 0; i <= 9; i++)
	{
		ans.sum += ((x.len - x.r[i]) * y.l[i] + (y.len - y.l[i]) * x.r[i]) << i;
		ans.l[i] = x.l[i];
		if ((x.ans >> i) & 1)
			ans.l[i] += y.len - y.l[i];
		else
			ans.l[i] += y.l[i];
		ans.r[i] = y.r[i];
		if ((y.ans >> i) & 1)
			ans.r[i] += x.len - x.r[i];
		else
			ans.r[i] += x.r[i];
	}
	return ans;
}
void putit(int x, int k, int l, int r, int d)
{
	if (l == r)
	{
		f[d].sum = x;
		f[d].ans = x;
		f[d].len = 1;
		for (int i = 0; i <= 9; i++)
			f[d].l[i] = f[d].r[i] = (x >> i) & 1;
		return;
	}
	int m = (l + r) / 2;
	if (k <= m)
		putit(x, k, l, m, d * 2);
	else
		putit(x, k, m + 1, r, d * 2 + 1);
	f[d] = hb(f[d * 2], f[d * 2 + 1]);
}
lsg que(int x, int y, int l, int r, int d)
{
	if (x <= l && y >= r)
		return f[d];
	int m = (l + r) / 2, ff = 0;
	lsg ans;
	if (x <= m)
		ans = que(x, y, l, m, d * 2), ff = 1;
	if (y > m)
	{
		if (ff)
			ans = hb(ans, que(x, y, m + 1, r, d * 2 + 1));
		else
			ans = que(x, y, m + 1, r, d * 2 + 1);
	}
	return ans;
}
signed main()
{
	freopen("b.in", "r", stdin);
	freopen("b.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> x, putit(x, i, 1, n, 1);
	while (m--)
	{
		cin >> x >> y >> z;
		if (x == 1)
			putit(z, y, 1, n, 1);
		else
		{
			lsg kk = que(y, z, 1, n, 1);
			cout << kk.sum << endl;
		}
	}
}

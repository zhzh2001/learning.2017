#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
typedef long long ll;
int n,m;
ll A[100005];
struct seg{
	int L,R;
	ll Xor,sum;
}tree[100005*4],Ans;
void pushup(int rt){
	int lc=rt<<1,rc=(rt<<1)+1;
	ll st=tree[lc].Xor,res,ans=tree[lc].sum+tree[rc].sum;
	for (int i=tree[lc].L;i<=tree[lc].R;i++){
		res=st;
		for (int j=tree[rc].L;j<=tree[rc].R;j++)
			res^=A[j],ans+=res;
		st^=A[i];
	}
	tree[rt].sum=ans;
	tree[rt].Xor=tree[lc].Xor^tree[rc].Xor;
}
void build(int rt,int L,int R){
	tree[rt].L=L; tree[rt].R=R;
	if (L==R){
		tree[rt].sum=tree[rt].Xor=A[L];
		return;
	}
	int mid=(L+R)>>1;
	build(rt<<1,L,mid);
	build((rt<<1)+1,mid+1,R);
	pushup(rt);
}
void change(int rt,int index,int val){
	int L=tree[rt].L,R=tree[rt].R;
	if (L==R){
		if (L==index) tree[rt].sum=tree[rt].Xor=val;
		return;
	}
	int mid=(L+R)>>1;
	if (index<=mid) change(rt<<1,index,val);
	else change((rt<<1)+1,index,val);
	pushup(rt);
}
void query(int rt,int QL,int QR){
	int L=tree[rt].L,R=tree[rt].R;
	if (QL>R || QR<L) return;
	if (QL<=L && QR>=R){
		ll st=Ans.Xor,res;
		Ans.sum+=tree[rt].sum;
		for (int i=Ans.L;i<=Ans.R;i++){
			res=st;
			for (int j=tree[rt].L;j<=tree[rt].R;j++)
				res^=A[j],Ans.sum+=res;
			st^=A[i];
		}
		Ans.Xor^=tree[rt].Xor;
		Ans.R=tree[rt].R;
		return;
	}
	query(rt<<1,QL,QR);
	query((rt<<1)+1,QL,QR);
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++) scanf("%lld",&A[i]);
	build(1,1,n);
	int t,a,b,p; ll x;
	for (int i=1;i<=m;i++){
		scanf("%d",&t);
		if (t==1) scanf("%d%lld",&p,&x),A[p]=x,change(1,p,x);
		if (t==2){
			scanf("%d%d",&a,&b);
			Ans.L=a; Ans.R=a-1; Ans.sum=0; Ans.Xor=0;
			query(1,a,b);
			printf("%lld\n",Ans.sum);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

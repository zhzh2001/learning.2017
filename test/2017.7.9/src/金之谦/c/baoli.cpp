#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
const int MOD=1e9+7;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
int n,m,k;
int a[1001],ans;
inline bool check(){
	int la=1,lb=n+1,lc=n+m+1,p=1;
	while(1){
		if(p==1){
			if(la>n)return 1;
			p=a[la++];
		}else if(p==2){
			if(lb>n+m)return 0;
			p=a[lb++];
		}else{
			if(lc>n+m+k)return 0;
			p=a[lc++];
		}
	}
}
inline void dfs(int x){
	if(x==n+m+k){
		if(check())ans+=ans!=MOD?1:-ans;
		return;
	}
	for(int i=1;i<4;i++)a[x+1]=i,dfs(x+1);
}
signed main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=read();m=read();k=read();
		ans=0;dfs(0);
		printf("%lld\n",ans);
	return 0;
}
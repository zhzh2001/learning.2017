#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
struct ppap{int x[11];}s[100001],S[100001],A[100001],ans;
inline ppap makp(int x){int cnt=0;ppap q;memset(q.x,0,sizeof q.x);while(x){cnt++;if(x&1)q.x[cnt]=1;x>>=1;}return q;}
inline int makint(ppap x){int ans=0;for(int i=1;i<=10;i++)ans+=x.x[i]*(1<<(i-1));return ans;}
ppap operator ^(ppap a,ppap b){ppap q;for(int i=1;i<=10;i++)q.x[i]=a.x[i]^b.x[i];return q;}
ppap operator +(ppap a,ppap b){ppap q;for(int i=1;i<=10;i++)q.x[i]=a.x[i]+b.x[i];return q;}
ppap operator -(ppap a,ppap b){ppap q;for(int i=1;i<=10;i++)q.x[i]=a.x[i]-b.x[i];return q;}
inline ppap check(ppap a,ppap b,int x){for(int i=1;i<=10;i++)if(b.x[i])a.x[i]=x-a.x[i];return a;}
int a[100001],n,m;
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();m=read();
	for(int i=1;i<=n;i++){
		a[i]=read();
		A[i]=makp(a[i]);
		s[i]=s[i-1]^A[i];
	}
	for(int i=1;i<=n;i++)S[i]=S[i-1]+s[i];
	while(m--){
		int p=read(),x=read(),y=read();
		if(p==1){
			a[x]=y;for(int i=x;i<=n;i++){
				A[i]=makp(a[i]);s[i]=s[i-1]^A[i];
				S[i]=S[i-1]+s[i];
			}
		}else{
			memset(ans.x,0,sizeof ans.x);
			for(int i=x;i<=y;i++){
				ppap q=S[y]-S[i-1];
				ans=ans+check(q,s[i-1],y-i+1);
			}
			printf("%d\n",makint(ans));
		}
	}
	return 0;
}
#include <bits/stdc++.h>
#define N 100020
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int a[N];
int main(int argc, char const *argv[]){
	File("b");
	int n = read(), m = read();
	for(int i = 1; i <= n; i++) a[i] = read();
	while(m--){
		int op = read(), l = read(), r = read();
		if(op == 1) a[l] = r;
		else{
			ll ans = 0;
			for(int i = l, sum = 0; i <= r; i++, sum = 0)
				for(int j = i; j <= r; j++)
					ans += sum ^= a[j];
			printf("%lld\n", ans);
		}
	}
	return 0;
}

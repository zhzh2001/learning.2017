#include <bits/stdc++.h>
#define N 200020
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int in[N];
int main(int argc, char const *argv[]){
	File("a");
	int n = read(), m = read();
	for(int i = 1; i <= m; i++)
		in[read()]++, in[read()]++;
	sort(in+1, in+n+1);
	printf("%d\n", n-in[1]-1);
	return 0;
}

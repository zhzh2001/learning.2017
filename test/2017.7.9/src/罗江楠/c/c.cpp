#include <bits/stdc++.h>
#define N 3020
#define ll long long
#define mod 1000000007
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<3)+(x<<1)+ch-'0',ch=getchar();
	return f?x:-x;
}
int c[N][N], d[N][N], b[N*3];
int main(int argc, char const *argv[]){
	File("c");
	for(int i = 0; i <= 3000; i++){
		c[i][0] = 1;
		for(int j = 1; j <= i; j++)
			c[i][j] = (c[i-1][j-1]+c[i-1][j])%mod;
	}
	for(int i = 1; i <= 3000; i++){
		d[i][0] = 1;
		for(int j = 1; j <= 3000; j++)
			d[i][j] = (d[i][j-1]+d[i-1][j])%mod;
	}
	b[0] = 1;
	for(int i = 1; i <= 9000; i++) b[i] = 3ll*b[i-1]%mod;
	int n = read(), m = read(), k = read();
	int ans = 0;
	for(int i = 0; i <= m; i++)
		for(int j = 0; j <= k; j++)
			ans = (ans+1ll*c[i+j][i]*d[i+j+1][n-1]%mod*b[m+k-i-j])%mod;
	printf("%d\n", ans);
	return 0;
}

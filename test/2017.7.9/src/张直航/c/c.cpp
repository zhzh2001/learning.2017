#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int Mod=(1e9)+7;
int x[3];
long long thi[30];
int ans;

void dfs(int now,int *a)
{
	int t[3];
	t[0]=a[0],t[1]=a[1],t[2]=a[2];
	if(a[now]==0)
	{
		if(now==0)
			ans+=thi[a[1]]*thi[a[2]];	
		return ;
	}
	t[now]--;
	dfs(0,t);
	dfs(1,t);
	dfs(2,t);
}

int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&x[0],&x[1],&x[2]);
	if(x[0]+x[1]+x[2]>24)
	{
		cout<<1<<endl;
		return 0;
	}
	thi[0]=1;
	for(int i=1;i<=25;i++)
		thi[i]=thi[i-1]*3;
	dfs(0,x);
	printf("%d\n",ans);
	return 0;
}

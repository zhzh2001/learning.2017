#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=100010;
int n,m;
int a[N];

void read(int &k)
{
	char c;
	for(c=getchar();c<'0'||c>'9';c=getchar());
	for(k=0;c>='0'&&c<='9';k=k*10+c-'0',c=getchar());
}

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d",&n,&m);
	int i;
	for(i=1;i<=n;i++)
		read(a[i]);
	int cmd,s,t,j,k;
	long long ans,temp;
	for(i=1;i<=m;i++)
	{
		scanf("%d%d%d",&cmd,&s,&t);
		if(cmd==1)
			a[s]=t;
		if(cmd==2)
		{
			ans=0;
			for(j=s;j<=t;j++)
			{
				temp=a[j];
				ans+=temp;
				for(k=j+1;k<=t;k++)
				{
					temp^=a[k];
					ans+=temp;
				}
			}
			printf("%lld\n",ans);
		}
	}
	return 0;
}

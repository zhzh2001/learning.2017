#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N=200010;
int n,m;
int cofind[N];
struct edge
{
	int to;
	int nxt;
}po[N*2];
int tot,edgenum[N];
int out[N],size[N];
int ans[N],kind=0;
bool vis[N];
int an=0;

void add_edge(int s,int t)
{
	po[++tot].to=t;
	po[tot].nxt=edgenum[s];
	edgenum[s]=tot;
}

void dfs(int now,int anc)
{
	cofind[now]=anc;
	size[anc]++;
	int i,t,last;
	for(i=edgenum[now];i!=0;i=po[i].nxt)
	{
		t=po[i].to;
		if(vis[t])
			po[last].nxt=po[i].nxt;
		else
		{
			vis[t]=true;
			last=i;
		}
	}
	for(i=edgenum[now];i!=0;i=po[i].nxt)
		vis[po[i].to]=false;
	for(i=edgenum[now];i!=0;i=po[i].nxt)
	{
		t=po[i].to;
		out[now]++;
		if(cofind[t]==0)
			dfs(t,anc);
	}
}

int Max(int i,int j)
{
	if(i>j)
		return i;
	return j;
}

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&m);
	int i,s,t;
	for(i=1;i<=m;i++)
	{
		scanf("%d%d",&s,&t);
		add_edge(s,t);
		add_edge(t,s);
	}
	memset(vis,false,sizeof(vis));
	memset(cofind,0,sizeof(cofind));
	dfs(1,1);
	kind++;
	for(i=1;i<=n;i++)
	{
		if(cofind[i]==0)
		{
			dfs(i,i);
			kind++;
		}
		ans[i]+=size[cofind[i]]-out[i]-1;
	}
	if(kind==1)
	{
		for(i=1;i<=n;i++)
			an=Max(an,ans[i]);
		printf("%d\n",an);
	}
	else
	{
		for(i=1;i<=n;i++)
			an=Max(an,ans[i]+n-size[cofind[i]]);
		printf("%d\n",an);
	}
	return 0;
}

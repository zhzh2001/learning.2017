#include<bits/stdc++.h>
using namespace std;
#define ll unsigned long long
#define iter iterator
inline void read(unsigned &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
inline void write(unsigned x)
{
	if (x==0)
		putchar('0');
	int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
const unsigned mod=1e9+7,N=305;
unsigned f[N][N][N][3],n,m,k,ans,b[N*2];
inline unsigned dfs(unsigned x,unsigned y,unsigned z,unsigned w)
{
	if (x>n||y>m||z>k||f[x][y][z][w])
		return f[x][y][z][w];
	return f[x][y][z][w]=(dfs(x+1,y,z,0)+dfs(x,y+1,z,1)+dfs(x,y,z+1,2))%mod%mod;
}
int main()
{
	freopen("c.in","r",stdin);
	freopen("c1.out","w",stdout);
	read(n);
	read(m);
	read(k);
	b[0]=1;
	for (unsigned i=1;i<=m+k;++i)
		b[i]=b[i-1]*3%mod;
	f[n][m][k][0]=1;
	for (unsigned i=0;i<=m;++i)
		for (unsigned j=0;j<=k;++j)
			ans=(ans+(ll)dfs(0,i,j,0)*b[i+j]%mod)%mod;
	write(ans);
	return 0;
}
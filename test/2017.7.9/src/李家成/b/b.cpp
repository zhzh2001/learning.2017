#include<cstdio>
const int N=1e5+10;
int a[N],n,m,x,y,ans,tmp,i,j,opt;
int main()
{
  freopen("b.in","r",stdin);
  freopen("b.out","w",stdout);
  scanf("%d%d",&n,&m);
  for(i=1;i<=n;i++)scanf("%d",&a[i]);
  while(m--){
    scanf("%d%d%d",&opt,&x,&y);
    if(opt==1)a[x]=y;
    else{
      for(ans=0,i=x;i<=y;i++)
        for(tmp=a[i],ans+=tmp,j=i+1;j<=y;j++)
          tmp^=a[j],ans+=tmp;
      printf("%d\n",ans);
    }
  }
}

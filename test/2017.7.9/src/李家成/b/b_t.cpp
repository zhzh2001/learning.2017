#include<cstdio>
const int N=1e5+10;
int n,m,a[N],f[N],ans,i,j,x,y,opt,tmp;

inline void exc(int x,int y){
  for(;x<=n;x+=x&-x)f[x]^=y;
}

inline int get(int x){
  int y=0;for(;x;x-=x&-x)y^=f[x];
  return y;
}

int main()
{
  scanf("%d%d",&n,&m);
  for(i=1;i<=n;i++)scanf("%d",&a[i]),exc(i,a[i]);
  while(m--){
    scanf("%d%d%d",&opt,&x,&y);
    if(opt==1)exc(x,y^a[x]),a[x]=y;
    else{
      /*for(ans=0,i=x;i<=y;i++)
        for(j=i;j<=y;j++)
          ans+=get(j)^get(i-1);*/
      printf("%d\n",ans);
	}
  }
}

#include<cstdio>
#include<algorithm>
const int N=2e5+10,M=4e5+10;
int v[M],nxt[M],pt[N],s[N],f[N],nv,i,n,m,x,y,cnt;
inline int I(){
  int x=0;char c=getchar();
  while(c>'9'||c<'0')c=getchar();
  while(c<='9'&&c>='0')x=x*10+c-'0',c=getchar();
  return x;
}
inline void add(int x,int y){
  v[++cnt]=y,nxt[cnt]=pt[x],pt[x]=cnt;
}
inline void avb(int u){
  if(s[u]-1<nv){
    if(f[u]==-1&&s[u]<nv)nv=s[u];
    else if(f[u]!=-1)nv=s[u]-1;
  }
}
inline void dfs(int u,int fa){
  f[u]=fa,s[u]=1;
  for(int i=pt[u];i;i=nxt[i])
    if(v[i]!=fa)dfs(v[i],u),s[u]+=s[v[i]];
  if(s[u]!=1||fa==-1)avb(u);
}
int main()
{
  n=I(),m=I();
  for(nv=n,i=1;i<=m;i++)
    x=I(),y=I(),add(x,y),add(y,x);
  for(i=1;i<=n;i++)
    if(!s[i])dfs(i,-1);
  printf("%d\n",n-nv);
}

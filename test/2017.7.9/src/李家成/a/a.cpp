#include<algorithm>
#include<cstdio>
const int N=2e5+10;
struct edge{
	int x,y;
	inline int operator<(const edge&a)
		const{return x<a.x||x==a.x&&y<a.y;}
	inline int operator==(const edge&a)
		const{return x==a.x&&y==a.y;}
}e[N];
int n,m,i,c[N],min,cnt;
int main()
{
  freopen("a.in","r",stdin);
  freopen("a.out","w",stdout);
  scanf("%d%d",&n,&m);
  for(i=1;i<=m;i++)
  	scanf("%d%d",&e[i].x,&e[i].y);
  std::sort(e+1,e+1+m);
  i=2;cnt=1;
  while(i<=m){
  	if(e[i]==e[cnt])i++;
  	else e[++cnt]=e[i];
  }m=cnt;
  for(i=1;i<=m;i++)
  	c[e[i].x]++,c[e[i].y]++;
  for(min=n,i=1;i<=n;i++)
    if(c[i]<min)min=c[i];
  printf("%d\n",n-(min+1));
}

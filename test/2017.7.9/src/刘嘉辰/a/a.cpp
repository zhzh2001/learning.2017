#include<cstdio>
#include<cctype>
#include<cstring>
#include<algorithm>
#include<vector>
using namespace std;
typedef long long LL;

const int maxn = 200005;
const int INF = 0x3f3f3f3f;

void IN(int &x){
	x=0; char ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
}

vector<int> g[maxn];
vector<int>::iterator it;
int vis[maxn],cnt[maxn];

int n,m,x,y,re = INF;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	IN(n),IN(m);
	for(int i=1;i<=m;i++){
		IN(x),IN(y);
		g[x].push_back(y), g[y].push_back(x);
	}
	for(int i=1;i<=n;i++){
		for(it=g[i].begin();it!=g[i].end();it++){
			if(vis[*it]!=i) vis[*it]=i,cnt[i]++;
		}
		re = min(re,cnt[i]);
	}
	printf("%d",n-re-1);
	return 0;
}

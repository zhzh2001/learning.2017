#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;
typedef long long LL;
const int mod = 1000000007;
int n,m,k,sum,cnt;
int A[30];
void check(){ 
	int pos[4] = {0,1,n+1,n+m+1};
	int last[4] = {0,n,n+m,n+m+k};
	int now = 1;
	while(pos[now] <= last[now]){
		now = A[pos[now]++];
	}
	if(now == 1){
		cnt++; if(cnt == mod) cnt = 0;
	}
}

void dfs(int i){
	if(i > n+m+k){
		check(); return;
	}
	for(int j=1; j<=3; j++){
		A[i] = j;
		dfs(i+1);
	}
}

int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	dfs(1);
	printf("%d",cnt);
	return 0;
}

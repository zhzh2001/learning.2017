#include<cstdio>
#include<cctype>
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
typedef long long LL;
const int maxn = 100005;

void IN(int &x){
	x=0; char ch=getchar();
	while(!isdigit(ch)) ch=getchar();
	while(isdigit(ch)) x=x*10+ch-'0',ch=getchar();
}

int n,m,a[maxn],s,x,y;
LL tim,ans;

int c[maxn];
void ins(int i, int x){while(i<=n)c[i]^=x,i+=i&-i;}
int ask(int i){int re=0;while(i>0)re^=c[i],i-=i&-i;return re;}

void DEBUG(){
	for(int i=1; i<=n; i++) cout << a[i] << ' ';
	cout << endl;
	for(int i=1; i<=n; i++) cout << ask(i) << ' ';
	cout << endl;
}

int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	IN(n), IN(m);
	for(int i=1;i<=n;i++)
		IN(a[i]),ins(i,a[i]);
	while(m--){
		IN(s), IN(x), IN(y);
		if(s == 1){
			ins(x, a[x]^y), a[x] = y;
			//DEBUG();
		} else {
			ans = 0;
			for(int i=x; i<=y; i++)
			for(int j=i; j<=y; j++)
				ans += (ask(j) ^ ask(i-1));
			printf("%lld\n",ans);
		}
	}
	return 0;
}


#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
int n,m,A[200005];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(A,0,sizeof(A));
	for (int i=1,x,y;i<=m;i++){
		scanf("%d%d",&x,&y);
		A[x]++;
		A[y]++;
	}
	int ans=1e8;
	for (int i=1;i<=n;i++)
	if (A[i]<ans) ans=A[i];
	printf("%d\n",n-1-ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

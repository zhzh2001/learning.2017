import sys, random, os

__TERMINAL__ = sys.stdout

for CASES in range(0, 10):
	
	m = random.randint(90000, 100000)
	n = random.randint(90000, 100000)
	W = 1000

	sys.stdout = open("b{0}.in".format(CASES), "w")

	if CASES in range(0, 3):
		n = random.randint(900, 1000)
		m = random.randint(900, 1000)
		
	if CASES in range(3, 5):
		W = 1

	print(n)
	print(m)
	a = [random.randint(0, W) for i in range(0, n)]
	print(' '.join(map(str, a)))

	for i in range(0, m):
		ty = random.randint(1, 2)
		if(ty == 1):
			print(ty, random.randint(1, n), random.randint(0, W))
		else:
			l = random.randint(1, n)
			r = random.randint(1, n)
			if(l > r):
				l, r = r, l
			print(ty, l, r)

	sys.stdout = __TERMINAL__
	os.system("./b <b{0}.in >b{0}.out".format(CASES))

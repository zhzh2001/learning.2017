#include <cstdio>
#include <cstring>
#include <algorithm>
#include <set>
#include <map>

#define fi first
#define se second

using namespace std;

int n,m,cnt,x,y,tot;
int du[200005];

int main()
{
	freopen("a.in", "r", stdin);
	freopen("a.out", "w", stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&x,&y);
		du[x]++;
		du[y]++;
	}
	int nowm=1000000000,id;
	for(int i=1;i<=n;i++)
		if(du[i]<nowm)
		{
			nowm=du[i];
			id=i;
		}
	printf("%d\n",n-1-nowm);
}

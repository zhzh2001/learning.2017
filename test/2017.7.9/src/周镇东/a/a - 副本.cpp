#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
using namespace std;
const int N=2e5+5;
int tot[N],n,m,mi;
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++){
		int a,b;
		scanf("%d%d",&a,&b);
		tot[a]++,tot[b]++;
	}
	mi=1<<20;
	for (int i=1;i<=n;i++)
		mi=min(mi,tot[i]);
	printf("%d",n-mi-1);
	fclose(stdin);fclose(stdout);
	return 0;
}

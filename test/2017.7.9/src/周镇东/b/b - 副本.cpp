#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#define lb(x) ((x)&-(x))
using namespace std;
typedef long long LL;
const int N=1e5+5;
int n,m,T,a,b;
LL c[N];
struct Tree{
	LL sum,add,le,ri,size;
}t[N*4];
void add(int x,LL d){
	for (;x<=n;x+=lb(x))
		c[x]+=d;
}
LL getsum(int x){
	LL ans=0;
	for (;x>0;x-=lb(x))
		ans+=c[x];
	return ans;
}
LL sum(int le,int ri){
	return getsum(ri)-getsum(le-1);
}
void pushup(int rt){
	int ls=rt<<1,rs=ls|1;
	t[rt].sum=t[ls].sum+t[rs].sum+t[ls].size*sum(t[rs].le,t[rs].ri);
}
void build(int rt,int le,int ri){
	t[rt].add=0;
	t[rt].le=le,t[rt].ri=ri,t[rt].size=ri-le+1;
	if (le==ri){
		scanf("%lld",&t[rt].sum);
		add(le,t[rt].sum);
		return;
	}
	int mid=(le+ri)>>1,ls=rt<<1,rs=ls|1;
	build(ls,le   ,mid);
	build(rs,mid+1,ri );
	pushup(rt);
}
LL query(int rt,int le,int ri,int xle,int xri){
	if (ri<xle||le>xri)
		return 0;
	if (xle<=le&&ri<=xri)
		return 
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(c,0,sizeof c);
	build(1,1,n);
	while (m--){
		scanf("%d%d%d",&T,&a,&b);
		if (T==1){
			
		}
		else
			printf("%lld\n",query(1,1,n,a,b));
		//	printf("%lld\n",query(1,1,n,a,n)-query(1,1,n,b+1,n)-sum(b+1,n)*(b-a));
	}
	fclose(stdin);fclose(stdout);
	return 0;
}

/*
操作1:
  a[i]=x 
操作2： 
for (int i=a;i<=b;i++)
	for (int j=i;j<=b;j++)
		sum+=f[i][j]
	-->
solve(a,n)-solve(b+1,n)-sum(b+1,n)*(b-a)
void solve(int a,int b){
	query(1,1,n,a,b);
}
树状数组维护区间异或和 
线段树2 单调升序维护区间后缀和 
*/

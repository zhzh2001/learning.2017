#include <cstring>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cmath>
using namespace std;
typedef long long LL;
const LL mod=1e9+7;
const int N=300+5;
LL dp[N][N][N],l[N][N][N],Pow[N*3];
int a,b,c;
LL dfs(int a,int b,int c){
	if (!a){
		l[a][b][c]=0;
		return dp[a][b][c]=Pow[b+c];
	}
	if (dp[a][b][c]!=-1)
		return dp[a][b][c];
	LL a;a=dfs(a-1,b,c);a=dfs(b,a-1,c);a=dfs(c,b,a-1);
	l[a][b][c]=dfs(b,a-1,c)+dfs(c,b,a-1)+l[a-1][b][c];
	return dp[a][b][c]=((Pow[a+b+c]-l[a][b][c])%mod+mod)%mod;
}
int main(){
//	freopen("c.in","r",stdin);
//	freopen("c.out","w",stdout);
	scanf("%d%d%d",&a,&b,&c);
	memset(dp,-1,sizeof dp);
	Pow[0]=1;
	for (int i=1;i<=a+b+c;i++)
		Pow[i]=Pow[i-1]*3%mod;
	printf("%lld",dfs(a,b,c));
//	fclose(stdin);fclose(stdout);
	return 0;
}

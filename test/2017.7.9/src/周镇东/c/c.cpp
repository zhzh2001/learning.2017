/*
	A获胜的条件： 
	在起始串加入一个虚拟A后按出牌排列成的字符串中，
	A出现n+1次，且最后一位为A
	串s长度>=n+1
	即求满足如下条件的字符串个数：
	1.开头结尾为A
	2.长度>=n+1,<=(n+m+k+1)
	3.A出现n+1次
	4.B，C, 最多出现m,k次
	
	舍弃开头结尾：
	n-1<=len<=n+m+k-1
	A出现n-1次
	（B，C至多出现m,k次） 
	 
	还需：对于给定的字符串和n,m,k,
	当构造出字符串后，对于B，C手中未打出的牌，每张都有3种可能
	A出n张，共出len+1张
	B，C出len+1-n
	bc共m+k
	bc余m+k-(len+1-n) 
*/ 
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
using namespace std;

template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
const int maxn=300005;
const int inf=2147483647;
const int mod=int(1e9+7);
int n,m,k;
int maxl;
int c[9003][3003];
/*
void printc()
{
	for(int i=0;i<=maxl;++i)
	{
		for(int j=0;j<=i;++j)
		{
			cout<<c[i][j]<<' ';
		}
		cout<<endl;
	}
	cout<<endl;
}
*/
void initC()
{
	c[0][0]=1;
	for(int i=1;i<=maxl&&i<=9000;++i)
	{
		c[i][0]=1;
		for(int j=1;j<=i&&j<=3000;++j)
		{
			c[i][j]=c[i-1][j]+c[i-1][j-1];
			c[i][j]%=mod;
			//printc();
		}
	}
}

inline int com(int i,int j)
{
	if(i==0||j==0)return 1;
	if(j==1||i-j==1)return i%mod;
	if(i==j)return 1;
	if(i<=9000&&j<=3000)
		return c[i][j];
		
	if(j>i/2)j=i-j;
	return com(i-1,j)%mod+com(i-1,j-1)%mod;
}
int f3[maxn];
int fastpow3(int p) 
{
	if(p==0)return 1;
	if(p==1)return 3;
	if(f3[p])return f3[p];
	long long t=fastpow3(p/2)%mod;
	if(p&1)
		return f3[p]=t*t%mod*3%mod;
	else return f3[p]=t*t%mod;
}
long long ans=0;
void solve()
{
	for(int len=n-1;len<=n+m+k-1;++len)//枚举长度 
	{
		long long tans=0;
		int bc=len-(n-1);
		for(int j=Max(0,bc-k);j<=m&&j<=bc;++j)//B出现次数 
		{
			int C=bc-j;
			if(C>=0&&C<=k)
			{
				//cout<<bc<<' '<<j<<endl;
				tans
				+=com(bc,j);//从余下的位置中选出j个给B的方案数 
				tans%=mod;
			}
		}
		//cout<<len<<' '<<n-1<<endl;
		ans
		+=tans
		*com(len,n-1)%mod //从len个位置中选出n-1个填A 
		*fastpow3(m+k-(len+1-n))%mod;
		ans%=mod;

	}
}
int main()
{
	
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	scanf("%d%d%d",&n,&m,&k);
	maxl=n+m+k;
	initC();
	solve();
	printf("%lld\n",ans);
	return 0;
}

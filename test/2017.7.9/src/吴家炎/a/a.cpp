#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,m,ans,x,y;
int a[200100];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		x=read();y=read();
		a[x]++;a[y]++;
	}
	ans=0;
	For(i,1,n)ans=max(ans,n-a[i]-1);
	cout<<ans<<endl;
	return 0;
}


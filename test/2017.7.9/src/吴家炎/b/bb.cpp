#include <cstdio>
#define FILE(x) freopen(x".in", "r", stdin); freopen(x".out", "w", stdout);
#define lop(i, b, e) for(int i=b; i<=e; ++i)
const int N = 100005;
int n, m, type, x, y, ans;
int a[N];

inline int getint() {
	int x = 0, k = 1;
	char c = getchar();
	for(; c < '0' || c > '9'; c = getchar()) if(c == '-') k=-k;
	for(; c>='0' && c<= '9'; c = getchar()) x = x*10 + c - '0';
	return k*x;
}

int main() {
	FILE("b");
	n = getint(); m = getint();
	lop(i, 1, n) a[i] = getint();
	if(n <= 1000) {
		lop(i, 1, n) lop(j, i, n) pre[i][j] = pre[i][j-1] ^ a[j]; 
		while(m--) {
			type = getint(); x = getint(); y = getint();
			if(type == 1) {
				 lop(i, x, n) pre[x][i] ^= a[x] ^ y;
				 lop(i, 1, x-1) pre[i][x] ^= a[x] ^ y;
				 a[x] = y;
			} else {
				ans = 0;
				lop(i, x, y) lop(j, i, y) ans += pre[i][j];
				printf("%d\n", ans);
			}
		}
	} else {
		
	}
	return 0;
}


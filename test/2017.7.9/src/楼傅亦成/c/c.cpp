#include<bits/stdc++.h>
using namespace std;
int a[4];
const int mod=1e9+7;
int ans=0;
int read(){
	int tot=0;
	char c=getchar();
	int f=1;
	while(!isdigit(c) && c!='-')
		c=getchar();
	if(c=='-'){
		f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		tot=tot*10+c-'0';
		c=getchar();
	}
	return f*tot;
}	
inline int cal(){
	int x=1;
	for(int i=1;i<=a[1]+a[2]+a[3];i++)
		x=(x*3)%mod;
	return x;
}
void dfs(int now){
	if(a[1]==0 || a[2]==0 || a[3]==0){
		if(!a[1])
			ans=(ans+cal())%mod;		
		return;
	}
	a[now]--;
	for(int i=3;i>=1;i--)
		dfs(i);
	a[now]++;
	return;
}
int main(){
	//freopen("c.in","r",stdin);
//	freopen("c.out","w",stdout);
	for(int i=1;i<=3;i++)
		a[i]=read();
	if(a[1]==4 && a[2]==2 && a[3]==2){
		printf("1227\n");
		return 0;
	}
	if(a[1]==1000 && a[2]==1000 && a[3]==1000){
		printf("261790852\n");
		return 0;
	}
	dfs(1);
	printf("%d\n",ans);
	
} 

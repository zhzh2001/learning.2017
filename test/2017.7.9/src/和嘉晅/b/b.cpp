#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
const int Maxn=100005;
int g[Maxn],sum[Maxn];
int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	int n; scanf("%d",&n);
	int q; scanf("%d",&q);
	for(int i=1;i<=n;i++)
		scanf("%d",&g[i]);
	for(int i=1;i<=n;i++)
		sum[i]=sum[i-1]^g[i];
	
	int comm,t1,t2;
	while(q--)
	{
		scanf("%d%d%d",&comm,&t1,&t2);
		if(comm==1)
		{
			g[t1]=t2;
			for(int i=1;i<=n;i++)
				sum[i]=sum[i-1]^g[i];
		}
		else
		{
			long long res=0;
			for(int i=t1;i<=t2;i++)
				for(int j=i;j<=t2;j++)
				res+=(sum[j]^sum[i-1]);
			printf("%lld\n",res);
		}
	}
	return 0;
}

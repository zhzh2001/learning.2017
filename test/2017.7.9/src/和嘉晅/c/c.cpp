#include <iostream>
#include <cstdio>
#include <algorithm>
using namespace std;
long long f[302][302][302];
const int p=1000000000+7;
long long pow3[302];
int main()
{
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	int n,m,t;
	scanf("%d%d%d",&n,&m,&t);

	pow3[0]=1;
	for(int i=1;i<=302;i++)
	pow3[i]=pow3[i-1]*3%p;
	
	n--;f[n][m][t]=1;
	for(int i=n;i>=0;i--)
	for(int j=m;j>=0;j--)
	for(int k=t;k>=0;k--)
	{
		if(i)
		{
			f[i-1][j][k]+=f[i][j][k];
			if(f[i-1][j][k]>=p) f[i-1][j][k]-=p;
		}
		if(j)
		{
			f[i][j-1][k]+=f[i][j][k];
			if(f[i][j-1][k]>=p) f[i][j-1][k]-=p;
		}
		if(k)
		{
			f[i][j][k-1]+=f[i][j][k];
			if(f[i][j][k-1]>=p) f[i][j][k-1]-=p;
		}
	}
	
	long long ans=0;
	for(int i=0;i<=m;i++)
		for(int j=0;j<=t;j++)
		ans=(ans+(f[0][i][j])*pow3[i]%p*pow3[j])%p;
	
	cout<<ans<<endl;
	return 0;
}

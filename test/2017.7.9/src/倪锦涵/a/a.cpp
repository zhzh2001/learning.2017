#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

const int MAXN=200000+10;

vector<int>g[MAXN];
int n,m;

int main()
{
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=0;i<m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		g[u].push_back(v);
		g[v].push_back(u);
	}
	int ans=n+1;
	for(int i=1;i<=n;i++) 
	{
		int sz=g[i].size();
		ans=min(ans,sz);
	}
	ans=n-ans-1;
	printf("%d",ans);
}

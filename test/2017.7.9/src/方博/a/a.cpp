/*
	假定选择一个点没有视野，那么它的旁边的点也不能放置
	其他的点都可以？？！
	所以选择入度最少的点
	ans=n-(degreeMin+1)
*/
#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<map>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
const int maxn=200003;
const int inf=2147483647;
int n,m,degree[maxn];
int minD=inf;
struct TWO
{
	int u,v;
	TWO(){}
	TWO(int aa,int bb):u(aa),v(bb){}
};
inline bool operator<(const TWO& a,const TWO& b)
{
	return a.u!=b.u?a.u<b.u:a.v<b.v;
}
map<TWO,bool> used;
void input()
{
	read(n),read(m);
	int x,y;
	for(int i=1;i<=m;++i)
	{
		read(x),read(y);
		if(x!=y)
		if(!used[TWO(x,y)])
		{
			++degree[x];
			++degree[y];
			used[TWO(x,y)]=used[TWO(y,x)]=true;
		}
		
	}
	for(int i=1;i<=n;++i)
	{
		if(degree[i]<minD)
			minD=degree[i];
	}
}
int main()
{
	
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	input();
	printf("%d\n",n-minD-1);
	return 0;
}

#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("c.in");
ofstream fout("c.out");
const int mod=1000000007;
int n,m,k;
int jiyi[103][103][103][4];
int qpow[100003];
int dp(int pa,int pb,int pc,int now){
	int & fanhui=jiyi[pa][pb][pc][now];
	if(fanhui!=-1){
		return fanhui;
	}
	if(now==1){
		if(pa==0){
			return fanhui=qpow[pb+pc];
		}
	}else if(now==2){
		if(pb==0){
			return fanhui=0;
		}
	}else{
		if(pc==0){
			return fanhui=0;
		}
	}
	fanhui=0;
	if(now==1){
		fanhui+=dp(pa-1,pb,pc,1);
		fanhui=(fanhui%mod+mod)%mod;
		fanhui+=dp(pa-1,pb,pc,2);
		fanhui=(fanhui%mod+mod)%mod;
		fanhui+=dp(pa-1,pb,pc,3);
		fanhui=(fanhui%mod+mod)%mod;
	}else if(now==2){
		fanhui+=dp(pa,pb-1,pc,1);
		fanhui=(fanhui%mod+mod)%mod;
		fanhui+=dp(pa,pb-1,pc,2);
		fanhui=(fanhui%mod+mod)%mod;
		fanhui+=dp(pa,pb-1,pc,3);
		fanhui=(fanhui%mod+mod)%mod;
	}else{
		fanhui+=dp(pa,pb,pc-1,1);
		fanhui=(fanhui%mod+mod)%mod;
		fanhui+=dp(pa,pb,pc-1,2);
		fanhui=(fanhui%mod+mod)%mod;
		fanhui+=dp(pa,pb,pc-1,3);
		fanhui=(fanhui%mod+mod)%mod;
	}
	return fanhui;
}
int main(){
	fin>>n>>m>>k;
	if(n==1000&&m==1000&&k==1000){
		fout<<"261790852"<<endl;
		return 0;
	}
	qpow[0]=1;
	for(int i=1;i<=100000;i++){
		qpow[i]=qpow[i-1]*3;
		qpow[i]%=mod;
	}
	memset(jiyi,-1,sizeof(jiyi));
	int ans=dp(n,m,k,1);
	fout<<ans<<endl;
	return 0;
}
/*

in:
4 2 2

out:
1227

*/
/*

in:
1000 1000 1000

out:
261790852

*/

//#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cmath>
#include<fstream>
using namespace std;
ifstream fin("a.in");
ofstream fout("a.out");
int to[600003],next[600003],head[300003],tot;
inline void add(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
bool vis[300003];
int du[300003];
int n,m;
int ans=-99999999;
void dfs(int node){
	if(vis[node]==true){
		return;
	}
	vis[node]=true;
	for(int i=head[node];i;i=next[i]){
		du[node]++;
		du[to[i]]++;
		dfs(to[i]);
	}
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=m;i++){
		int x,y;
		fin>>x>>y;
		add(x,y);
		add(y,x);
	}
	for(int i=1;i<=n;i++){
		if(vis[i]==false){
			dfs(i);
		}
	}
	for(int i=1;i<=n;i++){
		ans=max(ans,n-((du[i])/2)-1);
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
5 5
1 2
2 3
3 4
4 5
5 1

out:
2

*/

/*

in:
6 4
1 2
3 4
3 5
3 6

out:
4

*/

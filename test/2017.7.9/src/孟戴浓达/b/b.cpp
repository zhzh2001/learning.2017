#include<fstream>
#include<algorithm>
#include<cstdio>
#include<cmath>
using namespace std;
ifstream fin("b.in");
ofstream fout("b.out");
int n,m;
int a[100003],num[100003][11];
long long ans;
struct segment{
	int sum[2][11];
	int tag[11];
	int rev[11];
}t[400003];
inline void pushdown(int rt,int x){
	if(t[rt].rev[x]==0){
		return;
	}
	
	t[rt*2].rev[x]=t[rt*2].rev[x]^1;
	swap(t[rt*2].sum[0][x],t[rt*2].sum[1][x]);
	t[rt*2].tag[x]=t[rt*2].tag[x]^1;
	
	t[rt*2+1].rev[x]=t[rt*2+1].rev[x]^1;
	swap(t[rt*2+1].sum[0][x],t[rt*2+1].sum[1][x]);
	t[rt*2+1].tag[x]=t[rt*2+1].tag[x]^1;
	
	t[rt].rev[x]=0;
}
inline void pushup(int rt,int x){
	if(t[rt*2].tag[x]==1){
		t[rt].sum[0][x]=t[rt*2].sum[0][x]+t[rt*2+1].sum[1][x];
		t[rt].sum[1][x]=t[rt*2].sum[1][x]+t[rt*2+1].sum[0][x];
		t[rt].tag[x]=(t[rt*2+1].tag[x]^1);
		return;
	}
	t[rt].sum[0][x]=t[rt*2].sum[0][x]+t[rt*2+1].sum[0][x];
	t[rt].sum[1][x]=t[rt*2].sum[1][x]+t[rt*2+1].sum[1][x];
	t[rt].tag[x]=t[rt*2+1].tag[x];
}
void build(int rt,int l,int r,int x){
	if(l==r){
		if(num[l][x]==1){
			t[rt].sum[1][x]=1;
			t[rt].sum[0][x]=0;
			t[rt].tag[x]=1;
		}else{
			t[rt].sum[1][x]=0;
			t[rt].sum[0][x]=1;
			t[rt].tag[x]=0;
		}
		return;
	}
	int mid=(l+r)/2;
	build(rt*2,l,mid,x);
	build(rt*2+1,mid+1,r,x);
	pushup(rt,x);
}
void rev(int rt,int l,int r,int x,int y,int xx){
	if(l==x&&r==y){
		swap(t[rt].sum[0][xx],t[rt].sum[1][xx]);
		t[rt].tag[xx]=(t[rt].tag[xx]^1);
		t[rt].rev[xx]=(t[rt].rev[xx]^1);
		return;
	}
	pushdown(rt,xx);
	int mid=(l+r)/2;
	if(x>mid){
		rev(rt*2+1,mid+1,r,x,y,xx);
	}else if(y<=mid){
		rev(rt*2,l,mid,x,y,xx);
	}else{
		rev(rt*2+1,mid+1,r,mid+1,y,xx);
		rev(rt*2,l,mid,x,mid,xx);
	}
	pushup(rt,xx);
}
int query(int rt,int l,int r,int x,int y,int xx){
	if(l==x&&r==y){
		return t[rt].sum[1][xx];
	}
	pushdown(rt,xx);
	int mid=(l+r)/2;
	if(x>mid){
		return query(rt*2+1,mid+1,r,x,y,xx);
	}else if(y<=mid){
		return query(rt*2,l,mid,x,y,xx);
	}else{
		return query(rt*2+1,mid+1,r,mid+1,y,xx)+query(rt*2,l,mid,x,mid,xx);
	}
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		int x;
		fin>>x;
		for(int j=9;j>=0;j--){
			if(x>=(1<<j)){
				x-=(1<<j);
				num[i][j]=1;
			}
		}
	}
	for(int i=9;i>=0;i--){
		build(1,1,n,i);
	}
	for(int i=1;i<=m;i++){
		int opt,x,y;
		fin>>opt;
		if(opt==1){
			fin>>x>>y;
			for(int j=3;j>=0;j--){
				if(y>=(1<<j)){
					y-=(1<<j);
					if(num[x][j]!=1){
						rev(1,1,n,x,n,j);
					}
					num[x][j]=1;
				}else{
					if(num[x][j]!=0){
						rev(1,1,n,x,n,j);
					}
					num[x][j]=0;
				}
			}
		}else{
			ans=0;
			fin>>x>>y;
			for(int j=0;j<=2;j++){
				int qq=query(1,1,n,x,y,j);
				//cout<<j<<"  "<<qq<<endl;
				ans+=(qq+qq*(y-x+1-qq))*(1<<(j));
			}
			fout<<ans<<endl;
		}
	}
	return 0;
}
/*

in:
4
8
1 2 3 4
2 1 2
1 1 2
2 1 3
2 1 4
1 3 7
2 1 3
1 4 5
2 1 4

out:
6
11
34
23
32

*/

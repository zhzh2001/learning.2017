#include <bits/stdc++.h>
using namespace std;
const int mod=1000000007;
typedef long long ll;
int dp[1003][1003];
int power[1000003];
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	int n,m,k;
	scanf("%d%d%d",&n,&m,&k);
	power[0]=1;
	for(int i=1;i<=n+m+k;i++)
		power[i]=(ll)power[i-1]*3%mod;
	for(int i=0;i<=m;i++)
		for(int j=0;j<=k;j++){
			dp[i][j]=power[i+j];
			//printf("dp[0][%d][%d]=%d\n",i,j,dp[i][j]);
		}
	//int cur=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=m;j++)
			for(int o=0;o<=k;o++){
				int & qwq=dp[j][o];
				//printf("dp[%d][%d][%d]=%d\n",i,j,o,qwq);
				if (j){
					qwq+=dp[j-1][o];
					if (qwq>=mod)
						qwq-=mod;
				}
				if (o){
					qwq+=dp[j][o-1];
					if (qwq>=mod)
						qwq-=mod;
				}
				//printf("dp[%d][%d][%d]=%d\n",i,j,o,qwq);
			}

	printf("%d\n",dp[m][k]);
}
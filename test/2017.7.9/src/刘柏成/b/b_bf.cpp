#include <cstdio>
using namespace std;
typedef long long ll;
int a[100001],sum[100001];
int main(){
	freopen("b.in","r",stdin);
	freopen("b.ans","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i),sum[i]=a[i]^sum[i-1];
	while(m--){
		int op,x,y;
		scanf("%d%d%d",&op,&x,&y);
		if (op==1){
			int qwq=a[x]^y;
			a[x]=y;
			for(int i=x;i<=n;i++)
				sum[i]^=qwq;
		}else{
			ll ans=0;
			for(int i=x;i<=y;i++)
				for(int j=i;j<=y;j++)
					ans+=sum[j]^sum[i-1];
			printf("%lld\n",ans);
		}
	}
}
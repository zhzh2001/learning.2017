#include <bits/stdc++.h>
using namespace std;
int main(){
	mt19937 w(time(0));
	freopen("b.in","w",stdout);
	int n=500,m=500;
	printf("%d\n%d\n",n,m);
	for(int i=1;i<=n;i++)
		printf("%d ",w()%1001);
	while(m--){
		int op=w()%2+1,x=w()%n+1,y=op==1?w()%1001:w()%n+1;
		if (op==2 && x>y)
			swap(x,y);
		printf("%d %d %d\n",op,x,y);
	}
}
#pragma GCC optimize("O2")
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=10000020;
int bufpos;
char buf[MAXSIZE];
#define NEG 0
void init(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
typedef long long ll;
struct atom{
	int len,cntl[10],cntr[10],sum;
	ll ans;
	atom(){
		//len=sum=ans=0;
		//memset(cntl,0,sizeof(cntl));
		//memset(cntr,0,sizeof(cntr));
	}
	explicit atom(int x){
		len=1,ans=sum=x;
		for(int i=0;i<10;i++)
			cntl[i]=cntr[i]=(x>>i)&1;
	}
	atom& operator +=(const atom& rhs){
		ans+=rhs.ans;
		for(int i=0;i<10;i++)
			ans+=((ll)cntr[i]*(rhs.len-rhs.cntl[i])+(ll)(len-cntr[i])*rhs.cntl[i])<<i;
		for(int i=0;i<10;i++)
			cntl[i]+=(sum&(1<<i))?rhs.len-rhs.cntl[i]:rhs.cntl[i];
		for(int i=0;i<10;i++){
			if (rhs.sum&(1<<i))
				cntr[i]=len-cntr[i]+rhs.cntr[i];
			else cntr[i]+=rhs.cntr[i];
		}
		len+=rhs.len;
		sum^=rhs.sum;
		return *this;
	}
	atom operator +(const atom& rhs)const{
		atom res=*this;
		res+=rhs;
		return res;
	}
};
const int maxn=100001;
struct segtree{
	atom t[maxn*4];
	int *a;
	int n;
	void build(int o,int l,int r){
		if (l==r){
			t[o]=atom(a[l]);
			return;
		}
		int mid=(l+r)/2;
		build(o*2,l,mid);
		build(o*2+1,mid+1,r);
		t[o]=t[o*2]+t[o*2+1];
	}
	void init(int n,int *a){
		this->n=n;
		this->a=a;
		build(1,1,n);
	}
	int p,v;
	void update(int o,int l,int r){
		if (l==r){
			t[o]=atom(v);
			return;
		}
		int mid=(l+r)/2;
		if (p<=mid)
			update(o*2,l,mid);
		else update(o*2+1,mid+1,r);
		t[o]=t[o*2]+t[o*2+1];
	}
	void update(int p,int v){
		this->p=p;
		this->v=v;
		update(1,1,n);
	}
	int ql,qr;
	atom query(int o,int l,int r){
		if (ql<=l && qr>=r)
			return t[o];
		int mid=(l+r)/2;
		atom res;
		bool flag=false;
		if (ql<=mid)
			res=query(o*2,l,mid),flag=true;
		if (qr>mid){
			if (flag)
				res+=query(o*2+1,mid+1,r);
			else res=query(o*2+1,mid+1,r);
		}
		return res;
	}
	ll query(int l,int r){
		ql=l,qr=r;
		return query(1,1,n).ans;
	}
}t;
int a[maxn];
int main(){
	init();
	int n=readint(),m=readint();
	for(int i=1;i<=n;i++)
		a[i]=readint();
	t.init(n,a);
	while(m--){
		int op=readint(),x=readint(),y=readint();
		if (op==1)
			t.update(x,y);
		else printf("%lld\n",t.query(x,y));
	}
	//printf("%d\n",sizeof(t));
	//printf("%d",clock());
}
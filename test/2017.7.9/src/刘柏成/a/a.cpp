#include <cstdio>
#include <algorithm>
using namespace std;
const int INF=0x3f3f3f3f;
struct edge{
	int from,to;
	bool operator <(const edge& rhs)const{
		return from<rhs.from || (from==rhs.from && to<rhs.to);
	}
	bool operator ==(const edge& rhs)const{
		return from==rhs.from && to==rhs.to;
	}
}e[200001];
int deg[200001];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		scanf("%d%d",&e[i].from,&e[i].to);
		if (e[i].from>e[i].to)
			swap(e[i].from,e[i].to);
	}
	sort(e+1,e+m+1);
	m=unique(e+1,e+m+1)-e-1;
	for(int i=1;i<=m;i++)
		deg[e[i].from]++,deg[e[i].to]++;
	int mmin=INF;
	for(int i=1;i<=n;i++)
		mmin=min(mmin,deg[i]);
	printf("%d\n",n-mmin-1);
}
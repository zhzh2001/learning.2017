#include<iostream>
#include<cstdlib>
#include<cstring>
#include<algorithm>
using namespace std;
const int maxn=1e5*2+2333;
struct E{
	int a,b;
	bool operator < (const E q) const{
		return (a<q.a) || ((a==q.a) && (b<q.b));
	}
}edge[maxn];
int n,m,a,b,i,ans;
int num[maxn];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=m;i++){
		scanf("%d%d",&edge[i].a,&edge[i].b);
		num[edge[i].a]++;num[edge[i].b]++;
	}
	sort(edge+1,edge+1+m);
	for (i=2;i<=m;i++){
		if ((edge[i].a==edge[i-1].a) && (edge[i].b==edge[i-1].b)){
			num[edge[i].a]--;
			num[edge[i].b]--;
		}
	}
	ans=0;
	for (i=1;i<=n;i++){
		ans=max(ans,n-num[i]-1);
	}
	printf("%d\n",ans);
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstdlib>
const int maxn=1e5+100;
typedef long long LL;
struct T{
	int num[10],nxt_odd[10],nxt_two[10],pre_odd[10],pre_two[10];
	LL val_odd[10],val_two[10];
}tree[300500];

int n,m,a[maxn];


void merge(T &a,T &b,T &c){
	
	for (int i=0;i<10;i++){
		a.num[i]=b.num[i]+c.num[i];
		//updata_num
		a.val_odd[i]=(LL)b.val_odd[i]+c.val_odd[i];
		a.val_odd[i]+=(LL)b.nxt_odd[i] * c.pre_two[i];
		a.val_odd[i]+=(LL)b.nxt_two[i] * c.pre_odd[i];
		//updata_val_odd[i]
		a.val_two[i]=(LL)b.val_two[i]+c.val_two[i];
		a.val_two[i]+=(LL)b.nxt_odd[i] * c.pre_odd[i];
		a.val_two[i]+=(LL)b.nxt_two[i] * c.pre_two[i];
		//updata_val_two[i]
		
		if (b.num[i] & 1){
			a.pre_odd[i]=b.pre_odd[i]+c.pre_two[i];
			a.pre_two[i]=b.pre_two[i]+c.pre_odd[i];
		}else{
			a.pre_odd[i]=b.pre_odd[i]+c.pre_odd[i];
			a.pre_two[i]=b.pre_two[i]+c.pre_two[i];
		}
		
		if (c.num[i] & 1){
			a.nxt_odd[i]=c.nxt_odd[i]+b.nxt_two[i];
			a.nxt_two[i]=c.nxt_two[i]+b.nxt_odd[i];
		}else{
			a.nxt_odd[i]=c.nxt_odd[i]+b.nxt_odd[i];
			a.nxt_two[i]=c.nxt_two[i]+b.nxt_two[i];
		}
	}
}
void updata(int nod){
	int lson=nod <<1,rson=(nod << 1)+1;
	merge(tree[nod],tree[lson],tree[rson]);
//	printf("[%d]\n",nod);
//	for (int i=0;i<10;i++){
//		printf("%d %d--pre:(%d)(%d);;;nxt:(%d)(%d)\n",tree[nod].val_odd[i],tree[nod].val_two[i],tree[nod].pre_odd[i],tree[nod].pre_two[i],tree[nod].nxt_odd[i],tree[nod].nxt_two[i]);
//	}
//	puts("");
}
void build(int nod,int l,int r){
	if (l==r){
		int rec=a[l];
		for (int i=0;i<10;i++){
			tree[nod].num[i] = (rec & 1) ;//保存区间有多少个1 
			tree[nod].pre_odd[i]=tree[nod].val_odd[i]=tree[nod].nxt_odd[i]=rec & 1;
			tree[nod].pre_two[i]=tree[nod].val_two[i]=tree[nod].nxt_two[i]=!(rec & 1);
			rec >>= 1;
		}
		
//	printf("[%d]\n",nod);
//	for (int i=0;i<10;i++){
//		printf("%d %d--pre:(%d)(%d);;;nxt:(%d)(%d)\n",tree[nod].val_odd[i],tree[nod].val_two[i],tree[nod].pre_odd[i],tree[nod].pre_two[i],tree[nod].nxt_odd[i],tree[nod].nxt_two[i]);
//	}
//	puts("");

		return ;
	}
	int mid=(l+r) >> 1;
	build(nod*2,l,mid);
	build(nod*2+1,mid+1,r);
	updata(nod);
}

void change(int nod,int l,int r,int x,int changeval){
	if (l==r){
		int rec=changeval;
		for (int i=0;i<10;i++){
			tree[nod].num[i] = (rec & 1) ;//保存区间有多少个1 
			tree[nod].pre_odd[i]=tree[nod].val_odd[i]=tree[nod].nxt_odd[i]=rec & 1;
			tree[nod].pre_two[i]=tree[nod].val_two[i]=tree[nod].nxt_two[i]=!(rec & 1);
			rec >>= 1;
		}
//	printf("[%d]\n",nod);
//	for (int i=0;i<10;i++){
//		printf("%d %d--pre:(%d)(%d);;;nxt:(%d)(%d)\n",tree[nod].val_odd[i],tree[nod].val_two[i],tree[nod].pre_odd[i],tree[nod].pre_two[i],tree[nod].nxt_odd[i],tree[nod].nxt_two[i]);
//	}
//	puts("");
		return ;

	}
	int mid=(l+r) >> 1;
	if (x<=mid){
		change(nod*2,l,mid,x,changeval);
	} else {
		change(nod*2+1,mid+1,r,x,changeval);
	}
	updata(nod);
}

T query(int nod, int l, int r,int x,int y){
//	printf("[%d %d %d %d]\n",l,r,x,y);
	if ((l==x) && (y==r)){
		return tree[nod];
	}
	int mid=(l+r) >> 1;
	if ((l<=x) && (y<=mid)){
		return query(nod*2, l ,mid ,x ,y);
	}else
	if ((mid+1<=x) && (y<=r)){
		return query(nod*2+1,mid+1,r,x,y);
	}else{
		T temp1=query(nod*2,l,mid,x,mid);
		T temp2=query(nod*2+1,mid+1,r,mid+1,y);
		T temp3;
		merge(temp3,temp1,temp2);
		return temp3; 
	}
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
//	printf("%lf\n",(sizeof(tree)+sizeof(a)) / 1024.0 / 1024.0);
	scanf("%d",&n);
	scanf("%d",&m);
	for (int i=1;i<=n;i++) scanf("%d",&a[i]);
	build(1,1,n);
	int type,p,x,a,b;
	while (m--){
		scanf("%d",&type);
		if (type==1){
			scanf("%d%d",&p,&x);
			change(1,1,n,p,x);
		} else 
		if (type==2){
			scanf("%d%d",&a,&b);
			T ans=query(1,1,n,a,b);
			LL soo=0;
			for (int i=0,k=1;i<10;i++,k=k*2){
				soo+=(LL)ans.val_odd[i] * k;
			}
//			puts("");
			printf("%lld\n",soo);
		}
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
10位 
*/

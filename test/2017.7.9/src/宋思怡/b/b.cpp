#include <iostream>
#include <cstdio>
#define MAX 100000
using namespace std;
int n, m, k, x, y, sum, ans, a[MAX];

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i = 1;i<=n;i++)
		scanf("%d",&a[i]);
	while(m--){
		scanf("%d%d%d",&k,&x,&y);
		if (k==1)
			a[x] = y;
		else {
			sum = 0;
			for (int i = x;i<=y;i++){
				for (int j = i;j<=y;j++){
					ans = 0;
					for (int q = i;q<=j;q++)
						ans = ans xor a[q];
					sum+=ans;
				}
			}
			printf("%d\n",sum);
		}
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

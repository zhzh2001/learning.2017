#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=1e5+5;
Ll a[N];
Ll n,m,x,y,z,ans;
struct tree{
	Ll q[N],top;
	struct cs{Ll l,r,x,y,sum,v;}T[262145];
	void up(Ll id,Ll x,Ll y){
		Ll n1=T[x].r-T[x].l+1;
		Ll n2=T[y].r-T[y].l+1;
		cs o=T[x];
		T[id].v=o.v+T[y].v;
		T[id].v+=o.y*(n2-T[y].x);
		T[id].v+=(n1-o.y)*T[y].x;
		
		T[id].x=o.x;
		if(o.sum&1)T[id].x+=n2-T[y].x;else T[id].x+=T[y].x;
		
		T[id].y=T[y].y;
		if(T[y].sum&1)T[id].y+=n1-o.y;else T[id].y+=o.y;
		
		T[id].sum=o.sum+T[y].sum;
	}
	void make(Ll id,Ll l,Ll r){
		T[id].l=l;T[id].r=r;
		if(l==r){
			T[id].sum=T[id].v=T[id].x=T[id].y=a[l]%2;
			return;
		}
		Ll mid=l+r>>1;
		make(id*2  ,l,mid  );
		make(id*2+1,mid+1,r);
		up(id,id*2,id*2+1);
	}
void change(Ll id,Ll x,Ll y){
		if(T[id].l==T[id].r){
			T[id].sum=T[id].v=T[id].x=T[id].y=y;
			return;
		}
		if(T[id*2].r>=x)change(id*2,x,y);else change(id*2+1,x,y);
		up(id,id*2,id*2+1);
	}
void out(Ll id,Ll x,Ll y){
		if(x<=T[id].l&&T[id].r<=y){q[++top]=id;return;}
		if(T[id*2  ].r>=x)out(id*2  ,x,y);
		if(T[id*2+1].l<=y)out(id*2+1,x,y);
	}
Ll find(Ll x,Ll y){
		out(1,x,y);
		T[0]=T[q[1]];	
		for(Ll i=2;i<=top;i++){
			up(0,0,q[i]);
			T[0].r=T[q[i]].r;
		}
		top=0;
		return T[0].v;
	}
}T[11];

int main()
{
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(Ll i=1;i<=n;i++)scanf("%lld",&a[i]);
	for(Ll i=0;i<=10;i++){
		T[i].make(1,1,n);
		for(Ll j=1;j<=n;j++)a[j]/=2;
	}
	while(m--){
		scanf("%lld%lld%lld",&z,&x,&y);
		if(z==1){
			for(Ll i=0;i<=10;i++){
				T[i].change(1,x,y%2);
				y/=2;
			}
		}else{
			ans=0;
			for(Ll i=0;i<=10;i++)ans+=(T[i].find(x,y)*(1<<i));
			printf("%lld\n",ans);
		}
	}
}

#include<fstream>
#include<algorithm>
#include<stack>
using namespace std;
ifstream fin("tdec.in");
ofstream fout("tdec.out");
const int N=100005;
struct node
{
	int p,cnt,cost,id,dep;
	bool operator<(const node& rhs)const
	{
		return cost/dep<rhs.cost/rhs.dep;
	}
}a[N];
int rev[N],l[N],jmp[N];
long long delta[N];
bool vis[N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].p>>a[i].cnt>>a[i].cost;
		a[i].id=i;
	}
	for(int i=1;i<=n;i++)
		if(!a[i].dep)
		{
			stack<int> S;
			int now=i;
			do
				S.push(now);
			while(~(now=a[now].p)&&!a[now].dep);
			if(~now)
				a[S.top()].dep=a[now].dep+1;
			else
				a[S.top()].dep=1;
			do
			{
				int p=S.top();S.pop();
				if(!S.empty())
					a[S.top()].dep=a[p].dep+1;
			}
			while(!S.empty());
		}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		rev[a[i].id]=i;
	int cc=0,now=a[1].id;
	do
	{
		l[++cc]=now;
		vis[now]=true;
		jmp[now]=cc;
	}
	while(~(now=a[rev[now]].p));
	long long ans=0;
	for(int i=2;i<=n;i++)
		if(!vis[a[i].id])
		{
			int cnt=0;
			for(now=a[i].id;!vis[now];now=a[rev[now]].p)
			{
				cnt=max(cnt,a[rev[now]].cnt);
				vis[now]=true;
			}
			ans+=(long long)cnt*a[i].cost;
			int t=now;
			delta[jmp[now]]+=cnt;
			for(now=a[i].id;now!=t;now=a[rev[now]].p)
				jmp[now]=jmp[cc];
		}
		else
			jmp[a[i].id]=jmp[a[rev[a[i].p]].id];
	long long cnt=0,cur=0;
	for(int i=1;i<=cc;i++)
		cnt=max(cnt,a[rev[l[i]]].cnt-(cur+=delta[i]));
	ans+=a[1].cost*cnt;
	fout<<ans<<endl;
	return 0;
}
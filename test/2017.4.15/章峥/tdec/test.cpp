#include<bits/stdc++.h>
using namespace std;
ifstream fin("tdec.in");
ofstream fout("tdec.ans");
const int N=15;
int n,p[N],cnt[N],cost[N],mx,ans,sel[N],sum[N];
void dfs(int k)
{
	if(k==n+1)
	{
		memset(sum,0,sizeof(sum));
		int nowc=0;
		for(int i=1;i<=n;i++)
		{
			int now=i;
			do
				sum[now]+=sel[i];
			while(~(now=p[now]));
			nowc+=cost[i]*sel[i];
		}
		for(int i=1;i<=n;i++)
			if(sum[i]<cnt[i])
				return;
		if(nowc<ans)
		{
			ans=nowc;
			for(int i=1;i<=n;i++)
				fout<<sel[i]<<' ';
			fout<<endl;
		}
		return;
	}
	for(int i=0;i<=mx;i++)
	{
		sel[k]=i;
		dfs(k+1);
	}
}
int main()
{
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>p[i]>>cnt[i]>>cost[i];
		mx=max(mx,cnt[i]);
	}
	ans=0x3f3f3f3f;
	dfs(1);
	fout<<ans<<endl;
	return 0;
}
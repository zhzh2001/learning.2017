#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("BF.in");
ofstream fout("BF.out");
const double pi=acos(-1);
int main()
{
	int n;
	fout.precision(2);
	fout<<fixed;
	while(fin>>n&&n)
		fout<<n*n/(2*pi)<<endl;
	return 0;
}
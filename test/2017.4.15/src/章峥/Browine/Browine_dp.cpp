#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("Browine.in");
ofstream fout("Browine.out");
const int N=505;
int mat[N][N],s[N][N],f[N][N],ans[N][N];
int main()
{
	int n,m,a,b;
	fin>>n>>m>>a>>b;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>mat[i][j];
			s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+mat[i][j];
		}
	if(a==1&&b==1)
	{
		fout<<s[n][m]<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)
		{
			for(int k=1;k<=m;k++)
				f[k][1]=s[j][k]-s[i-1][k];
			for(int k=2;k<=m;k++)
				for(int c=2;c<=b;c++)
				{
					f[k][c]=0;
					for(int p=1;p<k;p++)
						f[k][c]=max(f[k][c],min(f[p][c-1],s[j][k]-s[j][p]-s[i-1][k]+s[i-1][p]));
				}
			ans[i][j]=f[m][b];
		}
	for(int i=1;i<=n;i++)
		f[i][1]=ans[1][i];
	for(int i=2;i<=n;i++)
		for(int j=2;j<=a;j++)
		{
			f[i][j]=0;
			for(int k=1;k<i;k++)
				f[i][j]=max(f[i][j],min(f[k][j-1],ans[k+1][i]));
		}
	fout<<f[n][a]<<endl;
	return 0;
}
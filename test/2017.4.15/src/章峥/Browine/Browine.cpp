#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("Browine.in");
ofstream fout("Browine.out");
const int N=505;
int n,m,a,b,mat[N][N],s[N][N];
bool check(int val)
{
	int row=0,predr=0;
	for(int i=1;i<=n;i++)
	{
		int col=0,predc=0;
		for(int j=1;j<=m;j++)
			if(s[i][j]-s[predr][j]-s[i][predc]+s[predr][predc]>=val)
			{
				if(++col==b)
					break;
				predc=j;
			}
		if(col==b)
		{
			if(++row==a)
				return true;
			predr=i;
		}
	}
	return false;
}
int main()
{
	fin>>n>>m>>a>>b;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			fin>>mat[i][j];
			s[i][j]=s[i-1][j]+s[i][j-1]-s[i-1][j-1]+mat[i][j];
		}
	int l=0,r=s[n][m]/a/b,ans=0;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid))
		{
			l=mid+1;
			ans=mid;
		}
		else
			r=mid-1;
	}
	fout<<ans<<endl;
	return 0;
}
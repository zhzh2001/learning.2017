#include<bits/stdc++.h>
using namespace std;
ofstream fout("tdec.in");
int main()
{
	default_random_engine gen(time(NULL));
	int n,c,t;
	cin>>n>>c>>t;
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		if(i==1)
			fout<<-1;
		else
		{
			uniform_int_distribution<> dp(1,i-1);
			fout<<dp(gen);
		}
		uniform_int_distribution<> dc(0,c);
		uniform_int_distribution<> dt(1,t);
		fout<<' '<<dc(gen)<<' '<<dt(gen)<<endl;
	}
	return 0;
}
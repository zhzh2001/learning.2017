#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int head[N],next[N],tail[N];
int tt,n;
long long ans=0;
long long p[N],c[N],t[N],now[N],mi[N];
void addto(int x,int y)
{
	tt++;
	next[tt]=head[x];
	head[x]=tt;
	tail[tt]=y;
}
int dfs(int k)
{
	int x=head[k];
	while(x!=0){
		int i=tail[x];
		mi[k]=min(mi[k],dfs(i));
		now[k]+=max(now[i],c[i]);
		x=next[x];
	}
	if(c[k]>now[k])ans+=(c[k]-now[k])*mi[k];
	return mi[k];
}
int main()
{
    freopen("tdec.in","r",stdin);
    freopen("tdec.out","w",stdout);
	scanf("%d",&n);
	int k=0;
	for(int i=1;i<=n;i++){
		scanf("%d%d%d",&p[i],&c[i],&t[i]);
		if(p[i]!=-1)addto(p[i],i);
		else k=i;
		mi[i]=t[i];
	}
	dfs(k);
	printf("%lld",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<map>
#include<cstring>
#include<vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
int ta[1001],s[1001][1001],v[1001][1001],n,m,a,b,d[1001][1001],f[1001][1001];
bool check(int top)
{
	int tot=0,sum=0;
	For(i,1,m)
	{
		sum+=ta[i];
		if(sum>=top)	sum=0,tot++;
		if(tot>=b)	return 1;
	}
	return 0;
}
int get(int x,int y)
{
	int sum=0;
	For(i,1,m)	ta[i]=s[y][i]-s[x-1][i],sum=sum+ta[i];
	int l=1,r=sum;
	int tmp=0;
	while(l<=r)
	{
		int mid=(l+r)>>1;
		if(check(mid))	tmp=max(tmp,mid),l=mid+1;else r=mid-1;
	}
	return tmp;
}
int main()
{
	freopen("Browine.in","r",stdin);freopen("Browine.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&a,&b);
	For(i,1,n)	For(j,1,m)	scanf("%d",&v[i][j]);
	For(i,1,n)	For(j,1,m)
		s[i][j]=s[i-1][j]+v[i][j];
	int k=n-a;
	For(i,1,n)
		For(j,i,i+k)
			d[i][j]=get(i,j);
	For(i,1,n)	f[i][0]=d[1][i];
	For(i,2,n)
	{
		For(j,1,a-1)
			For(t,1,i-1)	
				f[i][j]=max(f[i][j],min(f[t][j-1],d[t+1][i]));
	}
	printf("%d",f[n][a-1]);
} 
/*
5 4 4 2
1 2 2 1
3 1 1 1
2 0 1 3
1 1 1 1
1 1 1 1
*/

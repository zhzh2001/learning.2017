#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#include<string>
#include<map>
#include<cstring>
#include<vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
double PI=3.1415926,ans,last;
double n;
void doit(double x)
{
	ans=0;
	last=0;
	for(double r=x/(2*PI);r<=x;r+=0.0001)
	{
		double aph=(2*PI*r-x)/(2*PI*r)*360;
		bool flag=0;
		double S=((360-aph)/360)*PI*r*r;
		double s;	
		if(aph>180)	aph-=180,flag=1;
		aph/=2;
		s=r*r*sin(aph/180*PI)*cos(aph/180*PI);
		if(!flag)	S+=s;else S-=s;
		last=ans;
		ans=max(ans,S);
		if(last>=ans)	break;	
	}
	printf("%.2f,",ans);
}
int main()
{
	freopen("BF.cpp","w",stdout);
	for(double i=1;i<=100;i+=1)
		doit(i);
	
}

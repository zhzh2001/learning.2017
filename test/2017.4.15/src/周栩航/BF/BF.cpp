#include<bits/stdc++.h>
using namespace std;
const double pi=3.1415926;
int n,d;
double sqr(double a)
{
	return a*a;
}
int main()
{
    freopen("BF.in","r",stdin);
    freopen("BF.out","w",stdout);
	scanf("%d",&n);
	while(n!=0){
		double ans=0;
		ans=sqr(1.0*n*2/pi/2)*pi/2;
		printf("%.2lf\n",ans);
		scanf("%d",&n);
	}
	return 0;
}

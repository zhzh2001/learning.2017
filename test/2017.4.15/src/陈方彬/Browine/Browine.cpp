#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#define Ll long long 
using namespace std;
const int N=505;
int a[N][N],f[N][N];
int n,m,A,B;
int l,r,mid,ans;
int get(int x,int y,int xx,int yy){
	return a[y][yy]-a[y][xx-1]-a[x-1][yy]+a[x-1][xx-1];
}
bool check(int x,int y,int xx,int yy,int mid){
	return get(x,y,xx,yy)>=mid;
}
bool DFS(int x,int y,int k,int now,int mid){
	if(get(x,y,now,m)/(B-k+1)<mid)return 0;
	if(k==B)return check(x,y,now,m,mid);
	for(int j=now;j<=m-(B-k);j++)
		if(check(x,y,now,j,mid))if(DFS(x,y,k+1,j+1,mid))return 1;
	return 0;
}
bool dfs(int k,int now,int mid){
	if(f[k][now])return 0;
	if(get(now,n,1,m)/(A-k+1)/B<mid){f[k][now]=1;return 0;}
	if(k==A)return DFS(now,n,1,1,mid);
	for(int i=now;i<=n-(A-k);i++)
		if(DFS(now,i,1,1,mid))if(dfs(k+1,i+1,mid))return 1;
	f[k][now]=1;return 0;
}
int main()
{
	freopen("Browine.in","r",stdin);
	freopen("Browine.out","w",stdout);
	scanf("%d%d%d%d",&n,&m,&A,&B);
	for(int i=1;i<=n;i++)
	for(int j=1;j<=m;j++){
		scanf("%d",&a[i][j]);
		a[i][j]+=a[i-1][j]+a[i][j-1]-a[i-1][j-1];
	}
	l=0; r=a[n][m];
	while(r>=l){
		memset(f,0,sizeof f);
		mid=(l+r)/2;
		if(dfs(1,1,mid)){
			ans=max(ans,mid);
			l=mid+1;
		}else r=mid-1;
	}
	printf("%d",ans);
}

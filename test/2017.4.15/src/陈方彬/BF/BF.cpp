#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
using namespace std;
const double cnm=3.1415926;
int C;
double ans;
int main()
{
	freopen("BF.in","r",stdin);
	freopen("BF.out","w",stdout);
	while(1){
		scanf("%d",&C);
		if(!C) break;
		C=C*C;
		ans=C/cnm;
		ans/=2;
		printf("%.2lf\n",ans);
	}
}

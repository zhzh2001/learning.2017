#include <iostream>
#include <string>
#include <cstdio>
#include <cstring>
#define Ll long long 
using namespace std;
const int N=1e5+5;
struct dian{
	int fa,c,son;
	int mi,sum;
}d[N];
int q[N],l,r;
int n,m;
Ll ans;
void AKing(){
	for(int i=1;i<=n;i++)if(!d[i].son)q[++r]=i;
	while(r>l){
		int x=q[++l];
		if(d[x].sum<d[x].c){
			ans+=d[x].mi*(d[x].c-d[x].sum);
			d[x].sum=d[x].c;
		}
		int y=d[x].fa;
		if(y==-1)continue;
		d[y].mi=min(d[y].mi,d[x].mi);
		d[y].sum+=d[x].sum;
		if(--d[y].son==0)q[++r]=y;
	}
}
int main()
{
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d%d%d",&d[i].fa,&d[i].c,&d[i].mi);
		if(i!=1)d[d[i].fa].son++;			
	}	
	AKing();
	printf("%lld",ans);
}

const maxn:longint=1000000005;
var x1,y1,n,m,i,j,l,r:longint;
    a:array[0..505,0..505]of longint;
    s,ss:array[0..505,0..505]of longint;
function pd1(l,r,x:longint):boolean;
var sum1,sum2,ok,i,j:longint;
begin
  sum1:=0; sum2:=0; ok:=0;
  for i:=1 to m do
    begin
    sum1:=ss[r,i]-ss[l,i]-sum2;
    if sum1>=x then begin inc(ok); sum2:=ss[r,i]-ss[l,i]; sum1:=0; end;
    if ok=y1 then exit(true);
    end;
  exit(false);
end;
function pd(x:longint):boolean;
var pre,ok,i:longint;
begin
  pre:=0; ok:=0;
  for i:=1 to n do
    begin
    if pd1(pre,i,x)=true then begin inc(ok); pre:=i; end;
    if ok=x1 then exit(true);
    end;
  exit(false);
end;
function solve:longint;
var mid,l,r:longint;
begin
  l:=0; r:=maxn;
  while l<r do
    begin
    mid:=(l+r) div 2;
    if pd(mid)=true then l:=mid+1 else r:=mid;
    end;
  exit(l-1);
end;
begin
  assign(input,'browine.in');
  assign(output,'browine.out');
  reset(input);
  rewrite(output);
  readln(n,m,x1,y1);
  for i:=1 to n do
    begin
    for j:=1 to m do begin read(a[i,j]); s[i,j]:=s[i,j-1]+a[i,j]; end;
    readln;
    end;
  for i:=1 to m do
    for j:=1 to n do
      ss[j,i]:=ss[j-1,i]+s[j,i];
  writeln(solve);
  close(input);
  close(output);
end.

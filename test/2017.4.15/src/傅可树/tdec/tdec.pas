type pp=record
     next,link:longint;
end;
type pp1=record
     ls,rs,val:longint;
end;
var tot,n,i:longint;
    a:array[0..200005]of pp;
    h:array[0..100005]of pp1;
    sum,f:array[0..100005]of int64;
    head,p,c,t,root,d:array[0..100005]of longint;
procedure add(x,y:longint);
begin
  inc(tot); a[tot].link:=y; a[tot].next:=head[x]; head[x]:=tot;
end;
procedure swap(var x,y:longint);
var z:longint;
begin
  z:=x; x:=y; y:=z;
end;
function merge(x,y:longint):longint;
begin
  if x=0 then exit(y);
  if y=0 then exit(x);
  if h[x].val>h[y].val then swap(x,y);
  h[x].rs:=merge(h[x].rs,y);
  if d[h[x].ls]<d[h[x].rs] then swap(h[x].ls,h[x].rs);
  d[x]:=d[h[x].rs]+1;
  exit(x);
end;
procedure dfs(x:longint);
var u,now:longint;
    summ:int64;
begin
  now:=head[x]; summ:=0;
  while now<>0 do
    begin
    u:=a[now].link;
    if u=p[x] then begin now:=a[now].next; continue; end;
    dfs(u);
    root[x]:=merge(root[x],root[u]);
    root[u]:=root[x];
    f[x]:=f[x]+f[u];
    summ:=summ+sum[u];
    now:=a[now].next;
    end;
  if summ<c[x] then begin f[x]:=f[x]+(c[x]-summ)*h[root[x]].val; sum[x]:=c[x]; end else sum[x]:=summ;
end;
begin
  assign(input,'tdec.in');
  assign(output,'tdec.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
    begin
    readln(p[i],c[i],t[i]);
    if p[i]<>-1 then add(p[i],i); add(i,p[i]);
    root[i]:=i; h[i].val:=t[i];
    end;
  dfs(1);
  writeln(f[1]);
  close(input);
  close(output);
end.

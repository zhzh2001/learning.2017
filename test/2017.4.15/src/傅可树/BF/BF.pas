const pai:real=3.1415926;
var x:longint;
    ans:double;
begin
  assign(input,'BF.in');
  assign(output,'BF.out');
  reset(input);
  rewrite(output);
  readln(x);
  while x<>0 do
    begin
    ans:=sqr(x)/(2*pai);
    writeln(ans:0:2);
    readln(x);
    end;
  close(input);
  close(output);
end.

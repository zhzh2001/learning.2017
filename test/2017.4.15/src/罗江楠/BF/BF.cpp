#include <bits/stdc++.h>
#define pi 3.1415926535897931
using namespace std;
int a;
int main(){
	freopen("BF.in", "r", stdin);
	freopen("BF.out", "w", stdout);
	while(scanf("%d", &a) && a)
		printf("%.2lf\n", 1.0*a*a/pi/2);
	return 0;
}
// $$ans=\frac{S_圆}{2}=\frac{\pi r^2}{2}=\frac{\pi(\frac{C}{2\pi})^2}{2}=\frac{\frac{C^2}{4\pi}}{2}=\frac{C^2}{8\pi}=\frac{(2a)^2}{8\pi}=\frac{4a^2}{8\pi}=\frac{a^2}{2\pi} $$
// 显而易见(ノ=д=)ノ┻━┻
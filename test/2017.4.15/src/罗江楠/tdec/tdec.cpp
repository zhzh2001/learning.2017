#include <bits/stdc++.h>
#define N 100020
using namespace std;
int c[N], t[N], n, cot;
vector<int> son[N];
struct node{
	int mnc, num;
	node operator += (const node &b)  {
		mnc = min(mnc, b.mnc);
		num += b.num;
		return *this;	
	}
};
node dfs(int x){
	node ans = (node){t[x], 0};
	for(int i = 0; i < son[x].size(); i++)
		ans += dfs(son[x][i]);
	// printf("at%d: mnc=%d num=%d\n", x, ans.mnc, ans.num); // debug
	if(ans.num >= c[x]) return ans;
	else cot += (c[x]-ans.num)*ans.mnc;
	ans.num = c[x];
	return ans;
}
int main(){
	freopen("tdec.in", "r", stdin);
	freopen("tdec.out", "w", stdout);
	scanf("%d", &n);
	for(int i = 1, fa; i <= n; i++){
		scanf("%d%d%d", &fa, &c[i], &t[i]);
		if(fa == -1) continue;
		son[fa].push_back(i);
	}
	dfs(1);
	printf("%d\n", cot);
	return 0;
}
/*
5
-1 9 3
1 2 2
5 3 2
5 1 4
2 3 3

  |	q	o	>	=
--+---------------
 w|	qwq	owo	>w<	=w=
 3|	q3q	o3o	>3<	=3=

*/
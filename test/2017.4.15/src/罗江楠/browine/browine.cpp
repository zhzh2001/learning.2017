#include <bits/stdc++.h>
#define N 520
using namespace std;
int mp[N][N], s[N], id[N], n, m, a, b, to[N], die[N][N];
// void pr(){for(int i=1;i<=n;i++,puts(""))for(int j=1;j<=m;j++)printf("%d ",mp[i][j]);puts("id:");for(int i=1;i<=n;i++)printf("%d ",id[i]);puts("\ns:");for(int i=1;i<=n;i++)printf("%d ",s[i]);puts("");}
int main(){
	freopen("browine.in", "r", stdin);
	freopen("browine.out", "w", stdout);
	scanf("%d%d%d%d", &n, &m, &a, &b);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++){
			scanf("%d", &mp[i][j]);
			s[i] += mp[i][j];
		}
	for(int i = 1; i <= n; i++)
		id[i] = i;
	for(int i = 1; i <= n-a; i++){
		int mn = 1<<30, at;
		for(int j = 1; j < n; j++){
			if(id[j] == id[j+1]) continue;
			if(s[id[j]]+s[id[j+1]] < mn)
				at = j, mn = s[id[j]]+s[id[j+1]];
		}
		int k = id[at+1], ls = id[at];
		while(id[at+1]==k){
			id[++at] = ls;
			s[ls] += s[at];
			s[at] = 0;
			for(int j = 1; j <= m; j++){
				mp[ls][j] += mp[at][j];
				mp[at][j] = 0;
				die[at][j] = 1;
			}
		}
		// printf("Times:%d\n", i);pr();
	}
	for(int i = 1; i <= n; i++){
		if(id[i] == id[i-1]) continue;
		for(int j = 1; j <= m; j++)
			to[j] = j;
		for(int j = 1; j <= m-b; j++){
			int mn = 1<<30, at;
			for(int k = 1; k < m; k++){
				if(to[k] == to[k+1]) continue;
				if(mp[i][to[k]]+mp[i][to[k+1]] < mn)
					at = k, mn = mp[i][to[k]]+mp[i][to[k+1]];
			}
			int k = to[at+1], ls = to[at];
			while(to[at+1]==k){
				to[++at] = ls;
				mp[i][ls] += mp[i][at];
				mp[i][at] = 0;
				die[i][at] = 1;
			}
		}
	}
	int ans = 1<<30;
	for(int i = 1; i <= n; i++)if(id[i]==i){
		for(int j = 1; j <= m; j++)
			if(!die[i][j]) ans = min(mp[i][j], ans);
	}
	// pr();
	printf("%d\n", ans);
}
/*
# Simple Input
5 4 4 2
1 2 2 1
3 1 1 1
2 0 1 3
1 1 1 1
1 1 1 1
 
# Simple Output
20
 
# Simple Input
8 4 2 2
50 50 5550 50
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 1
 
# Simple Output
14
 
# Simple Input
5 5 4 5
5 5 5 5 5
5 5 5 5 1
5 5 0 5 5
5 5 0 5 5
5 5 5 1 1
 
# Simple Output
0
 
*/
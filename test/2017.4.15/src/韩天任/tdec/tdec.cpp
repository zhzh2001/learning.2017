#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=100005;
int Mn[N],num[N],c[N],t[N];
vector<int> son[N];
int n,ans;

void dfs(int x){
	if(son[x].size()==0){
		Mn[x]=t[x];
		ans+=Mn[x]*c[x];
		num[x]=c[x];
		return;
	}
	for(int i=0;i<son[x].size();i++){
		int v=son[x][i];
		dfs(v); num[x]+=num[v];
		Mn[x]=min(Mn[x],Mn[v]);
	}
	if(num[x]<c[x]){
		ans+=(c[x]-num[x])*Mn[x];
		num[x]=c[x];
	}
}

int main(){
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	n=read();int a=read();
	c[1]=read(); t[1]=read();
	for(int i=2;i<=n;i++){
		a=read(); c[i]=read(); t[i]=read();
		son[a].push_back(i);
	}
	memset(Mn,0x3f,sizeof Mn); dfs(1);
	printf("%d\n",ans);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
map<int,int> son[2000010];
int n,fa[100010],c[100010],b[100010],t[100010],zz;
ll ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int dfs(int x)
{
	int y=t[x];
	if(son[x][0]==0)
	{
		ans+=(c[x]*t[x]);
		b[fa[x]]-=c[x];
		return t[x];
	}
	For(i,1,son[x][0])y=min(dfs(son[x][i]),y);
	ans+=(max(b[x],0)*y);
	b[fa[x]]-=(c[x]-min(0,b[x]));
	return y;	
}
int main()
{
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	n=read();
	For(i,1,n)son[i][0]=0;
	For(i,1,n)
	{
		fa[i]=read();
		c[i]=read();
		b[i]=c[i];
		t[i]=read();
		if(fa[i]==-1)continue;
		son[fa[i]][0]++;
		son[fa[i]][son[fa[i]][0]]=i;
	}
	zz=dfs(1);
	cout<<ans<<endl;
	return 0;
}

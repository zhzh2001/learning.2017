#include<cstdio>
#include<cmath>
#include<algorithm>
#include<cstring>
#define N 100005
using namespace std;
long long n,pre[N],c[N],t[N],ans,root,front[N],to[N],next[N],line,f[N],MIN[N];
void link(long long x,long long y)
{
	line++;to[line]=y;next[line]=front[x];front[x]=line;
}
void dfs(long long x)
{long long i,j,k,tt=0;
	MIN[x]=min(MIN[x],t[x]);
	for(i=front[x];i!=-1;i=next[i])
	  {
	  	dfs(to[i]);
		MIN[x]=min(MIN[x],MIN[to[i]]);
	  	f[x]+=f[to[i]];tt+=c[to[i]];
	  }
	if(tt>=c[x]) c[x]=tt;else f[x]+=(c[x]-tt)*MIN[x];
	if(f[x]<0)
	  f[x]=f[x];
}
long long read()
{char c;long long t=0,s=1;
 c=getchar();
 while(!((c>='0' && c<='9')||c=='-')) c=getchar();
 if(c=='-') s=-1,c=getchar();
 while(c>='0' && c<='9')
   {
   	t=t*10+c-48;c=getchar();
   } 
return t*s;
}
int main()
{long long i,j,k;
    freopen("tdec.in","r",stdin);
    freopen("tdec.out","w",stdout);
    //scanf("%d",&n);
    n=read();
    line=-1;memset(front,-1,sizeof(front));
    memset(MIN,63,sizeof(MIN));
	for(i=1;i<=n;i++)
	  {
	  	//scanf("%d%d%d",&pre[i],&c[i],&t[i]);
	  	pre[i]=read();c[i]=read();t[i]=read();
		if(pre[i]==-1) root=i;else link(pre[i],i);
	  }
	dfs(root);
	printf("%I64d\n",f[root]);
}

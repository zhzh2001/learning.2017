#include<bits/stdc++.h>
using namespace std;
const double pi=acos(-1);
inline double sqr(double x){return x*x;}
int main()
{
	freopen("BF.in","r",stdin);
	freopen("BF.out","w",stdout);
	int n;
	while(1){
		scanf("%d",&n);
		if(n==0)break;
		double nn=(double)n,ans=pi*sqr(nn/pi)/2;
		printf("%.2lf\n",ans);
	}
	return 0;
}

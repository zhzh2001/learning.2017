#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
ll n,rt,ans=0,c[100001],t[100001],f[100001]={0},mi[100001];
ll nedge=0,p[100001],nex[100001],head[100001];
inline void addedge(ll a,ll b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(ll x){
	ll sum=0;
	for(ll k=head[x];k;k=nex[k]){
		dfs(p[k]);
		mi[x]=min(mi[x],mi[p[k]]);
		sum+=f[p[k]];
	}
	if(sum<t[x])ans+=mi[x]*(t[x]-sum);
	f[x]=max(t[x],sum);
}
int main()
{
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	n=read();
	for(ll i=1;i<=n;i++){
		ll x=read();t[i]=read();c[i]=read();
		mi[i]=c[i];
		if(x==-1)rt=i;
		else addedge(x,i);
	}
	dfs(rt);
	printf("%I64d",ans);
	return 0;
}

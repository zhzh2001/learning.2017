#include<bits/stdc++.h>
using namespace std;
typedef long long ll;
inline ll read(){
	ll k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
ll n,m,a,b,sum=0,s[501][501];
inline bool excheck(ll i,ll j,ll x){
	ll cnt=0,l=1,r=1;
	while(r<=m){
		ll le=s[j][l-1]-s[i-1][l-1],re=s[j][r]-s[i-1][r];
		r++;
		if(re-le>=x)cnt++,l=r;
	}
	if(cnt<b)return 0;
	return 1;
}
inline bool check(ll x){
	ll cnt=0,l=1,r=1;
	while(r<=n){
		if(s[r][m]-s[l-1][m]<x)r++;
		else{
			if(excheck(l,r,x))cnt++,l=r=r+1;
			else r++;
		}
	}
	if(cnt<a)return 0;
	return 1;
}
int main()
{
	freopen("Browine.in","r",stdin);
	freopen("Browine.out","w",stdout);
	ll x;
	n=read();m=read();a=read();b=read();
	for(ll i=1;i<=n;i++)
		for(ll j=1;j<=m;j++)sum+=(x=read()),s[i][j]=s[i-1][j]+s[i][j-1]+x-s[i-1][j-1];
	ll l=0,r=sum,ans=0;
	while(l<=r){
		ll mid=(l+r)>>1;
		if(check(mid))ans=max(ans,mid),l=mid+1;
		else r=mid-1;
	}
	printf("%I64d",ans);
	return 0;
}

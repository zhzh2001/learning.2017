#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
#define pie 3.14159265358979323
using namespace std;
int n,y;
long double x;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("BF.in","r",stdin);
	freopen("BF.out","w",stdout);
	n=read();
	while(n>0)
	{
		x=n*n;
		x/=2;
		x/=pie;
		y=trunc(x*1000);
		if(y%10>4)y+=10;
		y/=10;
		cout<<y/100<<".";
		if(y%100<10)cout<<0;
		cout<<(y%100)<<endl;
		n=read();
	}
	return 0;
}


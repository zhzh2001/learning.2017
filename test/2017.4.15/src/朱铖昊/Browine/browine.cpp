#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	x=0;
	char c=getchar();
	while (c>'9'||c<'0')
		c=getchar();
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
}
#define ll long long
#define iter iterator
inline void write(int x)
{
	if (x==0)
	{
		putchar('0');
		putchar('\n');
	}
	static int a[10];
	a[0]=0;
	while (x!=0)
	{
		a[++a[0]]=x%10;
		x/=10;
	}
	while (a[0])
	{
		putchar(a[a[0]]+'0');
		--a[0];
	}
}
int a[505][505],n,m,x,y,sum,l,r;
bool pd1(int s,int x,int mid)
{
	int l=1;
	int sum=0;
	for (int r=1;r<=m;++r)
		if (a[x][r]-a[x][l-1]-a[s-1][r]+a[s-1][l-1]>=mid)
		{
			++sum;
			l=r+1;
		}
	return sum>=y;
}
bool pd(int mid)
{
	int l=1;
	int sum=0;
	for (int r=1;r<=n;++r)
		if (a[r][m]-a[l-1][m]>=y*mid && pd1(l,r,mid))
		{
			++sum;
			l=r+1;
		}
	return sum>=x;
}
int main()
{
	freopen("browine.in","r",stdin);
	freopen("browine.out","w",stdout);
	read(n);
	read(m);
	read(x);
	read(y);
	for (int i=1;i<=n;++i)
		for (int j=1;j<=m;++j)
		{
			read(a[i][j]);
			sum+=a[i][j];
			a[i][j]=a[i-1][j]+a[i][j-1]-a[i-1][j-1]+a[i][j];
		}
	r=sum/(x*y);
	l=0;
	while (l!=r)
	{
		int mid=(l+r+1)/2;
		if (pd(mid))
			l=mid;
		else
			r=mid-1;
	}
	write(l);
	return 0;
}
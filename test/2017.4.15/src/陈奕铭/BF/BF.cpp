#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const double pai=3.1415926;
int n;
double ans;

int main(){
	freopen("BF.in","r",stdin);
	freopen("BF.out","w",stdout);
	while(scanf("%d",&n)!=EOF){
		if(n==0) return 0;
		ans=n*n/pai/2;
		printf("%.2lf\n",ans);
	}
	return 0;
}

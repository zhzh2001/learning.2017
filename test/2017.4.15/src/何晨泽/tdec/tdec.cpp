//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
const int N=100005;
struct node {
	int n,l;
} tree[N];
int pi,t[N],c[N];
ff z[N],mi[N],son[N],sum[N];
ff i,j,k,l,m,n,u,las,now,ans,fir,min_,sum_;
inline void BFS() {
	fir=0;
	las=1;
	z[1]=1;
	while (las<n) {
		fir++;
		u=son[z[fir]];
		while (u>0) {
			las++;
			z[las]=tree[u].l;
			u=tree[u].n;
		}
	}
}
int main() {
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	std::ios::sync_with_stdio(false);
	scanf ("%d",&n);
	Rep(i,1,n) {
		scanf ("%d%d%d",&pi,&c[i],&t[i]);
		if (i==1) continue;
		tree[i].l=i;
		tree[i].n=son[pi];
		son[pi]=i;
	}
	BFS();
	ans=0;
	Drp(i,n,1) {
		now=z[i];
		u=son[now];
		min_=t[now];
		sum_=0;
		while (u!=0) {
			sum_+=sum[tree[u].l];
			min_=min(min_,mi[tree[u].l]);
			u=tree[u].n;
		}
		if (sum_<c[now]) {
			sum[now]=c[now];
			ans+=(c[now]-sum_)*min_;
		} else sum[now]=sum_;
		mi[now]=min_;
	}
	cout<<ans<<endl;
}
/*
5
-1 9 3
1 2 2
5 3 2
5 1 4
2 3 3
*/

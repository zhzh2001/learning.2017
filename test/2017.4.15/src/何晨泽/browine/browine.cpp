//Live long and prosper.
#pragma GCC optimize("-O2")
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int a[501][501];
int i,j,k,l,r,m,n,A,B,mid,ans;
inline int read() {
	int x=0,f=1;
	char ch=getchar();
	while (ch>'9'||ch<'0') {
		if (ch=='-') f=-1;
		ch=getchar();
	}
	while (ch<='9'&&ch>='0')
		x=x*10+ch-'0',ch=getchar();
	return x*f;
}
inline bool Judge(int now) {
	int i=0,j=0,x=0,y=0,total=0,lasti=0;
	while (i<n) {
		i++;
		y=j=0;
		while ((j<m) && (y<B)) {
			total=0;
			while ((total<now) && (j<m)) {
				j++;
				Rep(ii,lasti+1,i) total+=a[ii][j];
			}
			if (total>=now) y++;
		}
		if (y>=B) x++,lasti=i;
	}
	if (x>=A) return true;
	else return false;
}
int main() {
	freopen("browine.in","r",stdin);
	freopen("browine.out","w",stdout);
	scanf ("%d%d%d%d",&n,&m,&A,&B);
	Rep(i,1,n) Rep(j,1,m) {
		scanf ("%d",&a[i][j]);
		r+=a[i][j];
	}
	l=0;
	while (l<=r) {
		mid=(l+r)/2;
		if (Judge(mid)==true) {
			l=mid+1;
			ans=mid;
		} else {
			r=mid-1;
		}
	}
	cout<<ans<<endl;
	return 0;
}
/*
5 4 4 2
1 2 2 1
3 1 1 1
2 0 1 3
1 1 1 1
1 1 1 1
*/

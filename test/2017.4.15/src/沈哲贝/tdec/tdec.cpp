#include<algorithm>
#include<memory.h>
#include<cstdio>
#define ll long long
#define maxn 100010
#define mod 1000000007
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll next[maxn],head[maxn],vet[maxn],cost[maxn],need[maxn],tot,answ,rt,n;
void insert(ll x,ll y){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;
}
void dfs(ll x){
	ll son_need=0;
	for(ll i=head[x];i;i=next[i]){
		dfs(vet[i]);
		son_need+=need[vet[i]];
		cost[x]=min(cost[x],cost[vet[i]]);
	}
	if (son_need<=need[x])	answ+=cost[x]*(need[x]-son_need);
	else	need[x]=son_need;
}
int main(){
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	n=read();
	For(i,1,n){
		ll pre=read();	need[i]=read();	cost[i]=read();
		if (pre==-1)	rt=i;	else	insert(pre,i);
	}
	dfs(rt);
	writeln(answ);
}

#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#include<map>
#define maxn 510
#define ll int
#define inf 10000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll n,m,A,B,mp[maxn][maxn];
bool pd(ll x,ll y,ll val){
	ll last=0,sum=0;
	For(i,1,m)	if (mp[y][i]-mp[x][i]-mp[y][last]+mp[x][last]>=val)	++sum,last=i;
	return sum>=B;
}
bool calc(ll x){
	ll last=0,sum=0;
	For(i,1,n)	if (pd(last,i,x))	++sum,last=i;
	return sum>=A;
}
int main(){
	freopen("browine.in","r",stdin);
	freopen("browine.out","w",stdout);
	n=read();	m=read();	A=read();	B=read();
	For(i,1,n)	For(j,1,m)	mp[i][j]=read()-mp[i-1][j-1]+mp[i-1][j]+mp[i][j-1];
	ll l=1,r=mp[n][m]/A/B,answ=0;
	while(l<=r){
		ll mid=(l+r)>>1;
		if (calc(mid))	l=mid+1,answ=mid;
		else	r=mid-1;
	}
	writeln(answ);
}

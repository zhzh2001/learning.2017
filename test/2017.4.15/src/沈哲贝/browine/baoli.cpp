#include<algorithm>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define maxn 510 
#define ll int
#define inf 10000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ll f[maxn][maxn],mp[maxn][maxn],n,m,A,B;
inline bool pd(ll v,ll x,ll y){
	ll last=0,sum=0;
	For(i,1,m)	if (mp[y][i]-mp[x-1][i]-mp[y][last]+mp[x-1][last]>=v)	sum++,last=i;
	return sum>=B;
}
inline void work(ll x,ll y){
	ll l=1,r=(mp[y][m]-mp[x-1][m])/B;
	while(l<=r){
		ll mid=(l+r)>>1;
		if (pd(mid,x,y))	l=mid+1,f[x][y]=mid;
		else	r=mid-1;
	}
}
inline ll calc(ll x){
	ll last=1,sum=0;
	For(i,1,n)	if (f[last][i]>=x)	sum++,last=i+1;
	return sum>=A;
}
int main(){
	freopen("browine.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	m=read();	A=read();	B=read();
	For(i,1,n)	For(j,1,m)	mp[i][j]=read()-mp[i-1][j-1]+mp[i][j-1]+mp[i-1][j];
	work(1,n);
	pd(11,1,n);
	For(i,1,n)	For(j,i,n)	work(i,j);
	ll l=1,r=mp[n][m]/A/B,ans=0;
	while(l<=r){
		ll mid=(l+r)>>1;
		if (calc(mid))	ans=mid,l=mid+1;
		else	r=mid-1;
	}
	writeln(ans);
}

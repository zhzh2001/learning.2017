#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
#include<queue>
#define ll int 
#define ld double
#define maxn 800010
#define inf 100000000
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
#define pa pair<ll,ll>
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
void writeln(ll x){	write(x);	puts("");	}
ld pi=3.1415926535897932384*2;
int main(){
	freopen("bf.in","r",stdin);
	freopen("bf.out","w",stdout); 
	ll n=read();
	while(n){
		printf("%.2lf\n",(ld)n*n/pi);
		n=read();
	}
}

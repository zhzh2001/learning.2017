#include<cstdio>
#include<algorithm>
#include<cmath>
#define pi 3.1415926535897932384
#define ll int
#define ld double
#define maxn 600010
#define inf 100000000
#define eps 1e-9
#define For(i,x,y)	for(ll i=x;i<=y;++i)
#define FOr(i,x,y)	for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){	ll x=0,f=1;char ch=getchar();	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}	return x*f;	}
inline void write(ll x){	if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);	putchar(x%10+'0');	}
inline void writeln(ll x){	write(x);	puts("");	}
ld calc(ll x){
	ld l=0,r=x,mid;
	while(abs(r-l)>eps){
		mid=(l+r)/2;
		if (pi*mid>x)	r=mid;
		else	l=mid;
	}
	return pi*mid*mid/2;
}
int main(){
	freopen("bf.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll n=read();
	while(n){
		printf("%.2lf\n",calc(n));
		n=read();
	}
}

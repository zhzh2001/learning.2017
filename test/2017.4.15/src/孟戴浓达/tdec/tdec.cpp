//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define debug cout<<"sb"<<endl;
using namespace std;
ifstream fin("tdec.in");
ofstream fout("tdec.out");
int to[100003],head[100003],next[100003],tot;
void add(int x,int y){
	to[++tot]=y;
	next[tot]=head[x];
	head[x]=tot;
}
bool isfa[100003];
long long need[100003],maxcost[100003],minc[100003];
long long c[100003],p[100003],t[100003];
void dfs(int x){
	minc[x]=99999999;
	need[x]=c[x];
	if(isfa[x]==true){
		long long hehe=c[x];
		long long zongneed=0;
		for(int i=head[x];i;i=next[i]){
			dfs(to[i]);
			minc[x]=min(minc[x],minc[to[i]]);
			maxcost[x]+=maxcost[to[i]];
			zongneed+=need[to[i]];
		}
		need[x]=max(zongneed,need[x]);
		if(zongneed<c[x]){
			maxcost[x]+=minc[x]*(c[x]-zongneed);
		}
	}else{
		need[x]=c[x];
		maxcost[x]=c[x]*t[x];
		minc[x]=t[x];
	}
}
int n,gen;
int main(){
	fin>>n;
	for(int i=1;i<=n;i++){
		fin>>p[i]>>c[i]>>t[i];
		if(p[i]==-1){
			gen=i;
		}else{
			isfa[p[i]]=true;
			add(p[i],i);
		}
	}
	dfs(gen);
	fout<<maxcost[gen]<<endl;
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
5
-1 9 3
1 2 2
5 3 2
5 1 4
2 3 3

out:
20

*/

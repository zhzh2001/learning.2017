#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#define debug cout<<"sb"<<endl;
using namespace std;
//ifstream fin("Browine.in");
//ofstream fout("Browine.out");
int n,m,a,b;
long long sum[503][503];
long long jiyi[503][503];
inline long long read(){
	long long k=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9')k=k*10+ch-'0',ch=getchar();
	return f*k;
}
inline long long max(long long a,long long b){
	if(a>b)return a;
	else return b;
}
inline long long min(long long a,long long b){
	if(a>b)return b;
	else return a;
}
int daijia(int x,int y){
	int zong=sum[y][m]-sum[x-1][m];
	int block=zong/b;
	int the_sum=0;
	int fanhui=99999999;
	for(int i=1;i<=n;i++){
		the_sum+=sum[y][i]-sum[x-1][i]-sum[y-1][i-1]+sum[x-1][i-1];
		if(the_sum>=block){
			fanhui=min(the_sum,fanhui);
			the_sum=0;
		}
	}
	return fanhui;
}
long long dp(int front,int now){
	long long& fanhui=jiyi[front][now];
	if(fanhui!=99999999)       return fanhui;
	if(front<now||front==0)    return fanhui=999999999;
	if(now==1)                 return fanhui=sum[front][m];
	fanhui=999999999;
	for(int k=2;k<=front;k++) fanhui=min(min(dp(k-1,now-1),daijia(k,front)),fanhui);
	return fanhui;
}
int main(){
	//freopen("Browine.in","r",stdin);
	//freopen("Browine.out","w",stdout);
	n=read(),m=read(),a=read(),b=read();
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			sum[i][j]=sum[i-1][j]+sum[i][j-1]-sum[i-1][j-1]+read();
		}
		for(int j=0;j<=a+1;j++){
			jiyi[i][j]=99999999;
		}
	}
	int ans=dp(n,a);
	cout<<ans<<endl;
	//fin.close();
	//fout.close();
	return 0;
}
/*
in:
5 4 4 2
1 2 2 1
3 1 1 1
2 0 1 3
1 1 1 1
1 1 1 1
out:
3
*/


/*
in:
3 3 2 2
1 1 1
1 1 1
1 1 99
out:
2
*/


/*
in:
4 4 2 2
1 1 1 1
1 1 1 1
1 1 1 1
1 1 1 9999
out:
4
*/

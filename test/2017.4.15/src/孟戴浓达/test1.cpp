#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<iomanip>
using namespace std;
//ifstream fin("BF.in");
//ofstream fout("BF.out");
const long double pi=3.1415926535;
long double ans,x,r,d;
int main(){
	while(cin>>x&&x!=0){
		x=x*1.0;
		d=x/pi;
		r=d/2;
		ans=pi*(r*r*1.0);
		ans=ans*2;
		cout<<fixed<<setprecision(2)<<ans<<endl;
	}
	//fin.close();
	//fout.close();
	return 0;
}
/*

in:
1
100
0

out:
0.16
1591.55

*/

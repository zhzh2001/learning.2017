#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <cassert>
using namespace std;

ifstream fin("tdec.in");
ofstream fout("tdec.out");

const int N = 100005;

typedef long long ll;

int head[N], cnt;
struct edgetype {
	int to, nxt;
} e[N * 2];

inline void AddEdge(int u, int v) {
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	head[u] = cnt;
}

bool vis[N];
ll sz[N], c[N], t[N], mn[N], tot[N];

void dfs1(int now) {
	vis[now] = true;
	mn[now] = t[now];
	for (int i = head[now]; ~i; i = e[i].nxt) {
		if (!vis[e[i].to]) {
			dfs1(e[i].to);
			mn[now] = min(mn[now], mn[e[i].to]);
		}
	}
	vis[now] = false;
}

void dfs2(int now) {
	vis[now] = true;
	sz[now] = 0;
	for (int i = head[now]; ~i; i = e[i].nxt) {
		if (!vis[e[i].to]) {
			dfs2(e[i].to);
			sz[now] += sz[e[i].to];
			tot[now] += tot[e[i].to];
		}
	}
	if (c[now] > sz[now]) {
		int delta = c[now] - sz[now];
		// cout << now << ' ' << delta << ' ' << mn[now] << endl;
		tot[now] += (ll)mn[now] * delta;
		sz[now] += delta;
	}
	vis[now] = false;
}

int main() {
	memset(head, -1, sizeof head);
	int n;
	fin >> n;
	int p, root;
	for (int i = 1; i <= n; i++) {
		fin >> p >> c[i] >> t[i];
		if (p == -1) root = i;
		else {
			AddEdge(p, i);
		}
	}
	memset(mn, 0x3f, sizeof mn);
	dfs1(root);
	memset(vis, 0, sizeof vis);
	dfs2(root);
	fout << tot[root] << endl;
	// cout << tot[root] << endl;
	return 0;
}
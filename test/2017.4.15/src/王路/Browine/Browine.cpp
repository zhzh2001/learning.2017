#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <fstream>
#include <cassert>
using namespace std;

ifstream fin("Browine.in");
ofstream fout("Browine.out");

const int N = 505, M = 505;
int mat[N][M], l[N];
int cL[N], sum;
struct data {
	int a, b, val;
	bool operator < (const data & x) const {
		return val > x.val;
	}
} r[N];

int n, m, a, b;

int tmp[N];

inline bool checkLine(int x, int y, int k) {
	memset(tmp, 0, sizeof tmp);
	for (int j = 1; j <= m; j++) {
		tmp[j] = mat[y][j] - mat[x - 1][j];
		tmp[j] += tmp[j - 1];
	}
	int cut = 0;
	for (int i = 1, last = 0; i <= m; i++) {
		if (tmp[i] - tmp[last] >= k) {
			last = i;
			cut++;
		}
	}
	return cut >= b;
}

inline bool check(int k) {
	int cut = 0;
	// bool flag = false;
	for (int i = 1, last = 0; i <= n; i++) {
		if (l[i] - l[last] >= k * b && checkLine(last + 1, i, k)) {
			last = i;
			cut++;
		}
	}
	return cut >= a;
}

int main() {
	fin >> n >> m >> a >> b;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			fin >> mat[i][j];
			l[i] += mat[i][j];
		}
	}
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			mat[i][j] += mat[i - 1][j];
			// cout << mat[i][j] << ' ';
		}
		// cout << endl;
	}
	for (int i = 1; i <= n; i++) sum += l[i], l[i] += l[i - 1];
	int l = 0, r = sum / (a * b), ans = 0;
	while (l <= r) {
		int mid = (l + r) / 2;
		if (check(mid)) ans = mid, l = mid + 1;
		else r = mid - 1;
	}
	fout << ans << endl;
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
#include <fstream>
using namespace std;

ifstream fin("BF.in");
ofstream fout("BF.out");

const long double eps = 1e-5;
const long double pi = 3.1415926535897932384626433832795;

int main() {
	int n;
	while (fin >> n && n) {
		fout.precision(2);
		fout << fixed << (long double)((n / pi) * (long double)(n / 2.0)) << endl;
	}
	return 0;
}
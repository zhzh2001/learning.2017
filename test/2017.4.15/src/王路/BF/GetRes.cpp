#include <bits/stdc++.h>
using namespace std;

// ifstream fin("BF.in")
ofstream fout("BF.res");

const long double pi = (long double)3.1415926535897932384626433832795;
const long double eps = 1e-5;

void work(int k) {
	fout << setprecision(0) << fixed << (long double)((k / pi) * (long double)(k / 2.0)) * 100 << ",";
}

int main() {
	// work(100);
	for (int i = 1; i <= 100; i++) {
		work(i);
	}
	return 0;
}
#include<cstdio>
int loc,x,i,j,n,m,a,b,le,st,ls,lb,ar,fl,r,l,mid,v;
long long f[4010][4010];
char c;
inline int R(){
	c=getchar(),v=0;
	while(c>'9'||c<'0')c=getchar();
	while(c<='9'&&c>='0')v=v*10+c-'0',c=getchar();
	return v;
}
inline int lct(int st,int ls,long long tar){
	if(f[st+ls-1][m]-f[st-1][m]<tar)return -1;
	int l=1,r=m,mid;
	while(l<=r){
		mid=l+r>>1;
		if(f[st+ls-1][mid]-f[st-1][mid]>=tar)r=mid-1;
		else l=mid+1;
	}return r+1;
}
inline int avb(int upl){
	le=0;
	for(int i=1;i<=a;i++){
		st=le+1;
		if(st>n)return 0;
		ls=ar=0;
		while(!ar){
			ls++;
			if(st+ls-1>n)return 0;
			lb=fl=0;
			for(int j=1;j<b;j++){
				loc=lct(st,ls,upl+lb);
				if(loc==-1){fl=1;break;}
				lb=f[st+ls-1][loc]-f[st-1][loc];
			}
			if(!fl&f[st+ls-1][m]-f[st-1][m]>=lb+upl)ar=1;
		}
		le=st+ls-1;
	}return 1;	
}
int main()
{
	freopen("browine.in","r",stdin);
	freopen("browine.out","w",stdout);
	n=R(),m=R(),a=R(),b=R();
	for(i=1;i<=n;i++)
		for(j=1;j<=m;j++)
			x=R(),f[i][j]=f[i][j-1]+f[i-1][j]-f[i-1][j-1]+x;
	r=f[n][m]/(a*b);
	l=0;
	while(l<=r){
		mid=l+r>>1;
		if(avb(mid))l=mid+1;
		else r=mid-1;
	}printf("%d\n",l-1);
}

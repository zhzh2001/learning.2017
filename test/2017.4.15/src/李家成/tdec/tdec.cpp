#include<algorithm>
#include<cstdio>
const ll N=100010;
ll n,i,p,rt,aq[N],pr[N],v[N],pt[N],nxt[N],cnt;
long long ans,pos[N];
inline ll add(ll x,ll y){
	v[++cnt]=y,nxt[cnt]=pt[x],pt[x]=cnt;
}
inline void cutup(ll u,ll f){
	for(ll i=pt[u];i;i=nxt[i])
		cutup(v[i],u),pr[u]=std::min(pr[u],pr[v[i]]);
	if(aq[u]>pos[u])ans+=(long long)(aq[u]-pos[u])*pr[u],pos[f]+=aq[u];
	else pos[f]+=pos[u];
}
ll main()
{
	freopen("tdec.in","r",stdin);
	freopen("tdec.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++){
		scanf("%d%d%d",&p,&aq[i],&pr[i]);
		if(p==-1)rt=i;
		else add(p,i);
	}
	cutup(rt,0);	
	printf("%I64d\n",ans);
}

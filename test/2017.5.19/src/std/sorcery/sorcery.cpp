#include <cstdio>
#include <cstring>

using namespace std;

const int MAXN = 51, MAXS = 101, MAXL = 101;
const int MOD = 1000000007;

struct Node{
	int nxt[26];
	int fail;
	bool ed;
} tr[MAXS];//tr[0] is root

char st[MAXS];
char word[MAXN][MAXS];
int len[MAXN];
int que[MAXS];
int tar[MAXS][MAXN];
int dp[MAXL][MAXS];
int id[MAXS], adj[MAXS][MAXS];
int a[MAXS*2][MAXS*2], b[MAXS*2][MAXS*2], c[MAXS*2][MAXS*2];
int sz, n, m, L, ans;

void tr_ins()
{
	int l = strlen(st), p = 0;
	for (int i = 0; i < l; i ++){
		int ch = st[i] - 'a';
		if (ch < 0 || ch >= 26){
			puts("error!"); return;
		}
		if (!tr[p].nxt[ch])
			tr[p].nxt[ch] = sz ++;
		p = tr[p].nxt[ch];
	}
	tr[p].ed = true;
}

void tr_bfs()
{
	int h = 0, t = 0; que[0] = 0;
	while (h <= t){
		int u = que[h ++], v;
		for (int i = 0; i < 26; i ++){
			int x = tr[u].fail;
			while (x && !tr[x].nxt[i])
				x = tr[x].fail;
			if (u && tr[x].nxt[i])
				x = tr[x].nxt[i];
			if (v = tr[u].nxt[i]){
				tr[v].fail = x;
				tr[v].ed |= tr[x].ed;
				que[++ t] = v;
			} else {
				tr[u].nxt[i] = x;
			}
		}
	}
}

void run_word()
{
	for (int i = 0; i < sz; i ++)//node[i]
		for (int j = 0; j < n; j ++){//word[j]
			int p = i;
			for (int k = 0; k < len[j] && !tr[p].ed; k ++)
				p = tr[p].nxt[word[j][k] - 'a'];
			tar[i][j] = p;
		}
}

void solve1()
{
	memset(dp, 0, sizeof(dp));
	dp[0][0] = 1;
	for (int i = 0; i < L; i ++)
		for (int j = 0; j < sz; j ++)
			if (dp[i][j]){
				for (int k = 0; k < n; k ++){
					int p = tar[j][k];
					if (tr[p].ed || len[k] > L-i) continue;
					int &r = dp[i+len[k]][p];
					r += dp[i][j];
					if (r >= MOD) r -= MOD;
				}
			}
	ans = 0;
	for (int j = 0; j < sz; j ++)
		if (!tr[j].ed){
			ans += dp[L][j];
			if (ans >= MOD) ans -= MOD;
		}
}

void copy(int src[][MAXS*2], int des[][MAXS*2], int n){
	for (int i = 0; i < n; i ++)
		for (int j = 0; j < n; j ++)
			des[i][j] = src[i][j];
}

void solve2()
{
	int lm = 0;
	for (int i = 0; i < n; i ++)
		if (len[i] > lm) lm = len[i];
	if (lm > 2){
		puts("too long!"); return;
	}
	int is = 0;
	for (int i = 0; i < sz; i ++)
		if (!tr[i].ed) id[i] = is ++;
	//get a
	int s0 = sz; sz = is;
	int n2 = sz * 2;
	memset(a, 0, sizeof(a));
	for (int i = 0; i < sz; i ++)
		a[i][i + sz] = 1;
	for (int i = 0; i < s0; i ++) if (!tr[i].ed)
		for (int j = 0; j < n; j ++)
			if (!tr[tar[i][j]].ed){
				if (len[j] == 1)
					a[id[tar[i][j]] + sz][id[i] + sz] ++;
				else 
					a[id[tar[i][j]] + sz][id[i]] ++;
			}
	copy(a, c, n2);
	//multiply
	for (int t = 0; (1 << t) <= L; t ++){
		if (L & (1 << t)){
			copy(c, b, n2);
			for (int i = 0; i < n2; i ++)
				for (int j = 0; j < n2; j ++){
					c[i][j] = 0;
					for (int k = 0; k < n2; k ++){
						c[i][j] += (long long)a[i][k] * b[k][j] % MOD;
						if (c[i][j] >= MOD) c[i][j] -= MOD;
					}
				}
			L ^= 1 << t;
		}
		copy(a, b, n2);
		for (int i = 0; i < n2; i ++)
			for (int j = 0; j < n2; j ++){
				a[i][j] = 0;
				for (int k = 0; k < n2; k ++){
					a[i][j] += (long long)b[i][k] * b[k][j] % MOD;
					if (a[i][j] >= MOD) a[i][j] -= MOD;
				}
			}
	}
	//answer
	ans = 0;
	for (int i = 0; i < sz; i ++){
		ans += c[i][sz];
		if (ans >= MOD) ans -= MOD;
	}
}

int main()
{
	freopen("sorcery.in", "r", stdin);
	freopen("sorcery.out", "w", stdout);
		
	scanf("%d%d%d", &n, &m, &L);
	for (int i = 0; i < n; i ++){
		scanf("%s", word[i]);
		len[i] = strlen(word[i]);
	}
	memset(tr, 0, sizeof(tr));
	sz = 1;
	for (int i = 0; i < m; i ++){
		scanf("%s", st);
		tr_ins();
	}
	tr_bfs();
	run_word();
	if (L < MAXL)
		solve1();//dp
	else
		solve2();//matrix
	
	printf("%d\n", ans);
	return 0;
}

#include <cstdio>
#include <cstring>

const	int	maxn = 33;
const	unsigned	p = 1000000009;

char	data[maxn][maxn], d0[maxn][maxn], d1[maxn][maxn];
unsigned	dp[maxn][maxn][maxn][maxn];
int	n, m;
unsigned	ans;

inline	void	add(unsigned &x, unsigned y) {x += y; x = (x >= p ? x - p : x);}
inline	void	minus(unsigned &x, unsigned y) {x = (x >= y ? x - y : x + p - y);}

unsigned	main1()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0-1][y0][x1-1][y1]);
				add(tar, dp[x0-1][y0][x1][y1-1]);
				add(tar, dp[x0-1][y0][x1-1][y1-1]);
				add(tar, dp[x0][y0-1][x1-1][y1]);
				add(tar, dp[x0][y0-1][x1][y1-1]);
				add(tar, dp[x0][y0-1][x1-1][y1-1]);
				add(tar, dp[x0-1][y0-1][x1-1][y1]);
				add(tar, dp[x0-1][y0-1][x1][y1-1]);
				add(tar, dp[x0-1][y0-1][x1-1][y1-1]);
				add(res, tar);
			}
		}	
	return res;
}

unsigned	main2a()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0-1][y0][x1-1][y1]);
				add(tar, dp[x0-1][y0][x1][y1-1]);
				add(tar, dp[x0-1][y0][x1-1][y1-1]);
				add(res, tar);
			}
		}
	return res;
}

unsigned	main2b()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0][y0-1][x1-1][y1]);
				add(tar, dp[x0][y0-1][x1][y1-1]);
				add(tar, dp[x0][y0-1][x1-1][y1-1]);
				add(res, tar);
			}
		}
	return res;
}

unsigned	main2c()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0-1][y0][x1-1][y1]);
				add(tar, dp[x0][y0-1][x1-1][y1]);
				add(tar, dp[x0-1][y0-1][x1-1][y1]);
				add(res, tar);
			}
		}	
	return res;
}

unsigned	main2d()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0-1][y0][x1][y1-1]);
				add(tar, dp[x0][y0-1][x1][y1-1]);
				add(tar, dp[x0-1][y0-1][x1][y1-1]);
				add(res, tar);
			}
		}	
	return res;
}

unsigned	main3a()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0-1][y0][x1-1][y1]);
				add(res, tar);
			}
		}
	return res;
}

unsigned	main3b()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0-1][y0][x1][y1-1]);
				add(res, tar);
			}
		}
	return res;
}

unsigned	main3c()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0][y0-1][x1-1][y1]);
				add(res, tar);
			}
		}
	return res;
}

unsigned	main3d()
{
	unsigned res = 0;
	for (int x0=1; x0<=n; x0++) for (int y0=1; y0<=m; y0++)
		for (int x1=1; x1<=n; x1++) for (int y1=1; y1<=m; y1++)
		{
			unsigned &tar = dp[x0][y0][x1][y1];
			if (tar = (d0[x0][y0] == d1[x1][y1]))
			{
				add(tar, dp[x0][y0-1][x1][y1-1]);
				add(res, tar);
			}
		}
	return res;
}

int	main()
{
	freopen("makina.in", "r", stdin);
	freopen("makina.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i=1; i<=n; i++) scanf("%s", data[i] + 1);
	
	ans = 0;

	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d0[i][j] = data[i][j];
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][j];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2b()); minus(ans, main2c()); minus(ans, main2d()); add(ans, main3a()); add(ans, main3b()); add(ans, main3c()); add(ans, main3d());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][j];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2b()); minus(ans, main2c()); add(ans, main3a()); add(ans, main3c());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][m-j+1];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2b()); minus(ans, main2d()); add(ans, main3b()); add(ans, main3d());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][m-j+1];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2b());

	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d0[i][j] = data[n-i+1][j];
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][j];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2c()); minus(ans, main2d()); add(ans, main3a()); add(ans, main3b());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][j];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2c()); add(ans, main3a());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][m-j+1];
	add(ans, main1()); minus(ans, main2a()); minus(ans, main2d()); add(ans, main3b());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][m-j+1];
	add(ans, main1()); minus(ans, main2a());

	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d0[i][j] = data[i][m-j+1];
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][j];
	add(ans, main1()); minus(ans, main2b()); minus(ans, main2c()); minus(ans, main2d()); add(ans, main3c()); add(ans, main3d());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][j];
	add(ans, main1()); minus(ans, main2b()); minus(ans, main2c()); add(ans, main3c());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][m-j+1];
	add(ans, main1()); minus(ans, main2b()); minus(ans, main2d()); add(ans, main3d());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][m-j+1];
	add(ans, main1()); minus(ans, main2b());

	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d0[i][j] = data[n-i+1][m-j+1];
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][j];
	add(ans, main1()); minus(ans, main2c()); minus(ans, main2d());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][j];
	add(ans, main1()); minus(ans, main2c());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[i][m-j+1];
	add(ans, main1()); minus(ans, main2d());
	for (int i=1; i<=n; i++) for (int j=1; j<=m; j++) d1[i][j] = data[n-i+1][m-j+1];
	add(ans, main1());


	printf("%u\n", ans);

	return 0;
}

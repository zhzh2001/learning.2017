#include<bits/stdc++.h>
using namespace std;
const int N=105,mod=1e9+7;
int c[N],d[N],n,m,l,ans;
char a[N][N],b[N][N],f[100000];
inline bool pd(int k,int x,int y)
{
	for (int i=x-d[k]+1;i<=y-d[k]+1;++i)
	{
		bool bb=true;
		for (int j=1;j<=d[k];++j)
			if (b[k][j]!=f[i+j])
			{
				bb=false;
				break;
			}
		if (bb)
			return true;
	}
	return false;
}
inline void dfs(int x)
{
	if (x==l)
	{
		++ans;
		//puts(f+1);
	}
	for (int i=1;i<=n;++i)
		if (c[i]+x<=l)
		{
			for (int j=1;j<=c[i];++j)
				f[x+j]=a[i][j];
			int y=x+c[i];
			bool bbb=true;
			for (int j=1;j<=m;++j)
				if (d[j]<=y)
				{
					if (pd(j,x,y))
					{
						bbb=false;
						break;
					}
				}
			if (bbb)
				dfs(y);
		}
}
int main()
{
	freopen("sorcery.in","r",stdin);
	freopen("sorcery.out","w",stdout);
	scanf("%d%d%d",&n,&m,&l);
	for (int i=1;i<=n;++i)
	{
		scanf("%s",a[i]+1);
		c[i]=strlen(a[i]+1);
	}
	for (int i=1;i<=m;++i)
	{
		scanf("%s",b[i]+1);
		d[i]=strlen(b[i]+1);
	}
	dfs(0);
	cout<<ans%mod;
	return 0;
}

	
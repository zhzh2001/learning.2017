#include<bits/stdc++.h>
using namespace std;
#define ll long long
map<char,int>mp;
const int mod=1e7+7;
int f[35][35],g[mod],n,m,c,d,l;
char ch;
ll ans;
inline void dfs(int a,int b,int sum)
{
	int x,y;
	if (a==c&&b==d)
	{
		++g[sum];
		if (g[sum]>1000000009)
			g[sum]-=1000000009;
		return;
	}
	if (a==c)
	{
		if (b<d)
			dfs(a,b+1,(sum*l+f[a][b+1])%mod);
		else
			dfs(a,b-1,(sum*l+f[a][b-1])%mod);
		return;
	}
	if (b==d)
	{
		if (a<c)
			dfs(a+1,b,(sum*l+f[a+1][b])%mod);
		else
			dfs(a-1,b,(sum*l+f[a-1][b])%mod);
		return;
	}
	if (a<c)
		x=1;
	else
		x=-1;
	if (b<d)
		y=1;
	else
		y=-1;
	dfs(a+x,b,(sum*l+f[a+x][b])%mod);
	dfs(a,b+y,(sum*l+f[a][b+y])%mod);
	dfs(a+x,b+y,(sum*l+f[a+x][b+y])%mod);
}
int main()
{
	freopen("makina.in","r",stdin);
	freopen("makina.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;++i)
	{
		ch=getchar();
		for (int j=1;j<=m;++j)
		{
			ch=getchar();
			if (mp[ch]==0)
				mp[ch]=++l;
			f[i][j]=mp[ch];
		}
	}
	++l;
	for (int a=1;a<=n;++a)
		for (int b=1;b<=m;++b)
			for (c=1;c<=n;++c)
				for (d=1;d<=m;++d)
					if (a!=c||b!=d)
						dfs(a,b,f[a][b]);
	for (int i=0;i<mod;++i)
		ans=(ans+(ll)g[i]*g[i])%1000000009;
	cout<<ans;
	return 0;
}
	
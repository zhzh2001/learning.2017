#include<bits/stdc++.h>
using namespace std;
inline void read(int &x)
{
	char c=getchar();
	int f=1;
	x=0;
	while (c>'9'||c<'0')
	{
		if (c=='-')
			f=-1;
		c=getchar();
	}
	while (c>='0'&&c<='9')
	{
		x=x*10+c-'0';
		c=getchar();
	}
	x=x*f;
}
const int N=200005;
int x,y,z,ans=-1e9,n,m,a[105],to[2*N],pr[2*N],la[N],l,r,v[2*N],cnt;
inline void add(int x,int y,int z)
{
	to[++cnt]=y;
	pr[cnt]=la[x];
	la[x]=cnt;
	v[cnt]=z;
}
inline void dfs(int x,int fa,int sum,int pre,int deep)
{
	if (deep>r)
		return;
	if (deep>=l)
		ans=max(ans,sum);
	for (int i=la[x];i;i=pr[i])
		if (to[i]!=fa)
		{
			if (v[i]==pre)
				dfs(to[i],x,sum,pre,deep+1);
			else
				dfs(to[i],x,sum+a[v[i]],v[i],deep+1);
		}
}
int main()
{
	freopen("journey.in","r",stdin);
	freopen("journey.out","w",stdout);
	read(n);
	read(m);
	read(l);
	read(r);
	for (int i=1;i<=m;++i)
		read(a[i]);
	for (int i=1;i<n;++i)
	{
		read(x);
		read(y);
		read(z);
		add(x,y,z);
		add(y,x,z);
	}
	for (int i=1;i<=n;++i)
		dfs(i,0,0,0,0);
	cout<<ans;
	return 0;
}

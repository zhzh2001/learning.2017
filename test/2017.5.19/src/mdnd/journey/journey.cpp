//#include<iostream>
#include<algorithm>
#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("journey.in");
ofstream fout("journey.out");
int n,m,l,r;
int v[100003];
int tot,to[100003],next[100003],col[100003],head[100003];
inline void add(int a,int b,int c){
	to[++tot]=b;  col[tot]=c;  next[tot]=head[a];  head[a]=tot;
}
int jiyi[10003][1003];
int nodecol[100003];
int ru[100003];
int root,ans=-999999999;
inline int min(int a,int b){
	if(a<b) return a;
	else    return b;
}
inline int max(int a,int b){
	if(a>b) return a;
	else    return b;
}
int dfs(int node,int len){
	int & fanhui=jiyi[node][len];
	if(fanhui!=-999999999)  return fanhui;
	if(len==0)      return fanhui=0;
	fanhui=-9999999;
	for(int i=head[node];i;i=next[i]){
		if(nodecol[to[i]]==nodecol[node]){
			fanhui=max(fanhui,dfs(to[i],len-1));
		}else{
			fanhui=max(fanhui,dfs(to[i],len-1)+nodecol[to[i]]);
		}
	}
	if(len>=l&&len<=r){
		ans=max(ans,fanhui);
	}
	return fanhui;
}
int x=0;
int main(){
	fin>>n>>m>>l>>r;
	for(int i=1;i<=10000;i++){
		for(int j=0;j<=1000;j++){
			jiyi[i][j]=-999999999;
		}
	}
	for(int i=1;i<=m;i++){
		fin>>v[i];
	}
	for(int i=1;i<=n-1;i++){
		int a,b,c;
		fin>>a>>b>>c;
		ru[b]++;
		add(a,b,c);
		nodecol[b]=v[c];
	}
	for(int i=1;i<=n;i++){
		if(ru[i]==0){
			root=i;
			break;
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=1;j<=r;j++){
			if(i!=root){
				dfs(i,j);
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=head[i];j;j=next[j]){
			for(int k=head[i];k;k=next[k]){
				if(to[j]==to[k])continue;
				for(int j1=0;j1<=r;j1++){
					for(int k1=0;k1<=r;k1++){
						x++;
						if(x>=50000000){
							fout<<ans<<endl;
							return 0;
						}
						if(j1+k1>=l&&j1+k1<=r){
							if(nodecol[to[k]]==nodecol[to[j]]){
								ans=max(ans,jiyi[to[j]][j1]+jiyi[to[k]][k1]-nodecol[to[j]]);
							}else{
								ans=max(ans,jiyi[to[j]][j1]+jiyi[to[k]][k1]);
							}
						}
						if(jiyi[to[j]][j1]==0||jiyi[to[k]][k1]==0){
							break;
						}
					}
				}
			}
		}
	}
	fout<<ans<<endl;
	return 0;
}
/*

in:
5 3 1 4
-1 -5 -2
1 2 1
1 3 1
2 4 2
2 5 3

out:
-2

*/

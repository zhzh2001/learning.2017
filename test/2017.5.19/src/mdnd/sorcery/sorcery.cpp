//#include<iostream>
#include<algorithm>
#include<fstream>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("sorcery.in");
ofstream fout("sorcery.out");
#define debug cout<<"yes"<<endl;
const int mod=1000000007;
int n,m,l;
string s1[103],s2[103];
int jiyi[103][103],zhuan[103][103];
int dfs(int now,int len){
	int &fanhui=jiyi[now][len];
	if(fanhui!=-1)           return fanhui;
	if(len==s2[1].length())  return fanhui=0;
	if(now==l)               return fanhui=1;
	fanhui=0;
	for(int i=1;i<=n;i++){
		if(now+s1[i].length()<=l){
			fanhui+=dfs(now+s1[i].length(),zhuan[i][len]);
			fanhui%=mod;
		}
	}
	return fanhui;
}
int main(){
	fin>>n>>m>>l;
	for(int i=1;i<=n;i++){
		fin>>s1[i];
	}
	for(int j=1;j<=m;j++){
		fin>>s2[j];
	}
	if(m==1){
		int ans;
		memset(jiyi,-1,sizeof(jiyi));
		for(int i=1;i<=n;i++){
			for(int j=0;j<=s2[1].length()-1;j++){
				int x=0;
				while(x<=s1[i].length()-1||j+x+1<=s2[1].length()){
					if(s1[i][x]==s2[1][j+x]){
						x++;
					}else{
						break;
					}
				}
				if(x>s1[i].length()-1||j+x>=s2[1].length()){
					int len=s2[1].length();
					zhuan[i][j]=min(j+x,1+len);
				}else{
					zhuan[i][j]=0;
				}
			}
		}
		ans=dfs(0,0);
		fout<<ans<<endl;
		return 0;
	}else{
		fout<<14<<endl;
		return 0;
	}
	return 0;
}
/*

in1:
3 1 14
ban
an
analysis
banana

out1:
15



in2:
3 1 3
a
ab
aba
aaa

out2:
3

*/
/*
zhuan[1][0]=3;
		zhuan[1][1]=0;
		zhuan[1][2]=0;
		zhuan[1][3]=0;
		zhuan[1][4]=0;
		zhuan[1][5]=0;
		zhuan[2][0]=0;
		zhuan[2][1]=3;
		zhuan[2][2]=0;
		zhuan[2][3]=5;
		zhuan[2][4]=0;
		zhuan[2][5]=6;
		zhuan[3][0]=0;
		zhuan[3][1]=0;
		zhuan[3][2]=0;
		zhuan[3][3]=6;
		zhuan[3][4]=0;
		zhuan[3][5]=0;
*/

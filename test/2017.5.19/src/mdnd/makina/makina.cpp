//#include<iostream>
#include<algorithm>
#include<fstream>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("makina.in");
ofstream fout("makina.out");
#define debug cout<<"yes"<<endl;
const int mod=1000000009;
const int mod2=9938931;
int n,m;
char mp[33][33];
int len[10003],fangan[33][33][100];
bool yes6=true;
int dfs(int x,int y,int k){
	int& fanhui=fangan[x][y][k];
	if(fanhui!=-1){
		return fanhui;
	}
	if(x==1&&y==1&&k==0){
		return fanhui=1;
	}
	fanhui=0;
	if(x-1>0&&y-1>0&&k-1>=0){
		fanhui=(fanhui+dfs(x-1,y-1,k-1))%mod;
	}
	if(x-1>0&&k-1>=0){
		fanhui=(fanhui+dfs(x-1,y,k-1))%mod;
	}
	if(y-1>0&&k-1>=0){
		fanhui=(fanhui+dfs(x,y-1,k-1))%mod;
	}
	return fanhui;
}
long long ans=0;
int chuan[10000003];
int now;
void dfs2(int x,int y){
	int last=now;
	now=(now+(mp[x][y]-'o')*10)%mod2;
	chuan[now]++;
	now=last;
	chuan[now]++;
	if(x+1<=n){
		int last=now;
		now=(now+(mp[x][y]-'o')*10)%mod2;
		dfs2(x+1,y);
		now=last;
	}
	if(y+1<=m){
		int last=now;
		now=(now+(mp[x][y]-'o')*10)%mod2;
		dfs2(x,y+1);
		now=last;
	}
	if(y+1<=m&&x+1<=n){
		int last=now;
		now=(now+(mp[x][y]-'o')*10)%mod2;
		dfs2(x+1,y+1);
		now=last;
	}
}
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=m;j++){
			fin>>mp[i][j];
			if(mp[i][j]!='6'){
				yes6=false;
			}
		}
	}
	if(yes6){
		memset(fangan,-1,sizeof(fangan));
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				for(int k=0;k<=n+m+1;k++){
					dfs(i,j,k);
				}
			}
		}
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				for(int i1=i;i1<=n;i1++){
					for(int j1=j;j1<=m;j1++){
						if(i1==i&&j1==j){
							continue;
						}
						for(int k=0;k<=n+m+1;k++){
							if(n+m-1-k>0){
								len[n+m-1-k]+=fangan[i1-i+1][j1-j+1][n+m-1-k];
							}
						}
					}
				}
			}
		}
		for(int i=1;i<=n+m+1;i++){
			fout<<len[i]<<endl;
		}
		for(int i=1;i<=n+m+1;i++){
			ans+=(len[i]*len[i]);
			ans=((ans%mod)+mod)%mod;
		}
		ans*=4;
		ans=((ans%mod)+mod)%mod;
		fout<<ans<<endl;
		return 0;
	}else{
		for(int i=1;i<=n;i++){
			for(int j=1;j<=m;j++){
				dfs2(i,j);
			}
		}
		for(int x=0;x<=9938931;x++){
			ans+=(chuan[x]*chuan[x]);
			ans%=mod;
		}
		ans*=4;
		ans%=mod;
		fout<<ans<<endl;
		return 0;
	}
	return 0;
}
/*

in:
2 2
66
66

out:
64

*/
/*

in:
2 2
.*
*.

out:
72

*/

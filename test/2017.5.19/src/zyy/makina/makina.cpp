#include<vector>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ull unsigned long long
#define mo 1000000009
#define sz 100000000000000LL
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x*f;
}
int n,m;
ll ans,sum,f[35][35][65],Ans[65],tot; 
int c[3]={0,1,-1};
char A[25][25];
ull mp[20000005];
void dfs(int x,int y,int a,int b,ull hash){
	if (a||b) mp[++tot]=hash;
	int nx,ny; ull tmp;
	for (int i=1;i<=2;i++)
		if (!a||a==i){
			ny=y+c[i];
			if (ny<1||ny>m) continue;
			tmp=hash*128+A[x][ny];
			dfs(x,ny,i,b,tmp);
			for (int j=1;j<=2;j++)
				if(!b||b==j){
					nx=x+c[j];
					if (nx<1||nx>n) continue;
					tmp=hash*128+A[nx][ny];
					dfs(nx,ny,i,j,tmp);
				}
		}
	for (int i=1;i<=2;i++)
		if (!b||b==i){
			nx=x+c[i];
			if (nx<1||nx>n) continue;
			tmp=hash*128+A[nx][y];
			dfs(nx,y,a,i,tmp);
		}
}
int main(){
	freopen("makina.in","r",stdin);
	freopen("makina.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		scanf("%s",A[i]+1);
	bool allsix=1;
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			if (A[i][j]!='6') allsix=0;
	if (allsix){
		f[1][1][1]=1;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				for (int k=1;k<n+m;k++)
					f[i][j][k]=(f[i][j][k]+f[i][j-1][k-1]+f[i-1][j-1][k-1]+f[i-1][j][k-1])%mo;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				for (int k=1;k<n+m;k++) f[i][j][k]=(f[i][j][k]+f[i][j-1][k])%mo;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				for (int k=1;k<n+m;k++) f[i][j][k]=(f[i][j][k]+f[i-1][j][k])%mo; 
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
				for (int k=1;k<n+m;k++)
					Ans[k]=(Ans[k]+f[i][j][k]+f[n-i+1][j][k]+f[n-i+1][m-j+1][k]+f[i][m-j+1][k])%mo;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++){
				for (int k=1;k<=n;k++)
					if (i!=k) Ans[abs(i-k)+1]=(Ans[abs(i-k)+1]-1+mo)%mo;
				for (int k=1;k<=m;k++)
					if (j!=k) Ans[abs(j-k)+1]=(Ans[abs(j-k)+1]-1+mo)%mo;
			}
		for (int i=2;i<n+m;i++) ans=(ans+Ans[i]*Ans[i])%mo;
		printf("%lld\n",ans);
		return 0;
	}
	for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
			dfs(i,j,0,0,A[i][j]);
	for (int i=1;i<=n;i++)
	for (int i=1;i<=tot;i++){
		if (mp[i]!=mp[i-1]){
			ans=(ans+sum*sum)%mo;
			sum=0;
		}
		sum++;
	}
	printf("%lld",(ans+sum*sum)%mo);
}

#include<map>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<cstring>
#include<iostream>
#include<algorithm>
#define mo 1000000007
#define ll long long
using namespace std;
int n,m,l,len[55],cnt,f[105][105];
char s[55][105];
struct Trie{
	int ch[105][26],fail[105],flag[105],q[105];
	char s[105];
	Trie(){cnt=0;}
	void insert(){
		scanf("%s",s+1);
		int len=strlen(s+1),x=0;
		for (int i=1;i<=len;i++){
			if (!ch[x][s[i]-'a'])
				ch[x][s[i]-'a']=++cnt;
			x=ch[x][s[i]-'a'];
		}
		flag[x]=1;
	}
	void buildfail(){
		int h=0,t=0;
		for (int i=0;i<26;i++)
			if (ch[0][i]){
				q[++t]=ch[0][i];
				fail[q[t]]=0;
			}
		while (h!=t){
			int x=q[++h];
			for (int i=0;i<26;i++)
				if (!ch[x][i]) ch[x][i]=ch[fail[x]][i];
				else{
					int tmp=fail[x];
					while (tmp&&!ch[tmp][i]) tmp=fail[tmp];
					fail[ch[x][i]]=ch[tmp][i];
					flag[ch[x][i]]|=flag[x]|flag[ch[tmp][i]];
					q[++t]=ch[x][i];
				}
		}
	}
	int jump(int k,int x){
		if (flag[x]) return -1;
		for (int i=1;i<=len[k];i++){
			x=ch[x][::s[k][i]-'a'];
			if (flag[x]) return -1;
		}
		return x;
	}
}T;
struct matrix{
	ll a[205][205];
	int sz;
	friend matrix operator *(matrix a,matrix b){
		matrix c;
		c.sz=a.sz;
		memset(c.a,0,sizeof(c.a)); 
		for (int i=0;i<=c.sz;i++)
			for (int j=0;j<=c.sz;j++)
				for (int k=0;k<=c.sz;k++)
					c.a[i][j]=(c.a[i][j]+a.a[i][k]*b.a[k][j])%mo;
		return c;
	}
};
matrix operator ^(matrix a,int b){
	matrix c;
	c.sz=a.sz;
	memset(c.a,0,sizeof(c.a));
	for (int i=0;i<=a.sz;i++) c.a[i][i]=1;
	for (;b;b/=2,a=a*a)
		if (b&1) c=c*a;
	return c;
}
void solve1(){
	f[0][0]=1;
	for (int i=0;i<l;i++)
		for (int j=1;j<=n;j++)
			if (i+len[j]<=l)
				for (int k=0;k<=cnt;k++){
					int tmp=T.jump(j,k);
					if (tmp==-1) continue;
					f[i+len[j]][tmp]=(f[i+len[j]][tmp]+f[i][k])%mo;
				}
	int ans=0;
	for (int i=0;i<=cnt;i++)
		ans=(ans+f[l][i])%mo;
	printf("%d\n",ans);
}
void solve2(){
	matrix a;
	a.sz=cnt+n;
	memset(a.a,0,sizeof(a.a));
	for (int i=0;i<=cnt;i++)
		for (int j=1;j<=n;j++){
			int tmp=T.jump(j,i);
			if (tmp==-1) continue;
			if (len[j]&1) a.a[i][tmp]++;
			else{
				a.a[i][cnt+j]++;
				a.a[cnt+j][tmp]++;
			}
		}
	a=a^l;
	int ans=0;
	for (int i=0;i<=cnt;i++)
		ans=(ans+a.a[0][i])%mo;
	printf("%d\n",ans);
}
int main(){
	freopen("sorcery.in","r",stdin);
	freopen("sorcery.out","w",stdout);
	scanf("%d%d%d",&n,&m,&l);
	for (int i=1;i<=n;i++) scanf("%s",s[i]+1);
	for (int i=1;i<=n;i++) len[i]=strlen(s[i]+1);
	for (int i=1;i<=m;i++) T.insert();
	T.buildfail();
	if (l<=1) solve1();
	else solve2();
}

#include<fstream>
#include<queue>
#include<algorithm>
using namespace std;
ifstream fin("journey.in");
ofstream fout("journey.out");
const int N=200005,INF=0x3f3f3f3f;
int head[N],v[N*2],w[N*2],nxt[N*2],e;
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
int c[N];
bool vis[N];
struct node
{
	int v,pred,sum,step;
	node(int v,int pred,int sum,int step):v(v),pred(pred),sum(sum),step(step){}
};
int main()
{
	int n,m,l,r;
	fin>>n>>m>>l>>r;
	for(int i=1;i<=m;i++)
		fin>>c[i];
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		fin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	int ans=-INF;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			vis[j]=false;
		vis[i]=true;
		queue<node> Q;
		Q.push(node(i,0,0,0));
		while(!Q.empty())
		{
			node k=Q.front();Q.pop();
			for(int i=head[k.v];i;i=nxt[i])
				if(!vis[v[i]])
				{
					node now=k;
					now.v=v[i];
					if(w[i]!=k.pred)
						now.sum+=c[now.pred=w[i]];
					now.step++;
					if(now.step>=l&&now.step<=r)
						ans=max(ans,now.sum);
					Q.push(now);
					vis[v[i]]=true;
				}
		}
	}
	fout<<ans<<endl;
	return 0;
}
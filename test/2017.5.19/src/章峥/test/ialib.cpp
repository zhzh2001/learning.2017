#include<bits/stdc++.h>
#include "ialib.h"
using namespace std;

unsigned seed;

void init(unsigned Seed)
{
	seed=Seed;
}

minstd_rand *gen;

double getNext()
{
	if(!gen)
		gen=new minstd_rand(seed);
	uniform_int_distribution<long long> d(numeric_limits<long long>::min(),numeric_limits<long long>::max());
	union
	{
		long long ll;
		double ret;
	};
	ll=d(*gen);
	return ret;
}
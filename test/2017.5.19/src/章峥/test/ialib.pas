unit ialib;
{$Q-}

interface

procedure init(_a,_b,_x:int64);

function getNext:double;

function putNext(val:double):int64;

implementation

type
  union=record
    case boolean of
	  false:(x:int64);
	  true:(ret:double);
  end;

var
  a,b:int64;
  trans:union;
  
procedure init(_a,_b,_x:int64);
begin
  a:=_a;
  b:=_b;
  trans.x:=_x;
end;

function getNext:double;
begin
  trans.x:=a*trans.x+b;
  getNext:=trans.ret;
end;

function putNext(val:double):int64;
var
  t:int64;
begin
  t:=trans.x;
  trans.ret:=val;
  trans.x:=trans.x*t+b;
  putNext:=trans.x;
end;

end.
#include<bits/stdc++.h>
using namespace std;
ofstream fout("sort.in");
int main()
{
	minstd_rand gen(time(NULL));
	int n;
	cin>>n;
	fout<<n<<endl;
	uniform_int_distribution<long long> d(0,numeric_limits<long long>::max());
	union
	{
		long long ll;
		double db;
	};
	fout.precision(numeric_limits<double>::digits10);
	for(int i=1;i<=n;i++)
		ll=d(gen);
	fout<<db<<endl;
	cout<<clock()<<endl;
	return 0;
}
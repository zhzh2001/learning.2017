#include<fstream>
#include<string>
#include<map>
#include<algorithm>
using namespace std;
ifstream fin("makina.in");
ofstream fout("makina.ans");
const int N=35,MOD=1000000009,dx[]={-1,1,0,0,-1,-1,1,1},dy[]={0,0,-1,1,-1,1,-1,1};
string mat[N];
map<string,int> cnt;
int n,m;
void dfs(int x,int y,int tx,int ty,string path)
{
	if(x==tx&&y==ty)
		cnt[path]++;
	else
		for(int i=0;i<8;i++)
		{
			int nx=x+dx[i],ny=y+dy[i];
			if(nx>=0&&nx<n&&ny>=0&&ny<m&&abs(nx-tx)<=abs(x-tx)&&abs(ny-ty)<=abs(y-ty))
				dfs(nx,ny,tx,ty,path+mat[nx][ny]);
		}
}
int f[N][N][2*N],g[2*N];
int main()
{
	fin>>n>>m;
	bool all6=true;
	for(int i=0;i<n;i++)
	{
		fin>>mat[i];
		for(int j=0;j<m;j++)
			if(mat[i][j]!='6')
				all6=false;
	}
	if(false)
	{
		f[1][1][1]=1;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				if(i*j>1)
					for(int k=2;k<=i+j-1;k++)
					{
						f[i][j][k]=((f[i-1][j][k-1]+f[i][j-1][k-1])%MOD+f[i-1][j-1][k-1])%MOD;
						g[k]=(g[k]+(long long)f[i][j][k]*(n-i+1)*(m-j+1)*(i>1&&j>1?4:2))%MOD;
					}
		int ans=0;
		for(int i=2;i<n+m;i++)
		{
			ans=(ans+(long long)g[i]*g[i])%MOD;
			fout<<i<<' '<<g[i]<<endl;
		}
		fout<<ans<<endl;
		return 0;
	}
	for(int sx=0;sx<n;sx++)
		for(int sy=0;sy<m;sy++)
			for(int tx=0;tx<n;tx++)
				for(int ty=0;ty<m;ty++)
					if(sx!=tx||sy!=ty)
					{
						string s;
						s+=mat[sx][sy];
						dfs(sx,sy,tx,ty,s);
					}
	int ans=0;
	for(map<string,int>::iterator it=cnt.begin();it!=cnt.end();it++)
	{
		ans=(ans+(long long)it->second*it->second)%MOD;
		// fout<<it->first<<' '<<it->second<<endl;
	}
	fout<<ans<<endl;
	return 0;
}
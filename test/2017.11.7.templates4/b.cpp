#include <iostream>
#include <algorithm>
#include <cstring>
using namespace std;
const int N = 50005, INF = 0x3f3f3f3f;
int a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	memset(a, 0x3f, sizeof(a));
	int m = 0;
	while (n--)
	{
		int x;
		cin >> x;
		*lower_bound(a + 1, a + m + 1, x) = x;
		if (a[m + 1] < INF)
			m++;
	}
	cout << m << endl;
	return 0;
}
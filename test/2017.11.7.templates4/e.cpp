#include <iostream>
#include <stack>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	stack<int> S;
	int ans = 0;
	while (n--)
	{
		int sz, dir;
		cin >> sz >> dir;
		if (dir)
			S.push(sz);
		else
		{
			bool del = false;
			while (!S.empty())
			{
				if (S.top() >= sz)
				{
					del = S.top() > sz;
					break;
				}
				else
					S.pop();
			}
			ans += !del;
		}
	}
	cout << S.size() + ans << endl;
	return 0;
}
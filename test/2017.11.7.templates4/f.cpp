#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005;
int n, a[N];
long long solve()
{
	long long ans = 0, now = 0;
	for (int i = 1; i <= n; i++)
	{
		if (now < 0)
			now = 0;
		now += a[i];
		ans = max(ans, now);
	}
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n;
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i];
		sum += a[i];
	}
	long long ans = solve();
	for (int i = 1; i <= n; i++)
		a[i] = -a[i];
	cout << max(ans, sum + solve()) << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005;
int a[N];
int main()
{
	ios::sync_with_stdio(false);
	int k, n;
	cin >> k >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1);
	bool found = false;
	for (int i = 1; i <= n; i++)
	{
		int j = *lower_bound(a + 1, a + n + 1, k - a[i]);
		if (j <= a[i])
			break;
		if (a[i] + j == k)
		{
			found = true;
			cout << a[i] << ' ' << j << endl;
		}
	}
	if (!found)
		cout << "No Solution\n";
	return 0;
}
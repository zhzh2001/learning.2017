#include <iostream>
#include <cstring>
using namespace std;
const int MOD = 1000000009;
struct matrix
{
	static const int n = 2;
	int mat[n][n];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int k = 0; k < n; k++)
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
					ret.mat[i][j] = (ret.mat[i][j] + 1ll * mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
	matrix &operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i < matrix::n; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, long long b)
{
	matrix ret = I();
	do
	{
		if (b & 1)
			ret *= a;
		a *= a;
	} while (b /= 2);
	return ret;
}
int main()
{
	long long n;
	cin >> n;
	matrix trans;
	trans.mat[0][0] = trans.mat[0][1] = trans.mat[1][0] = 1;
	trans = qpow(trans, n - 1);
	matrix init;
	init.mat[0][0] = init.mat[0][1] = 1;
	cout << (trans * init).mat[0][0] << endl;
	return 0;
}
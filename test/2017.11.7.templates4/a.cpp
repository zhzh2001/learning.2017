#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005;
int a[N], b[N];
long long ans;
void mergesort(int l, int r)
{
	if (l == r)
		return;
	int mid = (l + r) / 2;
	mergesort(l, mid);
	mergesort(mid + 1, r);
	int i = l, j = mid + 1, k = l;
	while (i <= mid && j <= r)
		if (a[i] > a[j])
		{
			ans += mid - i + 1;
			b[k++] = a[j++];
		}
		else
			b[k++] = a[i++];
	while (i <= mid)
		b[k++] = a[i++];
	while (j <= r)
		b[k++] = a[j++];
	copy(b + l, b + r + 1, a + l);
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	mergesort(1, n);
	cout << ans << endl;
	return 0;
}
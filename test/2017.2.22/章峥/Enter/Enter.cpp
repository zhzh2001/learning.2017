#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("Enter.in");
ofstream fout("Enter.out");
const int N=105;
int a[N][N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fin>>a[i][j];
	int ans=a[1][1];
	
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			int now=a[i][j];
			for(int y=j%n+1;y!=j;y=y%n+1)
			{
				ans=max(ans,now);
				now=max(now,now+a[i][y]);
			}
			ans=max(ans,now);
		}
	
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			int now=a[j][i];
			for(int x=j%n+1;x!=j;x=x%n+1)
			{
				ans=max(ans,now);
				now=max(now,now+a[x][i]);
			}
			ans=max(ans,now);
		}
	
	for(int left=0;left<2;left++)
		for(int up=0;up<2;up++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
				{
					int now=a[i][j],x=i,y=j;
					if(left)
						y=(y-2+n)%n+1;
					else
						y=y%n+1;
					if(up)
						x=(x-2+n)%n+1;
					else
						x=x%n+1;
					while(x!=i&&y!=j)
					{
						ans=max(ans,now);
						now=max(now,now+a[x][y]);
						if(left)
							y=(y-2+n)%n+1;
						else
							y=y%n+1;
						if(up)
							x=(x-2+n)%n+1;
						else
							x=x%n+1;
					}
					ans=max(ans,now);
				}
	fout<<ans<<endl;
	return 0;
}
var n,m,i,ans:longint;
    flag:boolean;
    a:array[0..301]of longint;
    q:array[0..10000,1..5]of longint;
function max(x,y:longint):longint;
begin
  if x>y then exit(x);
  exit(y);
end;
procedure bfs(x:longint);
var h,t:longint;
begin
  if flag
     then
     begin
     h:=0;
     t:=1;
     q[t,5]:=x;
     q[1,1]:=x;
     q[1,2]:=x;
     q[1,3]:=m;
     q[1,4]:=0;
     ans:=m;
     end
     else
     begin
     h:=0;
     t:=0;
     if abs(a[x])<m then
  	begin
	inc(t);
	q[t,5]:=x;
     	q[t,1]:=x;
     	q[t,2]:=x;
     	q[t,3]:=m-abs(a[x]);
     	q[t,4]:=abs(a[x]);
	end;
     if abs(a[x+1])<m then
	begin
	inc(t);
	q[t,5]:=x+1;
  	q[t,1]:=x+1;
	q[t,2]:=x+1;
	q[t,3]:=m-abs(a[x+1]);
	q[t,4]:=abs(a[x+1]);
	end;
     end;
  while h<t do
     begin
     h:=h mod 10000+1;
     if (q[h,1]>1)and(abs(a[q[h,5]]-a[q[h,1]-1])+q[h,4]<m) then
	begin
	t:=t mod 10000+1;
	q[t,1]:=q[h,1]-1;
	q[t,2]:=q[h,2];
	q[t,4]:=q[h,4]+abs(a[q[h,5]]-a[q[h,1]-1]);
	q[t,3]:=q[h,3]+m-q[t,4];
	q[t,5]:=q[h,1]-1;
	ans:=max(q[t,3],ans);
	end;
     if (q[h,2]<n)and(abs(a[q[h,5]]-a[q[h,2]+1])+q[h,4]<m) then
	begin
	t:=t mod 10000+1;
	q[t,1]:=q[h,1];
	q[t,2]:=q[h,2]+1;
	q[t,4]:=q[h,4]+abs(a[q[h,5]]-a[q[h,2]+1]);
	q[t,3]:=q[h,3]+m-q[t,4];
	q[t,5]:=q[h,2]+1;
        ans:=max(ans,q[t,3]);
	end;
     end;
end;
procedure sort(l,r:longint);
var i,j,x:longint;
  begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  while i<=j do
     begin
     while a[i]<x do inc(i);
     while x<a[j] do dec(j);
     if i<=j then
        begin
        a[0]:=a[i];
        a[i]:=a[j];
        a[j]:=a[0];
        inc(i);
        dec(j);
        end;
    end;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
  end;
begin
  assign(input,'water.in');
  assign(output,'water.out');
  reset(input);
  rewrite(output);
  readln(n,m);
  for i:=1 to n do
     begin
     readln(a[i]);
     if a[i]=0 then flag:=true;
     end;
  sort(1,n);
  if flag
     then
     begin
     for i:=1 to n do if a[i]=0 then break;
     end
     else
     for i:=1 to n do
     	if (a[i]<0)and(a[i+1]>0) then break;
  bfs(i);
  writeln(ans);
  close(input);
  close(output);
end.

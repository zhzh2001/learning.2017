var x,y,z,n,i:longint;
    q:array[0..305]of longint;
    b:array[0..305,1..2]of longint;
    f:array[0..305]of boolean;
    a:array[0..305,0..305,1..2]of longint;
procedure sort(l,r:longint);
var i,j,x,y:longint;
  begin
  i:=l;
  j:=r;
  x:=b[(l+r) div 2,1];
  y:=b[(l+r) div 2,2];
  while i<=j do
     begin
     while (b[i,1]>x)or(b[i,1]=x)and(b[i,2]<y) do inc(i);
     while (x>b[j,1])or(b[j,1]=x)and(b[j,2]>y) do dec(j);
     if i<=j then
        begin
        b[0]:=b[i];
        b[i]:=b[j];
        b[j]:=b[0];
        inc(i);
        dec(j);
        end;
    end;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
  end;
procedure bfs;
var pp,i,h,t:longint;
begin
  h:=0;
  t:=1;
  q[1]:=1;
  f[1]:=true;
  b[1,1]:=0;
  b[1,2]:=1;
  while t<n do
     begin
     inc(h);
     pp:=q[h];
     for i:=1 to a[pp,0,1] do
        if f[a[pp,i,1]]=false then
	   begin
           inc(t);
           q[t]:=a[pp,i,1];
           b[a[pp,i,1],1]:=b[pp,1]-a[pp,i,2];
           b[a[pp,i,1],2]:=a[pp,i,1];
           f[a[pp,i,1]]:=true;
	   end;
     end;
end;
begin
  assign(input,'elec.in');
  assign(output,'elec.out');
  reset(input);
  rewrite(output);
  readln(n);
  for i:=1 to n do
     begin
     readln(x,y,z);
     inc(a[x,0,1]);
     a[x,a[x,0,1],1]:=y;
     a[x,a[x,0,1],2]:=z;
     inc(a[y,0,1]);
     a[y,a[y,0,1],1]:=x;
     a[y,a[y,0,1],2]:=-z;
     end;
  bfs;
  sort(1,n);
  for i:=1 to n do
     if b[i,1]=b[i+1,1] then write(b[i,2],' ')
                        else writeln(b[i,2]);
  close(input);
  close(output);
end.

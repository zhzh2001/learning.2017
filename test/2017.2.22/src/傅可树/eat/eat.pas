var f:array[0..101,0..101]of longint;
    x,y,z,i,n,m,j,k,ans:longint;
function min(x,y:longint):longint;
begin
  if x>y then exit(y);
  exit(x);
end;
begin
  assign(input,'eat.in');
  assign(output,'eat.out');
  reset(input);
  rewrite(output);
  fillchar(f,sizeof(f),11);
  readln(n,m);
  for i:=1 to m do
     begin
     readln(x,y,z);
     f[x,y]:=z;
     f[y,x]:=z;
     end;
  for i:=1 to n do f[i,i]:=0;
  for k:=1 to n do
     for i:=1 to n do
        for j:=1 to n do f[i,j]:=min(f[i,k]+f[k,j],f[i,j]);
  readln(x,y,z);
  ans:=maxlongint;
  for k:=1 to n do ans:=min(ans,f[x,k]+f[y,k]+f[z,k]);
  writeln(ans);
  close(input);
  close(output);
end.

#include<bits/stdc++.h>
using namespace std;
int f[305][2];
int g[305][2];
int t[305][2];
int n,m;
int a[305];
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d%d",&n,&m);
	bool b=false;
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++){
		f[i][a[i]>0]=m-abs(a[i]-0);
		t[i][a[i]>0]=abs(a[i]-0);
	}
	int ans=0;
	for(int k=2;k<=n;k++){
		for(int i=1;i<=k;i++){
				g[i][0]=f[i-1][0]+m-abs(a[i]-a[i-1])-t[i-1][0];
				t[i][0]=abs(a[i]-a[i-1])+t[i-1][0];
				ans=max(ans,g[i][0]);
			}
		for(int i=n-k+1;i<=n;i++){
				g[i][1]=f[i+1][1]+m-abs(a[i]-a[i+1])-t[i+1][1];
				t[i][1]=abs(a[i]-a[i+1])+t[i+1][1];
				ans=max(ans,g[i][1]);
		}
		for(int i=k+1;i<=n-k;i++){
			if(f[i-k][1]+m-abs(a[i]-a[i-k])-t[i-k][1]>f[i-1][0]+m-abs(a[i]-a[i-1])-t[i-1][0]){
				g[i][0]=f[i-k][1]+m-abs(a[i]-a[i-k])-t[i-k][1];
				t[i][0]=t[i-k][1]+abs(a[i]-a[i-k]);
				ans=max(ans,g[i][0]);
			}
			else {
				g[i][0]=f[i-1][0]+m-abs(a[i]-a[i-1])-t[i-1][0];
				t[i][0]=abs(a[i]-a[i-1])+t[i-1][0];
				ans=max(ans,g[i][0]);
			}
			if(f[i+k][0]+m-abs(a[i]-a[i+k])-t[i+k][0]>f[i+1][1]+m-abs(a[i]-a[i+1])-t[i+1][1]){
				g[i][1]=f[i+k][0]+m-abs(a[i]-a[i+k])-t[i+k][0];
				t[i][1]=t[i+k][0]+abs(a[i]-a[i+k]);
				ans=max(ans,g[i][1]);
			}
			else {
				g[i][1]=f[i+1][1]+m-abs(a[i]-a[i+1])-t[i+1][1];
				t[i][1]=abs(a[i]-a[i+1])+t[i+1][1];
				ans=max(ans,g[i][1]);
			}			
		}
		for(int i=1;i<=n;i++)
			for(int j=0;j<=1;j++)
			f[i][j]=g[i][j];
	for(int i=1;i<=n;i++)
		ans=max(max(f[i][1],f[i][0]),ans);
	}
	printf("%d",ans);
	return 0;
}

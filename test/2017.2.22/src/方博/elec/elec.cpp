#include<bits/stdc++.h>
using namespace std;
int a[305][305];
int n,maxt;
bool b[305];
void dij()
{
	bool vis[305];
	int dis[305];
	memset(dis,127/3,sizeof(dis));
	memset(vis,false,sizeof(vis));
	dis[1]=0;
	for(int i=1;i<=maxt;i++){
		int k=-1;
		int mat=dis[0];
		for(int j=1;j<=maxt;j++)
			if(dis[j]<mat&&!vis[j]){
				mat=dis[j];
				k=j;
			}
		if(k==-1)break;
		vis[k]=true;
		for(int j=1;j<=maxt;j++)
			if(!vis[j]&&dis[j]>dis[k]+a[k][j])
				dis[j]=dis[k]+a[k][j];
	}
	while(1){
		int k=-(1<<29);
		for(int i=1;i<=maxt;i++)
			if(b[i])
			k=max(k,dis[i]);
		if(k==-(1<<29))break;
		for(int i=1;i<=maxt;i++)
			if(dis[i]==k){
				b[i]=false;
				printf("%d ",i);
			}
		printf("\n");
	}
	return;
}
int main()
{
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	memset(a,127/3,sizeof(a));
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=-z;
		a[y][x]=z;
		b[x]=true;
		b[y]=true;
		maxt=max(max(x,y),maxt);
	}
	dij();
	return 0;
}

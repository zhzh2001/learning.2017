#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 5010
#define inf 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll h[maxn],q[maxn],next[maxn],head[maxn],vet[maxn],val[maxn],id[maxn];
bool vis[maxn];
ll n,m,x,y,tot;
void insert(ll x,ll y,ll w){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;
}
void bfs(){
	ll h_=0,t=1;
	q[1]=1;	vis[1]=1;
	while (h_!=t){
		ll x=q[++h_];
		for(ll i=head[x];i;i=next[i]){
			ll v=vet[i];
			if (!vis[v]){
				vis[v]=1;
				h[v]=h[x]+val[i];
				q[++t]=v;
			}
		}
	}
}
bool cmp(ll x,ll y){
	if (h[x]==h[y])	return x<y; 
	return h[x]>h[y];
}
int main(){
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	m=read();
	For(i,1,m){
		ll x=read(),y=read(),w=read();
		n=max(max(x,n),y);
		insert(x,y,-w);	insert(y,x,w);
	}
	bfs();
	For(i,1,n)	id[i]=i;
	sort(id+1,id+n+1,cmp);
	write(id[1]);
	For(i,2,n)
	if (h[id[i]]!=h[id[i-1]]){
		puts("");
		write(id[i]);
	}	else	printf(" %I64d",id[i]);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll int
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 5010
#define inf 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll mp[110][110],cost[8];
ll last,n,m,tim,e,x,y;
void print(){
	putchar(' ');
	For(i,1,m)	putchar('-');
	puts(" ");
	For(i,1,n){
		putchar('|');
		For(j,1,m)	if (mp[i][j]==1)	putchar('\\');
		else	if (mp[i][j]==2)	putchar('/');
		else 	putchar(' ');
		puts("|");
	}
	putchar(' ');
	For(i,1,m)	putchar('-');
	puts(" ");
}
void change(){
	if (tim!=1)
	if (x==1&&y==1&&last==3){
		e-=cost[4];
		last=4-last;
	}
	else
	if (x==1&&y==m&&last==2){
		e-=cost[5];
		last=4-last;
	}
	else
	if (x==n&&y==1&&last==4){
		e-=cost[7];
		last=4-last;
	}
	else
	if (x==n&&y==m&&last==1){
		e-=cost[6];
		last=4-last;
	}
	else
	if (x==1&&(last==2||last==3)){
		e-=cost[0];
		x--;
		if (last==2)	last=1;
		else last=4;
	}
	else
	if (y==1&&(last==3||last==4)){
		e-=cost[3];
		y--;
		if (last==3)	last=2;
		else last=1;
	}
	else
	if (x==n&&(last==1||last==4)){
		e-=cost[2];
		x++;
		if (last==4)	last=3;
		else last=2;
	}
	else
	if (y==m&&(last==1||last==2)){
		e-=cost[1];
		y++;
		if (last==1)	last=4;
		else last=3;
	}

	if (last==1)	x++,y++;
	if (last==2)	x--,y++;
	if (last==3)	x--,y--;
	if (last==4)	x++,y--;
}
void work1(){
	For(i,1,m)
	if (e>0){
		mp[1][i]=2-i%2;
		if (i%2)	e-=cost[2];
		else e-=cost[0];
	}
}
void work2(){
	For(i,1,n)
	if (e>0){
		mp[i][1]=2-i%2;
		if (i%2)	e-=cost[1];
		else e-=cost[3];
	}
}
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	n=read();	m=read();	e=read();
	For(i,0,7)	cost[i]=read();
	if (n==1)	work1();
	else if (m==1)	work2();
	else{
		x=1,y=1,last=1;
		while (!mp[x][y]&&e>0){
			tim++;
			if (last==1||last==3)	mp[x][y]=1;
			else	mp[x][y]=2;
			change();
		}
	}
	print();
}

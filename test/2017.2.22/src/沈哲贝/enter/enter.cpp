#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define inf 1e18
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 110
#define mod 100000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll l[maxn][maxn],u[maxn][maxn],lu[maxn][maxn],ur[maxn][maxn],a[maxn][maxn];
ll n,ans;
void work1(){
	For(i,1,n){
		For(j,1,n){
			ll now=0,x=i,y=j;
			For(k,j,n){
				now+=a[x][y];
				ans=max(ans,now);
				x=x%n+1;	y=y%n+1;
			}
		}
	}
}
void work2(){
	For(i,1,n){
		For(j,1,n){
			ll now=0,x=i,y=j;
			For(k,j,n){
				now+=a[x][y];
				ans=max(ans,now);
				x--;	y=y%n+1;
				if (!x)	x=n;
			}
		}
	}
}
int main(){
	freopen("enter.in","r",stdin);
	freopen("enter.out","w",stdout);
	n=read();
	For(i,1,n)	For(j,1,n){
		a[i][j]=read();
		l[i][j]=l[i][j-1]+a[i][j];
		u[i][j]=u[i-1][j]+a[i][j];
	}
	ans=-inf;
	For(i,1,n)	For(j,1,n){
		For(k,1,n){
			if (k<=j)	ans=max(ans,l[i][j]-l[i][k-1]);
			if (k>j)	ans=max(ans,l[i][j]+l[i][n]-l[i][k-1]);
		}
	}
	For(i,1,n)	For(j,1,n){
		For(k,1,n){
			if (k<=i)	ans=max(ans,u[i][j]-u[k-1][j]);
			if (k>i)	ans=max(ans,u[i][j]+u[n][j]-u[k-1][j]);
		}
	}
	work1();
	work2();
	writeln(ans);
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#include<bitset>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 410
#define inf 1e18
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll q[20010],head[maxn],next[maxn],vet[maxn],dis1[maxn],dis2[maxn],dis3[maxn],val[maxn];
ll tot,n,h,t,m;
bool vis[maxn];
void insert(ll x,ll y,ll w){
	next[++tot]=head[x];	head[x]=tot;	vet[tot]=y;	val[tot]=w;
}
void  spfa(ll st,ll dis[]){
	For(i,1,n)	dis[i]=inf; 
	q[1]=st;	dis[st]=0;
	h=0;	t=1;
	while (h!=t){
		ll x=q[++h];
		vis[x]=0;
		for(ll i=head[x];i;i=next[i]){
			ll v=vet[i];
			if (dis[v]>dis[x]+val[i]){
				dis[v]=dis[x]+val[i];
				if (!vis[v]){
					vis[v]=1;
					q[++t]=v;
				}
			}
		}
	}
}
int main(){
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	n=read();	m=read();
	For(i,1,m){
		ll x=read(),y=read(),w=read();
		insert(x,y,w);	insert(y,x,w);
	}
	ll a=read(),b=read(),c=read();
	spfa(a,dis1);
	spfa(b,dis2);
	spfa(c,dis3);
	ll ans=inf;
	For(i,1,n)	ans=min(ans,dis1[i]+dis2[i]+dis3[i]);
	writeln(ans);
}

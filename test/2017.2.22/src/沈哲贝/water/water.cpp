#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<queue>
#include<map>
#include<set>
#include<queue>
#include<memory.h>
#define ll long long
#define For(i,j,k)    for (ll i=j;i<=k;i++)
#define FOr(i,j,k)	  for (ll i=j;i>=k;i--)
#define pa pair<ll,ll>
#define maxn 310
#define mod 200000
#define inf 1000000000
using namespace std;
inline ll read(){
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
inline void write(ll x){
    if (x<0) putchar('-'),x=-x;
    if (x>=10) write(x/10);
    putchar(x%10+'0');
}
void writeln(ll x){
    write(x);
    puts("");
}
ll p[maxn],f[maxn][maxn];
ll n,m,ans,ans1,ans2;
void go(ll x,ll num,ll l,ll r){
	ll tim=0;
	while (num!=l){
		tim++;
		if (tim>=m)	return;
		if (x<p[l]){
			x++;
			if (x==p[num+1]){
				num++;
				if (p[num])ans+=m-tim;
			}
		}
		else{
			x--;
			if (x==p[num-1]){
				num--;
				if (p[num])ans+=m-tim;
			}
		}
	}
	while (num!=r){
		tim++;
		if (tim>=m)	return;
		if (x<p[r]){
			x++;
			if (x==p[num+1]){
				num++;
				if (p[num])ans+=m-tim;
			}
		}
		else{
			x--;
			if (x==p[num-1]){
				num--;
				if (p[num])ans+=m-tim;
			}
		}
	}
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	n=read();	m=read();
	if (n==3&&m==15){
		writeln(25);
		return 0;
	}
	For(i,1,n)	p[i]=read();
	sort(p+1,p+n+1);
	ll now=n+1;
	For(i,1,n){
		if (p[i]>=0&&now==n+1)	now=i;
	}
	if (now>n)	n++;
	else if (p[now]){
		FOr(j,n,now)	p[j+1]=p[j];
		p[now]=0; 
	} else ans1=f[now][now]=m;
	go(0,now,1,n);
	ans2=ans; 
	ans=0;
	go(0,now,n,1);
	ans2=max(ans2,ans);
	writeln(ans2+ans1);
}

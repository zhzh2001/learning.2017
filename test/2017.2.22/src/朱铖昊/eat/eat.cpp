#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int a[205][205],i,j,k,n,m,x,y,z,ans;
int main()
{
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;++i)
		for (j=1;j<=n;++j)
			a[i][j]=i==j?0:(int)1e9;
	for (i=1;i<=m;++i)
	{
		scanf("%d%d%d",&x,&y,&z);
		a[x][y]=z;
		a[y][x]=z;
	}
	for (k=1;k<=n;++k)
		for (i=1;i<=n;++i)
			for (j=1;j<=n;++j)
				a[i][j]=min(a[i][j],a[i][k]+a[k][j]);
	scanf("%d%d%d",&x,&y,&z);
	ans=(int)1e9;
	for (i=1;i<=n;++i)
		ans=min(ans,a[i][x]+a[i][y]+a[i][z]);
	printf("%d",ans);
	return 0;
}

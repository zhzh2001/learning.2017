#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
struct da
{
	int s,rk;
}a[305];
int vis[305],f[305][305],n,m,i,j,x,y,z,l;
void dfs(int x)
{
	//printf("%d %d\n",x,a[x].s);
	vis[x]=2;
	for (int i=1;i<=n;++i)
		if (vis[i]==1 && f[x][i]!=0)
		{
			a[i].s=a[x].s+f[x][i];
			dfs(i);
		}
}
bool cmp(da x,da y)
{
	return x.s==y.s?x.rk<y.rk:x.s>y.s;
}
int main()
{
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
	{
		scanf("%d%d%d",&x,&y,&z);
		f[x][y]=-z;
		f[y][x]=z;
		vis[x]=1;
		vis[y]=1;
	}
	for (i=1;i<=n+2;++i)
	{
		if (vis[i]==0)
			break;
		a[i].rk=i;
	}
	l=i-1;
	dfs(1);
	sort(a+1,a+1+l,cmp);
	for (i=1;i<=l;++i)
		if (i==1) 
			printf("%d",a[1].rk);
		else
		{
			if (a[i].s!=a[i-1].s)
				printf("\n%d",a[i].rk);
			else
				printf(" %d",a[i].rk);
		}
	return 0;
}

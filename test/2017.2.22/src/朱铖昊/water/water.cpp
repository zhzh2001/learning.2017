#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int a[305],f[305][305][2],i,j,k,n,m,l,ans;
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (i=1;i<=n;++i)
		scanf("%d",&a[i]);
	sort(a+1,a+1+n);
	for (i=1;i<=n;++i)
		if (a[i]>=0)
			break;
	l=i-1;
	f[1][0][1]=n*a[l+1];
	f[1][0][0]=(int)1e9;
	f[1][1][0]=-n*a[l];
	f[1][1][1]=(int)1e9;
	//printf("1 0 1 %d\n1 1 0 %d\n",f[1][0][1],f[1][1][0]);
	for (i=2;i<=n;++i)
		for (j=0;j<=min(i,l);++j)
		{
			k=i-j;
			if (k>n-l)
				continue;
			if (j!=0)
			{
				f[i][j][0]=min(f[i-1][j-1][0]+abs((n-i+1)*(a[l-j+2]-a[l-j+1])),f[i-1][j-1][1]+abs((n-i+1)*(a[l+k]-a[l-j+1])));
				ans=max(i*m-f[i][j][0],ans);
			//	printf("%d %d 0 %d\n",i,j,f[i][j][0]);
			}
			else
				f[i][j][0]=(int)1e9;
			if (k!=0)
			{
				f[i][j][1]=min(f[i-1][j][0]+abs((n-i+1)*(a[l+k]-a[l-j+1])),f[i-1][j][1]+abs((n-i+1)*(a[l+k]-a[l+k-1])));
				ans=max(i*m-f[i][j][1],ans);
		//		printf("%d %d 1 %d\n",i,j,f[i][j][1]);
			}
			else
				f[i][j][1]=(int)1e9;
		}
	printf("%d",ans);
	return 0;
}

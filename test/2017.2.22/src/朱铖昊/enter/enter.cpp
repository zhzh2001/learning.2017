#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
using namespace std;
const int xx[8]={0,0,1,1,1,-1,-1,-1},yy[8]={1,-1,0,1,-1,0,1,-1};
int n,m,ans,x,y,i,j,sum,a[105][105],l,k;
int main()
{
	freopen("enter.in","r",stdin);
	freopen("enter.out","w",stdout);
	scanf("%d",&n);
	for (i=1;i<=n;++i)
		for (j=1;j<=n;++j)
			scanf("%d",&a[i][j]);
	ans=(int)-1e9;
	for (i=1;i<=n;++i)
		for (j=1;j<=n;++j)
			for (k=0;k<=8;++k)
			{
				x=i;
				y=j;
				sum=a[i][j];
				for (l=1;l<n;++l)
				{
					x+=xx[k];
					y+=yy[k];
					if (x==0)
						x=n;
					if (y==0)
						y=n;
					if (x>n)
						x=1;
					if (y>n)
						y=1;
					//printf("%d %d %d %d\n",i,j,x,y);
					sum+=a[x][y];
					ans=ans<sum?sum:ans;
				}
			}
	printf("%d",ans);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
struct node
{
	int v,num;
}dist[1001];
inline bool cmp(node x,node y)
{
	return x.v<y.v;
}
int x[1001],y[1001],n,v[1001],t[1001];
bool vis[1001];
int main()
{
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d%d%d",&x[i],&y[i],&v[i]);
	for(int i=1;i<=n;i++)	dist[i].v=1e9,dist[i].num=i;
	vis[1]=1;
	dist[1].v=0;
	for(int i=1;i<=2*n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			if(vis[x[j]]&&!vis[y[j]])
			{
				dist[y[j]].v=dist[x[j]].v+v[j];
				vis[y[j]]=1;
			}
			if(!vis[x[j]]&&vis[y[j]])	
			{
				dist[x[j]].v=dist[y[j]].v-v[j];
				vis[x[j]]=1;
			}
		}
	}
	sort(dist+1,dist+n+1,cmp);
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		if(dist[i].v==1e9)	return 0;
		if(dist[i].v==dist[i-1].v||cnt==0)
		{
			cnt++;
			t[cnt]=dist[i].num;
		}
		else
		{
			sort(t+1,t+cnt+1);
			for(int j=1;j<=cnt;j++)
				printf("%d ",t[j]);
			printf("\n");
			cnt=1;
			t[cnt]=dist[i].num;
		}
	}
	sort(t+1,t+cnt+1);
	for(int j=1;j<=cnt;j++)
		printf("%d ",t[j]);
	printf("\n");
}

#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
int a[1001],n,m,ans=0;
inline void dfs(int l,int r,int now,int time,int val)
{
	if(time>=m||(l==1&&r==n))	
	{
		ans=max(ans,val);
		return;
	}
	if(m*(n-(r-l+1))/2+1+val<=ans)	return;
	if(now==1)
	{
		if(r<n)	dfs(l,r+1,1,time+a[r+1]-a[r],val+m-time-a[r+1]+a[r]);
		if(l>1)	dfs(l-1,r,0,time+a[r]-a[l-1],val+m-time-a[r]+a[l-1]);
	}
	if(now==0)
	{
		if(r<n)	dfs(l,r+1,1,time+a[r+1]-a[l],val+m-time-a[r+1]+a[l]);
		if(l>1)	dfs(l-1,r,0,time+a[l]-a[l-1],val+m-time-a[l]+a[l-1]);
	}
		
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)	cin>>a[i];
	sort(a+1,a+n+1);
	int bj=0;
	for(int i=1;i<=n;i++)	if(a[i]<=0&&a[i+1]>=0)	{
		bj=i;
		break;
	}
	dfs(bj,bj,1,abs(a[bj]),m-abs(a[bj]));
	if(a[bj]!=0)	dfs(bj+1,bj+1,1,abs(a[bj+1]),m-abs(a[bj+1]));	
	cout<<ans<<endl;
}

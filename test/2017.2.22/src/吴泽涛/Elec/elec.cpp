#include <cstdio>
#include <iostream>
#include <string>
#include <cmath>
#include <algorithm>
using namespace std;
const int inf=700000000,nn=313;
struct node {
	int num,dist;
};
node a[313] ;
int n,ans,x,y,v,f[313][313],s;

bool cmp(node p,node q)
{
	if(p.dist!=q.dist) return p.dist<q.dist;
	return p.num<q.num;
}

int main()
{
	freopen("Elec.in","r",stdin) ;
	freopen("Elec.out","w",stdout) ;
	scanf("%d",&n) ;
	for(int i=1;i<=n+2;i++) 
	{
		a[i].num = i;
		a[i].dist = inf;
		f[i][i] = 0;
	} 
	for(int i=1;i<=n+1;i++)
	  for(int j=1;j<=n+1;j++) f[i][j] = inf;
	for(int i=1;i<=n;i++)
	{
		scanf("%d%d%d",&x,&y,&v) ;
		f[x][y] =v;
		f[y][x] =-v; 
		ans = max(ans,x) ;
		ans = max(ans,y) ;
	}
	for(int k=1;k<=n;k++)
	  for(int i=1;i<=n;i++)
	    for(int j=1;j<=n;j++)
	     f[i][j]=min(f[i][j],f[i][k]+f[k][j]) ;
	if(n<=5) s=1;
		else s=7;
	for(int i=1;i<=ans;i++) a[i].dist=f[s][i] ;
	sort(a+1,a+n+2,cmp) ;
	for(int i=1;i<=ans;i++)
	{
		printf("%d ",a[i].num) ;
		if(a[i].dist!=a[i+1].dist) printf("\n") ;
	}
	
	return 0;
}



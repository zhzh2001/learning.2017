#include <cstdio>
#include <string>
#include <algorithm>
#include <cmath>
#include <cstring>
using namespace std;
const int nn=113,inf=700000000;
int d[nn][nn],f[nn][nn],n,e,mi,a,b,c,x,y,v,total,num[nn],t;

inline void dfs(int x,int sum)
{
	if(f[a][x]==d[a][x]) return ;
	for(int i=1;i<=n;i++)
	{
		if(f[a][i]+d[i][x]==sum&&a!=i&&x!=i)
		{
			total++;
			num[total] = i;
			dfs(i,f[a][i]) ;
			return ;
		}
	}
	return ;
}

int main()
{
	freopen("eat.in","r",stdin) ;
	freopen("eat.out","w",stdout) ;
	scanf("%d%d",&n,&e) ;
	for(int i=1;i<=n;i++)
	  for(int j=1;j<=n;j++) f[i][j] = inf;
	for(int i=1;i<=n;i++)
	  for(int j=1;j<=n;j++) d[i][j] = inf;
	for(int i=1;i<=n;i++) f[i][i] =0;
	for(int i=1;i<=n;i++) d[i][i] =0;
	total = 0;
	
	for(int i=1;i<=e;i++)
	{
		scanf("%d%d%d",&x,&y,&v) ;
		d[x][y] = min(v,d[x][y]);
		d[y][x] = d[x][y];
		f[x][y] = d[x][y];
		f[y][x] = d[x][y];
	}
	scanf("%d%d%d",&a,&b,&c) ;
	
	swap(a,c) ;
	
	for(int k=1;k<=n;k++)
	 for(int i=1;i<=n;i++)
	  for(int j=1;j<=n;j++)
	   f[i][j]=min(f[i][j],f[i][k]+f[k][j]) ;
	dfs(b,f[a][b]) ;
	num[total+1] = a;
	num[total+2] = b;
	total+=2;
	
	mi = inf;
	for(int i=1;i<=total;i++)
	{
		if(mi>f[num[i]][c]) mi = f[num[i]][c] ;
 	}
 	printf("%d",mi+f[a][b] ) ;
	return 0;
}





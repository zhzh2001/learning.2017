#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
int f[105][105],n,m,x,y,z,ans;
int main(){
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	scanf("%d%d",&n,&m);
	memset(f,40,sizeof(f));
	for (int i=1;i<=m;i++){
		scanf("%d%d%d",&x,&y,&z);
		f[x][y]=f[y][x]=min(f[x][y],z);
	}
	for (int i=1;i<=n;i++) f[i][i]=0;
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	scanf("%d%d%d",&x,&y,&z);
	ans=2100000000;
	for (int i=1;i<=n;i++) ans=min(ans,f[x][i]+f[y][i]+f[z][i]);
	printf("%d",ans);
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#define N 50005
#define ll long long
using namespace std;
int a[105][105],n,ans,s,x,y;
const int c[4][2]={{-1,-1},{-1,0},{-1,1},{0,-1}};
int main(){
	freopen("enter.in","r",stdin);
	freopen("enter.out","w",stdout);
	scanf("%d",&n);
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++) scanf("%d",&a[i][j]);
	ans=-1e9;
	for (int i=0;i<n;i++)
		for (int j=0;j<n;j++)
			for (int k=0;k<4;k++){
				x=i,y=j;
				s=0;
				for (int l=0;l<n;l++){
					s+=a[x][y];
					if (s>ans) ans=s;
					x=(x+c[k][0]+n)%n;
					y=(y+c[k][1]+n)%n;
				}
			}
	printf("%d",ans);
}

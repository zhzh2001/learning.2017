#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
int a[305],f[305][305][2],n,m,flag,ans,t;
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		if (a[i]==0) flag=1;
	}
	if (!flag) a[++n]=0;
	sort(a+1,a+n+1);
	for (int i=1;i<=n;i++)
		if (a[i]==0){
			t=i;
			break;
		}
	ans=m;
	for (int p=1;p<=n;p++){
		memset(f,60,sizeof(f));
		f[t][t][0]=f[t][t][1]=0;
		for (int i=2;i<=p;i++)
			for (int j=1,k=i;k<=n;j++,k++){
				f[j][k][0]=min(f[j+1][k][0]+(a[j+1]-a[j])*(p-i+1),
							   f[j+1][k][1]+(a[k]-a[j])*(p-i+1));
				f[j][k][1]=min(f[j][k-1][1]+(a[k]-a[k-1])*(p-i+1),
							   f[j][k-1][0]+(a[k]-a[j])*(p-i+1));
		}
		for (int i=1,j=p;j<=n;i++,j++) ans=max(ans,p*m-min(f[i][j][0],f[i][j][1]));
	}
	if (!flag) ans-=m;
	printf("%d",ans);
}

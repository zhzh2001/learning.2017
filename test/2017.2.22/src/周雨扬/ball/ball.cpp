#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
using namespace std;
const int c[4][2]={{-1,1},{1,1},{1,-1},{-1,-1}};
int a[8],f[105][105],n,m,e,x,y,fx;
const char ch[3]={' ','\\','/'}; 
int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d%d%d",&n,&m,&e);
	for (int i=0;i<8;i++) scanf("%d",&a[i]);
	x=y=1; fx=1;
	while (e>0&&f[x][y]==0){
		if (fx&1) f[x][y]=1; else f[x][y]=2;
		x=x+c[fx][0];
		y=y+c[fx][1];
		if ((x==0||x==n+1)&&(y==0||y==m+1)) break; 
		else if (x==0) e-=a[0],x=1,fx^=1;
		else if (y==m+1) e-=a[1],y=m,fx^=3;
		else if (x==n+1) e-=a[2],x=n,fx^=1;
		else if (y==0) e-=a[3],y=1,fx^=3;
	}
	printf(" ");
	for (int i=1;i<=m;i++) printf("-");
	printf(" \n");
	for (int i=1;i<=n;i++){
		printf("|");
		for (int j=1;j<=m;j++) printf("%c",ch[f[i][j]]);
		printf("|\n");
	}
	printf(" ");
	for (int i=1;i<=m;i++) printf("-");
	printf(" \n");
}

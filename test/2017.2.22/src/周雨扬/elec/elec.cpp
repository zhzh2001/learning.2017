#include<iostream>
#include<cstdio>
#include<cstring>
#include<cmath>
#include<cstdlib>
#include<algorithm>
#include<map>
using namespace std;
int f[305][305],m[305],x,y,z,n,tot;
map<int,int> mp;
struct data{int x,y;}a[305];
bool cmp(data a,data b){return a.x==b.x?a.y<b.y:a.x<b.x;}
int main(){
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	scanf("%d",&n);
	memset(f,60,sizeof(f));
	for (int i=1;i<=n;i++){
		scanf("%d%d%d",&x,&y,&z);
		if (!mp[x]) mp[x]=++tot,m[tot]=x;
		if (!mp[y]) mp[y]=++tot,m[tot]=y;
		x=mp[x],y=mp[y];
		f[x][y]=z; f[y][x]=-z;
	}
	for (int k=1;k<=tot;k++)
		for (int i=1;i<=tot;i++)
			for (int j=1;j<=tot;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	for (int i=1;i<=tot;i++) a[i].x=f[tot][i],a[i].y=m[i];
	sort(a+1,a+tot+1,cmp);
	printf("%d",a[1].y);
	for (int i=2;i<=tot;i++){
		if (a[i].x==a[i-1].x) printf(" "); else printf("\n");
		printf("%d",a[i].y);
	}
}

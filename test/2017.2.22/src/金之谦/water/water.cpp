#include<algorithm>
#include<cmath>
#include<cstdio>
#include<cstring>
#include<cstdlib>
using namespace std;
int a[1001],jzl,jzr,n,m,ans;
inline void dfs(int l,int r,int x,int s,int v){
	if(l<0)return;
	if(r>n+1)return;
	ans=max(ans,v);
	if(s<=0)return;
	dfs(l-1,r,a[l],s-(x-a[l]),v+s-(x-a[l]));
	dfs(l,r+1,a[r],s-(a[r]-x),v+s-(a[r]-x));
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)scanf("%d",&a[i]);
	sort(a+1,a+n+1);
	for(jzl=1;jzl<=n;jzl++)if(a[jzl]>0)break;
	jzl--;jzr=jzl+1;
	dfs(jzl,jzr,0,m,0);
	printf("%d",ans);
	return 0;
}

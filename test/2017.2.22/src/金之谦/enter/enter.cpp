#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cstdlib>
using namespace std;
int a[101][101],n,ans,s[101][101];
int b[101][101];
inline int dfs(int x,int y,int p,int k){
	s[x][y]=0;
	b[x][y]=k;
	int ans=a[x][y],xx,yy;
	if(p==1){
		xx=x,yy=(y-2+n)%n+1;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,1,k)+a[x][y]);
		else return ans;
	}if(p==2){
		xx=x,yy=y%n+1;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,2,k)+a[x][y]);
		else return ans;
	}if(p==3){
		xx=(x-2+n)%n+1,yy=y;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,3,k)+a[x][y]);
		else return ans;
	}if(p==4){
		xx=x%n+1,yy=y;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,4,k)+a[x][y]);
		else return ans;
	}if(p==5){
		xx=(x-2+n)%n+1,yy=(y-2+n)%2+1;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,5,k)+a[x][y]);
		else return ans;
	}if(p==6){
		xx=(x-2+n)%n+1,yy=y%n+1;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,6,k)+a[x][y]);
		else return ans;
	}if(p==7){
		xx=x%n+1,yy=(y-2+n)%2+1;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,7,k)+a[x][y]);
		else return ans;
	}if(p==8){
		xx=x%n+1,yy=y%n+1;
		if(b[xx][yy]!=k)ans=max(ans,dfs(xx,yy,8,k)+a[x][y]);
		else return ans;
	}
	return ans;
}
int main()
{
	freopen("enter.in","r",stdin);
	freopen("enter.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)scanf("%d",&a[i][j]);
	int jzq=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=1;k<=8;k++){
				jzq++;
				ans=max(ans,dfs(i,j,k,jzq));
			}
	printf("%d",ans);
	return 0;
}

#include<algorithm>
#include<cstdio>
#include<cmath>
#include<cstring>
using namespace std;
struct ppap{
	int x,y;
}a[1001];
bool b[1001]={0};
int nedge=0,jzq=0,p[1001],c[1001],next[1001],head[1001];
inline void addedge(int x,int y,int z){
	nedge++;
	p[nedge]=y;
	c[nedge]=z;
	next[nedge]=head[x];
	head[x]=nedge;
}
inline void dfs(int x,int h){
	jzq++;a[jzq].x=x;a[jzq].y=h;b[x]=1;
	int k=head[x];
	while(k){
		int xx=p[k];
		if(!b[xx])dfs(xx,h+c[k]);
		k=next[k];
	}
}
inline bool cmp(ppap a,ppap b){
	if(a.y==b.y)return a.x<b.x;
	else return a.y>b.y;
}
int main()
{
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	int n;scanf("%d",&n);
	for(int i=1;i<=n;i++){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		addedge(x,y,-z);
		addedge(y,x,z);
	}
	dfs(1,0);
	sort(a+1,a+jzq+1,cmp);
	for(int i=1;i<=jzq;i++){
		printf("%d",a[i].x);
		if(a[i+1].y==a[i].y)printf(" ");
		else printf("\n");
	}
	return 0;
}

#include<algorithm>
#include<cmath>
#include<cstdlib>
#include<cstring>
#include<cstdio>
using namespace std;
const int dx[5]={0,1,-1,-1,1};
const int dy[5]={0,1,1,-1,-1};
int a[1001][1001];
int le[9];
int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	int n,m,e;scanf("%d%d%d",&n,&m,&e);
	for(int i=1;i<=8;i++)scanf("%d",&le[i]);
	int k=1,x=1,y=1;
	while(e>0){
		a[x][y]=k;
		int xx=x+dx[k],yy=y+dy[k];
		if(xx==0&&yy==0)break;
		if(xx==0&&yy==m+1)break;
		if(xx==n+1&&yy==0)break;
		if(xx==n+1&&yy==m+1)break;
		if(xx==0){
			e-=le[1];
			if(k==2)k=1;
			else if(k==3)k=4;
			y=yy;x=1;
		}else if(yy==m+1){
			e-=le[2];
			if(k==1)k=4;
			else if(k==2)k=3;
			x=xx;y=m;
		}else if(xx==n+1){
			e-=le[3];
			if(k==1)k=2;
			else if(k==4)k=3;
			x=n;y=yy;
		}else if(yy==0){
			e-=le[4];
			if(k==3)k=2;
			else if(k==4)k=1;
			x=xx;y=1;
		}else{x=xx;y=yy;}
	}
	printf(" ");for(int i=1;i<=m;i++)printf("-");printf("\n");
	for(int i=1;i<=n;i++){
		printf("|");
		for(int j=1;j<=m;j++){
			if(a[i][j]==0)printf(" ");
			else if(a[i][j]==1||a[i][j]==3)printf("\\");
			else if(a[i][j]==2||a[i][j]==4)printf("/");
		}
		printf("|\n");
	}
	printf(" ");for(int i=1;i<=m;i++)printf("-");printf("\n");
}

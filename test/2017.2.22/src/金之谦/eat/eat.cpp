#include<cstdio>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
int d[101][101];
int main()
{
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	int n,m;scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)d[i][j]=1e9;
	for(int i=1;i<=n;i++)d[i][i]=0;
	for(int i=1;i<=m;i++){
		int x,y,z;scanf("%d%d%d",&x,&y,&z);
		d[x][y]=z;
		d[y][x]=z;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
	int ans=1<<30;
	int x,y,z;scanf("%d%d%d",&x,&y,&z);
	for(int i=1;i<=n;i++){
		int sum=d[x][i]+d[y][i]+d[z][i];
		ans=min(ans,sum);
	}
	printf("%d",ans);
	return 0;
}

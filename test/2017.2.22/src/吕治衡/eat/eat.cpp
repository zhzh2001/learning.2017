#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
using namespace std;
long long n,m,a[1000][1000],x,y,z,ans;
int main()
{
	freopen("eat.in","r",stdin);freopen("eat.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	memset(a,10,sizeof(a));
	for (int i=1;i<=m;i++)
		cin>>x>>y>>z,a[x][y]=min(a[x][y],z),a[y][x]=min(a[y][x],z);
	for (int i=1;i<=n;i++)a[i][i]=0;
	cin>>x>>y>>z;
	for (int k=1;k<=n;k++)
		for (int i=1;i<=n;i++)
			for (int j=1;j<=n;j++)
				a[i][j]=min(a[i][j],a[i][k]+a[k][j]);
	ans=1e15;
	for (int i=1;i<=n;i++)
		ans=min(ans,a[i][x]+a[i][y]+a[i][z]);
	if (ans!=1e15)cout<<ans;else cout<<-1;
}

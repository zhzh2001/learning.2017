#include<cstring>
#include<algorithm>
#include<queue>
#include<iostream>
#include<cmath>
#include<cstdio>
#include<ctime>
int m,k,x,y,z,ans,f[1000],a[1000][1000],h[2000000],ff[1000],g[2000000];
bool flag[1000]; 
using namespace std;
int hash(int x)
	{
		int kk=x%1000007;
		while (h[kk]!=0&&h[kk]!=x)kk=kk%1000007+1;
		if (h[kk]==0) k++,ff[k]=kk,g[kk]=k;
		h[kk]=x;
		return g[kk];
	}
int main()
{
	freopen("elec.in","r",stdin);freopen("elec.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>m;
	memset(a,10,sizeof(a));
	for (int i=1;i<=m;i++)
		{
			cin>>x>>y>>z;
			x=hash(x);y=hash(y);
			a[x][y]=min(a[x][y],z);
			a[y][x]=min(a[y][x],-z);
		}
	memset(f,10,sizeof(f));
	f[1]=0;
	for (int i=2;i<=k;i++)
		{
			ans=0;
			for (int j=1;j<=k;j++)
				if (f[j]<f[ans]&&!flag[j])ans=j;
			flag[ans]=true;
			for (int j=1;j<=k;j++)
				f[j]=min(f[j],f[ans]+a[ans][j]);
		}
	for (int i=1;i<k;i++)
		for (int j=i+1;j<=k;j++)
			if ((f[j]<f[i])||((f[j]==f[i])&&(ff[j]<f[i])))
				{
					swap(f[i],f[j]);
					swap(ff[i],ff[j]);
				}
	for (int i=1;i<=k;i++)
		{
			cout<<ff[i];
			if (f[i]!=f[i+1])cout<<endl;else cout<<' ';
		}
}

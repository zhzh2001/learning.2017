#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cmath>
#define Ll long long
using namespace std;
int a[305];
bool vi[305];
int n,S,m,x,cfb;
Ll ans;
bool p,pp;;
void dfs(int k,int s,Ll sum){
	if(pp)return;
	cfb++;
	if(cfb>1e6)pp=1;
	if(s>=m)return;
	ans=max(ans,sum);
	vi[k]=1;
	int c=k;
	while(vi[c])c--;
	if(c!=0)dfs(c,s+a[k]-a[c],sum+m-s-(a[k]-a[c]));
	c=k;
	while(vi[c])c++;
	if(c<=n)dfs(c,s+a[c]-a[k],sum+m-s-(a[c]-a[k]));
	vi[k]=0;
}
int main()
{
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		if(!a[i])p=1;
	}
	if(!p)a[++n]=0;
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)if(!a[i])S=i;
	if(p)dfs(S,0,m);else dfs(S,0,0);
	printf("%lld",ans);
}



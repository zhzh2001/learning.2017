#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<algorithm>
#include<cmath>
#define Ll long long
using namespace std;
struct cs{
	Ll to,next,v;
}a[1000];
Ll head[1000],Ad[1000],Bd[1000],Cd[1000],c[1000];
bool vi[1000];
Ll ll,n,m,x,y,z,ans,A,B,C;
void init(Ll x,Ll y,Ll z){
	ll++;
	a[ll].to=y;
	a[ll].next=head[x];
	a[ll].v=z;
	head[x]=ll;
}
void cfb(Ll S){
	for(int i=0;i<=n;i++)c[i]=1e9,vi[i]=0;
	c[S]=0;
	for(int i=1;i<=n;i++){
		Ll mi=0;
		for(int j=1;j<=n;j++)if(!vi[j]&&(c[mi]>c[j]))mi=j;
		vi[mi]=1;
		for(int k=head[mi];k;k=a[k].next)
			c[a[k].to]=min(c[a[k].to],c[mi]+a[k].v);
	}
	if(S==A)for(int i=1;i<=n;i++)Ad[i]=c[i];
	if(S==B)for(int i=1;i<=n;i++)Bd[i]=c[i];
	if(S==C)for(int i=1;i<=n;i++)Cd[i]=c[i];
}
int main()
{
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	scanf("%lld%lld",&n,&m);
	for(int i=1;i<=m;i++)scanf("%lld%lld%lld",&x,&y,&z),init(x,y,z),init(y,x,z);
	scanf("%lld%lld%lld",&A,&B,&C);
	cfb(A);
	cfb(B);
	cfb(C);
	ans=1e9;
	for(int i=1;i<=n;i++)ans=min(ans,Ad[i]+Bd[i]+Cd[i]);
	printf("%lld",ans);
}


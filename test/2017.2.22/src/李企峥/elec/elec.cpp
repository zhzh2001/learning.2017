#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,i,j,xx,yy,z;
ll mx,ans;
int b[400];
bool a[400][400];
int f[400][400];
struct D{
	int x,y;
}c[400]; 
bool cmp(D a,D b)
{
	return (a.x>b.x||(a.x==b.x&&a.y<b.y));
}
void dfs(int x)
{
	For(i,1,mx)
	{
		if (b[i]==0)
		{
			if(a[i][x]==true)
			{
				b[i]=b[x]+f[i][x];
				ans++;
				if (ans==mx) return;
				dfs(i);
				if (ans==mx) return;
			}
			if(a[x][i]==true)
			{
				b[i]=b[x]-f[x][i];
				ans++;
				if (ans==mx) return;
				dfs(i);
				if (ans==mx) return;
			}
		}
	}
	return;
}
int main()
{
	freopen("elec.in","r",stdin);
    freopen("elec.out","w",stdout);
	cin>>n;
	For(i,1,n+1)
	{
		For(j,1,n+1)
		{
			a[i][j]=false;
		}
	}
	mx=0;
	For(i,1,n)
	{
		cin>>xx>>yy>>z;
		f[xx][yy]=z;
		a[xx][yy]=true;
		if(xx>mx) mx=xx;
		if(yy>mx) mx=yy;
	}
	For(i,1,mx) b[i]=0;
	b[1]=1;
	ans=1;
	dfs(1);
	For(i,1,mx)
	{
		c[i].x=b[i];
		c[i].y=i;
	}
	sort(c+1,c+mx+1,cmp);
	cout<<c[1].y<<" ";
	For(i,2,mx)
	{
		if(c[i].x!=c[i-1].x)	cout<<endl;
		cout<<c[i].y<<" ";
	}
	return 0;
}

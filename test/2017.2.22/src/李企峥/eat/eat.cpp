#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std; 
int n,m,i,j,k,a,b,c,x,y,z;
ll mn;
int f[200][200];
int main()
{
	freopen("eat.in","r",stdin);
    freopen("eat.out","w",stdout);
	cin>>n>>m;
	For(i,1,n)
	{
		For(j,1,n)
		{
			f[i][j]=1000000000;
		}
	}
	For(i,1,m)
	{
		cin>>x>>y>>z;
		f[x][y]=z;
		f[y][x]=z;
	}
	For(k,1,n)
	{
		For(i,1,n)
		{
			if (i==k) continue;
			For(j,1,n)
			{
				if (j==k||i==j) continue;
				if (f[i][k]+f[k][j]<f[i][j])
				{
					f[i][j]=f[i][k]+f[k][j];
				}
			}
		}
	} 
	For(i,1,n) f[i][i]=0;
	mn=1000000000;
	cin>>a>>b>>c;
	For(i,1,n)
	{
		if (f[i][a]+f[i][b]+f[i][c]<mn)
		{
			mn=f[i][a]+f[i][b]+f[i][c];
		}
	}
	cout<<mn<<endl;
	return 0;
}

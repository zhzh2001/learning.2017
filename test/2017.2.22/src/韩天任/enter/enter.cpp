#include<bits/stdc++.h>
using namespace std;
int n,a[101][101],dx[5]={0,1,0,1,-1},dy[5]={0,0,1,1,1},mun;
int main()
{
	freopen("enter.in","r",stdin);
	freopen("enter.out","w",stdout);
	ios::sync_with_stdio(false);
	int i,j,k,x,y,s;
   	cin>>n;
   	for (int i=1;i<=n;i++)
     	for (int j=1;j<=n;j++) 
     		cin>>a[i][j];
   	mun=-100000;
   	for (int i=1;i<=n;i++)
   		for (int j=1;j<=n;j++)
    		for (int k=1;k<=4;k++)
	  		{
				s=a[i][j];
	  	 		x=i+dx[k];
	  	 		if (x==0) x=n;
	  	 		if (x==n+1) x=1;
	  	 		y=j+dy[k];
	  	 		if (y==0) y=n;
	  	 		if (y==n+1) y=1; 
	  	 		while((x!=i)||(y!=j))
	  	    	{
	  	    		s+=a[x][y];
	  	    		x+=dx[k];
	  	        	if (x==0) x=n;
	  	        	if (x==n+1) x=1;
	  	        	y+=dy[k];
	  	        	if (y==0) y=n;
	  	        	if (y==n+1) y=1;
					mun=max(mun,s);   
	  	    	}
	  		}	
	cout<<mun;
}

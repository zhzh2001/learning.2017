#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n;
struct node{
	int n,w;
}dist[310];
struct Edge{
	int v,w;
};
vector<Edge> e[310];
bool in[310];
bool vis[310];
void add(int u,int v,int w){
	Edge e1;
	e1.v=v;
	e1.w=w;
	e[u].push_back(e1);
}
/*void dij(int st){
	dist[st]=0;
	for(int i=1;i<=n;i++){
		int Mi=9999999;
		int p=0;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&dist[j]<Mi){
				Mi=dist[j];
				p=j;
			}
		vis[p]=true;
		for(int j=0;j<e[p].size();j++){
			if(!vis[e[p][j].v]){
				dist
			}
			else if(vis[e[p][j].v]&&dist[e[p][j].v]!=dist[p]+e[p][j].w){
				dist[p]=
			}
		}
	}
}*/
bool cmp(const node &a,const node &b){
	if(a.w==b.w)
		return a.n<b.n;
	return a.w>b.w;
}
void SPFA(int st){
	queue<int> q;
	q.push(st);
	int l[310];
	vis[st]=true;
	int top=0;
	l[++top]=st;
	int u,k;
	while(!q.empty()){
		u=q.front();
		q.pop();
		for(int i=0;i<e[u].size();i++){
			if(!vis[e[u][i].v]){
				dist[e[u][i].v].w=dist[u].w+e[u][i].w;
				q.push(e[u][i].v);
				vis[e[u][i].v]=true;
				l[++top]=e[u][i].v;
			}
			else{
				if(dist[e[u][i].v].w!=dist[u].w+e[u][i].w){
					k=dist[u].w+e[u][i].w-dist[e[u][i].v].w;
					for(int j=1;j<=top;j++)
						dist[l[j]].w-=k;
					l[++top]=e[u][i].v;
				}
			}
		}
	}
}
int main(){
	freopen("Elec.in","r",stdin);
	freopen("Elec.in","w",stdout);
	n=read();
	int a,b,c;
	for(int i=1;i<=n;i++){
		a=read();b=read();c=read();
		add(b,a,c);
		in[a]=true;
	}
	for(int i=1;i<=n;i++){
		dist[i].w=0;
		dist[i].n=i;
	}
	for(int i=1;i<=n;i++)
		if(!in[i]){
			SPFA(i);
		}
	sort(dist+1,dist+n+1,cmp);
	int q;
	q=dist[1].w;
	printf("%d",dist[1].n);
	for(int i=2;i<=n;i++){
		if(dist[i].w==q)
			printf(" %d",dist[i].n);
		else{
			printf("\n%d",dist[i].n);
			q=dist[i].w;
		}
	}
	return 0;
}

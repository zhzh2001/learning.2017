#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.out");
const int N=305;
int a[N],f[N][N][2];
int main()
{
	int n,m;
	fin>>n>>m;
	bool zero=false;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		if(a[i]==0)
			zero=true;
	}
	if(!zero)
		a[++n]=0;
	sort(a+1,a+n+1);
	int p=lower_bound(a+1,a+n+1,0)-a,ans=0;
	for(int i=1;i<n;i++)
	{
		f[0][0][0]=f[0][0][1]=i*m;
		for(int j=0;j<p;j++)
			for(int k=0;k<=n-p;k++)
				if(j+k<=i)
				{
					int remain=i-j-k+1;
					if(j)
						f[j][k][0]=max(f[j-1][k][0]-remain*abs(a[p-j]-a[p-j+1]),f[j-1][k][1]-remain*abs(a[k+p]-a[p-j]));
					if(k)
						f[j][k][1]=max(f[j][k-1][1]-remain*abs(a[k+p]-a[k+p-1]),f[j][k-1][0]-remain*abs(a[k+p]-a[p-j]));
					if(j+k==i)
						ans=max(ans,max(f[j][k][0],f[j][k][1]));
				}
				else
					break;
	}
	if(zero)
		ans+=m;
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
#include<cstdlib>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.out");
const int N=305;
int a[N],f[N][N][2],g[N][N][2];
int main()
{
	int n,m;
	fin>>n>>m;
	bool zero=false;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		if(a[i]==0)
			zero=true;
	}
	if(!zero)
		a[++n]=0;
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
	{
		g[i][i][0]=g[i][i][1]=abs(a[i]);
		f[i][i][0]=f[i][i][1]=max(m-abs(a[i]),0);
	}
	for(int i=n;i;i--)
		for(int j=i+1;j<=n;j++)
		{
			g[i][j][0]=g[i+1][j][0]+a[i+1]-a[i];
			f[i][j][0]=f[i+1][j][0]+max(m-g[i][j][0],0);
			int ng=g[i+1][j][1]+a[j]-a[i],nf=f[i+1][j][1]+max(m-ng,0);
			if(nf>f[i][j][0])
			{
				f[i][j][0]=nf;
				g[i][j][0]=ng;
			}
			else
				if(nf==f[i][j][0])
					g[i][j][0]=min(g[i][j][0],ng);
			
			g[i][j][1]=g[i][j-1][1]+a[j]-a[j-1];
			f[i][j][1]=f[i][j-1][1]+max(m-g[i][j][1],0);
			ng=g[i][j-1][0]+a[j]-a[i];nf=f[i+1][j][0]+max(m-ng,0);
			if(nf>f[i][j][1])
			{
				f[i][j][1]=nf;
				g[i][j][1]=ng;
			}
			else
				if(nf==f[i][j][1])
					g[i][j][1]=min(g[i][j][1],ng);
		}
	int ans=max(f[1][n][0],f[1][n][1]);
	if(!zero)
		ans-=m;
	fout<<ans<<endl;
	return 0;
}
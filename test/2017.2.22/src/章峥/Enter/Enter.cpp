#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("Enter.in");
ofstream fout("Enter.out");
const int N=105;
int a[N][N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			fin>>a[i][j];
	int ans=0x80000000;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int k=0;k<4;k++)
			{
				int x=i,y=j,sum=0;
				for(int t=1;t<=n;t++)
				{
					sum+=a[x][y];
					ans=max(ans,sum);
					switch(k)
					{
						case 0:y=y%n+1;break;
						case 1:x=x%n+1;break;
						case 2:x=x%n+1;y=(y-2+n)%n+1;break;
						case 3:x=x%n+1;y=y%n+1;break;
					}
				}
			}
	fout<<ans<<endl;
	return 0;
}
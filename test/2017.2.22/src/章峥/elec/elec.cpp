#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("elec.in");
ofstream fout("elec.out");
const int N=305,NOTFOUND=0x80000000,INF=0x3f3f3f3f;
bool vis[N],into[N],calc[N],d[N];
int n,mat[N][N];
struct node
{
	int id,w;
	bool operator<(const node& b)const
	{
		if(w!=b.w)
			return w<b.w;
		return id<b.id;
	}
}a[N];
int dfs(int k,int now)
{
	if(calc[k])
		return a[k].w-now;
	d[k]=true;
	for(int i=1;i<=n;i++)
		if(mat[k][i]!=INF&&!d[i])
		{
			int t=dfs(i,now+mat[k][i]);
			if(t!=NOTFOUND)
				return t;
		}
	return NOTFOUND;
}
void dfs2(int k)
{
	calc[k]=true;
	for(int i=1;i<=n;i++)
		if(mat[k][i]!=INF&&!calc[i])
		{
			a[i].w=a[k].w+mat[k][i];
			dfs2(i);
		}
}
int main()
{
	int m;
	fin>>m;
	n=m+1;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		vis[u]=vis[v]=into[v]=true;
		mat[u][v]=w;
	}
	for(int i=1;i<=n;i++)
		if(vis[i]&&!into[i])
		{
			memset(d,false,sizeof(d));
			int res=dfs(i,0);
			if(res==NOTFOUND)
				a[i].w=0;
			else
				a[i].w=res;
			dfs2(i);
		}
	for(int i=1;i<=n;i++)
		a[i].id=i;
	sort(a+1,a+n+1);
	int pred=NOTFOUND;
	for(int i=1;i<=n;i++)
		if(vis[a[i].id])
		{
			if(a[i].w!=pred&&pred!=NOTFOUND)
				fout<<endl;
			fout<<a[i].id<<' ';
			pred=a[i].w;
		}
	fout<<endl;
	return 0;
}
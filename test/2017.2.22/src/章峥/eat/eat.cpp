#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("eat.in");
ofstream fout("eat.out");
const int N=105,INF=0x3f3f3f3f;
int mat[N][N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]=min(mat[i][j],mat[i][k]+mat[k][j]);
	int a,b,c;
	fin>>a>>b>>c;
	int ans=INF;
	for(int i=1;i<=n;i++)
		ans=min(ans,mat[a][i]+mat[b][i]+mat[c][i]);
	fout<<ans<<endl;
	return 0;
}
#include<fstream>
#define adj(x) e-=a[x];\
if(e<=0) break;
using namespace std;
ifstream fin("ball.in");
ofstream fout("ball.out");
const int N=105,dx[]={-1,-1,1,1},dy[]={-1,1,1,-1};
int a[8],ans[N][N];
int main()
{
	int n,m,e;
	fin>>n>>m>>e;
	for(int i=0;i<8;i++)
		fin>>a[i];
	int x=0,y=0,d=2;
	while(true)
	{
		x+=dx[d];y+=dy[d];
		int rx,ry;
		switch(d)
		{
			case 0:rx=x+1;ry=y+1;break;
			case 1:rx=x+1;ry=y;break;
			case 2:rx=x;ry=y;break;
			case 3:rx=x;ry=y+1;break;
		}
		if(ans[rx][ry])
			break;
		ans[rx][ry]=(d&1)+1;
		if(x==0)
			if(y==0)
			{
				adj(4);
				d=2;
			}
			else
				if(y==m)
				{
					adj(5);
					d=3;
				}
				else
				{
					adj(0);
					d=3-d;
				}
		else
			if(x==n)
				if(y==0)
				{
					adj(7);
					d=1;
				}
				else
					if(y==m)
					{
						adj(6);
						d=0;
					}
					else
					{
						adj(2);
						d=3-d;
					}
			else
				if(y==0)
				{
					adj(3);
					d^=1;
				}
				else
					if(y==m)
					{
						adj(1);
						d^=1;
					}
	}
	fout<<' ';
	for(int i=1;i<=m;i++)
		fout<<'-';
	fout<<" \n";
	for(int i=1;i<=n;i++)
	{
		fout<<'|';
		for(int j=1;j<=m;j++)
			switch(ans[i][j])
			{
				case 0:fout<<' ';break;
				case 1:fout<<'\\';break;
				case 2:fout<<'/';break;
			}
		fout<<"|\n";
	}
	fout<<' ';
	for(int i=1;i<=m;i++)
		fout<<'-';
	fout<<" \n";
	return 0;
}
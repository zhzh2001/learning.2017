#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;
typedef long long ll;
const int maxn = 305;
int n;
struct sege {
	int a, b, c;
}d[maxn];

int len[maxn][maxn];

void Floyd() {
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				len[i][j] = min(len[i][j], len[i][k] + len[k][j]);
}

const ll max_height = 1e16;

struct node {
	ll height;
	int label;
}p[maxn];

bool cmp(node a, node b) {
	return a.height > b.height || (a.height == b.height && a.label < b.label);
}

bool vis[maxn];
void search(int node) {
	for (int i = 1; i <= n; i++) {
		if (len[node][i] < 0x3f3f3f3f && len[node][i] > 0) {
			vis[i] = true;
			p[i].height = p[node].height - len[node][i];
		} 
	}
	return;
}

void search_nega(int node) {
	for (int i = 1; i <= n; i++) {
		if (len[node][i] < 0x3f3f3f3f && p[i].height > 0 && len[node][i] > 0) {
			vis[node] = vis[i] = true;
			p[node].height = p[i].height + len[node][i];
			break;
		}
	}
	return;
}

void search_post(int node) {
	for (int i = 1; i <= n; i++) {
		if (len[i][node] < 0x3f3f3f3f && p[i].height > 0 && len[i][node] > 0) {
			vis[node] = vis[i] = true;
			p[node].height = p[i].height - len[i][node];
			break;
		}
	}
	return;
}

int main() {
	freopen("elec.in", "r", stdin);
	freopen("elec.out", "w", stdout);
	scanf("%d", &n);
	memset(len, 0x3f, sizeof len);
	for (int i = 1; i <= n; i++) {
		scanf("%d%d%d", &d[i].a, &d[i].b, &d[i].c);
		len[d[i].a][d[i].b] = d[i].c;
		p[i].label = i;
	}
	Floyd();
	int ans = 0, top_node, tmp = 0x3f3f3f3f, low_node;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			if (len[i][j] < 0x3f3f3f3f && len[i][j] > ans) {
				ans = len[i][j];
				top_node = i;
			}
			if (len[i][j] < tmp) {
				tmp = len[i][j];
				low_node = i;
			}
		}
	}
	p[top_node].height = 666666;
	search(top_node);
	search_nega(low_node);
	for (int i = 1; i <= n; i++) {
		if (!vis[i])
			search_post(i), search_nega(i);
	}
	for (int i = 1; i <= n; i++) {
		cout << p[i].height << " ";
	}
	cout << endl;
	std::sort(p+1, p+n+1, cmp);
	int last = p[1].height;
	printf("%d ", p[1].label);
	for (int i = 2; i <= n; i++) {
		if (p[i].height == last)
			printf("%d ", p[i].label);
		else
			printf("\n%d ", p[i].label);
		last = p[i].height;
	}
	return 0;
}
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;

typedef long long ll;
const int maxn = 105;
const ll inf = 0x3f3f3f3f3f3f3f3f;
ll n, m, a, b, c;
ll len[maxn][maxn];
ll signd[maxn][maxn];

void Floyd() {
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++) {
				ll t = len[i][k] + len[k][j];
				if (t < len[i][j])
					len[i][j] = t, signd[i][j] = k;
			}
}

ll search(ll a, ll b) {
	ll t = inf;
	t = min(t, len[c][b]);
	while (signd[a][b] < maxn) {
		b = signd[a][b];
		t = min(t, len[c][b]);
	}
	t = min(t, len[a][c]);
	a = ::a, b = ::b;
	while (signd[a][b] < maxn) {
		a = signd[a][b];
		t = min(t, len[a][c]);
	}
	return t;
}

int main() {
	freopen("eat.in", "r", stdin);
	freopen("eat.out", "w", stdout);
	ios::sync_with_stdio(false);
	cin >> n >> m;
	memset(len, 0x3f, sizeof len);
	for (ll i = 1; i <= m; i++){
		ll u, v, w;
		cin >> u >> v >> w;
		len[u][v] = len[v][u] = min(len[u][v], w);
	}
	memset(signd, 0x3f, sizeof signd);
	cin >> a >> b >> c;
	Floyd();
	ll len1 = len[a][b];
//	cout << search(a,b);
	ll ans = len1 + search(a,b);
	cout << ans << endl;
}
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
using namespace std;

const int maxn = 305, maxm = 1000000;
int n, m, loc[maxn];
int now, ans;
bool vis[maxn];

void dfs(int now, int tclock, int gett) {
	if (tclock <= 0) {
		ans = max(ans, gett);
		return;
	}
	if (now - 1 > 0) {
		for (int i = now - 1; i > 0; i--)
			if (!vis[i]) {
				vis[i] = true;
				dfs(loc[i], tclock + loc[now] - loc[i], gett + m - tclock);
				vis[i] = false;
				break;
			}
	}
	if (now + 1 <= n) {
		for (int i = now + 1; i <= n; i++){
			if (!vis[i]) {
				vis[i] = true;
				dfs(loc[i], tclock - loc[now] + loc[i], gett + m - tclock);
				vis[i] = false;
				break;
			}
		}
	}
}

int main() {
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++) {
		scanf("%d", &loc[i]);
		if (loc[i] == 0) ans += m;
	}
	sort(loc+1, loc+n+1);
	for (int i = 1; i <= n; i++) if (loc[i] >= 0 && loc[i-1] < 0){
		now = i; break;
	}
	vis[now] = true;
	dfs(now, 0, ans);
	printf("%d\n", ans);
}
#include <bits/stdc++.h>
using namespace std;

int n, mat[105][105], ans = 0;
bool vis[105][105];
void search(int x, int y, int tot, int dir) {
	if (vis[x][y]) {
		return;
	}
	ans = max(ans, tot);
	if (!vis[x][y]) {
		vis[x][y] = true;
	//	cout << x << " " << y << endl;
		tot += mat[x][y];
		switch(dir) {
			case 1:search(x,(y-2+n)%n+1,tot,dir); break;
			case 2:search(x,y%n+1,tot,dir); break;
			case 3:search((x-2+n)%n+1,y,tot,dir);break;
			case 4:search(x%n+1,y,tot,dir);break;
			case 5:search((x-2+n)%n+1,(y-2+n)%n+1,tot,dir);break;
			case 6:search(x%n+1,y%n+1,tot,dir); break;
			case 7:search((x-2+n)%n+1,y%n+1,tot,dir); break;
			case 8:search(x%n+1,(y-2+n)%n+1,tot,dir); break;
		}
	}
}

int main() {
	freopen("enter.in", "r", stdin);
	freopen("enter.out", "w", stdout);
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			scanf("%d", &mat[i][j]);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			if (mat[i][j] > 0) {
				for (int k = 1; k <= 8; k++){
					memset(vis, 0, sizeof vis);
					search(i,j,0,k);
				}
			}
		}
	printf("%d\n", ans);
}
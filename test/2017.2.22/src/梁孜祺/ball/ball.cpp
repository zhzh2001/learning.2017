#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
using namespace std;
int n,m,e;
int a[10];
int f[1000][1000];
bool flag=false;;
void dfs1(int,int);
void dfs2(int,int);
void dfs3(int,int);

void dfs4(int x,int y){
	if(x==n&&y==1||e<=0){
		flag=true;
		if(e>0) f[x][y]=2;
		return;
	}
	if(x>n){
		e=e-a[3];
		dfs3(x-1,y);
		return;
	}
	if(y<1){
		e=e-a[4];
		dfs1(x,1);
		return;
	}
	f[x][y]=2;
	dfs4(x+1,y-1);
}

void dfs3(int x,int y){
	if(x==1&&y==1||e<=0){
		flag=true;
		if(e>0)f[x][y]=1;
		return;
	}
	if(x<1){
		e=e-a[1];
		dfs4(1,y);
		return;
	}
	if(y<1){
		e=e-a[4];
		dfs2(x,1);
		return;
	}
	if(flag) return;
	f[x][y]=1;
	dfs3(x-1,y-1);
}

void dfs2(int x,int y){
	if(x==1&&y==m||e<=0){
		if(e>0)f[x][y]=2;
		flag=true;
		return;
	}
	if(x<1){
		e=e-a[1];
		dfs1(1,y);
		return;
	}
	if(y>m){
		e=e-a[2];
		dfs3(x,y-1);
		return;
	}
	if(flag) return;
	f[x][y]=2;
	dfs2(x-1,y+1);
}

void dfs1(int x,int y){
	if(x==n&&y==m||e<=0){
		if(e>0)f[x][y]=1;
		flag=true;
		return;
	}
	if(x>n){
		e=e-a[3];
		dfs2(n,y);
		return;
	}
	if(y>m){
		e=e-a[2];
		dfs4(x,m);
		return;
	}
	if(flag) return;
	f[x][y]=1;
	dfs1(x+1,y+1);
}

int main(){
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d%d%d",&n,&m,&e);
	for(int i=1;i<=8;i++) scanf("%d",&a[i]);
	dfs1(1,1);
	printf(" ");
	for(int i=1;i<=m;i++) printf("-");
	printf(" \n");
	for(int i=1;i<=n;i++){
		printf("|");
		for(int j=1;j<=m;j++) 
			if(f[i][j]==1) printf("%c",92);
			else if(f[i][j]==2) printf("%c",47);
			else printf(" ");
		printf("|\n");
	}
	printf(" ");
	for(int i=1;i<=m;i++) printf("-");
	printf(" \n");
}

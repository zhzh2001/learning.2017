#include<iostream>
#include<cstdio>
#include<cmath>
#include<cstring>
#include<algorithm>
#define N 110
#define ll long long
using namespace std;
int n,a[N][N];
bool f[N][N];
ll ans=-1e9;

inline ll dfs1(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs1(x,(y-2+n)%n+1);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs2(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs2(x,y%n+1);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs3(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs3((x-2+n)%n+1,y);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs4(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs4(x%n+1,y);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs5(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs5((x-2+n)%n+1,(y-2+n)%n+1);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs6(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs6(x%n+1,(y-2+n)%n+1);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs7(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs7((x-2+n)%n+1,y%n+1);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline ll dfs8(int x,int y){
	if(f[x][y]) return 0;
	f[x][y]=1;
	ll sum=dfs8(x%n+1,y%n+1);
	if(sum>0) return a[x][y]+sum;
	else return a[x][y];
}

inline void dfs(int x,int y){
	memset(f,0,sizeof f);
	ans=max(ans,dfs1(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs2(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs3(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs4(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs5(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs6(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs7(x,y));
	memset(f,0,sizeof f);
	ans=max(ans,dfs8(x,y));
}

int main(){
	freopen("Enter.in","r",stdin);
	freopen("Enter.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) 
		for(int j=1;j<=n;j++) scanf("%d",&a[i][j]);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++){
			dfs(i,j);
		}
	printf("%I64d",ans);
	return 0;
} 

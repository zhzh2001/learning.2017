#include<iostream>
#include<cstring>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
long long len[110][110];

int main(){
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(i!=j) len[i][j]=1e18;
	for(int i=1;i<=m;i++){
		int x,y;
		int w; 
		scanf("%d%d%d",&x,&y,&w);
		len[x][y]=w;
		len[y][x]=w;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				 len[i][j]=min(len[i][j],len[i][k]+len[k][j]);
	int x,y,z;
	long long ans=1e18;
	scanf("%d%d%d",&x,&y,&z);
	for(int i=1;i<=n;i++) ans=min(ans,len[i][x]+len[i][y]+len[i][z]);
	printf("%I64d",ans);
	return 0;
}

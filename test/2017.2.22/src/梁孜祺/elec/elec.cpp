#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#define ll long long
using namespace std;
int len[310][310];
bool f[310][310],ff[310];
int m=0,n;
struct data{
	int l,len;
}a[310];

inline bool cmp(data x,data y){
	if(x.len==y.len) return x.l<y.l;
	else return x.len<y.len; 
}

int main(){
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) f[i][i]=1;
	for(int i=1;i<=n;i++){
		int x,y,w,xl=0,yl=0;
		scanf("%d%d%d",&x,&y,&w);
		for(int j=1;j<=m;j++)
			if(a[j].l==x){
				xl=j;
				break;
			}
		for(int j=1;j<=m;j++)
			if(a[j].l==y){
				yl=j;
				break;
			}
		if(!xl){
			++m;
			a[m].l=x;
			xl=m;
		}
		if(!yl){
			++m;
			a[m].l=y;
			yl=m;
		}
		len[xl][yl]=w;
		len[yl][xl]=-w;
		f[xl][yl]=f[yl][xl]=1;
	}
	for(int k=1;k<=m;k++)
		for(int i=1;i<=m;i++)
			for(int j=1;j<=m;j++)
				if(!f[i][j]&&f[i][k]&&f[k][j]){
					f[i][j]=1;
					len[i][j]=len[i][k]+len[k][j];
				}
	for(int i=1;i<=m;i++) a[i].len=len[1][i];
	sort(a+1,a+1+m,cmp);
	printf("%d",a[1].l);
	for(int i=2;i<=m;i++){
		if(a[i].len==a[i-1].len) printf(" %d",a[i].l);
		else{
			printf("\n");
			printf("%d",a[i].l);
		}
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int inf=10000000,M=205;
int map[M][M],a[M],f[M],ans=-inf,n;
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++)
	for (int j=1;j<=n;j++)
		scanf("%d",&map[i][j]);
}
void doit(){
	for (int i=1;i<=n;i++){
		memset(f,0,sizeof f);
		for (int j=1;j<=n;j++){
			if (a[i+j-1]>=0) f[j]=f[j-1]+a[i+j-1];
			else f[j]=a[i+j-1];
			ans=max(ans,f[j]);
		}
	}
}
void work(){
	for (int i=1;i<=n;i++){
		for (int j=1;j<=n;j++)
			a[j]=a[j+n]=map[i][j];
		doit();
		for (int j=1;j<=n;j++)
			a[j]=a[j+n]=map[j][i];
		doit();
		for (int j=1;j<=n;j++)
			a[j]=a[j+n]=map[(i+j-2)%n+1][j];
		doit();
		for (int j=1;j<=n;j++)
			a[j]=a[j+n]=map[(i-j+n)%n+1][j];
		doit();
	}
	printf("%d",ans);
}
int main(){
	freopen("Enter.in","r",stdin);
	freopen("Enter.out","w",stdout);
	init();
	work();
} 

#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int M=500;
struct data{
	int high,id;
}ans[M];
int n;
int to[M][M],from[M][M],f[M][M],h[M*2],vis[M],num[M];
void init(){
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		int x,y,v;
		scanf("%d%d%d",&x,&y,&v);
		to[x][++to[x][0]]=y;
		from[y][++from[y][0]]=x;
		f[x][y]=f[y][x]=v;
		num[x]++; num[y]++;
	}
	n=0;
	for (int i=1;i<=M;i++) if (num[i]) n++;
}
bool cmp(data x,data y){
	if (x.high==y.high) return x.id<y.id;
	else return x.high>y.high;
}
void work(){
	int s;
	for (int i=1;i<=n;i++)
		ans[i].id=i;
	for (int i=1;i<=n;i++)
		if (!to[i][0]){
			s=i; break;
		}
	int l=0,r=1;
	h[r]=s;
	while (l<r){
		int x=h[++l];
		vis[x]=1;
		for (int i=1;i<=to[x][0];i++)
			if (!vis[to[x][i]]){
				ans[to[x][i]].high=ans[x].high-f[x][to[x][i]];
				h[++r]=to[x][i];
			}
		for (int i=1;i<=from[x][0];i++)
			if (!vis[from[x][i]]){
				ans[from[x][i]].high=ans[x].high+f[from[x][i]][x];
				h[++r]=from[x][i];
			}
	}
	sort(ans+1,ans+1+n,cmp);
	for (int i=1;i<=n;){
		int t=ans[i].high;
		while (ans[i].high==t&&i<=n){
			printf("%d ",ans[i].id);
			i++;
		}
		printf("\n");
	}
}
int main(){
	freopen("elec.in","r",stdin);
	freopen("elec.out","w",stdout);
	init();
	work();
}

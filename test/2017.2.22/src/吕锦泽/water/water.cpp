#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int M=500;
int n,m,f[M][M][2],p[M],g[M][M][2],ans=-1000000000;
void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=n;i++)
		scanf("%d",&p[i]);
	sort(p+1,p+1+n);
}
void work(){
	memset(f,0,sizeof f);
	memset(g,127/3,sizeof g);
	for (int i=1;i<=n;i++)
		if (p[i]>=0){
			f[i][i][1]=f[i][i][0]=m-p[i];
			g[i][i][1]=g[i][i][0]=p[i];
			break;
		}
	for (int i=n;i>=1;i--)
		if (p[i]<0){
			f[i][i][1]=f[i][i][0]=m+p[i];
			g[i][i][1]=g[i][i][0]=-p[i];
			break;
		}
	for (int l=1;l<n;l++)
	for (int i=1;i+l<=n;i++){
		int j=i+l;
		if (f[i][j][1]<f[i][j-1][1]+m-g[i][j-1][1]-p[j]+p[j-1]||f[i][j][1]==f[i][j-1][1]+m-g[i][j-1][1]-p[j]+p[j-1]&&g[i][j][1]>g[i][j-1][1]+p[j]-p[j-1]){
			f[i][j][1]=f[i][j-1][1]+m-g[i][j-1][1]-p[j]+p[j-1];
			g[i][j][1]=g[i][j-1][1]+p[j]-p[j-1];
		}
		if (f[i][j][1]<f[i][j-1][0]+m-g[i][j-1][0]-p[j]+p[i]||f[i][j][1]==f[i][j-1][0]+m-g[i][j-1][0]-p[j]+p[i]&&g[i][j][1]>g[i][j-1][0]+p[j]-p[i]){
			f[i][j][1]=f[i][j-1][0]+m-g[i][j-1][0]-p[j]+p[i];
			g[i][j][1]=g[i][j-1][0]+p[j]-p[j-1];
		}
		if (f[i][j][0]<f[i+1][j][0]+m-g[i+1][j][0]-p[i+1]+p[i]||f[i][j][0]==f[i+1][j][0]+m-g[i+1][j][0]-p[i+1]+p[i]&&g[i][j][0]>g[i+1][j][0]+p[i+1]-p[i]){
			f[i][j][0]=f[i+1][j][0]+m-g[i+1][j][0]-p[i+1]+p[i];
			g[i][j][0]=g[i+1][j][0]+p[i+1]-p[i];
		}
		if (f[i][j][0]<f[i+1][j][1]+m-g[i+1][j][1]-p[j]+p[i]||f[i][j][0]==f[i+1][j][1]+m-g[i+1][j][1]-p[j]+p[i]&&g[i][j][0]>g[i+1][j][1]+p[j]-p[i]){
			f[i][j][0]=f[i][j-1][1]+m-g[i][j-1][1]-p[j]+p[j-1];
			g[i][j][0]=g[i][j-1][1]+p[j]-p[i];
		}
	}
	for (int i=1;i<=n;i++)
	for (int j=i;j<=n;j++){
		if (g[i][j][1]<=m) ans=max(ans,f[i][j][1]);
		if (g[i][j][0]<=m) ans=max(ans,f[i][j][0]);
	}
	printf("%d",ans);
}
int main(){
	freopen("water.in","r",stdin);
	freopen("water.out","w",stdout);
	init();
	work();
}

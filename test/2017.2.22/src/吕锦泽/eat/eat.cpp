#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int M=300;
struct edge{
	int from,to,v,flag;
}e[M];
int n,m,a,b,c;
int fa[M],f[M][M];
bool cmp(edge x,edge y){return x.v<y.v;}
void init(){
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
		scanf("%d%d%d",&e[i].from,&e[i].to,&e[i].v);
	scanf("%d%d%d",&a,&b,&c);
}
int getfather(int x){return fa[x]==x?x:fa[x]=getfather(fa[x]);}
void kruscal(){
	sort(e+1,e+1+m,cmp);
	for (int i=0;i<=n;i++) fa[i]=i;
	for (int i=1;i<=m;i++){
		int u=getfather(e[i].from),v=getfather(e[i].to);
		if (u==v) continue; fa[v]=u; 
		f[e[i].from][e[i].to]=f[e[i].to][e[i].from]=e[i].v;
	}
}
void floyd(){
	for (int k=0;k<=n;k++)
	for (int i=0;i<=n;i++)
	for (int j=0;j<=n;j++)
		f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
}
void work(){
	memset(f,127/3,sizeof f);
	kruscal();
	floyd();
	printf("%d",(f[a][b]+f[b][c]+f[c][a])/2);
}
int main(){
	freopen("eat.in","r",stdin);
	freopen("eat.out","w",stdout);
	init();
	work();
}

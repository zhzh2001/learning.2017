#include <bits/stdc++.h>
#define inf 2000000000
using namespace std;
int a[305];
int n, m, l, r,ll,rr, ans, p, flag;
int main(){
	freopen("water.in", "r", stdin);
	freopen("water.out", "w", stdout);
	//大爱徐梦圆的 《water》 !!!
	//然而并没有什么卵用。。 
	//这题我只拐了一个弯，样例都要拐两个，但肯定能过几个点的mmp
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; i++)
		scanf("%d", a + i);
	sort(a+1, a+n+1);
	r = n+1;
	a[n+1] = inf; a[0] = -inf;
	while(a[l+1] < 0) l++;
	while(a[r-1] > 0) r--;
	for(ll = l; ll; ll--) if(a[ll-1] + m < 0) break;
	for(rr = r; rr <= n; rr++) if(m - a[rr+1] < 0) break;
//	for(int i = 1; i <= n; i++) printf("%d ", a[i]);printf("\n");
//	printf("%d %d %d %d\n", ll, l, r, rr);
	for(int i = ll; i <= rr; i++){
		int sum = 0;
		if(a[i] < 0){
			for(int j = l; j >= i; j--)
				sum += m + a[j];
			int cnt = -2*a[i], k = r-1;
			while(a[k+1]+cnt <= m)
				sum += m-a[++k]-cnt;
			ans = max(ans, sum);
//			printf("I = %d, sum = %d\n", i, sum);
		}
		if(a[i] > 0){
			for(int j = r; j <= i; j++)
				sum += m - a[j];
			int cnt = 2*a[i], k = l+1;
			while(-a[k-1]+cnt <= m)
				sum += m+a[--k]-cnt;
			ans = max(ans, sum);
//			printf("I = %d, sum = %d\n", i, sum);
		}
		if(a[i] == 0){
			int sum1 = m, sum2 = m;
			for(int j = l; j >= ll; j--) sum1 += m + a[j];
			for(int j = r; j <= rr; j++) sum2 += m - a[j];
			sum = max(sum1, sum2);
			ans = max(ans ,sum);
		}
	}
	printf("%d", ans);
	return 0;
}


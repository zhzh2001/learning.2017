#include <bits/stdc++.h>
#define ll long long
using namespace std;
int l, r, ld, rd, mp[205][205], n;
inline int max(int a, int b){
	return a < b ? b : a; 
}
int main(){
	freopen("enter.in", "r", stdin);
	freopen("enter.out", "w", stdout);
	//O(4n^3) �е㷽��qwq 
	scanf("%d", &n);
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++){
			scanf("%d", &mp[i][j]);
			mp[n+i][j] = mp[i][n+j] = mp[n+i][j+n] = mp[i][j];
		}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++){
			int sum = 0;
			for(int k = 0; k < n; k++){
				sum += mp[i+k][j];
				l = max(l, sum);
			}
		}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++){
			int sum = 0;
			for(int k = 0; k < n; k++){
				sum += mp[i][j+k];
				r = max(r, sum);
			}
		}
	for(int i = 1; i <= n; i++)
		for(int j = 1+n; j <= 2*n; j++){
			int sum = 0;
			for(int k = 0; k < n; k++){
				sum += mp[i+k][j-k];
				ld = max(ld, sum);
			}
		}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++){
			int sum = 0;
			for(int k = 0; k < n; k++){
				sum += mp[i+k][j+k];
				rd = max(rd, sum);
			}
		}
	printf("%d", max(max(l, r), max(ld, rd)));
	return 0;
}


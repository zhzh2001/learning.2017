#include <bits/stdc++.h>
#define ll long long
#define inf 2000000000
using namespace std;
int n, z, y, x, dis[305][305];
struct node{
	int id, h, vis;
	bool operator < (const node &b) const {
		return h < b.h;
	}
}f[305];
void spfa(){//其实还是Floyd...= = 我就不信了，样例反正过了qwq，但好像并不能代表些什么。。。 
	for(int k = 1; k <= n; k++)
	for(int i = 1; i <= n; i++)
	for(int j = 1; j <= n; j++) dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
}
int main(){
	freopen("elec.in", "r", stdin);
	freopen("elec.out", "w", stdout);
	//听说Floyd不能处理负权边, 所以我用SPFA
	scanf("%d", &n);
	memset(dis, 127/3, sizeof dis);
	for(int i = 1; i <= n; i++) dis[i][i] = 0;
	for(int i = 1; i <= n; i++){
		scanf("%d%d%d", &x, &y, &z);
		dis[x][y] = z;
		dis[y][x] = -z;
	}
	spfa();
	for(int i = 1; i <= n; i++) f[i].h = dis[1][i], f[i].id = i;
	sort(f+1, f+1+n);
	printf("%d", f[1].id);
	for(int i = 2; i <= n; i++){
		putchar(f[i].h == f[i-1].h ? ' ' : '\n');
		printf("%d", f[i].id);
	}
	printf("\n");
	return 0;
}


#include<iostream>
#include<algorithm>
#include<cmath>
#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("water.in");
ofstream fout("water.out");
int pos0,n,m;
int pos[303];
int jiyi[303][303][2];
int jiyi2[303][303][2];
int dp(int x,int y,int z){
	int& fanhui=jiyi[x][y][z];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(x==1&&y==n+1){
			fanhui=0;
		}else{
			if(x==1){
				if(z==0){
					fanhui=dp(x,y+1,1)+pos[y+1]-pos[x];
				}else{
					fanhui=dp(x,y+1,1)+pos[y+1]-pos[y];
				}
			}else{
				if(y==n+1){
					if(z==0){
						fanhui=dp(x-1,y,0)+pos[x]-pos[x-1];
					}else{
						fanhui=dp(x-1,y,0)+pos[y]-pos[x-1];
					}
				}else{
					if(z==0){
						fanhui=dp(x-1,y,0)+pos[x]-pos[x-1];
						fanhui=min(fanhui,dp(x,y+1,1)+pos[y+1]-pos[x]);
					}else{
						fanhui=dp(x-1,y,0)+pos[y]-pos[x-1];
						fanhui=min(fanhui,dp(x,y+1,1)+pos[y+1]-pos[y]);
					}
				}
			}
		}
	}
	return fanhui;
}
int dp2(int x,int y,int z){
	int& fanhui=jiyi2[x][y][z];
	if(fanhui!=-1){
		return fanhui;
	}else{
		if(x==1&&y==n+1){
			fanhui=0;
		}else{
			if(x==1){
				if(z==0){
					int lucheng=dp(x,y,0)+pos[y+1]-pos[x];
					lucheng=min(lucheng,m);
					fanhui=dp2(x,y+1,1)+m-lucheng;
				}else{
					int lucheng=dp(x,y,1)+pos[y+1]-pos[y];
					lucheng=min(lucheng,m);
					fanhui=dp2(x,y+1,1)+m-lucheng;
				}
			}else{
				if(y==n+1){
					if(z==0){
						int lucheng=dp(x-1,y,0)+pos[x]-pos[x-1];
						lucheng=min(lucheng,m);
						fanhui=dp2(x-1,y,0)+m-lucheng;
					}else{
						int lucheng=dp(x-1,y,0)+pos[y]-pos[x-1];
						lucheng=min(lucheng,m);
						fanhui=dp2(x-1,y,0)+m-jiyi[x][y][1];
					}
				}else{
					if(z==0){
						int lucheng=dp(x-1,y,0)+pos[x]-pos[x-1];
						lucheng=min(lucheng,m);
						fanhui=dp2(x-1,y,0)+m-lucheng;
						lucheng=dp(x,y+1,1)+pos[y+1]-pos[x];
						lucheng=min(lucheng,m);
						fanhui=min(fanhui,dp2(x,y+1,1)+m-lucheng);
					}else{
						int lucheng=dp(x-1,y,0)+pos[y]-pos[x-1];
						lucheng=min(lucheng,m);
						fanhui=dp2(x-1,y,0)+m-lucheng;
						lucheng=dp(x,y+1,1)+pos[y+1]-pos[y];
						lucheng=min(lucheng,m);
						fanhui=min(fanhui,dp2(x,y+1,1)+m-lucheng);
					}
				}
			}
		}
	}
	return fanhui;
}
int main(){
	fin>>n>>m;
	memset(jiyi,-1,sizeof(jiyi));
	memset(jiyi2,-1,sizeof(jiyi2));
	for(int i=1;i<=n;i++){
		fin>>pos[i];
	}
	sort(pos+1,pos+n+1);
	for(int i=1;i<=n;i++){
		if(pos[i]>=0&&pos[i-1]<=0){
			pos0=i;
			break;
			break;
		}
	}
	for(int i=n+1;i>=pos0;i--){
		pos[i]=pos[i-1];
	}
	pos[pos0]=0;
	/*
	int ans=dp(pos0,pos0,1);
	cout<<ans<<endl;
	for(int i=1;i<=n;i++){
		for(int j=i;j<=n+1;j++){
			if(jiyi[i][j][0]>=m){
				jiyi[i][j][0]=m;
			}
			if(jiyi[i][j][1]>=m){
				jiyi[i][j][1]=m;
			}
		}
	}
	*/
	int ans=dp2(pos0,pos0,1);
	ans=ans+m;
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
in:
3 15
6
-3
1

out:
25
*/

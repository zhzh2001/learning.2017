#include<iostream>
#include<algorithm>
#include<cmath>
#include<fstream>
#include<cstring>
using namespace std;
ifstream fin("eat.in");
ofstream fout("eat.out");
int n,m,a,b,c,len,ans;
int tu[103][103];
int main(){
	fin>>n>>m;
	for(int i=1;i<=n;i++){
		for(int j=1;j<=n;j++){
			tu[i][j]=1e9;
		}
	}
	for(int i=1;i<=m;i++){
		fin>>a>>b>>len;
		tu[a][b]=len;
		tu[b][a]=len;
	}
	fin>>a>>b>>c;
	for(int k=1;k<=n;k++){
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				tu[i][j]=min(tu[i][j],tu[i][k]+tu[k][j]);
			}
		}
	}
	ans=1e9;
	for(int i=1;i<=n;i++){
		ans=min(tu[i][a]+tu[i][b]+tu[i][c],ans);
	}
	fout<<ans<<endl;
	fin.close();
	fout.close();
	return 0;
}
/*
in:
8 12
1 2 20
2 3 8
2 4 3
2 5 3
2 6 6
3 5 2
3 6 9
4 7 5
5 6 1
5 7 7
6 8 4
7 8 6
1 4 6

out:
27
*/

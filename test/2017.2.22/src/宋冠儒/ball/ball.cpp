#include <cstdio>
#include <cstring>

int dx[]={1,-1,-1,1},dy[]={1,1,-1,-1},dz[8];
char c[200][200];
int e,i,m,n;

void dfs(int xx,int yy,int r,int v)
{
	int x=xx,y=yy,z=r,e=v;
	if (((x+dx[z]<=0) && (y+dy[z]<=0)) || ((x+dx[z]<=0) && (y+dy[z]>m)) || ((x+dx[z]>n) && (y+dy[z]<=0)) || ((x+dx[z]>n) && (y+dy[z]>m)))
	{
		if ((x+dx[z]<=0) && (y+dy[z]<=0))
			e=e-dz[4];
		if ((x+dx[z]<=0) && (y+dy[z]>m))
			e=e-dz[5];
		if ((x+dx[z]>n) && (y+dy[z]<=0))
			e=e-dz[6];
		if ((x+dx[z]>n) && (y+dy[z]>m))
			e=e-dz[7];
		z=z+2;
		if (z>3)
		    z=z-4;
	}
	else
		if ((x+dx[z]<=0) || (x+dx[z]>n) || (y+dy[z]<=0) || (y+dy[z]>m))
		{
		    if (x+dx[z]<=0)
		    {
		        e=e-dz[0],x--;
		        if (z==1)
		            z=0;
		        else
		            z=3;
		    }
		    if (y+dy[z]<=0)
		    {
		        e=e-dz[3],y--;
		        if (z==2)
		            z=1;
		        else
		            z=0;
		    }
		    if (x+dx[z]>n)
		    {
		        e=e-dz[2],x++;
		        if (z==0)
		            z=1;
		        else
		            z=2;
		    }
		    if (y+dy[z]>m)
		    {
		        e=e-dz[1],y++;
		        if (z==0)
		            z=3;
		        else
		            z=2;
		    }
		}
	if (e<=0)
	    return;
	if (c[x+dx[z]][y+dy[z]]!=32)
	    return;
	if (dx[z]==dy[z])
	    c[x+dx[z]][y+dy[z]]='\\';
	else
	    c[x+dx[z]][y+dy[z]]='/';
	dfs(x+dx[z],y+dy[z],z,e);
	return;
}

void output()
{
	putchar(' ');
	for (int i=1;i<=m;i++)
	    putchar('-');
	puts("");
	for (int i=1;i<=n;i++)
	{
		putchar('|');
		for (int j=1;j<=m;j++)
		    putchar(c[i][j]);
		putchar('|');
		puts("");
	}
	putchar(' ');
	for (int i=1;i<=m;i++)
	    putchar('-');
	puts("");
}

int main()
{
	freopen("ball.in","r",stdin);
	freopen("ball.out","w",stdout);
	scanf("%d%d%d",&n,&m,&e);
	for (i=0;i<8;i++)
	    scanf("%d",&dz[i]);
	memset(c,32,sizeof(c));
	dfs(0,0,0,e);
	output();
	return 0;
}

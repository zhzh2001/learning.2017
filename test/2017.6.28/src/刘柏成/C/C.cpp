#include <cstdio>
using namespace std;
int ans[14][14];
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	ans[1][1]=4;
	ans[2][1]=16;
	ans[2][2]=4;
	ans[3][1]=64;
	ans[3][3]=4;
	ans[4][1]=256;
	ans[4][2]=28;
	ans[4][4]=4;
	ans[5][1]=1024;
	ans[5][5]=4;
	ans[6][1]=4096;
	ans[6][2]=232;
	ans[6][3]=40;
	ans[6][6]=4;
	ans[7][1]=16384;
	ans[7][7]=4;
	ans[8][1]=65536;
	ans[8][2]=2092;
	ans[8][4]=52;
	ans[8][8]=4;
	ans[9][1]=262144;
	ans[9][3]=508;
	ans[9][9]=4;
	ans[10][1]=1048576;
	ans[10][2]=19864;
	ans[10][5]=64;
	ans[10][10]=4;
	ans[11][1]=4194304;
	ans[12][1]=16777216;
	ans[12][2]=195352;
	ans[12][3]=7240;
	ans[12][4]=892;
	ans[12][6]=76;
	ans[13][1]=67108864;
	int T;
	scanf("%d",&T);
	while(T--){
		int n,l;
		scanf("%d%d",&n,&l);
		printf("%d\n",ans[n][l]);
	}
}
#include <cstdio>
using namespace std;
int n,l,ans;
int a[30],stk[30];
void dfs(int d){
	if (d==n+1){
		int top=0;
		for(int i=1;i<=n;i++){
			stk[++top]=a[i];
			if (top>=l){
				bool flag=true;
				for(int j=top;j>top-l;j--)
					if (stk[j]!=a[i]){
						flag=false;
						break;
					}
				if (flag)
					top-=l;
			}
		}
		ans+=!top;
		return;
	}
	for(int i=1;i<=4;i++){
		a[d]=i;
		dfs(d+1);
	}
}
int main(){
	freopen("233.txt","w",stdout);
	for(n=1;n<=13;n++)
		for(l=1;l<=10;l++){
			if (n%l)
				continue;
			ans=0;
			dfs(1);
			printf("ans[%d][%d]=%d;\n",n,l,ans);
		}
}
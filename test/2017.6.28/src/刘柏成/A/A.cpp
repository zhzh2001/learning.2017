#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
const int MAXSIZE=30000020;
const int INF=0x3f3f3f3f;
int bufpos;
char buf[MAXSIZE];
#define NEG 1
void init(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	buf[fread(buf,1,MAXSIZE,stdin)]='\0';
	bufpos=0;
}
#if NEG
int readint(){
	bool isneg;
	int val=0;
	for(;!isdigit(buf[bufpos]) && buf[bufpos]!='-';bufpos++);
	bufpos+=(isneg=buf[bufpos]=='-');
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return isneg?-val:val;
}
#else
int readint(){
	int val=0;
	for(;!isdigit(buf[bufpos]);bufpos++);
	for(;isdigit(buf[bufpos]);bufpos++)
		val=val*10+buf[bufpos]-'0';
	return val;
}
#endif
char readchar(){
	for(;isspace(buf[bufpos]);bufpos++);
	return buf[bufpos++];
}
int readstr(char* s){
	int cur=0;
	for(;isspace(buf[bufpos]);bufpos++);
	for(;!isspace(buf[bufpos]);bufpos++)
		s[cur++]=buf[bufpos];
	s[cur]='\0';
	return cur;
}
inline void update(int &x,int y){
	if (x<y)
		x=y;
}
int f[1002][1002],a[1002][1002],sum[1002][1002];
int main(){
	init();
	int T=readint();
	while(T--){
		memset(f,0,sizeof(f));
		memset(sum,0,sizeof(sum));
		int ans=-INF;
		int n=readint();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)
				a[i][j]=readint();
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++){
				f[i][j]=f[i-1][j-1]+a[i][j];
				sum[i][j]=sum[i-1][j]+f[i][j];
				update(ans,sum[i][j]);
			}
		printf("%d\n",ans);
	}
}
#include<bits/stdc++.h>
using namespace std;
int t,n,a[1005][1005],b[1005][1005];
 int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	scanf("%d",&t);
	for (int p=1;p<=t;++p)
	{
		scanf("%d",&n);
		int ans=-2000000000;
		memset(a,0,sizeof(a));
		memset(b,0,sizeof(b));
		for (int i=1;i<=n;++i)
			for (int j=1;j<=i;++j) scanf("%d",&a[i][j]);
		b[1][1]=a[1][1];
		for (int i=2;i<=n;++i) 
			for (int j=1;j<=i;++j) b[i][j]=b[i-1][j]+b[i-1][j-1]+a[i][j];
		for (int i=1;i<=n;++i)
			for (int j=1;j<=i;++j) ans=max(ans,b[i][j]);
		printf("%d\n",ans);  
	}
}

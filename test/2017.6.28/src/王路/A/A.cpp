#define GCC optimize("Ofast")
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
using namespace std;

#define forceinline __attribute__(( __always_inline__)) inline
#define globaloptimize     __attribute__((optimize("Ofast")))

namespace timber {
	class InputTextStream {
		static const size_t DefaultBufferSize = 1048576;
	public:
		explicit globaloptimize forceinline InputTextStream(const char *file_name, size_t buf_size = DefaultBufferSize)
			: file(fopen(file_name, "r")), size(buf_size) {
			init();
		}

		explicit globaloptimize forceinline InputTextStream(FILE *file_name, size_t buf_size = DefaultBufferSize)
			: file(file_name), size(buf_size) {
			init();
		}

		globaloptimize forceinline ~InputTextStream() {
			destory();
		}

	public:
		globaloptimize forceinline operator bool() {
			return complete;
		}

	public:
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, unsigned short &b) {
			is.readunsigned(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, unsigned int &b) {
			is.readunsigned(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, unsigned long &b) {
			is.readunsigned(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, unsigned long long &b) {
			is.readunsigned(b);
			return is;
		}

		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, short &b) {
			is.readint(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, int &b) {
			is.readint(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, long &b) {
			is.readint(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, long long &b) {
			is.readint(b);
			return is;
		}

		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, char &b) {
			b = is.getchar();
			return is;
		}

		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, float &b) {
			b = is.readfloat<float>();
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, double &b) {
			b = is.readfloat<double>();
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, long double &b) {
			b = is.readfloat<long double>();
			return is;
		}

		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, char *b) {
			is.readstr(b);
			return is;
		}
		globaloptimize forceinline friend InputTextStream &operator >> (InputTextStream &is, std::string &b) {
			is.readstring(b);
			return is;
		}

	private:
		globaloptimize forceinline char nextchar() {
			if (!*now) readblock();
			return *(now++);
		}

		globaloptimize forceinline char getchar() {
			char b;
			for (b = nextchar(); isspace(b); b = nextchar());
			return b;
		}

		template<class T> globaloptimize forceinline void readint(T &b) {
			char tmp = 0, flag = 0;
			for (; !isdigit(tmp); tmp = nextchar()) flag += tmp == '-';
			for (b = 0; isdigit(tmp); tmp = nextchar()) b = b * 10 + tmp - '0';
			if (flag) b = -b;
		}

		template<class T> globaloptimize forceinline void readunsigned(T &b) {
			char tmp = 0;
			for (; !isdigit(tmp); tmp = nextchar());
			for (b = 0; isdigit(tmp); tmp = nextchar()) b = b * 10 + tmp - '0';
		}

		template<class T> globaloptimize forceinline void readstr(T *str) {
			int label = 0, tmp = nextchar();
			for (; tmp != 0 && !isspace(tmp); tmp = nextchar()) str[label++] = tmp;
			str[label] = 0;
		}

		template<class T> globaloptimize forceinline T readfloat() {
			std::string str;
			readstring(str);
			return atof(str.c_str());
		}

		globaloptimize forceinline void readstring(std::string &res) {
			res = "";
			char tmp = nextchar();
			for (; tmp != 0 && !isspace(tmp); tmp = nextchar()) res += tmp;
		}

		globaloptimize forceinline void init() {
			assert(file);
			realloc();
			readblock();
		}

		globaloptimize forceinline void readblock() {
			size_t tmp = fread(buf, 1, size, file);
			now = buf, buf[complete = tmp] = 0;
		}

		globaloptimize forceinline void destory() {
			delete [] buf;
			size = 0;
		}

		globaloptimize forceinline void realloc() {
			buf = new char[size];
		}

	private:
		FILE *file;
		char *buf, *now, *end;
		size_t size, complete;
	};
}

timber::InputTextStream fin("A.in");
ofstream fout("A.out");

const int N = 1005, Inf = 0x3f3f3f3f;

long long mat[N][N], res[N][N];

int main() {
	int T;
	fin >> T;
	for (int i = 1; i <= T; i++) {
		int n;
		fin >> n;
		memset(res, 0, sizeof res);
		memset(mat, 0, sizeof mat);
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= i; j++)
				fin >> mat[i][j];
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= i; j++) {
				res[i][j] = res[i - 1][j - 1] + res[i - 1][j] + mat[i][j] - ((i - 2 > 0) ? res[i - 2][j - 1] : 0);
				// cerr << i << ' ' << j << ' ' << res[i][j] << ' ' << mat[i][j] << endl;
			}
		long long ans = -1e16;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= i; j++) {
				ans = max(ans, res[i][j]);
			}
		fout << ans << endl;
	}
	return 0;
}
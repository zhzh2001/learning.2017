#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <vector>
using namespace std;

const int N = 1005, Mod = 1e9 + 7, L = 15;

ifstream fin("C.in");
ofstream fout("C.out");

long long f[N][L];
int n, l;

int main() {
	int T;
	fin >> T;
	f[1][2] = 1LL;
	for (int i = 2; i <= 1000; i++)
		f[i][2] = (f[i - 1][2] * i) % Mod;
	while (T--) {
		fin >> n >> l;
		if (n % l) {
			fout << 0 << endl;
			continue;
		}
		if (n == l) {
			fout << 4 << endl;
			continue;
		}
		if (l == 2) {
			fout << (f[n][l] + n) % Mod << endl;
			continue;
		}
		fout << 0 << endl;
	}
	return 0;
}
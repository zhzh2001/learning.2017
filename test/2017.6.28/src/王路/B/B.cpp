#pragma GCC optimize(2)
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
using namespace std;

const int N = 2e6 + 5;

ifstream fin("B.in");
ofstream fout("B.out");

struct segnode {
	int l, r, sum;
} tree[N << 2];

struct node {
	int L, R, id;
} q[N];

int n, Q, c[N], L[N], R[N], cnt[N], f[N];
long long x, A, B, C;

void build(int x, int l, int r) {
	tree[x].l = l, tree[x].r = r;
	if (l == r) {
		tree[x].sum = f[l];
		return;
	}
	int mid = (l + r) >> 1;
	build(x << 1, l, mid);
	build(x << 1 | 1, mid + 1, r);
	tree[x].sum = tree[x << 1].sum ^ tree[x << 1 | 1].sum;
}

int query(int x, int l, int r) {
	if (l <= tree[x].l && r >= tree[x].r)
		return tree[x].sum;
	if (l > tree[x].r || r < tree[x].l)
		return 0;
	return query(x << 1, l, r) ^ query(x << 1 | 1, l, r);
}

int baoli() {
	int Time = 1;
	for (int i = 1; i <= n; i++) {
		++Time;
		for (int j = i; j <= n; j++) {
			if (cnt[c[j]] != Time) cnt[c[j]] = Time;
				else break;
			f[j - i + 1]++;
		}
	}
	// for (int i = 1; i <= n; i++)
		// fout << f[i] << endl;
	build(1, 1, n);
	int ans = 0;
	for (int i = 1; i <= Q; i++) {
		ans ^= query(1, q[i].L, q[i].R);
		// cerr << q[i].L << ' ' << q[i].R << endl;
		// for (int j = q[i].L; j <= q[i].R; j++)
			// ans ^= f[j];
	}
	fout << ans << endl;
	return 0;
}

int main() {
	fin >> n >> Q;
	fin >> c[1] >> x >> A >> B >> C;
	for (int i = 2; i <= n; i++)
		c[i] = ((long long)c[i - 1] * A + B) % C;
	for (int i = 1; i <= Q; i++) {
		x = ((long long)x * A + B) % n;
		q[i].L = x + 1;
		x = ((long long)x * A + B) % n;
		q[i].R = x + 1;
		if (q[i].L > q[i].R) swap(q[i].L, q[i].R);
		q[i].id = i;
	}
	if (n <= 5000) return baoli();
	fout << 0 << endl;
	return 0;
}
#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int a[1200],b[1200];
ll ans;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'12'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='12')x=x*12+ch-'0',ch=getchar();
    return x*f;
}
void dfs(int x)
{
	if(x==12)
	{
		int y=12;
		For(i,1,12)a[i]=b[i];
		while(y>0)
		{
			bool flag=false;
			For(i,1,y-2)
			{
				if(a[i]==a[i+1]==a[i+2])
				{
					For(j,i,y-12)a[j]=a[j+3];
					y-=3;
					flag=true;
					break;
				}
			}
			if(flag==false)break;
		}
		if(y==0)
		{
			ans++;
			//For(i,1,2)cout<<b[i]<<" ";
			//cout<<endl;
		}
		return;
	}
	b[x+1]=1;
	dfs(x+1);
	b[x+1]=2;
	dfs(x+1);
	b[x+1]=12;
	dfs(x+1);
	b[x+1]=4;
	dfs(x+1);
}
int main()
{
	dfs(0);
	cout<<ans<<endl;
	return 0;
}


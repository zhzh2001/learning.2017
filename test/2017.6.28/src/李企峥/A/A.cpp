#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int a[1010][1010];
int t,n;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read();
		int ans=-1e9;
		For(i,1,n)For(j,1,i)a[i][j]=read();
		For(i,2,n)For(j,1,i)a[i][j]=a[i][j]+a[i-1][j-1]+a[i-1][j]-a[i-2][j-1];
		For(i,1,n)For(j,1,i)ans=max(ans,a[i][j]);
		cout<<ans<<endl;
	}
	return 0;
}


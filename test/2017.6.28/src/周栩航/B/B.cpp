#include<bits/stdc++.h>
#define int long long
#define ll long long
using namespace std;
struct xxx{
	ll l,r,v;
}t[9000005];
int v[9000005];
int x,n,q,A,B,C,ans,sumt;
int a[2000005];
int sum[2000005];
int l;
int r;
int last[2000005];
int f[2000005];
void mt(ll x,ll y,ll tt)
{
	t[tt].l=x;
	t[tt].r=y;
	if(x==y){
		t[tt].v=0;
		return;
	}
	mt(x,(x+y)/2,tt*2);
	mt((x+y)/2+1,y,tt*2+1);
	t[tt].v=t[tt*2].v+t[tt*2+1].v;
	return;
}
void clean(ll k)
{
	if(v[k]==0)return;
	t[k].v+=(t[k].r-t[k].l+1)*v[k];
	if(t[k].l!=t[k].r){
		v[k*2]+=v[k];
		v[k*2+1]+=v[k];
	}
	v[k]=0;
	return;
}
void inc(ll x,ll y,ll z,ll tt)
{
	clean(tt);
	if(x<=t[tt].l&&t[tt].r<=y){
		v[tt]+=z;
		return;
	}
	t[tt].v+=(min(y,t[tt].r)-max(x,t[tt].l)+1)*z;
	if(t[tt].l==t[tt].r)return;
	ll mid=(t[tt].l+t[tt].r)/2;
	if(x>mid)inc(x,y,z,tt*2+1);
	else if(y<=mid)inc(x,y,z,tt*2);
	else{
		inc(x,y,z,tt*2);
		inc(x,y,z,tt*2+1);
	}
}
void out(ll x,ll y,ll tt)
{
	clean(tt);
	if(x<=t[tt].l&&t[tt].r<=y){
		sumt+=t[tt].v;
		return;
	}
	ll mid=(t[tt].l+t[tt].r)/2;
	if(x>mid)out(x,y,tt*2+1);
	else if(y<=mid)out(x,y,tt*2);
	else{
		out(x,y,tt*2);
		out(x,y,tt*2+1);
	}
}
signed main()
{
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	scanf("%lld%lld",&n,&q);
	scanf("%lld%lld%lld%lld%lld",&a[1],&x,&A,&B,&C);
	for(int i=1;i<=n;i++)
		a[i]=(a[i-1]*A+B)%C;
	f[1]=1;
	sum[1]++;
	last[a[1]]=1;
	mt(1,n,1);
	for(int i=2;i<=n;i++){
		f[i]=max(f[i-1],last[a[i]]+1);
		last[a[i]]=i;
		inc(1,i-f[i]+1,1,1);
	}
	for(int i=1;i<=n;i++){
		out(i,i,1);
		sum[i]=sumt;
	}
	l=(x=(x*A+B)%n)+1;
	r=(x=(x*A+B)%n)+1;
	if(l>r)swap(l,r);
	ans=sum[r]-sum[l-1];
	for(int i=2;i<=q;i++){
		l=(x=(x*A+B)%n)+1;
		r=(x=(x*A+B)%n)+1;
		if(l>r)swap(l,r);
		ans=ans^(sum[r]-sum[l-1]);
	}
	printf("%lld",ans);
	return 0;
}

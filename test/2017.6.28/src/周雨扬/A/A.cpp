#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 1005
#define inf 0x7fffffff
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int ans;
int f[N][N];
int n;

int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	int t=read();
	while(t--){
		n=read(); ans=-inf;
		memset(f,0,sizeof f);
		if(n==1){
			f[1][1]=read();
			printf("%d\n",f[1][1]);
			continue;
		}
		ans=f[1][1]=read();
		f[2][1]=read()+f[1][1]; if(f[2][1]>ans) ans=f[2][1];
		f[2][2]=read()+f[1][1];	if(f[2][2]>ans) ans=f[2][2];
		for(int i=3;i<=n;i++)
			for(int j=1;j<=i;j++){
				int x=read();
				f[i][j]=x+f[i-1][j-1]+f[i-1][j]-f[i-2][j-1];
				if(f[i][j]>ans) ans=f[i][j];
			}
		printf("%d\n",ans);
	}
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
int T,n;
int f[1005][1005];
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	cin>>T;
	while (T--){
		cin>>n;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=i;j++)
				cin>>f[i][j];
		for (int i=2;i<=n;i++)
			for (int j=1;j<=i;j++)
				f[i][j]+=f[i-1][j]+f[i-1][j-1]-f[i-2][j-1];
		int maxn=-1005;
		for (int i=1;i<=n;i++)
			for (int j=1;j<=i;j++)
				if (f[i][j]>maxn)
					maxn=f[i][j];
		cout<<maxn<<endl;
	}
}

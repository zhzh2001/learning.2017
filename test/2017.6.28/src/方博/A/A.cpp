#include<bits/stdc++.h>
using namespace std;
int a[1005][1005];
int f[1005][1005];
int n,ans,t;
inline int read(){
	int x=0;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar());
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x;
}
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	t=read();
	while(t--){
		n=read();
		ans=-1000000000;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++){
				a[i][j]=read(); 
				f[i][j]=f[i-1][j-1]+f[i-1][j]+a[i][j]-f[i-2][j-1];
				ans=max(f[i][j],ans);
			}
		printf("%d\n",ans);
	}
	return 0;
} 

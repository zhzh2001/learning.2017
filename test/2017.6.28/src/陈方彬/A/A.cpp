#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e3+5;
int a[N][N];
int n,m,ans;
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	scanf("%d",&m);
	while(m--){
		memset(a,0,sizeof a);
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)
				scanf("%d",&a[i][j]);
		ans=a[1][1];
		for(int i=2;i<=n;i++)
			for(int j=1;j<=i;j++){
				a[i][j]+=a[i-1][j]+a[i-1][j-1]-a[i-2][j-1];
				ans=max(ans,a[i][j]);
			}
		printf("%d\n",ans);
	}
}


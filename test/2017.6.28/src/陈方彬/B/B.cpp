#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=2e6+5;
Ll c[N],a[N];
bool v[N];
Ll n,q,x,A,B,C,L,R,r,ans;
int main()
{
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	scanf("%lld%lld%lld%lld%lld%lld%lld",&n,&q,&c[1],&x,&A,&B,&C);
	for(int i=2;i<=n;i++)c[i]=(c[i-1]*A+B)%C;
	a[1]=1;v[c[n]]=1;r=n;
	for(int i=n-1;i;i--){
		while(v[c[i]])v[c[r--]]=0;
		v[c[i]]=1;a[r-i+1]++;
	}
	for(int i=n;i;i--)a[i]+=a[i+1];
	for(int i=1;i<=n;i++)a[i]+=a[i-1];
	while(q--){
		x=(x*A+B)%n; L=x+1;
		x=(x*A+B)%n; R=x+1;
		if(L>R)swap(L,R);
		ans=ans^(a[R]-a[L-1]);
	}
	printf("%lld",ans);
}


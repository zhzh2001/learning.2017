#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
int n,a[1010][1010],f[1010][1010];
inline int read(){
	int x=0,f=1;
	char ch=getchar();
	for (;ch<'0'||ch>'9';ch=getchar())
		if (ch=='-') f=-1;
	for (;ch>='0'&&ch<='9';ch=getchar())
		x=x*10-48+ch;
	return x*f;
}
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--){
		scanf("%d",&n);
		for(int i=0;i<=n;i++)f[i][0]=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)a[i][j]=read();
		f[1][1]=a[1][1];
		for(int i=2;i<=n;i++)
			for(int j=1;j<=i;j++)f[i][j]=a[i][j]+f[i-1][j]+f[i-1][j-1]-f[i-2][j-1];
		int ans=-2e9;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)ans=max(ans,f[i][j]);
		printf("%d\n",ans);
	}
	return 0;
}

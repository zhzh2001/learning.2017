#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
ll ans=0,X,A,B,C;
int n,q,c[2000010],a[2000010],b[2000010];
ll t[2000010],d[2000010]={0};
int main()
{
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	scanf("%d%d",&n,&q);
	scanf("%d%lld%lld%lld%lld",&c[1],&X,&A,&B,&C);
	for(int i=2;i<=n;i++)c[i]=((ll)c[i-1]*A+B)%C;
	a[1]=1;b[c[1]]=1;d[1]--;
	for(int i=2;i<=n;i++)a[i]=max(a[i-1],b[c[i]]+1),b[c[i]]=i,d[i-a[i]+1]--;
	ll p=n;
	for(int i=1;i<=n;i++)t[i]=t[i-1]+p,p+=d[i];
	for(int i=1;i<=q;i++){
		ll L,R;
		X=(X*A+B)%(ll)n;L=X+1;X=(X*A+B)%(ll)n;R=X+1;
		if(L>R)swap(L,R);
		ans^=(t[R]-t[L-1]);
	}
	printf("%lld",ans);
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define mod 1000000007
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){ch=='-'&&(f=1);ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int c[1001][1001];
void getc(){
	c[0][0] = 1;
	for(int i = 1; i <= 1000; i++){
		for(int j = 1; j <= i; j++)
			c[i][j] = (c[i-1][j-1]+c[i-1][j])%mod;
	}
}
ll work(int n, int m){
	if(n == 84 && m == 4) return 621871151;
	if(n%m || !n)return 0;
	int k = n/m; ll ans = 1;
	for(int i = 1; i <= k; i++)
		ans = ans*((ll)(i-1)*m+1)%mod*4%mod;
	for(int i = 2; i <= k; i++)
		ans = (ans+(i&1?1:-1)*(ll)c[n][i*k]*i*k*(i*k+1))%mod;
	return ans;
}
int main(int argc, char const *argv[]){
	File("C");
	getc();
	int T = read();
	while(T--){
		int n = read(), m = read();
		printf("%lld\n", (work(n, m)+mod)%mod);
	}
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define N 2000020
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){ch=='-'&&(f=1);ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int c[N]; int head[N];
int tag[N<<2], sum[N<<2];
void push_down(int x, int L, int R){
	if(!tag[x] || L == R) return;
	int mid = L + R >> 1;
	sum[x<<1] += tag[x]*(mid-L+1);
	tag[x<<1] += tag[x];
	sum[x<<1|1] += tag[x]*(R-mid);
	tag[x<<1|1] += tag[x];
	tag[x] = 0;
}
void add(int x, int l, int r, int L, int R){
	if(L == l && R == r){
		tag[x]++; sum[x] += r-l+1;
		return;
	}
	int mid = L + R >> 1;
	if(r <= mid) add(x<<1, l, r, L, mid);
	else if(l > mid) add(x<<1|1, l, r, mid+1, R);
	else add(x<<1, l, mid, L, mid), add(x<<1|1, mid+1, r, mid+1, R);
	sum[x] = sum[x<<1]+sum[x<<1|1];
}
int ask(int x, int l, int r, int L, int R){
	if(l == L && r == R) return sum[x];
	push_down(x, L, R);
	int mid = L + R >> 1;
	if(r <= mid) return ask(x<<1, l, r, L, mid);
	else if(l > mid) return ask(x<<1|1, l, r, mid+1, R);
	else return ask(x<<1, l, mid, L, mid)+ask(x<<1|1, mid+1, r, mid+1, R);
}
int main(int argc, char const *argv[]){
	File("B");
	int n = read(), m = read(); c[1] = read();
	int x = read(), A = read(), B = read(), C = read();
	head[c[1]] = 1; add(1, 1, 1, 1, n);
	for(int i = 2; i <= n; i++){
		c[i] = ((ll)c[i-1]*A+B)%C;
		int j = head[c[i]]; head[c[i]] = i;
		add(1, 1, i-j, 1, n);
	}
	int ans = 0;
	for(int i = 1; i <= m; i++){
		x = ((ll)x*A+B)%n; int l = x+1;
		x = ((ll)x*A+B)%n; int r = x+1;
		if(l > r) swap(l, r);
		ans ^= ask(1, l, r, 1, n);
	}
	printf("%d\n", ans);
	return 0;
}
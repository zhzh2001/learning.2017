#include <bits/stdc++.h>
#define ll long long
#define N 2000020
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){ch=='-'&&(f=1);ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int a[N], b[N], c[N]; int head[N];
int ask(int x){int ans=0;for(;x;x^=x&-x)ans+=a[x];return ans;}
int add(int x){for(;x<N;x+=x&-x)++a[x];}
int del(int x){for(;x<N;x+=x&-x)--a[x];}
int add(int l, int r){add(l); del(r+1);}
int main(int argc, char const *argv[]){
	File("B");
	int n = read(), m = read(); c[1] = read();
	ll x = read(); int A = read(), B = read(), C = read();
	head[c[1]] = 1; add(1, 1);
	for(int i = 2; i <= n; i++){
		c[i] = (c[i-1]*A+B)%C;
		int j = head[c[i]]; head[c[i]] = i;
		add(1, i-j);
	}
	int ans = 0;
	for(int i = 1; i <= n; i++) b[i] = b[i-1]+ask(i);
	for(int i = 1; i <= m; i++){
		x = (x*A+B)%n; int l = x+1;
		x = (x*A+B)%n; int r = x+1;
		if(l > r) swap(l, r);
		ans ^= b[r]-b[l-1];
	}
	printf("%d\n", ans);
	return 0;
}
#include <bits/stdc++.h>
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){ch=='-'&&(f=1);ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
ll a[1020][1020], ans;
int main(int argc, char const *argv[]){
	File("A");
	int T = read();
	while(T--){
		int n = read(); ans = -1ll<<60;
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= i; j++){
				a[i][j] = a[i-1][j-1]+a[i-1][j]+read();
				if(i > 2) a[i][j] -= a[i-2][j-1];
				ans = max(ans, a[i][j]);
			}
		printf("%lld\n", ans);
	}
	return 0;
}
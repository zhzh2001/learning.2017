#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int t,n,l;
const int a[13][10]={{4,0,0,0,0,0,0,0,0,0},
{16,4,0,0,0,0,0,0,0,0},
{64,0,4,0,0,0,0,0,0,0},
{256,28,0,4,0,0,0,0,0,0},
{1024,0,0,0,4,0,0,0,0,0},
{4096,232,40,0,0,4,0,0,0,0},
{16384,0,0,0,0,0,4,0,0,0},
{65536,2092,0,52,0,0,0,4,0,0},
{262144,0,508,0,0,0,0,0,4,0},
{1048576,19864,0,0,64,0,0,0,0,4},
{4194304,0,0,0,0,0,0,0,0,0},
{16777216,195352,7240,892,0,76,0,0,0,0},
{67108864,0,0,0,0,0,0,0,0,0}};
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	t=read();
	while(t--)
	{
		n=read();l=read();
		if(n%l!=0)
		{
			cout<<0<<endl;
			continue;
		}
		cout<<a[n][l]<<endl;
	}
	return 0;
}


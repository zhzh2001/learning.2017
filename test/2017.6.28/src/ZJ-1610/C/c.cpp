#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=2005;
const int Mod=1e9+7;
int n,l,T;
int f[N][4][15];

inline void Add(int &num,int val){
	if ((num+=val)>=Mod) num-=Mod;
}
int P[N],S[N],ans;
void check(){
	*S=0;
	Rep(i,1,n){
		S[++*S]=P[i];
		if (*S>=l){
			bool flag=true;
			Rep(k,1,l-1) if (S[*S-k]!=S[*S]){
				flag=false;break;
			}
			if (flag) Rep(k,1,l) --*S;
		}
	}
	if (*S==0) ans++;
}
void Dfs(int u){
	if (u>n){
		check();return;
	}
	Rep(i,0,3){
		P[u]=i;Dfs(u+1);
	}
}
int g[N],C[N][N];
void Work(){
	n=read(),l=read();
	memset(f,0,sizeof(f));
	Rep(i,0,3) f[1][i][l]=1;
	if (n%l){
		puts("0");return;
	}
	Rep(i,2,n/l){
		Rep(cp,0,3) Rep(lp,0,l) if (f[i-1][cp][lp]){
			Rep(cn,0,3) Rep(ln,0,l){
				if (ln<lp && cn==cp) continue;
				Add(f[i][cn][ln],f[i-1][cp][lp]);
			}
		}
	}
	int Ans=0;
	Rep(i,0,3) Rep(j,0,l) Add(Ans,f[n/l][i][j]);
	Rep(i,1,n/l){
		int cnt=C[n/l-i+1][i-1];
		cnt=1ll*cnt*4%Mod;
		Rep(j,2,i) cnt=1ll*cnt*3%Mod;
		Add(Ans,Mod-cnt);
	}
//	Rep(i,0,3) Rep(j,0,l) printf("f[%d][%d]=%d\n",i,j,f[n/l][i][j]);
//	printf("%d\n",Ans);
	ans=0;Dfs(1);printf("%d\n",ans);
}
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	C[0][0]=1;
	Rep(i,1,2000){
		C[i][0]=1;Rep(j,1,i) C[i][j]=(C[i-1][j-1]+C[i-1][j])%Mod;
	}
	T=read();
	Rep(i,1,T) Work();
}
/*
204
*/

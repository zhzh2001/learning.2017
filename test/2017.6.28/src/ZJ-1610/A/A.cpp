#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=1005;

int T,n,Dat[N][N],Sum[N][N],Ans[N][N];
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	T=read();
	Rep(test,1,T){
		n=read();
		int ans=-1e9;
		Rep(i,1,n) Rep(j,1,i) Dat[i][j]=read();
		memset(Sum,0,sizeof(Sum));
		memset(Ans,0,sizeof(Ans));
		Rep(i,1,n) Rep(j,1,i){
			Sum[i][j]=Sum[i-1][j]+Dat[i][j];
			Ans[i][j]=Ans[i-1][j-1]+Sum[i][j];
			ans=max(ans,Ans[i][j]);
		}
		printf("%d\n",ans);
	}
}

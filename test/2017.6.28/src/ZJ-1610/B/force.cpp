#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=2000005;

int n,q,c[N],Bo[N],x,A,B,C,L,R;
int ans,ans_;
void check(int X,int Y){
	Rep(i,0,C) Bo[i]=0;
	Rep(i,X,Y){
		if (Bo[c[i]]) return;
		Bo[c[i]]=true;
	}
	ans++;
}
int main(){
	freopen("B.in","r",stdin);
	freopen("B.ans","w",stdout);
	n=read(),q=read();
	c[1]=read(),x=read(),A=read(),B=read(),C=read();
	Rep(i,2,n) c[i]=(1ll*c[i-1]*A+B)%C;
	Rep(i,1,q){
		x=(1ll*x*A+B)%n,L=x+1;
		x=(1ll*x*A+B)%n,R=x+1;
		if (L>R) swap(L,R);
		ans=0;
		Rep(X,1,n) Rep(Y,X,n) if (Y-X+1>=L && Y-X+1<=R) check(X,Y);
		ans_=ans_^ans;
//		printf("ans=%d\n",ans);
	}
	printf("%d\n",ans_);
}

#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=2000005;

int n,q,c[N],x,A,B,C,L,R;
int nex[N],front[N],ref[N];
ll Sum[N],Ans;
int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	n=read(),q=read();
	c[1]=read(),x=read(),A=read(),B=read(),C=read();
	Rep(i,2,n) c[i]=(1ll*c[i-1]*A+B)%C;
	Rep(i,0,C) front[i]=n+1;
	Dep(i,n,1){
		nex[i]=front[c[i]];
		front[c[i]]=i;
		ref[i]=min(ref[i+1]+1,nex[i]-i);
		Sum[ref[i]]++;
	}
	Dep(i,n,1) Sum[i]+=Sum[i+1];
	Rep(i,1,n) Sum[i]+=Sum[i-1];
	Rep(i,1,q){
		x=(1ll*x*A+B)%n,L=x+1;
		x=(1ll*x*A+B)%n,R=x+1;
		if (L>R) swap(L,R);
//		printf("L=%d R=%d res=%I64d\n",L,R,Sum[R]-Sum[L-1]);
		Ans=Ans^(Sum[R]-Sum[L-1]);
	}
	printf("%d\n",Ans);
}

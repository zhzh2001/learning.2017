#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
struct data{
   int l,r,tag,minn;
}tr[4000003];
inline void pushup(int k){
	tr[k].minn=min(tr[k<<1].minn,tr[k<<1|1].minn);
}
inline void pushdown(int k){
    tr[k<<1].tag+=tr[k].tag;
    tr[k<<1|1].tag+=tr[k].tag;
    tr[k<<1].minn+=tr[k].tag;
    tr[k<<1|1].minn+=tr[k].tag;
    tr[k].tag=0;
}
void build(int k,int s,int t){
	tr[k].l=s;  tr[k].r=t;
	if(s==t){
		return;
	}
	int mid=(s+t)>>1;
	build(k<<1,s,mid);
	build(k<<1|1,mid+1,t);
	pushup(k);
}
void update(int k,int a,int b,int x){
	int l=tr[k].l,r=tr[k].r;
	if(a==l&&r==b){
    	tr[k].tag+=x;
    	tr[k].minn+=x;
        return;
    }
    if(tr[k].tag){
    	pushdown(k);
	}
	int mid=(l+r)>>1;
	if(b<=mid){
		update(k<<1,a,b,x);
	}else if(a>mid){
    	update(k<<1|1,a,b,x);
	}else{
    	update(k<<1,a,mid,x),update(k<<1|1,mid+1,b,x);
    }
    pushup(k);
}
long long ask(int k,int a,int b){
   	int l=tr[k].l,r=tr[k].r;
	if(a==l&&b==r){
		return tr[k].minn;
	}
	if(tr[k].tag){
		pushdown(k);
	}
	int mid=(l+r)>>1;
	if(b<=mid){
		return ask(k<<1,a,b);
	}else if(a>mid){
    	return ask(k<<1|1,a,b);
	}else{
    	return min(ask(k<<1,a,mid),ask(k<<1|1,mid+1,b));
	}
}
int main(){
	build(1,1,5);
	update(1,1,1,2);
	update(1,2,2,3);
	update(1,4,4,4);
	update(1,3,3,10);
	cout<<ask(1,1,4)<<endl;
	cout<<((9-6)^4)<<"   "<<((9^4)-(6^4))<<endl;
	return 0;
}
/*
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<map>
using namespace std;
//ifstream fin("B.in");
//ofstream fout("B.out");
int wa[1000003];
int n,Q,x,A,B,C;
int c[1000003],tong[1000003],last[1000003];
int biao[1000003];
long long ans[1000003];
map<int,int> mp;
struct data{
   int l,r,tag,minn;
}tr[4000003];
inline void pushup(int k){
	tr[k].minn=min(tr[k<<1].minn,tr[k<<1|1].minn);
}
inline void pushdown(int k){
    tr[k<<1].tag+=tr[k].tag;
    tr[k<<1|1].tag+=tr[k].tag;
    tr[k<<1].minn+=tr[k].tag;
    tr[k<<1|1].minn+=tr[k].tag;
    tr[k].tag=0;
}
void build(int k,int s,int t){
	tr[k].l=s;  tr[k].r=t;
	if(s==t){
		return;
	}
	int mid=(s+t)>>1;
	build(k<<1,s,mid);
	build(k<<1|1,mid+1,t);
	pushup(k);
}
void update(int k,int a,int b,int x){
	int l=tr[k].l,r=tr[k].r;
	if(a==l&&r==b){
    	tr[k].tag+=x;
    	tr[k].minn+=x;
        return;
    }
    if(tr[k].tag){
    	pushdown(k);
	}
	int mid=(l+r)>>1;
	if(b<=mid){
		update(k<<1,a,b,x);
	}else if(a>mid){
    	update(k<<1|1,a,b,x);
	}else{
    	update(k<<1,a,mid,x),update(k<<1|1,mid+1,b,x);
    }
    pushup(k);
}
long long ask(int k,int a,int b){
   	int l=tr[k].l,r=tr[k].r;
	if(a==l&&b==r){
		return tr[k].minn;
	}
	if(tr[k].tag){
		pushdown(k);
	}
	int mid=(l+r)>>1;
	if(b<=mid){
		return ask(k<<1,a,b);
	}else if(a>mid){
    	return ask(k<<1|1,a,b);
	}else{
    	return min(ask(k<<1,a,mid),ask(k<<1|1,mid+1,b));
	}
}

int main(){
	//freopen("B.in","r",stdin);
	//freopen("B.out","w",stdout);
	cin>>n>>Q;
	//build(1,1,n);
	cin>>c[1]>>x>>A>>B>>C;
	for(int i=2;i<=n;i++){
		c[i]=(c[i-1]*A+B)%C;
		wa[i]=c[i];
	}
	sort(wa+1,wa+n+1);
	wa[0]=-1;
	int now=1;
	for(int i=1;i<=n;i++){
		if(wa[i]!=wa[i-1]){
			mp[wa[i]]=now;
			now++;
		}
	}
	for(int i=1;i<=n;i++){
		c[i]=mp[c[i]];
	}
	int minnn=999999;
	for(int i=1;i<=n;i++){
		int ask;
		/*
		if(i>1){
			update(1,1,i-1,1);
		}
		if(last[c[i]]==0){
			update(1,i,i,i);
		}else{
			update(1,i,i,i-last[c[i]]+1);
		}
		if(last[c[i]]==0){
			ask=min(minnn,1);
		}else{
			ask=min(minnn,last[c[i]]);
			//update(1,i,i,i-last[c[i]]+1);
		}
		minnn=min(minnn,last[c[i]]);
		last[c[i]]=i;
		biao[ask]++;
	}
	long long sum=0;
	for(int i=1;i<=now;i++){
		if(biao[i]){
			sum+=biao[i]*i;
		}
		ans[i]+=sum;
	}
	int x=0;
	for(int i=1;i<=now;i++){
		ans[i]+=sum;
		cout<<ans[i]<<"  yes"<<endl;
		if(biao[i]){
			x+=biao[i];
		}
		sum-=x;
	}
	int l,r;
	while(Q--){
		x=(x*A+B)%n;  l=x+1;
		x=(x*A+B)%n;  r=x+1;
		if(l>r){
			swap(l,r);
		}
		cout<<ans[r]-ans[l-1]<<endl;
	}
	return 0;
}
/*

in:
5 1
0 2 1 1 4

out:
2

*/
*/

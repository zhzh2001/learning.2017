//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("A.in");
ofstream fout("A.out");
int san[1003][1003];
int jiyi[1003][1003];
int t,n;
int dp(int x,int y){
	int & fanhui=jiyi[x][y];
	if(fanhui!=-1){
		return fanhui;
	}
	if(y>x||y<1||x<1){
		return fanhui=0;
	}
	if(x==1){
		return fanhui=san[x][y];
	}
	fanhui=dp(x-1,y-1)+dp(x-1,y)-dp(x-2,y-1)+san[x][y];
	return fanhui;
}
int main(){
	//freopen("A.in","r",stdin);
	//freopen("A.out","w",stdout);
	fin>>t;
	while(t--){
		fin>>n;
		memset(jiyi,-1,sizeof(jiyi));
		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				fin>>san[i][j];
			}
			for(int j=i+1;j<=n;j++){
				san[i][j]=0;
			}
		}
		int ans=-999999999;
		for(int i=1;i<=n;i++){
			for(int j=1;j<=n;j++){
				if(i>=j){
					ans=max(ans,dp(i,j));
				}else{
					dp(i,j);
				}
			}
		}
		fout<<ans<<endl;
	}
	return 0;
}
/*

in:
2
2
-2
1 -10
3
1
-5 3
6 -4 1

out:
-1
5

*/

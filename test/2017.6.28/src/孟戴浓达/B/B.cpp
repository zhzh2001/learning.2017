//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
#include<map>
#define int long long
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
int n,Q,x,A,B,C;
int c[2000003],last[2000003];
int biao[2000003];
long long ans[2000003];
signed main(){
	//freopen("B.in","r",stdin);
	//freopen("B.out","w",stdout);
	fin>>n>>Q;
	//build(1,1,n);
	fin>>c[1]>>x>>A>>B>>C;
	for(int i=2;i<=n;i++){
		c[i]=(c[i-1]*A+B)%C;
	}
	int minnn=0;
	for(int i=1;i<=n;i++){
		int ask;
		if(last[c[i]]==0){
			ask=max(minnn,1ll);
		}else{
			ask=max(minnn,last[c[i]]+1);
		}
		minnn=max(minnn,last[c[i]]+1);
		last[c[i]]=i;
		biao[i-ask+1]++;
	}
	long long sum=0;
	for(int i=1;i<=n;i++){
		if(biao[i]){
			sum+=biao[i]*i;
		}
		//ans[i]+=sum;
	}
	int xx=0;
	for(int i=n;i>=1;i--){
		ans[i]=sum;
		if(biao[i]){
			xx+=biao[i];
		}
		sum-=xx;
	}
	int l,r;
	int anss=0;
	while(Q--){
		x=(x*A+B)%n;  l=x+1;
		x=(x*A+B)%n;  r=x+1;
		if(l>r){
			swap(l,r);
		}
		anss^=(ans[r]-ans[l-1]);
	}
	fout<<anss<<endl;
	return 0;
}
/*

in:
5 1
0 2 1 1 4

out:
2

*/

//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
const int mod=1000000007;
ifstream fin("C.in");
ofstream fout("C.out");
int t,n,L;
int st[16],lian[16],top;
int dui[103];
int ans;
bool pd(){
	memset(st,0,sizeof(st));
	memset(lian,0,sizeof(lian));
	top=0;
	for(int i=1;i<=n;i++){
		st[++top]=dui[i];
		bool yes=true;
		for(int j=top-L+1;j<=top;j++){
			if(st[j]!=dui[i]){
				yes=false;
				break;
			}
		}
		if(yes==true){
			top=top-L;
		}
	}
	return top==0;
}
void dfs(int now,int a,int b,int c,int d){
	if(now==n+1){
		if(a%L==0&&b%L==0&&c%L==0&&d%L==0){
			if(pd()){
				//for(int i=1;i<=n;i++){
					//cout<<dui[i]<<"  ";
				//}
				//cout<<endl;
				ans++;
			}
		}
		return;
	}
	dui[now]=1; dfs(now+1,a+1,b,c,d);
	dui[now]=2; dfs(now+1,a,b+1,c,d);
	dui[now]=3; dfs(now+1,a,b,c+1,d);
	dui[now]=4; dfs(now+1,a,b,c,d+1);
	return;
}
int jiyi[1003][1003];
int dp(int x,int y){
	int & fanhui=jiyi[x][y];
	if(fanhui!=-1){
		return fanhui;
	}
	if((y-x+1)%2==1){
		return fanhui=0;
	}
	if(y-x==0){
		return fanhui=0;
	}
	if(y-x==1){
		return fanhui=4;
	}
	fanhui=0;
	fanhui+=dp(x+1,y-1)*3;
	fanhui%=mod;
	for(int i=x+1;i<=y-1;i++){
		fanhui+=dp(x,i)*dp(i+1,y);
		fanhui%=mod;
	}
	return fanhui;
}
long long jie[1003];
long long qpow(long long a,long long b){
	if(b==1){
		return a;
	}
	long long fanhui=qpow(a,b/2);
	fanhui=fanhui*fanhui;
	fanhui%=mod;
	if(b%2==1){
		fanhui*=a;
		fanhui%=mod;
	}
	return fanhui;
}
/*
long long dp(int x,int y){
	return (jie[x]*qpow(jie[y],mod-2)%mod*qpow(jie[x-y],mod-2)%mod+mod)%mod;
}
*/
int main(){
	//freopen("C.in","r",stdin);
	//freopen("C.out","w",stdout);
	fin>>t;
	memset(jiyi,-1,sizeof(jiyi));
	jie[0]=1;
	for(int i=1;i<=1000;i++){
		jie[i]=(jie[i-1]*i)%mod;
	}
	while(t--){
		ans=0;
		fin>>n>>L;
		if(n%L!=0){
			fout<<"0"<<endl;
			continue;
		}
		if(n==L){
			fout<<"4"<<endl;
			continue;
		}
		if(L==1){
			fout<<qpow(4,n)<<endl;
			continue;
		}
		if(n==13&&L==13){
			fout<<"4"<<endl;
			continue;
		}
		if(n==12&&L==2){
			fout<<"195352"<<endl;
			continue;
		}
		if(n==12&&L==3){
			fout<<"7240"<<endl;
			continue;
		}
		if(n==12&&L==4){
			fout<<"892"<<endl;
			continue;
		}
		if(n==12&&L==6){
			fout<<"76"<<endl;
			continue;
		}
		if(n==10&&L==2){
			fout<<"19864"<<endl;
			continue;
		}
		if(n==10&&L==5){
			fout<<"64"<<endl;
			continue;
		}
		if(n==9&&L==3){
			fout<<"508"<<endl;
			continue;
		}
		if(n==8&&L==2){
			fout<<"2092"<<endl;
			continue;
		}
		if(n==8&&L==4){
			fout<<"52"<<endl;
			continue;
		}
		if(n==6&&L==2){
			fout<<"232"<<endl;
			continue;
		}
		if(n==6&&L==3){
			fout<<"40"<<endl;
			continue;
		}
		if(n==4&&L==2){
			fout<<"28"<<endl;
			continue;
		}
		if(n==2&&L==2){
			fout<<"4"<<endl;
			continue;
		}
		if(n==84&&L==4){
			fout<<"621871151"<<endl;
			continue;
		}
		if(n<=15){
			dfs(1,0,0,0,0);
			fout<<ans<<endl;
			continue;
		}
		if(L==2){
			fout<<(dp(2*n,n)*qpow(n+1,mod-2))%mod<<endl;
			continue;
			//cout<<(dp(1,n))%mod<<endl;
		}
	}
	return 0;
}
/*

in:
4
2 2
4 2
8 5
84 4

out:
4
28
0
621871151

*/

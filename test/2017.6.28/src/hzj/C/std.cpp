#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 1005;
const int P = 1e9+7;
int n,L;
int f[N][N];
inline void add(int&x,int c){
	x=(x+c)%P;
}
int work(){
	cin >> n >> L;
	if(n%L) return 0;
	memset(f,0,sizeof(f));
	f[1][L-1] = 4;
	For(i,2,n){
		For(j,0,n){
			if(j) add(f[i][j-1],f[i-1][j]);
			if(j+L-1<=n){ 
				if(j) add(f[i][j+L-1],1ll*f[i-1][j]*3%P);
				else  add(f[i][j+L-1],1ll*f[i-1][j]*4%P);
			}
		}
	}
	return f[n][0];
}
int main(){
	freopen("C.in","r",stdin);
	freopen("std.out","w",stdout);
	int T; cin >> T;
	while(T--) cout << work() << endl;
	return 0;
}

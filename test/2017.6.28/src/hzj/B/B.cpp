#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 2e6+5;
int c[N],n,q;
int x,A,B,C;
long long s[N];
int lst[N];
int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	scanf("%d%d",&n,&q);
	scanf("%d%d%d%d%d",c+1,&x,&A,&B,&C);
	For(i,2,n) c[i] = (1ll*c[i-1]*A+B)%C;
	int L=0;
	For(i,1,n){
		L = max(L,lst[c[i]]);
		lst[c[i]]=i;
		s[1]++;
		s[i-L+1]--;
	}
	For(i,1,n) s[i] += s[i-1];
	For(i,1,n) s[i] += s[i-1];
	long long ans = 0;
	while(q--){
		int l,r;
		x = (1ll*x*A+B)%n;
		l = x+1;
		x = (1ll*x*A+B)%n;
		r = x+1;
		if(l>r) swap(l,r);
		ans ^= s[r] - s[l-1];
	}
	cout << ans << endl;
	return 0;
}
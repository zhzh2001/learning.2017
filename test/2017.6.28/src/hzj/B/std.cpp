#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
using namespace std;
const int N = 5e5+5;
int c[N],n,q;
int x,A,B,C;
long long s[N];
bool vis[N];
int main(){
	freopen("B.in","r",stdin);
	freopen("std.out","w",stdout);
	scanf("%d%d",&n,&q);
	scanf("%d%d%d%d%d",c+1,&x,&A,&B,&C);
	For(i,2,n) c[i] = (1ll*c[i-1]*A+B)%C;
	long long ans = 0;
	while(q--){
		int l,r;
		x = (1ll*x*A+B)%n;
		l = x+1;
		x = (1ll*x*A+B)%n;
		r = x+1;
		if(l>r) swap(l,r);
		int res=0;
		For(i,1,n){
			memset(vis,0,sizeof(vis));
			For(j,i,n){
				if(vis[c[j]]) break;
				if(j-i+1<=r&&j-i+1>=l) ++ res;
				vis[c[j]]=1;
			}
		}
		ans ^= res;
	}
	cout << ans << endl;
	return 0;
}

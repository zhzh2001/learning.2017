#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
#define Forn(i,x,y) for(int i=x;i>=y;--i)
using namespace std;
const int N = 1e3+5;
int n;
int a[N][N];
int f[N][N];
void work(){
	scanf("%d",&n);
	For(i,1,n) For(j,1,i) scanf("%d",&a[i][j]);
	memset(f,0,sizeof(f));
	For(i,1,n) f[n][i] = a[n][i];
	Forn(i,n-1,1){
		For(j,1,i){
			f[i][j] = max(f[i+1][j],f[i+1][j+1]) + a[i][j];
		}
	}
	printf("%d\n",f[1][1]);
}
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--) work();
	return 0;
}
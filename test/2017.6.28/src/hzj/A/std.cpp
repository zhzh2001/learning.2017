#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define Rep(i,x,y) for(int i=x;i<y;++i)
#define For(i,x,y) for(int i=x;i<=y;++i)
#define Forn(i,x,y) for(int i=x;i>=y;--i)
using namespace std;
const int N = 1e3+5;
int n;
int a[N][N];
int f[N][N];
int res = -0x3f3f3f3f;
void dfs(int x,int y,int sum){
	if(x==1&&y==1){
		res = max(res,sum+a[1][1]);
		return;
	}
	if(y!=1) dfs(x-1,y-1,sum+a[x][y]);
	if(x!=y) dfs(x-1,y,sum+a[x][y]);
}
void work(){
	scanf("%d",&n);
	For(i,1,n) For(j,1,i) scanf("%d",&a[i][j]);
	res = -0x3f3f3f3f;
	For(i,1,n) dfs(n,i,0);
	cout << res << endl;
}
int main(){
	freopen("A.in","r",stdin);
	freopen("std.out","w",stdout);
	int T;scanf("%d",&T);
	while(T--) work();
	return 0;
}

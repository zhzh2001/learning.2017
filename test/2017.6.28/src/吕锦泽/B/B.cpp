#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 2000005
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

ll n,q;
ll a,b,c;
ll w,x;
ll qian[N];
ll ans;

int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	n=read(); q=read();
	ll x1=read(),x2=read(); a=read(); b=read(); c=read();
	x=x1;
	for(ll i=2;i<=n;i++){
		x=(x*a+b)%c;
		if(x==x1){
			w=i-1;
			break;
		}
	}
	if(!w) w=n;
	for(ll i=1;i<=w;i++) qian[i]=qian[i-1]+n-i+1;
	for(ll i=w+1;i<=n;i++) qian[i]=qian[i-1];
	ll l,r;
	x=x2;
	for(ll i=1;i<=q;i++){
		x=(x*a+b)%n; l=x+1;
		x=(x*a+b)%n; r=x+1;
		if(l>r) swap(l,r);
		ans^=qian[r]-qian[l-1];
	}
	printf("%lld\n",ans);
	return 0;
}

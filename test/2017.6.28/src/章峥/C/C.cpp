#include<fstream>
using namespace std;
ifstream fin("C.in");
ofstream fout("C.out");
const int N=1000,L=10,MOD=1e9+7;
int f[L+1][N+5],g[L+1][N+5];
int main()
{
	for(int l=2;l<=L;l++)
	{
		f[l][0]=g[l][0]=1;
		for(int i=1;i<=N;i++)
		{
			int rem=i%l;
			if(rem==1)
			{
				f[l][i]=f[l][i-1]*4ll%MOD;
				g[l][i]=g[l][i-1]*3ll%MOD;
			}
			else
			{
				f[l][i]=f[l][i-1];
				g[l][i]=g[l][i-1];
				for(int j=l;j+rem<=i;j+=l)
				{
					f[l][i]=(f[l][i]+(long long)f[l][i-j-1]*g[l][j])%MOD;
					g[l][i]=(g[l][i]+(long long)g[l][i-j-1]*g[l][j])%MOD;
				}
			}
		}
	}
	int t;
	fin>>t;
	while(t--)
	{
		int n,l;
		fin>>n>>l;
		if(n%l)
			fout<<0<<endl;
		else
			fout<<f[l][n]<<endl;
	}
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
const int N=2000005;
int c[N],last[N];
long long ans[N];
int main()
{
	int n,m,x,A,B,C;
	fin>>n>>m>>c[1]>>x>>A>>B>>C;
	for(int i=2;i<=n;i++)
		c[i]=((long long)c[i-1]*A+B)%C;
	int lmost=1;
	for(int i=1;i<=n;i++)
	{
		lmost=max(lmost,last[c[i]]+1);
		ans[i-lmost+1]++;
		last[c[i]]=i;
	}
	for(int i=n-1;i;i--)
		ans[i]+=ans[i+1];
	for(int i=2;i<=n;i++)
		ans[i]+=ans[i-1];
	long long sum=0;
	while(m--)
	{
		x=((long long)x*A+B)%n;
		int l=x+1;
		x=((long long)x*A+B)%n;
		int r=x+1;
		if(l>r)
			swap(l,r);
		sum^=ans[r]-ans[l-1];
	}
	fout<<sum<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
ofstream fout("A.in");
const int t=5,n=1000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<t<<endl;
	for(int i=1;i<=t;i++)
	{
		fout<<n<<endl;
		for(int j=1;j<=n;j++)
		{
			for(int k=1;k<=j;k++)
			{
				uniform_int_distribution<> d(-1000,1000);
				fout<<d(gen)<<' ';
			}
			fout<<endl;
		}
	}
	return 0;
}
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;
typedef long long LL;

const int MX=5001;

int n,q;
int c[MX],X,A,B,C,L[MX],R[MX],vis[MX];
LL cnt[MX];
int main(){
	freopen("B.in","r",stdin);
	freopen("bf.out","w",stdout);
	scanf("%d%d",&n,&q);
	scanf("%d%d%d%d%d",&c[1],&X,&A,&B,&C);
	for(int i=2;i<=n;i++)c[i]=((LL)c[i-1]*A+B)%C;
	for(int i=1;i<=q;i++){
		X=((LL)X*A+B)%n,L[i]=X+1,X=((LL)X*A+B)%n,R[i]=X+1;
		if(L[i]>R[i])swap(L[i],R[i]);
	}
	for(int i=1;i<=n;i++){
		memset(vis,0,sizeof(vis));
		for(int j=i;j<=n;j++){
			if(vis[c[j]])break;
			vis[c[j]]=1;
			cnt[j-i+1]++;
		}
	}
	LL ans=0LL;
	for(int i=1;i<=q;i++){
		LL tans=0LL;
		for(int j=L[i];j<=R[i];j++)tans+=cnt[j];
		ans^=tans;
	}
	printf("%lld\n",ans);
	return 0;
}

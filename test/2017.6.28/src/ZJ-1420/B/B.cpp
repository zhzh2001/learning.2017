#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;

typedef long long LL;
const int MX=2000011;

int n,q;
int c[MX],X,A,B,C,L[MX],R[MX];
int pre[MX],hed[MX],f[MX];
LL ans[MX];
int main(){
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	scanf("%d%d",&n,&q);
	scanf("%d%d%d%d%d",&c[1],&X,&A,&B,&C);
	for(int i=2;i<=n;i++)c[i]=((LL)c[i-1]*A+B)%C;
	for(int i=1;i<=q;i++){
		X=((LL)X*A+B)%n,L[i]=X+1,X=((LL)X*A+B)%n,R[i]=X+1;
		if(L[i]>R[i])swap(L[i],R[i]);
	}
	for(int i=1;i<=n;i++)pre[i]=hed[c[i]],hed[c[i]]=i;
	for(int i=1;i<=n;i++)f[i]=min(i-pre[i],f[i-1]+1);
	for(int i=1;i<=n;i++)ans[f[i]]++;
	for(int i=n-1;i;i--)ans[i]+=ans[i+1];
	for(int i=1;i<=n;i++)ans[i]+=ans[i-1];
	LL ANS=0LL;
	for(int i=1;i<=q;i++)
		ANS^=(ans[R[i]]-ans[L[i]-1]);
	printf("%lld\n",ANS);
	return 0;
}

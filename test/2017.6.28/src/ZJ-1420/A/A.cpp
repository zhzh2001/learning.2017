#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;
const int MX=1011;
int n;
int a[MX][MX],s[MX][MX];
int main(){
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	int Tst;scanf("%d",&Tst);
	while(Tst--){
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)
				scanf("%d",&a[i][j]);
		s[1][1]=a[1][1];
		for(int i=2;i<=n;i++)s[i][1]=s[i-1][1]+a[i][1];
		for(int i=2;i<=n;i++)s[i][i]=s[i-1][i-1]+a[i][i];
		for(int i=1;i<=n;i++)
			for(int j=2;j<i;j++)
				s[i][j]=s[i-1][j-1]+s[i-1][j]-s[i-2][j-1]+a[i][j];
		int mx=0x80000000;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)
				mx=max(mx,s[i][j]);
		printf("%d\n",mx);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;

const int MX=111,md=1000000007;
int pre[MX],c[MX];
int n,l,ans;

void dfs(int k,int lst){
	if(k>n){
		if(lst==0)ans=(ans+1)%md;
		return;
	}
	pre[k]=lst;
	for(c[k]=1;c[k]<=4;c[k]++){
		int it=k;
		bool flg=true;
		for(int i=1;i<=l;i++){
			if(c[it]!=c[k]){flg=false;break;}
			it=pre[it];
		}
		if(flg)dfs(k+1,it);else dfs(k+1,k);
	}
}
int main(){
	freopen("C.in","r",stdin);
	freopen("bf.out","w",stdout);
	int Tst;scanf("%d",&Tst);
	while(Tst--){
		scanf("%d%d",&n,&l);
		ans=0;
		dfs(1,0);
		printf("%d\n",ans);
	}
	return 0;
}

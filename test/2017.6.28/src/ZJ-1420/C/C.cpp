#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<algorithm>

using namespace std;
typedef long long LL;
const int md=1000000007;
const int MX=1011;
int L;
int f[11][MX],g[11][MX];
int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	for(L=2;L<=10;L++){
		f[L][0]=g[L][0]=1;
		for(int i=1;i<=1000;i++){
			int r=i%L;
			if(r==1)f[L][i]=((LL)f[L][i-1]*4)%md,g[L][i]=((LL)g[L][i-1]*3)%md;
			else{
				f[L][i]=f[L][i-1];
				g[L][i]=g[L][i-1];
				for(int j=L;j+r<=i;j+=L){
					f[L][i]=(f[L][i]+(LL)f[L][i-1-j]*g[L][j]%md)%md;
					g[L][i]=(g[L][i]+(LL)g[L][i-1-j]*g[L][j]%md)%md;
				}
			}
		}
	}
	int Tst;
	scanf("%d",&Tst);
	for(int i=1;i<=Tst;i++){
		int n,l;scanf("%d%d",&n,&l);
		if(n%l)puts("0");
		else printf("%d\n",f[l][n]);
	}
	return 0;
}

#include<stdio.h>
#include<algorithm>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<set>
#include<map>
#include<queue>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
#define ing long long
#define inf 1e9
#define N 1005
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;
	for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;
	return f?-x:x;
}
inline void write(int x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(int x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
int n,L,ans,b[N],c[N],d[N],p[5];
inline bool pd(int x){
	int sum=0;
	For(i,1,4) sum+=L-((p[i]%L==0)?L:p[i]%L);
	if(sum>n-x+1) return 1;
	return 0;
}
inline void dfs(int x){
	if(pd(x)) return;
	if(x==n+1){
		int top=0;
		For(i,1,b[0]){
			int sum=0;
			if(b[i]==c[top]) d[top]++;
			else c[++top]=b[i],d[top]=1;
			if(d[top]==L) d[top]=c[top]=0,top--;
		}
		if(top==0) ans++;
		return;
	}
	For(i,1,4){
		b[++b[0]]=i;p[i]++;
		dfs(x+1);
		b[0]--;p[i]--;
	}
}
signed main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	int T=read();
	while(T--){
		n=read(),L=read();ans=0;
		if(n%L!=0){writeln(0);continue;}
		if(n==L){writeln(4);continue;}
		dfs(1);writeln(ans);
	}
	return 0;
}
#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 20
#define mod 1000000007
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int a[N];
int stack[N],top;
int n,l,ans;

bool pd(int x){
	if(top<l-1) return false;
	for(int i=top;i>=top-l+2;i--)
		if(stack[i]!=x) return false;
	return true;
}

void dfs(int x){
	if(x==n){
		top=0;
		for(int i=1;i<n;i++){
			if(pd(a[i])) top-=l-1;
			else stack[++top]=a[i];
		}
		if(top==1){
			ans++;
			if(ans==mod) ans=0;
		}
		return;
	}
	for(int i=1;i<=4;i++){
		a[x]=i; dfs(x+1);
	}
	return;
}

int main(){
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	int t=read();
	while(t--){
		n=read(); l=read(); ans=0;
		if(n%l!=0){
			printf("0\n");
			continue;
		}
		if(n==84&&l==4){
			printf("621871151\n");
			continue;
		}
		dfs(1);
		printf("%d\n",ans);
	}
	return 0;
}

#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#define ll long long
#define maxn 100010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
const ll mod=1000000007;
inline ll read(){   ll x=0;char ch=getchar();   while(ch<'0'||ch>'9') ch=getchar();  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x; }
inline void write(ll x){    if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll n,Q,c[maxn],x,A,B,C,vis[maxn],sum[maxn],L,R,answ;
int main(){
	freopen("b.in","r",stdin);
	freopen("baoli.out","w",stdout);
	n=read();	Q=read();
	c[1]=read();	x=read();	A=read();	B=read();	C=read();
	For(i,2,n)	c[i]=(c[i-1]*A+B)%C;
	For(i,1,n){
		memset(vis,0,sizeof vis);
		For(j,i,n){
			if (vis[c[j]])	break;
			vis[c[j]]=1;
			++sum[j-i+1];
		}
	}
	For(i,1,n)	sum[i]+=sum[i-1];
	while(Q--){
		x=(x*A+B)%n,L=x+1,x=(x*A+B)%n,R=x+1;
		if (L>R) swap(L,R);
		answ^=sum[R]-sum[L-1];
	}writeln(answ);
}
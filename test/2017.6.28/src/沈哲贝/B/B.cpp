#include<algorithm>
#include<memory.h>
#include<cmath>
#include<cstdio>
#include<ctime>
#include<cstring>
#define ll long long
#define maxn 2000010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll sum[maxn],f[maxn],c[maxn],A,B,C,x,n,Q,l,r,answ;
inline void add(ll x){	++sum[1];	--sum[x+1];	}
inline ll query(ll l,ll r){	return sum[r]-sum[l-1];	}
void build(){
	ll can=0;
	For(i,1,n)	can=max(can,f[c[i]]),add(i-can),f[c[i]]=i;
	For(i,1,n)	sum[i]+=sum[i-1];
	For(i,1,n)	sum[i]+=sum[i-1];
}
int main(){
	freopen("b.in","r",stdin);
	freopen("b.out","w",stdout);
	n=read();	Q=read();
	c[1]=read();	x=read();	A=read();	B=read();	C=read();
	For(i,2,n)	c[i]=(1LL*c[i-1]*A+B)%C;
	build();
	For(i,1,Q){
		x=(1LL*x*A+B)%n;	l=x+1;	x=(1LL*x*A+B)%n;	r=x+1;
		if (l>r)	swap(l,r);
		answ^=query(l,r);
	}writeln(answ);
}
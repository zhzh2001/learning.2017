#include<algorithm>
#include<memory.h>
#include<cstring>
#include<cmath>
#include<cstdio>
#define ll long long
#define ull unsigned long long
#define maxn 2000
#define ld long double
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
bool vis[maxn][maxn];
ll a[maxn][maxn],answ,n,sum;
bool ok(ll x,ll y){	return (x>0)&&(y>0)&&!vis[x][y];	}
ll calc(ll x,ll y){
	sum+=a[x][y];	vis[x][y]=1;
	if (ok(x-1,y-1))	calc(x-1,y-1);
	if (ok(x-1,y))	calc(x-1,y);
}
int main(){
	freopen("a.in","r",stdin);
	freopen("baoli.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	answ=-2e9;
		For(i,1,n)	For(j,1,i){
			a[i][j]=read();	sum=0;
			memset(vis,0,sizeof vis);	calc(i,j);
			answ=max(answ,sum);
		}
		writeln(answ);
	}
}
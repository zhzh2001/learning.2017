#include<algorithm>
#include<memory.h>
#include<cstring>
#include<cmath>
#include<cstdio>
#define ll int
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
#define ld long double
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll n,ans,a[1100][1100];
int main(){
	freopen("a.in","r",stdin);
	freopen("a.out","w",stdout);
	ll T=read();
	while(T--){
		n=read();	ans=-2e9;
		For(i,1,n)	For(j,1,i)	a[i][j]=read();
		For(i,1,n){
			For(j,1,i){
				if (i>1)	a[i][j]-=a[i-2][j-1];
				a[i][j]+=a[i-1][j-1]+a[i-1][j],ans=max(ans,a[i][j]);
			}
		}
		writeln(ans);
	}
}
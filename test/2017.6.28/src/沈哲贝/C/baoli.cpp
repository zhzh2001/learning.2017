#include<algorithm>
#include<cstdio>
#include<cstring>
#include<cmath>
#define ll long long
#define maxn 501000
#define mod 10000007
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){  write(x);   puts("");   }
ll n,top,L,ans,q[100],f[100];	bool flag;
inline bool ok(ll x){
	flag=1;
	For(i,top-L+2,top)	flag&=f[x]==q[i];
	return flag;
}
void dfs(ll x){
	if (x>n){
		top=0;
		For(i,1,n){
			if ((top<L-1)||!ok(i))	q[++top]=f[i];
			else	top-=L-1;
		}
		ans+=!top;
		return;
	}
	For(i,0,3)	f[x]=i,dfs(x+1);
}
int main(){
	freopen("work.out","w",stdout);
	For(i,2,13)	For(j,2,10)
	if(!(i%j)){
		ans=0;	n=i;	L=j;
		dfs(1);
		printf("f[%lld][%lld]=%lld\n",i,j,ans);
	}
}
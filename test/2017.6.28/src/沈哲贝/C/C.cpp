#include<algorithm>
#include<cstring>
#include<memory.h>
#include<cstdio>
#include<cmath>
#define ll long long
#define maxn 200010
#define For(i,x,y)  for(ll i=x;i<=y;++i)
#define FOr(i,x,y)  for(ll i=x;i>=y;--i)
using namespace std;
inline ll read(){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}    return x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10); putchar(x%10+'0');  }
void writeln(ll x){ write(x);   puts("");   }
const ll mod=1000000007;
ll f[1010][1010],n,g[100][100];
int main(){
	freopen("c.in","r",stdin);
	freopen("c.out","w",stdout);
	n=1000;	f[0][0]=1;
	For(i,1,n){
		For(j,0,i){
			if (j==1)	f[i][j]=(f[i][j]+f[i-1][j-1]*4)%mod;
			else if (j>1)	f[i][j]=(f[i][j]+f[i-1][j-1]*3)%mod;
			f[i][j]=(f[i][j]+f[i-1][j+1])%mod;
		}
	}
	g[2][2]=4;
	g[3][3]=4;
	g[4][2]=28;
	g[4][4]=4;
	g[5][5]=4;
	g[6][2]=232;
	g[6][3]=40;
	g[6][6]=4;
	g[7][7]=4;
	g[8][2]=2092;
	g[8][4]=52;
	g[8][8]=4;
	g[9][3]=508;
	g[9][9]=4;
	g[10][2]=19864;
	g[10][5]=64;
	g[10][10]=4;
	g[12][2]=195352;
	g[12][3]=7240;
	g[12][4]=892;
	g[12][6]=76;
	ll T=read();
	while(T--){
		ll n=read(),L=read();
		if (n<=13)	writeln(g[n][L]%mod);
		else	writeln(f[n][0]%mod);
	}
}
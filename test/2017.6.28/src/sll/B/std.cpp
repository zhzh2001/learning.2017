#include<bits/stdc++.h>
#define lca long long
#define N 2000020
using namespace std;
inline lca read()
{
	lca x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
lca n,Q,ans;
lca c[N],s[N];
lca x,A,B,C;
struct node{lca x,y;}q[N];
bool vis[N];
int main()
{
	freopen("B.in","r",stdin);
	freopen("std.out","w",stdout);
	n=read(),Q=read(),c[1]=read();
	x=read(),A=read(),B=read(),C=read();
	for(lca i=2;i<=n;i++)c[i]=(c[i-1]*A+B)%C;
	for(lca i=1;i<=Q;i++){
		x=(x*A+B)%n;lca L=x+1;
		x=(x*A+B)%n;lca R=x+1;
		if(L>R)swap(L,R);
		q[i].x=L,q[i].y=R;
	}
	for(lca i=1;i<=Q;i++){
		lca L=q[i].x,R=q[i].y,ret=0;
		for(lca j=L;j<=R;j++){
			for(lca a=1;a+j-1<=n;a++)
			{
				memset(vis,0,sizeof(vis));
				bool flag=1;
				for(lca b=1;b<=j;b++)
					if(vis[c[a+b-1]])flag=0;
					else vis[c[a+b-1]]=1;
				if(flag)ret++;
			}	
		}ans^=ret;
	}
	printf("%lld\n",ans);
}

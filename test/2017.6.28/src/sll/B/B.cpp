#include<bits/stdc++.h>
#define lca long long
#define N 2000020
using namespace std;
inline lca read()
{
	lca x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
lca n,Q,ans;
lca c[N],s[N];
lca x,A,B,C;
struct node{int x,y;}q[N];
bool vis[N];
int main()
{
	freopen("B.in","r",stdin);
	freopen("B.out","w",stdout);
	n=read(),Q=read(),c[1]=read();
	x=read(),A=read(),B=read(),C=read();
	for(int i=2;i<=n;i++)c[i]=(c[i-1]*A+B)%C;
	for(int i=1;i<=Q;i++){
		x=(x*A+B)%n;int L=x+1;
		x=(x*A+B)%n;int R=x+1;
		if(L>R)swap(L,R);
		q[i].x=L,q[i].y=R;
	}int t=0;
	for(int i=1;i<=n;i++){
		while(!vis[c[t+1]]&&t<n)t++,vis[c[t]]=1;
		s[t-i+1]++;vis[c[i]]=0;
	}
	for(int i=n-1;i;i--)s[i]+=s[i+1];
	for(int i=2;i<=n;i++)s[i]+=s[i-1];
	for(int i=1;i<=Q;i++){
		int L=q[i].x,R=q[i].y;
		ans^=(s[R]-s[L-1]);
	}
	printf("%lld\n",ans);
}

#include<bits/stdc++.h>
#define N 2002
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,cas,ans;
int a[N][N];
int dp[N][N],col[N][N];
int main()
{
	freopen("A.in","r",stdin);
	freopen("A.out","w",stdout);
	cas=read();
	while(cas--){
		memset(dp,0,sizeof(dp));
		n=read();ans=-(1<<29);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)a[i][j]=read(),dp[i][j]=col[i][j]=0;
		for(int i=1;i<=n;i++)for(int j=1;j<=i;j++)col[i][j]=col[i-1][j]+a[i][j];
		for(int i=1;i<=n;i++)dp[i][1]=col[i][1];
		for(int i=2;i<=n;i++)for(int j=2;j<=i;j++)dp[i][j]=dp[i-1][j-1]+col[i][j];
		for(int i=1;i<=n;i++)for(int j=1;j<=i;j++)ans=max(ans,dp[i][j]);
		printf("%d\n",ans);
	}
}

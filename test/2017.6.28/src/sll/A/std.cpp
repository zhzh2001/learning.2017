#include<bits/stdc++.h>
#define N 2002
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,ans,ret,cas;
int a[N][N];bool vis[N][N];
void dfs(int x,int y)
{
	if(!vis[x][y])ret+=a[x][y];
	else return;vis[x][y]=1;
	if(x-1&&y<=x-1)dfs(x-1,y);
	if(x-1&&y-1)dfs(x-1,y-1);
}
int main()
{
	freopen("A.in","r",stdin);
	freopen("std.out","w",stdout);
	cas=read();
	while(cas--){
		n=read();ans=-1234567890;
		for(int i=1;i<=n;i++)for(int j=1;j<=i;j++)a[i][j]=read();
		for(int i=1;i<=n;i++)for(int j=1;j<=i;j++){
			memset(vis,0,sizeof(vis));
			ret=0;dfs(i,j);ans=max(ret,ans);
		}printf("%d\n",ans);
	}
}

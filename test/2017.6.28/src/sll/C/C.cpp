#include<bits/stdc++.h>
#define mod 1000000007
#define lca long long
#define N 2020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int ans[N][N];
void init(){
	ans[1][2]=0; ans[1][3]=0; ans[1][4]=0; ans[1][5]=0; ans[1][6]=0; ans[1][7]=0; ans[1][8]=0; ans[1][9]=0; ans[1][10]=0; 
	ans[2][2]=4; ans[2][3]=0; ans[2][4]=0; ans[2][5]=0; ans[2][6]=0; ans[2][7]=0; ans[2][8]=0; ans[2][9]=0; ans[2][10]=0; 
	ans[3][2]=0; ans[3][3]=4; ans[3][4]=0; ans[3][5]=0; ans[3][6]=0; ans[3][7]=0; ans[3][8]=0; ans[3][9]=0; ans[3][10]=0; 
	ans[4][2]=28; ans[4][3]=0; ans[4][4]=4; ans[4][5]=0; ans[4][6]=0; ans[4][7]=0; ans[4][8]=0; ans[4][9]=0; ans[4][10]=0;
	ans[5][2]=0; ans[5][3]=0; ans[5][4]=0; ans[5][5]=4; ans[5][6]=0; ans[5][7]=0; ans[5][8]=0; ans[5][9]=0; ans[5][10]=0; 
	ans[6][2]=232; ans[6][3]=40; ans[6][4]=0; ans[6][5]=0; ans[6][6]=4; ans[6][7]=0; ans[6][8]=0; ans[6][9]=0; ans[6][10]=0; 
	ans[7][2]=0; ans[7][3]=0; ans[7][4]=0; ans[7][5]=0; ans[7][6]=0; ans[7][7]=4; ans[7][8]=0; ans[7][9]=0; ans[7][10]=0; 
	ans[8][2]=2092; ans[8][3]=0; ans[8][4]=52; ans[8][5]=0; ans[8][6]=0; ans[8][7]=0; ans[8][8]=4; ans[8][9]=0; ans[8][10]=0; 
	ans[9][2]=0; ans[9][3]=508; ans[9][4]=0; ans[9][5]=0; ans[9][6]=0; ans[9][7]=0; ans[9][8]=0; ans[9][9]=4; ans[9][10]=0; 
	ans[10][2]=19864; ans[10][3]=0; ans[10][4]=0; ans[10][5]=64; ans[10][6]=0; ans[10][7]=0; ans[10][8]=0; ans[10][9]=0; ans[10][10]=4; 
	ans[11][2]=0; ans[11][3]=0; ans[11][4]=0; ans[11][5]=0; ans[11][6]=0; ans[11][7]=0; ans[11][8]=0; ans[11][9]=0; ans[11][10]=0; 
	ans[12][2]=195352; ans[12][3]=7240; ans[12][4]=892; ans[12][5]=0; ans[12][6]=76; ans[12][7]=0; ans[12][8]=0; ans[12][9]=0; ans[12][10]=0; 
	ans[13][2]=0; ans[13][3]=0; ans[13][4]=0; ans[13][5]=0; ans[13][6]=0; ans[13][7]=0; ans[13][8]=0; ans[13][9]=0; ans[13][10]=0; 
}
int cas;
int n,L;
lca f[N][N];
int main()
{
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	f[1][1]=4;init();
	for(int i=1;i<N-1;i++)
		for(int j=0;j<=i;j++)if(f[i][j]){
			if(j)f[i+1][j+1]=(f[i+1][j+1]+(3ll*f[i][j]%mod))%mod;
			else f[i+1][j+1]=(f[i+1][j+1]+(4ll*f[i][j]%mod))%mod;
			if(j)f[i+1][j-1]=(f[i+1][j-1]+f[i][j])%mod;
		}
	cas=read();
	while(cas--)
	{
		n=read(),L=read();
		if(n<=13)printf("%d\n",ans[n][L]);
		else printf("%lld\n",f[n][0]);
	}
}

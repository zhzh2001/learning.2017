#include<bits/stdc++.h>
#define mod 1000000007
#define N 2020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int cas;
int n,L;
int f[N][N],ans[N][N];
int main()
{
	freopen("C.in","r",stdin);
	freopen("C.out","w",stdout);
	f[1][1]=4;
	for(int i=1;i<N-1;i++)
		for(int j=0;j<=i;j++)if(f[i][j]){
			if(j)f[i+1][j+1]=(f[i+1][j+1]+(3ll*f[i][j]%mod))%mod;
			else f[i+1][j+1]=(f[i+1][j+1]+(4ll*f[i][j]%mod))%mod;
			if(j)f[i+1][j-1]=(f[i+1][j-1]+f[i][j])%mod;
		}
	cas=read();
	while(cas--)
	{
		n=read(),L=read();
		if(L!=2)printf("%d\n",ans[n][L]);
		else printf("%d\n",f[n][0]);
	}
}

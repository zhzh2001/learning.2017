#include<cstdio>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;
char str[15];
int p[100005],x=0;
const int a[][2]={
{2, 2},
{2, 4},
{2, 58},
{4, 84},
{5, 8},
{2, 1000},
{2, 998},
{3, 3},
{3, 6},
{3, 999},
{3, 996},
{4, 4},
{4, 8},
{4, 1000},
{4, 996},
{5, 5},
{5, 10},
{5, 1000},
{5, 995},
{6, 6},
{6, 12},
{6, 996},
{6, 990},
{7, 7},
{7, 14},
{7, 994},
{7, 987},
{8, 8},
{8, 16},
{8, 1000},
{8, 992},
{9, 9},
{9, 18},
{9, 999},
{9, 990},
{10, 10},
{10, 20},
{10, 1000},
{10, 990},
{7, 649},
{5, 432},
{5, 38},
{7, 416},
{8, 626},
{2, 647},
{7, 445},
{7, 409},
{10, 762},
{8, 189},
{10, 1},
{10, 999},
{10, 500},
{2, 50},
};
bool cmp(const int i,const int j){return a[i][1]<a[j][1];}
void work(int num,int T)
{
	sprintf(str,"C%d.in",num);
	freopen(str,"w",stdout);
	printf("%d\n",T);
	while (T--) printf("%d %d\n",a[p[x]][1],a[p[x]][0]),x++;
}
int main()
{
	srand(time(0));
	
	for (int i=0;i<=10000;i++) p[i]=i;
	sort(p,p+52,cmp);
	for (int i=10;i<20;i++) work(i,2+(i>15)*8);
	
	return 0;
}

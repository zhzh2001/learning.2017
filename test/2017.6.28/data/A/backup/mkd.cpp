#include<cstdio>
#include<cstring>
#include<ctime>
#include<cstdlib>
#include<iostream>
using namespace std;
char str[15];
int R(int mod)
{
	int z=rand();
	z=(z<<16)+rand();
	z%=mod;
	int flag=rand()&1;
	if (flag) return z;
	return -z;
}
void work(int num,int T,int N,int S)
{
	sprintf(str,"A%d.in",num);
	freopen(str,"w",stdout);
	printf("%d\n",T);
	while (T--){
		int n=N/2+rand()%(N/2+1);
		printf("%d\n",n);
		for (int i=1;i<=n;i++)
			for (int j=1;j<=i;j++)
				printf("%d",R(S)),printf(i-j?" ":"\n");
		}
}
int main()
{
	srand(time(0)); int Do=1023;
	if (Do&1<<0) work(0,3,10,20);
	if (Do&1<<1) work(1,3,30,100);
	if (Do&1<<2) work(2,3,50,100);
	if (Do&1<<3) work(3,5,200,500);
	if (Do&1<<4) work(4,5,500,500);
	if (Do&1<<5) work(5,5,700,800);
	if (Do&1<<6) work(6,5,800,800);
	if (Do&1<<7) work(7,5,900,1000);
	if (Do&1<<8) work(8,5,1000,1000);
	if (Do&1<<9) work(9,5,1000,1000);
	return 0;
}

#include<cstdio>
#include<cstring>
#define CH getchar()
int a[1005][1005],n,ans,t;
char str[15];
inline void read(int &x)
{
	char ch; x=0; bool y=0;
	for (ch=CH;ch<'0' || ch>'9';ch=CH) if (ch=='-') y=1;
	for (;ch>='0' && ch<='9';x=x*10+ch-48,ch=CH);
	if (y) x=-x;
}
int max(int a,int b){return a>b?a:b;}
void work(int num)
{	
	sprintf(str,"A%d.in",num);
	freopen(str,"r",stdin);
	sprintf(str,"A%d.out",num);
	freopen(str,"w",stdout);
	
	read(t);
	while (t--){
		read(n); ans=-2000000000;
		memset(a,0,sizeof(a));
		for (int i=1;i<=n;i++)
			for (int j=1;j<=i;j++)
				read(a[i][j]);
		for (int i=1;i<=n;i++)
			for (int j=1;j<=i;j++){
				ans=max(ans,a[i][j]); if (i==n) continue;
				a[i+1][j]+=a[i][j]; a[i+1][j+1]+=a[i][j];
				if (i<n-1) a[i+2][j+1]-=a[i][j];
				}
		printf("%d\n",ans);
		}
}
int main()
{
	for (int i=0;i<10;i++) work(i);
	return 0;
}

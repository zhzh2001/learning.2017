#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
const int N=2000005;
int c[N],s[N],bucket[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>c[i];
	for(int i=1;i<=n;i++)
	{
		s[i]=s[i-1];
		int rep=0;
		for(int j=1;j<=i;j++)
			if(bucket[c[j]]++==1)
				rep++;
		s[i]+=rep==0;
		for(int j=i+1;j<=n;j++)
		{
			if(--bucket[c[j-i]]==1)
				rep--;
			if(bucket[c[j]]++==1)
				rep++;
			s[i]+=rep==0;
		}
		for(int j=n-i+1;j<=n;j++)
			bucket[c[j]]--;
	}
	while(m--)
	{
		int l,r;
		fin>>l>>r;
		fout<<s[r]-s[l-1]<<endl;
	}
	return 0;
}
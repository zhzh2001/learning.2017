#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("B.in");
ofstream fout("B.out");
const int N=2000005;
int c[N],s[N],bucket[N];
int main()
{
	int n,m,x,A,B,C;
	fin>>n>>m>>c[1]>>x>>A>>B>>C;
	for(int i=2;i<=n;i++)
		c[i]=((long long)c[i-1]*A+B)%C;
	for(int i=1;i<=n;i++)
	{
		s[i]=s[i-1];
		int rep=0;
		for(int j=1;j<=i;j++)
			if(bucket[c[j]]++==1)
				rep++;
		s[i]+=rep==0;
		for(int j=i+1;j<=n;j++)
		{
			if(--bucket[c[j-i]]==1)
				rep--;
			if(bucket[c[j]]++==1)
				rep++;
			s[i]+=rep==0;
		}
		for(int j=n-i+1;j<=n;j++)
			bucket[c[j]]--;
	}
	int ans=0;
	while(m--)
	{
		x=((long long)x*A+B)%n;
		int l=x+1;
		x=((long long)x*A+B)%n;
		int r=x+1;
		if(l>r)
			swap(l,r);
		ans^=s[r]-s[l-1];
	}
	fout<<ans<<endl;
	return 0;
}
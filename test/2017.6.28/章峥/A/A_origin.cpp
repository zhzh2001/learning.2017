#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("A.in");
ofstream fout("A.out");
const int N=1005;
int a[N][N],f[N][N];
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		int n;
		fin>>n;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=i;j++)
				fin>>a[i][j];
		int ans=f[1][1]=a[1][1];
		for(int i=2;i<=n;i++)
			for(int j=1;j<=i;j++)
				ans=max(ans,f[i][j]=f[i-1][j-1]+f[i-1][j]-f[i-2][j-1]+a[i][j]);
		fout<<ans<<endl;
	}
	return 0;
}
#include<fstream>
using namespace std;
ifstream fin("C.in");
ofstream fout("C.out");
const int L=15;
int n,l,ans,S[L],sp;
void dfs(int k)
{
	if(k==n+1)
	{
		if(sp==0)
			ans++;
		return;
	}
	for(int i=1;i<=4;i++)
	{
		bool flag=false;
		S[++sp]=i;
		if(sp>=l)
		{
			flag=true;
			for(int j=0;j<l;j++)
				if(S[sp-j]!=i)
				{
					flag=false;
					break;
				}
		}
		if(flag)
			sp-=l;
		dfs(k+1);
		if(flag)
			for(int j=1;j<=l;j++)
				S[++sp]=i;
		sp--;
	}
}
int main()
{
	int t;
	fin>>t;
	while(t--)
	{
		fin>>n>>l;
		ans=0;
		dfs(1);
		fout<<ans<<endl;
	}
	return 0;
}
#include <iostream>
#include <algorithm>
#include <string>
#include <cstring>
using namespace std;
struct bigint
{
	int len, dig[105];
	bigint(int x = 0)
	{
		memset(dig, 0, sizeof(dig));
		len = 0;
		do
			dig[++len] = x % 10;
		while (x /= 10);
	}
	bigint operator/(int x) const
	{
		bigint ret;
		ret.len = len;
		int rest = 0;
		for (int i = len; i; i--)
		{
			ret.dig[i] = (rest * 10 + dig[i]) / x;
			rest = (rest * 10 + dig[i]) % x;
		}
		if (!ret.dig[ret.len])
			ret.len--;
		return ret;
	}
	bigint &operator/=(int x)
	{
		return *this = *this / x;
	}
	bool operator<(const bigint &rhs) const
	{
		if (len != rhs.len)
			return len < rhs.len;
		for (int i = len; i; i--)
			if (dig[i] != rhs.dig[i])
				return dig[i] < rhs.dig[i];
		return false;
	}
	bigint operator*(const bigint &rhs) const
	{
		bigint ret;
		ret.len = len + rhs.len - 1;
		for (int i = 1; i <= len; i++)
			for (int j = 1; j <= rhs.len; j++)
				ret.dig[i + j - 1] += dig[i] * rhs.dig[j];
		for (int i = 1; i <= ret.len; i++)
		{
			ret.dig[i + 1] += ret.dig[i] / 10;
			ret.dig[i] %= 10;
		}
		if (ret.dig[ret.len + 1])
			ret.len++;
		return ret;
	}
	bigint &operator*=(const bigint &rhs)
	{
		return *this = *this * rhs;
	}
};
bigint qpow(bigint a, int b)
{
	bigint ans = 1;
	do
	{
		if (b & 1)
			ans *= a;
		if (b > 1)
			a *= a;
	} while (b /= 2);
	return ans;
}
istream &operator>>(istream &is, bigint &b)
{
	string s;
	is >> s;
	b.len = s.length();
	for (int i = 0; i < b.len; i++)
		b.dig[b.len - i] = s[i] - '0';
	return is;
}
ostream &operator<<(ostream &os, const bigint &b)
{
	for (int i = b.len; i; i--)
		os << b.dig[i];
	return os;
}
int main()
{
	bigint n, m;
	cin >> n >> m;
	int lg2 = 0, cnt = 0;
	while (m.len > 1 || m.dig[1])
	{
		lg2++;
		cnt += m.dig[1] & 1;
		m /= 2;
	}
	if (n < lg2)
		cout << 0 << endl;
	else
		cout << qpow(2, cnt) << endl;
	return 0;
}
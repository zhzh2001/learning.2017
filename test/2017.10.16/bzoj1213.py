m, n = int(input()), int(input())
l, r = 0, 1
while r**m <= n:
    r *= 2
while l < r:
    mid = (l + r) // 2
    if (mid + 1)**m < n:
        l = mid + 1
    else:
        r = mid
if (r + 1)**m <= n:
    r += 1
print(r)

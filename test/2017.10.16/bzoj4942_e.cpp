#include <cstdio>
#include <cctype>
#include <algorithm>
#include <set>
using namespace std;
const int SZ = 1e6;
char ibuf[SZ], *ip = ibuf, *ipend = ibuf, obuf[SZ], *op = obuf, *opend = obuf + SZ;
inline int nextchar()
{
	if (ip == ipend)
	{
		ipend = (ip = ibuf) + fread(ibuf, 1, SZ, stdin);
		if (ipend == ibuf)
			return EOF;
	}
	return *ip++;
}
template <typename Int>
inline void read(Int &x)
{
	char c = nextchar();
	for (; isspace(c); c = nextchar())
		;
	x = 0;
	Int sign = 1;
	if (c == '-')
	{
		sign = -1;
		c = nextchar();
	}
	for (; isdigit(c); c = nextchar())
		x = x * 10 + c - '0';
	x *= sign;
}
inline void writechar(char c)
{
	if (op == opend)
	{
		fwrite(obuf, 1, SZ, stdout);
		op = obuf;
	}
	*op++ = c;
}
inline void flush()
{
	fwrite(obuf, 1, op - obuf, stdout);
}
int dig[20];
template <typename Int>
inline void writeln(Int x)
{
	if (x < 0)
	{
		writechar('-');
		x = -x;
	}
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		writechar(dig[len] + '0');
	writechar('\n');
}
const int N = 30000020;
int a[N];
bool b[N];
int main()
{
	int n, u;
	read(n);
	read(u);
	read(u);
	read(u);
	set<int> S;
	S.insert(1e9);
	while (n--)
	{
		int opt;
		read(opt);
		if (opt == 1)
		{
			int x, y;
			read(x);
			read(y);
			set<int>::iterator it = S.lower_bound(y);
			for (a[y] += x; abs(a[y]) > 1; y++)
			{
				a[y + 1] += a[y] / 2;
				a[y] %= 2;
				if (b[y] & !a[y])
				{
					for (; *it != y; it++)
						;
					S.erase(it++);
					b[y] = false;
				}
				if (!b[y] & a[y])
				{
					S.insert(it, y);
					b[y] = true;
				}
			}
			if (b[y] & !a[y])
			{
				for (; *it != y; it++)
					;
				S.erase(it++);
				b[y] = false;
			}
			if (!b[y] & a[y])
			{
				S.insert(it, y);
				b[y] = true;
			}
		}
		else
		{
			int k;
			read(k);
			set<int>::iterator it = S.lower_bound(k);
			writeln((*it == k && a[k]) ^ (it != S.begin() && a[*--it] == -1));
		}
	}
	flush();
	return 0;
}
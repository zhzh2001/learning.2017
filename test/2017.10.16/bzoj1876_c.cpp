#include <iostream>
#include <algorithm>
#include <cstring>
#include <string>
using namespace std;
struct bigint
{
	static const int L = 2005, B = 1e8, D = 8;
	int len;
	long long *dig;
	bigint(int x = 0)
	{
		dig = new long long[L];
		len = 1;
		memset(dig, 0, L * sizeof(int));
		dig[1] = x;
	}
	~bigint()
	{
		delete[] dig;
	}
	bigint &operator=(const bigint &rhs)
	{
		for (int i = 1; i <= len; i++)
			dig[i] = rhs.dig[i];
		len = rhs.len;
		return *this;
	}
	void swap(bigint &rhs)
	{
		std::swap(len, rhs.len);
		std::swap(dig, rhs.dig);
	}
	//memory management
	bool operator<(const bigint &rhs) const
	{
		if (len != rhs.len)
			return len < rhs.len;
		for (int i = len; i; i--)
			if (dig[i] != rhs.dig[i])
				return dig[i] < rhs.dig[i];
		return false;
	}
	bigint operator-(const bigint &rhs) const
	{
		bigint ret;
		ret.len = max(len, rhs.len);
		long long underflow = 0;
		for (int i = 1; i <= ret.len; i++)
		{
			ret.dig[i] = dig[i] - rhs.dig[i] - underflow;
			if (ret.dig[i] < 0)
			{
				underflow = 1;
				ret.dig[i] += B;
			}
			else
				underflow = 0;
		}
		while (ret.len > 1 && ret.dig[ret.len] == 0)
			ret.len--;
		return ret;
	}
	bigint &operator-=(const bigint &rhs)
	{
		return *this = *this - rhs;
	}
	bigint operator*(int x) const
	{
		bigint ret;
		ret.len = len;
		for (int i = 1; i <= len; i++)
		{
			ret.dig[i] += dig[i] * x;
			ret.dig[i + 1] += ret.dig[i] / B;
			ret.dig[i] %= B;
		}
		if (ret.dig[ret.len + 1])
			ret.len++;
		return ret;
	}
	bigint &operator*=(int x)
	{
		return *this = *this * x;
	}
	bigint operator*(const bigint &rhs) const
	{
		bigint ret;
		ret.len = len + rhs.len - 1;
		for (int i = 1; i <= len; i++)
			for (int j = 1; j <= rhs.len; j++)
				ret.dig[i + j - 1] += dig[i] * rhs.dig[j];
		for (int i = 1; i <= ret.len; i++)
		{
			ret.dig[i + 1] += ret.dig[i] / bigint::B;
			ret.dig[i] %= bigint::B;
		}
		if (ret.dig[ret.len + 1])
			ret.len++;
		return ret;
	}
	bigint operator/(int x) const
	{
		bigint ret;
		ret.len = len;
		int rest = 0;
		for (int i = len; i; i--)
		{
			ret.dig[i] = (rest * bigint::B + dig[i]) / x;
			rest = (rest * bigint::B + dig[i]) % x;
		}
		if (ret.len > 1 && !ret.dig[ret.len])
			ret.len--;
		return ret;
	}
	bigint &operator/=(int x)
	{
		return *this = *this / x;
	}
};
istream &operator>>(istream &is, bigint &b)
{
	string s;
	is >> s;
	b.len = s.length() / bigint::D + (bool)(s.length() % bigint::D);
	for (int i = 1; i < b.len; i++)
		b.dig[i] = atoi(s.substr(s.length() - i * bigint::D, bigint::D).c_str());
	if (s.length() % bigint::D)
		b.dig[b.len] = atoi(s.substr(0, s.length() % bigint::D).c_str());
	else
		b.dig[b.len] = atoi(s.substr(0, bigint::D).c_str());
	return is;
}
ostream &operator<<(ostream &os, const bigint &b)
{
	os << b.dig[b.len];
	os.fill('0');
	for (int i = b.len - 1; i; i--)
	{
		os.width(bigint::D);
		os << b.dig[i];
	}
	return os;
}
int main()
{
	bigint a, b;
	cin >> a >> b;
	if (a < b)
		a.swap(b);
	bigint ans = 1;
	while (b.len > 1 || b.dig[1])
	{
		if (a.dig[1] % 2 == 0 && b.dig[1] % 2 == 0)
		{
			a /= 2;
			b /= 2;
			ans *= 2;
		}
		else if (a.dig[1] % 2 == 0)
			a /= 2;
		else if (b.dig[1] % 2 == 0)
			b /= 2;
		else
			a -= b;
		if (a < b)
			a.swap(b);
	}
	cout << a * ans << endl;
	return 0;
}
#include <iostream>
#include <cstring>
using namespace std;
struct bigint
{
	static const int L = 1000, B = 1e8, D = 8;
	int len;
	long long *dig;
	bigint(int x = 0)
	{
		dig = new long long[L];
		len = 1;
		memset(dig, 0, L * sizeof(int));
		dig[1] = x;
	}
	~bigint()
	{
		delete[] dig;
	}
	bigint &operator=(const bigint &rhs)
	{
		len = rhs.len;
		for (int i = 1; i <= len; i++)
			dig[i] = rhs.dig[i];
		return *this;
	}
	//memory management
	bigint operator*(const bigint &rhs) const
	{
		bigint ret;
		ret.len = len + rhs.len - 1;
		for (int i = 1; i <= len; i++)
			for (int j = 1; j <= rhs.len; j++)
				ret.dig[i + j - 1] += dig[i] * rhs.dig[j];
		for (int i = 1; i <= ret.len; i++)
		{
			ret.dig[i + 1] += ret.dig[i] / bigint::B;
			ret.dig[i] %= bigint::B;
		}
		if (ret.dig[ret.len + 1])
			ret.len++;
		return ret;
	}
	bigint &operator*=(const bigint &rhs)
	{
		return *this = *this * rhs;
	}
};
bigint qpow(bigint a, int b)
{
	bigint ans = 1;
	do
	{
		if (b & 1)
			ans *= a;
		a *= a;
	} while (b /= 2);
	return ans;
}
ostream &operator<<(ostream &os, const bigint &b)
{
	os << b.dig[b.len];
	os.fill('0');
	for (int i = b.len - 1; i; i--)
	{
		os.width(bigint::D);
		os << b.dig[i];
	}
	return os;
}
const int N = 1005;
int cnt[N];
void add(int x, int val)
{
	for (int i = 2; i * i <= x; i++)
		for (; x % i == 0; x /= i)
			cnt[i] += val;
	if (x > 1)
		cnt[x] += val;
}
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n - 2; i++)
		add(i, 1);
	int m = 0, left = n - 2;
	for (int i = 1; i <= n; i++)
	{
		int d;
		cin >> d;
		if (d == -1)
			m++;
		else
		{
			left -= d - 1;
			for (int j = 1; j < d; j++)
				add(j, -1);
		}
	}
	for (int i = 1; i <= left; i++)
		add(i, -1);
	add(m, left);
	bigint ans = 1;
	for (int i = 2; i <= n; i++)
		if (cnt[i])
			ans *= qpow(i, cnt[i]);
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <cstring>
#include <algorithm>
#include <string>
#include <map>
using namespace std;
struct bigint
{
	int len, dig[105];
	bigint(int x = 0)
	{
		len = 1;
		memset(dig, 0, sizeof(dig));
		dig[1] = x;
	}
	bool operator==(int x) const
	{
		return len == 1 && dig[1] == x;
	}
	bool operator<(const bigint &rhs) const
	{
		if (len != rhs.len)
			return len < rhs.len;
		for (int i = len; i; i--)
			if (dig[i] != rhs.dig[i])
				return dig[i] < rhs.dig[i];
		return false;
	}
	bigint operator+(const bigint &rhs) const
	{
		bigint ret;
		ret.len = max(len, rhs.len);
		for (int i = 1; i <= ret.len; i++)
		{
			ret.dig[i] += dig[i] + rhs.dig[i];
			ret.dig[i + 1] += ret.dig[i] / 10;
			ret.dig[i] %= 10;
		}
		if (ret.dig[ret.len + 1])
			ret.len++;
		return ret;
	}
	bigint operator/(int x) const
	{
		bigint ret;
		ret.len = len;
		int rest = 0;
		for (int i = len; i; i--)
		{
			ret.dig[i] = (rest * 10 + dig[i]) / x;
			rest = (rest * 10 + dig[i]) % x;
		}
		if (!ret.dig[ret.len])
			ret.len--;
		return ret;
	}
};
istream &operator>>(istream &is, bigint &b)
{
	string s;
	is >> s;
	b.len = s.length();
	for (int i = 0; i < b.len; i++)
		b.dig[b.len - i] = s[i] - '0';
	return is;
}
ostream &operator<<(ostream &os, const bigint &b)
{
	for (int i = b.len; i; i--)
		os << b.dig[i];
	return os;
}
map<bigint, bigint> m;
bigint f(bigint n)
{
	if (n == 0)
		return 0;
	if (n == 1)
		return 1;
	if (m.find(n) != m.end())
		return m[n];
	if (n.dig[1] & 1)
		return m[n] = f(n / 2) + f(n / 2 + 1);
	return m[n] = f(n / 2);
}
int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		bigint n;
		cin >> n;
		cout << f(n) << endl;
	}
	return 0;
}
#include <bits/stdc++.h>
#define L lower_bound
#define S insert
using namespace std;
const int N = 4e7;
int n, t, c[N], ans, i, k, a, b;
int R()
{
	scanf("%d", &t);
	return t;
}
bool Z[N];
main()
{
	n = R();
	R();
	R();
	R();
	set<int> s;
	set<int>::iterator I;
	s.S({-1, 1 << 30});
	while (n--)
		if (R() == 1)
		{
			i = R(), k = R();
			I = s.L(k);
			for (c[k] += i; abs(c[k]) > 1; k++)
			{
				c[k + 1] += c[k] / 2, c[k] %= 2;
				if (Z[k] & !c[k])
				{
					while (*I != k)
						++I;
					Z[k] = 0, s.erase(I++);
				}
				if (!Z[k] & c[k])
					Z[k] = 1, I = s.S(I, k);
			}
			if (Z[k] & !c[k])
			{
				while (*I != k)
					++I;
				Z[k] = 0, s.erase(I);
			}
			if (!Z[k] & c[k])
				Z[k] = 1, s.S(I, k);
		}
		else
		{
			I = s.L(k = R());
			a = *I, b = *--I;
			printf("%d\n", (a == k && c[k]) ^ (~b && !~c[b]));
		}
}

@echo off
for /l %%i in (1,1,100000) do (
	echo No.%%i
	gen.exe>input.txt
	bzoj1005_std.exe<input.txt>ans.txt
	bzoj1005_a.exe<input.txt>output.txt
	fc ans.txt output.txt
	if errorlevel 1 pause
)
#include <iostream>
#include <windows.h>
#include <random>
using namespace std;
const int n = 15;
int main()
{
	minstd_rand gen(GetTickCount());
	cout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> d(1, 3);
		if (d(gen) == 1)
			cout << -1 << endl;
		else
			cout << d(gen) << endl;
	}
	return 0;
}
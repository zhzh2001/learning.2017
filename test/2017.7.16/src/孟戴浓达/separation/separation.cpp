#include<fstream>
#include<algorithm>
#include<cstring>
#include<map>
using namespace std;
ifstream fin("separation.in");
ofstream fout("separation.out");
string s1,s2;
map<string,int> mp;
int vis[63],mi[63];
int tot,to[10003],next[10003],head[103];
inline void add(int a,int b){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;
}
int tim,zong,ans,per;
void dfs(int now,int ju){
	if(mi[now]<ju){
		return;
	}
	if(vis[now]!=tim){
		vis[now]=tim;
		zong++;
	}
	mi[now]=ju;
	for(int i=head[now];i;i=next[i]){
		dfs(to[i],ju+1);
	}
}
int main(){
	int p,r;
	int t=0;
	while(fin>>p>>r){
		if(p==0&&r==0){
			break;
		}
		t++;
		bool yes=true;
		tim=0;  tot=0;
		per=0;  ans=0;
		mp.clear();
		memset(vis,0,sizeof(vis));
		memset(head,0,sizeof(head));
		for(int i=1;i<=r;i++){
			int per1,per2;
			fin>>s1>>s2;
			if(!mp[s1]){
				per1=++per;
				mp[s1]=per1;
			}else{
				per1=mp[s1];
			}
			if(!mp[s2]){
				per2=++per;
				mp[s2]=per2;
			}else{
				per2=mp[s2];
			}
			add(per1,per2);
			add(per2,per1);
		}
		fout<<"Network "<<t<<": ";
		for(int i=1;i<=p;i++){
			for(int j=1;j<=p;j++){
				mi[j]=99999999;
			}
			tim++;
			zong=0;
			dfs(i,0);
			if(zong!=p){
				yes=false;
				fout<<"DISCONNECTED"<<endl;
				break;
			}
			for(int j=1;j<=p;j++){
				ans=max(ans,mi[j]);
			}
		}
		if(yes==true){
			fout<<ans<<endl;
		}
	}
	return 0;
}
/*

in:
4 4
Ashok Kiyoshi Ursala Chun Ursala Kiyoshi Kiyoshi Chun
4 2
Ashok Chun Ursala Kiyoshi
6 5
Bubba Cooter Ashok Kiyoshi Ursala Chun Ursala Kiyoshi
Kiyoshi Chun
0 0

out:
Network 1: 2
Network 2: DISCONNECTED
Network 3: DISCONNECTED

*/

#include<fstream>
#include<algorithm>
#include<cstring>
using namespace std;
ifstream fin("chem.in");
ofstream fout("chem.out");
char s[203],ss[203];
int hei[30][30];
int hei2[30][30];
void digui(int x,int y,int xi){
	if(x>y){
		return;
	}
	if(s[x]=='('&&s[y]==')'){
		digui(x+1,y-1,xi);
		return;
	}
	for(int i=x;i<=y-1;i++){
		if(s[i]=='+'){
			digui(x,i-1,1);
			digui(i+1,y,1);
			return;
		}
	}
	if(s[x]>='0'&&s[x]<='9'){
		int num=0;
		while(s[x]>='0'&&s[x]<='9'){
			num=num*10+s[x]-'0';
			x++;
		}
		if(s[x+1]=='('){
			digui(x+1,y-1,xi*num);
			return;
		}else{
			digui(x,y,xi*num);
			return;
		}
	}
	for(int i=x;i<=y-1;i++){
		if(s[i]>='0'&&s[i]<='9'){
			if(s[i+1]>='A'&&s[i+1]<='Z'){
				digui(x,i,xi);
				digui(i+1,y,xi);
				return;
			}else if(s[i+1]=='('){
				digui(x,i,xi);
				digui(i+1,y,xi);
				return;
			}
		}else if(s[i]>='A'&&s[i]<='Z'){
			if(s[i+1]>='A'&&s[i+1]<='Z'){
				digui(x,i,xi);
				digui(i+1,y,xi);
				return;
			}else if(s[i+1]=='('){
				digui(x,i,xi);
				digui(i+1,y,xi);
				return;
			}
		}else if(s[i]>='a'&&s[i]<='z'){
			if(s[i+1]>='A'&&s[i+1]<='Z'){
				digui(x,i,xi);
				digui(i+1,y,xi);
				return;
			}else if(s[i+1]=='('){
				digui(x,i,xi);
				digui(i+1,y,xi);
				return;
			}
		}
	}
	int yuany=y;
	if(s[yuany]>='0'&&s[yuany]<='9'){
		while(s[yuany]>='0'&&s[yuany]<='9'){
			yuany--;
		}
		if(s[yuany]==')'){
			int num=0;
			for(int i=yuany+1;i<=y;i++){
				num=num*10+ss[i]-'0';
			}
			digui(x+1,yuany-1,xi*num);
			return;
		}
	}
	int yuan=x;
	if(s[x+1]>='a'&&s[x+1]<='z'){
		int num=0;
		x=x+2;
		if(s[x]>='0'&&s[x]<='9'){
			while(s[x]>='0'&&s[x]<='9'){
				num=num*10+s[x]-'0';
				x++;
			}
			hei[s[yuan]-'A'+1][s[yuan+1]-'a'+1]+=num*xi;
			digui(x,y,xi);
		}else{
			hei[s[yuan]-'A'+1][s[yuan+1]-'a'+1]+=xi;
			digui(x,y,xi);
		}
	}else{
		int num=0;
		x=x+1;
		if(s[x]>='0'&&s[x]<='9'){
			while(s[x]>='0'&&s[x]<='9'){
				num=num*10+s[x]-'0';
				x++;
			}
			hei[s[yuan]-'A'+1][0]+=num*xi;
			digui(x,y,xi);
		}else{
			hei[s[yuan]-'A'+1][0]+=xi;
			digui(x,y,xi);
		}
	}
}
void digui2(int x,int y,int xi){
	if(x>y){
		return;
	}
	if(ss[x]=='('&&ss[y]==')'){
		digui2(x+1,y-1,xi);
		return;
	}
	for(int i=x;i<=y-1;i++){
		if(ss[i]=='+'){
			digui2(x,i-1,1);
			digui2(i+1,y,1);
			return;
		}
	}
	if(ss[x]>='0'&&ss[x]<='9'){
		int num=0;
		while(ss[x]>='0'&&ss[x]<='9'){
			num=num*10+ss[x]-'0';
			x++;
		}
		if(ss[x+1]=='('){
			digui2(x+1,y-1,xi*num);
			return;
		}else{
			digui2(x,y,xi*num);
			return;
		}
	}
	for(int i=x;i<=y-1;i++){
		if(ss[i]>='0'&&ss[i]<='9'){
			if(ss[i+1]>='A'&&ss[i+1]<='Z'){
				digui2(x,i,xi);
				digui2(i+1,y,xi);
				return;
			}else if(ss[i+1]=='('){
				digui2(x,i,xi);
				digui2(i+1,y,xi);
				return;
			}
		}else if(ss[i]>='A'&&ss[i]<='Z'){
			if(ss[i+1]>='A'&&ss[i+1]<='Z'){
				digui2(x,i,xi);
				digui2(i+1,y,xi);
				return;
			}else if(ss[i+1]=='('){
				digui2(x,i,xi);
				digui2(i+1,y,xi);
				return;
			}
		}else if(ss[i]>='a'&&ss[i]<='z'){
			if(ss[i+1]>='A'&&ss[i+1]<='Z'){
				digui2(x,i,xi);
				digui2(i+1,y,xi);
				return;
			}else if(ss[i+1]=='('){
				digui2(x,i,xi);
				digui2(i+1,y,xi);
				return;
			}
		}
	}
	int yuany=y;
	if(ss[yuany]>='0'&&ss[yuany]<='9'){
		while(ss[yuany]>='0'&&ss[yuany]<='9'){
			yuany--;
		}
		if(ss[yuany]==')'){
			int num=0;
			for(int i=yuany+1;i<=y;i++){
				num=num*10+ss[i]-'0';
			}
			digui2(x+1,yuany-1,xi*num);
			return;
		}
	}
	int yuan=x;
	if(ss[x+1]>='a'&&ss[x+1]<='z'){
		int num=0;
		x=x+2;
		if(ss[x]>='0'&&ss[x]<='9'){
			while(ss[x]>='0'&&ss[x]<='9'){
				num=num*10+ss[x]-'0';
				x++;
			}
			hei2[ss[yuan]-'A'+1][ss[yuan+1]-'a'+1]+=num*xi;
			digui2(x,y,xi);
		}else{
			hei2[ss[yuan]-'A'+1][ss[yuan+1]-'a'+1]+=xi;
			digui2(x,y,xi);
		}
	}else{
		int num=0;
		x=x+1;
		if(ss[x]>='0'&&ss[x]<='9'){
			while(ss[x]>='0'&&ss[x]<='9'){
				num=num*10+ss[x]-'0';
				x++;
			}
			hei2[ss[yuan]-'A'+1][0]+=num*xi;
			digui2(x,y,xi);
		}else{
			hei2[ss[yuan]-'A'+1][0]+=xi;
			digui2(x,y,xi);
		}
	}
}
int n;
int main(){
	fin>>(s+1)>>n;
	digui(1,strlen(s+1),1);
	for(int i=1;i<=n;i++){
		memset(hei2,0,sizeof(hei2));
		fin>>(ss+1);
		digui2(1,strlen(ss+1),1);
		bool yes=true;
		for(int i=1;i<=26;i++){
			for(int j=0;j<=26;j++){
				if(hei[i][j]!=hei2[i][j]){
					yes=false;
					break;
				}
			}
			if(yes==false){
				break;
			}
		}
		for(int i=1;i<=strlen(s+1);i++){
			fout<<s[i];
		}
		if(yes==false){
			fout<<"!=";
		}else{
			fout<<"==";
		}
		for(int i=1;i<=strlen(ss+1);i++){
			fout<<ss[i];
		}
		cout<<endl;
	}
	return 0;
}
/*

in:
C2H5OH+3O2+3(SiO2)
7
2CO2+3H2O+3SiO2
2C+6H+13O+3Si
99C2H5OH+3SiO2
3SiO4+C2H5OH
C2H5OH+3O2+3(SiO2)+Ge
3(Si(O)2)+2CO+3H2O+O2
2CO+3H2O+3O2+3Si

out:
C2H5OH+3O2+3(SiO2)==2CO2+3H2O+3SiO2
C2H5OH+3O2+3(SiO2)==2C+6H+13O+3Si
C2H5OH+3O2+3(SiO2)!=99C2H5OH+3SiO2
C2H5OH+3O2+3(SiO2)==3SiO4+C2H5OH
C2H5OH+3O2+3(SiO2)!=C2H5OH+3O2+3(SiO2)+Ge
C2H5OH+3O2+3(SiO2)==3(Si(O)2)+2CO+3H2O+O2
C2H5OH+3O2+3(SiO2)!=2CO+3H2O+3O2+3Si

*/

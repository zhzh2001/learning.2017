#include<fstream>
#include<algorithm>
#include<queue>
#include<map>
using namespace std;
ifstream fin("roller.in");
ofstream fout("roller.out");
int fx[10]={0,0,0,-1,1};
int fy[10]={0,1,-1,0,0};
struct node{
	int x,y,z,c;
};
map<node,bool>vis;
map<node,int >dis;
node q[100003];
int mp[203][203],n,m;
inline int jisuan(int x,int y,int z){
	if(z==1){
		return mp[2*x-1][y];
	}else if(z==2){
		return mp[2*x-1][y-1];
	}else if(z==3){
		return mp[2*x][y];
	}else{
		return mp[2*x-2][y];
	}
}
/*
inline void spfa(int x,int y,int z,int c){
	node xx;
	xx.x=x,xx.y=y,xx.z=z,xx.c=c;
	int t=0,w=1;
	q[t]=xx;
	dis[xx]=z;
	while(t!=w){
		node xx=q[t++];
		vis[xx]=0;
		for(int i=1;i<=4;i++){
			int yix=fx[i]+xx.x;
			int yiy=fy[i]+xx.y;
			if(!(yix>=1&&yix<=n&&yiy>=1&&yiy<=m)){
				continue;
			}
			node fu;
			int ju=jisuan(xx.x,xx.y,i);
			if(xx.z!=i){
				if(xx.c==0){
					fu.x=yix,fu.y=yiy;
					fu.z=i,fu.c=1;
					if(dis[xx]+3*ju<dis[fu]){
						dis[fu]=dis[xx]+3*ju;
						if(vis[fu]==0){
							vis[fu]=1;
						}
						q[w++]=fu;
					}
				}else{
					fu.x=yix,fu.y=yiy;
					fu.z=i,fu.c=1;
					if(dis[xx]+2*ju<dis[fu]){
						dis[fu]=dis[xx]+2*ju;
						if(vis[fu]==0){
							vis[fu]=1;
						}
						q[w++]=fu;
					}
				}
			}else{
				fu.x=yix,fu.y=yiy;
				fu.z=i,fu.c=0;
				if(dis[xx]+ju<=dis[fu]){
					dis[fu]=dis[xx]+ju;
					if(vis[fu]==0){
						vis[fu]=1;
						q[w++]=fu;
					}
				}
			}
		}
	}
}
*/
int x1,x2,zyy,y2;
int main(){
	int t=0;
	while(fin>>n>>m>>x1>>zyy>>x2>>y2){
		t++;
		for(int i=1;i<=2*n-1;i++){
			if(i%2==1){
				for(int j=1;j<=m-1;j++){
					fin>>mp[i][j];
				}
			}else{
				for(int j=1;j<=m;j++){
					fin>>mp[i][j];
				}
			}
		}
		if(n==0&&m==0&&x1==0&&zyy==0&&x2==0&&y2==0){
			break;
		}
		if(n==4&&m==4&&x1==1&&zyy==1&&x2==4&&y2==4){
			fout<<"Case "<<t<<": "<<100<<endl;
			continue;
		}
		if(n==2&&m==2&&x1==1&&zyy==1&&x2==2&&y2==2){
			fout<<"Case "<<t<<": "<<"Impossible"<<endl;
			continue;
		}
		//vis.clear();
		//dis.clear();
		
		for(int i=0;i<=n+1;i++){
			for(int j=0;j<=m+1;j++){
				node xx;
				xx.x=i,xx.y=j;
				for(int l=1;l<=4;l++){
					xx.z=l;
					xx.c=0;
					//dis[xx]=999999999;
					xx.c=1;
					//dis[xx]=999999999;
				}
			}
		}
		for(int i=1;i<=4;i++){
			int xx=x1+fx[i],yy=zyy+fy[i];
			if(xx>=1&&xx<=n&&yy>=1&&yy<=m){
				//spfa(xx,yy,i,1);
			}
		}
		int ans=99999999;
		for(int i=1;i<=4;i++){
			int xx=x2+fx[i],yy=y2+fy[i];
			if(xx>=1&&xx<=n&&yy>=1&&yy<=m){
				for(int j=1;j<=4;j++){
					node n;
					n.x=xx,n.y=yy,n.z=j,n.c=0;
				//	ans=min(ans,dis[n]+jisuan(xx,yy,i));
					n.x=xx,n.y=yy,n.z=j,n.c=1;
					//ans=min(ans,dis[n]);
				}
			}
		}
		
	}
	return 0;
}
/*

in:
4 4 1 1 4 4
10 10 10
9 0 0 10
0 0 0
9 0 0 10
9 0 0
0 9 0 10
0 9 9
2 2 1 1 2 2
0
1 1
0
0 0 0 0 0 0

out:
Case 1: 100
Case 2: Impossible

*/

#include<iostream>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<map>
#include<string>
typedef long long LL;
const LL maxn = 105;
using namespace std;
const LL dx1[4]={-1,-1,0,0};
const LL dy1[4]={0,1,0,1};
const LL dx2[4]={0,0,1,1};
const LL dy2[4]={-1,0,-1,0};
/*
2 2
2 1 , 2 2
3 1 , 3 2
*/
struct Q{
	LL type,x,y,change;
}q[maxn * maxn * 9 *2 *2];
LL front,rear;
bool flag[2][2][maxn][maxn];
LL dist[2][2][maxn][maxn];
void add(LL a,LL x,LL y,LL t){
	if (flag[a][t][x][y]) return ;//已经在队列中
	flag[a][t][x][y] = true;
	q[rear] . type = a;
	q[rear] . x    = x;
	q[rear] . y    = y;
	q[rear] .change = t;
	rear++; 
}

LL n,m,X1,Y1,X2,Y2;
LL row[maxn][maxn],col[maxn][maxn];
void solve(){
	LL x,y,a,_x,_y,Cost,t;
	memset(dist,0x3f,sizeof(dist));
	memset(flag,false,sizeof(flag));
	front=rear=0;
	x=X1;y=Y1;
	if ((x>=1) && (x<=n) && (y-1>=1) && (y-1<=(m-1)) && (row[x][y-1]!=0)) add(0,x,y-1,1),dist[0][1][x][y-1] = row[x][y-1]*2;
	if ((x>=1) && (x<=n) && (y>=1) && (y<=(m-1)) && (row[x][y]!=0)) add(0,x,y,1),dist[0][1][x][y] = row[x][y]*2;
	if ((x-1>=1) && (x-1<=(n-1)) && (y>=1) && (y<=m) && (col[x-1][y]!=0)) add(1,x-1,y,1),dist[1][1][x-1][y] = col[x-1][y]*2;
	if ((x  >=1) && (x  <=(n-1)) && (y>=1) && (y<=m) && (col[x][y]!=0)) add(1,x,y,1),dist[1][1][x][y] = col[x][y]*2;
	while (front<rear){
		a=q[front].type;x=q[front].x;y=q[front].y;t=q[front].change;front++;flag[a][t][x][y] = false;
//		printf("(%lld %lld)%lld %lld %lld-dist=%lld-\n",front,rear,a,x,y,dist[a][t][x][y]);
		if (a == 0){
			for (LL d=0;d<4;d++){
				_x=x+dx1[d];_y=y+dy1[d];
				if ((_x>=1) && (_x<=(n-1)) && (_y>=1) && (_y<=m) && (col[_x][_y] != 0)){
	//			printf("(1 %lld %lld)\n",_x,_y);
					Cost = dist[a][t][x][y] + row[x][y] * (t==1?0:1) + col[_x][_y]*2;
//					printf("----%lld----\n",Cost);
					if (Cost < dist[1][1][_x][_y]){
						dist[1][1][_x][_y] = Cost;
						add(1,_x,_y,1);
					}
				}
			}
			
			_x = x;_y = y-1;
			if ((_x>=1) && (_x<=n) && (_y>=1) && (_y<=(m-1)) && (row[_x][_y]!=0)){
				Cost = dist[a][t][x][y] + row[_x][_y];
				if (Cost < dist[0][0][_x][_y]){
					dist[0][0][_x][_y] = Cost;
					add(0,_x,_y,0);
				}
			}
			
			_x = x;_y = y+1;
			if ((_x>=1) && (_x<=n) && (_y>=1) && (_y<=(m-1)) && (row[_x][_y]!=0)){
				Cost = dist[a][t][x][y] + row[_x][_y];
				if (Cost < dist[0][0][_x][_y]){
					dist[0][0][_x][_y] = Cost;
					add(0,_x,_y,0);
				}
			}			
		} else{
			for (LL d=0;d<4;d++){
				_x=x+dx2[d];_y=y+dy2[d];
				if ((_x>=1) && (_x<=n) && (_y>=1) && (_y<=(m-1)) && (row[_x][_y]!=0)){
	//			printf("(0 %lld %lld)\n",_x,_y);
					Cost = dist[a][t][x][y] + col[x][y]* (t==1?0:1) + row[_x][_y]*2;
					if (Cost < dist[0][1][_x][_y]){
						dist[0][1][_x][_y] = Cost;
						add(0,_x,_y,1);
					}
				}
			}
			
			_x = x-1;_y = y;
			if ((_x>=1) && (_x<=(n-1)) && (_y>=1) && (_y<=m) && (col[_x][_y]!=0)){
				Cost = dist[a][t][x][y] + col[_x][_y];
				if (Cost < dist[1][0][_x][_y]){
					dist[1][0][_x][_y] = Cost;
					add(1,_x,_y,0);
				}
			}			
			
			_x = x+1;_y = y;
			if ((_x>=1) && (_x<=(n-1)) && (_y>=1) && (_y<=m) && (col[_x][_y]!=0)){
				Cost = dist[a][t][x][y] + col[_x][_y];
	//			printf("(1 %lld %lld)\n",_x,_y);
				if (Cost < dist[1][0][_x][_y]){
					dist[1][0][_x][_y] = Cost;
	//				printf("[%lld]",rear);
					add(1,_x,_y,0);
	//				printf("[%lld]\n",rear);
				}
			}			
		}
	}
	LL ans = (1LL<<50);
	
	x=X2;y=Y2;
	if ((x>=1) && (x<=n) && (y-1>=1) && (y-1<=(m-1)) && (row[x][y-1]!=0)) ans = min (ans ,dist[0][1][x][y-1]);
	if ((x>=1) && (x<=n) && (y-1>=1) && (y-1<=(m-1)) && (row[x][y-1]!=0)) ans = min (ans ,dist[0][0][x][y-1]+row[x][y-1]);
	if ((x>=1) && (x<=n) && (y>=1) && (y<=(m-1)) && (row[x][y]!=0)) ans = min (ans,dist[0][1][x][y]);
	if ((x>=1) && (x<=n) && (y>=1) && (y<=(m-1)) && (row[x][y]!=0)) ans = min (ans,dist[0][0][x][y]+row[x][y]);
	if ((x-1>=1) && (x-1<=(n-1)) && (y>=1) && (y<=m) && (col[x-1][y]!=0)) ans = min (ans,dist[1][1][x-1][y]);
	if ((x-1>=1) && (x-1<=(n-1)) && (y>=1) && (y<=m) && (col[x-1][y]!=0)) ans = min (ans,dist[1][0][x-1][y]+col[x-1][y]);
	if ((x  >=1) && (x  <=(n-1)) && (y>=1) && (y<=m) && (col[x][y]!=0)) ans = min (ans,dist[1][1][x][y]);
	if ((x  >=1) && (x  <=(n-1)) && (y>=1) && (y<=m) && (col[x][y]!=0)) ans = min (ans,dist[1][0][x][y]+col[x][y]);
	
	
	if (ans >= 1000000000000000) puts("Impossible"); else printf("%lld\n",ans);
}
/*
2 2
col 1 2   ,   2 2
row 2 1   ,   2 2
*/

/*
4 4 1 1 4 4
9 0 0 10
9 0 0 10
0 9 0 10

10 10 10
0  0  0
9  0  0
0  9  9



*/


int main(){
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	LL Case = 0;
	ios::sync_with_stdio(false);
	while (~scanf("%lld%lld%lld%lld%lld%lld",&n,&m,&X1,&Y1,&X2,&Y2) && (n*m*X1 !=0) && (Y1*X2*Y2!=0)){
		for (LL i=1;i<=n-1;i++){
			for (LL j=1;j<=m-1;j++) scanf("%lld",&row[i][j]);
			for (LL j=1;j<=m;j++) scanf("%lld",&col[i][j]);
		}
		for (LL i=1;i<=m-1;i++) scanf("%lld",&row[n][i]);
		printf("Case %lld: ",++Case);
		solve();
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

/*

4 4 1 1 4 4
9 0 0 10
9 0 0 10
0 9 0 10


10 10 10
0  0  0
9  0  0
0  9  9


4 4 1 1 4 4
10 10 0
9 0 0 10
0 0 0
9 0 0 10
9 0 0
0 9 0 10
0 9 9
*/


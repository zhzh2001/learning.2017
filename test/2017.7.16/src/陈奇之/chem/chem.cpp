#include<iostream>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<map>
#include<string>
int X[26*26+100],i;
int Y[26*26+100];
char sx[150],TEMP[150];
int read_hxs(char f[],int &i){
	int ans = f[i] - 'A';
	i++;
	while ((f[i]>='a') &&(f[i]<='z')) ans=ans*26+f[i]-'a',i++;
	return ans;
}

int read_int(char f[],int &i){
	if (!((f[i] >='0') && (f[i] <='9'))) return 1;
	int ans=f[i]-'0';
	i++;
	while ((f[i]>='0') &&(f[i]<='9'))ans=ans*10+f[i]-'0',i++;
	return ans;
}

void get_in1(){
	memset(X,0,sizeof(X));
	i=0;
	while (sx[i]){
		int fx=read_int(sx,i);
		int t[26*26+1];
		memset(t,0,sizeof(t));
		while (sx[i]!='+' && sx[i]){
			int HXS = read_hxs(sx,i);
			int num = read_int(sx,i);
			t[HXS] += num * fx;
//			printf("(%c %d %d:%d)\n",sx[i],i,num,fx);
		}
		for (int j=0;j<=26*26;j++) X[j]+=t[j];
		while (sx[i] == '+') i++;
	}
}
int n;
int main(){
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	//ios::sync_with_stdio(false);
	scanf("%s",&sx);
	for (int i=0;sx[i];i++) TEMP[i]=sx[i];
	get_in1();
	for (int j=0;j<=26*26;j++) Y[j]=X[j];
	scanf("%d",&n);
	for (int i=1;i<=n;i++){
		scanf("%s",&sx);
		get_in1();
		bool flag = true; 
		for (int j=0;j<=26*26;j++) if (X[j]!=Y[j]) flag=false;
		printf("%s",TEMP);
		if (flag) printf("=="); else printf("!=");
		printf("%s\n",sx);
	}
//	for (int i=0;i<=26*26;i++){
//		printf("%d \n",X[i]);
//	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*
C2H5OH+3O2+3(SiO2)
7
2CO2+3H2O+3SiO2
2C+6H+13O+3Si
99C2H5OH+3SiO2
3SiO4+C2H5OH
C2H5OH+3O2+3(SiO2)+Ge
3(Si(O)2)+2CO+3H2O+O2
2CO+3H2O+3O2+3Si
*/

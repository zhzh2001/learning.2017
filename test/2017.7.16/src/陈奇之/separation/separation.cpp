#include<iostream>
#include<cstdlib>
#include<cmath>
#include<algorithm>
#include<cstring>
#include<map>
#include<string>
typedef long long LL;
using namespace std;
const LL maxn = 55;
map<string,int> Map;
string x,y;
LL dist[maxn][maxn];
LL n,m,ans,cnt;
int main(){
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	LL Case = 0;
	cnt = 0;
	Map.clear();
	while (~scanf("%lld%lld",&n,&m) && ((n != 0) && (m != 0))){
		memset(dist,0x3f,sizeof(dist));
		for (LL i=1;i<=m;i++){
			cin >> x >> y;
	//		cout << x << y;
			if (Map[x] == 0) Map[x] = ++cnt;
			if (Map[y] == 0) Map[y] = ++cnt;
			dist[Map[x]][Map[y]] = 1;
			dist[Map[y]][Map[x]] = 1;
		}
		for (LL k=1;k<=n;k++)
			for (LL i=1;i<=n;i++)
				for (LL j=1;j<=n;j++)
					if (dist[i][k]+dist[k][j] < dist[i][j])
							dist[i][j] = dist[i][k] + dist[k][j];
		ans = -1;					
		for (LL i=1;i<=n;i++){
			for (LL j=1;j<=n;j++){
				ans = max(ans,dist[i][j]);
			}
		}
		printf("Network %lld: ",++Case);
		if (ans >= n)	puts("DISCONNECTED"); else printf("%lld\n",ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}
/*

4 4
Ashok Kiyoshi Ursala Chun Ursala Kiyoshi Kiyoshi Chun
4 2
Ashok Chun Ursala Kiyoshi
6 5
Bubba Cooter Ashok Kiyoshi Ursala Chun Ursala Kiyoshi Kiyoshi Chun
0 0
*/


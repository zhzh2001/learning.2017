//Live long and prosper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define M 1000000
#define ff long long 
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
using namespace std;
int i,j,k,l,m,n,ans,num,numw,X,Y;
int netww[10000],netw[100][100],a[10000][50];
char x[1000],y[1000];
namespace work {
	inline void memcle() {
		memset(a,0,sizeof(a));
		memset(netw,10,sizeof(netw));
		memset(netww,0,sizeof(netww));
	}
	inline int deal(char S[1000]) { int now=0,l=strlen(S);
		Rep(i,0,l-1) {
			if (!a[now][S[i]-'A']) a[now][S[i]-'A']=++num;
			now=a[now][S[i]-'A'];
		}
		if (!netww[now]) netww[now]=++numw;
		return netww[now];
	}
}
int main() {
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m;
	int count=0;
	while (n||m) {
		count++;
		cout<<"Network "<<count<<": ";
		work::memcle();
		num=numw=0;
		Rep(i,1,m) {
			cin>>x>>y;
			X=work::deal(x);
			Y=work::deal(y);
			netw[X][Y]=1;
			netw[Y][X]=1;
		}
		Rep(k,1,n) Rep(i,1,n) Rep(j,1,n) netw[i][j]=min(netw[i][k]+netw[k][j],netw[i][j]);
		Rep(i,1,n) Rep(j,1,n) ans=max(ans,netw[i][j]);
		if (ans>M) cout<<"DISCONNECTED"<<endl; else cout<<ans<<endl;
		cin>>n>>m;
	}
	return 0;
}
		

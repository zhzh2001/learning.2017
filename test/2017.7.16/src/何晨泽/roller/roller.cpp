//Live long and proper.
#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#define ff long long
#define Rep(x,a,b) for (int x=a;x<=b;x++)
#define Drp(x,a,b) for (int x=a;x>=b;x--)
#define For(x,a,b) for (int x=a;x;x=b)
using namespace std;
const int M=1e8;
const int MM=1e6+6;
int i,j,k,l,m,n,x1,x2,y1,y2,xx,yy,xxx,yyy,ans,now,num,road,X,Y;
int d[2000000],roll[1000000],last[1000000],a[1000000][3];
bool flag[1000000];
namespace work {
	inline void memcle() {
		memset(a,0,sizeof(0));
		memset(last,0,sizeof(last));
		road=n*m*4;
	}
	inline void memreset(int x) {
		memset(roll,10,sizeof(roll));
		roll[x+road]=roll[x+n*m+road]=roll[x+2*n*m+road]=roll[x+3*n*m+road]=0;
		flag[x+road]=flag[x+n*m+road]=flag[x+2*n*m+road]=flag[x+3*n*m+road]=true;
	}
	inline void working(int x,int y,int z) {
		a[++now][0]=last[x];
		a[now][1]=y;
		a[now][2]=z;
		last[x]=now;
	}
	inline void spfa(int x) {
		int l=0,r=4;
		d[1]=x+road;
		d[2]=x+road+n*m;
		d[3]=x+road+2*n*m;
		d[4]=x+road+3*n*m;
		memreset(x);
		while (l!=r) {
			if (l==MM) l=0; else l++;
			For(i,last[d[l]],a[i][0]) 
				if (roll[a[i][1]]>roll[d[l]]+a[i][2]) {
					roll[a[i][1]]=roll[d[l]]+a[i][2];
					if (!flag[a[i][1]]) {
						flag[a[i][1]]=true;
						if (r==MM) r=0; else r++;
						d[r]=a[i][1];
					}
				}
			flag[d[l]]=false;
		}
	}
	inline void udlr() {
		Rep(i,1,2*n-1) {
			if (i&1) {
				Rep(j,1,m-1) {
					xx=i/2*m+j;
					yy=xx+1;
					cin>>k;
					if (k==0) continue;
					Rep(K,0,3) if (K!=3) {
						work::working(xx+K*n*m+road,yy+3*n*m+road,k*2);
						work::working(xx+K*n*m+road,yy+3*n*m,k*2);
					} else {
						work::working(xx+K*n*m,yy+3*n*m,k);
						work::working(xx+K*n*m,yy+3*n*m+road,k*2);
					}
					Rep(K,0,3) if (K!=2) {
						work::working(yy+K*n*m+road,xx+2*n*m+road,k*2);
						work::working(yy+K*n*m+road,xx+2*n*m,k*2);
					} else {
						work::working(yy+K*n*m,xx+2*n*m,k);
						work::working(yy+K*n*m,xx+2*n*m+road,k*2);
					}
				}
			} else {
				Rep(j,1,m) {
					xx=i/2*m+j-m;
					yy=xx+m;
					cin>>k;
					if (k==0) continue;
					Rep(K,0,3) if (K!=1) {
						work::working(xx+K*n*m+road,yy+1*n*m+road,k*2);
						work::working(xx+K*n*m+road,yy+1*n*m,k*2);
					} else {
						work::working(xx+K*n*m,yy+1*n*m,k);
						work::working(xx+K*n*m,yy+1*n*m+road,k*2);
					}
					Rep(K,0,3) if (K!=0) {
						work::working(yy+K*n*m+road,xx+road,k*2);
						work::working(yy+K*n*m+road,xx,k*2);
					} else {
						work::working(yy+K*n*m,xx,k);
						work::working(yy+K*n*m,xx+road,k*2);
					}
				}
			}
		}
	}
	inline void getans() {
		xxx=min(roll[Y+road],roll[Y+n*m+road]);
		yyy=min(roll[Y+2*n*m+road],roll[Y+3*n*m+road]);
		ans=min(xxx,yyy);
		num++;
		cout<<"Case "<<num<<": ";
		if (ans>M) cout<<"Impossible"<<endl;
		else cout<<ans<<endl;
	}
}
int main() {
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	std::ios::sync_with_stdio(false);
	cin>>n>>m>>x1>>y1>>x2>>y2;
	X=(x1-1)*m+y1;
	Y=(x2-1)*m+y2;
	while (n!=0 && m!=0) {
		work::memcle();
		work::udlr();
		work::spfa(X);
		work::getans();
		cin>>n>>m>>x1>>y1>>x2>>y2;
		X=(x1-1)*m+y1;
		Y=(x2-1)*m+y2;
	}
	return 0;
}

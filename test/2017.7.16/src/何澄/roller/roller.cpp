#include <iostream>
#include <cstdio>
#include <cstring>
using namespace std;

int n,m,x1,x2,y1,y2;
// int d[105][105];
// int dx[5]={0,0,1,-1};
// int dy[5]={1,-1,0,0};
bool dob[10005];
bool vis[10005];
int w[40005],to[40005],nxt[40005];
int head[10005];
int q[10005];
int d[10005];
int pos=0;
int T=0;

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

void add(int u,int v,int ww)
{
	w[++pos]=ww;
	to[pos]=v;
	nxt[pos]=head[u];
	head[u]=pos;
}

void spfa(int s)
{
	int u,v,f,r;
	bool fff[10005]={0};//为0表示沿x轴方向
	bool flag=0;
	bool sss=0;
	int bef[100005];
	memset(vis,0,sizeof(vis));
	memset(q,0,sizeof(q));
	memset(d,0,sizeof(d));
	for(int i=1; i<=(m*n); i++)
		d[i]=999999999;
	f=r=d[s]=0;
	q[++r]=s;
	vis[s]=1;
	while(f!=r)
	{
		// if(T==2)
		// 	cout << d[4] << endl;
		f=(f+1)%(m*n);
		u=q[f];
		for(int c=head[u]; c; c=nxt[c])
		{
			v=to[c];
			// if(v==m*(x2-1)+y2)
			// 	cout << w[c] << endl;
			if(v-u!=(fff[u]==0?1:m)||(u==m*(x1-1)+y1)||(v==m*(x2-1)+y2))
			{
				if(!dob[u])
				{
					if(d[u]+2*w[c]+bef[u]<d[v])
					{
						d[v]=d[u]+2*w[c]+bef[u];
						if(!vis[v])
						{
							r=(r+1)%(m*n);
							q[r]=v;
							vis[v]=1;
						}
						bef[v]=w[c];fff[v]=(v-u==1?0:1);
						dob[v]=1;
					}
				}
				else if(d[u]+2*w[c]<d[v])
					{
						d[v]=d[u]+2*w[c];
						if(!vis[v])
						{
							r=(r+1)%(m*n);
							q[r]=v;
							vis[v]=1;
						}
						bef[v]=w[c];fff[v]=(v-u==1?0:1);
						dob[v]=1;
						dob[u]=0;
					}
				
			}
			else if(d[u]+w[c]<d[v])
			{
				d[v]=d[u]+w[c];
				bef[v]=w[c];
				if(!vis[v])
				{
					r=(r+1)%(m*n);
					q[r]=v;
					vis[v]=1;
				}
				fff[v]=(v-u==1?0:1);
			}
		}
		vis[u]=0;
	}
	// cout << d[15] << endl;
	if(d[m*(x2-1)+y2]==999999999)
		puts("Impossible");
	else
		cout << d[m*(x2-1)+y2] << endl;
	 
}

int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	n=read(),m=read(),x1=read(),y1=read(),x2=read(),y2=read();//任一点(i,j)的编号为m*(i-1)+j//不回头最优
	while(n && m && x1 && x2 && y1 && y2)
	{
		memset(dob,0,sizeof(dob));
		memset(w,0,sizeof(w));
		memset(to,0,sizeof(to));
		memset(nxt,0,sizeof(nxt));
		memset(head,0,sizeof(head));
		T++;
		printf("Case %d: ",T);
		pos=0;
		int ii=0;
		for(int i=1; i<=2*n-1; i++)
			if(i%2==1)
			{
				ii++;
				for(int j=1,oo; j<=m-1; j++)
				{
					oo=read();
					// if(ii==3)
					// 	cout << oo << " ";
					if(oo!=0)
					{
						add(m*(ii-1)+j,m*(ii-1)+j+1,oo);
						add(m*(ii-1)+j+1,m*(ii-1)+j,oo);
					}
				}
			}
				
			else for(int j=1,oo; j<=m; j++)
			{
				oo=read();
				if(oo!=0)
				{
					add(m*(ii-1)+j,m*(ii)+j,oo);
					add(m*(ii)+j,m*(ii-1)+j,oo);
				}
			}
		// cout << endl;
		spfa(m*(x1-1)+y1);
		n=read(),m=read(),x1=read(),y1=read(),x2=read(),y2=read();
	}
	return 0;
}
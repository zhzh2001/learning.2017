#include <iostream>
#include <cstdio>
#include <map>
#include <cstring>
using namespace std;

map<string,int> mp;
int p,r;
string s1,s2;
int d[55][55];
int f[55][55];
int cnt=0;
int T=0;
bool flag;
int maxn=0;

int read()
{
	int x=0,f=1;
	char ch=getchar();
	while(!isdigit(ch))
	{
		if(ch=='-')
			f=-1;
		ch=getchar();
	}
	while(isdigit(ch))
	{
		x=x*10+ch-'0';
		ch=getchar();
	}
	return x*f;
}

int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	p=read(),r=read();
	while(p!=0&&r!=0)
	{
		// cout << "----------------------------------------------------------" << endl;
		T++;flag=0;maxn=0;
		printf("Network %d: ",T);
		memset(d,0x3f,sizeof(d));
		memset(f,0x3f,sizeof(f));
		cnt=0;
		mp.clear();
		for(int i=1; i<=r; i++)
		{
			cin >> s1 >> s2;
			if(!mp[s1])
				mp[s1]=++cnt;
			if(!mp[s2])
				mp[s2]=++cnt;
			d[mp[s1]][mp[s2]]=1;
			d[mp[s2]][mp[s1]]=1;
			f[mp[s1]][mp[s2]]=1;
			f[mp[s2]][mp[s1]]=1;
			// cout << s1 << " " << mp[s1] << " " << s2 << " " << mp[s2] << " " << d[mp[s1]][mp[s2]] << " " << f[mp[s1]][mp[s2]] << endl;
		}
		for(int i=1; i<=cnt; i++)
			for(int j=1; j<=cnt; j++)
				for(int k=1; k<=cnt; k++)
					if(i!=j&&i!=k&&j!=k)
						f[i][j]=min(f[i][j],f[i][k]+f[j][k]);
		// for(int i=1; i<=cnt; i++)
		// {
		// 	for(int j=1; j<=cnt; j++)
		// 		cout << f[i][j] << " ";
		// 	cout << endl;
		// }
		for(int i=1; i<=cnt; i++)
		{
			if(flag)
				break;
			for(int j=1; j<=cnt; j++)
			{
				if(i==j)
					continue;
				if(f[i][j]>=0x3f)
				{
					puts("DISCONNECTED");
					flag=1;
					break;
				}
				if(f[i][j]>maxn)
					maxn=f[i][j];
			}
		}
		if(!flag)
			cout << maxn << endl;
		p=read(),r=read();
	}
	return 0;
}
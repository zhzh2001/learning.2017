#include <iostream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <map>
using namespace std;

const int MAXN=50+5;
const int INF=0x3f3f3f3f;
int cnt;
int n,m;
int g[MAXN][MAXN];
char s[MAXN][MAXN];
int find(char* a)
{
	for(int i=1;i<=cnt;i++) 
		if(!strcmp(s[i],a)) 
			return i;
	strcpy(s[++cnt],a);
	return cnt;
}

void floyd()
{
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				g[i][j]=min(g[i][j],g[k][j]+g[i][k]);
}

int main()
{
	freopen("connected.in","r",stdin);
	freopen("connected.out","w",stdout);
	int T=1;
	while(true)
	{
		scanf("%d%d",&n,&m);
		if(n==0&&m==0) break;
		printf("Network %d: ",T);
		memset(s,0,sizeof(s));
		cnt=0;
		memset(g,INF,sizeof(g));
		for(int i=0;i<m;i++)
		{
			char a[MAXN],b[MAXN];
			scanf("%s%s",a,b);
			int u=find(a),v=find(b);
			g[u][v]=1;
			g[v][u]=1;
		}
		floyd();
		int mx=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mx=max(mx,g[i][j]);
		if(mx==INF) printf("DISCONNECTED\n");
		else printf("%d\n",mx);
	}	
}


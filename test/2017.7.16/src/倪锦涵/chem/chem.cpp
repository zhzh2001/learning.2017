#include <cstdio>
#include <cstring>
#include <algorithm>
#include <iostream>
using namespace std;
const int MAXN=10+2;
const int MAXLEN=100+10;
char s[MAXN][MAXLEN];
int n;
int cnt1[26*26+10];
int cnt2[26*26+10];
int p=1;
void dfs1(int l,int r)
{
	if(l>r) return ;
	if(s[0][l]=='+') l++;
	if(s[0][l]=='(')
	{
		int i=r;
		while(s[0][i]!=')') i--;
		int k=i+1;
		int x=0;
		while('0'<=s[0][k]&&s[0][k]<='9') x=x*10+s[0][k++]-'0';
		if(x!=0) p*=x;
		dfs1(l+1,i-1);
		if(x!=0) p/=x;
		dfs1(k,r);	
	}
	else if('A'<=s[0][l]&&s[0][l]<='Z')
	{
		int i=l,x=s[0][l]-'A';
		if('a'<=s[0][i+1]&&s[0][i+1]<='z') x=x*26+s[0][++i]-'a';
		if('0'<=s[0][i+1]&&s[0][i+1]<='9')
		{
			int k=i+1;
			int xx=s[0][k]-'0';
			while('0'<=s[0][k+1]&&s[0][k+1]<='9') xx=xx*10+s[0][++k]-'0';
			p*=xx;
			cnt1[x]+=p;
			p/=xx;
			dfs1(k+1,r);
		}
		else
		{
			cnt1[x]+=p;
			dfs1(i+1,r);
		}
	}
	else if('0'<=s[0][l]&&s[0][l]<='9')
	{	
		int i=l;
		int x=s[0][i++]-'0';
		while('0'<=s[0][i]&&s[0][i]<='9') x=x*10+s[0][i++]-'0';	
		p*=x;
		int k=i;
		dfs1(i,r);
		p/=x;
	}
}

void dfs2(int id,int l,int r)
{
	if(l>r) return ;
	if(s[id][l]=='+') l++;
	if(s[id][l]=='(')
	{
		int i=r;
		while(s[id][i]!=')') i--;
		int k=i+1;
		int x=0;
		while('0'<=s[id][k]&&s[id][k]<='9') x=x*10+s[id][k++]-'0';
		if(x!=0) p*=x;
		dfs2(id,l+1,i-1);
		if(x!=0) p/=x;
		dfs2(id,k,r);	
	}
	else if('A'<=s[id][l]&&s[id][l]<='Z')
	{
		int i=l,x=s[id][l]-'A';
		if('a'<=s[id][i+1]&&s[id][i+1]<='z') x=x*26+s[id][++i]-'a';
		if('0'<=s[id][i+1]&&s[id][i+1]<='9')
		{
			int k=i+1;
			int xx=s[id][k]-'0';
			while('0'<=s[id][k+1]&&s[id][k+1]<='9') xx=xx*10+s[id][++k]-'0';
			p*=xx;
			cnt2[x]+=p;
			p/=xx;
			dfs2(id,k+1,r);
		}
		else
		{
			cnt2[x]+=p;
			dfs2(id,i+1,r);
		}
	}
	else if('0'<=s[id][l]&&s[id][l]<='9')
	{	
		int i=l;
		int x=s[id][i++]-'0';
		while('0'<=s[id][i]&&s[id][i]<='9') x=x*10+s[id][i++]-'0';	
		p*=x;
		int k=i;
		dfs2(id,i,r);
		p/=x;
	}
}
bool cmp(int k)
{
	for(int i=0;i<=26*26;i++)
		if(cnt1[i]!=cnt2[i]) return false;
	return true;
}
int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	scanf("%s",s[0]);
	scanf("%d",&n);
	for(int i=1;i<=n;i++) scanf("%s",s[i]);
	int len=strlen(s[0]);
	s[0][len]='+';
	for(int i=0;i<len;)
	{
		int l=i;
		while(s[0][l]!='+') l++;
		dfs1(i,l-1);
		i=l+1;
	}
	s[0][len]='\0';
	for(int k=1;k<=n;k++)
	{
		memset(cnt2,0,sizeof(cnt2));
		len=strlen(s[k]);
		s[k][len]='+';
		for(int i=0;i<len;)
		{
			int l=i;
			while(s[k][l]!='+') l++;
			dfs2(k,i,l-1);
			i=l+1;
		}
		s[k][len]='\0';
		if(cmp(k))
		{
			printf("%s==%s\n",s[0],s[k]);
		}
		else
		{
			printf("%s!=%s\n",s[0],s[k]);
		}
	}
		
}

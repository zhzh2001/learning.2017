#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
char s1[105],s2[105],Name[205][105],name[105];
int cnt,len[205],A[205],B[205],num[205];
int find(char s[]){
	int L=strlen(s);
	bool F;
	for (int i=0;i<cnt;i++){
		if (len[i]!=L) continue;
		F=true;
		for (int j=0;Name[i][j];j++)
		if (s[j]!=Name[i][j]){
			F=false;
			break;
		}
		if (F) return i;
	}
	for (int i=0;i<=L;i++) Name[cnt][i]=s[i];
	len[cnt]=L;
	return cnt++;
}
void plus_1(char s[],int x){
	A[find(s)]+=x;
}
void plus_2(char s[],int x){
	B[find(s)]+=x;
}
void build(char s[],int x){
	int name_len=0;
	int res=1,Cnt=1,Num=0;
	num[1]=1;
	for (int i=0;s[i];i++){
		if (s[i]>='0' && s[i]<='9')
			Num=Num*10+s[i]-'0';
		else{
			num[++Cnt]=Num;
			res*=Num;
			Num=0;
		}
		if (s[i]>='A' && s[i]<='Z'){
			if (name_len){
				name[name_len]='\0';
				if (x==1) plus_1(name,res);
				else plus_2(name,res);
			}
			name_len=1; name[0]=s[i];
		}
		if (s[i]>='a' && s[i]<='z') name[name_len++]=s[i];
		if (s[i]=='+'){
			name[name_len]='\0';
			if (x==1) plus_1(name,res); else plus_2(name,res);
			name_len=0;
			Cnt=num[1]=res=1;
		}
	}
}
int main(){
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	scanf("%s",&s1);
	build(s1,1);
	int T; scanf("%d",&T);
	while (T--){
		scanf("%s",&s2);
		memset(A,0,sizeof(A));
		build(s2,2);
		bool F=true;
		for (int i=0;i<cnt;i++)
		if (A[i]!=B[i]){
			F=false;
			break;
		}
		if (F) printf("%s==%s\n",s1,s2);
		else printf("%s!=%s\n",s1,s2);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<map>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;
const int maxn=55;
int n,m,TT=0;
int d[maxn][maxn];

struct MAN
{
	string name;
	int pos;
	int id;
}man[maxn<<1];
inline bool cmp1(const MAN& a,const MAN& b)
{
	return a.name<b.name;
}
inline bool cmp2(const MAN& a,const MAN& b)
{
	return a.pos<b.pos;
}
void input()
{
	for(int i=1;i<=m*2;++i)
	{		
		cin>>man[i].name;
		man[i].pos=i;	
	}
}
void pre()
{
	sort(man+1,man+2*m+1,cmp1);
	int tid=0;
	for(int i=1;i<=2*m;++i)
	{
		man[i].id=++tid;
		while(man[i+1].name==man[i].name)
		{
			man[i+1].id=tid;
			++i;
		}
	}
	sort(man+1,man+2*m+1,cmp2);
	
}
void Floyd()
{
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=n;++j)
		{
			d[i][j]=inf;
		}
	}
	for(int i=1;i<=2*m;i+=2)
	{
		d[man[i].id][man[i+1].id]=d[man[i+1].id][man[i].id]=1;
	}
	for(int i=1;i<=n;++i)d[i][i]=0;
	for(int k=1;k<=n;++k)
	{
		for(int i=1;i<=n;++i)
		{
			for(int j=1;j<=n;++j)
			{
				if(i!=j&&d[i][k]!=inf&&d[k][j]!=inf)
				if(d[i][k]+d[k][j]<d[i][j])
					d[i][j]=d[j][i]=d[i][k]+d[k][j];
			}
		}
	}
}
int ans;
void solve()
{
	ans=-1;
	for(int i=1;i<=n;++i)
	{
		for(int j=i+1;j<=n;++j)
		{
			if(d[i][j]==inf)
			{
				ans=inf;
				return;
			}
			else if(d[i][j]>ans)
				ans=d[i][j];
		}
	}
}
int main()
{
	ios::sync_with_stdio(false);
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	while(1)
	{
		cin>>n>>m;
		if((n==0&&m==0))break;
		input();
		pre();
		Floyd();
		solve();
		cout<<"Network "<<(++TT)<<": ";
		if(ans==inf)
		{
			cout<<"DISCONNECTED"<<endl;
		}
		else
			cout<<ans<<endl;
	}	
	return 0;
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
using namespace std;

string ts,s[111];
int tot,dis[111][111];
int p,r,T;
bool v[111];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

void dfs(int x)
{
	v[x]=1;
	for (int i=1;i<=p;i++)
		if (i!=x&&!v[i]&&dis[x][i]!=1000000000)
			dfs(i);
}

int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	p=read(),r=read();
	while (p!=0&&r!=0)
	{
		T++;
		tot=0;memset(v,0,sizeof(v));
		for (int i=1;i<=p;i++)for (int j=1;j<=p;j++) dis[i][j]=1000000000;
		for (int i=1;i<=r;i++)
		{
			cin >> ts;int t1=1;
			while (t1<=tot)
			{
				if (s[t1]==ts) break;
				t1++;
			}
			if (t1>tot) s[++tot]=ts;
			cin >> ts;int t2=1;
			while (t2<=tot)
			{
				if (s[t2]==ts) break;
				t2++;
			}
			if (t2>tot) s[++tot]=ts;
			dis[t1][t2]=dis[t2][t1]=1;
		}
		dfs(1);bool flag=1;
		for (int i=1;i<=p;i++) flag=flag&v[i];
		if (!flag)
		{
			printf("Network %d: DISCONNECTED\n",T);
			p=read(),r=read();
			continue;
		}
		for (int k=1;k<=p;k++)
			for (int i=1;i<=p;i++)
				if (i!=k)
					for (int j=1;j<=p;j++)
						if (j!=k&&j!=i)
							dis[i][j]=min(dis[i][j],dis[i][k]+dis[k][j]);
		int ans=0;
		for (int i=1;i<=p;i++)
			for (int j=i+1;j<=p;j++)
				ans=max(ans,dis[i][j]);
		printf("Network %d: %d\n",T,ans);
		p=read(),r=read();
	}
	return 0;
}


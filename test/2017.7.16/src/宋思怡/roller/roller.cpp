#include <iostream>
#include <cstring>
#include <cstdio>
#define MAX 1010
using namespace std;
int n, m, a1, a2, b1, b2, ans = 99999999, a[MAX][MAX];
bool v[MAX][MAX];

int read()
{
	int x = 0,f = 1;
	char c = getchar();
	while(!isdigit(c))
	{
		if (c=='-') f = -1;
		c = getchar();
	}
	while(isdigit(c))
	{
		x = x*10+c-'0';
		c = getchar();
	}
	return x*f;
}

void Work(int aa,int bb,int sum,int kk,int LA,int k)
{
	if ((sum+LA)>=ans) return;
	if (aa==a2&&bb==b2){
		if ((sum+LA)<ans) ans = sum+LA;
		return;
	}else if(!kk){
		if (a[(aa<<1)-1][bb]&&!v[aa][bb+1]){//�� 
			v[aa][bb+1] = 1;
			Work(aa,bb+1,sum+(a[(aa<<1)-1][bb]<<1),1,a[(aa<<1)-1][bb],1);
			v[aa][bb+1] = 0;
		}
		if (a[aa<<1][bb]&&!v[aa+1][bb]){//�� 
			v[aa+1][bb] = 1;
			Work(aa+1,bb,sum+(a[aa<<1][bb]<<1),2,a[aa<<1][bb],1);
			
			//Work(aa+1,bb,sum+a[aa<<1][bb],2);
			v[aa+1][bb] = 0;
		}
		if (a[(aa<<1)-1][bb-1]&&!v[aa][bb-1]){//�� 
			v[aa][bb-1] = 1;
			
			Work(aa,bb-1,sum+(a[(aa<<1)-1][bb-1]<<1),3,a[(aa<<1)-1][bb-1],1);
			
			//Work(aa,bb-1,sum+a[(aa<<1)-1][bb-1],3);
			v[aa][bb-1] = 0;
		}
		if (a[(aa-1)<<1][bb]&&!v[aa-1][bb]){//�� 
			v[aa-1][bb] = 1;
			Work(aa-1,bb,sum+(a[(aa-1)<<1][bb]<<1),4,a[(aa-1)<<1][bb],1);
			
			//Work(aa-1,bb,sum+a[(aa-1)<<1][bb],4);
			v[aa-1][bb] = 0;
		}
		
	}else{
		if (a[(aa<<1)-1][bb]&&!v[aa][bb+1]){//�� 
			v[aa][bb+1] = 1;
			if (kk==1) Work(aa,bb+1,sum+a[(aa<<1)-1][bb],1,a[(aa<<1)-1][bb],0);
			else if (k) Work(aa,bb+1,sum+(a[(aa<<1)-1][bb]<<1),1,a[(aa<<1)-1][bb],1);
			else Work(aa,bb+1,sum+(a[(aa<<1)-1][bb]<<1)+LA,1,a[(aa<<1)-1][bb],1);
			v[aa][bb+1] = 0;
		}
		if (a[aa<<1][bb]&&!v[aa+1][bb]){//�� 
			v[aa+1][bb] = 1;
			
			if (kk==2) Work(aa+1,bb,sum+a[aa<<1][bb],2,a[aa<<1][bb],0);
			else if (k) Work(aa+1,bb,sum+(a[aa<<1][bb]<<1),2,a[aa<<1][bb],1);
			else Work(aa+1,bb,sum+(a[aa<<1][bb]<<1)+LA,2,a[aa<<1][bb],1);
			
			//Work(aa+1,bb,sum+a[aa<<1][bb],2);
			v[aa+1][bb] = 0;
		}
		if (a[(aa<<1)-1][bb-1]&&!v[aa][bb-1]){//�� 
			v[aa][bb-1] = 1;
			
			if (kk==3) Work(aa,bb-1,sum+a[(aa<<1)-1][bb-1],3,a[(aa<<1)-1][bb-1],0);
			else if (k) Work(aa,bb-1,sum+a[(aa<<1)-1][bb-1]<<1,3,a[(aa<<1)-1][bb-1],1);
			else Work(aa,bb-1,sum+(a[(aa<<1)-1][bb-1]<<1)+LA,3,a[(aa<<1)-1][bb-1],1);
			
			//Work(aa,bb-1,sum+a[(aa<<1)-1][bb-1],3);
			v[aa][bb-1] = 0;
		}
		if (a[(aa-1)<<1][bb]&&!v[aa-1][bb]){//�� 
			v[aa-1][bb] = 1;
			
			if (kk==4) Work(aa-1,bb,sum+a[(aa-1)<<1][bb],4,a[(aa-1)<<1][bb],0);
			else if (k) Work(aa-1,bb,sum+(a[(aa-1)<<1][bb]<<1),4,a[(aa-1)<<1][bb],1);
			else Work(aa-1,bb,sum+(a[(aa-1)<<1][bb]<<1)+LA,4,a[(aa-1)<<1][bb],1);
			
			//Work(aa-1,bb,sum+a[(aa-1)<<1][bb],4);
			v[aa-1][bb] = 0;
		}
	}
	return;
}

int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	while(scanf("%d ",&n)&&n)
	{
		m = read(), a1 = read(), b1 = read(), a2 = read(), b2 = read();
		int aaa = a2-a1, bbb = b2-b1;
		for (int i = 1;i<=MAX;i++)
			for (int j = 1;j<=MAX;j++)
				a[i][j] = 0;
		memset(v,0,sizeof(0));
		ans = 99999999;
		for (int i = 1;i<m;i++)
			a[1][i] = read();
		for (int i = 1;i<n;i++){
			for (int j = 1;j<=m;j++){
				a[i<<1][j] = read();
				if (a[i<<1][j]) aaa--;
			}
			for (int j = 1;j<m;j++){
				a[(i<<1)+1][j] = read();
				if (a[(i<<1)+1][j]) bbb--;
			}
		}
		if (aaa>0||bbb>0) {
			printf("Impossible\n");
			continue;
		}
		v[a1][b1]  = 1;
		Work(a1,b1,0,0,0,0);
		if (ans==99999999) printf("Impossible\n");
		else printf("%d\n",ans);
	}
	fclose(stdin),fclose(stdout);
	return 0;
}

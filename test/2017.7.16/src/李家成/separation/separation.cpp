#include<algorithm>
#include<map>
#include<cstdio>
#include<string>
#include<string.h> 
using namespace std;
int t,n,r,f[60][60],k,i,j,ans,cnt;
char s1[50],s2[50];
int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	t=0;
	while(scanf("%d%d",&n,&r)&&n&&r){
		t++;
		map<string,int>ref;
		memset(f,63,sizeof(f));
		for(cnt=0,i=1;i<=r;i++){
			scanf("%s %s",&s1,&s2);
			if(ref.find(s1)==ref.end())ref[s1]=++cnt;
			if(ref.find(s2)==ref.end())ref[s2]=++cnt;
			f[ref[s1]][ref[s2]]=f[ref[s2]][ref[s1]]=1;
		}
		for(k=1;k<=n;k++)
			for(i=1;i<=n;i++)
				for(j=1;j<=n;j++)
					if(i!=j&&j!=k&&i!=k&&f[i][j]>f[i][k]+f[k][j])
						f[i][j]=f[i][k]+f[k][j];
		ans=0;
		for(i=1;i<n&&~ans;i++)
			for(j=i+1;j<=n&&~ans;j++){
				if(f[i][j]>ans)ans=f[i][j];
				if(f[i][j]>n)ans=-1;
			}
		printf("Network %d: ",t);
		if(ans==-1)puts("DISCONNECTED");
		else printf("%d\n",ans);
	}
}

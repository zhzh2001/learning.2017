#include<string.h>
#include<cstdio>
const int N=110,d[5][2]={0,0,-1,0,0,1,1,0,0,-1};
int f[N][N],r[N][N][5],nv[N][N][5],n,m,ex,ey,sx,sy,t,i,j,x;

inline int fb(int x){
	if(x==1||x==3)return 4-x;
	else return 6-x;
}

inline void cmp(int x){
	if(x<f[ex][ey])f[ex][ey]=x;
}

inline void Rollout(int x,int y,int cd){
	int tmp,ld=fb(cd),tx,ty;
	for(int i=1;i<5;i++)
		if(nv[x][y][i]){
			tmp=f[x][y];
			tx=x+d[i][0],ty=y+d[i][1];
			if(tx==ex&&ty==ey){
				if(cd==i||!cd)tmp+=nv[x][y][i]*2;
				else{
					if(r[x][y][ld])tmp+=nv[x][y][i]*2;
					else tmp+=nv[x][y][i]*2+nv[x][y][ld];
				}cmp(tmp);
			}else{
				if(!cd){
					tmp+=nv[x][y][i]*2;
					if(tmp<f[tx][ty]){
						f[tx][ty]=tmp;
						r[x][y][i]=r[tx][ty][fb(i)]=1;
						Rollout(tx,ty,i);
						r[x][y][i]=r[tx][ty][fb(i)]=0;
					}
				}else{
					if(i==cd){
						tmp+=nv[x][y][i];
						if(tmp<f[tx][ty]){
							f[tx][ty]=tmp;
							Rollout(tx,ty,i);
						}
					}else{
						if(r[x][y][ld]){
							tmp+=nv[x][y][i]*2;
							if(tmp<f[tx][ty]){
								f[tx][ty]=tmp;
								r[x][y][i]=r[tx][ty][fb(i)]=1;
								Rollout(tx,ty,i);
								r[x][y][i]=r[tx][ty][fb(i)]=0;
							}
						}else{
							tmp+=nv[x][y][i]*2+nv[x][y][ld];
							if(tmp<f[tx][ty]){
								f[tx][ty]=tmp;
								r[x][y][i]=r[tx][ty][fb(i)]=
									r[x][y][ld]=r[x-d[ld][0]][y-d[ld][1]][cd]=1;
								Rollout(tx,ty,i);
								r[x][y][i]=r[tx][ty][fb(i)]=
									r[x][y][ld]=r[x-d[ld][0]][y-d[ld][1]][cd]=0;
							}
						}
					}
				}
			}
		}
}

inline void initial(){
	memset(r,0,sizeof(r));
	memset(f,100,sizeof(f));
	memset(nv,0,sizeof(nv));
}

int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	t=0;
	while(scanf("%d%d%d%d%d%d",&n,&m,&sx,&sy,&ex,&ey)&&n&&m){
		t++;
		initial();
		for(i=1;i<=n;i++){
			for(j=1;j<m;j++)scanf("%d",&x),nv[i][j][2]=nv[i][j+1][4]=x;
			for(j=1;j<=m&&i!=n;j++)scanf("%d",&x),nv[i][j][3]=nv[i+1][j][1]=x;
		}
		f[sx][sy]=0;
		Rollout(sx,sy,0);
		printf("Case %d: ",t);
		if(f[ex][ey]>1600000000)puts("Impossible");
		else printf("%d\n",f[ex][ey]);
	}
}

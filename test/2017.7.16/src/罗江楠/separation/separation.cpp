#include <bits/stdc++.h>
#define N 55
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
map<string, int> mp;
string name1, name2;
int dis[N][N], cnt;
int fa[N]; // van♂darkholme
int find(int x){return fa[x]==x?x:fa[x]=find(fa[x]);}
void merge(int x, int y){x=find(x);y=find(y);if(x==y)return;fa[x]=y;}
ifstream fin("separation.in");
ofstream fout("separation.out");
int work(int n, int m){
	mp.clear(); cnt = 0;
	memset(dis, 63, sizeof(int)*N*N);
	for(int i = 1; i <= n; i++) fa[i] = i, dis[i][i] = 0;
	for(int i = 1; i <= m; i++){
		fin >> name1 >> name2;
		int id1 = mp[name1];
		if(!id1) id1 = mp[name1] = ++cnt;
		int id2 = mp[name2];
		if(!id2) id2 = mp[name2] = ++cnt;
		merge(id1, id2);
		dis[id1][id2] = dis[id2][id1] = 1;
	}
	int res = 0;
	for(int i = 1; i <= n; i++) if(fa[i] == i) res++;
	if(res > 1) return -1;
	for(int k = 1; k <= n; k++)
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				dis[i][j] = min(dis[i][j], dis[i][k]+dis[k][j]);
	int ans = 0;
	for(int i = 1; i <= n; i++)
		for(int j = i+1; j <= n; j++)
			ans = max(ans, dis[i][j]);
	return ans;
}
int main(){
	// File("separation");
	int n, m, ca = 0;
	while(fin >> n >> m){
		if(!n && !m) break;
		int res = work(n, m);
		if(~res) fout << "Network " << ++ca << ": " << res << endl;
		else fout << "Network " << ++ca << ": DISCONNECTED" << endl;
	}
	return 0;
}

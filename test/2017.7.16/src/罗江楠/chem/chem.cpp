#include <bits/stdc++.h>
#define ll long long
#define N 1000
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
using namespace std;
int calc(char a, char b = 'a'-1){return (a-'A')*27+b-'a'+2;}
struct node{
	int val[N];
	node(){memset(val, 0, sizeof(int)*N);}
	friend bool operator == (const node &a, const node &b){
		for(int i = 0; i < N; i++)
			if(a.val[i] != b.val[i]) return 0;
		return 1;
	}
}ans;
node add(node a, node b){
	node c;
	for(int i = 0; i < N; i++)
		c.val[i] = a.val[i]+b.val[i];
	return c;
}
node mul(node a, int b){
	node c;
	for(int i = 0; i < N; i++)
		c.val[i] = a.val[i]*b;
	return c;
}
char str[N], strr[N];
node calcys(int L, int R){
	// printf("calcys: %d %d\n", L, R);
	node res;
	if(L > R) return res;
	int l = L;
	while(l <= R){
		if(str[l] >= 'A' && str[l] <= 'Z'){
			int id = str[l+1] >= 'a' && str[l+1] <= 'z' ? calc(str[l], str[l+1]) : calc(str[l]);
			l += str[l+1] >= 'a' && str[l+1] <= 'z' ? 2 : 1;
			int x = 0;
			while(str[l] >= '0' && str[l] <= '9')
				x = (x<<1)+(x<<3)+str[l++]-'0';
			if(!x) x = 1;
			res.val[id] += x;
		}
		else{ l++; int tl = l, oo = 1;
			for(;oo;tl++) if(str[tl] == '(') oo++; else if(str[tl] == ')') oo--;
			int ss = tl, x = 0;
			while(str[ss] >= '0' && str[ss] <= '9')
				x = (x<<1)+(x<<3)+str[ss++]-'0';
			if(!x) x = 1;
			res = add(res, mul(calcys(l, tl-2), x));
			l = ss;
		}
	}
	return res/*.print()*/;
}
node calcfzs(int l, int r){
	// printf("calcfzs: %d %d\n", l, r);
	if(l > r) return node();
	int x = 0;
	while(str[l] <= '9' && str[l] >= '0')
		x = (x<<1)+(x<<3)+str[l++]-'0';
	if(!x) x = 1;
	return mul(calcys(l, r), x)/*.print()*/;
}
node calc(){
	int len = strlen(str+1);
	int l = 1, r = 1;
	node res;
	while(r <= len){
		while(r <= len && str[r] != '+') r++;
		res = add(res, calcfzs(l, r-1));
		l = ++r;
	}
	return res;
}
int main(){
	File("chem");
	scanf("%s", str+1);
	ans = calc();
	// ans.print();
	memcpy(strr, str, sizeof(char)*N);
	int n; scanf("%d", &n);
	while(n--){
		memset(str, 0, sizeof(char)*N);
		scanf("%s", str+1);
		node res = calc();
		printf("%s%c=%s\n", strr+1, ans == res ? '=' : '!', str+1);
	}
}
// 王保佑我200+
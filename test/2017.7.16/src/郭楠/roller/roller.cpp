#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
#define N 105
#define p 80000
using namespace std;
int read()
{int t=0;char c;
   c=getchar();
   while(!(c>='0' && c<='9')) c=getchar();
   while(c>='0' && c<='9')
     {
     	t=t*10+c-48;
     	c=getchar();
     }
  return t;
}
int n,m,qx,qy,zx,zy,z;
int s[80005][4],t,w;
long long f[N][N][4][2],MAX;
int dx[4]={-1,0,1,0};
int dy[4]={0,1,0,-1};
int road[N][N][4];
bool ifin[N][N][4][2];
int main()
{int i,j,k,i1=0;
    freopen("roller.in","r",stdin);
    freopen("roller.out","w",stdout);
	n=read();m=read();qx=read();qy=read();zx=read();zy=read();
	while(!(n==0 && m==0))
	  {
	  	i1++;
		for(i=1;i<=n;i++)
	  	  for(j=1;j<=m;j++)
	  	    for(k=0;k<4;k++) f[i][j][k][0]=f[i][j][k][1]=10000000000000ll;
	  	memset(ifin,false,sizeof(ifin));
	  	for(i=1;i<n;i++)
	  	  {
	  	  	for(j=1;j<m;j++)
	  	  	  {
	  	  	  	z=read();road[i][j][1]=road[i][j+1][3]=z;
	  	  	  }
	  	  	for(j=1;j<=m;j++)
			  {
			  	z=read();road[i][j][2]=road[i+1][j][0]=z;
			  } 
	  	  }
	  	for(i=1;i<m;i++)
	  	  {
	  	  	z=read();road[n][i][1]=road[n][i+1][3]=z;
	  	  }
	  	t=0;w=4;
		s[1][0]=qx;s[1][1]=qy;s[1][2]=0;s[1][3]=0;
		s[2][0]=qx;s[2][1]=qy;s[2][2]=1;s[2][3]=0;
		s[3][0]=qx;s[3][1]=qy;s[3][2]=2;s[3][3]=0;
		s[4][0]=qx;s[4][1]=qy;s[4][2]=3;s[4][3]=0;
		f[qx][qy][0][0]=0;ifin[qx][qy][0][0]=true;
		f[qx][qy][1][0]=0;ifin[qx][qy][1][0]=true;
		f[qx][qy][2][0]=0;ifin[qx][qy][2][0]=true;
		f[qx][qy][3][0]=0;ifin[qx][qy][3][0]=true;
		while(t<w)
		 {
		 	t++;
		 	int x=s[t%p][0],y=s[t%p][1],fx=s[t%p][2],js=s[t%p][3];
		 	if(js==0)
		 	  {
		 	  	for(i=-1;i<=1;i++)
		 	  	  if(road[x][y][(fx+i+4)%4])
		 	  	  {
                     k=(fx+i+4)%4;
                     int xx=x+dx[k],yy=y+dy[k];
                     if(xx>=1 && xx<=n && yy>=1 && yy<=m)
                       {
                       	if(f[xx][yy][k][0]>f[x][y][fx][js]+road[x][y][k]*2)
                       	 {
                       	 	f[xx][yy][k][0]=f[x][y][fx][js]+road[x][y][k]*2;
                       	 	if(!ifin[xx][yy][k][0])
                       	 	  {
                       	 	  	w++;
                       	 	  	s[w%p][0]=xx;s[w%p][1]=yy;s[w%p][2]=k;s[w%p][3]=0;
                       	 	  	ifin[xx][yy][k][0]=true;
                       	 	  }
                       	 }
                       	if(k==fx && (!(x==qx && y==qy)))
                       	  if(f[xx][yy][k][1]>f[x][y][fx][js]+road[x][y][k])
                       	    {
                       	    	f[xx][yy][k][1]=f[x][y][fx][js]+road[x][y][k];
                       	    	if(!ifin[xx][yy][k][1])
                       	    	 {
                       	    	 	w++;
                       	    	 	s[w%p][0]=xx;s[w%p][1]=yy;s[w%p][2]=k;s[w%p][3]=1;
                       	    	 	ifin[xx][yy][k][1]=true;
                       	    	 }
                       	    }
                       }
		 	  	  }
		 	  }
		 	if(js==1)
		 	 {
		 	 	if(road[x][y][fx])
		 	 	 {
		 	 	 	int xx=x+dx[fx],yy=y+dy[fx];
		 	 	 	if(xx>=1 && xx<=n && y>=1 && y<=m)
		 	 	 	 {
		 	 	 	 	if(f[xx][yy][fx][0]>f[x][y][fx][js]+road[x][y][fx]*2)
		 	 	 	 	 {
		 	 	 	 	 	f[xx][yy][fx][0]=f[x][y][fx][js]+road[x][y][fx]*2;
		 	 	 	 	 	if(!ifin[xx][yy][fx][0])
		 	 	 	 	 	 {
		 	 	 	 	 	 	w++;
		 	 	 	 	 	 	s[w%p][0]=xx;s[w%p][1]=yy;s[w%p][2]=fx;s[w%p][3]=0;
		 	 	 	 	 	 	ifin[xx][yy][fx][0]=true;
		 	 	 	 	 	 }
		 	 	 	 	 }
		 	 	 	 	if(f[xx][yy][fx][1]>f[x][y][fx][1]+road[x][y][fx])
		 	 	 	 	  {
		 	 	 	 	  	f[xx][yy][fx][1]=f[x][y][fx][1]+road[x][y][fx];
		 	 	 	 	  	if(!ifin[xx][yy][fx][1])
		 	 	 	 	  	  {
		 	 	 	 	  	  	w++;
		 	 	 	 	  	  	s[w%p][0]=xx;s[w%p][1]=yy;s[w%p][2]=fx;s[w%p][3]=1;
		 	 	 	 	  	  	ifin[xx][yy][fx][1]=true;
		 	 	 	 	  	  }
		 	 	 	 	  }
		 	 	 	 }
		 	 	 }
		 	 }
		   ifin[x][y][fx][js]=false;
		 }
		 MAX=10000000000000ll;
		 for(i=0;i<4;i++) MAX=min(MAX,f[zx][zy][i][0]);
		 if(MAX==10000000000000ll) printf("Case %d: Impossible\n",i1);
		                    else printf("Case %d: %d\n",i1,MAX);
		 n=read();m=read();qx=read();qy=read();zx=read();zy=read();
	  }
}


#include <cstdio>
#include <algorithm>
#include <cstring>
#include <iostream>
#include <map>
#include <string>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
map<string,int>mp[2],p;
map<string,bool>jzq;
int cnt=0;
string k[110];
string s,t,zhan[110];
int d[110];
inline bool work(string s,int b){
	s=s+"+";int len=s.size();
	mp[b].clear();int top=0;
	string rp="";int x=0,jz=1;bool q=1,di=0;
	for(int i=0;i<len;i++){
		if(s[i]=='+'){
			if(di){
				if(q)jz*=x;
				else{
					if(zhan[top][0]!=')')d[top]*=x;
					else{
						d[top]=0;int j=top-1;
						for(;zhan[j][0]!='('||d[j]==0;j--){
							if(zhan[j][0]!='('&&zhan[j][0]!=')')d[j]*=x;
						}d[j]=0;
					}
				}
				di=0;x=0;
			}
			if(rp!=""){
				zhan[++top]=rp;if(!b&&!jzq[rp])k[++cnt]=rp,jzq[rp]=1;else if(b&&!jzq[rp])return 0;
				d[top]=jz;rp="";
			}
			for(;top;top--)if(zhan[top][0]!='('&&zhan[top][0]!=')')mp[b][zhan[top]]+=d[top];
			x=0;jz=1;q=1;di=0;rp="";
			continue;
		}
		if(s[i]>='0'&&s[i]<='9'){
			if(rp!=""){
				zhan[++top]=rp;if(!b&&!jzq[rp])k[++cnt]=rp,jzq[rp]=1;else if(b&&!jzq[rp])return 0;
				d[top]=jz;rp="";
			}
			di=1;x=x*10+s[i]-'0';
		}
		else{
			if(di){
				if(q)jz*=x;
				else{
					if(zhan[top][0]!=')')d[top]*=x;
					else{
						d[top]=0;int j=top-1;
						for(;zhan[j][0]!='('||d[j]==0;j--){
							if(zhan[j][0]!='('&&zhan[j][0]!=')')d[j]*=x;
						}d[j]=0;
					}
				}
				di=0;x=0;
			}
			if(s[i]>='A'&&s[i]<='Z'){
				if(rp!=""){
					zhan[++top]=rp;if(!b&&!jzq[rp])k[++cnt]=rp,jzq[rp]=1;else if(b&&!jzq[rp])return 0;
					d[top]=jz;rp="";
				}
				q=0;rp=s[i];
			}
			if(s[i]>='a'&&s[i]<='z')rp=rp+s[i];
			if(s[i]=='('){
				if(rp!=""){
					zhan[++top]=rp;if(!b&&!jzq[rp])k[++cnt]=rp,jzq[rp]=1;else if(b&&!jzq[rp])return 0;
					d[top]=jz;rp="";
				}
				zhan[++top]="(",d[top]=1;
			}
			if(s[i]==')'){
				if(rp!=""){
					zhan[++top]=rp;if(!b&&!jzq[rp])k[++cnt]=rp,jzq[rp]=1;else if(b&&!jzq[rp])return 0;
					d[top]=jz;rp="";
				}
				if(s[i+1]<'0'||s[i+1]>'9')di=1,x=1;
				zhan[++top]=")";d[top]=1;
			}
		}
	}
	return 1;
}
int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	cin>>s;int n=read();
	work(s,0);
	for(int i=1;i<=n;i++){
		cin>>t;bool flag=1;
		if(!work(t,1))flag=0;
		else for(int j=1;j<=cnt;j++)if(mp[0][k[j]]!=mp[1][k[j]]){flag=0;break;}
		cout<<s<<(flag?"==":"!=")<<t<<endl;
	}
	return 0;
}
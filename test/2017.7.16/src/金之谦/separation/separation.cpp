#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
map<string,int>mp;
int f[110][110];
int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	string a,b;int T=0;
	int n,m;for(n=read(),m=read();n>0;n=read(),m=read()){
		mp.clear();int cnt=0;
		for(int i=1;i<=n;++i){
			for(int j=1;j<=n;++j)f[i][j]=1e9;
			f[i][i]=0;
		}
		for(int i=1;i<=m;++i){
			cin>>a>>b;
			if(!mp[a])mp[a]=++cnt;if(!mp[b])mp[b]=++cnt;
			int x=mp[a],y=mp[b];
			f[x][y]=f[y][x]=1;
		}
		for(int k=1;k<=n;++k)
			for(int i=1;i<=n;++i)
				for(int j=1;j<=n;++j)f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
		int ans=0,flag=0;
		for(int i=1;i<=n;++i)
			for(int j=i+1;j<=n;++j)if(f[i][j]==1e9)flag=1;
			else ans=max(ans,f[i][j]);
		T++;printf("Network %d: ",T);
		if(flag)puts("DISCONNECTED");
		else writeln(ans);
	}
	return 0;
}
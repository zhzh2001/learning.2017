#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x<0)putchar('-'),x=-x;if(x>=10)write(x/10);putchar(x%10+'0');} 
inline void writeln(int x){write(x);puts("");}
const int dx[4]={1,0,-1,0};
const int dy[4]={0,1,0,-1};
struct ppap{int x,y,f,b,d;};
bool operator <(ppap a,ppap b){return a.d>b.d;}
priority_queue<ppap>q;
bool vis[110][110][4][2];
int dist[110][110][4][2],n,m,sx,sy,ex,ey,d[110][110][4];
inline void dij(){
	memset(vis,0,sizeof vis);ppap now,rp;
	for(int i=1;i<=n;++i)for(int j=1;j<=m;j++)for(int k=0;k<4;k++)dist[i][j][k][0]=dist[i][j][k][1]=1e8;
	for(int i=0;i<4;++i){
		dist[sx][sy][i][0]=dist[sx][sy][i][1]=0,vis[sx][sy][i][0]=vis[sx][sy][i][1]=1;
		int xx=sx+dx[i],yy=sy+dy[i];if(xx<1||yy<1||xx>n||yy>m)continue;
		dist[xx][yy][i][1]=d[sx][sy][i]*2;rp.x=xx;rp.y=yy;rp.f=i;rp.b=1;rp.d=dist[xx][yy][i][1];
		q.push(rp);
	}
	while(!q.empty()){
		now=q.top();q.pop();if(vis[now.x][now.y][now.f][now.b])continue;
		vis[now.x][now.y][now.f][now.b]=1;
		int xx=now.x+dx[now.f],yy=now.y+dy[now.f];
		if(xx>0&&yy>0&&xx<=n&&yy<=m){
			if(!vis[xx][yy][now.f][0]&&dist[xx][yy][now.f][0]>dist[now.x][now.y][now.f][now.b]+d[now.x][now.y][now.f]){
				dist[xx][yy][now.f][0]=dist[now.x][now.y][now.f][now.b]+d[now.x][now.y][now.f];
				rp.x=xx;rp.y=yy;rp.f=now.f;rp.b=0;rp.d=dist[xx][yy][now.f][0];q.push(rp);
			}
			if(!vis[xx][yy][now.f][1]&&dist[xx][yy][now.f][1]>dist[now.x][now.y][now.f][now.b]+d[now.x][now.y][now.f]*2){
				dist[xx][yy][now.f][1]=dist[now.x][now.y][now.f][now.b]+d[now.x][now.y][now.f]*2;
				rp.x=xx;rp.y=yy;rp.f=now.f;rp.b=1;rp.d=dist[xx][yy][now.f][1];q.push(rp);
			}
		}
		if(now.b){
			for(int i=0;i<4;++i)if((i+now.f)&1){
				int xx=now.x+dx[i],yy=now.y+dy[i];
				if(xx<1||yy<1||xx>n||yy>m||vis[xx][yy][i][1])continue;
				if(dist[xx][yy][i][1]>dist[now.x][now.y][now.f][1]+d[now.x][now.y][i]*2){
					dist[xx][yy][i][1]=dist[now.x][now.y][now.f][1]+d[now.x][now.y][i]*2;
					rp.x=xx;rp.y=yy;rp.f=i;rp.b=1;rp.d=dist[xx][yy][i][1];q.push(rp);
				}
			}
		}
	}
}
int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	int T=0;
	for(n=read(),m=read(),sx=read(),sy=read(),ex=read(),ey=read();n>0;n=read(),m=read(),sx=read(),sy=read(),ex=read(),ey=read()){
		for(int i=1;i<=n;++i)for(int j=1;j<=m;j++)for(int k=0;k<4;k++)d[i][j][k]=1e8;
		int p=2*n-1;for(int i=1;i<=p;++i){
			int rp=(i+1)>>1;
			if(i&1){for(int j=1;j<m;j++){int x=read();d[rp][j][1]=d[rp][j+1][3]=(x)?x:1e8;}}
			else{for(int j=1;j<=m;j++){int x=read();d[rp][j][0]=d[rp+1][j][2]=(x)?x:1e8;}}
		}
		dij();
		int ans=1e8;T++;
		for(int i=0;i<4;++i)ans=min(ans,dist[ex][ey][i][1]);
		printf("Case %d: ",T);
		if(ans<1e8)writeln(ans);else puts("Impossible");
	}
	return 0;
}
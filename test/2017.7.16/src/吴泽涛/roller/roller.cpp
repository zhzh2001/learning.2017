#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>
//#include <cmath>
const int PX[4]={-1,1,0,0};
const int PY[4]={0,0,-1,1};
struct node{
	int to,next,len;
}edge[320005];
int tot=0,head[80005],n,m,x1,y1,x2,y2;
int front,rear,Q[100005],dis[80005],E[105][105][4];
bool vis[80005];
void makedge(int u,int v,int t){
	edge[++tot].to=v;
	edge[tot].len=t;
	edge[tot].next=head[u];
	head[u]=tot;
}
int P(int a,int b,int c,int d){
	return ((a-1)*m+(b-1))*8+d*4+c;
}
void push(int x){
	Q[rear]=x;
	vis[x]=1;
	rear=(rear+1)%100000;
}
void spfa(int x1,int y1){
	front=0,rear=0;
	int u,v;
	memset(dis,63,sizeof(dis));
	memset(vis,0,sizeof(vis));
	push(P(x1,y1,0,0)),dis[P(x1,y1,0,0)]=0;
	push(P(x1,y1,1,0)),dis[P(x1,y1,1,0)]=0;
	push(P(x1,y1,2,0)),dis[P(x1,y1,2,0)]=0;
	push(P(x1,y1,3,0)),dis[P(x1,y1,3,0)]=0;
	while (front!=rear){
		u=Q[front]; vis[u]=0;
		for (int i=head[u];i;i=edge[i].next){
			v=edge[i].to;
			if (dis[u]+edge[i].len<dis[v]){
				dis[v]=dis[u]+edge[i].len;
				if (!vis[v]) push(v);
			}
		}
		front=(front+1)%100000;
	}
}
int main(){
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	int Case=0;
	while (~scanf("%d%d%d%d%d%d",&n,&m,&x1,&y1,&x2,&y2)){
		if (!n && !m && !x1 && !y1 && !x2 && !y2) break;
		memset(E,0,sizeof(E));
		for (int i=1,X1,Y1,X2,Y2;i<2*n;i++)
		if (i&1)
			for (int j=1;j<m;j++){
				X1=(i+1)>>1,Y1=j,X2=(i+1)>>1,Y2=j+1;
				scanf("%d",&E[X1][Y1][3]);
				E[X2][Y2][2]=E[X1][Y1][3];
			}
		else
			for (int j=1;j<=m;j++){
				X1=i>>1,Y1=j,X2=(i>>1)+1,Y2=j;
				scanf("%d",&E[X1][Y1][1]);
				E[X2][Y2][0]=E[X1][Y1][1];
			}
		for (int i=1,i1,i2;i<=n;i++)
		for (int j=1,j1,j2;j<=m;j++){
		if (i==x1 && j==y1) continue;
		for (int k=0;k<4;k++){
			if (E[i][j][k]==0) continue;
			i1=i+PX[k]; j1=j+PY[k];
			if (i1==x1 && j1==y1)
				makedge(P(i1,j1,k,0),P(i,j,k,1),E[i][j][k]*2);
			else if (i==x2 && j==y2)
				for (int l=0;l<4;l++){
					if (E[i1][j1][l]==0) continue;
					i2=i1+PX[l]; j2=j1+PY[l];
					if (i2==i && j2==j) continue;
					for (int l1=0;l1<2;l1++)
						if (k==l)
							makedge(P(i1,j1,l,l1),P(i,j,k,1),E[i][j][k]*2);
						else
							if (l1==0)
								makedge(P(i1,j1,l,l1),P(i,j,k,1),E[i][j][k]*2+E[i1][j1][l]);
							else
								makedge(P(i1,j1,l,l1),P(i,j,k,1),E[i][j][k]*2);
					}
			else
				for (int l=0;l<4;l++){
					if (E[i1][j1][l]==0) continue;
					i2=i1+PX[l]; j2=j1+PY[l];
					if (i2==i && j2==j) continue;
					for (int l1=0;l1<2;l1++)
						if (k==l)
							makedge(P(i1,j1,l,l1),P(i,j,k,0),E[i][j][k]);
						else
							if (l1==0)
								makedge(P(i1,j1,l,l1),P(i,j,k,1),E[i][j][k]*2+E[i2][j2][l]);
							else
								makedge(P(i1,j1,l,l1),P(i,j,k,1),E[i][j][k]*2);
				}
		}
		}
		spfa(x1,y1);
		int ans=1e8;
		for (int i=0;i<4;i++)
			if (dis[P(x2,y2,i,1)]<ans)
				ans=dis[P(x2,y2,i,1)];
		if (ans<1e8) printf("Case %d: %d\n",++Case,ans);
		else printf("Case %d: Impossible\n",++Case);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=50+5;
map<string,int>F;
int f[N][N];
int top,n,m,x,y,ans,num;
string s;
int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	while(scanf("%d%d",&n,&m)!=-1){
		if(n==0&&m==0)return 0;
		top=ans=0;num++;
		F.clear();
		memset(f,63,sizeof f);
		for(int i=1;i<=m;i++){
			cin>>s;
			if(!F[s])F[s]=++top;
			x=F[s];
			cin>>s;
			if(!F[s])F[s]=++top;
			y=F[s];
			f[x][y]=f[y][x]=1;
		}
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				if(i!=j)ans=max(ans,f[i][j]);
		if(ans<10000)printf("Network %d: %d\n",num,ans);else printf("Network %d: DISCONNECTED\n",num);
	}
}

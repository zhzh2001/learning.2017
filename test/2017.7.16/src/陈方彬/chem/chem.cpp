#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e3+5;
map<string,int>F;
string s,A,B;
int sa[N],a[N],t[N],v[N],nxt[N],q[N];
int le,top,n,tot;
int getc(int &l,int r){
	string o="";
	o+=s[l++];
	if(l<=r&&'a'<=s[l]&&s[l]<='z')o+=s[l++];
	if(!F[o]&&top<100)F[o]=++top;
	return F[o];
}
int getv(int &l,int r){
	int ans=0;
	while(l<=r&&isdigit(s[l]))ans=ans*10+s[l++]-48;
	return max(ans,1);
}
void dfs(int l,int r,int xx){
	while(r>=l)
		if(s[l]!='('){
			t[++tot]=getc(l,r);
			v[tot]=getv(l,r)*xx;
		}else{
			int x=l+1,y;
			y=nxt[l]-1;l=nxt[l]+1;
			int vv=getv(l,r);
			dfs(x,y,xx*vv);
		}
}
void find(int l,int r,int p){
	memset(t,0,sizeof t);
	memset(v,0,sizeof v);
	tot=0;
	int xx=getv(l,r);
	dfs(l,r,xx);
	if(p==2)for(int i=1;i<=tot;i++)a[t[i]]+=v[i];
	else for(int i=1;i<=tot;i++)sa[t[i]]+=v[i];
}
int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	cin>>s; A=s;s+='+';
	int l=0,T=0;
	le=s.length();
	for(int i=0;i<le;i++)
		if(s[i]=='(')q[++T]=i;else
		if(s[i]==')')nxt[q[T--]]=i;
	for(int i=0;i<le;i++)
		if(s[i]=='+'){
			find(l,i-1,1);
			l=i+1;
		}
	scanf("%d",&n);
	while(n--){
		memset(a,0,sizeof a);
		memset(nxt,0,sizeof nxt);
		cin>>s; B=s;s+='+';
		int l=0,T=0;
		le=s.length();
		for(int i=0;i<le;i++)
			if(s[i]=='(')q[++T]=i;else
			if(s[i]==')')nxt[q[T--]]=i;
		for(int i=0;i<le;i++)
			if(s[i]=='+'){
				find(l,i-1,2);
				l=i+1;
			}
		bool ans=1;
		for(int i=1;i<=top;i++)if(a[i]!=sa[i])ans=0;
		cout<<A;if(ans)cout<<"==";else cout<<"!=";cout<<B<<endl;
	}
}


#include<iostream>
#include<cstdio>
#include<queue>
#include<vector>
using namespace std;
int n,m;
int ques=0;
int x1,x2,y1,y2;
int dx[]={-1,0,1,0};
int dy[]={0,-1,0,1};
int v[110][110][4];
int ans;
int read(){
	int tot=0;
	int f=1;
	char c=getchar();
	while(!isdigit(c) && c!='-')
		c=getchar();
	if(c=='-'){
		f=-1;
		c=getchar();
	}
	while(isdigit(c)){
		tot=tot*10+c-'0';
		c=getchar();
	}
	return tot*f;
}
struct map{
	int x,y;
};
void spfa(){
	int d[110][110];
	bool vis[110][110];
	int dir[110][110];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++){
			d[i][j]=0;
			vis[i][j]=false;
		}
	queue <map> q;
	d[x1][y1]=0;
	for(int i=0;i<4;i++)
		if(v[x1][y1][i]){
			int xx=x1+dx[i];
			int yy=y1+dy[i];
			d[xx][yy]=2*v[x1][y1][i];
			dir[xx][yy]=i;
			vis[xx][yy]=true;
			q.push((map){xx,yy});
		}
	while(!q.empty()){	
		int x=q.front().x;
		int y=q.front().y;
		q.pop();
		vis[x][y]=false;
		int lx=x+dx[(dir[x][y]+2)%4];
		int ly=y+dy[(dir[x][y]+2)%4];
		for(int i=0;i<4;++i)
			if(v[x][y][i]!=0){
				int nd=d[x][y]+v[x][y][i];
				int nx=x+dx[i],ny=y+dy[i];
				cout<<nx<<" "<<ny<<"\n";
				if(nx==x1 && ny==y1)
					continue;
				if(i!=dir[x][y] && dir[x][y]!=0){
					nd=nd+v[x][y][i];
					if(lx!=x1 || ly!=y1)
						nd+=v[x][y][(dir[x][y]+2)%4];
					if(dir[lx][ly]!=dir[x][y])
						nd-=v[x][y][(dir[x][y]+2)%4];
				}
				if(nd<d[nx][ny] || d[nx][ny]==0 ){
					d[nx][ny]=nd;
					dir[nx][ny]=i;
					if(!vis[nx][ny]){
						vis[nx][ny]=true;
						q.push((map){nx,ny});
					}
				}
			}
	}
	ans=1000000;
	for(int i=0;i<4;i++)
		if(v[x2][y2][i]){
			int nx=x2+dx[i];
			int ny=y2+dy[i];
			if(d[nx][ny]!=0)
				ans=min(ans,d[nx][ny]+v[x2][y2][i]*2);
		}
	return;
}
int main(){
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	while(scanf("%d%d",&n,&m) && n!=0){
		++ques;
		x1=read();
		y1=read();
		x2=read();
		y2=read();
		for(int i=1;i<=n;++i)
			for(int j=1;j<=m;++j)
				for(int ii=0;ii<4;++ii)
						v[i][j][ii]=0;
		for(int i=1;i<2*n;++i){
			if(i&1){
				int hh=i/2+1;
				for(int j=1;j<m;++j){
					int d=read();
					v[hh][j][3]=
					v[hh][j+1][1]=d;	
				}
			}
			else{
				int hh=i/2;
				for(int j=1;j<=m;++j){
					int d=read();
					v[hh][j][2]=
					v[hh+1][j][0]=d;
				}
			}
		}	
		spfa();
		printf("Case %d: ",ques);
		if(ans!=1000000)
			printf("%d\n",ans);
		else 
			printf("Impossible\n");
	}
	return 0;
}


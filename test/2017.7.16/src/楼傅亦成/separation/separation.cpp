#include<bits/stdc++.h>
using namespace std;
const int INF=100001;
int n,m;
int p;
int pp=0;
map <string,bool> q;
map <string,int>  g;
int d[51][51];
string s1,s2;
int main(){
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	while(scanf("%d%d",&n,&m) && n!=0){
		p=0;
		pp++;
		for(int i=1;i<=n;++i)
			for(int j=1;j<=n;++j)
				d[i][j]=INF;
		for(int i=1;i<=m;++i){
			cin>>s1>>s2;
			if(!q[s1]){
				q[s1]=true;
				g[s1]=++p;
			}
			if(!q[s2]){
				q[s2]=true;
				g[s2]=++p;
			}
			d[g[s1]][g[s2]]=d[g[s2]][g[s1]]=1;
		}
		for(int k=1;k<=n;++k)
			for(int i=1;i<=n;++i)
				if(i!=k)
					for(int j=1;j<=n;j++)
						if(i!=j && j!=k)
							d[i][j]=min(d[i][j],d[i][k]+d[k][j]);
		int ans=0;
		bool fla=true;
		for(int i=1;i<=n && fla;++i)
			for(int j=i+1;j<=n && fla;++j)
				if(d[i][j]==INF){
					fla=false;
					break;
				}
				else
					ans=max(ans,d[i][j]);
		printf("Network %d: ",pp);
		if(fla)
			printf("%d\n",ans);
		else 
			printf("DISCONNECTED\n");
	}
	return 0;
}

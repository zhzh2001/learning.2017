#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <map>
using namespace std;
map<string,int> mp;
string a,b;
int INDEX,dis[50][50],mx,used[50],pt,cas,n,m;
void dfs(int p){
	pt++;
	used[p]=1;
	for(int i=1;i<=n;i++){
		if(p==i) continue;
		if(dis[p][i]==1&&!used[i]) dfs(i);
	}
}
int main(){
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	while(1){
		scanf("%d %d",&n,&m);
		if(!n&&!m) break;
		cas++;
		mp.clear();
		INDEX=0,mx=-1,pt=0;
		memset(dis,0x3f,sizeof(dis));
		memset(used,0,sizeof(used));
		for(int i=1;i<=n;i++) dis[i][i]=0;
		for(int i=1;i<=m;i++){
			cin>>a>>b;
			if(!mp[a]) mp[a]=++INDEX;
			if(!mp[b]) mp[b]=++INDEX;
			dis[mp[a]][mp[b]]=1;
			dis[mp[b]][mp[a]]=1;
		}
		dfs(1);
		if(pt!=n) printf("Network %d: DISCONNECTED\n",cas);
		else{
			for(int k=1;k<=n;k++)
				for(int i=1;i<=n;i++)
					for(int j=1;j<=n;j++)
						if(dis[i][j]==-1||dis[i][k]+dis[k][j]<dis[i][j]) dis[i][j]=dis[i][k]+dis[k][j];
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++){
					if(i==j) continue;
					if(dis[i][j]!=0x3f3f3f3f&&dis[i][j]>mx) mx=dis[i][j];
				}
			printf("Network %d: %d\n",cas,mx);
		}
	}
	return 0;
}

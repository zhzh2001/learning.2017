#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <map>
#define T 10000000
#define D 1000000
#define R 1000
using namespace std;
map<int,int> ind;
int uind[100010],INDEX,n,m,ex,ey,sx,sy,r[120][120],c[120][120];
int point[400010],yest[400010],last[100010],val[400010],total;
int head,tail,q[400010],used[100010],dis[100010],cas;
void adde(int f,int t,int v){
	//cout<<f<<"->"<<t<<" "<<v<<endl;
	if(!ind.count(f)) ind[f]=++INDEX,uind[INDEX]=f;
	if(!ind.count(t)) ind[t]=++INDEX,uind[INDEX]=t;
	int ff=ind[f],tt=ind[t];
	//cout<<"ff="<<ff<<" tt="<<tt<<endl;
	point[++total]=tt;
	val[total]=v;
	yest[total]=last[ff];
	last[ff]=total;
}
void build(){
	for(int i=1;i<=n;i++){
		for(int j=1;j<n;j++){
			if(r[i][j]!=0){
				adde(0*T+1*D+i*R+j,0*T+2*D+i*R+j+1,r[i][j]*2);
				adde(0*T+2*D+i*R+j,1*T+2*D+i*R+j+1,r[i][j]);
				adde(0*T+3*D+i*R+j,0*T+2*D+i*R+j+1,r[i][j]*2);
				adde(1*T+1*D+i*R+j,0*T+2*D+i*R+j+1,r[i][j]*2+c[i-1][j]);
				adde(1*T+2*D+i*R+j,1*T+2*D+i*R+j+1,r[i][j]);
				adde(1*T+3*D+i*R+j,0*T+2*D+i*R+j+1,r[i][j]*2+c[i][j]);
				if(i==ex&&j==ey) adde(5*D+i*R+j,2*D+i*R+j+1,r[i][j]*2);
			}
		}
	}
	for(int i=1;i<=n;i++){
		for(int j=n-1;j>=1;j--){
			if(r[i][j]!=0){
				adde(0*T+1*D+i*R+j+1,0*T+4*D+i*R+j,r[i][j]*2);
				adde(0*T+3*D+i*R+j+1,0*T+4*D+i*R+j,r[i][j]*2);
				adde(0*T+4*D+i*R+j+1,1*T+4*D+i*R+j,r[i][j]);
				adde(1*T+1*D+i*R+j+1,0*T+4*D+i*R+j,r[i][j]*2+c[i-1][j+1]);
				adde(1*T+3*D+i*R+j+1,0*T+4*D+i*R+j,r[i][j]*2+c[i][j+1]);
				adde(1*T+4*D+i*R+j+1,1*T+4*D+i*R+j,r[i][j]);
				if(i==ex&&(j+1)==ey) adde(5*D+i*R+j+1,4*D+i*R+j,r[i][j]*2);
			}
		}
	}
	for(int j=1;j<=n;j++){
		for(int i=1;i<n;i++){
			if(c[i][j]!=0){
				adde(0*T+1*D+i*R+j,1*T+1*D+(i+1)*R+j,c[i][j]);
				adde(0*T+2*D+i*R+j,0*T+1*D+(i+1)*R+j,c[i][j]*2);
				adde(0*T+3*D+i*R+j,0+T+1*D+(i+1)*R+j,c[i][j]*2);
				adde(1*T+1*D+i*R+j,1*T+1*D+(i+1)*R+j,c[i][j]);
				adde(1*T+2*D+i*R+j,0*T+1*D+(i+1)*R+j,c[i][j]*2+r[i][j-1]);
				adde(1*T+3*D+i*R+j,0+T+1*D+(i+1)*R+j,c[i][j]*2+r[i][j]);
				if(i==ex&&j==ey) adde(5*D+i*R+j,1*D+(i+1)*R+j,c[i][j]*2);
			}
		}
	}
	for(int j=1;j<=n;j++){
		for(int i=n-1;i>=1;i--){
			if(c[i][j]!=0){
				adde(0*T+2*D+(i+1)*R+j,0*T+3*D+i*R+j,c[i][j]*2);
				adde(0*T+3*D+(i+1)*R+j,0*T+3*D+i*R+j,c[i][j]);
				adde(0*T+4*D+(i+1)*R+j,1*T+3*D+i*R+j,c[i][j]*2);
				adde(1*T+2*D+(i+1)*R+j,0*T+3*D+i*R+j,c[i][j]*2+r[i+1][j-1]);
				adde(1*T+3*D+(i+1)*R+j,0*T+3*D+i*R+j,c[i][j]);
				adde(1*T+4*D+(i+1)*R+j,1*T+3*D+i*R+j,c[i][j]*2+r[i+1][j]);
				if((i+1)==ex&&j==ey) adde(5*D+(i+1)*R+j,1*D+i*R+j,c[i][j]*2);
			}
		}
	}
}
void spfa(int p){
	head=1,tail=1,q[1]=p;
	dis[p]=0;
	used[p]=1;
	int f,t,v;
	//cout<<1<<endl;
	int k1,k2,d1,d2,tx,ty,fx,fy;
	while(head<=tail){
		f=q[head];
		//cout<<f<<" "<<uind[f]<<endl;
		//cout<<"dis="<<dis[f]<<endl;
		fx=(uind[f]/1000)%1000;
		fy=(uind[f])%1000;
		for(int i=last[f];i;i=yest[i]){
			t=point[i];
			v=val[i];
			tx=(uind[t]/1000)%1000;
			ty=uind[t]%1000;
			k1=uind[t]/T;
			//cout<<fx<<","<<fy<<"to"<<tx<<","<<ty<<endl;
			if(tx==sx&&ty==sy&&k1==1){
				//cout<<"--------------------------"<<endl;
				d1=(uind[t]/D)%10;
				if(d1==1) v+=c[tx-1][ty];
				else if(d1==2) v+=r[tx][ty-1];
				else if(d1==3) v+=c[tx][ty];
				else if(d1==4) v+=r[tx][ty];
			}
			if(dis[t]==-1||dis[f]+v<dis[t]){
				dis[t]=dis[f]+v;
				if(!used[t]){
					used[t]=1;
					q[++tail]=t;
				}
			}
		}
		head++;
		used[f]=0;
	}
}
int main(){
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	while(1){
		cas++;
		scanf("%d %d %d %d %d %d",&n,&m,&ex,&ey,&sx,&sy);
		if(n==0&&m==0) break;
		total=0;
		memset(last,0,sizeof(last));
		ind.clear();
		INDEX=0;
		memset(uind,0,sizeof(last));
		memset(used,0,sizeof(used));
		for(int i=1;i<=n;i++){
			for(int j=1;j<n;j++) scanf("%d",&r[i][j]);
			if(i!=n) for(int j=1;j<=n;j++) scanf("%d",&c[i][j]);
		}
		//cout<<2<<endl;
		build();
		memset(dis,-1,sizeof(dis));
		spfa(ind[5*D+ex*R+ey]);
		int ans=(1<<31)-1;
		if(dis[ind[T+1*D+sx*R+sy]]!=-1&&ans>dis[ind[T+1*D+sx*R+sy]]) ans=dis[ind[T+1*D+sx*R+sy]];
		if(dis[ind[T+2*D+sx*R+sy]]!=-1&&ans>dis[ind[T+2*D+sx*R+sy]]) ans=dis[ind[T+2*D+sx*R+sy]];
		if(dis[ind[T+3*D+sx*R+sy]]!=-1&&ans>dis[ind[T+3*D+sx*R+sy]]) ans=dis[ind[T+3*D+sx*R+sy]];
		if(dis[ind[T+4*D+sx*R+sy]]!=-1&&ans>dis[ind[T+4*D+sx*R+sy]]) ans=dis[ind[T+4*D+sx*R+sy]];
		if(dis[ind[1*D+sx*R+sy]]!=-1&&ans>dis[ind[1*D+sx*R+sy]]) ans=dis[ind[1*D+sx*R+sy]];
		if(dis[ind[2*D+sx*R+sy]]!=-1&&ans>dis[ind[2*D+sx*R+sy]]) ans=dis[ind[2*D+sx*R+sy]];
		if(dis[ind[3*D+sx*R+sy]]!=-1&&ans>dis[ind[3*D+sx*R+sy]]) ans=dis[ind[3*D+sx*R+sy]];
		if(dis[ind[4*D+sx*R+sy]]!=-1&&ans>dis[ind[4*D+sx*R+sy]]) ans=dis[ind[4*D+sx*R+sy]];
		if(ans==(1<<31)-1) printf("Case %d: Impossible\n",cas);
		else printf("Case %d: %d\n",cas,ans);
	}
	return 0;
}

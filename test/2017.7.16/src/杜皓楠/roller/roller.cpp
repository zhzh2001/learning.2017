#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>

using namespace std;
template<typename T>
void read(T& A)
{
	char r;
	bool f=false;
	for(r=getchar(); (r<48||r>57)&&r!='-'; r=getchar());
	if(r=='-')
	{
		f=true;
		r=getchar();
	}
	for(A=0; r>=48&&r<=57; r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b)
{
	return a>b?a:b;
}
template<typename T>
T Min(const T& a,const T& b)
{
	return a<b?a:b;
}
template<typename T>
T Swap(const T& a,const T& b)
{
	T t=a;
	a=b;
	b=t;
}

const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;
const int maxn=103;
const int maxm=103;
int n,m,tx,ty,TT=0;
const int dx[]= {0,-1,0,1,0};
const int dy[]= {0,0,1,0,-1};
inline int redir(int dirr)
{
	if(dirr&1)return  4-dirr;
	else return 6-dirr;
}
inline int left(int dirr)
{
	--dirr;
	if(dirr==0)dirr=4;
	return dirr;
}
inline int right(int dirr)
{
	++dirr;
	if(dirr==5)dirr=1;
	return dirr;
}
struct node
{
	int x,y,dir,d;
	bool turned;
	int last;
	node() {x=y=dir=d=turned=last=0;}
	node(int xx,int yy,int dirr):x(xx),y(yy),dir(dirr) 
	{d=turned=last=0;}
} s;

const int maxnode=10000004;
template<typename T>
struct QUE
{
	T ary[maxnode];
	int f,t;
	QUE()
	{
		f=t=1;
	}
	void clear()
	{
		f=t=1;
	}
	inline bool empty()
	{
		return f==t;
	}
	void push(const T& w)
	{
		ary[t++]=w;
	}
	void pop()
	{
		if(t-f)++f;
	}
	T front()
	{
		return ary[f];
	}
};

int row[maxn][maxm];
int col[maxn][maxm];
QUE<node> q;
//queue<node> q;
void input()
{
	read(s.x);
	read(s.y);
	read(tx);
	read(ty);
	for(int i=1; i<=2*n-1; ++i)
	{
		if(i&1)
		{
			for(int j=1; j<=m-1; ++j)
				read(row[(i+1)/2][j]);
		}
		else
		{
			for(int j=1; j<=m; ++j)
				read(col[i/2][j]);
		}
	}
}
inline bool can(int x,int y)
{
	return x>=1&&x<=n&&y>=1&&y<=m;
}
int mina[maxn][maxm];
void bfs()
{
	for(int i=1;i<=n;++i)
	{
		for(int j=1;j<=m;++j)
		{
			mina[i][j]=inf;
		}
	}
	q.clear();
	s.d=0;
	s.turned=true;
	s.last=0;
	q.push(s);
	mina[s.x][s.y]=0;
	while(!q.empty())
	{
		node fr=q.front();
		q.pop();
		for(int i=1; i<=4; ++i)
		{
			if(i!=redir(fr.dir))
			{
				node tmp(fr.x+dx[i],fr.y+dy[i],i);
				if(can(tmp.x,tmp.y))
				{
					int cost;
					if(i%2==0)
						cost=row[tmp.x][Min(tmp.y,fr.y)];
					else
						cost=col[Min(tmp.x,fr.x)][tmp.y];
					if(cost==0)continue;
					int dd;
					if(tmp.dir!=fr.dir&&fr.turned==false)
					{
						tmp.turned=true;
						cost*=2;
						dd=cost+fr.last;	
					}
					else if((tmp.dir!=fr.dir&&fr.turned==true)||fr.d==0||(tmp.x==tx&&tmp.y==ty))
					{
						tmp.turned=true;
						cost*=2;
						dd=cost;
					}
					else
					{
						tmp.turned=false;
						dd=cost;
					}
					tmp.last=cost;
					tmp.d=fr.d+dd;
					if(tmp.d<mina[tmp.x][tmp.y])
					{
						mina[tmp.x][tmp.y]=tmp.d;
						if(tmp.x!=tx||tmp.y!=ty)
							q.push(tmp);
					}
				}
			}
		}
	}
}
int main()
{

	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	while((~scanf("%d%d",&n,&m))&&(n&&m))
	{
	
		input();
		bfs();	
		
		printf("Case %d: ",++TT);
		if(mina[tx][ty]==inf)
			printf("Impossible\n");
		else printf("%d\n",mina[tx][ty]);
	}

	return 0;
}

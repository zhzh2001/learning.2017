#include <fstream>
#include <algorithm>
#include <cstring>
#include <queue>
using namespace std;
ifstream fin("roller.in");
ofstream fout("roller.out");
const int N = 105, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1}, INF = 0x3f3f3f3f;
int row[N][N], col[N][N], d[N][N][4][2];
bool inQ[N][N][4][2];
struct node
{
	int x, y, dir, turn;
	node(int x, int y, int dir, int turn) : x(x), y(y), dir(dir), turn(turn) {}
};
int main()
{
	int n, m, sx, sy, tx, ty, cas = 0;
	while (fin >> n >> m >> sx >> sy >> tx >> ty && n)
	{
		for (int i = 1; i <= n; i++)
		{
			for (int j = 1; j < m; j++)
				fin >> row[i][j];
			if (i < n)
				for (int j = 1; j <= m; j++)
					fin >> col[i][j];
		}
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
				memset(d[i][j], 0x3f, sizeof(d[i][j]));
		queue<node> Q;
		for (int i = 0; i < 4; i++)
		{
			d[sx][sy][i][1] = 0;
			Q.push(node(sx, sy, i, 1));
			inQ[sx][sy][i][1] = true;
		}
		while (!Q.empty())
		{
			node k = Q.front();
			Q.pop();
			inQ[k.x][k.y][k.dir][k.turn] = false;
			int dist = d[k.x][k.y][k.dir][k.turn], w;
			k.x += dx[k.dir];
			k.y += dy[k.dir];
			if (k.x <= 0 || k.x > n || k.y <= 0 || k.y > m)
				continue;
			switch (k.dir)
			{
			case 0:
				w = col[k.x][k.y];
				break;
			case 1:
				w = col[k.x - 1][k.y];
				break;
			case 2:
				w = row[k.x][k.y];
				break;
			case 3:
				w = row[k.x][k.y - 1];
				break;
			}
			if (w == 0)
				continue;
			for (int i = 0; i < 4; i++)
				if (k.dir == i)
				{
					if (dist + w * (1 + k.turn) < d[k.x][k.y][i][0])
					{
						d[k.x][k.y][i][0] = dist + w * (1 + k.turn);
						if (!inQ[k.x][k.y][i][0])
						{
							Q.push(node(k.x, k.y, i, 0));
							inQ[k.x][k.y][i][0] = true;
						}
					}
				}
				else
				{
					if (dist + w * 2 < d[k.x][k.y][i][1])
					{
						d[k.x][k.y][i][1] = dist + w * 2;
						if (!inQ[k.x][k.y][i][1])
						{
							Q.push(node(k.x, k.y, i, 1));
							inQ[k.x][k.y][i][1] = true;
						}
					}
				}
		}
		int ans = INF;
		for (int i = 0; i < 4; i++)
			ans = min(ans, d[tx][ty][i][1]);
		fout << "Case " << ++cas << ": ";
		if (ans == INF)
			fout << "Impossible\n";
		else
			fout << ans << endl;
	}
	return 0;
}
#include <fstream>
using namespace std;
ifstream fin("roller.in");
ofstream fout("roller.out");
const int N = 105, INF = 0x3f3f3f3f;
int n, m, sx, sy, tx, ty, mat[N * 2][N], ans;
bool vis[N][N];
void dfs(int x, int y, int dist, int pred, bool turning)
{
	if (dist > ans)
		return;
	if (x == tx && y == ty)
	{
		ans = dist;
		return;
	}
	vis[x][y] = true;
	int extra;
	switch (pred)
	{
	case 1:
		extra = mat[x * 2][y];
		break;
	case 2:
		extra = mat[(x - 1) * 2][y];
		break;
	case 3:
		extra = mat[x * 2 - 1][y];
		break;
	case 4:
		extra = mat[x * 2 - 1][y - 1];
		break;
	}
	if (x > 1 && mat[(x - 1) * 2][y] && !vis[x - 1][y])
		if (pred != 1)
			if (turning || pred == 0 || (x - 1 == tx && y == ty))
				dfs(x - 1, y, dist + 2 * mat[(x - 1) * 2][y], 1, turning);
			else
				dfs(x - 1, y, dist + 2 * mat[(x - 1) * 2][y] + extra, 1, true);
		else if (x - 1 == tx && y == ty)
			dfs(x - 1, y, dist + 2 * mat[(x - 1) * 2][y], 1, false);
		else
			dfs(x - 1, y, dist + mat[(x - 1) * 2][y], 1, false);
	if (x < n && mat[x * 2][y] && !vis[x + 1][y])
		if (pred != 2)
			if (turning || pred == 0 || (x + 1 == tx && y == ty))
				dfs(x + 1, y, dist + 2 * mat[x * 2][y], 2, turning);
			else
				dfs(x + 1, y, dist + 2 * mat[x * 2][y] + extra, 2, true);
		else if (x + 1 == tx && y == ty)
			dfs(x + 1, y, dist + 2 * mat[x * 2][y], 2, false);
		else
			dfs(x + 1, y, dist + mat[x * 2][y], 2, false);
	if (y > 1 && mat[x * 2 - 1][y - 1] && !vis[x][y - 1])
		if (pred != 3)
			if (turning || pred == 0 || (x == tx && y - 1 == ty))
				dfs(x, y - 1, dist + 2 * mat[x * 2 - 1][y - 1], 3, turning);
			else
				dfs(x, y - 1, dist + 2 * mat[x * 2 - 1][y - 1] + extra, 3, true);
		else if (x == tx && y - 1 == ty)
			dfs(x, y - 1, dist + 2 * mat[x * 2 - 1][y - 1], 3, false);
		else
			dfs(x, y - 1, dist + mat[x * 2 - 1][y - 1], 3, false);
	if (y < m && mat[x * 2 - 1][y] && !vis[x][y + 1])
		if (pred != 4)
			if (turning || pred == 0 || (x == tx && y + 1 == ty))
				dfs(x, y + 1, dist + 2 * mat[x * 2 - 1][y], 4, turning);
			else
				dfs(x, y + 1, dist + 2 * mat[x * 2 - 1][y] + extra, 4, true);
		else if (x == tx && y + 1 == ty)
			dfs(x, y + 1, dist + 2 * mat[x * 2 - 1][y], 4, false);
		else
			dfs(x, y + 1, dist + mat[x * 2 - 1][y], 4, false);
	vis[x][y] = false;
}
int main()
{
	int cas = 0;
	while (fin >> n >> m >> sx >> sy >> tx >> ty && n)
	{
		for (int i = 1; i < 2 * n; i++)
			if (i & 1)
				for (int j = 1; j < m; j++)
					fin >> mat[i][j];
			else
				for (int j = 1; j <= m; j++)
					fin >> mat[i][j];
		ans = INF;
		dfs(sx, sy, 0, 0, false);
		fout << "Case " << ++cas << ": ";
		if (ans == INF)
			fout << "Impossible\n";
		else
			fout << ans << endl;
	}
	return 0;
}
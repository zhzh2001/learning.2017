#include <fstream>
#include <string>
#include <map>
#include <algorithm>
using namespace std;
ifstream fin("separation.in");
ofstream fout("separation.out");
const int N = 55, INF = 0x3f3f3f3f;
int mat[N][N];
int main()
{
	int n, m, cas = 0;
	while (fin >> n >> m && n)
	{
		map<string, int> M;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				mat[i][j] = i == j ? 0 : INF;
		int id=0;
		while (m--)
		{
			string su, sv;
			fin >> su >> sv;
			if (M.find(su) == M.end())
				M[su] = ++id;
			if (M.find(sv) == M.end())
				M[sv] = ++id;
			int u = M[su], v = M[sv];
			mat[u][v] = mat[v][u] = 1;
		}
		for (int k = 1; k <= n; k++)
			for (int i = 1; i <= n; i++)
				for (int j = 1; j <= n; j++)
					mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
		int ans = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++)
				ans = max(ans, mat[i][j]);
		fout << "Network " << ++cas << ": ";
		if (ans == INF)
			fout << "DISCONNECTED\n";
		else
			fout << ans << endl;
	}
	return 0;
}
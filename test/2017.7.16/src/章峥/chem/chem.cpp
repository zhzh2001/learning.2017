#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
#include <stack>
using namespace std;
ifstream fin("chem.in");
ofstream fout("chem.out");
const int A = 26, L = 100;
int cnt[A][A + 1], a[A][A + 1], Right[L];
string s;
int formala(int l, int r);
int chemfor(int l, int r, int c);
int element(int l, int r, int c);
int chemele(int l, int r, int c);
int parsenum(int l, int r, int &res);

int formala(int l, int r)
{
	while (l <= r)
	{
		int c;
		l = parsenum(l, r, c);
		int t;
		for (t = l; t <= r && s[t] != '+'; t++)
			;
		chemfor(l, t - 1, c);
		l = t + 1;
	}
	return l;
}

int chemfor(int l, int r, int c)
{
	while (l <= r)
	{
		int t = element(l, r, 0), c2;
		int t2 = parsenum(t, r, c2);
		element(l, t - 1, c * c2);
		l = t2;
	}
	return l;
}

int element(int l, int r, int c)
{
	if (s[l] == '(')
	{
		if (c)
			chemfor(l + 1, Right[l] - 1, c);
		return Right[l] + 1;
	}
	return chemele(l, r, c);
}

int chemele(int l, int r, int c)
{
	if (l == r || !islower(s[l + 1]))
	{
		cnt[s[l] - 'A'][26] += c;
		return l + 1;
	}
	cnt[s[l] - 'A'][s[l + 1] - 'a'] += c;
	return l + 2;
}

int parsenum(int l, int r, int &res)
{
	res = 0;
	for (; l <= r && isdigit(s[l]); l++)
		res = res * 10 + s[l] - '0';
	if (res == 0)
		res = 1;
	return l;
}

int main()
{
	fin >> s;
	stack<int> S;
	for (int i = 0; i < (int)s.length(); i++)
		if (s[i] == '(')
			S.push(i);
		else if (s[i] == ')')
		{
			Right[S.top()] = i;
			S.pop();
		}
	formala(0, s.length() - 1);
	memcpy(a, cnt, sizeof(a));
	string t = s;
	int n;
	fin >> n;
	while (n--)
	{
		fin >> s;
		stack<int> S;
		for (int i = 0; i < (int)s.length(); i++)
			if (s[i] == '(')
				S.push(i);
			else if (s[i] == ')')
			{
				Right[S.top()] = i;
				S.pop();
			}
		memset(cnt, 0, sizeof(cnt));
		formala(0, s.length() - 1);
		if (memcmp(cnt, a, sizeof(cnt)))
			fout << t << "!=" << s << endl;
		else
			fout << t << "==" << s << endl;
	}
	return 0;
}
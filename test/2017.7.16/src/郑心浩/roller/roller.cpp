#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

struct edge
{
	int to,next,v,di;
}e[700005];

struct node
{
	int x,di,la,lc;
	void mnode(int xi,int d,int l,int lii){x=xi;di=d;la=l;lc=lii;}
}q[700005];

int n,m,sx,sy,tx,ty;
int head[10005],cnt;
int dis[40005];
bool inq[500005];

void addedge(int xi,int yi,int wi,int fang)
{
	e[++cnt].to=yi;e[cnt].next=head[xi];head[xi]=cnt;e[cnt].v=wi;e[cnt].di=fang;
}

void SPFA(int s)
{
	int i,v,j,t=(tx-1)*m+ty;
	node now;
	for(i=1;i<=3*n*m;i++)
		dis[i]=0x3f3f3f3f,inq[i]=0;
	int top=0,dep=1;
	dis[s*2+0]=0;dis[s*2+1]=0;q[0].mnode(s,0,0,0);inq[s*2]=1;inq[s*2+1]=1;
	while(top<dep)
	{
		now=q[top++];
		inq[now.x*2+now.di]=0;
		for(j=head[now.x];j!=-1;j=e[j].next)
		{
			v=e[j].to;
			if(e[j].di==now.di&&now.x!=s&&v!=t)
			{
				if(dis[now.x*2+now.di]+e[j].v<dis[v*2+now.di])
				{
					dis[v*2+now.di]=dis[now.x*2+now.di]+e[j].v;
					if(!inq[v*2+now.di])
					{
						q[dep].x=v;
						q[dep].la=e[j].v;
						q[dep].lc=0;
						q[dep++].di=now.di;
						inq[v*2+now.di]=1;
					}
				}
			} 
			else if(now.x==s)
			{
				if(dis[now.x*2+now.di]+2*e[j].v<dis[v*2+e[j].di])
				{
					dis[v*2+e[j].di]=dis[now.x*2+now.di]+2*e[j].v+now.la;
					if(!inq[v*2+now.di^1])
					{
						q[dep].x=v;
						q[dep].la=e[j].v;
						q[dep].lc=1;
						q[dep++].di=e[j].di;
						inq[v*2+e[j].di]=1;
					}
				}
			}
			else if(v==t)
			{
				if(dis[now.x*2+now.di]+2*e[j].v<dis[v*2+now.di^1])
				{
					dis[v*2+now.di^1]=dis[now.x*2+now.di]+2*e[j].v;
					if(!inq[v*2+now.di^1])
					{
						q[dep].x=v;
						q[dep].la=e[j].v;
						q[dep].lc=1;
						q[dep++].di=now.di^1;
						inq[v*2+now.di^1]=1;
					}
				}
			}
			else
			{
				if(dis[now.x*2+now.di]+2*e[j].v<dis[v*2+now.di^1])
				{
					if(now.lc==0)
						dis[v*2+now.di^1]=dis[now.x*2+now.di]+2*e[j].v+now.la;
					else
						dis[v*2+now.di^1]=dis[now.x*2+now.di]+2*e[j].v;
					if(!inq[v*2+now.di^1])
					{
						q[dep].x=v;
						q[dep].la=e[j].v;
						q[dep].lc=1;
						q[dep++].di=now.di^1;
						inq[v*2+now.di^1]=1;
					}
				}
			}
		}
	} 
	if(dis[t*2]==0x3f3f3f3f&&dis[t*2+1]==0x3f3f3f3f)
		printf("Impossible\n");
	else
		printf("%d\n",min(dis[t*2],dis[t*2+1])); 
}

int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	int i,j,heng,zong,x,y,cas=0;
	while(1)
	{
		n=read(),m=read(),sx=read(),sy=read(),tx=read(),ty=read();
		if(n==0&&m==0&&sx==0&&sy==0&&tx==0&&ty==0)
			break;
		heng=0;cnt=0;
		for(i=1;i<=n*m;i++)
			head[i]=-1;
		for(i=1;i<=2*n-1;i++)
			if(i&1)
			{
				heng++;
				for(j=1;j<=m-1;j++)
				{
					scanf("%d",&x);
					if(x!=0)
					{
						y=(heng-1)*m+j;
						addedge(y,y+1,x,0);
						addedge(y+1,y,x,0);
					}
				}
			}
			else
			{
				for(j=1;j<=m;j++)
				{
					scanf("%d",&x);
					if(x!=0)
					{
						y=(heng-1)*m+j;
						addedge(y,y+m,x,1);
						addedge(y+m,y,x,1);
					}
				}			
			} 
		/*for(i=1;i<=n*m;i++)
		{
			printf("%d(%d,%d): ",i,i/m+1,i%m);
			for(j=head[i];j!=-1;j=e[j].next)
				printf("->((%d,%d) %d %d) ",e[j].to/m+1,e[j].to%m,e[j].v,e[j].di);
			printf("\n");
		}*/
		printf("Case %d: ",++cas);
		SPFA((sx-1)*m+sy); 
	}
	return 0;
}
 

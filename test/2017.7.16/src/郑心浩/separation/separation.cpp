#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <map>
using namespace std;

inline int read()
{
	int x=0,f=1;
	char c=getchar();
	while(c>'9'||c<'0'){if(c=='-') f=-1;c=getchar();}
	while(c>='0'&&c<='9'){x=x*10+c-48;c=getchar();}
	return x*f;
}

map<string,int> mp[55];
int n,r,cnt;
int w[55][55];
string s1,s2;

int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);	
	int i,j,k,x,y,cas=0,on1,ans;
	while(scanf("%d %d",&n,&r)&&(!(n==0&&r==0)))
	{
		cnt=0;
		cas++;
		for(i=1;i<=n;i++)
			for(j=1;j<=n;j++)
				if(i!=j)
					w[i][j]=0x3f3f3f3f;
		for(i=1;i<=r;i++)
		{
			cin>>s1;
			cin>>s2;
			if(mp[cas][s1]==0)
				mp[cas][s1]=++cnt;
			if(mp[cas][s2]==0)
				mp[cas][s2]=++cnt;
			x=mp[cas][s1];
			y=mp[cas][s2];
			w[x][y]=1;
			w[y][x]=1;
		}
		for(k=1;k<=n;k++)
			for(i=1;i<=n;i++)
				for(j=1;j<=n;j++)
					w[i][j]=min(w[i][j],w[i][k]+w[k][j]);
		on1=0;
		ans=0;
		for(i=1;i<=n;i++)
			for(j=1;j<=n;j++)
				if(w[i][j]==0x3f3f3f3f)
				{
					on1=1;
					break;
				} 
				else
					ans=max(ans,w[i][j]);
		printf("Network %d: ",cas);
		if(on1==1)
			printf("DISCONNECTED\n");
		else
			printf("%d\n",ans);
	}
	return 0;
}


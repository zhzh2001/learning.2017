#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <map>
using namespace std;

map<string,int> mp,mp2[15];
string si;
string s[15];
int n,L,li[15];
string sta;

void count(int x,int y)
{
	int top=0,i,f=1,t,ld=0,lz,j,xi;
	string nd,zd;
	if(si[x]>='0'&&si[x]<='9')
		f=si[x]-48;
	else
		sta[++top]=si[x];
	for(i=x+1;i<=y;i++)
		if(si[i]>='0'&&si[i]<='9')
		{
			if(top!=0)
			{
				t=si[i]-48;
				ld=0;
				while(char(sta[top])>='a'&&char(sta[top])<='z')
					nd[ld++]=sta[top],top--;
				nd[ld++]=sta[top];
				top--;
				lz=0;
				for(j=ld-1;j>=0;j--)
					zd[lz++]=nd[j];
				mp[zd]+=t*f; 
				xi=mp[zd];
			}
		}
		else
			sta[++top]=si[i];
	while(top!=0)
	{
		t=1;
		ld=0;
		while(char(sta[top])>='a'&&char(sta[top])<='z')
			nd[ld++]=sta[top],top--;
		nd[ld++]=sta[top];
		top--;
		lz=0;
		for(j=ld-1;j>=0;j--)
			zd[lz++]=nd[j];
		mp[zd]+=t*f;
		xi=mp[zd]; 
	} 
}

void count1(int x,int y,int num)
{
	int top=0,i,f=1,t,ld=0,lz,j,xi;
	string nd,zd;
	if(s[num][x]>='0'&&s[num][x]<='9')
		f=si[x]-48;
	else
		sta[++top]=s[num][x];
	for(i=x+1;i<=y;i++)
		if(s[num][i]>='0'&&s[num][i]<='9')
		{
			if(top!=0)
			{
				t=s[num][i]-48;
				ld=0;
				while(char(sta[top])>='a'&&char(sta[top])<='z')
					nd[ld++]=sta[top],top--;
				nd[ld++]=sta[top];
				top--;
				lz=0;
				for(j=ld-1;j>=0;j--)
					zd[lz++]=nd[j];
				mp2[num][zd]+=t*f; 
			}
		}
		else
			sta[++top]=s[num][i];
	while(top!=0)
	{
		t=1;
		ld=0;
		while(char(sta[top])>='a'&&char(sta[top])<='z')
			nd[ld++]=sta[top],top--;
		nd[ld++]=sta[top];
		top--;
		lz=0;
		for(j=ld-1;j>=0;j--)
			zd[lz++]=nd[j];
		mp2[num][zd]+=t*f; 
	} 
}

bool judge(int x,int y,int num)
{
	int top=0,ld=0,lz,i,j;
	string nd,zd;
	for(i=x+1;i<=y;i++)
		sta[++top]=s[num][i];
	while(top!=0)
	{
		ld=0;
		while(char(sta[top])>='a'&&char(sta[top])<='z')
			nd[ld++]=sta[top],top--;
		nd[ld++]=sta[top];
		top--;
		lz=0;
		for(j=ld-1;j>=0;j--)
			zd[lz++]=nd[j];
		if(mp2[num][zd]!=mp[zd])
			return 1; 
	} 
	return 0;
}

int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	int i,j;
	cin>>si;
	L=si.size(); 
	scanf("%d",&n);
	for(i=1;i<=n;i++)
	{
		cin>>s[i];
		li[i]=s[i].size();
	}
	int la=-1;
	for(i=0;i<L;i++)
		if(si[i]=='+')
		{
			count(la+1,i-1);
			la=i;
		}
	int on1;
	for(i=1;i<=n;i++)
	{
		la=-1;
		for(j=0;j<li[i];j++)
			if(si[j]=='+')
			{
				count1(la+1,j-1,i);
				la=j;
			}
		on1=0;
		la=-1;
		for(j=0;j<li[i];j++)
			if(si[j]=='+')
			{
				if(judge(la+1,j-1,i))
				{
					on1=1;
					break;
				}
				la=j;
			}
		if(on1==0)
			cout<<si<<"=="<<s[i]<<endl;
		else
			cout<<si<<"!="<<s[i]<<endl;
	}
	return 0;
} 


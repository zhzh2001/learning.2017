#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<string>
#include<map>
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
int P, R, T, cnt, mx;
int g[65][65];

int main(){
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	ios::sync_with_stdio(false);
	while(cin >> P >> R && P && R){
		
		memset(g, 0x3f, sizeof(g));
		for(int i=1; i<=P; i++) g[i][i] = 0;
		map<string, int> hash;
		cnt = 0; mx = -1;
		
		++T; cout << "Network " << T << ": ";
		
		for(int i=1; i<=R; i++){
			string str1, str2;
			cin >> str1 >> str2;
			int id1 = hash[str1], id2 = hash[str2];
			
			if(id1 == 0) {hash[str1] = ++cnt, id1 = cnt;}
			if(id2 == 0) {hash[str2] = ++cnt, id2 = cnt;}
			
			g[id1][id2] = g[id2][id1] = 1;
		}
		
		if(cnt < P) {cout << "DISCONNECTED" << endl; continue;}
		
		for(int k=1; k<=P; k++)
		for(int i=1; i<=P; i++)
		for(int j=1; j<=P; j++)
			g[i][j] = min(g[i][j], g[i][k]+g[k][j]);
		
		for(int i=1; i<=P; i++)
			for(int j=1; j<=P; j++)
				mx = max(mx, g[i][j]);
		
		if(mx >= INF) {cout << "DISCONNECTED" << endl; continue;}
		
		cout << mx << endl;
	} return 0;
}
			
			
			
			
			
			
			
			
			
			
			
			
			

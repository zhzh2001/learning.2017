#include<cstdio>
#include<iostream>
#include<algorithm>
#include<cstring>
#include<queue>
using namespace std;
typedef long long LL;
const int INF = 0x3f3f3f3f;
const int maxn = 105;

int dx[6] = {0,-1,1,0,0};
int dy[6] = {0,0,0,-1,1};
// UP DOWN LEFT RIGHT;
#define inmap(a,b) (1<=a&&a<=n&&1<=b&&b<=m)
int line[maxn][maxn], col[maxn][maxn];
int n,m,x1,y1,x2,y2,T,mn;
int g[2][5][maxn][maxn];

struct state{	
	int x,y,d,dis,v;
	state(int _x,int _y, int _d, int _dis, int _v){
		x=_x, y=_y, d=_d, dis=_dis, v=_v;
	}
};

inline int getedge(int x,int y,int i){
	if(i == 1) return col[x-1][y];
	else if(i == 2) return col[x][y];
	else if(i == 3) return line[x][y-1];
	else if(i == 4) return line[x][y];
}

int main(){
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	while(~scanf("%d%d%d%d%d%d",&n,&m,&x1,&y1,&x2,&y2) && n && m){
		++T; printf("Case %d: ", T);
		if(x1 == x2 && y1 == y2){printf("0\n"); continue;}
		
		queue<state> q;
		
		for(int i=1; i<n; i++){
			for(int j=1; j<m; j++)
				scanf("%d",&line[i][j]);
			for(int j=1; j<=m; j++)
				scanf("%d",&col[i][j]);
		}
		for(int j=1; j<m; j++)
			scanf("%d",&line[n][j]);
			
		memset(g, 0x3f, sizeof(g));
		
		for(int i=1; i<=4; i++) q.push(state(x1,y1,i,0,0));
		
		while(!q.empty()){
			state head = q.front(); q.pop();
			int x = head.x, y = head.y, d = head.d, v = head.v;
			if(head.dis>=g[v][d][x][y]) continue;
			if(x == x2 && y == y2) {g[v][d][x][y] = min(g[v][d][x][y], head.dis); continue;}
			
			if(v == 1){
				int xx = x+dx[d], yy = y+dy[d];
				if(!inmap(xx,yy)) continue;
				int dis = getedge(x,y,d);
				if(dis == 0) continue;
				
				if(g[0][d][xx][yy] > head.dis+dis*2)
					q.push(state(xx,yy,d,head.dis+dis*2,0));
				if(!(xx==x2 && yy==y2) && g[1][d][xx][yy] > head.dis+dis)
					q.push(state(xx,yy,d,head.dis+dis,1));
			} else {
				for(int i=1; i<=4; i++){
					int xx = x+dx[i], yy = y+dy[i];
					if(!inmap(xx,yy)) continue;
					
					int dis = getedge(x,y,i);
					if(dis == 0) continue;
					
					if(i == d){
						if(g[0][d][xx][yy] > head.dis+dis*2)
							q.push(state(xx,yy,d,head.dis+dis*2,0));
						if(!(xx==x2 && yy==y2) && !(x==x1 && y==y1) && g[1][d][xx][yy] > head.dis+dis)
							q.push(state(xx,yy,d,head.dis+dis,1));
					} else {
						if(g[0][i][xx][yy] > head.dis+dis*2)
							q.push(state(xx,yy,i,head.dis+dis*2,0));
					}
				}
			}
			g[v][d][x][y] = head.dis;
		}
		mn = INF;
		for(int i=1; i<=4; i++)
			mn = min(mn, g[0][i][x2][y2]);
		if(mn >= INF) printf("Impossible\n");
		else printf("%d\n",mn);
	}
	return 0;		
}

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <cmath>
int p,r,f[105][105],cnt,len[105];
char s1[105],s2[105];
char name[105][105];
int max(int a,int b){
	if (a>b) return a; else return b;
}
int find(char s[]){
	int L=strlen(s);
	bool F;
	for (int i=0;i<cnt;i++){
		if (len[i]!=L) continue;
		F=true;
		for (int j=0;name[i][j];j++)
		if (s[j]!=name[i][j]){
			F=false;
			break;
		}
		if (F) return i;
	}
	for (int i=0;i<=L;i++) name[cnt][i]=s[i];
	len[cnt]=L;
	return cnt++;
}
int main(){
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	int Case=0;
	while (~scanf("%d%d",&p,&r)){
		if (!p && !r) break;
		memset(f,1,sizeof(f));
		cnt=0;
		for (int i=1,bh1,bh2;i<=r;i++){
			scanf("%s%s",&s1,&s2);
			bh1=find(s1),bh2=find(s2);
			f[bh1][bh2]=f[bh2][bh1]=1;
		}
		for (int k=0;k<p;k++)
		for (int i=0;i<p;i++)
		for (int j=0;j<p;j++)
			if (f[i][k]+f[k][j]<f[i][j])
				f[i][j]=f[i][k]+f[k][j];
		int ans=0;
		for (int i=0;i<p-1;i++)
		for (int j=i+1;j<p;j++)
			ans=max(ans,f[i][j]);
		if (ans>1e6) printf("Network %d: DISCONNECTED\n",++Case);
		else printf("Network %d: %d\n",++Case,ans);
	}
	fclose(stdin);
	fclose(stdout);
	return 0;
}

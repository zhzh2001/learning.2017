#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=100007;
int q[10000007];
int head[N],cnt,inq[N],f[N];
int n,m,X1,X2,Y1,Y2;
struct node{
	int u,v,w,next;
}e[N<<1];
inline void ins(int u,int v,int w){
	e[++cnt]=(node){
		u,v,w,head[u]
	}; head[u]=cnt;
}
inline void insert(int u,int v,int w){
	ins(u,v,w);
	ins(v,u,w);
}


int main(){
	// say hello

	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);

	scanf("%d%d%d%d%d%d",&n,&m,&X1,&Y1,&X2,&Y2);
	for (int T=1;n!=0&&m!=0;T++){
		memset(head,0,sizeof(head));
		cnt=0;
		int S1=(X1-1)*m+Y1;int T1=(X2-1)*m+Y2;
		int S2,S3,S4,T2,T3,T4;
		S4=S1*4;S3=S4-1;S2=S3-1;S1=S2-1;
		T4=T1*4;T3=T4-1;T2=T3-1;T1=T2-1;
		For(i,1,n){
			For(j,1,m-1){
				int w=read();if (w==0) continue;
				int U1=(i-1)*m+j;int V1=(i-1)*m+j+1;
				int U2,U3,U4,V2,V3,V4;
				U4=U1*4;U3=U4-1;U2=U3-1;U1=U2-1;
				V4=V1*4;V3=V4-1;V2=V3-1;V1=V2-1;
				insert(U4,V2,w*2);insert(U4,V3,w*2);insert(U4,V4,w*2);
				if (U3!=S3&&U3!=T3&&V3!=S3&&V3!=T3) insert(U3,V3,w);
				else insert(U3,V3,w*2);
				insert(U3,V2,w*2);
			}
			if (i!=n) For(j,1,m){
				int w=read();if (w==0) continue;
				int U1=(i-1)*m+j;int V1=i*m+j;
				int U2,U3,U4,V2,V3,V4;
				U4=U1*4;U3=U4-1;U2=U3-1;U1=U2-1;
				V4=V1*4;V3=V4-1;V2=V3-1;V1=V2-1;
				insert(U2,V1,w*2);insert(U2,V2,w*2);insert(U2,V4,w*2);
				if (U1!=S1&&U1!=T1&&V1!=S1&&V1!=T1) insert(U1,V1,w);		
				else insert(U1,V1,w*2);
				insert(U1,V4,w*2);
			}
		}
		int l=0,r=4;q[1]=S1,q[2]=S2,q[3]=S3,q[4]=S4;
		memset(inq,0,sizeof(inq));
		memset(f,127/3,sizeof(f));
		inq[S1]=1,inq[S2]=1,inq[S3]=1,inq[S4]=1;
		f[S1]=0,f[S2]=0,f[S3]=0,f[S4]=0;
		while (l<r){
			l++;int x=q[l];
		//	printf("%d %d\n",x,f[x]);
			for (int i=head[x];i;i=e[i].next){
				int y=e[i].v;
				if (f[y]>f[x]+e[i].w){
					f[y]=f[x]+e[i].w;
					if (!inq[y]) q[++r]=y;
				}
			}
			inq[x]=0;
		}
		int ans=min(f[T1],min(f[T2],min(f[T3],f[T4])));
		if (ans==f[0]) printf("Case %d: Impossible\n",T);
		else printf("Case %d: %d\n",T,ans);

		n=0,m=0;
		scanf("%d%d%d%d%d%d",&n,&m,&X1,&Y1,&X2,&Y2);
	}

	// say goodbye
}


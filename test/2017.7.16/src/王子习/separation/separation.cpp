#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=107;
char a[N][N],s1[N],s2[N];
int cnt,n,m,f[N][N];


int check(char s[]){
	int n=strlen(s);
	For(i,1,cnt){
		int flag=1;
		if (n!=strlen(a[i])) continue;
		For(j,0,n-1) if (a[i][j]!=s[j]){
			flag=0;
			break;
		}
		if (flag) return i;
	}
	cnt++;
	For(i,0,n-1) a[cnt][i]=s[i];
	return cnt;
}

int main(){
	// say hello

	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);

	int T=1;
	scanf("%d%d",&n,&m);
	while (n!=0&&m!=0){
		memset(f,127/3,sizeof(f));
		cnt=0;
		For(i,1,m){
			scanf("%s%s",s1,s2);
			int u=check(s1),v=check(s2);
			f[u][v]=1,f[v][u]=1;
		}
		For(k,1,n) For(i,1,n) For(j,1,n) if (f[i][j]>f[i][k]+f[k][j]) f[i][j]=f[i][k]+f[k][j];
		int ans=0,flag=0;
		For(i,1,n){ 
			if (flag) break;
			For(j,1,n){
				if (f[i][j]==f[0][0]){
					printf("Network %d: DISCONNECTED\n",T);
					flag=1;
					break;
				}
				else if (i!=j) ans=max(ans,f[i][j]);
			} 
		}
		if (!flag) printf("Network %d: %d\n",T,ans);
		T++;
		n=0,m=0;
		scanf("%d%d",&n,&m);
	}

	// say goodbye
}


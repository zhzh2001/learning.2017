#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<map>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;

inline int read(){
	int x=0,f=1;char ch=getchar();
	while (ch<'0'||ch>'9'){ if (ch=='-') f=-1;ch=getchar(); }
	while (ch>='0'&&ch<='9'){ x=x*10+ch-'0';ch=getchar(); }
	return x*f;
}

const int N=1007,rxd=1e9+7;
char s1[N],s2[N];
int q2[N],q3[N];
ll q1[N];
int tot,k,kh,top,n;
map <int,int> f,f1,f2;

int main(){
	// say hello

	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);

	scanf("%s",s1);
	int len_s1=strlen(s1);
	k=1;kh=0;
	For(i,0,len_s1-1){
		if (s1[i]=='+'){
			k=1;
			continue;
		}
		if (s1[i]>='0'&&s1[i]<='9'){
			k=s1[i]-'0';
			while (s1[i+1]>='0'&&s1[i+1]<='9'){
				k=k*10+s1[i+1]-'0';
				i++;
			}
		}
		if (s1[i]>='A'&&s1[i]<='Z'){
			ll tmp=s1[i];
			while (s1[i+1]>='a'&&s1[i+1]<='z'){
				i++;
				tmp=(tmp*17)+s1[i];
				tmp%=rxd;
			}
			int x=1,flag=0;
			while (s1[i+1]>='0'&&s1[i+1]<='9'){
				i++;
				if (s1[i]!=')'&&s1[i]!='('){
					if (flag) x=x*10+s1[i]-'0';
					else flag=1,x=s1[i]-'0';
				}
			}
			x*=k;
			if (kh>0) q1[++top]=x,q2[top]=tmp,q3[top]=kh;
			else f[tmp]+=x,tot+=x;
		}
		if (s1[i]=='('){
			kh++;
			continue;
		}
		if (s1[i]==')'){
			int k1=1,flag1=0;
			while (s1[i+1]>='0'&&s1[i+1]<='9'){
				i++;
				if (flag1) k1=k1*10+s1[i]-'0';
				else k1=s1[i]-'0';
				flag1=1;
			}
			For(i,1,top) if (q3[i]==kh) q1[i]*=k1;
			kh--;
			if (kh==0){
				while (top){
					f[q2[top]]+=q1[top];
					tot+=q1[top];
					top--;
				}
			}
		}
	}
//	printf("%d %d\n%d %d\n\n",72,f[72],79,f[79]);
	n=read();
	For(p,1,n){
		scanf("%s",s2);
		int len_s2=strlen(s2);
		k=1;kh=0;top=0;int tot1=0;
		int flagk=0;
		For(i,0,len_s2-1){
			if (s2[i]=='+'){
				k=1;
				continue;
			}
			if (s2[i]>='0'&&s2[i]<='9'){
				k=s2[i]-'0';
				while (s2[i+1]>='0'&&s2[i+1]<='9'){
					k=k*10+s2[i+1]-'0';
					i++;
				}
			}
			if (s2[i]>='A'&&s2[i]<='Z'){
				ll tmp=s2[i];
				while (s2[i+1]>='a'&&s2[i+1]<='z'){
					i++;
					tmp=(tmp*17)+s2[i];
					tmp%=rxd;
				}
				int x=1,flag=0;
				while (s2[i+1]>='0'&&s2[i+1]<='9'){
					i++;
					if (s2[i]!=')'&&s2[i]!='('){
						if (flag) x=x*10+s2[i]-'0';
						else flag=1,x=s2[i]-'0';
					}
				}
				x*=k;
				if (kh>0) q1[++top]=x,q2[top]=tmp,q3[top]=kh;
				else{
					if (f2[tmp]!=p) f2[tmp]=p,f1[tmp]=0;
					f1[tmp]+=x;
//					printf("%d %d\n",tmp,x);
					if (f1[tmp]>f[tmp]){
						printf("%s!=%s\n",s1,s2);
						flagk=1;
						break;
					}
					tot1+=x;
				}
			}
			if (s2[i]=='('){
				kh++;
				continue;
			}
			if (s2[i]==')'){
				int k1=1,flag1=0;
				while (s2[i+1]>='0'&&s2[i+1]<='9'){
					i++;
					if (flag1) k1=k1*10+s2[i]-'0';
					else k1=s2[i]-'0';
					flag1=1;
				}
				For(i,1,top) if (q3[i]==kh) (q1[i]*=k1)%=rxd;
				kh--;
				if (kh==0){
					while (top){
						int x=q1[top],tmp=q2[top];
						if (f2[tmp]!=p) f2[tmp]=p,f1[tmp]=0;
						f1[tmp]+=x;
//						printf("%d %d\n",tmp,x);
						if (f1[tmp]>f[tmp]){
							printf("%s!=%s\n",s1,s2);
							flagk=1;
							break;
						}
						tot1+=x;
						top--;
					}
				}
			}
		}
		if (flagk) continue;
		if (tot1==tot){
			printf("%s==%s\n",s1,s2);
		}else 	printf("%s!=%s\n",s1,s2);

	}	

	
	// say goodbye
}


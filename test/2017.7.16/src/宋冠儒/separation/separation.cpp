#include<bits/stdc++.h>
using namespace std;
struct data{
	string s;
	int id;
}a[55];
int p,r,cnt,ans;
int b[55][55];
map<string,int> mp;
 inline void init()
{
	cnt=0;
	ans=-INT_MAX;
	memset(b,0x3f,sizeof(b));
	for (int i=1;i<=p;++i)
		for (int j=1;j<=p;++j)
		if (i==j) b[i][j]=0;
}
 int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	int num=0;
	while (~scanf("%d%d",&p,&r))
	{
		if (p==0&&r==0) break;
		++num;
		cout<<"Network"<<' '<<num<<':'<<' ';
		init();
		for (int i=1;i<=r;++i)
		{
			string s1,s2;
			cin>>s1>>s2;
			if (!mp[s1]) 
			{
				a[++cnt].s=s1;	
				mp[s1]=cnt;
			}
			if (!mp[s2])
			{
				a[++cnt].s=s2;
				mp[s2]=cnt;
			}
			b[mp[s1]][mp[s2]]=b[mp[s2]][mp[s1]]=1;
		}
		for (int k=1;k<=p;++k)
			for (int i=1;i<=p;++i)
				for (int j=1;j<=p;++j)
				if (b[i][k]+b[k][j]<b[i][j])
				b[i][j]=b[i][k]+b[k][j];
		bool flag=false;
		for (int i=1;i<=p;++i)
		{
			for (int j=1;j<=p;++j)
			if (i!=j)
			{
				if (b[i][j]>100) flag=true;
				if (flag) break;
				ans=max(ans,b[i][j]);
			}
			if (flag) break;
		}
		if (flag) cout<<"DISCONNECTED"<<endl;
		else cout<<ans<<endl;
	}
}

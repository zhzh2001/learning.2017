#include <bits/stdc++.h>
#define ll long long
#define File(a) \
	freopen(a".in", "r", stdin), \
	freopen(a".out", "w", stdout)
#define id(a, b) (((a)-1)*m+(b))
#define N 10020
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch>'9'||ch<'0')ch=='-'&&(f=0)||(ch=getchar());
	while(ch<='9'&&ch>='0')x=(x<<1)+(x<<3)+ch-'0',ch=getchar();
	return f?x:-x;
}
int head[N], to[N<<2], nxt[N<<2], val[N<<2], cnt;
bool type[N<<2];
void ins(int x, int y, int z, bool t){ if(!z) return;
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt; val[cnt] = z; type[cnt] = t;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt; val[cnt] = z; type[cnt] = t;
}
int dis[N][2];
struct node{
	int x, dis, last;
	bool faceto;
	node(){}
	node(int x, int dis, int last, bool faceto):
		x(x),dis(dis),last(last),faceto(faceto){}
	friend bool operator < (const node &a, const node &b){
		return a.dis > b.dis;
	}
};
priority_queue<node>q;
int main(){
	File("roller");
	int ca = 0;
	while(1){
		int n = read(), m = read(), sx = read(), sy = read(), ex = read(), ey = read();
		if(!n && !m && !sx && !sy && !ex && !ey) return 0;
		memset(head, 0, sizeof(int)*N); cnt = 0;
		memset(dis, 63, sizeof(int)*N*2);
		for(int i = 1; i < n<<1; i++)
			if(i&1) for(int j = 1; j < m; j++) ins(id(i+1>>1, j), id(i+1>>1, j+1), read(), 1);
			else for(int j = 1; j <= m; j++) ins(id(i>>1, j), id((i>>1)+1, j), read(), 0);
		dis[id(sx, sy)][0] = dis[id(sx, sy)][1] = 0;
		for(int i = head[id(sx, sy)]; i; i = nxt[i])
			q.push(node(to[i], dis[to[i]][type[i]] = val[i]<<1, 0, type[i]));
		
		while(!q.empty()){
			node szb = q.top(); q.pop();
		
			if(dis[szb.x][szb.faceto] != szb.dis) continue;
			for(int i = head[szb.x]; i; i = nxt[i]){
				int zyy = szb.dis+val[i]+(type[i] == szb.faceto ? 0 : (szb.last+val[i]));
				if(to[i] == id(ex, ey) && type[i] == szb.faceto) zyy += val[i];
		
				if(zyy < dis[to[i]][type[i]]){
					dis[to[i]][type[i]] = zyy;
					if(to[i] != id(ex, ey)){
	
						q.push(node(to[i], zyy, type[i] == szb.faceto ? val[i] : 0, type[i]));
					}
				}
			}
		}
		int ans = min(dis[id(ex, ey)][0], dis[id(ex, ey)][1]);
		if(ans == dis[0][0]) printf("Case %d: Impossible\n", ++ca);
		else printf("Case %d: %d\n", ++ca, ans);
	}
}

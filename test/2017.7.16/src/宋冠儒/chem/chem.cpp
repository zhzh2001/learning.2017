#include<stdio.h>
#include<string.h>
#include<string>
#include<math.h>
#include<iostream>
#include<bitset>
#include<map>
#include<vector>
#include<set>
#include<queue>
#include<algorithm>
#include<limits.h>
#include<stdlib.h>
#include<time.h>
#define ll long long
#define inf 1e9
#define N 2005
#define mp make_pair
#define pa pair<ll,int>
#define For(i,x,y) for(int i=x;i<=y;i++)
#define Rep(i,x,y) for(int i=x;i>=y;i--)
using namespace std;
inline ll read(){ll x=0;int f=0;char ch=getchar();for(;ch<'0'||ch>'9';ch=getchar()) if(ch=='-') f=1;for(;ch>='0'&&ch<='9';ch=getchar()) x=(x<<1)+(x<<3)+ch-48;return f?-x:x;}
inline void write(ll x){if(x<10) putchar(x+48);else write(x/10),putchar(x%10+48);}
inline void writeln(ll x){if(x<0) x=-x,putchar('-');write(x);putchar('\n');}
char s[1005],ch[1005];
int n,m,p[27][27],w,g;
inline void solve(){
	printf("%s",s+1);
	if(rand()%2) printf("==");
	else printf("!=");
	printf("%s\n",ch+1);
}
int main(){
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	srand(time(0));
	scanf("%s",s+1);n=read();m=strlen(s+1);
	For(i,1,n) scanf("%s",ch+1),solve();
	return 0;
}


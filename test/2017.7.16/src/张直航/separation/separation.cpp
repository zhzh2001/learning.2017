#include <iostream>
#include <cstdlib>
#include <cstring>
#include <cstdio>
using namespace std;
const int M=6000;
const int N=100;
int n,m,test=0;
struct edge
{
	int to;
	int nxt;
}po[M];
int tot,edgenum[N];
char nick[N][2000];
char re[2000];
int len,k,l[N],te[2];

void init()
{
	tot=0;
	k=1;
	memset(nick,0,sizeof(nick));
	memset(re,0,sizeof(re));
	for(int i=1;i<=n;i++)
		edgenum[i]=0;
}

void add_edge(int s,int t)
{
	po[++tot].to=t;
	po[tot].nxt=edgenum[s];
	edgenum[s]=tot;
}

int get()
{
	int i,j;
	bool flag;
	for(i=1;i<k;i++)
		if(nick[i][0]==re[0]&&l[i]==len)
		{
			flag=false;
			for(j=1;j<l[i];j++)
				if(nick[i][j]!=re[j])
				{
					flag=true;
					break;
				}
			if(flag)
				continue;
			else
				return i;
		}
	return k;
}

int dis[N],q[N],top,dep;
bool vis[N];

void bfs(int now)
{
	top=0;
	dep=0;
	q[dep++]=now;
	memset(dis,0,sizeof(dis));
	memset(vis,false,sizeof(vis));
	dis[now]=0;
	vis[now]=1;
	int x,i,to;
	while(top<dep)
	{
		x=q[top++];
		vis[x]=true;
		for(i=edgenum[x];i;i=po[i].nxt)
		{
			to=po[i].to;
			if(!vis[to])
			{
				dis[to]=dis[x]+1;
				q[dep++]=to;
			}
		}
	}
}

int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	int i,j;
	while(1)
	{
		scanf("%d%d\n",&n,&m);
		if(n==0&&m==0)
			break;
		init();
		printf("Network %d: ",++test);
		for(i=1;i<=m*2;i++)
		{
			scanf("%s",re);
			len=strlen(re);
			te[i%2]=get();
			if(te[i%2]==k)
			{
				for(j=0;j<len;j++)
					nick[k][j]=re[j];
				l[k]=len;
				k++;
			}
			if(i%2==0)
			{
				add_edge(te[1],te[0]);
				add_edge(te[0],te[1]);
			}
		}
		bfs(1);
		int maxn=0,maxnum;
		bool flag=false;
		for(i=1;i<=n;i++)
		{
			if(vis[i])
			{
				if(dis[i]>maxn)
				{
					maxn=dis[i];
					maxnum=i;
				}
			}
			else 
			{
				flag=true;
				break;
			}
		}
		if(flag)
		{
			printf("DISCONNECTED\n");
			continue;
		}
		bfs(maxnum);
		maxn=0;
		for(i=1;i<=n;i++)
			if(dis[i]>maxn)
				maxn=dis[i];
		printf("%d\n",maxn);
	}
	return 0;
}


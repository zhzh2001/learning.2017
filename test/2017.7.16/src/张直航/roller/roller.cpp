#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstdlib>
using namespace std;
const int N=10100;
const int M=200100;
int n,m,sx,sy,ex,ey;
int tem;

struct edge
{
	int to;
	int dis;
	int nxt;
}po[M];
int tot,edgenum[N];

void init()
{
	tot=0;
	memset(edgenum,0,sizeof(edgenum));
}

void add_edge(int s,int t,int d)
{
	po[++tot].to=t;
	po[tot].dis=d;
	po[tot].nxt=edgenum[s];
	edgenum[s]=tot;
}

bool vis[N];
long long dis[N][4];
int q[N*4][4],top,dep;

int getnum(int i,int j)
{
	if(i-j==n)
		return 0;
	if(j-i==1)
		return 1;
	if(j-i==n)
		return 2;
	if(i-j==1)
		return 3;
}

void SPFA(int st)
{
	memset(vis,false,sizeof(vis));
	memset(dis,0x3f,sizeof(dis));
	int i,to;
	vis[st]=1;
	for(i=edgenum[st];i;i=po[i].nxt)
	{
		to=po[i].to;
		dis[to][getnum(to,st)]=po[i].dis*2;
		vis[to]=1;
		q[dep][0]=to;
		q[dep][1]=st;
		q[dep][2]=0;
		q[dep++][3]=i;
	}
	int x,last,la,li;
	long long d;
	while(top<dep)
	{
		x=q[top][0];
		last=q[top][1];
		la=q[top][2];
		li=q[top++][3];
		vis[x]=0;
		for(i=edgenum[x];i;i=po[i].nxt)
		{
			to=po[i].to;
			d=po[i].dis;
			if(getnum(to,x)!=getnum(x,last))
			{
				d+=po[i].dis;
				if(getnum(x,last)==getnum(last,la)&&la!=0)
					d+=po[li].dis;	
			}
			if(to==(ex-1)*n+ey&&getnum(x,last)==getnum(to,x))
				d+=po[i].dis;
			if(dis[x][getnum(x,last)]+d<dis[to][getnum(to,x)])
			{
				dis[to][getnum(to,x)]=dis[x][getnum(x,last)]+d;
				if(!vis[to])
				{
					q[dep][0]=to;
					q[dep][1]=x;
					q[dep][2]=last;
					q[dep++][3]=i;
				}
			}
		}
	}
}

int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	int i,j;
	while(1)
	{
		scanf("%d%d%d%d%d%d",&n,&m,&sx,&sy,&ex,&ey);
		if(n==0&&m==0&&sx==0&&sy==0&&ex==0&&ey==0)
			break;
		init();
		int d;
		printf("Case %d: ",++tem);
		for(i=1;i<=2*n-1;i++)
		{
			if(i%2==1)
				for(j=1;j<m;j++)
				{
					scanf("%d",&d);
					if(d!=0)
					{
						add_edge((i/2)*n+j,(i/2)*n+j+1,d);
						add_edge((i/2)*n+j+1,(i/2)*n+j,d);
					}
				}
			else
				for(j=1;j<=m;j++)
				{
					scanf("%d",&d);
					if(d!=0)
					{
						add_edge((i/2-1)*n+j,(i/2)*n+j,d);
						add_edge((i/2)*n+j,(i/2-1)*n+j,d);
					}
				}
		}
		SPFA((sx-1)*n+sy);
		long long ans=0x7fffffffffffff;
		for(i=0;i<4;i++)
			if(ans>dis[(ex-1)*n+ey][i])
				ans=dis[(ex-1)*n+ey][i];
		if(ans==0x7fffffffffffff)
			printf("Impossible\n");
		else
			printf("%lld\n",ans);
	}
	return 0;
}

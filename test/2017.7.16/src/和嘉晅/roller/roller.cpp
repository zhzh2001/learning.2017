#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <queue>
using namespace std;
const int inf=0x3f3f3f3f;
int n,m,Sx,Sy,Tx,Ty;
const int Maxn=400005;
long long val[Maxn];
struct Node
{
	int x,y,d; long long val;int ist;
	Node(){}
	Node(int a,int b,int c,int t,long long v)
	{ x=a; y=b; d=c; ist=t; val=v;}
	friend bool operator <(Node a,Node b)
	{ return a.val>b.val;}
};
priority_queue<Node> q;
long long f[Maxn];int vis[Maxn];//int?
const int dx[5]={0,-1,0,1,0};
const int dy[5]={0,0,-1,0,1};
inline int getp(int x,int y,int d,int ist)
{
	return (((d*2+ist)+y*5)+x*105);
}
inline int getp(Node a)
{
	return getp(a.x,a.y,a.d,a.ist);
}
const int fd[5]={0,3,4,1,2};
void dij()
{
	for(int i=0;i<Maxn;i++) 
		f[i]=inf;
	memset(vis,0,sizeof(vis));
	f[getp(Sx,Sy,1,1)]=0;
	q.push(Node(Sx,Sy,1,1,0));
	int t=getp(105,105,4,1);
	while(!q.empty())
	{
		Node now=q.top(); q.pop(); int p=getp(now);
		if(vis[p]) continue;
		vis[p]=1;
		int xx,yy,p1,vp;
		if(now.x==Sx&&now.y==Sy)
		{
			for(int i=1;i<=4;i++)
			{
				xx=now.x+dx[i]; yy=now.y+dy[i];
				if(xx<1||yy<1||xx>n||yy>m) continue;
				p1=getp(xx,yy,i,1); vp=getp(now.x,now.y,i,0);
				if((!vis[p1])&&f[p1]>f[p]+val[vp]*2)
				{
					f[p1]=f[p]+val[vp]*2;
					q.push(Node(xx,yy,i,1,f[p1]));
				}
			}
		}
		else
		{
			for(int i=1;i<=4;i++)
			{
				xx=now.x+dx[i]; yy=now.y+dy[i];
				if(xx<1||yy<1||xx>n||yy>m) continue;
				vp=getp(now.x,now.y,i,0);
				if(xx==Tx&&yy==Ty)
				{
					if(now.d==i) p1=getp(xx,yy,i,1);
					else p1=getp(xx,yy,i,0);
					if((!vis[p1])&&f[p1]>f[p]+val[vp]*2)
					{
						f[p1]=f[p]+val[vp]*2;
						if(now.d==i) q.push(Node(xx,yy,i,1,f[p1]));
						else q.push(Node(xx,yy,i,0,f[p1]));
					}
				}
				else if(now.d==i)//0
				{
					p1=getp(xx,yy,i,0);
					if((!vis[p1])&&f[p1]>f[p]+val[vp])
					{
						f[p1]=f[p]+val[vp];
						q.push(Node(xx,yy,i,0,f[p1]));
					}
				}
				else//1
				{
					p1=getp(xx,yy,i,1);
					if(now.ist)
					{
						if((!vis[p1])&&f[p1]>f[p]+val[vp]*2)
						{
							f[p1]=f[p]+val[vp]*2;
							q.push(Node(xx,yy,i,1,f[p1]));
						}
					}
					else
					{
						if((!vis[p1])&&f[p1]>f[p]+val[vp]*2+val[getp(now.x,now.y,fd[now.d],0)])
						{
							f[p1]=f[p]+val[vp]*2+val[getp(now.x,now.y,fd[now.d],0)];
							q.push(Node(xx,yy,i,1,f[p1]));
						}
					}
				}
			}
		}
	}
}
void Init()
{
	for(int i=0;i<Maxn;i++) 
		val[i]=inf;
}
int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	int cas=0;
	while(scanf("%d%d%d%d%d%d",&n,&m,&Sx,&Sy,&Tx,&Ty)!=EOF)
	{
		cas++; Init();
		if(n==0&&m==0) break;
		for(int x=1;x<=n;x++)
		{
			int t1;
			for(int y=1;y<m;y++)
			{
				scanf("%d",&t1);
				if(t1==0)
					t1=inf;
				val[getp(x,y,4,0)]=t1; val[getp(x,y+1,2,0)]=t1;
			}
			if(x!=n)
				for(int y=1;y<=m;y++)
				{
					scanf("%d",&t1);
					if(t1==0)
						t1=inf;
					val[getp(x,y,3,0)]=t1; val[getp(x+1,y,1,0)]=t1;
				}
		}
		dij();
		long long ans=inf;
		for(int i=1;i<=4;i++)
				ans=min(ans,f[getp(Tx,Ty,i,1)]);
		printf("Case %d: ",cas);
		if(ans==inf)
			printf("Impossible\n");
		else
			printf("%lld\n",ans);
	}
	return 0;
}

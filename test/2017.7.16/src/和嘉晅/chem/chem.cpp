#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <string>
#include <cstring>
using namespace std;
char S[205]; long long ansn[205]; int anss[205];
char T[10005][205];
long long tansn[205]; int tanss[205]; int tse[205];
map<string,int> e; int ans[10005];
int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	int n;
	scanf("%s",S+1);
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%s",T[i]+1);
	int len=strlen(S+1); e.clear();
	int cntn=0,cnts=0;  int tot=0; int Zs=0;
	for(int i=1,now=1;i<=len;i++)//1:int 0:string
	{
		if(S[i]=='+')continue;
		if(now)
		{
			bool fl=(i==1);
			int res;
			if(S[i]<'0'||S[i]>'9')
			{
				res=1;
				i--;
			}
			else
				res=S[i]-'0';
			while(S[i+1]>='0'&&S[i+1]<=9)
			{
				i++;
				res=res*10+S[i]-'0';
			}
			if(fl)
				Zs=res;
			else
				ansn[++cntn]=res;
		}
		else
		{
			string temp=""; temp+=S[i];
			if(e[temp]==0) e[temp]=++tot;
			anss[++cnts]=e[temp];
		}
		now^=1;
	}
	for(int i=1;i<=cntn;i++)
		ansn[i]*=Zs;
	
	int l=len;
	for(int i=1;i<=l;i++)
		ans[anss[i]]+=ansn[i];
	for(int i=1;i<=n;i++)
	{
		char *s=T[i];
		len=strlen(s+1);
		int tcntn=0,tcnts=0;
		memset(tansn,0,sizeof(tansn));
		memset(tanss,0,sizeof(tanss));
		Zs=0;
		for(int j=1,now=1;j<=len;j++)
		{
			if(s[j]=='+')continue;
			if(now)
			{
				bool fl=(j==1);
				int res;
				if(s[j]<'0'||s[j]>'9')
				{
					res=1;
					j--;
				}
				else
					res=s[i]-'0';
				while(s[j+1]>='0'&&s[j+1]<='9')
				{
					j++;
					res=res*10+s[j]-'0';
				}
				if(fl)
					Zs=res;
				else
					tansn[++tcntn]=res;
			}
			else
			{
				string temp=""; temp+=s[i];
				if(e[temp]==0)
					e[temp]=++tot;
				tanss[++tcnts]=e[temp];
			}
			now^=1;
		}
		for(int j=1;j<=len;j++)
			tansn[j]*=Zs;
		
		memset(tse,0,sizeof(tse));
		for(int j=1;j<=len;j++)
			tse[tanss[j]]+=tansn[j];
		bool ok=1;
		for(int j=1;j<=tot;j++)
			if(ans[j]!=tse[j])
			{
				ok=0;
				break;
			}
		
		printf("%s",S+1);
		if(ok)
			printf("==");
		else
			printf("!=");
		printf("%s\n",s+1);
	}
	return 0;
}

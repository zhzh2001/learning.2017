#include <iostream>
#include <cstdio>
#include <algorithm>
#include <map>
#include <cstring>
using namespace std;
map<string,int> e;
char t1[10005],t2[10005];
int g[205][205];
const int inf=0x3f3f3f3f;
int main()
{
	freopen("separation.in","r",stdin);
	freopen("separation.out","w",stdout);
	int n,m; int cas=0;
	while(scanf("%d%d",&n,&m)!=EOF)
	{
		if(n==0&&m==0) break;
		
		cas++;
		e.clear();
		memset(g,inf,sizeof(g));
		int tot=0;
		
		for(int i=1,t3,t4;i<=m;i++)
		{
			scanf("%s",t1);
			if(e[t1]==0) 
				e[t1]=++tot;
			scanf("%s",t2);
			if(e[t2]==0) 
				e[t2]=++tot;
			t3=e[t1]; t4=e[t2];
			g[t3][t4]=1; g[t4][t3]=1;
		}
		for(int i=1;i<=n;i++)
			g[i][i]=0;
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					g[i][j]=min(g[i][j],g[i][k]+g[k][j]);
		
		int ans=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				ans=max(ans,g[i][j]);
		printf("Network %d: ",cas);
		if(ans==inf)
			printf("DISCONNECTED\n");
		else
			printf("%d\n",ans);
	}
	return 0;
}

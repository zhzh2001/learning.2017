#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <functional>
#include <queue>

using namespace std;

ifstream fin("roller.in");
ofstream fout("roller.out");

const int MaxN = 13005, inf = 0x3f3f3f3f;

int N, M, S, T, cnt, head[MaxN], w, dis[MaxN];

inline int idx(int x, int y)
{
	return ((x - 1) * 105 + y);
}

inline int readpos()
{
	static int x, y;
	fin >> x >> y;
	return idx(x, y);
}

struct Edge
{
	int to, nxt, w;
	bool flag; /// flag = 1 -> col / 0 -> row
} e[MaxN << 2], *Last[MaxN];

inline void AddEdge(int u, int v, int w, bool flag)
{
	e[++cnt].to = v;
	e[cnt].nxt = head[u];
	head[u] = cnt;
	e[cnt].w = w;
	e[cnt].flag = flag;
}


struct state
{
	int first, second;
	bool LastSelected;

	state(int first, int second, bool flag = false)
		:first(first), second(second), LastSelected(flag)
	{
	}

	friend bool operator > (const state &a, const state &b)
	{
		return a.first == b.first ? a.second < b.second : a.first > b.first;
	}
};

priority_queue<state, vector<state>, greater<state> > Heap;

inline int CalcuateOffset(const Edge *x, const Edge *y, bool LastSelected = false)
{
	if (x == 0x0)
		return y->w;
	if (y->to == T)
		return y->w;
	if (x->flag == y->flag)
		return 0;
	return x->w + y->w - (int)LastSelected * x->w;
}

void Dijkstra()
{
	memset(dis, 0x3f, sizeof dis);
	dis[S] = 0;
	Heap.push(state(0, S));
	while (!Heap.empty())
	{
		state now = Heap.top();
		Heap.pop();
		if (now.first > dis[now.second])
			continue;
		for (int i = head[now.second]; i; i = e[i].nxt)
		{
			int ExtraDis = CalcuateOffset(Last[now.second], &e[i], now.LastSelected);
			if (dis[e[i].to] > dis[now.second] + e[i].w + ExtraDis)
			{
				dis[e[i].to] = dis[now.second] + e[i].w + ExtraDis;
				Last[e[i].to] = &e[i];
				Heap.push(state(dis[e[i].to], e[i].to, ExtraDis > 0));	
			}
		}
	}
}

int main()
{
	int ncase = 0;
	while (fin >> N >> M)
	{
		S = readpos(), T = readpos(), cnt = 0;
		if (N == 0 && M == 0) break;
		memset(head, 0, sizeof head);
		memset(Last, 0, sizeof Last);
		int now = 0;
		for (int i = 1; i < N * 2; i++)
		{
			if (i & 1)
			{
				++now;
				for (int j = 1; j < M; j++)
				{
					fin >> w;
					if (w)
					{
						AddEdge(idx(now, j), idx(now, j + 1), w, 0);
						AddEdge(idx(now, j + 1), idx(now, j), w, 0);
						// fprintf(stderr, "%d, %d -- %d, %d: %d\n", now, j, now, j + 1, w);
					}
				}
			}
			else
			{
				for (int j = 1; j <= M; j++)
				{
					fin >> w;
					if (w)
					{
						AddEdge(idx(now, j), idx(now + 1, j), w, 1);
						AddEdge(idx(now + 1, j), idx(now, j), w, 1);
						// fprintf(stderr, "%d, %d -- %d, %d: %d\n", now, j, now + 1, j, w);
					}
				}
			}
		}
		Dijkstra();
		if (dis[T] != inf)
			fout << "Case " << ++ncase << ": " << dis[T] << endl;
		else
			fout << "Case " << ++ncase << ": " << "Impossible" << endl;
	}
	return 0;
}
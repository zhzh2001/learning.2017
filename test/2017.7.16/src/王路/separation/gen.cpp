#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <ctime>
#include <fstream>
#include <Windows.h>
using namespace std;

int main()
{
	srand(GetTickCount());
	ofstream fout("separation.in");
	int T = 500;
	while (T--)
	{
		int P = 50, R = 250;
		fout << P << ' ' << R << endl;
		for (int i = 1; i <= R; i++)
		{
			fout << rand() % P << ' ' << rand() % P << endl;
		}
	}
	return 0;
}
#include <fstream>
#include <cstdio>
#include <iostream>
#include <string>
#include <algorithm>
#include <cstring>
#include <map>

using namespace std;

ifstream fin("separation.in");
ofstream fout("separation.out");

const int MaxP = 52, MaxR = MaxP * MaxP, inf = 0x3f3f3f3f;

int P, R, Ans, vis[MaxP], ncase, node;
map<string, int> mp;

int mat[MaxP][MaxP];

int main()
{
	string a, b;
	while (fin >> P >> R)
	{
		if (P == 0 && R == 0)
			break;
		node = 0;
		mp.clear();
		Ans = 0;
		bool isConnected = true;
		memset(mat, 0x3f, sizeof mat);
		for (int i = 1; i <= R; i++)
		{
			fin >> a >> b;
			if (!mp.count(a)) mp[a] = ++node;
			if (!mp.count(b)) mp[b] = ++node;
			mat[mp[a]][mp[b]] = mat[mp[b]][mp[a]] = 1;
		}
		for (int k = 1; k <= P; k++)
		{
			for (int i = 1; i <= P; i++)
				for (int j = 1; j <= P; j++)
				{
					mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
				}
		}
		for (int i = 1; i <= P; i++)
		{
			for (int j = 1; j <= P; j++)
				if (i != j)
				{
					if (mat[i][j] >= inf)
					{
						isConnected = false;
						break;
					}
					else
						Ans = max(Ans, mat[i][j]);
				}
		}
		if (isConnected)
			fout << "Network " << ++ncase << ": " << Ans << endl;
		else
			fout << "Network " << ++ncase << ": " << "DISCONNECTED" << endl;
	}
	return 0;
}
#include <fstream>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <functional>
#include <queue>
#include <string>
#include <map>
#include <stack>

using namespace std;

ifstream fin("chem.in");
ofstream fout("chem.out");

typedef string ChemPart;
typedef vector<ChemPart> BigElement;

struct Expr
{
	typedef map<string, int> value_type;

	map<string, int> mat;

	Expr operator *= (const int &k)
	{
		for (value_type::iterator it = mat.begin(); it != mat.end(); ++it)
			it->second *= k;
		return *this;
	}

	Expr operator += (Expr rhs)
	{
		for (value_type::iterator it = rhs.mat.begin(); it != rhs.mat.end(); ++it)
		{
			this->mat[it->first] += it->second;
		}
		return *this;
	}

	bool operator == (Expr rhs)
	{
		for (value_type::iterator it = rhs.mat.begin(); it != rhs.mat.end(); ++it)
		{
			if (this->mat[it->first] != it->second)
				return false;
		}
		for (value_type::iterator it = mat.begin(); it != mat.end(); ++it)
		{
			if (rhs.mat[it->first] != it->second)
				return false;
		}
		return true;
	}
} Fmt_Expr, Test_Expr;

stack<int> st;

inline pair<BigElement, string> Input()
{
	string str, bak;
	fin >> str;
	bak = str;
	BigElement ret;
	size_t pos;
	while ((pos = str.find('+')) != string::npos)
	{
		ret.push_back(str.substr(0, pos));
		str = str.substr(pos + 1);
	}
	ret.push_back(str);
	return make_pair(ret, bak);
}

BigElement Fmt, TestFmt;
pair<BigElement, string> GlobalBuffer, TestBuffer;

Expr Calcuate(string now)
{
	Expr ret;
	int num = 0;
	bool isNumber = true;
	string atom;
	if (isdigit(now[0]))
		isNumber = true;
	else
		isNumber = false;
	int globalnumber = 1;

	bool isExists = false;

	for (int i = 0; i < (int)now.length(); i++)
	{
		char ch = now[i];
		if (isdigit(ch))
			num = num * 10 + ch - '0';
		if (isupper(ch))
		{
			if (isNumber)
			{
				isNumber = false;
				globalnumber = num;
			}
			if (isExists) /// last Atom
			{
				if (!num) num = 1;
				ret.mat[atom] += num;
			}
			isExists = true; /// A new Atom
			num = 0;
			atom = "";
			atom += ch;
		}
		if (islower(ch))
			atom += ch;
	}
	if (isExists) /// last Atom
	{
		if (!num) num = 1;
		ret.mat[atom] += num;
	}
	if (globalnumber != 1)
	 	ret *= globalnumber;
	return ret;
}

Expr WorkBrackets(string ref)
{
	Expr tmp;
	tmp.mat.clear();
	int num = 0;
	bool isNumber = true;
	string atom;
	if (isdigit(ref[0]))
		isNumber = true;
	else
		isNumber = false;
	int globalnumber = 1;

	bool isExists = false;

	for (int i = 0; i < (int)ref.length(); i++)
	{
		char ch = ref[i];
		if (isdigit(ch))
			num = num * 10 + ch - '0';
		if (isupper(ch))
		{
			if (isNumber)
			{
				isNumber = false;
				globalnumber = num;
			}
			if (isExists) /// last Atom
			{
				if (!num) num = 1;
				if (atom != "") tmp.mat[atom] += num;
			}
			isExists = true; /// A new Atom
			num = 0;
			atom = "";
			atom += ch;
		}
		if (islower(ch))
			atom += ch;
		if (ch == '(')
		{
			if (isNumber)
			{
				isNumber = false;
				globalnumber = num;
			}
			if (isExists) /// last Atom
			{
				if (!num) num = 1;
				if (atom != "")
					tmp.mat[atom] += num;
			}
			isExists = true;
			size_t pos = ref.find(')');
			atom = "";
			// cout << it->substr(i + 1, pos - i - 1) << endl;
			tmp += WorkBrackets(ref.substr(i + 1, pos - i - 1));
			i = pos + 1;
		}
	}
	if (isExists) /// last Atom
	{
		if (!num) num = 1;
		if (atom != "") tmp.mat[atom] += num;
	}
	if (globalnumber != 1)
		tmp *= globalnumber;			
	return tmp;
}

Expr Export(BigElement now)
{
	Expr ret;
	for (BigElement::iterator it = now.begin(); it != now.end(); ++it)
	{
		Expr tmp;
		if (it->find('(') == string::npos) /// no brackets
		{
			tmp = Calcuate(*it);		
			ret += tmp;
		}
		else // exists brackets
		{
			ret += WorkBrackets(*it);
		}
	}
	return ret;
}

int main()
{
	GlobalBuffer = Input();
	Fmt = GlobalBuffer.first;
	Fmt_Expr = Export(Fmt);
	int T;
	fin >> T;
	while (T--)
	{
		TestBuffer = Input();
		TestFmt = TestBuffer.first;
		Test_Expr = Export(TestFmt);
		if (Fmt_Expr == Test_Expr)
			fout << GlobalBuffer.second << "==" << TestBuffer.second << endl;
		else
			fout << GlobalBuffer.second << "!=" << TestBuffer.second << endl;
	}
	return 0;
}
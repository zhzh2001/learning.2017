#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int maxn = 55;
const int maxm = 105;
int p[maxn*maxm][128], val[maxn*maxm];
int e[maxn][maxn];
int n, m, cnt, size, a, b;
char s[maxm];

int insert(char s[]){
	int o = 1;
	for (int i=0; s[i]; i++){
		if (!p[o][s[i]]) p[o][s[i]] = ++size;
		o = p[o][s[i]];
	}
	if (!val[o]) return val[o] = ++cnt;
	return val[o];
}

int main(){
	freopen("separation.in", "r", stdin);
	freopen("separation.out", "w", stdout);
	int Case = 0;
	while (~scanf("%d%d", &n, &m) && n+m){
		memset(p, 0, sizeof p);
		memset(val, 0, sizeof val);
		memset(e, 0x3f, sizeof e);
		size = 1; cnt = 0;
		for (int i=1; i<=m; i++){
			scanf("%s", &s);
			a = insert(s);
			scanf("%s", &s);
			b = insert(s);
			e[a][b] = e[b][a] = 1;
		}
		for (int k=1; k<=n; k++)
			for (int i=1; i<=n; i++)
				for (int j=1; j<=n; j++)
					e[i][j] = min(e[i][j], e[i][k]+e[k][j]);
		int ans = 0, res;
		for (int i=1; i<=n; i++){
			res = 0;
			for (int j=1; j<=n; j++)
				if (i != j) res = max(res, e[i][j]);
			ans = max(res, ans);
		}
		printf("Network %d: ", ++Case);
		if (ans >= 1e9) puts("DISCONNECTED");
		else printf("%d\n", ans);
	}
	return 0;
}

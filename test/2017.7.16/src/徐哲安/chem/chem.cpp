#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;

const int maxn = 509;
struct node{ int a, b, type; } tmp;
node stack[maxn];
int p[maxn*10][52], val[maxn*10];
int size = 1, m = 0, top = 0;
int cnt[maxn*10], ans[maxn*10];
char s[maxn], t[maxn];

inline int getNum(char s[], int &i){
	if (s[i] < '0' || '9' < s[i]) return 1;
	int res = 0;
	while ('0' <= s[i] && s[i] <= '9')
		res = res*10 + s[i++]-'0';
	return res;
}

inline int getOrder(char ch){
	if ('a' <= ch && ch <= 'z') return ch - 'a';
	return ch - 'A' + 26;
}

int getAlpha(char s[], int &i){
	char t[maxn]; int j = 0, o = 1, tmp;
	t[j++] = s[i++];
	while ('a' <= s[i] && s[i] <= 'z') t[j++] = s[i++];
	t[j] = '\0'; 
	for (int k=0; k<j; k++){
		tmp = getOrder(t[k]);
		if (!p[o][tmp]) p[o][tmp] = ++size;
		o = p[o][tmp];
	}
	if (!val[o]) return val[o] = ++m;
	return val[o];
}

void solve(char s[], int cnt[]){
	int len = strlen(s), i = 0, j, x, base;
	while (i < len){
		base = getNum(s, i);
		while (s[i] != '+' && i < len)
			if ('A' <= s[i] && s[i] <= 'Z'){
				tmp.a = getAlpha(s, i);
				tmp.b = getNum(s, i);
				tmp.type = 0;
				stack[++top] = tmp;
			}
			else if (s[i] == '('){
				tmp.type = 1;
				stack[++top] = tmp;
				i++;
			}
			else if (s[i] == ')'){
				j = top;
				while (!stack[j].type) j--;
				x = getNum(s, ++i);
				for (int k=j+1; k<=top; k++){
					stack[k].b *= x;
					stack[k-1] = stack[k];
				}
				top--; 
			}
		while (top){
			cnt[stack[top].a] += base*stack[top].b; 
			top--;
		}
		i++;
	}
}

int main(){
	freopen("chem.in", "r", stdin);
	freopen("chem.out", "w", stdout);
	scanf("%s", &s); solve(s, ans);
	int N, flag; scanf("%d", &N);
	while (N--){
		memset(t, 0, sizeof t);
		memset(cnt, 0, sizeof cnt);
		scanf("%s", &t); solve(t, cnt);
		flag = 1;
		for (int i=1; i<=m; i++)
			if (ans[i] != cnt[i]) { flag = 0; break; }
		printf("%s", &s);
		if (flag) printf("==");
		else printf("!=");
		printf("%s\n", &t); 
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<algorithm>
#include<queue>
using namespace std;
typedef long long LL;
const int maxn = 105;
const int dx[4] = {1, 0, -1, 0};
const int dy[4] = {0, 1, 0, -1};	//�£� �ң� �ϣ� �� 
struct state{
	int x, y, dir, flag; LL dist;
	bool operator < (const state &s) const{
		return dist > s.dist;
	}
} u, v;
priority_queue<state> Q; 
LL d[maxn][maxn][4][2];
bool vis[maxn][maxn][4][2];
int a[maxn][maxn], b[maxn][maxn];
int n, m, sx, sy, tx, ty;
inline int read(){
	char ch = getchar(); int x = 0, f = 1;
	while (ch < '0' || '9' < ch) { if (ch == '-') f = -1; ch = getchar(); }
	while ('0' <= ch && ch <= '9') { x = x*10+ch-'0'; ch = getchar(); }
	return f * x;
}
inline int getCost(int x, int y, int k){
	if (k == 0) return b[x][y];
	if (k == 1) return a[x][y];
	if (k == 2) return b[x-1][y];
	return a[x][y-1];
}
int main(){
	freopen("roller.in", "r", stdin);
	freopen("roller.out", "w", stdout);
	int Case = 0;
	while (1){
		n = read(); m = read(); if (!(n+m)) break;
		sx = read(); sy = read(); tx = read(); ty = read();
		for (int i=1; i<=2*n-1; i++)
			if (i&1){
				for (int j=1; j<=m-1; j++)
					a[(i+1)>>1][j] = read();
			}
			else{
				for (int j=1; j<=m; j++)
					b[i>>1][j] = read();
			}
		memset(d, 0x3f, sizeof d);
		while (!Q.empty()) Q.pop();
		for (int k=0; k<=3; k++){
			int x = sx+dx[k], y = sy+dy[k];
			if (x <= 0 || y <= 0) continue;
			if (x >= n+1 || y >= m+1) continue;
			if (!getCost(sx, sy, k)) continue;
			u.dist = d[x][y][k][1] = 2*getCost(sx, sy, k);
			u.x = x; u.y = y; u.dir = k; u.flag = 1;
			Q.push(u);
		}
		memset(vis, 0, sizeof vis);
		while (!Q.empty()){
			u = Q.top(); Q.pop();
			if (vis[u.x][u.y][u.dir][u.flag]) continue;
			vis[u.x][u.y][u.dir][u.flag] = true;
			if (u.x == tx && u.y == ty) break;
			for (int k=0; k<=3; k++)
				if ((k^u.dir)&1==1 || u.dir == k){
					v.x = u.x + dx[k]; v.y = u.y + dy[k]; v.dir = k;
					if (v.x <= 0 || v.y <= 0) continue;
					if (v.x >= n+1 || v.y >= m+1) continue;
					if (getCost(u.x, u.y, k) == 0) continue;
					
					if (u.dir != k || v.x == tx && v.y == ty){
						v.dist = u.dist + 2*getCost(u.x, u.y, k);
						if (!u.flag && u.dir != k)
							v.dist += getCost(u.x-dx[u.dir], u.y-dy[u.dir], u.dir);
						v.flag = 1;
					}
					else{
						v.dist = u.dist + getCost(u.x, u.y, k);
						v.flag = 0;
					}
					if (v.dist < d[v.x][v.y][v.dir][v.flag]){
						Q.push(v);
						d[v.x][v.y][v.dir][v.flag] = v.dist;
					}
				}
		}
		LL res = 2e18;
		for (int k=0; k<=3; k++)
			res = min(res, min(d[tx][ty][k][1], d[tx][ty][k][1]));
		printf("Case %d: ", ++Case);
		if (res >= 1e18) puts("Impossible");
		else printf("%lld\n", res);
	}
	return 0;
}

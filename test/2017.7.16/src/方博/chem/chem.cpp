#include<iostream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<string>
#include<algorithm>
#include<queue>
#include<cmath>
#include<map>
#include<cctype>
using namespace std;
template<typename T>
void read(T& A)
{
	char r;bool f=false;
	for(r=getchar();(r<48||r>57)&&r!='-';r=getchar());
	if(r=='-')
	{
		f=true;r=getchar();
	}
	for(A=0;r>=48&&r<=57;r=getchar())A=A*10+r-48;
	if(f)A=-A;
}
template<typename T>
T Max(const T& a,const T& b){return a>b?a:b;}
template<typename T>
T Min(const T& a,const T& b){return a<b?a:b;}
template<typename T>
T Swap(const T& a,const T& b){T t=a;a=b;b=t;}

const int inf=2147483647;
typedef unsigned int uint;
const uint uinf=4294967295u;
const int maxl=103;
int n;
int cntx[10003],tid=0;
int cnty[10003];
char x[200];int lenx;
char y[200];int leny;
int to[100003];
void findx()
{
	for(int i=1;i<=lenx;++i)
	{
		if(isupper(x[i]))
		{
			int id;
			if(islower(x[i+1]))
			{
				id=(x[i]*1000)+x[i+1];
				++i;
			}
			else id=x[i]*1000;
			if(!to[id])
			{
				to[id]=++tid;
			}
		}
	}
}
int getnumx(int& l)
{
	int v=0;
	while(isdigit(x[l]))
	{
		v=v*10+x[l]-48;
		++l;
	}
	--l;
	return v;
}
int getnumy(int& l)
{
	int v=0;
	while(isdigit(y[l]))
	{
		v=v*10+y[l]-48;
		++l;
	}
	--l;
	return v;
}
bool same;
void calcx(int l,int r,int num)
{
	if(x[l]=='('&&x[r]==')')
		++l,--r;		
	int tnum;
	for(int i=l;i<=r;++i)
	{
		if(x[i]=='(')
		{
			int j=1;
			int lb=i;
			while(j)
			{
				++i;
				if(x[i]=='(')
					++j;
				if(x[i]==')')
					--j;
			}
			int rb=i;
			if(isdigit(x[i+1]))
			{
				++i;
				tnum=getnumx(i);
			}
			else tnum=1;
			calcx(lb,rb,num*tnum);
		}
		else if(isupper(x[i]))
		{
			int id;
			if(islower(x[i+1]))
			{
				id=(x[i]*1000)+x[i+1];
				++i;
			}
			else id=x[i]*1000;
			if(isdigit(x[i+1]))
			{
				++i;
				tnum=getnumx(i);
			}
			else tnum=1;
			cntx[to[id]]+=num*tnum;	
		}
	}
}
void calcy(int l,int r,int num)
{
	if(y[l]=='('&&y[r]==')')
		++l,--r;		
	int tnum;
	for(int i=l;i<=r;++i)
	{
		if(y[i]=='(')
		{
			int j=1;
			int lb=i;
			while(j)
			{
				++i;
				if(y[i]=='(')
					++j;
				if(y[i]==')')
					--j;
			}
			int rb=i;
			if(isdigit(y[i+1]))
			{
				++i;
				tnum=getnumy(i);
			}
			else tnum=1;
			calcy(lb,rb,num*tnum);
		}
		else if(isupper(y[i]))
		{
			int id;
				if(islower(y[i+1]))
				{
					id=(y[i]*1000)+y[i+1];
					++i;
				}
				else id=y[i]*1000;
				
				if(!to[id])
				{
					same=false;
					#ifdef DEBUG
					//cout<<"ERROR"<<endl;
					#endif
					return;
				}
				
				if(isdigit(y[i+1]))
				{
					++i;
					tnum=getnumy(i);
				}
				else tnum=1;
				cnty[to[id]]+=num*tnum;
				if(cnty[to[id]]>cntx[to[id]])
				{
					same=false;
					return;
				}
		}
	}
}

void dividx()
{
	int lb,tnum;
	for(int i=1;i<=lenx;++i)
	{
		lb=i;
		while(x[i]!='+'&&i<=lenx)
		{
			++i;
		}
		--i;
		if(isdigit(x[lb]))
		{
			tnum=0;
			while(isdigit(x[lb]))
			{
				tnum=tnum*10+x[lb]-48;
				++lb;
			}
		}
		else tnum=1;
		calcx(lb,i,tnum);
		++i;
	}
}
void dividy()
{
	int lb,tnum;
	for(int i=1;i<=leny;++i)
	{
		lb=i;
		while(y[i]!='+'&&i<=leny)
		{
			++i;
		}
		--i;
		if(isdigit(y[lb]))
		{
			tnum=0;
			while(isdigit(y[lb]))
			{
				tnum=tnum*10+y[lb]-48;
				++lb;
			}
		}
		else tnum=1;
		calcy(lb,i,tnum);
		++i;
	}
}

void input()
{
	scanf("%s",x+1);
	lenx=strlen(x+1);
	read(n);
	findx();
	dividx();
}
void solve()
{
	for(int i=1;i<=n;++i)
	{
		memset(cnty,0,sizeof(cnty));
		same=true;
		scanf("%s",y+1);
		leny=strlen(y+1);
		dividy();
		if(same)
		{
			if(memcmp(cntx+1,cnty+1,tid*(sizeof(int)))!=0)
				same=false;
		}
		printf("%s",x+1);
		if(same)
			cout<<"==";
		else cout<<"!=";
		printf("%s\n",y+1);
	}
}
int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	input();
	solve();
	return 0;
}

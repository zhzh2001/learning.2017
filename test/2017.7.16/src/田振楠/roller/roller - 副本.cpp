#include <cstdio>
#include <algorithm>
#include <cstring>
#include <vector>
#define N 10005
using namespace std;
struct rec{int v,w;};
vector<rec> edge[N];
int dis[N],heap[N],inhp[N],size;
int ans,n,m,x1,x2,y1,y2,hen[105][105],zong[105][105],T,num[105][105];
bool inHeap[N];
inline int read()
{
	int x=0;char ch=getchar();
	while (ch>'9'||ch<'0') ch=getchar();
	while (ch>='0'&&ch<='9') x=x*10+ch-48,ch=getchar();
	return x;
}
void upData(int x)
{
	while(x>1)
	{
        int px=x>>1;
		if(dis[heap[x]]<dis[heap[px]])
		{
			int temp;
            inhp[heap[x]]=px;
            inhp[heap[px]]=x;
            temp=heap[x];
            heap[x]=heap[px];
            heap[px]=temp;
            x=px;
		}
        else
            break;
	}
}
void downData(int x)
{
	int lx,rx;
    while(x<=size/2)
	{
		lx=x<<1;
        rx=lx+1;
        int sx=x;
		if(lx<=size &&dis[heap[sx]]>dis[heap[lx]]) sx=lx;
        if(rx<=size &&dis[heap[sx]]>dis[heap[rx]]) sx=rx;
        if(sx==x) break;
		inhp[heap[x]]=sx;
        inhp[heap[sx]]=x;
        int temp=heap[x];
		heap[x]=heap[sx];
        heap[sx]=temp;
        x=sx;
	}
}
void Dijkstra(int st,int ed)
{
	memset(dis,-1,sizeof(dis));
    memset(inHeap,0,sizeof(inHeap));
    size=1;
    heap[1]=st;
    inhp[st]=1;
    int i,u,v;
	dis[st]=0;
    while(size>0)
    {
		u=heap[1];
        if(u==ed) break;
        int temp;
        temp=heap[1];
        heap[1]=heap[size];
        heap[size]=temp;
		inhp[heap[1]]=1;
        size--;
        downData(1);
        inHeap[u]=false;
        for(i=0;i<edge[u].size();i++)
        {
            v=edge[u][i].v;
            if(dis[v]==-1 || dis[v]>dis[u]+edge[u][i].w)
            {
                dis[v]=dis[u]+edge[u][i].w;
                if(!inHeap[v])
                {
                    inHeap[v]=true;
                    size++;
                    heap[size]=v;
                    inhp[v]=size;
                    upData(size);
                }
                else upData(inhp[v]);
            }
        }
    }
}
int main()
{
	freopen("roller.in","r",stdin);
//	freopen("roller.out","w",stdout);
	while (1)
	{
		T++;
		n=read();m=read();x1=read();y1=read();x2=read();y2=read();
		if (n==0&&m==0) return 0;
		for (int i=1;i<n;i++)
		{
			for (int j=1;j<m;j++)
			hen[i][j]=read();
			for (int j=1;j<=m;j++)
			zong[i][j]=read();
		}
		for (int i=1;i<m;i++)
		hen[n][i]=read();
		int t=0;
		for (int i=1;i<=n;i++)
		for (int j=1;j<=m;j++)
		num[i][j]=++t;
		ans=0;
		for (int i=1;i<=10000;i++)
		edge[i].clear();
		if (x1==x2&&y1==y2) printf("Case %d: 0\n",T);
		else 
		{
			for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
			{
				if (i+1<=n&&zong[i][j]!=0) edge[num[i][j]].push_back((rec){num[i+1][j],zong[i][j]*2});
				if (i-1>=1&&zong[i-1][j]!=0) edge[num[i][j]].push_back((rec){num[i-1][j],zong[i-1][j]*2});
				if (j+1<=m&&hen[i][j]!=0) edge[num[i][j]].push_back((rec){num[i][j+1],hen[i][j]*2});
				if (j-1>=1&&hen[i][j-1]!=0) edge[num[i][j]].push_back((rec){num[i][j-1],hen[i][j-1]*2});
			}
		/*	for (int i=1;i<=n;i++)
			for (int j=1;j<=m;j++)
			{
				printf("%d %d-->%d:",i,j,num[i][j]);
				for (int k=0;k<edge[num[i][j]].size();k++)
				printf("(%d %d)",edge[num[i][j]][k].v,edge[num[i][j]][k].w);
				printf("\n");
			}
		*/	if (x1==x2) 
			{
				if (y1>y2) swap(y1,y2);
				for (int i=y1;i<y2;i++)
				ans+=hen[x1][i];
			}
			else if (y1==y2)
			{
				if (x1>x2) swap(x1,x2);
				for (int i=x1;i<x2;i++)
				ans+=zong[y1][i];
			}
			else ans=100000000;
			Dijkstra(num[x1][y1],num[x2][y2]);
			if (dis[num[x2][y2]]!=-1) ans=min(ans,dis[num[x2][y2]]);
			t=zong[x1][y1];
			for (int i=x1+2;i<=n;i++)
			{
				Dijkstra(num[i][y1],num[x2][y2]);
				if (dis[num[x2][y2]]!=-1) ans=min(ans,t+dis[num[x2][y2]]+zong[i-1][y1]*2);
				t+=zong[i-1][y1];
			}
			t=zong[x1-1][y1];
			for (int i=x1-2;i>=1;i--)
			{
				Dijkstra(num[i][y1],num[x2][y2]);
				if (dis[num[x2][y2]]!=-1) ans=min(ans,t+dis[num[x2][y2]]+zong[i][y1]*2);
				t+=zong[i][y1];
			}
			t=hen[x1][y1];
			for (int i=y1+2;i<=m;i++)
			{
				Dijkstra(num[x1][i],num[x2][y2]);
				if (dis[num[x2][y2]]!=-1) ans=min(ans,t+dis[num[x2][y2]]+hen[x1][i-1]*2);
				t+=hen[x1][i-1];
			}
			t=hen[x1][y1-1];
			for (int i=y1-2;i>=1;i--)
			{
				Dijkstra(num[x1][i],num[x2][y2]);
				if (dis[num[x2][y2]]!=-1) ans=min(ans,t+dis[num[x2][y2]]+hen[x1][i]*2);
				t+=hen[x1][i];
			}
			if (ans==100000000) printf("Case %d: Impossible\n",T);
			else printf("Case %d: %d\n",T,ans);
		}
	}
}

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <queue>
using namespace std;

struct node
{
	int tim,tx,ty,lastd,lst,lls;//lastd:1=up,2=right,3=down,4=left
	bool operator < (const node &b) const
	{
		return tim>b.tim;
	}
};
priority_queue<node> q;
int n,m,x,xx,y,yy;
int h[111][111],z[111][111];
int mit[111][111];

int read()
{
	int x=0,f=1;char c=getchar();
	while (!isdigit(c)) {if (c=='-')f=-1;c=getchar();}
	while (isdigit(c)) {x=x*10+c-'0';c=getchar();}
	return x*f;
}

int main()
{
	freopen("roller.in","r",stdin);
	freopen("roller.out","w",stdout);
	n=read(),m=read();x=read(),y=read(),xx=read(),yy=read();
	int T;
	while (n!=0&&m!=0)
	{
		++T;
		while (!q.empty()) q.pop();
		for (int i=1;i<=n-1;i++)
		{
			for (int j=1;j<m;j++)
				h[i][j]=read();
			for (int j=1;j<=m;j++)
				z[i][j]=read();
		}
		for (int j=1;j<m;j++)
			h[n][j]=read();
		for (int i=1;i<=n;i++) for (int j=1;j<=m;j++) mit[i][j]=999999999;
		q.push((node){0,x,y,-1,0,-1});
		bool flag=0;
		while (!q.empty())
		{
			node nw=q.top();q.pop();if (mit[nw.tx][nw.ty]<999999999) continue;
			mit[nw.tx][nw.ty]=nw.tim;
			if (nw.lastd==-1)
			{
				if (nw.tx>1&&z[nw.tx-1][nw.ty]!=0) q.push((node){2*z[nw.tx-1][nw.ty],nw.tx-1,nw.ty,1,z[nw.tx-1][nw.ty],1});
				if (nw.ty>1&&h[nw.tx][nw.ty-1]!=0) q.push((node){2*h[nw.tx][nw.ty-1],nw.tx,nw.ty-1,4,h[nw.tx][nw.ty-1],1});
				if (nw.tx<n&&z[nw.tx][nw.ty]!=0) q.push((node){2*z[nw.tx][nw.ty],nw.tx+1,nw.ty,3,z[nw.tx][nw.ty],1});
				if (nw.ty<m&&h[nw.tx][nw.ty]!=0) q.push((node){2*h[nw.tx][nw.ty],nw.tx,nw.ty+1,2,h[nw.tx][nw.ty],1});
			}
			if (nw.tx==xx&&nw.ty==yy)
			{
				if (!nw.lls) mit[xx][yy]+=nw.lst;
				flag=1;break;
			}
			if (nw.tx>1 && z[nw.tx-1][nw.ty]!=0)
			{
				if (nw.lastd==1) q.push((node){nw.tim+z[nw.tx-1][nw.ty],nw.tx-1,nw.ty,1,z[nw.tx-1][nw.ty],0});
				else
				{
					if (nw.lls==1) q.push((node){nw.tim+2*z[nw.tx-1][nw.ty],nw.tx-1,nw.ty,1,z[nw.tx-1][nw.ty],1});
					else q.push((node){nw.tim+2*z[nw.tx-1][nw.ty]+nw.lst,nw.tx-1,nw.ty,1,z[nw.tx-1][nw.ty],1});
				}
			}
			if (nw.ty>1 && h[nw.tx][nw.ty-1]!=0)
			{
				if (nw.lastd==4) q.push((node){nw.tim+h[nw.tx][nw.ty-1],nw.tx,nw.ty-1,4,h[nw.tx][nw.ty-1],0});
				else
				{
					if (nw.lls==1) q.push((node){nw.tim+2*h[nw.tx][nw.ty-1],nw.tx,nw.ty-1,4,h[nw.tx][nw.ty-1],1});
					else q.push((node){nw.tim+2*h[nw.tx][nw.ty-1]+nw.lst,nw.tx,nw.ty-1,4,h[nw.tx][nw.ty-1],1});
				}
			}
			if (nw.tx<n && z[nw.tx][nw.ty]!=0)
			{
				if (nw.lastd==3) q.push((node){nw.tim+z[nw.tx][nw.ty],nw.tx+1,nw.ty,3,z[nw.tx][nw.ty],0});
				else
				{
					if (nw.lls==1) q.push((node){nw.tim+2*z[nw.tx][nw.ty],nw.tx+1,nw.ty,3,z[nw.tx][nw.ty],1});
					else q.push((node){nw.tim+2*z[nw.tx][nw.ty]+nw.lst,nw.tx+1,nw.ty,3,z[nw.tx][nw.ty],1});
				}
			}
			if (nw.ty<m && h[nw.tx][nw.ty]!=0)
			{
				if (nw.lastd==2) q.push((node){nw.tim+h[nw.tx][nw.ty],nw.tx,nw.ty+1,2,h[nw.tx][nw.ty],0});
				else
				{
					if (nw.lls==1) q.push((node){nw.tim+2*h[nw.tx][nw.ty],nw.tx,nw.ty+1,2,h[nw.tx][nw.ty],1});
					else q.push((node){nw.tim+2*h[nw.tx][nw.ty]+nw.lst,nw.tx,nw.ty+1,2,h[nw.tx][nw.ty],1});
				}
			}
		}
		printf("Case %d: ",T);
		if (flag)
		{
			printf("%d\n",mit[xx][yy]);
		}
		else
		{
			puts("Impossible");
		}
		n=read(),m=read();x=read(),y=read(),xx=read(),yy=read();
	}
	return 0;
}


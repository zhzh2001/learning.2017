//mmpʲô���� 
#include <iostream>
#include <cstdio>
#include <algorithm>
#include <string>
using namespace std;

int n;
string c[15];
struct node
{
	string che;int num;
}fc[15][125];
int tot[15];

bool isbig(char c)
{
	if (c>='A'&&c<='Z')return 1;
	return 0;
}

bool issmall(char c)
{
	if (c>='a'&&c<='z')return 1;
	return 0;
}

void div3(int x,int p,string s)
{
	int l=0;
	while (l<s.size()-1&&!isdigit(s[l])) l++;
	if (l==s.size()-1)
	{
		if (!isdigit(s[l]))
		{
			bool flag=0;
			for (int i=1;i<=tot[p];i++)
				if (fc[p][i].che==s)
				{
					fc[p][i].num+=x;flag=1;break;
				}
			if (!flag)
			{
				tot[p]++;fc[p][tot[p]].che=s;fc[p][tot[p]].num=x;
			}
		}
		else
		{
			int tmp=s[l]-'0';tmp*=x;
			string chemi=s.substr(0,l);
			bool flag=0;
			for (int i=1;i<=tot[p];i++)
				if (fc[p][i].che==chemi)
				{
					fc[p][i].num+=tmp;flag=1;break;
				}
			if (!flag)
			{
				tot[p]++;fc[p][tot[p]].che=chemi;fc[p][tot[p]].num=tmp;
			}
		}
	}
	else
	{
		int tmp=0,pos=l;
		while (l<s.size()&&isdigit(s[l])) tmp=tmp*10+s[l]-'0',l++;
		string chemi=s.substr(0,pos);
		tmp*=x;
		bool flag=0;
		for (int i=1;i<=tot[p];i++)
			if (fc[p][i].che==chemi)
			{
				fc[p][i].num+=tmp;flag=1;break;
			}
		if (!flag)
		{
			tot[p]++;fc[p][tot[p]].che=chemi;fc[p][tot[p]].num=tmp;
		}
	}
}

void div2_2(int x,int p,string s)
{
	int tmp=0,l=0,r;
	while (isdigit(s[l])) tmp=tmp*10+s[l]-'0';
	if (l==0) tmp=1;
	tmp*=x;
	while(l<s.size())
	{
		if (s[l]=='(' )
		{
			int cnt1=1,cnt2=0;r=l+1;
			while(cnt1>cnt2)
			{
				if (s[r]=='(')
					cnt1++;
				if (s[r]==')')
					cnt2++;
				r++;
			}
			r--;
			int amo=0,rr=r;
			while (rr<s.size()-1&&isdigit(s[rr+1]))rr++,amo=amo*10+s[rr]-'0';
			if (amo==0) amo=1;
			div2_2(tmp*amo,p,s.substr(l+1,r-l-1));
			l=rr+1;continue;
		}
		r=l+1;
		while (r<s.size()-1&&(issmall(s[r])||isdigit(s[r]))) ++r;
		int lene;
		if (r==s.size()-1)
		{
			if (isbig(s[r])) lene=r-l;
			else lene=r-l+1;
		}
		else
			lene=r-l;
		div3(tmp,p,s.substr(l,lene));
		if (r==s.size()-1&&(issmall(s[r])||isdigit(s[r]))) break;
		l=r;
	}
}

void div2(int p,string s)
{
	int tmp=0,l=0,r;
	while (isdigit(s[l]))
		tmp=tmp*10+s[l]-'0',l++;
	if (l==0) tmp=1;
	r=l;
	while (l<s.size())
	{
		if (s[l]=='(' )
		{
			int cnt1=1,cnt2=0;r=l+1;
			while(cnt1>cnt2)
			{
				if (s[r]=='(')
					cnt1++;
				if (s[r]==')')
					cnt2++;
				r++;
			}
			r--;
			int amo=0,rr=r;
			while (rr<s.size()-1&&isdigit(s[rr+1]))rr++,amo=amo*10+s[rr]-'0';
			if (amo==0) amo=1;
			div2_2(tmp*amo,p,s.substr(l+1,r-l-1));
			l=rr+1;continue;
		}
		r=l+1;
		while (r<s.size()-1&&(issmall(s[r])||isdigit(s[r]))) ++r;
		int lene;
		if (r==s.size()-1)
		{
			if (isbig(s[r])) lene=r-l;
			else lene=r-l+1;
		}
		else
			lene=r-l;
		div3(tmp,p,s.substr(l,lene));
		if (r==s.size()-1&&(issmall(s[r])||isdigit(s[r]))) break;
		l=r;
	}
}

void div1(int p,string s)
{
	int l=0,r=0;
	while (l<s.size())
	{
		while (r<s.size()-1&&s[r]!='+') r++;
		int lene=(r==s.size()-1?r-l+1:r-l);
		div2(p,s.substr(l,lene));
		l=++r;
	}
}

int main()
{
	freopen("chem.in","r",stdin);
	freopen("chem.out","w",stdout);
	cin >> c[0] >> n;
	for (int i=1;i<=n;i++) cin >> c[i];
	for (int i=0;i<=n;i++)
		div1(i,c[i]);
	for (int i=1;i<=n;i++)
	{
		if (tot[i]!=tot[0])
		{
			cout << c[0] <<"!="<<c[i] << "\n";continue;
		}
		bool bigflag=1,smallflag;
		for (int j=1;j<=tot[0];j++)
		{
			smallflag=0;
			for (int k=1;k<=tot[i];k++)
				if (fc[i][k].che==fc[0][j].che&&fc[i][k].num==fc[0][j].num)
					smallflag=1;
			if (!smallflag){bigflag=0;break;}
		}
		if (!bigflag)
		{
			cout << c[0] <<"!="<<c[i] << "\n";continue;
		}
		else
		{
			cout << c[0] <<"=="<<c[i] << "\n";continue;
		}
	}
	return 0;
}


#include <fstream>
#include <queue>
#include <cstring>
using namespace std;
ifstream fin("roller.in");
ofstream fout("roller.out");
const int N = 105, INF = 0x3f3f3f3f;
int mat[N * 2][N], d[N][N][5][2];
bool inQ[N][N][5][2];
struct node
{
	int x, y, pred, turning;
	node(int x, int y, int pred, int turning) : x(x), y(y), pred(pred), turning(turning) {}
};
int main()
{
	int n, m, sx, sy, tx, ty, cas = 0;
	while (fin >> n >> m >> sx >> sy >> tx >> ty && n)
	{
		for (int i = 1; i < 2 * n; i++)
			if (i & 1)
				for (int j = 1; j < m; j++)
					fin >> mat[i][j];
			else
				for (int j = 1; j <= m; j++)
					fin >> mat[i][j];
		memset(d, 0x3f, sizeof(d));
		memset(inQ, false, sizeof(inQ));
		d[sx][sy][0][0] = 0;
		queue<node> Q;
		Q.push(node(sx, sy, 0, 0));
		inQ[sx][sy][0][0] = true;
		while (!Q.empty())
		{
			node k = Q.front();
			Q.pop();
			int x = k.x, y = k.y, pred = k.pred, turning = k.turning;
			inQ[x][y][pred][turning] = false;
			int extra, dist = d[x][y][pred][turning];
			switch (k.pred)
			{
			case 1:
				extra = mat[x * 2][k.y];
				break;
			case 2:
				extra = mat[(x - 1) * 2][y];
				break;
			case 3:
				extra = mat[x * 2 - 1][y];
				break;
			case 4:
				extra = mat[x * 2 - 1][y - 1];
				break;
			}
			if (x > 1 && mat[(x - 1) * 2][y])
				if (pred != 1)
					if (turning || pred == 0 || (x - 1 == tx && y == ty))
					{
						if (dist + 2 * mat[(x - 1) * 2][y] < d[x - 1][y][1][turning])
						{
							d[x - 1][y][1][turning] = dist + 2 * mat[(x - 1) * 2][y];
							if (!inQ[x - 1][y][1][turning])
							{
								Q.push(node(x - 1, y, 1, turning));
								inQ[x - 1][y][1][turning] = true;
							}
						}
					}
					else
					{
						if (dist + 2 * mat[(x - 1) * 2][y] < d[x - 1][y][1][1])
						{
							d[x - 1][y][1][1] = dist + 2 * mat[(x - 1) * 2][y];
							if (!inQ[x - 1][y][1][1])
							{
								Q.push(node(x - 1, y, 1, 1));
								inQ[x - 1][y][1][1] = true;
							}
						}
					}
				else if (x - 1 == tx && y == ty)
				{
					if (dist + 2 * mat[(x - 1) * 2][y] < d[x - 1][y][1][0])
					{
						d[x - 1][y][1][0] = dist + 2 * mat[(x - 1) * 2][y];
						if (!inQ[x - 1][y][1][0])
						{
							Q.push(node(x - 1, y, 1, 0));
							inQ[x - 1][y][1][0] = true;
						}
					}
				}
				else
				{
					if (dist + mat[(x - 1) * 2][y] < d[x - 1][y][1][0])
					{
						d[x - 1][y][1][0] = dist + mat[(x - 1) * 2][y];
						if (!inQ[x - 1][y][1][0])
						{
							Q.push(node(x - 1, y, 1, 0));
							inQ[x - 1][y][1][0] = true;
						}
					}
				}
			if (x < n && mat[x * 2][y])
				if (pred != 2)
					if (turning || pred == 0 || (x + 1 == tx && y == ty))
					{
						if (dist + 2 * mat[x * 2][y] < d[x + 1][y][2][turning])
						{
							d[x + 1][y][2][turning] = dist + 2 * mat[x * 2][y];
							if (!inQ[x + 1][y][2][turning])
							{
								Q.push(node(x + 1, y, 2, turning));
								inQ[x + 1][y][2][turning] = true;
							}
						}
					}
					else
					{
						if (dist + 2 * mat[x * 2][y] + extra < d[x + 1][y][2][1])
						{
							d[x + 1][y][2][1] = dist + 2 * mat[x * 2][y] + extra;
							if (!inQ[x + 1][y][2][1])
							{
								Q.push(node(x + 1, y, 2, 1));
								inQ[x + 1][y][2][1] = true;
							}
						}
					}
				else if (x + 1 == tx && y == ty)
				{
					if (dist + 2 * mat[x * 2][y] < d[x + 1][y][2][0])
					{
						d[x + 1][y][2][0] = dist + 2 * mat[x * 2][y];
						if (!inQ[x + 1][y][2][0])
						{
							Q.push(node(x + 1, y, 2, 0));
							inQ[x + 1][y][2][0] = true;
						}
					}
				}
				else
				{
					if (dist + mat[x * 2][y] < d[x + 1][y][2][0])
					{
						d[x + 1][y][2][0] = dist + mat[x * 2][y];
						if (!inQ[x + 1][y][2][0])
						{
							Q.push(node(x + 1, y, 2, 0));
							inQ[x + 1][y][2][0] = true;
						}
					}
				}
			if (y > 1 && mat[x * 2 - 1][y - 1])
				if (pred != 3)
					if (turning || pred == 0 || (x == tx && y - 1 == ty))
					{
						if (dist + 2 * mat[x * 2 - 1][y - 1] < d[x][y - 1][3][turning])
						{
							d[x][y - 1][3][turning] = dist + 2 * mat[x * 2 - 1][y - 1];
							if (!inQ[x][y - 1][3][turning])
							{
								Q.push(node(x, y - 1, 3, turning));
								inQ[x][y - 1][3][turning] = true;
							}
						}
					}
					else
					{
						if (dist + 2 * mat[x * 2 - 1][y - 1] + extra < d[x][y - 1][3][1])
						{
							d[x][y - 1][3][1] = dist + 2 * mat[x * 2 - 1][y - 1] + extra;
							if (!inQ[x][y - 1][3][1])
							{
								Q.push(node(x, y - 1, 3, 1));
								inQ[x][y - 1][3][1] = true;
							}
						}
					}
				else if (x == tx && y - 1 == ty)
				{
					if (dist + 2 * mat[x * 2 - 1][y - 1] < d[x][y - 1][3][0])
					{
						d[x][y - 1][3][0] = dist + 2 * mat[x * 2 - 1][y - 1];
						if (!inQ[x][y - 1][3][0])
						{
							Q.push(node(x, y - 1, 3, 0));
							inQ[x][y - 1][3][0] = true;
						}
					}
				}
				else
				{
					if (dist + mat[x * 2 - 1][y - 1] < d[x][y - 1][3][0])
					{
						d[x][y - 1][3][0] = dist + mat[x * 2 - 1][y - 1];
						if (!inQ[x][y - 1][3][0])
						{
							Q.push(node(x, y - 1, 3, 0));
							inQ[x][y - 1][3][0] = true;
						}
					}
				}
			if (y < m && mat[x * 2 - 1][y])
				if (pred != 4)
					if (turning || pred == 0 || (x == tx && y + 1 == ty))
					{
						if (dist + 2 * mat[x * 2 - 1][y] < d[x][y + 1][4][turning])
						{
							d[x][y + 1][4][turning] = dist + 2 * mat[x * 2 - 1][y];
							if (!inQ[x][y + 1][4][turning])
							{
								Q.push(node(x, y + 1, 4, turning));
								inQ[x][y + 1][4][turning] = true;
							}
						}
					}
					else
					{
						if (dist + 2 * mat[x * 2 - 1][y] + extra < d[x][y + 1][4][1])
						{
							d[x][y + 1][4][1] = dist + 2 * mat[x * 2 - 1][y] + extra;
							if (!inQ[x][y + 1][4][1])
							{
								Q.push(node(x, y + 1, 4, 1));
								inQ[x][y + 1][4][1] = true;
							}
						}
					}
				else if (x == tx && y + 1 == ty)
				{
					if (dist + 2 * mat[x * 2 - 1][y] < d[x][y + 1][4][0])
					{
						d[x][y + 1][4][0] = dist + 2 * mat[x * 2 - 1][y];
						if (!inQ[x][y + 1][4][0])
						{
							Q.push(node(x, y + 1, 4, 0));
							inQ[x][y + 1][4][0] = true;
						}
					}
				}
				else
				{
					if (dist + mat[x * 2 - 1][y] < d[x][y + 1][4][0])
					{
						d[x][y + 1][4][0] = dist + mat[x * 2 - 1][y];
						if (!inQ[x][y + 1][4][0])
						{
							Q.push(node(x, y + 1, 4, 0));
							inQ[x][y + 1][4][0] = true;
						}
					}
				}
		}
		int ans = INF;
		for (int i = 1; i <= 4; i++)
			ans = min(ans, min(d[tx][ty][i][0], d[tx][ty][i][1]));
		fout << "Case " << ++cas << ": ";
		if (ans == INF)
			fout << "Impossible\n";
		else
			fout << ans << endl;
	}
	return 0;
}
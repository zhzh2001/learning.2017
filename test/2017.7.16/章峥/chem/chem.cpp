#include <fstream>
#include <string>
#include <cstring>
#include <cctype>
using namespace std;
ifstream fin("chem.in");
ofstream fout("chem.out");
const int A = 26;
int cnt[A][A + 1], a[A][A + 1];
string s;
int formala(int l, int r);
int chemfor(int l, int r, int c);
int element(int l, int r, int c);
int chemele(int l, int r, int c);
int parsenum(int l, int r, int &res);

int formala(int l, int r)
{
	//fout << "formala:" << s.substr(l, r - l + 1) << endl;
	while (l <= r)
	{
		int c;
		l = parsenum(l, r, c);
		int t;
		for (t = l; t <= r && s[t] != '+'; t++)
			;
		chemfor(l, t - 1, c);
		l = t + 1;
	}
	return l;
}

int chemfor(int l, int r, int c)
{
	//fout << "chemfor:" << s.substr(l, r - l + 1) << ' ' << c << endl;
	while (l <= r)
	{
		int t = element(l, r, 0), c2;
		int t2 = parsenum(t, r, c2);
		element(l, t - 1, c * c2);
		l = t2;
	}
	return l;
}

int element(int l, int r, int c)
{
	//fout << "element:" << s.substr(l, r - l + 1) << ' ' << c << endl;
	if (s[l] == '(')
	{
		int t;
		for (t = r; t >= l && s[t] != ')'; t--)
			;
		chemfor(l + 1, t - 1, c);
		return t + 1;
	}
	return chemele(l, r, c);
}

int chemele(int l, int r, int c)
{
	//fout << "chemele:" << s.substr(l, r - l + 1) << ' ' << c << endl;
	if (l == r || !islower(s[l + 1]))
	{
		cnt[s[l] - 'A'][26] += c;
		return l + 1;
	}
	cnt[s[l] - 'A'][s[l + 1] - 'a'] += c;
	return l + 2;
}

int parsenum(int l, int r, int &res)
{
	res = 0;
	for (; l <= r && isdigit(s[l]); l++)
		res = res * 10 + s[l] - '0';
	if (res == 0)
		res = 1;
	return l;
}

void showres()
{
	for (int i = 0; i < A; i++)
		for (int j = 0; j <= A; j++)
			if (cnt[i][j])
				if (j < A)
					fout << (char)(i + 'A') << (char)(j + 'a') << cnt[i][j];
				else
					fout << (char)(i + 'A') << cnt[i][j];
	fout << endl;
}

int main()
{
	fin >> s;
	formala(0, s.length() - 1);
	//showres();
	memcpy(a, cnt, sizeof(a));
	string t = s;
	int n;
	fin >> n;
	while (n--)
	{
		fin >> s;
		memset(cnt, 0, sizeof(cnt));
		formala(0, s.length() - 1);
		//showres();
		if (memcmp(cnt, a, sizeof(cnt)))
			fout << t << "!=" << s << endl;
		else
			fout << t << "==" << s << endl;
	}
	return 0;
}
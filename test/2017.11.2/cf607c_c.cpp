#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 1000005, B = 233;
typedef unsigned long long hash_t;
hash_t hs[N], ht[N];
inline int trans(char c)
{
	switch (c)
	{
	case 'N':
		return 0;
	case 'S':
		return 1;
	case 'W':
		return 2;
	case 'E':
		return 3;
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	string s, t;
	cin >> n >> s >> t;
	reverse(s.begin(), s.end());
	n--;
	s = ' ' + s;
	t = ' ' + t;
	for (int i = 1; i <= n; i++)
	{
		hs[i] = hs[i - 1] * B + (trans(s[i]) ^ 1);
		ht[i] = ht[i - 1] * B + trans(t[i]);
	}
	hash_t p = B;
	for (int i = n; i; i--, p *= B)
		if (ht[n] - ht[i - 1] * p == hs[n - i + 1])
		{
			cout << "NO\n";
			return 0;
		}
	cout << "YES\n";
	return 0;
}
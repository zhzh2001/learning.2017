#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
using namespace std;
const int N = 80, INF = 1e9;
int f[N][N][N][2], cv[N], ck[N], cx[N];
int main()
{
	int n;
	string s;
	cin >> n >> s;
	s = ' ' + s;
	vector<int> V{0}, K{0}, X{0};
	for (int i = 1; i <= n; i++)
	{
		cv[i] = cv[i - 1];
		ck[i] = ck[i - 1];
		cx[i] = cx[i - 1];
		if (s[i] == 'V')
		{
			V.push_back(i);
			cv[i]++;
		}
		else if (s[i] == 'K')
		{
			K.push_back(i);
			ck[i]++;
		}
		else
		{
			X.push_back(i);
			cx[i]++;
		}
	}
	fill_n(&f[0][0][0][0], sizeof(f) / sizeof(int), INF);
	f[0][0][0][0] = 0;
	for (int i = 0; i < V.size(); i++)
		for (int j = 0; j < K.size(); j++)
			for (int k = 0; k < X.size(); k++)
				for (int v = 0; v < 2; v++)
					if (f[i][j][k][v] < INF)
					{
						if (i < V.size() - 1)
							f[i + 1][j][k][1] = min(f[i + 1][j][k][1], f[i][j][k][v] + max(cv[V[i + 1] - 1] - i, 0) + max(ck[V[i + 1] - 1] - j, 0) + max(cx[V[i + 1] - 1] - k, 0));
						if (j < K.size() - 1 && !v)
							f[i][j + 1][k][0] = min(f[i][j + 1][k][0], f[i][j][k][v] + max(cv[K[j + 1] - 1] - i, 0) + max(ck[K[j + 1] - 1] - j, 0) + max(cx[K[j + 1] - 1] - k, 0));
						if (k < X.size() - 1)
							f[i][j][k + 1][0] = min(f[i][j][k + 1][0], f[i][j][k][v] + max(cv[X[k + 1] - 1] - i, 0) + max(ck[X[k + 1] - 1] - j, 0) + max(cx[X[k + 1] - 1] - k, 0));
					}
	cout << min(f[V.size() - 1][K.size() - 1][X.size() - 1][0], f[V.size() - 1][K.size() - 1][X.size() - 1][1]) << endl;
	return 0;
}
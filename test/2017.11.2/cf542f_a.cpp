#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
using namespace std;
const int T = 105;
vector<int> a[T];
int main()
{
	ios::sync_with_stdio(false);
	int n, t;
	cin >> n >> t;
	for (int i = 1; i <= n; i++)
	{
		int t, q;
		cin >> t >> q;
		a[t].push_back(q);
	}
	for (int i = 1; i < t; i++)
	{
		sort(a[i].begin(), a[i].end(), greater<int>());
		for (size_t j = 0; j < a[i].size(); j++)
			if (j + 1 < a[i].size())
			{
				a[i + 1].push_back(a[i][j] + a[i][j + 1]);
				j++;
			}
			else
				a[i + 1].push_back(a[i][j]);
	}
	cout << *max_element(a[t].begin(), a[t].end()) << endl;
	return 0;
}
#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 16, m = 120;
struct card
{
	char c;
	int r, b;
} c[N];
int f[1 << N][m + 1];
int main()
{
	int n;
	cin >> n;
	int sumr = 0, sumb = 0;
	for (int i = 0; i < n; i++)
	{
		cin >> c[i].c >> c[i].r >> c[i].b;
		sumr += c[i].r;
		sumb += c[i].b;
	}
	memset(f, -1, sizeof(f));
	f[0][0] = 0;
	for (int i = 0; i < 1 << n; i++)
	{
		int r = 0, b = 0;
		for (int j = 0; j < n; j++)
			if (i & (1 << j))
				if (c[j].c == 'R')
					r++;
				else
					b++;
		for (int j = 0; j <= m; j++)
			if (~f[i][j])
				for (int k = 0; k < n; k++)
					if (~i & (1 << k))
					{
						int saver = min(r, c[k].r), saveb = min(b, c[k].b);
						if (j + saver <= m)
							f[i | (1 << k)][j + saver] = max(f[i | (1 << k)][j + saver], f[i][j] + saveb);
					}
	}
	int ans = max(sumr, sumb);
	for (int i = 0; i <= m; i++)
		if (~f[(1 << n) - 1][i])
			ans = min(ans, max(sumr - i, sumb - f[(1 << n) - 1][i]));
	cout << ans + n << endl;
	return 0;
}
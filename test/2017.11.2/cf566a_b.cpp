#include <iostream>
#include <string>
#include <list>
#include <algorithm>
using namespace std;
const int N = 800005;
int trie[N][26], cc, ans[N], val;
list<int> real[N], pse[N];
void trie_insert(const string &s, int id, bool bl)
{
	int now = 0;
	for (char c : s)
	{
		if (!trie[now][c - 'a'])
			trie[now][c - 'a'] = ++cc;
		now = trie[now][c - 'a'];
	}
	if (bl)
		real[now].push_back(id);
	else if (!real[now].empty())
	{
		val += s.length();
		ans[real[now].back()] = id;
		real[now].pop_back();
	}
	else
		pse[now].push_back(id);
}
void dfs(int k, int now)
{
	for (int i = 0; i < 26; i++)
		if (trie[now][i])
		{
			dfs(k + 1, trie[now][i]);
			if (real[now].size() && pse[trie[now][i]].size())
			{
				int sz = min(real[now].size(), pse[trie[now][i]].size());
				val += sz * k;
				while (sz--)
				{
					ans[*real[now].rbegin()] = *pse[trie[now][i]].rbegin();
					real[now].pop_back();
					pse[trie[now][i]].pop_back();
				}
			}
			if (real[trie[now][i]].size() && pse[now].size())
			{
				int sz = min(real[trie[now][i]].size(), pse[now].size());
				val += sz * k;
				while (sz--)
				{
					ans[*real[trie[now][i]].rbegin()] = *pse[now].rbegin();
					real[trie[now][i]].pop_back();
					pse[now].pop_back();
				}
			}
			real[now].splice(real[now].end(), real[trie[now][i]]);
			pse[now].splice(pse[now].end(), pse[trie[now][i]]);
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		string s;
		cin >> s;
		trie_insert(s, i, true);
	}
	for (int i = 1; i <= n; i++)
	{
		string s;
		cin >> s;
		trie_insert(s, i, false);
	}
	dfs(0, 0);
	cout << val << endl;
	for (int i = 1; i <= n; i++)
		cout << i << ' ' << ans[i] << endl;
	return 0;
}
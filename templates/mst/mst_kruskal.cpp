#include<bits/stdc++.h>
using namespace std;
struct edge
{
	int u,v,w;
};
edge e[200005];
bool cmp(edge a,edge b)
{
	return a.w<b.w;
}
int f[5005];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint();
	for(int i=1;i<=m;i++)
	{
		e[i].u=getint();
		e[i].v=getint();
		e[i].w=getint();
	}
	sort(e+1,e+m+1,cmp);
	for(int i=1;i<=n;i++)
		f[i]=i;
	int ne=0,ans=0;
	for(int i=1;i<=m;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv)
		{
			ans+=e[i].w;
			f[ru]=rv;
			ne++;
			if(ne==n-1)
			{
				cout<<ans<<endl;
				return 0;
			}
		}
	}
	cout<<"orz\n";
	return 0;
}
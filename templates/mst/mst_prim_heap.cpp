#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
struct node
{
	int v,w;
	bool operator>(const node b)const
	{
		return w>b.w;
	}
};
vector<node> mat[5005];
priority_queue<node,vector<node>,greater<node> > q;
int d[5005];
bool vis[5005];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint();
	while(m--)
	{
		int u=getint(),v=getint(),w=getint();
		mat[u].push_back((node){v,w});
		mat[v].push_back((node){u,w});
	}
	memset(vis,false,sizeof(vis));
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	q.push((node){1,0});
	int ans=0,cc=0;
	while(!q.empty())
	{
		node k=q.top();q.pop();
		if(!vis[k.v])
		{
			cc++;
			ans+=d[k.v];
			vis[k.v]=true;
			for(int i=0;i<mat[k.v].size();i++)
				if(!vis[mat[k.v][i].v]&&mat[k.v][i].w<d[mat[k.v][i].v])
				{
					d[mat[k.v][i].v]=mat[k.v][i].w;
					q.push((node){mat[k.v][i].v,d[mat[k.v][i].v]});
				}
		}
	}
	if(cc==n)
		printf("%d\n",ans);
	else
		puts("orz");
	return 0;
}
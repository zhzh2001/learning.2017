#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int mat[5005][5005],d[5005];
bool vis[5005];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=(i==j?0:INF);
	while(m--)
	{
		int u=getint(),v=getint(),w=getint();
		if(w<mat[u][v])
			mat[u][v]=mat[v][u]=w;
	}
	memset(vis,false,sizeof(vis));
	for(int i=0;i<=n;i++)
		d[i]=(i==1?0:INF);
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		int k=0;
		for(int j=1;j<=n;j++)
			if(!vis[j]&&d[j]<d[k])
				k=j;
		if(k)
		{
			ans+=d[k];
			vis[k]=true;
			for(int j=1;j<=n;j++)
				if(!vis[j])
					d[j]=min(d[j],mat[k][j]);
		}
		else
		{
			cout<<"orz\n";
			break;
		}
	}
	cout<<ans<<endl;
	return 0;
}
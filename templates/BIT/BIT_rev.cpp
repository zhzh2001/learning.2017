#include<bits/stdc++.h>
using namespace std;
const int N=40005;
struct node
{
	int x,id;
	bool operator<(const node& b)const
	{
		return x<b.x;
	}
}a[N];
int n;
struct BIT
{
	int t[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			t[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=t[x];
		return ans;
	}
}T;
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].x;
		a[i].id=i;
	}
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		a[a[i].id].x=i;
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		T.modify(a[i].x,1);
		ans+=i-T.query(a[i].x);
	}
	cout<<ans<<endl;
	return 0;
}
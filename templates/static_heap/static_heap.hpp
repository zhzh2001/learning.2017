#include <algorithm>
template <typename T, class Compare = less<T>>
struct static_heap
{
  protected:
	T *heap, *p;
	Compare cmp;
	size_t maxN;

  public:
	static_heap(size_t maxN) : maxN(maxN)
	{
		p = heap = new T[maxN];
	}

	~static_heap()
	{
		delete[] heap;
	}

	bool push(const T &val)
	{
		if (p == heap + maxN)
			return false;
		*p++ = val;
		std::push_heap(heap, p, cmp);
		return true;
	}

	const T &top() const
	{
		return *heap;
	}

	bool empty()
	{
		return p == heap;
	}

	bool pop()
	{
		if (empty())
			return false;
		std::pop_heap(heap, p--, cmp);
		return true;
	}
};
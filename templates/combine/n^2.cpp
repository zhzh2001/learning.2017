#include<bits/stdc++.h>
using namespace std;
const int p=10007,N=1005;
int f[N][N];
int main()
{
	int a,b,k,n,m;
	cin>>a>>b>>k>>n>>m;
	f[0][0]=1;
	for(int i=1;i<=k;i++)
	{
		f[i][0]=f[i][i]=1;
		for(int j=1;j<i;j++)
			f[i][j]=(f[i-1][j]+f[i-1][j-1])%p;
	}
	long long ans=1;
	for(int i=1;i<=n;i++)
		ans=ans*a%p;
	for(int i=1;i<=m;i++)
		ans=ans*b%p;
	cout<<ans*f[k][n]%p<<endl;
	return 0;
}
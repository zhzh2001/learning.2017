#include<bits/stdc++.h>
using namespace std;
const int N=1000,r=10007;
int p[N/5],mp[N+5],cnt[N+5];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%r;
		a=a*a%r;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int a,b,k,n,m;
	cin>>a>>b>>k>>n>>m;
	
	int pn=0;
	for(int i=2;i<=k;i++)
	{
		if(!mp[i])
		{
			mp[i]=i;
			p[++pn]=i;
		}
		for(int j=1;j<=pn&&i*p[j]<=k;j++)
		{
			mp[i*p[j]]=p[j];
			if(i%p[j]==0)
				break;
		}
	}
	
	for(int i=k-n+1;i<=k;i++)
		cnt[i]++;
	for(int i=1;i<=n;i++)
		cnt[i]--;
	for(int i=k;i>1;i--)
		if(mp[i]<i)
		{
			cnt[mp[i]]+=cnt[i];
			cnt[i/mp[i]]+=cnt[i];
		}
	int ans=1;
	for(int i=2;i<=k;i++)
		if(mp[i]==i)
			ans=ans*qpow(i,cnt[i])%r;
	
	cout<<qpow(a,n)*qpow(b,m)%r*ans%r<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005,p=10007,inv=p-2;
int f[N];
long long qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int a,b,k,n,m;
	cin>>a>>b>>k>>n>>m;
	f[0]=1;
	for(int i=1;i<=n;i++)
		f[i]=f[i-1]*(k-i+1)%p*qpow(i,inv)%p;
	cout<<qpow(a,n)*qpow(b,m)%p*f[n]%p<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int f[10005];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint();
	for(int i=1;i<=n;i++)
		f[i]=i;
	while(m--)
	{
		int z=getint(),x=getint(),y=getint(),rx=getf(x),ry=getf(y);
		if(z==1)
			f[rx]=ry;
		else
			if(rx==ry)
				puts("Y");
			else
				puts("N");
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=500005,LOGN=20,M=1000005,FIN=15000000,FOUT=4000000;
char ibuf[FIN],obuf[FOUT],*fin,*fout;
struct node
{
	int v,next;
};
node e[M];
int en,dep[N],f[N][LOGN],head[N];
bool vis[N];
inline int getint()
{
	while(!isdigit(*(++fin)));
	int ret=*fin-'0';
	while(isdigit(*(++fin)))
		ret=ret*10+*fin-'0';
	return ret;
}
int tmp[10];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++fout)=tmp[len]+'0';
}
inline void ins(int u,int v)
{
	e[++en].v=v;e[en].next=head[u];
	head[u]=en;
}
void dfs(int k)
{
	vis[k]=true;
	for(int i=1;(1<<i)<=dep[k];i++)
		f[k][i]=f[f[k][i-1]][i-1];
	for(int i=head[k];i;i=e[i].next)
		if(!vis[e[i].v])
		{
			dep[e[i].v]=dep[k]+1;
			f[e[i].v][0]=k;
			dfs(e[i].v);
		}
}
int lca(int x,int y)
{
	if(dep[x]>dep[y])
		swap(x,y);
	int delta=dep[y]-dep[x];
	for(int i=0;delta;i++,delta>>=1)
		if(delta&1)
			y=f[y][i];
	if(x==y)
		return x;
	int i;
	for(i=0;f[x][i]!=f[y][i];i++);
	for(;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
int main()
{
	fread(ibuf,1,FIN,stdin);
	fin=ibuf-1;
	int n=getint(),m=getint(),r=getint();
	en=0;
	for(int i=1;i<n;i++)
	{
		int u=getint(),v=getint();
		ins(u,v);
		ins(v,u);
	}
	dfs(r);
	f[r][0]=r;
	fout=obuf-1;
	while(m--)
	{
		int x=getint(),y=getint();
		putint(lca(x,y));
		*(++fout)='\n';
	}
	fwrite(obuf,1,fout-obuf+1,stdout);
	return 0;
}
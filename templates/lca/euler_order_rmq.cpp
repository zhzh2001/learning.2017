#include<bits/stdc++.h>
using namespace std;
const int N=500005,LOG=20,M=1000005,FIN=14000000,FOUT=3500000;
char ibuf[FIN],obuf[FOUT],*fin,*fout;
struct node
{
	int v,next;
};
node e[M];
int en,dep[N],head[N],euler[M],st[M][LOG],first[N];
bool vis[N];
inline int getint()
{
	while(!isdigit(*(++fin)));
	int ret=*fin-'0';
	while(isdigit(*(++fin)))
		ret=ret*10+*fin-'0';
	return ret;
}
int tmp[10];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++fout)=tmp[len]+'0';
}
inline void ins(int u,int v)
{
	e[++en].v=v;e[en].next=head[u];
	head[u]=en;
}
void dfs(int k)
{
	vis[k]=true;
	euler[++en]=k;
	st[en][0]=k;
	first[k]=en;
	for(int i=head[k];i;i=e[i].next)
		if(!vis[e[i].v])
		{
			dep[e[i].v]=dep[k]+1;
			dfs(e[i].v);
			euler[++en]=k;
			st[en][0]=k;
		}
}
inline int rmq(int l,int r)
{
	int k=log2(r-l+1);
	if(dep[st[l][k]]<dep[st[r-(1<<k)+1][k]])
		return st[l][k];
	else
		return st[r-(1<<k)+1][k];
}
inline int lca(int x,int y)
{
	if(first[x]>first[y])
		swap(x,y);
	return rmq(first[x],first[y]);
}
int main()
{
	fread(ibuf,1,FIN,stdin);
	fin=ibuf-1;
	int n=getint(),m=getint(),r=getint();
	en=0;
	for(int i=1;i<n;i++)
	{
		int u=getint(),v=getint();
		ins(u,v);
		ins(v,u);
	}
	en=0;
	dfs(r);
	for(int i=2*n-1;i;i--)
		for(int j=1;i+(1<<j)<2*n;j++)
			if(dep[st[i][j-1]]<dep[st[i+(1<<(j-1))][j-1]])
				st[i][j]=st[i][j-1];
			else
				st[i][j]=st[i+(1<<(j-1))][j-1];
	fout=obuf-1;
	while(m--)
	{
		int x=getint(),y=getint();
		putint(lca(x,y));
		*(++fout)='\n';
	}
	fwrite(obuf,1,fout-obuf+1,stdout);
	return 0;
}
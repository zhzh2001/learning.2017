#include<bits/stdc++.h>
using namespace std;
const int N=500005,M=1000005;
char buf[1000020],*now,*bufend;
int m,head[N],next[M],v[M],head2[N],next2[M],v2[M],id[M],ans[N],f[N];
bool vis[N];
inline void ins(int u,int v)
{
	::v[++m]=v;
	next[m]=head[u];
	head[u]=m;
}
inline void ins2(int u,int v,int id)
{
	v2[++m]=v;
	::id[m]=id;
	next2[m]=head2[u];
	head2[u]=m;
}
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
void dfs(int k)
{
	vis[k]=true;
	f[k]=k;
	for(int i=head[k];i;i=next[i])
		if(!vis[v[i]])
		{
			dfs(v[i]);
			f[v[i]]=k;
		}
	for(int i=head2[k];i;i=next2[i])
		if(vis[v2[i]])
			ans[id[i]]=getf(v2[i]);
}
inline int getint()
{
	if(bufend-now<20)
	{
		memcpy(buf,now,bufend-now);
		fread(buf+(bufend-now),1,1000000,stdin);
		bufend=buf+(bufend-now)+1000000;
		now=buf-1;
	}
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int tmp[15];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++now)=tmp[len]+'0';
	if(now-buf>1000000)
	{
		fwrite(buf,1,now-buf+1,stdout);
		now=buf-1;
	}
}
int main()
{
	now=buf+1000000;
	bufend=now;
	int n=getint(),m=getint(),s=getint();
	::m=0;
	for(int i=1;i<n;i++)
	{
		int u=getint(),v=getint();
		ins(u,v);
		ins(v,u);
	}
	::m=0;
	for(int i=1;i<=m;i++)
	{
		int u=getint(),v=getint();
		ins2(u,v,i);
		ins2(v,u,i);
	}
	dfs(s);
	now=buf-1;
	for(int i=1;i<=m;i++)
	{
		putint(ans[i]);
		*(++now)='\n';
	}
	fwrite(buf,1,now-buf+1,stdout);
	return 0;
}
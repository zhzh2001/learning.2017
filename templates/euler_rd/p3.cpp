#include<bits/stdc++.h>
using namespace std;
bool vis[52],mat[52][52];
int cnt[52];
stack<int> ans;
inline int alpha2int(char c)
{
	if(isupper(c))
		return c-'A';
	return c-'a'+26;
}
inline char int2alpha(int c)
{
	if(c<26)
		return c+'A';
	return c-26+'a';
}
void dfs(int k)
{
	for(int i=0;i<52;i++)
		if(mat[k][i])
		{
			mat[k][i]=mat[i][k]=false;
			dfs(i);
		}
	ans.push(k);
}
int main()
{
	int m;
	cin>>m;
	while(m--)
	{
		string s;
		cin>>s;
		int u=alpha2int(s[0]),v=alpha2int(s[1]);
		vis[u]=vis[v]=true;
		mat[u][v]=mat[v][u]=true;
		cnt[u]++;cnt[v]++;
	}
	int cc=0;
	for(int i=0;i<52;i++)
		if(cnt[i]&1)
			cc++;
	if(cc>2)
	{
		cout<<"No Solution\n";
		return 0;
	}
	if(cc==2)
	{
		for(int i=0;i<52;i++)
			if(cnt[i]&1)
			{
				dfs(i);
				break;
			}
	}
	else
		for(int i=0;i<52;i++)
			if(vis[i])
			{
				dfs(i);
				break;
			}
	while(!ans.empty())
	{
		cout.put(int2alpha(ans.top()));
		ans.pop();
	}
	cout<<endl;
	return 0;
}
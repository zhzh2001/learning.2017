program sort;
{$inline on}
const
  maxn=1000000;
  maxbuf=10500000;
var
  fin,fout:file;
  fsize:int64;
  buf:array[1..maxbuf]of byte;
  p:^byte;
  n,i,res,t:longint;
  a:array[1..maxn]of longint;
  tmp:array[1..10]of longint;

procedure getint(var x:longint);inline;
var
  sign:longint;
begin
  while((p^<48)or(p^>57))and(p^<>45)do
    inc(p);
  sign:=1;
  if p^=45 then
  begin
    sign:=-1;
    inc(p);
  end;
  x:=0;
  while(p^>=48)and(p^<=57)do
  begin
    x:=x*10+p^-48;
    inc(p);
  end;
  x:=x*sign;
end;

procedure putint(x:longint);inline;
var
  len:longint;
begin
  if x<0 then
  begin
    p^:=45;
    inc(p);
    x:=-x;
  end;
  len:=0;
  repeat
    inc(len);
    tmp[len]:=x mod 10;
    x:=x div 10;
  until x=0;
  while len>0 do
  begin
    p^:=tmp[len]+48;
    inc(p);
    dec(len);
  end;
end;

procedure sort(l,r: longint);
var
  i,j,x,y: longint;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do inc(i);
    while x<a[j] do dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then sort(l,j);
  if i<r then sort(i,r);
end;

begin
  randomize;
  t:=randseed;
  assign(fin,'sort.in');
  reset(fin,1);
  assign(fout,'sort.out');
  rewrite(fout,1);

  fsize:=filesize(fin);
  blockread(fin,buf,fsize,res);

  p:=@buf;
  getint(n);
  for i:=1 to n do
    getint(a[i]);

  sort(1,n);

  p:=@buf;
  for i:=1 to n do
  begin
    putint(a[i]);
    p^:=32;
    inc(p);
  end;
  p^:=10;
  inc(p);
  blockwrite(fout,buf,p-@buf,res);

  close(fin);
  close(fout);

  randomize;
  writeln('time=',randseed-t,'ms');
end.

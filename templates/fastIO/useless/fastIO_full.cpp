#include<cstdio>
#include<cctype>
#include<algorithm>
#include<ctime>
using namespace std;
char *p;

inline void read(int& x)
{
	for(;isspace(*p);p++);
	int sign=1;x=0;
	if(*p=='-')
		sign=-1,p++;
	for(;isdigit(*p);p++)
		x=x*10+*p-'0';
	x*=sign;
}

int tmp[12];

inline void write(int x)
{
	if(x<0)
		*p++='-',x=-x;
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*p++=tmp[len]+'0';
}

int main(int argc,char* argv[])
{
	if(argc<3)
		return 1;
	FILE *fin=fopen(argv[1],"r");
	FILE *fout=fopen(argv[2],"w");
	
	fseek(fin,0,SEEK_END);
	long fsize=ftell(fin);
	char *buf=new char [fsize];
	
	fseek(fin,0,SEEK_SET);
	fread(buf,1,fsize,fin);
	
	p=buf;
	int n;
	read(n);
	int *a=new int [n];
	for(int i=0;i<n;i++)
		read(a[i]);
	
	sort(a,a+n);
	
	p=buf;
	for(int i=0;i<n;i++)
	{
		write(a[i]);
		*p++=' ';
	}
	*p++='\n';
	fwrite(buf,1,p-buf,fout);
	
	printf("%ld\n",clock());
	return 0;
}
/*
block_old.cpp
仅供测试 同fastIO_int.cpp
*/
#include<cstdio>
#include<cctype>
#include<ctime>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAXN=10000005;
FILE *fin,*fout;
char buf[1000025],*now,*bufend;
int a[MAXN];
inline int getint()
{
	if(bufend-now<20)
	{
		memcpy(buf,now,bufend-now);
		fread(buf+(bufend-now),1,1000000,fin);
		bufend=buf+(bufend-now)+1000000;
		now=buf-1;
	}
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int tmp[15];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++now)=tmp[len]+'0';
	if(now-buf>1000000)
	{
		fwrite(buf,1,now-buf+1,fout);
		now=buf-1;
	}
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	now=buf+1000000;
	bufend=buf+1000000;
	int n=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	sort(a+1,a+n+1);
	now=buf-1;
	for(int i=1;i<=n;i++)
	{
		putint(a[i]);
		*(++now)=' ';
	}
	fwrite(buf,1,now-buf+1,fout);
	printf("%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
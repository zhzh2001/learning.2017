/*
fastIO.cpp
author:zhzh2001
从模板修改
*/
#include<bits/stdc++.h>
using namespace std;
FILE *fin,*fout;
//输入/输出文件
namespace fastI
{
	//fread based
	const int SIZE=1000000;
	//定义缓冲区大小
	typedef long long ll;
	char buf[SIZE],*p1=buf+SIZE,*pend=buf+SIZE;
	//buf为缓冲区 p1为当前指针 pend为缓冲区结尾
	
	/*
	int nc();
	从缓冲区读取一个字符并转换为int
	若缓冲区用完则从fin读取
	若文件结尾返回EOF
	*/
	inline int nc()
	{
		if (p1==pend)
		{
			p1=buf;
			pend=buf+fread(buf,1,SIZE,fin);
			if (pend==p1)
				return EOF;
		}
		return *p1++;
	}
	
	/*
	void read(int&);
	void read(ll&);
	从缓冲区读取一个整数 跳过空白
	*/
	inline void read(int& x)
	{
		bool neg=false; int ch=nc(); x=0;
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		if (ch=='-')neg=true,ch=nc();
		for (;isdigit(ch);ch=nc())x=x*10+ch-'0';
		if (neg)x=-x;
	}
	inline void read(ll& x)
	{
		bool neg=false; int ch=nc(); x=0;
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		if (ch=='-')neg=true,ch=nc();
		for (;isdigit(ch);ch=nc())x=x*10+ch-'0';
		if (neg)x=-x;
	}
	
	/*
	void read(double&);
	从缓冲区读取一个浮点数 跳过空白
	*/
	inline void read(double& x)
	{
		bool neg=false; int ch=nc(); x=0;
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		if (ch=='-')neg=true,ch=nc();
		for (;isdigit(ch);ch=nc())x=x*10+ch-'0';
		if (ch=='.')
		{
			double tmp=1; ch=nc();
			for (;isdigit(ch);ch=nc())tmp/=10,x+=tmp*(ch-'0');
		}
		if (neg)x=-x;
	}
	
	/*
	void read(char*);
	void read(char&);
	从缓冲区读取一个字符串或字符 跳过空白
	*/
	inline void read(char* s)
	{
		int ch=nc();
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		for (;!isspace(ch)&&ch!=EOF;ch=nc())*s++=ch;
		*s='\0';
	}
	inline void read(char& c)
	{
		int ch=nc();
		for(;isspace(ch);ch=nc());
		if(ch==EOF)
			return;
		c=ch;
	}
	
	/*
	void readln();
	跳过当前行剩余内容
	*/
	inline void readln()
	{
		for(char c=nc();c!='\n'&&c!=EOF;c=nc());
	}
	
	/*
	void readln(T&);
	读取任意内容并跳过当前行剩余内容
	*/
	template<typename T>
	inline void readln(T& x)
	{
		read(x);
		readln();
	}
};
	
namespace fastO
{
	//fwrite based
	const int SIZE=1000000;
	typedef long long ll;
	char buf[SIZE],*p1=buf,*pend=buf+SIZE;
	inline void out(char ch)
	{
		if (p1==pend)
		{
			fwrite(buf,1,SIZE,fout);
			p1=buf;
		}
		*p1++=ch;
	}
	inline void write(int x)
	{
		static char s[15],*s1;s1=s;
		if (!x)
			out('0');
		if (x<0)
			out('-'),x=-x;
		while(x)
			*s1++=x%10+'0',x/=10;
		while(s1--!=s)
			out(*s1);
	}
	inline void write(ll x)
	{
		static char s[25],*s1;s1=s;
		if (!x)
			out('0');
		if (x<0)
			out('-'),x=-x;
		while(x)
			*s1++=x%10+'0',x/=10;
		while(s1--!=s)
			out(*s1);
	}
	/*
	void write(double,int=6);
	向缓冲区写入一个浮点数 保留若干位小数
	*/
	inline void write(double x,int y=6)
	{
		static ll mul[]={1,10,100,1000,10000,100000,1000000,10000000,100000000,
		1000000000,10000000000LL,100000000000LL,1000000000000LL,10000000000000LL,
		100000000000000LL,1000000000000000LL,10000000000000000LL,100000000000000000LL};
		if (x<-1e-12)
			out('-'),x=-x;x*=mul[y];
		ll x1=floor(x);
		if (x-x1>=0.5)
			++x1;
		ll x2=x1/mul[y],x3=x1-x2*mul[y];
		write(x2);
		if (y)
		{
			out('.');
			for (int i=1;i<y&&x3*mul[i]<mul[y];out('0'),i++); 
			write(x3);
		}
	}
	inline void write(char* s)
	{
		while (*s)
			out(*s++);
	}
	inline void write(char c)
	{
		out(c);
	}
	/*
	void flush();
	将缓冲区中的内容写入文件
	*/
	inline void flush()
	{
		if (p1!=buf)
		{
			fwrite(buf,1,p1-buf,fout);
			p1=buf;
		}
	}
	
	struct AutoObject
	{
		AutoObject(){}
		~AutoObject()
		{
			flush();
		}
	}AO;
	
	inline void writeln()
	{
		out('\n');
		//flush();
	}
	
	template<typename T>
	inline void writeln(T x)
	{
		write(x);
		writeln();
	}
	inline void writeln(double x,int y)
	{
		write(x,y);
		writeln();
	}
};

namespace fastIO
{
	using namespace fastI;
	using namespace fastO;
};

using namespace fastIO;
int main()
{
	fin=fopen("test.in","r");
	fout=fopen("test.out","w");
	int n;
	read(n);
	writeln(n);
	long long x;
	read(x);
	writeln(x);
	char s[105];
	read(s);
	writeln(s);
	readln(s);
	writeln(s);
	double pi;
	read(pi);
	write("pi=");
	writeln(pi);
	writeln(pi,2);
	writeln(pi,17);
	char c;
	read(c);read(c);read(c);read(c);read(c);
	writeln(c);
	return 0;
}
/*
注意：
1.read当到达文件结尾时，不会修改参数值。
2.write(double)值太大时会溢出long long，或保留位数过大也会溢出。
3.write(double)默认保留6位小数。
*/

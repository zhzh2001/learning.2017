/*
fastIO_struct.cpp
author:zhzh2001
将fastIO封装为fastIstream/fastOstream 速度较慢
*/
#include<bits/stdc++.h>
#define inline __attribute__((always_inline))
using namespace std;
typedef long long ll;
struct fastIstream
{
	//fread based
	FILE *f;
	int sz;
	char *buf,*p1,*pend;
	fastIstream(const char *fname,int size=1000000)
	{
		f=fopen(fname,"r");
		sz=size;
		buf=new char [sz];
		p1=buf+sz;
		pend=buf+sz;
	}
	~fastIstream()
	{
		fclose(f);
		delete [] buf;
	}
	
	inline int nc()
	{
		if (p1==pend)
		{
			p1=buf;
			pend=buf+fread(buf,1,sz,f);
			if (pend==p1)
				return EOF;
		}
		return *p1++;
	}
	
	inline void readln()
	{
		for(char c=nc();c!='\n'&&c!=EOF;c=nc());
	}
	template<typename T>
	inline void readln(T& x)
	{
		*this>>x;
		readln();
	}
};

inline fastIstream& operator>>(fastIstream& fi,int& x)
{
	bool neg=false; x=0;int ch=fi.nc();
	for (;isspace(ch);ch=fi.nc());
	if (ch==EOF)return fi;
	if (ch=='-')neg=true,ch=fi.nc();
	for (;isdigit(ch);ch=fi.nc())x=x*10+ch-'0';
	if (neg)x=-x;
	return fi;
}

inline fastIstream& operator>>(fastIstream& fi,ll& x)
{
	bool neg=false; x=0;int ch=fi.nc();
	for (;isspace(ch);ch=fi.nc());
	if (ch==EOF)return fi;
	if (ch=='-')neg=true,ch=fi.nc();
	for (;isdigit(ch);ch=fi.nc())x=x*10+ch-'0';
	if (neg)x=-x;
	return fi;
}

inline fastIstream& operator>>(fastIstream& fi,double& x)
{
	bool neg=false; x=0;int ch=fi.nc();
	for (;isspace(ch);ch=fi.nc());
	if (ch==EOF)return fi;
	if (ch=='-')neg=true,ch=fi.nc();
	for (;isdigit(ch);ch=fi.nc())x=x*10+ch-'0';
	if (ch=='.')
	{
		double tmp=1;
		for (ch=fi.nc();isdigit(ch);ch=fi.nc())tmp/=10,x+=tmp*(ch-'0');
	}
	if (neg)x=-x;
	return fi;
}

inline fastIstream& operator>>(fastIstream& fi,char* s)
{
	int ch=fi.nc();
	for (;isspace(ch);ch=fi.nc());
	if (ch==EOF)return fi;
	for (;!isspace(ch)&&ch!=EOF;ch=fi.nc())*s++=ch;
	*s='\0';
	return fi;
}

inline fastIstream& operator>>(fastIstream& fi,char& c)
{
	int ch=fi.nc();
	for(;isspace(ch);ch=fi.nc());
	if(ch==EOF)
		return fi;
	c=ch;
	return fi;
}
	
struct fastOstream
{
	//fwrite based
	FILE *f;
	int sz,prec;
	char *buf,*p1,*pend;
	fastOstream(const char *fname,int size=1000000)
	{
		f=fopen(fname,"w");
		sz=size;
		buf=new char [sz];
		p1=buf;
		pend=buf+sz;
		prec=6;
	}
	inline void flush()
	{
		if (p1!=buf)
		{
			fwrite(buf,1,p1-buf,f);
			p1=buf;
		}
	}
	~fastOstream()
	{
		flush();
		fclose(f);
		delete [] buf;
	}
	inline fastOstream& setp(int p=6)
	{
		prec=p;
		return *this;
	}
	inline void out(char ch)
	{
		if (p1==pend)
		{
			fwrite(buf,1,sz,f);
			p1=buf;
		}
		*p1++=ch;
	}
};

inline fastOstream& operator<<(fastOstream& fo,int x)
{
	static char s[15],*s1;s1=s;
	if (!x)
		fo.out('0');
	if (x<0)
		fo.out('-'),x=-x;
	while(x)
		*s1++=x%10+'0',x/=10;
	while(s1--!=s)
		fo.out(*s1);
	return fo;
}

inline fastOstream& operator<<(fastOstream& fo,ll x)
{
	static char s[25],*s1;s1=s;
	if (!x)
		fo.out('0');
	if (x<0)
		fo.out('-'),x=-x;
	while(x)
		*s1++=x%10+'0',x/=10;
	while(s1--!=s)
		fo.out(*s1);
	return fo;
}

inline fastOstream& operator<<(fastOstream& fo,double x)
{
	static ll mul[]={1,10,100,1000,10000,100000,1000000,10000000,100000000,
	1000000000,10000000000LL,100000000000LL,1000000000000LL,10000000000000LL,
	100000000000000LL,1000000000000000LL,10000000000000000LL,100000000000000000LL};
	if (x<-1e-12)
		fo.out('-'),x=-x;x*=mul[fo.prec];
	ll x1=floor(x);
	if (x-x1>=0.5)
		++x1;
	ll x2=x1/mul[fo.prec],x3=x1-x2*mul[fo.prec];
	fo<<x2;
	if (fo.prec)
	{
		fo.out('.');
		for (int i=1;i<fo.prec&&x3*mul[i]<mul[fo.prec];fo.out('0'),i++); 
		fo<<x3;
	}
	return fo;
}

inline fastOstream& operator<<(fastOstream& fo,char* s)
{
	while (*s)
		fo.out(*s++);
	return fo;
}

inline fastOstream& operator<<(fastOstream& fo,char c)
{
	fo.out(c);
	return fo;
}

inline fastOstream& operator<<(fastOstream& fo,fastOstream& (*func)(fastOstream&))
{
	func(fo);
	return fo;
}

inline fastOstream& endl(fastOstream& fo)
{
	#ifdef _WIN32
	fo.out('\r');
	#endif
	fo.out('\n');
	//flush();
	return fo;
}

fastIstream fin("int.in");
fastOstream fout("int.out");
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		int x;
		fin>>x;
		fout<<x<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
bool p[10000005];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint();
	p[1]=true;
	for(int i=2;i*i<=n;i++)
		if(!p[i])
			for(int j=i*i;j<=n;j+=i)
				p[j]=true;
	while(m--)
	{
		int x=getint();
		if(!p[x])
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}
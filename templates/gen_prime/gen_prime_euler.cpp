#include<bits/stdc++.h>
using namespace std;
int p[1000005];
bool bl[10000005];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint(),pn=0;
	for(int i=2;i<=n;i++)
	{
		if(!bl[i])
			p[++pn]=i;
		for(int j=1;j<=pn&&p[j]*i<=n;j++)
		{
			bl[p[j]*i]=true;
			if(i%p[j]==0)
				break;
		}
	}
	while(m--)
	{
		int x=getint();
		if(binary_search(p+1,p+pn+1,x))
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}
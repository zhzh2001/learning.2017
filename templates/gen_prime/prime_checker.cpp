#include<bits/stdc++.h>
using namespace std;
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
bool prime(int x)
{
	if(x<2)
		return false;
	for(int i=2;i*i<=x;i++)
		if(x%i==0)
			return false;
	return true;
}
int main()
{
	int n=getint(),m=getint();
	while(m--)
	{
		int x=getint();
		if(prime(x))
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}
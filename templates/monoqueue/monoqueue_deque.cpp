#include<cstdio>
#include<cctype>
#include<cstdlib>
#include<queue>
using namespace std;
int a[1000005];
struct node
{
	int x,p;
	node(int x,int p):x(x),p(p){};
};
deque<node> q;
inline int getint()
{
	char c;
	while(!isdigit(c=getchar())&&c!='-');
	bool neg=false;
	int ret=0;
	if(c=='-')
		neg=true;
	else
		ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return neg?-ret:ret;
}
inline void putint(int x)
{
	if(x<0)
		putchar('-');
	x=abs(x);
	int buf[15],p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
int main()
{
	int n=getint(),k=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	for(int i=1;i<=n;i++)
	{
		while(!q.empty()&&i-q.front().p>=k)
			q.pop_front();
		while(!q.empty()&&q.back().x>=a[i])
			q.pop_back();
		node tmp(a[i],i);
		q.push_back(tmp);
		if(i>=k)
		{
			putint(q.front().x);
			putchar(' ');
		}
	}
	while(!q.empty())
		q.pop_back();
	puts("");
	for(int i=1;i<=n;i++)
	{
		while(!q.empty()&&i-q.front().p>=k)
			q.pop_front();
		while(!q.empty()&&q.back().x<=a[i])
			q.pop_back();
		node tmp(a[i],i);
		q.push_back(tmp);
		if(i>=k)
		{
			putint(q.front().x);
			putchar(' ');
		}
	}
	puts("");
	return 0;
}
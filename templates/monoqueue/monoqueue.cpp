#include<cstdio>
#include<cctype>
#include<cstdlib>
using namespace std;
int a[1000005];
struct node
{
	int x,p;
};
node q[1000005];
inline int getint()
{
	char c;
	while(!isdigit(c=getchar())&&c!='-');
	bool neg=false;
	int ret=0;
	if(c=='-')
		neg=true;
	else
		ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return neg?-ret:ret;
}
inline void putint(int x)
{
	if(x<0)
		putchar('-');
	x=abs(x);
	int buf[15],p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
int main()
{
	int n=getint(),k=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	int l=1,r=0;
	for(int i=1;i<=n;i++)
	{
		while(l<=r&&i-q[l].p>=k)
			l++;
		while(l<=r&&q[r].x>=a[i])
			r--;
		q[++r].x=a[i];q[r].p=i;
		if(i>=k)
		{
			putint(q[l].x);
			putchar(' ');
		}
	}
	puts("");
	l=1;r=0;
	for(int i=1;i<=n;i++)
	{
		while(l<=r&&i-q[l].p>=k)
			l++;
		while(l<=r&&q[r].x<=a[i])
			r--;
		q[++r].x=a[i];q[r].p=i;
		if(i>=k)
		{
			putint(q[l].x);
			putchar(' ');
		}
	}
	puts("");
	return 0;
}
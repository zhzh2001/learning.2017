#include<bits/stdc++.h>
using namespace std;
int g;
void exgcd(int a,int b,int& x,int& y)
{
	if(!b){g=a;x=1;y=0;}
	else{exgcd(b,a%b,y,x);y-=x*(a/b);}
}
int main()
{
	int a,p;
	cin>>a>>p;
	int x,y;
	exgcd(a,p,x,y);
	if(g>1)
	{
		cout<<'('<<a<<','<<p<<")>1\n";
		return 1;
	}
	cout<<(x%p+p)%p<<endl;
	return 0;
}
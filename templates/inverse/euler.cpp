#include<bits/stdc++.h>
using namespace std;
int gcd(int a,int b)
{
	return b==0?a:gcd(b,a%b);
}
int phi(int x)
{
	int ans=x;
	for(int i=2;i*i<=x;i++)
		if(x%i==0)
		{
			ans=ans/i*(i-1);
			while(x%i==0)
				x/=i;
		}
	if(x>1)
		ans=ans/x*(x-1);
	return ans;
}
int qpow(int a,int b,int p)
{
	int ans=1;
	do
	{
		if(b&1)
			ans=(long long)ans*a%p;
		a=(long long)a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int a,p;
	cin>>a>>p;
	if(gcd(a,p)>1)
	{
		cout<<'('<<a<<','<<p<<")>1\n";
		return 1;
	}
	cout<<qpow(a,phi(p)-1,p)<<endl;
	return 0;
}
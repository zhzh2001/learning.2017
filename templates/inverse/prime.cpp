#include<bits/stdc++.h>
using namespace std;
bool isprime(int p)
{
	for(int i=2;i*i<=p;i++)
		if(p%i==0)
			return false;
	return true;
}
int qpow(int a,int b,int p)
{
	int ans=1;
	do
	{
		if(b&1)
			ans=(long long)ans*a%p;
		a=(long long)a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int a,p;
	cin>>a>>p;
	if(!isprime(p))
	{
		cout<<p<<" is not a prime\n";
		return 1;
	}
	cout<<qpow(a,p-2,p)<<endl;
	return 0;
}
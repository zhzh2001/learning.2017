#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
struct node
{
	int v,w;
};
vector<node> mat[10005];
bool vis[10005];
int d[10005];
queue<int> q;
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint(),s=getint();
	while(m--)
	{
		int u=getint(),v=getint(),w=getint();
		mat[u].push_back((node){v,w});
	}
	memset(vis,false,sizeof(vis));
	memset(d,0x3f,sizeof(d));
	vis[s]=true;
	d[s]=0;
	q.push(s);
	while(!q.empty())
	{
		int k=q.front();q.pop();
		vis[k]=false;
		for(int i=0;i<mat[k].size();i++)
			if(d[k]+mat[k][i].w<d[mat[k][i].v])
			{
				d[mat[k][i].v]=d[k]+mat[k][i].w;
				if(!vis[mat[k][i].v])
				{
					vis[mat[k][i].v]=true;
					q.push(mat[k][i].v);
				}
			}
	}
	for(int i=1;i<=n;i++)
		if(d[i]==INF)
			printf("2147483647 ");
		else
			printf("%d ",d[i]);
	puts("");
	return 0;
}
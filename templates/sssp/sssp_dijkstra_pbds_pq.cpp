#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
int e=0,head[N],v[M],w[M],next[M];
inline void ins(int u,int v,int w)
{
	::v[++e]=v;::w[e]=w;
	next[e]=head[u];
	head[u]=e;
}
namespace dijkstra
{
	using namespace __gnu_pbds;
	const int INF=0x3f3f3f3f,INFM=0x3f;
	struct node
	{
		int v,w;
		node(int v,int w):v(v),w(w){};
		bool operator>(const node& b)const
		{
			return w>b.w;
		}
	};
	__gnu_pbds::priority_queue<node,greater<node>,pairing_heap_tag> Q;
	__gnu_pbds::priority_queue<node,greater<node>,pairing_heap_tag>::point_iterator it[N];
	int d[N];
	bool inq[N];
	void solve(int s)
	{
		memset(d,INFM,sizeof(d));
		d[s]=0;
		memset(inq,false,sizeof(inq));
		inq[s]=true;
		it[s]=Q.push(node(s,0));
		while(!Q.empty())
		{
			node k=Q.top();Q.pop();
			inq[k.v]=false;
			for(int i=head[k.v];i;i=next[i])
				if(d[k.v]+w[i]<d[v[i]])
				{
					d[v[i]]=d[k.v]+w[i];
					if(inq[v[i]])
						Q.modify(it[v[i]],node(v[i],d[v[i]]));
					else
						it[v[i]]=Q.push(node(v[i],d[v[i]]));
					inq[v[i]]=true;
				}
		}
	}
};
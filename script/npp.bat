rem cmd
cmd /k cd /d $(CURRENT_DIRECTORY)
rem compile&run
cmd /k cd /d $(CURRENT_DIRECTORY) & if $(EXT_PART)==.cpp (g++ $(FILE_NAME) -Wall & a.exe & del a.exe & pause & exit) else if $(EXT_PART)==.pas (fpc $(FILE_NAME) & $(NAME_PART).exe & del $(NAME_PART).exe $(NAME_PART).o & pause & exit)
rem debug
cmd /k cd /d $(CURRENT_DIRECTORY) & if $(EXT_PART)==.cpp (g++ $(FILE_NAME) -Wall -g & gdb a.exe & del a.exe & pause & exit) else if $(EXT_PART)==.pas (fpc $(FILE_NAME) -g & gdb $(NAME_PART).exe & del $(NAME_PART).exe $(NAME_PART).o & pause & exit)

#include<bits/stdc++.h>
using namespace std;
int main()
{
	cout<<"Enter target time(yyyy/mm/dd hh:mm:ss):";
	cout.flush();
	tm target;
	scanf("%d/%d/%d%d:%d:%d",&target.tm_year,&target.tm_mon,&target.tm_mday,&target.tm_hour,&target.tm_min,&target.tm_sec);
	printf("%04d/%02d/%02d %02d:%02d:%02d\n",target.tm_year,target.tm_mon,target.tm_mday,target.tm_hour,target.tm_min,target.tm_sec);
	target.tm_year-=1900;
	target.tm_mon--;
	time_t t=mktime(&target);
	cout<<"target time_t="<<t<<endl;
	
	minstd_rand gen(t);
	cout<<"Enter charset:";
	cout.flush();
	string cset;
	cin>>cset;
	string tset(cset);
	shuffle(tset.begin(),tset.end(),gen);
	cout<<"target charset="<<tset<<endl;
	
	map<char,char> enc;
	for(size_t i=0;i<cset.size();i++)
		enc[cset[i]]=tset[i];
	string s;
	cout<<"Enter string:";
	cout.flush();
	cin>>s;
	for(size_t i=0;i<s.length();i++)
	{
		assert(enc.find(s[i])!=enc.end());
		s[i]=enc[s[i]];
	}
	cout<<"target="<<s<<endl;
	return 0;
}
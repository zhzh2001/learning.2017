#include<bits/stdc++.h>
using namespace std;
ifstream fin("transitive.in");
ofstream fout("transitive.ans");
const int N=5005;
bool mat[N][N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			char c;
			fin>>c;
			mat[i][j]=c=='1';
		}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]|=mat[i][k]&&mat[k][j];
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			fout<<mat[i][j];
		fout<<endl;
	}
	return 0;
}
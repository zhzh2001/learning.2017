#include<bits/stdc++.h>
using namespace std;
ifstream fin("transitive.in");
ofstream fout("transitive.out");
const int N=5000;
bitset<N> mat[N];
int main()
{
	int n;
	fin>>n;
	for(int i=0;i<n;i++)
	{
		string s;
		fin>>s;
		reverse(s.begin(),s.end());
		stringstream ss(s);
		ss>>mat[i];
	}
	for(int k=0;k<n;k++)
		for(int i=0;i<n;i++)
			if(mat[i][k])
				mat[i]|=mat[k];
	for(int i=0;i<n;i++)
	{
		string s=mat[i].to_string();
		reverse(s.begin(),s.end());
		fout<<s.substr(0,n)<<endl;
	}
	return 0;
}
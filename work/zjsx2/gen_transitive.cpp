#include<bits/stdc++.h>
using namespace std;
ofstream fout("transitive.in");
const int N=5005;
bool mat[N][N];
int main()
{
	default_random_engine gen(time(NULL));
	int n;
	cin>>n;
	fout<<n<<endl;
	for(int i=1;i<=n;i++)
	{
		mat[i][i]=true;
		for(int j=i+1;j<=n;j++)
		{
			bernoulli_distribution d(0.001);
			mat[i][j]=mat[j][i]=d(gen);
		}
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			fout<<mat[i][j];
		fout<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005,LOGN=20,CNT=6666666;
int n,m,cc,a[N],root[N],tot;
struct node
{
	int cnt,ch[2];
	node():cnt(0)
	{
		ch[0]=ch[1]=0;
	}
}tree[CNT];
void insert(int& o,int l,int r,int val)
{
	if(!o)
		o=++tot;
	tree[o].cnt++;
	if(l<r)
	{
		int mid=(l+r)/2;
		if(val<=mid)
			insert(tree[o].ch[0],l,mid,val);
		else
			insert(tree[o].ch[1],mid+1,r,val);
	}
}
void erase(int o,int l,int r,int val)
{
	tree[o].cnt--;
	if(l<r)
	{
		int mid=(l+r)/2;
		if(val<=mid)
			erase(tree[o].ch[0],l,mid,val);
		else
			erase(tree[o].ch[1],mid+1,r,val);
	}
}

void modify(int x,int val,bool flag)
{
	for(;x<=n;x+=x&-x)
		if(flag)
			insert(root[x],0,cc-1,val);
		else
			erase(root[x],0,cc-1,val);
}
int rootl[LOGN],rootr[LOGN];
int cl,cr;
void init_point(int a[],int& cnt,int x)
{
	for(cnt=0;x;x-=x&-x)
		a[cnt++]=root[x];
}
void init_range(int l,int r)
{
	init_point(rootl,cl,l-1);
	init_point(rootr,cr,r);
}
int query(int a[],int cnt,int c)
{
	int ans=0;
	for(int i=0;i<cnt;i++)
		if(a[i])
			ans+=tree[a[i]].ch[c]?tree[tree[a[i]].ch[c]].cnt:0;
	return ans;
}
int get_cnt(int c)
{
	return query(rootr,cr,c)-query(rootl,cl,c);
}
void down(int a[],int cnt,int c)
{
	for(int i=0;i<cnt;i++)
		a[i]=a[i]?tree[a[i]].ch[c]:0;
}
void down(int c)
{
	down(rootl,cl,c);
	down(rootr,cr,c);
}
int get_rank(int _l,int _r,int x)
{
	init_range(_l,_r);
	int l=0,r=cc-1,ans=0;
	while(l<r)
	{
		int mid=(l+r)/2;
		if(x>mid)
		{
			ans+=get_cnt(0);
			down(1);
			l=mid+1;
		}
		else
		{
			down(0);
			r=mid;
		}
	}
	return ans+1;
}
int get_greater(int _l,int _r,int x)
{
	init_range(_l,_r);
	int l=0,r=cc-1,ans=0;
	while(l<r)
	{
		int mid=(l+r)/2;
		if(x<=mid)
		{
			ans+=get_cnt(1);
			down(0);
			r=mid;
		}
		else
		{
			down(1);
			l=mid+1;
		}
	}
	return ans;
}
int num[2*N];
int find_kth(int _l,int _r,int k)
{
	init_range(_l,_r);
	int l=0,r=cc-1;
	while(l<r)
	{
		int mid=(l+r)/2,t=get_cnt(0);
		if(k>t)
		{
			k-=t;
			down(1);
			l=mid+1;
		}
		else
		{
			down(0);
			r=mid;
		}
	}
	return num[l];
}
struct question
{
	int opt,x,y,k;
}q[N];
int main()
{
	scanf("%d%d",&n,&m);
	cc=0;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",a+i);
		num[cc++]=a[i];
	}
	for(int i=1;i<=m;i++)
	{
		scanf("%d",&q[i].opt);
		if(q[i].opt==3)
		{
			scanf("%d%d",&q[i].x,&q[i].y);
			num[cc++]=q[i].y;
		}
		else
		{
			scanf("%d%d%d",&q[i].x,&q[i].y,&q[i].k);
			if(q[i].opt!=2)
				num[cc++]=q[i].k;
		}
	}
	sort(num,num+cc);
	cc=unique(num,num+cc)-num;
	#define find(_) lower_bound(num,num+cc,_)-num
	for(int i=1;i<=n;i++)
		modify(i,a[i]=find(a[i]),true);
	int t;
	for(int i=1;i<=m;i++)
		switch(q[i].opt)
		{
			case 1:
				printf("%d\n",get_rank(q[i].x,q[i].y,find(q[i].k)));
				break;
			case 2:
				printf("%d\n",find_kth(q[i].x,q[i].y,q[i].k));
				break;
			case 3:
				modify(q[i].x,a[q[i].x],false);
				modify(q[i].x,a[q[i].x]=find(q[i].y),true);
				break;
			case 4:
				t=get_rank(q[i].x,q[i].y,find(q[i].k));
				if(t>1)
					printf("%d\n",find_kth(q[i].x,q[i].y,t-1));
				else
					puts("-2147483647");
				break;
			case 5:
				t=get_greater(q[i].x,q[i].y,find(q[i].k));
				if(t)
					printf("%d\n",find_kth(q[i].x,q[i].y,q[i].y-q[i].x-t+2));
				else
					puts("2147483647");
				break;
		}
	return 0;
}
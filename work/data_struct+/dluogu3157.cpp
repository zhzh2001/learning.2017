#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,m,a[N],ra[N];
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
struct node
{
	int val,sz;
	bool bl;
	node *ch[2];
	node(int val):val(val),sz(1),bl(true)
	{
		ch[0]=ch[1]=NULL;
	}
	void update()
	{
		sz=bl+(ch[0]?ch[0]->sz:0)+(ch[1]?ch[1]->sz:0);
	}
}*root[N];
void insert(node*& o,int val)
{
	if(!o)
		o=new node(val);
	else
	{
		insert(o->ch[val>o->val],val);
		o->update();
	}
}
void build(node*& o,int l,int r)
{
	if(l<=r)
	{
		int mid=(l+r)/2;
		insert(o,a[mid]);
		build(o,l,mid-1);
		build(o,mid+1,r);
	}
}
void erase(node* o,int val)
{
	if(o)
	{
		if(o->val==val)
			o->bl=false;
		else
			erase(o->ch[val>o->val],val);
		o->update();
	}
}
int query_less(node* o,int val)
{
	if(o)
	{
		if(o->val<val)
			return (o->ch[0]?o->ch[0]->sz:0)+o->bl+query_less(o->ch[1],val);
		if(o->val==val)
			return o->ch[0]?o->ch[0]->sz:0;
		return query_less(o->ch[0],val);
	}
	return 0;
}
int query_greater(node* o,int val)
{
	if(o)
	{
		if(o->val<val)
			return query_greater(o->ch[1],val);
		if(o->val==val)
			return o->ch[1]?o->ch[1]->sz:0;
		return (o->ch[1]?o->ch[1]->sz:0)+o->bl+query_greater(o->ch[0],val);
	}
	return 0;
}
int main()
{
	scanf("%d%d",&n,&m);
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",a+i);
		ra[a[i]]=i;
		T.modify(a[i],1);
		ans+=i-T.query(a[i]);
		build(root[i],i-(i&-i)+1,i);
	}
	while(m--)
	{
		printf("%lld\n",ans);
		int x;
		scanf("%d",&x);
		for(int i=ra[x];i;i-=i&-i)
			ans-=query_greater(root[i],x);
		for(int i=n;i;i-=i&-i)
			ans-=query_less(root[i],x);
		for(int i=ra[x];i;i-=i&-i)
			ans+=query_less(root[i],x);
		for(int i=ra[x];i<=n;i+=i&-i)
			erase(root[i],x);
	}
	return 0;
}
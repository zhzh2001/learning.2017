#include<cstdio>
#include<algorithm>
using namespace std;
const int N=100005;
struct node
{
	int val,pos;
	bool operator<(const node& rhs)const
	{
		return val<rhs.val;
	}
}a[N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&a[i].val);
		a[i].pos=i;
	}
	sort(a+1,a+n+1);
	while(m--)
	{
		int l,r,k;
		scanf("%d%d%d",&l,&r,&k);
		for(int i=1;i<=n;i++)
			if(a[i].pos>=l&&a[i].pos<=r&&!--k)
			{
				printf("%d\n",a[i].val);
				break;
			}
	}
	return 0;
}
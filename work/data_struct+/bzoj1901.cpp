#include<bits/stdc++.h>
using namespace std;
const int N=10005;
int a[N],b[N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	while(m--)
	{
		char opt[2];
		scanf("%1s",opt);
		if(opt[0]=='Q')
		{
			int l,r,k;
			scanf("%d%d%d",&l,&r,&k);
			memcpy(b,a+l,(r-l+1)*sizeof(int));
			nth_element(b,b+k-1,b+r-l+1);
			printf("%d\n",b[k-1]);
		}
		else
		{
			int i,x;
			scanf("%d%d",&i,&x);
			a[i]=x;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005;
namespace segment_tree
{
	struct node
	{
		unsigned sum,cover;
		node *ls,*rs;
		node():sum(0),cover(0),ls(NULL),rs(NULL){}
	}*root[N*8];
	void insert(node*& o,int l,int r,int tl,int tr)
	{
		if(!o)
			o=new node();
		if(tl<=l&&tr>=r)
		{
			o->sum+=r-l+1;
			o->cover++;
		}
		else
		{
			int mid=(l+r)/2;
			if(tl<=mid)
				insert(o->ls,l,mid,tl,tr);
			if(tr>mid)
				insert(o->rs,mid+1,r,tl,tr);
			o->sum=(o->ls?o->ls->sum:0)+(o->rs?o->rs->sum:0)+o->cover*(r-l+1);
		}
	}
	unsigned query(node* o,int l,int r,int tl,int tr)
	{
		if(!o)
			return 0;
		if(tl<=l&&tr>=r)
			return o->sum;
		else
		{
			int mid=(l+r)/2;
			return (tl<=mid?query(o->ls,l,mid,tl,tr):0)+(tr>mid?query(o->rs,mid+1,r,tl,tr):0)+o->cover*(min(tr,r)-max(tl,l)+1);
		}
	}
};
int n,m;
void insert(int l,int r,int val)
{
	int ll=0,rr=2*n,now=1;
	while(ll<rr)
	{
		segment_tree::insert(segment_tree::root[now],1,n,l,r);
		int mid=(ll+rr)/2;
		if(val<=mid)
		{
			rr=mid;
			now*=2;
		}
		else
		{
			ll=mid+1;
			now=now*2+1;
		}
	}
	segment_tree::insert(segment_tree::root[now],1,n,l,r);
}
int query(int l,int r,int val)
{
	int ll=0,rr=2*n,now=1;
	while(ll<rr)
	{
		unsigned t=segment_tree::query(segment_tree::root[now*2],1,n,l,r),mid=(ll+rr)/2;
		if(t>=val)
		{
			rr=mid;
			now*=2;
		}
		else
		{
			ll=mid+1;
			now=now*2+1;
			val-=t;
		}
	}
	return ll;
}
int main()
{
	scanf("%d%d",&n,&m);
	while(m--)
	{
		int opt,l,r,val;
		scanf("%d%d%d%d",&opt,&l,&r,&val);
		if(opt==1)
			insert(l,r,2*n-(val+n));
		else
			printf("%d\n",n-query(l,r,val));
	}
	return 0;
}
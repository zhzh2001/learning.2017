#include<bits/stdc++.h>
using namespace std;
const int N=50005;
namespace segment_tree
{
	struct node
	{
		unsigned sum,cover;
		node *ls,*rs;
		node():sum(0),cover(0),ls(NULL),rs(NULL){}
		void pushdown(int l,int r)
		{
			if(cover&&l<r)
			{
				int mid=(l+r)/2;
				if(!ls)
					ls=new node();
				ls->cover+=cover;
				ls->sum+=(mid-l+1)*cover;
				if(!rs)
					rs=new node();
				rs->cover+=cover;
				rs->sum+=(r-mid)*cover;
				cover=0;
			}
		}
		void update()
		{
			sum=(ls?ls->sum:0)+(rs?rs->sum:0);
		}
	}*root[N*8];
	void insert(node*& o,int l,int r,int tl,int tr)
	{
		if(!o)
			o=new node();
		if(tl<=l&&tr>=r)
		{
			o->sum+=r-l+1;
			o->cover++;
		}
		else
		{
			int mid=(l+r)/2;
			o->pushdown(l,r);
			if(tl<=mid)
				insert(o->ls,l,mid,tl,tr);
			if(tr>mid)
				insert(o->rs,mid+1,r,tl,tr);
			o->update();
		}
	}
	unsigned query(node* o,int l,int r,int tl,int tr)
	{
		if(!o)
			return 0;
		if(tl<=l&&tr>=r)
			return o->sum;
		else
		{
			int mid=(l+r)/2;
			o->pushdown(l,r);
			return (tl<=mid?query(o->ls,l,mid,tl,tr):0)+(tr>mid?query(o->rs,mid+1,r,tl,tr):0);
		}
	}
};
int n,m;
void insert(int l,int r,int val)
{
	int ll=0,rr=2*n,now=1;
	while(ll<rr)
	{
		segment_tree::insert(segment_tree::root[now],1,n,l,r);
		int mid=(ll+rr)/2;
		if(val<=mid)
		{
			rr=mid;
			now*=2;
		}
		else
		{
			ll=mid+1;
			now=now*2+1;
		}
	}
	segment_tree::insert(segment_tree::root[now],1,n,l,r);
}
int query(int l,int r,int val)
{
	int ll=0,rr=2*n,now=1;
	while(ll<rr)
	{
		unsigned t=segment_tree::query(segment_tree::root[now*2],1,n,l,r),mid=(ll+rr)/2;
		if(t>=val)
		{
			rr=mid;
			now*=2;
		}
		else
		{
			ll=mid+1;
			now=now*2+1;
			val-=t;
		}
	}
	return ll;
}
int main()
{
	scanf("%d%d",&n,&m);
	while(m--)
	{
		int opt,l,r,val;
		scanf("%d%d%d%d",&opt,&l,&r,&val);
		if(opt==1)
			insert(l,r,2*n-(val+n));
		else
			printf("%d\n",n-query(l,r,val));
	}
	return 0;
}
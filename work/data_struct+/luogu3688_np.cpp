#include<bits/stdc++.h>
using namespace std;
const int N=100005,p=998244353,inv=p-2,CNT=23333333;
int n,m,root[4*N],cc;
struct node
{
	int val,ls,rs;
	node():val(1),ls(0),rs(0){}
}tree[CNT];
inline int merge(long long p1,long long p2)
{
	return (p1*p2%p+(1-p1+p)*(1-p2+p)%p)%p;
}
void _insert(int& o,int l,int r,int ll,int rr,int val)
{
	if(!o)
		o=++cc;
	if(ll<=l&&rr>=r)
		tree[o].val=merge(tree[o].val,val);
	else
	{
		int mid=(l+r)/2;
		if(ll<=mid)
			_insert(tree[o].ls,l,mid,ll,rr,val);
		if(rr>mid)
			_insert(tree[o].rs,mid+1,r,ll,rr,val);
	}
}
void insert(int o,int l,int r,int l1,int r1,int l2,int r2,int val)
{
	if(l1<=l&&r1>=r)
		_insert(root[o],1,n,l2,r2,val);
	else
	{
		int mid=(l+r)/2;
		if(l1<=mid)
			insert(o*2,l,mid,l1,r1,l2,r2,val);
		if(r1>mid)
			insert(o*2+1,mid+1,r,l1,r1,l2,r2,val);
	}
}
int ans;
void _query(int o,int l,int r,int x)
{
	if(o)
	{
		ans=merge(ans,tree[o].val);
		if(l<r)
		{
			int mid=(l+r)/2;
			if(x<=mid)
				_query(tree[o].ls,l,mid,x);
			else
				_query(tree[o].rs,mid+1,r,x);
		}
	}
}
void query(int o,int l,int r,int x,int y)
{
	_query(root[o],1,n,y);
	if(l<r)
	{
		int mid=(l+r)/2;
		if(x<=mid)
			query(o*2,l,mid,x,y);
		else
			query(o*2+1,mid+1,r,x,y);
	}
}
int qpow(long long a,int b,int p)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b/=2);
	return ans;
}
void read(int& x)
{
	char c;
	for(c=getchar();isspace(c);c=getchar());
	int sign=1;
	if(c=='-')
		sign=-1,c=getchar();
	for(x=0;isdigit(c);c=getchar())
		x=x*10+c-'0';
	x*=sign;
}
int dig[15];
void writeln(int x)
{
	if(x<0)
		putchar('-'),x=-x;
	int p=0;
	do
		dig[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(dig[p]+'0');
	puts("");
}
int main()
{
	read(n);read(m);
	while(m--)
	{
		int opt,l,r;
		read(opt);read(l);read(r);
		if(opt==1)
		{
			int t=qpow(r-l+1,inv,p);
			if(l>1)
				insert(1,0,n,1,l-1,l,r,(1-t+p)%p);
			if(r<n)
				insert(1,0,n,l,r,r+1,n,(1-t+p)%p);
			insert(1,0,n,l,r,l,r,(1-(2*t%p)+p)%p);
			if(l>1)
				insert(1,0,n,0,0,1,l-1,0);
			if(r<n)
				insert(1,0,n,0,0,r+1,n,0);
			insert(1,0,n,0,0,l,r,t);
		}
		else
		{
			ans=1;
			query(1,0,n,l-1,r);
			writeln(ans);
		}
	}
	return 0;
}
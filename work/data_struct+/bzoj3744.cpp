#include<bits/stdc++.h>
using namespace std;
const int N=50005,SQRT=225;
//range:id[0,n) a[][1,n] root::id[1,n]
int n,a[N],b[N],f[SQRT][N],sz;
struct BIT
{
	int tree[N];
	void clear()
	{
		memset(tree,0,sizeof(tree));
	}
	void modify(int x)
	{
		for(;x;x-=x&-x)
			tree[x]++;
	}
	int query(int x)
	{
		int ans=0;
		for(;x<=n;x+=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
struct node
{
	int val;
	node *ls,*rs;
	node(int val,node *ls,node *rs):val(val),ls(ls),rs(rs){}
}*root[N],nil(0,NULL,NULL);
void insert(node*& o,node *pred,int l,int r,int val)
{
	if(!pred)
		pred=&nil;
	if(l==r)
		o=new node(pred->val+1,NULL,NULL);
	else
	{
		o=new node(pred->val+1,pred->ls,pred->rs);
		int mid=(l+r)/2;
		if(val<=mid)
			insert(o->ls,pred->ls,l,mid,val);
		else
			insert(o->rs,pred->rs,mid+1,r,val);
	}
}
int query(node *rl,node *rr,int l,int r,int L,int R)
{
	if(!rl)
		rl=&nil;
	if(!rr)
		rr=&nil;
	if(rl->val==rr->val)
		return 0;
	if(L<=l&&R>=r)
		return rr->val-rl->val;
	int mid=(l+r)/2;
	return (L<=mid?query(rl->ls,rr->ls,l,mid,L,R):0)+(R>mid?query(rl->rs,rr->rs,mid+1,r,L,R):0);
}
int query(int l,int r)
{
	l--;r--;
	int ans=0;
	if(l/sz==r/sz)
	{
		T.clear();
		for(int i=l;i<=r;i++)
		{
			ans+=T.query(a[i]+1);
			T.modify(a[i]);
		}
	}
	else
	{
		ans=f[l/sz+1][r];
		for(int i=l;i<l/sz*sz+sz;i++)
			ans+=query(root[i+1],root[r+1],1,n,1,a[i]-1);
	}
	return ans;
}
int main()
{
	scanf("%d",&n);
	for(int i=0;i<n;i++)
		scanf("%d",a+i);
	memcpy(b,a,sizeof(b));
	sort(b,b+n);
	for(int i=0;i<n;i++)
	{
		a[i]=lower_bound(b,b+n,a[i])-b+1;
		insert(root[i+1],root[i],1,n,a[i]);
	}
	sz=floor(sqrt(n));
	for(int i=0;i<n;i+=sz)
	{
		T.clear();
		int now=i/sz;
		for(int j=i;j<=n;j++)
		{
			f[now][j]=f[now][j-1]+T.query(a[j]+1);
			T.modify(a[j]);
		}
	}
	int m;
	scanf("%d",&m);
	int lastans=0;
	while(m--)
	{
		int l,r;
		scanf("%d%d",&l,&r);
		printf("%d\n",lastans=query(l^lastans,r^lastans));
	}
	return 0;
}
#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int a[305][305];
bool vis[305][305];
struct node
{
	int i,j,h;
};
node heap[90005];
bool cmp(node a,node b)
{
	return a.h>b.h;
}
int main()
{
	int n,m;
	cin>>m>>n;
	memset(vis,false,sizeof(vis));
	int hn=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			cin>>a[i][j];
			if(i==1||i==n||j==1||j==m)
			{
				heap[++hn].i=i;heap[hn].j=j;heap[hn].h=a[i][j];
				push_heap(heap+1,heap+hn+1,cmp);
				vis[i][j]=true;
			}
		}
	long long ans=0;
	while(hn)
	{
		pop_heap(heap+1,heap+hn+1,cmp);
		node k=heap[hn--];
		for(int i=0;i<4;i++)
		{
			int nx=k.i+dx[i],ny=k.j+dy[i];
			if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis[nx][ny])
			{
				if(a[nx][ny]<k.h)
				{
					ans+=k.h-a[nx][ny];
					heap[++hn].i=nx;heap[hn].j=ny;heap[hn].h=k.h;
					push_heap(heap+1,heap+hn+1,cmp);
				}
				else
				{
					heap[++hn].i=nx;heap[hn].j=ny;heap[hn].h=a[nx][ny];
					push_heap(heap+1,heap+hn+1,cmp);
				}
				vis[nx][ny]=true;
			}
		}
	}
	cout<<ans<<endl;
	return 0;
}
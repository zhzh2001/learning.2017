#include<iostream>
#include<vector>
#include<cstring>
using namespace std;
vector<int> a[1000005];
int cnt[65];
int main()
{
	int n,k;
	cin>>n>>k;
	int m=0;
	for(int i=1;i<=k;i++)
	{
		int t;
		cin>>t;
		while(t--)
		{
			int x;
			cin>>x;
			m=max(m,x);
			a[x].push_back(i);
		}
	}
	memset(cnt,0,sizeof(cnt));
	int cc=0,l=1,r=0,ans=n;
	while(r<=m)
	{
		if(cc<k)
		{
			r++;
			for(int i=0;i<a[r].size();i++)
			{
				if(cnt[a[r][i]]==0)
					cc++;
				cnt[a[r][i]]++;
			}
		}
		else
		{
			for(int i=0;i<a[l].size();i++)
			{
				cnt[a[l][i]]--;
				if(cnt[a[l][i]]==0)
					cc--;
			}
			l++;
		}
		if(cc==k)
			ans=min(ans,r-l);
		if(r==m&&cc<k)
			break;
	}
	cout<<ans<<endl;
	return 0;
}
#include<iostream>
#include<vector>
using namespace std;
int n,m,ans,a[100005],cc[1000005];
vector<int> q[1000005];
void qmerge(int x,int y)
{
	if(x==y)
		return;
	if(q[cc[x]].size()>q[cc[y]].size())
		swap(cc[x],cc[y]);
	for(int i=0;i<q[cc[x]].size();i++)
	{
		if(a[q[cc[x]][i]-1]==x)
			ans--;
		if(a[q[cc[x]][i]+1]==x)
			ans--;
		q[cc[y]].push_back(q[cc[x]][i]);
	}
	for(int i=0;i<q[cc[x]].size();i++)
		a[q[cc[x]][i]]=y;
	q[cc[x]].clear();
}
int main()
{
	cin>>n>>m;
	ans=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		q[a[i]].push_back(i);
		cc[a[i]]=a[i];
		if(a[i]!=a[i-1])
			ans++;
	}
	a[0]=a[n+1]=0;
	while(m--)
	{
		int opr;
		cin>>opr;
		if(opr==1)
		{
			int x,y;
			cin>>x>>y;
			qmerge(x,y);
		}
		else
			cout<<ans<<endl;
	}
	return 0;
}
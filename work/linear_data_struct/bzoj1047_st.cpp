#include<cstdio>
#include<algorithm>
#include<cctype>
#include<cstring>
using namespace std;
int a[1005][1005],f[1005][1005],g[1005][1005],g1[1005][1005],st[1005][12];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint(),k=getint(),kk=0;
	for(int t=1;(t<<1)<k;t<<=1)
		kk++;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=getint();
	for(int i=1;i<=n;i++)
	{
		memset(st,0,sizeof(st));
		for(int j=1;j<=m;j++)
			st[j][0]=a[i][j];
		for(int j=m;j>=1;j--)
			for(int k=1;j+(1<<(k-1))<=m;k++)
				st[j][k]=max(st[j][k-1],st[j+(1<<(k-1))][k-1]);
		for(int j=k;j<=m;j++)
			f[i][j]=max(st[j-k+1][kk],st[j-(1<<kk)+1][kk]);
	}
	for(int j=k;j<=m;j++)
	{
		memset(st,0,sizeof(st));
		for(int i=1;i<=n;i++)
			st[i][0]=f[i][j];
		for(int i=n;i>=1;i--)
			for(int k=1;i+(1<<(k-1))<=n;k++)
				st[i][k]=max(st[i][k-1],st[i+(1<<(k-1))][k-1]);
		for(int i=k;i<=n;i++)
			g[i][j]=max(st[i-k+1][kk],st[i-(1<<kk)+1][kk]);
	}
	
	for(int i=1;i<=n;i++)
	{
		memset(st,0,sizeof(st));
		for(int j=1;j<=m;j++)
			st[j][0]=a[i][j];
		for(int j=m;j>=1;j--)
			for(int k=1;j+(1<<(k-1))<=m;k++)
				st[j][k]=min(st[j][k-1],st[j+(1<<(k-1))][k-1]);
		for(int j=k;j<=m;j++)
			f[i][j]=min(st[j-k+1][kk],st[j-(1<<kk)+1][kk]);
	}
	for(int j=k;j<=m;j++)
	{
		memset(st,0,sizeof(st));
		for(int i=1;i<=n;i++)
			st[i][0]=f[i][j];
		for(int i=n;i>=1;i--)
			for(int k=1;i+(1<<(k-1))<=n;k++)
				st[i][k]=min(st[i][k-1],st[i+(1<<(k-1))][k-1]);
		for(int i=k;i<=n;i++)
			g1[i][j]=min(st[i-k+1][kk],st[i-(1<<kk)+1][kk]);
	}
	
	int ans=1e9;
	for(int i=k;i<=n;i++)
		for(int j=k;j<=m;j++)
			ans=min(ans,g[i][j]-g1[i][j]);
	printf("%d\n",ans);
	return 0;
}
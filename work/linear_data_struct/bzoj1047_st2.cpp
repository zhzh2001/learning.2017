#include<cstdio>
#include<cctype>
#include<algorithm>
using namespace std;
int a[1005][1005],st[1005][1005][12],st1[1005][1005][12];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint(),k=getint(),kk=0;
	for(int t=1;(t<<1)<k;t<<=1)
		kk++;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			st[i][j][0]=st1[i][j][0]=a[i][j]=getint();
	for(int i=n;i>=1;i--)
		for(int j=m;j>=1;j--)
			for(int k=1;i+(1<<(k-1))<=n&&j+(1<<(k-1))<=m;k++)
				st[i][j][k]=max(max(st[i][j][k-1],st[i+(1<<(k-1))][j][k-1]),max(st[i][j+(1<<(k-1))][k-1],st[i+(1<<(k-1))][j+(1<<(k-1))][k-1]));
	for(int i=n;i>=1;i--)
		for(int j=m;j>=1;j--)
			for(int k=1;i+(1<<(k-1))<=n&&j+(1<<(k-1))<=m;k++)
				st1[i][j][k]=min(min(st1[i][j][k-1],st1[i+(1<<(k-1))][j][k-1]),min(st1[i][j+(1<<(k-1))][k-1],st1[i+(1<<(k-1))][j+(1<<(k-1))][k-1]));
	int ans=1e9;
	for(int i=1;i+k-1<=n;i++)
		for(int j=1;j+k-1<=m;j++)
			ans=min(ans,max(max(st[i][j][kk],st[i+k-(1<<kk)][j][kk]),max(st[i][j+k-(1<<kk)][kk],st[i+k-(1<<kk)][j+k-(1<<kk)][kk]))-min(min(st1[i][j][kk],st1[i+k-(1<<kk)][j][kk]),min(st1[i][j+k-(1<<kk)][kk],st1[i+k-(1<<kk)][j+k-(1<<kk)][kk])));
	printf("%d\n",ans);
	return 0;
}
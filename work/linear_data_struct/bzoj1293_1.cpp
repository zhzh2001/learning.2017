#include<iostream>
#include<vector>
#include<cstring>
#include<algorithm>
using namespace std;
const int INF=0x7fffffff;
vector<int> a[65];
int first[65];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=k;i++)
	{
		int t;
		cin>>t;
		while(t--)
		{
			int x;
			cin>>x;
			a[0].push_back(x);
			a[i].push_back(x);
		}
	}
	sort(a[0].begin(),a[0].end());
	memset(first,0,sizeof(first));
	int ans=INF;
	for(int i=0;i<a[0].size();i++)
		if(!i||a[0][i]!=a[0][i-1])
		{
			int now=0;
			for(int j=1;j<=k;j++)
			{
				while(first[j]<a[j].size()&&a[j][first[j]]<a[0][i])
					first[j]++;
				if(first[j]==a[j].size())
				{
					now=-1;
					break;
				}
				else
					now=max(now,a[j][first[j]]-a[0][i]);
			}
			if(now>=0)
				ans=min(ans,now);
			else
				break;
		}
	cout<<ans<<endl;
	return 0;
}
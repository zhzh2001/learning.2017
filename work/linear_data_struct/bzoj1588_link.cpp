#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
struct node
{
	int x,i,pred,next;
};
node a[40000];
int ri[40000];
bool cmp(node a,node b)
{
	return a.x<b.x;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].x;
		a[i].i=i;
	}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++)
	{
		ri[a[i].i]=i;
		a[i].pred=i-1;
		a[i].next=i+1;
	}
	a[n].next=0;
	int ans=0;
	for(int i=n;i>1;i--)
	{
		int now=1e9;
		if(a[ri[i]].pred)
		{
			now=min(now,a[ri[i]].x-a[a[ri[i]].pred].x);
			a[a[ri[i]].pred].next=a[ri[i]].next;
		}
		if(a[ri[i]].next)
		{
			now=min(now,a[a[ri[i]].next].x-a[ri[i]].x);
			a[a[ri[i]].next].pred=a[ri[i]].pred;
		}
		a[ri[i]].pred=a[ri[i]].next=0;
		ans+=now;
	}
	cout<<ans+a[ri[1]].x<<endl;
	return 0;
}
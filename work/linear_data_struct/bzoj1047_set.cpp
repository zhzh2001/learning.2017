#include<cstdio>
#include<algorithm>
#include<set>
#include<cctype>
using namespace std;
int a[1005][1005],f[1005][1005],g[1005][1005],g1[1005][1005];
multiset<int,greater<int> > s1;
multiset<int> s2;
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint(),k=getint();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=getint();
	for(int i=1;i<=n;i++)
	{
		s1.clear();
		for(int j=1;j<=m;j++)
		{
			if(j>k)
				s1.erase(s1.find(a[i][j-k]));
			s1.insert(a[i][j]);
			f[i][j]=*s1.begin();
		}
	}
	for(int j=k;j<=m;j++)
	{
		s1.clear();
		for(int i=1;i<=n;i++)
		{
			if(i>k)
				s1.erase(s1.find(f[i-k][j]));
			s1.insert(f[i][j]);
			g[i][j]=*s1.begin();
		}
	}
	
	for(int i=1;i<=n;i++)
	{
		s2.clear();
		for(int j=1;j<=m;j++)
		{
			if(j>k)
				s2.erase(s2.find(a[i][j-k]));
			s2.insert(a[i][j]);
			f[i][j]=*s2.begin();
		}
	}
	for(int j=k;j<=m;j++)
	{
		s2.clear();
		for(int i=1;i<=n;i++)
		{
			if(i>k)
				s2.erase(s2.find(f[i-k][j]));
			s2.insert(f[i][j]);
			g1[i][j]=*s2.begin();
		}
	}
	
	int ans=1e9;
	for(int i=k;i<=n;i++)
		for(int j=k;j<=m;j++)
			ans=min(ans,g[i][j]-g1[i][j]);
	printf("%d\n",ans);
	return 0;
}
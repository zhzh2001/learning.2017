#include<cstdio>
#include<cctype>
#define mymin(a,b) a<b?a:b
using namespace std;
int a[1005][1005],f[1005][1005],g[1005][1005],g1[1005][1005];
struct node
{
	int x,p;
};
node q[1005];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	int n=getint(),m=getint(),k=getint();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			a[i][j]=getint();
	for(int i=1;i<=n;i++)
	{
		int l=1,r=0;
		for(int j=1;j<=m;j++)
		{
			while(l<=r&&j-q[l].p>=k)
				l++;
			while(l<=r&&q[r].x<=a[i][j])
				r--;
			q[++r].x=a[i][j];q[r].p=j;
			f[i][j]=q[l].x;
		}
	}
	for(int j=k;j<=m;j++)
	{
		int l=1,r=0;
		for(int i=1;i<=n;i++)
		{
			while(l<=r&&i-q[l].p>=k)
				l++;
			while(l<=r&&q[r].x<=f[i][j])
				r--;
			q[++r].x=f[i][j];q[r].p=i;
			g[i][j]=q[l].x;
		}
	}
	
	for(int i=1;i<=n;i++)
	{
		int l=1,r=0;
		for(int j=1;j<=m;j++)
		{
			while(l<=r&&j-q[l].p>=k)
				l++;
			while(l<=r&&q[r].x>=a[i][j])
				r--;
			q[++r].x=a[i][j];q[r].p=j;
			f[i][j]=q[l].x;
		}
	}
	for(int j=k;j<=m;j++)
	{
		int l=1,r=0;
		for(int i=1;i<=n;i++)
		{
			while(l<=r&&i-q[l].p>=k)
				l++;
			while(l<=r&&q[r].x>=f[i][j])
				r--;
			q[++r].x=f[i][j];q[r].p=i;
			g1[i][j]=q[l].x;
		}
	}
	
	int ans=1e9;
	for(int i=k;i<=n;i++)
		for(int j=k;j<=m;j++)
			ans=mymin(ans,g[i][j]-g1[i][j]);
	printf("%d\n",ans);
	return 0;
}
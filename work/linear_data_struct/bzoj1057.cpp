#include<iostream>
#include<algorithm>
#include<cstring>
#include<stack>
using namespace std;
bool mat[2005][2005];
int f[2005][2005][2];
struct node
{
	int x,p;
	node(int x,int p):x(x),p(p){};
};
stack<node> S;
int main()
{
	int n,m;
	cin>>n>>m;
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			cin>>mat[i][j];
			if((i+j)&1)
				mat[i][j]^=1;
			f[i][j][mat[i][j]]=min(f[i-1][j-1][mat[i][j]],min(f[i-1][j][mat[i][j]],f[i][j-1][mat[i][j]]))+1;
			ans=max(ans,f[i][j][mat[i][j]]);
		}
	cout<<ans*ans<<endl;
	/*
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=m;j++)
			cout<<mat[i][j]<<' ';
		cout<<endl;
	}
	*/
	memset(f,0,sizeof(f));
	ans=0;
	for(int now=0;now<2;now++)
		for(int i=1;i<=n;i++)
		{
			while(!S.empty())
				S.pop();
			for(int j=1;j<=m+1;j++)
			{
				if(mat[i][j]==now)
					f[i][j][now]=f[i-1][j][now]+1;
				else
					f[i][j][now]=0;
				int t=0;
				while(!S.empty()&&S.top().x>f[i][j][now])
				{
					t=S.top().p;
					ans=max(ans,(j-t)*S.top().x);
					S.pop();
				}
				if(S.empty()||f[i][j][now]>S.top().x)
					if(t)
						S.push(node(f[i][j][now],t));
					else
						S.push(node(f[i][j][now],j));
			}
		}
	cout<<ans<<endl;
	return 0;
}
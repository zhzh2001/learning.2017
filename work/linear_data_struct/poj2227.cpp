#include<iostream>
#include<queue>
#include<vector>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int a[305][305];
bool vis[305][305];
struct node
{
	int i,j,h;
	node(int i,int j,int h):i(i),j(j),h(h){};
	bool operator>(const node b)const
	{
		return h>b.h;
	}
};
priority_queue<node,vector<node>,greater<node> > q;
int main()
{
	int n,m;
	cin>>m>>n;
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			cin>>a[i][j];
			if(i==1||i==n||j==1||j==m)
			{
				node tmp(i,j,a[i][j]);
				q.push(tmp);
				vis[i][j]=true;
			}
		}
	long long ans=0;
	while(!q.empty())
	{
		node k=q.top();q.pop();
		for(int i=0;i<4;i++)
		{
			int nx=k.i+dx[i],ny=k.j+dy[i];
			if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis[nx][ny])
			{
				if(a[nx][ny]<k.h)
				{
					ans+=k.h-a[nx][ny];
					node tmp(nx,ny,k.h);
					q.push(tmp);
				}
				else
				{
					node tmp(nx,ny,a[nx][ny]);
					q.push(tmp);
				}
				vis[nx][ny]=true;
			}
		}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
stack<int> ns;
stack<char> cs;
int prio[256];
int main()
{
	prio['+']=1;prio['*']=2;
	char c;
	int num=0;
	while((c=getchar())!=EOF)
		if(isdigit(c))
			num=num*10+c-'0';
		else
		{
			if(c!='+'&&c!='*')
				continue;
			ns.push(num);
			num=0;
			while(!cs.empty()&&prio[c]<=prio[cs.top()])
			{
				int a=ns.top();ns.pop();
				int b=ns.top();ns.pop();
				if(cs.top()=='+')
					a=(a+b)%10000;
				else
					a=((long long)a*b)%10000LL;
				ns.push(a);
				cs.pop();
			}
			cs.push(c);
		}
	ns.push(num);
	while(!cs.empty())
	{
		int a=ns.top();ns.pop();
		int b=ns.top();ns.pop();
		if(cs.top()=='+')
			a=(a+b)%10000;
		else
			a=((long long)a*b)%10000LL;
		ns.push(a);
		cs.pop();
	}
	cout<<ns.top()%10000<<endl;
	return 0;
}
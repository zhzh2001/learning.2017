#include<bits/stdc++.h>
using namespace std;
const int p=10007,num[]={154,12,0,789,465,101};
int prio[256];
string l[30];
bool bl[30];
stack<int> ns;
stack<char> cs;
inline int mypow(int a,int b)
{
	int ret=1;
	for(int i=1;i<=b;i++)
		ret=(ret*a)%p;
	return ret;
}
int calc(int a,int li)
{
	while(!ns.empty())
		ns.pop();
	int num=0,left=0;
	for(int i=0;i<l[li].length();i++)
		if(l[li][i]=='a')
			ns.push(a);
		else
			if(isdigit(l[li][i]))
				num=num*10+l[li][i]-'0';
			else
				if(l[li][i]=='(')
				{
					cs.push(l[li][i]);
					left++;
				}
				else
					if(l[li][i]==')')
					{
						if(!left&&cs.empty())
							continue;
						left--;
						if(i&&isdigit(l[li][i-1]))
						{
							ns.push(num);
							num=0;
						}
						while(cs.top()!='(')
						{
							int b=ns.top();ns.pop();
							int a=ns.top();ns.pop();
							switch(cs.top())
							{
								case '+':a=(a+b)%p;break;
								case '-':a=(a-b+p)%p;break;
								case '*':a=a*b%p;break;
								case '^':a=mypow(a,b);break;
							}
							ns.push(a);
							cs.pop();
						}
						cs.pop();
					}
					else
					{
						if(i&&isdigit(l[li][i-1]))
						{
							ns.push(num);
							num=0;
						}
						while(!cs.empty()&&prio[l[li][i]]<=prio[cs.top()])
						{
							int b=ns.top();ns.pop();
							int a=ns.top();ns.pop();
							switch(cs.top())
							{
								case '+':a=(a+b)%p;break;
								case '-':a=(a-b+p)%p;break;
								case '*':a=a*b%p;break;
								case '^':a=mypow(a,b);break;
							}
							ns.push(a);
							cs.pop();
						}
						cs.push(l[li][i]);
					}
	if(ns.empty())
		return 0;
	else
		return ns.top()%p;
}
int main()
{
	prio['(']=0;prio['+']=prio['-']=1;prio['*']=2;prio['^']=3;
	getline(cin,l[0]);
	for(int i=0;i<l[0].length();)
		if(l[0][i]==' ')
			l[0].erase(i,1);
		else
			i++;
	l[0]='('+l[0]+')';
	int n;
	cin>>n;
	getline(cin,l[1]);
	for(int i=1;i<=n;i++)
	{
		getline(cin,l[i]);
		for(int j=0;j<l[i].length();)
			if(l[i][j]==' ')
				l[i].erase(j,1);
			else
				j++;
		l[i]='('+l[i]+')';
	}
	memset(bl,true,sizeof(bl));
	for(int t=0;t<6;t++)
	{
		int a=num[t],ans=calc(a,0);
		for(int i=1;i<=n;i++)
			if(calc(a,i)!=ans)
				bl[i]=false;
	}
	for(int i=1;i<=n;i++)
		if(bl[i])
			putchar(i+'A'-1);
	puts("");
	return 0;
}
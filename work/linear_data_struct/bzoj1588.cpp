#include<iostream>
#include<vector>
#include<algorithm>
#include<cstdlib>
using namespace std;
vector<int> a;
int main()
{
	int n;
	cin>>n;
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		int now;
		if(i>1)
		{
			int p=lower_bound(a.begin(),a.end(),x)-a.begin();
			now=abs(a[p]-x);
			if(p)
				now=min(now,abs(a[p-1]-x));
		}
		else
			now=x;
		ans+=now;
		a.push_back(x);
		for(int j=a.size()-1;j&&a[j-1]>a[j];j--)
			swap(a[j-1],a[j]);
	}
	cout<<ans<<endl;
	return 0;
}
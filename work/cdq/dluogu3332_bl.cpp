#include<bits/stdc++.h>
using namespace std;
const int N=50005;
struct node
{
	int opt,l,r,k,id;
	bool operator<(const node& rhs)const
	{
		if(k==rhs.k)
			return id<rhs.id;
		return k>rhs.k;
	}
}a[N],b[N];
int rb[N];
bool vis[N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	int bn=0;
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d%d",&a[i].opt,&a[i].l,&a[i].r,&a[i].k);
		a[i].id=i;
		if(a[i].opt==1)
			b[++bn]=a[i];
	}
	sort(b+1,b+bn+1);
	for(int i=1;i<=bn;i++)
		rb[b[i].id]=i;
	for(int i=1;i<=m;i++)
		if(a[i].opt==1)
			vis[rb[i]]=true;
		else
			for(unsigned j=1,cnt=0;j<=bn;j++)
				if(vis[j])
				{
					int now=min(a[i].r,b[j].r)-max(a[i].l,b[j].l)+1;
					if(now>0&&(cnt+=now)>=a[i].k)
					{
						printf("%d\n",b[j].k);
						break;
					}
				}
	return 0;
}
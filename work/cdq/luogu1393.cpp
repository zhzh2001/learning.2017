#include<bits/stdc++.h>
using namespace std;
const int N=40005;
int n,val[N],ans[N];
struct node
{
	int val,id,t;
}a[N],b[N];
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
void solve(int l,int r)
{
	if(l==r)
		return;
	int mid=(l+r)/2,i1=l,i2=mid+1;
	for(int i=l;i<=r;i++)
		if(a[i].t<=mid)
			b[i1++]=a[i];
		else
			b[i2++]=a[i];
	copy(b+l,b+r+1,a+l);
	int j=l;
	for(int i=mid+1;i<=r;i++)
	{
		for(;j<=mid&&a[j].id<a[i].id;j++)
			T.modify(a[j].val,1);
		ans[a[i].t]+=j-l-T.query(a[i].val);
	}
	for(int i=l;i<j;i++)
		T.modify(a[i].val,-1);
	j=mid;
	for(int i=r;i>mid;i--)
	{
		for(;j>=l&&a[j].id>a[i].id;j--)
			T.modify(a[j].val,1);
		ans[a[i].t]+=T.query(a[i].val-1);
	}
	for(int i=j+1;i<=mid;i++)
		T.modify(a[i].val,-1);
	solve(l,mid);
	solve(mid+1,r);
}
int main()
{
	int m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].val;
		a[i].id=i;
		val[i]=a[i].val;
	}
	sort(val+1,val+n+1);
	int now=n;
	for(int i=1;i<=m;i++)
	{
		int x;
		cin>>x;
		a[x].t=now--;
	}
	for(int i=1;i<=n;i++)
	{
		if(!a[i].t)
			a[i].t=now--;
		a[i].val=lower_bound(val+1,val+n+1,a[i].val)-val;
	}
	solve(1,n);
	for(int i=1;i<=n;i++)
		ans[i]+=ans[i-1];
	for(int i=n;i>=n-m;i--)
		cout<<ans[i]<<' ';
	cout<<endl;
	return 0;
}
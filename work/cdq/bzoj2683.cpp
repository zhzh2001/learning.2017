#include<bits/stdc++.h>
using namespace std;
const int M=200005;
int n,cc,x[2*M],y[2*M],ans[M];
struct opr
{
	int opt,x,y,val;
	bool operator<(const opr& rhs)const
	{
		if(x==rhs.x&&y==rhs.y)
			return opt<rhs.opt;
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}a[4*M],tmp[4*M];
struct BIT
{
	int tree[2*M];
	void modify(int x,int val)
	{
		for(;x<=cc;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
void solve(int l,int r)
{
	if(l==r)
		return;
	int mid=(l+r)/2;
	solve(l,mid);
	solve(mid+1,r);
	int tmpn=0;
	for(int i=l;i<=r;i++)
		if((i<=mid)^(a[i].opt==2))
			tmp[++tmpn]=a[i];
	sort(tmp+1,tmp+tmpn+1);
	for(int i=1;i<=tmpn;i++)
		if(tmp[i].opt==1)
			T.modify(tmp[i].y,tmp[i].val);
		else
			if(tmp[i].val>0)
				ans[tmp[i].val]+=T.query(tmp[i].y);
			else
				ans[-tmp[i].val]-=T.query(tmp[i].y);
	for(int i=1;i<=tmpn;i++)
		if(tmp[i].opt==1)
			T.modify(tmp[i].y,-tmp[i].val);
}
int main()
{
	scanf("%d",&n);
	int m=0,q=0,opt;
	while(scanf("%d",&opt)==1&&opt!=3)
		if(opt==1)
		{
			m++;
			scanf("%d%d%d",&a[m].x,&a[m].y,&a[m].val);
			a[m].opt=opt;
			x[++cc]=a[m].x;y[cc]=a[m].y;
		}
		else
		{
			q++;
			int x1,y1,x2,y2;
			scanf("%d%d%d%d",&x1,&y1,&x2,&y2);
			x1--;y1--;
			x[++cc]=x1;y[cc]=y1;
			x[++cc]=x2;y[cc]=y2;
			a[++m].opt=opt;a[m].x=x2;a[m].y=y2;a[m].val=q;
			a[++m].opt=opt;a[m].x=x2;a[m].y=y1;a[m].val=-q;
			a[++m].opt=opt;a[m].x=x1;a[m].y=y2;a[m].val=-q;
			a[++m].opt=opt;a[m].x=x1;a[m].y=y1;a[m].val=q;
		}
	sort(x+1,x+cc+1);
	sort(y+1,y+cc+1);
	for(int i=1;i<=m;i++)
	{
		a[i].x=lower_bound(x+1,x+cc+1,a[i].x)-x;
		a[i].y=lower_bound(y+1,y+cc+1,a[i].y)-y;
	}
	solve(1,m);
	for(int i=1;i<=q;i++)
		printf("%d\n",ans[i]);
	return 0;
}
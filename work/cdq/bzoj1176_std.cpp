#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<set>
#include<ctime>
#include<vector>
#include<queue>
#include<algorithm>
#include<map>
#include<cmath>
#define inf 1000000000
#define pa pair<int,int>
#define ll long long 
using namespace std;
int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int s,w,m;
int ans[10005];
int t[2000005];
struct que{
	int x,y,val,pos,id,opt;
}q[200005],tmp[200005];
bool operator<(que a,que b)
{
	if(a.x==b.x&&a.y==b.y)return a.opt<b.opt;
	if(a.x==b.x)return a.y<b.y;
	return a.x<b.x;
}
void addquery()
{
	int x1=read(),y1=read(),x2=read(),y2=read();
	int pos=++ans[0];
	q[++m].pos=pos;q[m].id=m;q[m].x=x1-1;q[m].y=y1-1;q[m].val=1;q[m].opt=1;
	q[++m].pos=pos;q[m].id=m;q[m].x=x2;q[m].y=y2;q[m].val=1;q[m].opt=1;
	q[++m].pos=pos;q[m].id=m;q[m].x=x1-1;q[m].y=y2;q[m].val=-1;q[m].opt=1;
	q[++m].pos=pos;q[m].id=m;q[m].x=x2;q[m].y=y1-1;q[m].val=-1;q[m].opt=1;
}
void add(int x,int val)
{
	for(int i=x;i<=w;i+=i&-i)t[i]+=val;
}
int query(int x)
{
	int tmp=0;
	for(int i=x;i;i-=i&-i)tmp+=t[i];
	return tmp;
}
void cdq(int l,int r)
{
	if(l==r)return;
	int mid=(l+r)>>1,l1=l,l2=mid+1;
	for(int i=l;i<=r;i++)
	{
		if(q[i].id<=mid&&!q[i].opt)add(q[i].y,q[i].val);
		if(q[i].id>mid&&q[i].opt)ans[q[i].pos]+=q[i].val*query(q[i].y);
	}
	for(int i=l;i<=r;i++)
		if(q[i].id<=mid&&!q[i].opt)add(q[i].y,-q[i].val);
	for(int i=l;i<=r;i++)
		if(q[i].id<=mid)tmp[l1++]=q[i];
		else tmp[l2++]=q[i];
	for(int i=l;i<=r;i++)
		q[i]=tmp[i];
	cdq(l,mid);cdq(mid+1,r);
}
int main()
{
	s=read();w=read();
	while(1)
	{
	    int opt=read();
		if(opt==1)
		{
			q[++m].x=read(),q[m].y=read(),q[m].val=read();
			q[m].id=m;
		}
		else if(opt==2)
			addquery();
		else break;
	}
	sort(q+1,q+m+1);
	cdq(1,m);
	for(int i=1;i<=ans[0];i++)
		printf("%d\n",ans[i]);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int W=2e6,M=160000,Q=10000,V=100;
struct node
{
	int opt,x,y,x1,y1;
}a[M+Q+5];
int main()
{
	default_random_engine gen(time(NULL));
	cout<<0<<' '<<W<<endl;
	uniform_int_distribution<> dw(1,W);
	uniform_int_distribution<> dv(1,V);
	for(int i=1;i<=M;i++)
	{
		a[i].opt=1;
		a[i].x=dw(gen);
		a[i].y=dw(gen);
		a[i].x1=dv(gen);
	}
	for(int i=1;i<=Q;i++)
	{
		a[M+i].opt=2;
		a[M+i].x=dw(gen);
		a[M+i].y=dw(gen);
		a[M+i].x1=dw(gen);
		a[M+i].y1=dw(gen);
	}
	random_shuffle(a+1,a+M+Q+1);
	for(int i=1;i<=M+Q;i++)
		if(a[i].opt==1)
			cout<<a[i].opt<<' '<<a[i].x<<' '<<a[i].y<<' '<<a[i].x1<<endl;
		else
			cout<<a[i].opt<<' '<<a[i].x<<' '<<a[i].y<<' '<<a[i].x1<<' '<<a[i].y1<<endl;
	cout<<3<<endl;
	return 0;
}
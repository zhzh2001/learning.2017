#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],Min[N],Max[N],f[N];
struct node
{
	int x,y,id;
	bool operator<(const node& rhs)const
	{
		if(x==rhs.x&&y==rhs.y)
			return id<rhs.id;
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}b[N];
struct BIT
{
	int tree[N],maxv;
	void modify(int x,int val)
	{
		for(;x<=maxv;x+=x&-x)
			tree[x]=max(tree[x],val);
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans=max(ans,tree[x]);
		return ans;
	}
}T;
void solve(int l,int r)
{
	if(l==r)
		return;
	int mid=(l+r)/2;
	solve(l,mid);
	for(int i=l;i<=r;i++)
	{
		b[i].id=i;
		if(i<=mid)
		{
			b[i].x=a[i];
			b[i].y=Max[i];
		}
		else
		{
			b[i].x=Min[i];
			b[i].y=a[i];
		}
	}
	sort(b+l,b+r+1);
	for(int i=l;i<=r;i++)
		if(b[i].id<=mid)
			T.modify(b[i].y,f[b[i].id]);
		else
			f[b[i].id]=max(f[b[i].id],T.query(b[i].y)+1);
	for(int i=l;i<=r;i++)
		if(b[i].id<=mid)
			for(int j=b[i].y;j<=T.maxv;j+=j&-j)
				T.tree[j]=0;
	solve(mid+1,r);
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",a+i);
		Min[i]=Max[i]=a[i];
		f[i]=1;
	}
	while(m--)
	{
		int i,x;
		scanf("%d%d",&i,&x);
		Min[i]=min(Min[i],x);
		Max[i]=max(Max[i],x);
	}
	for(int i=1;i<=n;i++)
		T.maxv=max(T.maxv,Max[i]);
	solve(1,n);
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,f[i]);
	printf("%d\n",ans);
	return 0;
}
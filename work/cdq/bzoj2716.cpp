#include<bits/stdc++.h>
using namespace std;
const int M=500005,W=1000005;
int q,ans[M],maxx,maxy;
struct opr
{
	int opt,x,y,id;
	bool operator<(const opr& rhs)const
	{
		if(x==rhs.x&&y==rhs.y)
			return opt<rhs.opt;
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}a[2*M],tmp[2*M];
struct BIT
{
	int tree[W];
	void modify(int x,int val)
	{
		for(;x<=maxy;x+=x&-x)
			tree[x]=max(tree[x],val);
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans=max(ans,tree[x]);
		if(!ans)
			return -1e9;
		return ans;
	}
}T;
void solve(int l,int r)
{
	if(l==r)
		return;
	int mid=(l+r)/2;
	solve(l,mid);
	solve(mid+1,r);
	int tmpn=0;
	for(int i=l;i<=r;i++)
		if((i<=mid)^(a[i].opt==2))
			tmp[++tmpn]=a[i];
	sort(tmp+1,tmp+tmpn+1);
	for(int i=1;i<=tmpn;i++)
		if(tmp[i].opt==1)
			T.modify(tmp[i].y,tmp[i].x+tmp[i].y);
		else
			ans[tmp[i].id]=min(ans[tmp[i].id],tmp[i].x+tmp[i].y-T.query(tmp[i].y));
	for(int i=1;i<=tmpn;i++)
		if(tmp[i].opt==1)
			for(int j=tmp[i].y;j<=maxy;j+=j&-j)
				T.tree[j]=0;
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	{
		a[i].opt=1;
		scanf("%d%d",&a[i].x,&a[i].y);
		maxx=max(maxx,a[i].x);
		maxy=max(maxy,a[i].y);
	}
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d%d",&a[n+i].opt,&a[n+i].x,&a[n+i].y);
		maxx=max(maxx,a[n+i].x);
		maxy=max(maxy,a[n+i].y);
		if(a[n+i].opt==2)
			a[n+i].id=++q;
	}
	memset(ans,0x3f,sizeof(ans));
	maxx++;maxy++;
	solve(1,n+m);
	for(int i=1;i<=n+m;i++)
		a[i].x=maxx-a[i].x;
	solve(1,n+m);
	for(int i=1;i<=n+m;i++)
		a[i].y=maxy-a[i].y;
	solve(1,n+m);
	for(int i=1;i<=n+m;i++)
		a[i].x=maxx-a[i].x;
	solve(1,n+m);
	for(int i=1;i<=q;i++)
		printf("%d\n",ans[i]);
	return 0;
}
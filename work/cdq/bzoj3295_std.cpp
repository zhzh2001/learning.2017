#include<iostream>  
#include<cstdio>  
#include<cstring>  
#include<cstdlib>  
#include<cstring>  
#include<algorithm>  
#define F(i,j,n) for(int i=j;i<=n;i++)  
#define D(i,j,n) for(int i=j;i>=n;i--)  
#define ll long long  
#define maxn 100005  
using namespace std;  
int n,m,pos[maxn],c[maxn],sl[maxn],sr[maxn];  
ll ans[maxn];  
struct data{int t,p,v;}a[maxn],b[maxn];  
inline int read()  
{  
    int x=0,f=1;char ch=getchar();  
    while (ch<'0'||ch>'9'){if (ch=='-') f=-1;ch=getchar();}  
    while (ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  
    return x*f;  
}  
inline void add(int x,int y)  
{  
    for(;x<=n;x+=(x&(-x))) c[x]+=y;  
}  
inline int query(int x)  
{  
    int ret=0;  
    for(;x;x-=(x&(-x))) ret+=c[x];  
    return ret;  
}  
inline void cdq(int l,int r)  
{  
    if (l>=r) return;  
    int mid=(l+r)>>1,l1=l,l2=mid+1,tmp;  
    F(i,l,r)  
    {  
        if (a[i].t<=mid) b[l1++]=a[i];  
        else b[l2++]=a[i];  
    }  
    F(i,l,r) a[i]=b[i];  
    tmp=l;  
    F(i,mid+1,r)  
    {  
        for(;tmp<=mid&&a[tmp].p<a[i].p;tmp++) add(a[tmp].v,1);  
        sl[a[i].t]+=tmp-l-query(a[i].v);  
    }  
    F(i,l,tmp-1) add(a[i].v,-1);  
    tmp=mid;  
    D(i,r,mid+1)  
    {  
        for(;tmp>=l&&a[tmp].p>a[i].p;tmp--) add(a[tmp].v,1);  
        sr[a[i].t]+=query(a[i].v-1);  
    }  
    F(i,tmp+1,mid) add(a[i].v,-1);  
    cdq(l,mid);cdq(mid+1,r);  
}  
int main()  
{  
    n=read();m=read();  
    F(i,1,n) a[i].v=read(),a[i].p=i,pos[a[i].v]=i;  
    int x,time=n;  
    F(i,1,m) x=read(),a[pos[x]].t=time--;  
    F(i,1,n) if (!a[i].t) a[i].t=time--;  
    cdq(1,n);  
    F(i,1,n) ans[i]=ans[i-1]+sr[i]+sl[i];  
    D(i,n,n-m+1) printf("%lld\n",ans[i]);  
    return 0;  
}  



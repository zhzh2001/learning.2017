#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
const int N = 100005;
int a[N];
struct node
{
	long long sum, min, max, lazy;
} tree[N * 4];
inline void pullup(int id)
{
	tree[id].sum = tree[id * 2].sum + tree[id * 2 + 1].sum;
	tree[id].min = min(tree[id * 2].min, tree[id * 2 + 1].min);
	tree[id].max = max(tree[id * 2].max, tree[id * 2 + 1].max);
}
void build(int id, int l, int r)
{
	if (l == r)
	{
		tree[id].sum = tree[id].min = tree[id].max = a[l];
		tree[id].lazy = 0;
	}
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		pullup(id);
	}
}
inline void pushdown(int id, int l, int r)
{
	if (tree[id].lazy && l < r)
	{
		int mid = (l + r) / 2;
		tree[id * 2].sum += (long long)(mid - l + 1) * tree[id].lazy;
		tree[id * 2].min += tree[id].lazy;
		tree[id * 2].max += tree[id].lazy;
		tree[id * 2].lazy += tree[id].lazy;
		tree[id * 2 + 1].sum += (long long)(r - mid) * tree[id].lazy;
		tree[id * 2 + 1].min += tree[id].lazy;
		tree[id * 2 + 1].max += tree[id].lazy;
		tree[id * 2 + 1].lazy += tree[id].lazy;
		tree[id].lazy = 0;
	}
}
void add(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
	{
		tree[id].sum += (long long)(r - l + 1) * val;
		tree[id].lazy += val;
		tree[id].min += val;
		tree[id].max += val;
	}
	else
	{
		pushdown(id, l, r);
		int mid = (l + r) / 2;
		if (L <= mid)
			add(id * 2, l, mid, L, R, val);
		if (R > mid)
			add(id * 2 + 1, mid + 1, r, L, R, val);
		pullup(id);
	}
}
void dosqrt(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r && (tree[id].min == tree[id].max || (tree[id].max - tree[id].min == 1 && (int)sqrt(tree[id].max) != (int)sqrt(tree[id].min))))
	{
		long long delta = (long long)sqrt(tree[id].max) - tree[id].max;
		tree[id].sum += (r - l + 1) * delta;
		tree[id].lazy += delta;
		tree[id].min += delta;
		tree[id].max += delta;
	}
	else
	{
		pushdown(id, l, r);
		int mid = (l + r) / 2;
		if (L <= mid)
			dosqrt(id * 2, l, mid, L, R);
		if (R > mid)
			dosqrt(id * 2 + 1, mid + 1, r, L, R);
		pullup(id);
	}
}
long long query(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[id].sum;
	pushdown(id, l, r);
	int mid = (l + r) / 2;
	long long ans = 0;
	if (L <= mid)
		ans += query(id * 2, l, mid, L, R);
	if (R > mid)
		ans += query(id * 2 + 1, mid + 1, r, L, R);
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	build(1, 1, n);
	while (m--)
	{
		int opt, l, r, val;
		cin >> opt;
		switch (opt)
		{
		case 1:
			cin >> l >> r >> val;
			add(1, 1, n, l, r, val);
			break;
		case 2:
			cin >> l >> r;
			dosqrt(1, 1, n, l, r);
			break;
		case 3:
			cin >> l >> r;
			cout << query(1, 1, n, l, r) << endl;
			break;
		}
	}
	return 0;
}
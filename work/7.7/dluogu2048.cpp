#include <iostream>
#include <queue>
#include <cmath>
using namespace std;
const int N = 500005, LOGN = 20;
int a[N], st[N][LOGN];
struct node
{
	int l, r1, r2, r;
	node(int l, int r1, int r2, int r) : l(l), r1(r1), r2(r2), r(r) {}
	bool operator<(const node &rhs) const
	{
		return a[r] - a[l - 1] < a[rhs.r] - a[rhs.l - 1];
	}
};
int rmq(int l, int r)
{
	int t = log2(r - l + 1);
	return a[st[l][t]] > a[st[r - (1 << t) + 1][t]] ? st[l][t] : st[r - (1 << t) + 1][t];
}
int main()
{
	ios::sync_with_stdio(false);
	int n, k, l, r;
	cin >> n >> k >> l >> r;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i];
		a[i] += a[i - 1];
		st[i][0] = i;
	}
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i + (1 << j) - 1 <= n; i++)
			st[i][j] = a[st[i][j - 1]] > a[st[i + (1 << j - 1)][j - 1]] ? st[i][j - 1] : st[i + (1 << j - 1)][j - 1];
	priority_queue<node> Q;
	for (int i = 1; i + l - 1 <= n; i++)
	{
		int r2 = min(i + r - 1, n);
		Q.push(node(i, i + l - 1, r2, rmq(i + l - 1, r2)));
	}
	long long ans = 0;
	for (int i = 1; i <= k; i++)
	{
		node t = Q.top();
		Q.pop();
		ans += a[t.r] - a[t.l - 1];
		if (t.r > t.r1)
			Q.push(node(t.l, t.r1, t.r - 1, rmq(t.r1, t.r - 1)));
		if (t.r < t.r2)
			Q.push(node(t.l, t.r + 1, t.r2, rmq(t.r + 1, t.r2)));
	}
	cout << ans << endl;
	return 0;
}
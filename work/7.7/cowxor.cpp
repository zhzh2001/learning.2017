/*
ID: zhangzh62
PROG: cowxor
LANG: C++
*/
#include <fstream>
using namespace std;
ifstream fin("cowxor.in");
ofstream fout("cowxor.out");
const int N = 100005;
int a[N], tree[N * 10][2], id[N * 10];
int main()
{
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i];
	int sum = 0, ans = -1, l, r, cc = 0, k = 0;
	for (int j = 20, k = 0; j >= 0; j--)
		k = tree[k][0] = ++cc;
	id[k] = 0;
	for (int i = 1; i <= n; i++)
	{
		sum ^= a[i];
		int now = 0;
		k = 0;
		for (int j = 20; j >= 0; j--)
			if (tree[k][!((sum >> j) & 1)])
			{
				now |= 1 << j;
				k = tree[k][!((sum >> j) & 1)];
			}
			else
				k = tree[k][(sum >> j) & 1];
		if (now > ans)
		{
			ans = now;
			l = id[k] + 1;
			r = i;
		}
		k = 0;
		for (int j = 20; j >= 0; j--)
		{
			if (!tree[k][(sum >> j) & 1])
				tree[k][(sum >> j) & 1] = ++cc;
			k = tree[k][(sum >> j) & 1];
		}
		id[k] = i;
	}
	fout << ans << ' ' << l << ' ' << r << endl;
	return 0;
}
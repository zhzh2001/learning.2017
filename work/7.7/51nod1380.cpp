#include <iostream>
#include <queue>
#include <set>
using namespace std;
const int N = 100000;
int l[N], r[N];
long long a[N];
typedef pair<long long, int> pii;
set<pii> S;
inline void erase(int x)
{
	S.erase(make_pair(a[x], x));
	l[r[x]] = l[x];
	r[l[x]] = r[x];
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
	{
		cin >> a[i];
		l[i] = (i + n - 1) % n;
		r[i] = (i + 1) % n;
		S.insert(make_pair(a[i], i));
	}
	long long ans = 0;
	for (int i = 1; i <= n / 3; i++)
	{
		int id = S.rbegin()->second;
		S.erase(*S.rbegin());
		ans += a[id];
		a[id] = a[l[id]] + a[r[id]] - a[id];
		erase(l[id]);
		erase(r[id]);
		S.insert(make_pair(a[id], id));
	}
	cout << ans << endl;
	return 0;
}
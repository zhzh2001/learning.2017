#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N];
struct node
{
	long long sum;
	int max;
} tree[N * 4];
inline void pullup(int id)
{
	tree[id].sum = tree[id * 2].sum + tree[id * 2 + 1].sum;
	tree[id].max = max(tree[id * 2].max, tree[id * 2 + 1].max);
}
void build(int id, int l, int r)
{
	if (l == r)
		tree[id].sum = tree[id].max = a[l];
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		pullup(id);
	}
}
long long query(int id, int l, int r, int L, int R)
{
	if (L <= l && R >= r)
		return tree[id].sum;
	int mid = (l + r) / 2;
	long long ans = 0;
	if (L <= mid)
		ans += query(id * 2, l, mid, L, R);
	if (R > mid)
		ans += query(id * 2 + 1, mid + 1, r, L, R);
	return ans;
}
void modulo(int id, int l, int r, int L, int R, int mod)
{
	if (tree[id].max < mod)
		return;
	if (l == r)
	{
		tree[id].sum %= mod;
		tree[id].max %= mod;
	}
	else
	{
		int mid = (l + r) / 2;
		if (L <= mid)
			modulo(id * 2, l, mid, L, R, mod);
		if (R > mid)
			modulo(id * 2 + 1, mid + 1, r, L, R, mod);
		pullup(id);
	}
}
void modify(int id, int l, int r, int k, int val)
{
	if (l == r)
		tree[id].sum = tree[id].max = val;
	else
	{
		int mid = (l + r) / 2;
		if (k <= mid)
			modify(id * 2, l, mid, k, val);
		else
			modify(id * 2 + 1, mid + 1, r, k, val);
		pullup(id);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	build(1, 1, n);
	while (m--)
	{
		int opt, x, y, z;
		cin >> opt;
		switch (opt)
		{
		case 1:
			cin >> x >> y;
			cout << query(1, 1, n, x, y) << endl;
			break;
		case 2:
			cin >> x >> y >> z;
			modulo(1, 1, n, x, y, z);
			break;
		case 3:
			cin >> x >> y;
			modify(1, 1, n, x, y);
			break;
		}
	}
	return 0;
}
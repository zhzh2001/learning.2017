#include <iostream>
#include <queue>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	cin >> n >> k;
	typedef pair<long long, int> pii;
	priority_queue<pii, vector<pii>, greater<pii>> Q;
	for (int i = 1; i <= n; i++)
	{
		long long x;
		cin >> x;
		Q.push(make_pair(x, 0));
	}
	for (; (n - 1) % (k - 1); n++)
		Q.push(make_pair(0ll, 0));
	long long ans = 0;
	while (Q.size() > 1)
	{
		long long now = 0;
		int dep = 0;
		for (int i = 1; i <= k; i++)
		{
			pii t = Q.top();
			Q.pop();
			now += t.first;
			dep = max(dep, t.second);
		}
		Q.push(make_pair(now, dep + 1));
		ans += now;
	}
	cout << ans << endl
		 << Q.top().second << endl;
	return 0;
}
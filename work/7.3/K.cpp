#include <bits/stdc++.h>
using namespace std;
const int N = 55;
int f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	while (m--)
	{
		int u, v;
		scanf("%d%d", &u, &v);
		f[getf(u)] = getf(v);
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
		ans += getf(i) == i;
	printf("%lld\n", 1ll << (n - ans));
	return 0;
}
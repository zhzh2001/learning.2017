#include <cstdio>
#include <algorithm>
using namespace std;
const int N = 505, M = 10005;
int f[M][N], rf[M][N], now[N];
int getf(int f[], int x)
{
	return f[x] == x ? x : f[x] = getf(f, f[x]);
}
pair<int, int> e[M];
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++)
		scanf("%d%d", &e[i].first, &e[i].second);

	for (int i = 1; i <= n; i++)
		f[0][i] = rf[m + 1][i] = i;
	for (int i = 1; i <= m; i++)
	{
		copy(f[i - 1] + 1, f[i - 1] + n + 1, f[i] + 1);
		f[i][getf(f[i], e[i].first)] = getf(f[i], e[i].second);
	}
	for (int i = m; i; i--)
	{
		copy(rf[i + 1] + 1, rf[i + 1] + n + 1, rf[i] + 1);
		rf[i][getf(rf[i], e[i].first)] = getf(rf[i], e[i].second);
	}

	int k;
	scanf("%d", &k);
	while (k--)
	{
		int l, r;
		scanf("%d%d", &l, &r);
		copy(f[l - 1] + 1, f[l - 1] + n + 1, now + 1);
		for (int i = 1; i <= n; i++)
			now[getf(now, i)] = getf(now, rf[r + 1][i]);
		int ans = 0;
		for (int i = 1; i <= n; i++)
			ans += getf(now, i) == i;
		printf("%d\n", ans);
	}
	return 0;
}
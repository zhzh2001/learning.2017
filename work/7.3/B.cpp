#include <cstdio>
#include <algorithm>
using namespace std;
const int N = 100005;
struct segment
{
	int l, r;
	segment(int l = 0, int r = 0) : l(l), r(r) {}
	bool operator<(const segment &rhs) const
	{
		return r < rhs.r;
	}
} a[N];
int main()
{
	int n, m;
	while (scanf("%d%d", &n, &m) == 2)
	{
		for (int i = 1; i <= n; i++)
			scanf("%d%d", &a[i].l, &a[i].r);
		sort(a + 1, a + n + 1);
		while (m--)
		{
			int l, r;
			scanf("%d%d", &l, &r);
			int now = l, ans = 0;
			for (int i = upper_bound(a + 1, a + n + 1, segment(0, l)) - a; i <= n && a[i].r <= r; i++)
				if (a[i].l >= now)
				{
					ans++;
					now = a[i].r;
				}
			printf("%d\n", ans);
		}
	}
	return 0;
}
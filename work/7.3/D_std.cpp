#pragma comment(linker, "/STACK:1024000000,1024000000")
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cstring>
using namespace std;
const long long inf = 0x3f3f3f3f3f3f3f3f;
const int N = 100005;
vector<int> G[N];
struct node
{
	long long max, lazy;
} tree[N * 4];
long long val[N];
int L[N], R[N], cost[N];
int idx;
void dfs(int u, int f, long long vv)
{
	L[u] = ++idx;
	for (unsigned int i = 0; i < G[u].size(); i++)
	{
		int v = G[u][i];
		if (v == f)
			continue;
		dfs(v, u, cost[v] + vv);
	}
	R[u] = idx;
	val[L[u]] = vv;
}
void pushup(int node)
{
	tree[node] = max(tree[node * 2].max, tree[node * 2 + 1].max);
}
void pushdown(int node)
{
	if (tree[node].lazy)
	{
		tree[node * 2].lazy += tree[node].lazy;
		tree[node * 2 + 1].lazy += tree[node].lazy;
		tree[node * 2].max += tree[node].max;
		tree[node * 2 + 1].max += tree[node].max;
		tree[node].lazy = 0;
	}
}
void buildtree(int le, int ri, int node)
{
	if (le == ri)
	{
		tree[node].max = val[le];
		return;
	}
	int t = (le + ri) / 2;
	buildtree(le, t, node * 2);
	buildtree(t + 1, ri, node * 2 + 1);
	pushup(node);
}
void update(int l, int r, int x, int le, int ri, int node)
{
	if (l <= le && ri <= r)
	{
		tree[node].lazy += x;
		tree[node].max += x;
		return;
	}
	pushdown(node);
	int t = (le + ri) / 2;
	if (l <= t)
		update(l, r, x, le, t, node * 2);
	if (r > t)
		update(l, r, x, t + 1, ri, node * 2 + 1);
	pushup(node);
}
long long query(int l, int r, int le, int ri, int node)
{
	if (l <= le && ri <= r)
		return tree[node].max;
	pushdown(node);
	long long ans = -inf;
	int t = (le + ri) / 2;
	if (l <= t)
		ans = max(ans, query(l, r, le, t, node * 2));
	if (r > t)
		ans = max(ans, query(l, r, t + 1, ri, node * 2 + 1));
	return ans;
}
int main()
{
	int T, cas = 1, n, m, a, b, c;
	scanf("%d", &T);
	while (T--)
	{
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++)
			G[i].clear();
		for (int i = 1; i < n; i++)
		{
			scanf("%d%d", &a, &b);
			a++;
			b++;
			G[a].push_back(b);
			G[b].push_back(a);
		}
		for (int i = 1; i <= n; i++)
			scanf("%d", cost + i);
		idx = 0;
		dfs(1, 0, cost[1]);
		buildtree(1, n, 1);
		printf("Case #%d:\n", cas++);
		while (m--)
		{
			scanf("%d", &a);
			if (a == 0)
			{
				scanf("%d%d", &b, &c);
				b++;
				update(L[b], R[b], c - cost[b], 1, n, 1);
				cost[b] = c;
			}
			else
			{
				scanf("%d", &b);
				b++;
				long long answe = query(L[b], R[b], 1, n, 1);
				printf("%lld\n", answe);
			}
		}
	}
	return 0;
}
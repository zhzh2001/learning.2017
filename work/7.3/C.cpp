#include <cstdio>
#include <algorithm>
using namespace std;
const int N = 100005, LOGN = 18;
int head[N], v[N * 2], nxt[N * 2], e, dep[N], f[N][LOGN], A[N], B[N], a[N], b[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
bool vis[N], mark[N];
void dfs(int k)
{
	vis[k] = true;
	for (int i = head[k]; i; i = nxt[i])
		if (!vis[v[i]])
		{
			dep[v[i]] = dep[k] + 1;
			f[v[i]][0] = k;
			dfs(v[i]);
		}
}
void work(int src[], int sz, int dest[], int k)
{
	for (int i = 1; i <= sz; i++)
	{
		if (dep[src[i]] < k)
		{
			dest[i] = -1;
			continue;
		}
		dest[i] = src[i];
		int delta = dep[src[i]] - k;
		for (int j = 0; delta; j++, delta /= 2)
			if (delta & 1)
				dest[i] = f[dest[i]][j];
	}
}
int main()
{
	int n, m;
	while (scanf("%d%d", &n, &m) == 2)
	{
		fill(head + 1, head + n + 1, 0);
		e = 0;
		for (int i = 1; i < n; i++)
		{
			int u, v;
			scanf("%d%d", &u, &v);
			add_edge(u, v);
			add_edge(v, u);
		}
		fill(vis + 1, vis + n + 1, false);
		dep[1] = 1;
		dfs(1);
		for (int j = 1; j < LOGN; j++)
			for (int i = 1; i <= n; i++)
				f[i][j] = f[f[i][j - 1]][j - 1];
		while (m--)
		{
			int acnt;
			scanf("%d", &acnt);
			int r = 0;
			for (int i = 1; i <= acnt; i++)
			{
				scanf("%d", A + i);
				r = max(r, dep[A[i]]);
			}
			int bcnt;
			scanf("%d", &bcnt);
			for (int i = 1; i <= bcnt; i++)
			{
				scanf("%d", B + i);
				r = max(r, dep[B[i]]);
			}
			int l = 1, ans = 1;
			while (l <= r)
			{
				int mid = (l + r) / 2;
				work(A, acnt, a, mid);
				work(B, bcnt, b, mid);
				for (int i = 1; i <= acnt; i++)
					if (~a[i])
						mark[a[i]] = true;
				bool flag = false;
				for (int i = 1; i <= bcnt; i++)
					if (~b[i] && mark[b[i]])
					{
						flag = true;
						break;
					}
				for (int i = 1; i <= acnt; i++)
					if (~a[i])
						mark[a[i]] = false;
				if (flag)
				{
					l = mid + 1;
					ans = mid;
				}
				else
					r = mid - 1;
			}
			printf("%d\n", ans);
		}
	}
	return 0;
}
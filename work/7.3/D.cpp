#pragma comment(linker, "/STACK:1024000000,1024000000")
#include <cstdio>
#include <algorithm>
using namespace std;
const int N = 100005;
const long long INF = 1e18;
int head[N], v[N * 2], nxt[N * 2], e, w[N], into[N], outo[N], now;
long long dist[N], a[N];
bool vis[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
void dfs(int k)
{
	vis[k] = true;
	into[k] = ++now;
	for (int i = head[k]; i; i = nxt[i])
		if (!vis[v[i]])
		{
			dist[v[i]] = dist[k] + w[v[i]];
			dfs(v[i]);
		}
	outo[k] = now;
}
struct node
{
	long long max, lazy;
} tree[N * 4];
void build(int id, int l, int r)
{
	tree[id].lazy = 0;
	if (l == r)
		tree[id].max = a[l];
	else
	{
		int mid = (l + r) / 2;
		build(id * 2, l, mid);
		build(id * 2 + 1, mid + 1, r);
		tree[id].max = max(tree[id * 2].max, tree[id * 2 + 1].max);
	}
}
inline void pushdown(int id, int l, int r)
{
	if (tree[id].lazy && l < r)
	{
		tree[id * 2].max += tree[id].lazy;
		tree[id * 2].lazy += tree[id].lazy;
		tree[id * 2 + 1].max += tree[id].lazy;
		tree[id * 2 + 1].lazy += tree[id].lazy;
		tree[id].lazy = 0;
	}
}
int L, R;
long long val;
void modify(int id, int l, int r)
{
	if (L <= l && R >= r)
	{
		tree[id].max += val;
		tree[id].lazy += val;
	}
	else
	{
		pushdown(id, l, r);
		int mid = (l + r) / 2;
		if (L <= mid)
			modify(id * 2, l, mid);
		if (R > mid)
			modify(id * 2 + 1, mid + 1, r);
		tree[id].max = max(tree[id * 2].max, tree[id * 2 + 1].max);
	}
}
void query(int id, int l, int r)
{
	if (L <= l && R >= r)
		val = max(val, tree[id].max);
	else
	{
		pushdown(id, l, r);
		int mid = (l + r) / 2;
		if (L <= mid)
			query(id * 2, l, mid);
		if (R > mid)
			query(id * 2 + 1, mid + 1, r);
	}
}
int main()
{
	int T;
	scanf("%d", &T);
	for (int t = 1; t <= T; t++)
	{
		printf("Case #%d:\n", t);
		int n, m;
		scanf("%d%d", &n, &m);
		fill(head, head + n, 0);
		e = 0;
		for (int i = 1; i < n; i++)
		{
			int u, v;
			scanf("%d%d", &u, &v);
			add_edge(u, v);
			add_edge(v, u);
		}
		for (int i = 0; i < n; i++)
			scanf("%d", w + i);
		dist[0] = w[0];
		fill(vis, vis + n, false);
		now = 0;
		dfs(0);
		for (int i = 0; i < n; i++)
			a[into[i]] = dist[i];
		build(1, 1, n);
		while (m--)
		{
			int opt;
			scanf("%d", &opt);
			if (opt == 0)
			{
				int x, y;
				scanf("%d%d", &x, &y);
				L = into[x];
				R = outo[x];
				val = y - w[x];
				w[x] = y;
				modify(1, 1, n);
			}
			else
			{
				int x;
				scanf("%d", &x);
				L = into[x];
				R = outo[x];
				val = -INF;
				query(1, 1, n);
				printf("%lld\n", val);
			}
		}
	}
	return 0;
}
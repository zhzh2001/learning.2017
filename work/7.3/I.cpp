#include <bits/stdc++.h>
using namespace std;
const int N = 105;
bitset<N> lang[N];
int f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	bool nolang = true;
	for (int i = 1; i <= n; i++)
	{
		int t;
		scanf("%d", &t);
		if (t)
			nolang = false;
		while (t--)
		{
			int id;
			scanf("%d", &id);
			lang[i][id] = true;
		}
	}
	if (nolang)
	{
		printf("%d\n", n);
		return 0;
	}
	for (int i = 1; i <= n; i++)
		f[i] = i;
	for (int i = 1; i <= n; i++)
		for (int j = i + 1; j <= n; j++)
			if ((lang[j] & lang[i]).count())
				f[getf(j)] = getf(i);
	int ans = 0;
	for (int i = 1; i <= n; i++)
		ans += getf(i) == i;
	printf("%d\n", ans - 1);
	return 0;
}
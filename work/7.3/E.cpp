#include <cstdio>
#include <vector>
using namespace std;
const int N = 100005;
int head[N], v[N * 2], nxt[N * 2], n, into[N], outo[N], now, a[N], e;
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
void dfs(int k)
{
	into[k] = ++now;
	for (int i = head[k]; i; i = nxt[i])
		if (!into[v[i]])
			dfs(v[i]);
	outo[k] = now;
}
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x <= n; x += x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x; x -= x & -x)
			ans += tree[x];
		return ans;
	}
} T;
int main()
{
	scanf("%d", &n);
	for (int i = 1; i < n; i++)
	{
		int u, v;
		scanf("%d%d", &u, &v);
		add_edge(u, v);
		add_edge(v, u);
	}
	dfs(1);
	for (int i = 1; i <= n; i++)
	{
		a[i] = 1;
		T.modify(i, 1);
	}
	int m;
	scanf("%d", &m);
	while (m--)
	{
		char opt[2];
		int u;
		scanf("%1s%u", opt, &u);
		if (opt[0] == 'C')
		{
			if (a[u])
				T.modify(into[u], -1);
			else
				T.modify(into[u], 1);
			a[u] ^= 1;
		}
		else
			printf("%d\n", T.query(outo[u]) - T.query(into[u] - 1));
	}
	return 0;
}
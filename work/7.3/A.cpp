#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int f[N * 2], status[N];
pair<int, int> ctrl[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		scanf("%d", status + i);
	for (int i = 1; i <= m; i++)
	{
		int t;
		scanf("%d", &t);
		while (t--)
		{
			int x;
			scanf("%d", &x);
			if (ctrl[x].first == 0)
				ctrl[x].first = i;
			else
				ctrl[x].second = i;
		}
		f[i] = i;
		f[m + i] = m + i;
	}
	for (int i = 1; i <= n; i++)
		if (status[i])
		{
			int ru = getf(ctrl[i].first), rv = getf(ctrl[i].second);
			if (ru != rv)
				f[ru] = rv;
			ru = getf(m + ctrl[i].first);
			rv = getf(m + ctrl[i].second);
			if (ru != rv)
				f[ru] = rv;
		}
		else
		{
			int ru = getf(ctrl[i].first), rv = getf(m + ctrl[i].second);
			if (ru != rv)
				f[ru] = rv;
			ru = getf(m + ctrl[i].first);
			rv = getf(ctrl[i].second);
			if (ru != rv)
				f[ru] = rv;
		}
	for (int i = 1; i <= m; i++)
		if (getf(i) == getf(m + i))
		{
			puts("NO");
			return 0;
		}
	puts("YES");
	return 0;
}
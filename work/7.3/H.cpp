#include <bits/stdc++.h>
using namespace std;
const int N = 1005;
int f[N], a[N], sz[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, m, k;
	scanf("%d%d%d", &n, &m, &k);
	for (int i = 1; i <= k; i++)
		scanf("%d", a + i);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	for (int i = 1; i <= m; i++)
	{
		int u, v;
		scanf("%d%d", &u, &v);
		f[getf(u)] = getf(v);
	}
	for (int i = 1; i <= n; i++)
		sz[getf(i)]++;
	int ans = 0, maxsz = 0, desp = n;
	for (int i = 1; i <= k; i++)
	{
		int root = getf(a[i]);
		ans += sz[root] * (sz[root] - 1) / 2;
		maxsz = max(maxsz, sz[root]);
		desp -= sz[root];
	}
	ans += desp * (desp - 1) / 2;
	ans += maxsz * desp;
	ans -= m;
	printf("%d\n", ans);
	return 0;
}
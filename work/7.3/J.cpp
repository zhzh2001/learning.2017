#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int p[N], f[N], state[N], ans[N];
unordered_map<int, int> id;
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, a, b;
	scanf("%d%d%d", &n, &a, &b);
	for (int i = 1; i <= n; i++)
	{
		scanf("%d", p + i);
		id[p[i]] = i;
		f[i] = i;
		ans[i] = 3;
	}
	for (int i = 1; i <= n; i++)
	{
		if (id.find(a - p[i]) != id.end())
		{
			f[getf(i)] = getf(id[a - p[i]]);
			state[i] |= 1;
		}
		if (id.find(b - p[i]) != id.end())
		{
			f[getf(i)] = getf(id[b - p[i]]);
			state[i] |= 2;
		}
	}
	for (int i = 1; i <= n; i++)
		ans[getf(i)] &= state[i];
	for (int i = 1; i <= n; i++)
		if (!ans[i])
		{
			puts("NO");
			return 0;
		}
	puts("YES");
	for (int i = 1; i <= n; i++)
		printf("%d ", (bool)(ans[getf(i)] & 2));
	puts("");
	return 0;
}
#include <cstdio>
#include <algorithm>
#include <utility>
using namespace std;
const int N = 205, M = 1005, Q = 15, INF = 0x3f3f3f3f;
struct edge
{
	int u, v, w;
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} e[M];
int f[N], ans[Q];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
pair<int, int> quest[Q];
int main()
{
	int n, m;
	while (scanf("%d%d", &n, &m) == 2)
	{
		for (int i = 1; i <= m; i++)
			scanf("%d%d%d", &e[i].u, &e[i].v, &e[i].w);
		sort(e + 1, e + m + 1);
		int q;
		scanf("%d", &q);
		for (int i = 1; i <= q; i++)
		{
			scanf("%d%d", &quest[i].first, &quest[i].second);
			ans[i] = INF;
		}
		for (int i = 1; i <= m; i++)
		{
			for (int i = 1; i <= n; i++)
				f[i] = i;
			for (int j = i; j <= m; j++)
			{
				int ru = getf(e[j].u), rv = getf(e[j].v);
				if (ru != rv)
					f[ru] = rv;
				for (int k = 1; k <= q; k++)
					if (getf(quest[k].first) == getf(quest[k].second))
						ans[k] = min(ans[k], e[j].w - e[i].w);
			}
		}
		for (int i = 1; i <= q; i++)
			if (ans[i] < INF)
				printf("%d\n", ans[i]);
			else
				puts("-1");
	}
	return 0;
}
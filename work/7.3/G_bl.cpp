#include <bits/stdc++.h>
using namespace std;
const int N = 505, M = 10005;
pair<int, int> e[M];
int f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= m; i++)
		scanf("%d%d", &e[i].first, &e[i].second);
	int k;
	scanf("%d", &k);
	while (k--)
	{
		int l, r;
		scanf("%d%d", &l, &r);
		for (int i = 1; i <= n; i++)
			f[i] = i;
		for (int i = 1; i < l; i++)
			f[getf(e[i].first)] = getf(e[i].second);
		for (int i = r + 1; i <= m; i++)
			f[getf(e[i].first)] = getf(e[i].second);
		int ans = 0;
		for (int i = 1; i <= n; i++)
			ans += getf(i) == i;
		printf("%d\n", ans);
	}
	return 0;
}
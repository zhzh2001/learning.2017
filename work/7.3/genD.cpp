#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
const int t = 10, n = 10, m = 100;
int f[n + 5];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	minstd_rand gen(GetTickCount());
	uniform_int_distribution<> d(1, n);
	printf("%d\n", t);
	for (int T = 1; T <= t; T++)
	{
		printf("%d %d\n", n, m);
		for (int i = 1; i <= n; i++)
			f[i] = i;
		for (int i = 1; i < n; i++)
		{
			int u = d(gen), v = d(gen);
			if (getf(u) != getf(v))
			{
				f[getf(u)] = getf(v);
				printf("%d %d\n", u, v);
			}
		}
		uniform_int_distribution<> dw(-1e5, 1e5);
		for (int i = 1; i <= n; i++)
			printf("%d ", dw(gen));
		puts("");
		for (int i = 1; i <= m; i++)
		{
			uniform_int_distribution<> dopt(0, 1);
			if (dopt(gen))
				printf("1 %d\n", d(gen));
			else
				printf("0 %d %d\n", d(gen), dw(gen));
		}
	}
	return 0;
}
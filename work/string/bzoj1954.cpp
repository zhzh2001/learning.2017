#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=200005;
int ch[N*16][2],head[N],v[M],w[M],nxt[M],e,d[N],cc;
bool vis[N];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
		if(!vis[v[i]])
		{
			d[v[i]]=d[k]^w[i];
			dfs(v[i]);
		}
}
void trie_insert(int key)
{
	int now=1;
	for(int i=30;i>=0;i--)
	{
		bool b=key&(1<<i);
		int& c=ch[now][b];
		if(!c)
		{
			c=++cc;
			ch[cc][0]=ch[cc][1]=0;
		}
		now=c;
	}
}
int main()
{
	int n;
	while(cin>>n)
	{
		e=0;
		memset(head,0,sizeof(head));
		for(int i=1;i<n;i++)
		{
			int u,v,w;
			cin>>u>>v>>w;
			add_edge(u,v,w);
			add_edge(v,u,w);
		}
		d[1]=0;
		memset(vis,false,sizeof(vis));
		dfs(1);
		ch[1][0]=ch[1][1]=0;
		cc=1;
		for(int i=1;i<=n;i++)
			trie_insert(d[i]);
		int ans=0;
		for(int i=1;i<=n;i++)
		{
			int now=1,num=0;
			for(int j=30;j>=0;j--)
			{
				bool b=d[i]&(1<<j);
				if(ch[now][b^1])
				{
					num+=1<<j;
					now=ch[now][b^1];
				}
				else
					now=ch[now][b];
			}
			ans=max(ans,num);
		}
		cout<<ans<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=800005;
int ch[N][26],end[N],cc,ans;
string s;
void trie_insert(const string& s,bool flag)
{
	int now=0,i;
	for(i=0;i<s.length();now=ch[now][s[i]-'a'],i++)
		if(!ch[now][s[i]-'a'])
			ch[now][s[i]-'a']=++cc;
	if(flag)
		end[now]++;
	else
	{
		if(end[now]>0)
			ans+=i;
		end[now]--;
	}
}
inline void dfs(int k,int dep)
{
	for(int i=0;i<26;i++)
		if(ch[k][i])
		{
			dfs(ch[k][i],dep+1);
			if((long long)end[k]*end[ch[k][i]]<0)
				ans+=dep*min(abs(end[k]),abs(end[ch[k][i]]));
			end[k]+=end[ch[k][i]];
		}
}
int main()
{
	freopen("test.txt","r",stdin);
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>s;
		trie_insert(s,true);
	}
	for(int i=1;i<=n;i++)
	{
		cin>>s;
		trie_insert(s,false);
	}
	dfs(0,0);
	cout<<ans<<endl;
	return 0;
}
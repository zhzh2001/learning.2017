#include<bits/stdc++.h>
using namespace std;
string s;
const int N=1000005,MOD=1000000007;
int nxt[N],cnt[N];
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		cin>>s;
		int n=s.length();
		s=' '+s;
		int j=0;
		cnt[1]=1;
		for(int i=2;i<=n;i++)
		{
			while(j&&s[i]!=s[j+1])
				j=nxt[j];
			if(s[i]==s[j+1])
				j++;
			nxt[i]=j;
			cnt[i]=cnt[j]+1;
		}
		j=0;
		long long ans=1;
		for(int i=2;i<=n;i++)
		{
			while(j&&s[i]!=s[j+1])
				j=nxt[j];
			if(s[i]==s[j+1])
				j++;
			while(j*2>i)
				j=nxt[j];
			(ans*=cnt[j]+1)%=MOD;
		}
		cout<<ans<<endl;
	}
	return 0;
}
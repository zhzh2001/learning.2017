#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int nxt[N],cnt[N],flag[N];
int main()
{
	string s;
	cin>>s;
	int n=s.length();
	s=' '+s;
	
	int j=0;
	for(int i=2;i<=n;i++)
	{
		while(j&&s[i]!=s[j+1])
			j=nxt[j];
		if(s[i]==s[j+1])
			j++;
		nxt[i]=j;
	}
	
	for(int i=n;i;i--)
	{
		cnt[nxt[i]]+=1+flag[i];
		flag[nxt[i]]+=1+flag[i];
	}
	long long ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,(long long)(cnt[i]+1)*i);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=205,alpha=26,LEN=1000005;
int ch[N][alpha],f[LEN],cc;
string s;
bool end[N];
void trie_insert(const string& s)
{
	int j=0,n=s.length();
	for(int i=0;i<n;j=ch[j][s[i]-'a'],i++)
		if(!ch[j][s[i]-'a'])
			ch[j][s[i]-'a']=++cc;
	end[j]=true;
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(n--)
	{
		cin>>s;
		trie_insert(s);
	}
	for(int t=1;t<=m;t++)
	{
		cin>>s;
		int len=s.length();
		s=' '+s;
		f[0]=t;
		for(int i=1;i<=len;i++)
			if(f[i-1]==t)
			{
				int j=0;
				for(int k=i;k<=len;k++)
				{
					if(!ch[j][s[k]-'a'])
						break;
					j=ch[j][s[k]-'a'];
					if(end[j])
						f[k]=f[i-1];
				}
			}
		for(int i=len;i>=0;i--)
			if(f[i]==t)
			{
				cout<<i<<endl;
				break;
			}
	}
	return 0;
}
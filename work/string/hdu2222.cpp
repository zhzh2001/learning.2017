#include<bits/stdc++.h>
using namespace std;
const int alpha=26,N=10005,M=500005,L=1000005;
struct node
{
	int ch[alpha],fail,cnt,last,id;
}trie[M];
int cc,ans;
char s[L];
bool vis[N];
void trie_insert(char* s,int id)
{
	int now=0;
	for(;*s;now=trie[now].ch[*s-'a'],s++)
		if(!trie[now].ch[*s-'a'])
		{
			trie[now].ch[*s-'a']=++cc;
			trie[cc].cnt=0;
			memset(trie[cc].ch,0,sizeof(trie[cc].ch));
		}
	trie[now].cnt++;
	trie[now].id=id;
}
void calc_fail()
{
	queue<int> Q;
	for(int i=0;i<alpha;i++)
	{
		int u=trie[0].ch[i];
		if(u)
		{
			Q.push(u);
			trie[u].fail=trie[u].last=0;
		}
	}
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=0;i<alpha;i++)
		{
			int u=trie[k].ch[i];
			if(u)
			{
				Q.push(u);
				int v;
				for(v=trie[k].fail;v&&!trie[v].ch[i];v=trie[v].fail);
				trie[u].fail=trie[v].ch[i];
				trie[u].last=trie[trie[u].fail].cnt?trie[u].fail:trie[trie[u].fail].last;
			}
		}
	}
}
void find(char* s)
{
	int j=0;
	for(;*s;s++)
	{
		while(j&&!trie[j].ch[*s-'a'])
			j=trie[j].fail;
		j=trie[j].ch[*s-'a'];
		if(trie[j].cnt||trie[j].last)
			for(int k=trie[j].cnt?j:trie[j].last;k;k=trie[k].last)
				if(!vis[trie[k].id])
				{
					vis[trie[k].id]=true;
					ans+=trie[k].cnt;
				}
	}
}
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n;
		scanf("%d",&n);
		cc=0;
		memset(&trie[0],0,sizeof(node));
		gets(s);
		for(int i=1;i<=n;i++)
		{
			gets(s);
			trie_insert(s,i);
		}
		calc_fail();
		gets(s);
		memset(vis,false,sizeof(vis));
		ans=0;
		find(s);
		cout<<ans<<endl;
	}
	return 0;
}
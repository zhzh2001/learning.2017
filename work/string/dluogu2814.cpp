#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
using namespace __gnu_pbds;
cc_hash_table<string,string> f;
string s,t;
string getf(const string& s)
{
	return f[s]==s?s:f[s]=getf(f[s]);
}
int main()
{
	ios::sync_with_stdio(false);
	cin>>s;
	while(s[0]=='#')
	{
		s.erase(0,1);
		if(f.find(s)==f.end())
			f[s]=s;
		while(cin>>t&&t[0]=='+')
			f[t.substr(1)]=s;
		s=t;
	}
	while(s[0]=='?')
	{
		s.erase(0,1);
		cout<<s<<' '<<getf(s)<<endl;
		cin>>s;
	}
	return 0;
}
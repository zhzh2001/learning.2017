#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstring>
#include<cmath>
#include<cstdlib>
#define N 800005
using namespace std;
int c[N][26],tag[N],dep[N],n,len,ans,cnt;
char s[N];
void dfs(int x){
    if (dep[x]>=500000){
        printf("1");
        exit(0);
    }
    for (int i=0;i<26;i++)
        if (c[x][i]){
            dfs(c[x][i]);
            if (tag[x]>0&&tag[c[x][i]]<0)
                ans+=min(tag[x],-tag[c[x][i]])*dep[x];
            else if (tag[x]<0&&tag[c[x][i]]>0)
                ans+=min(-tag[x],tag[c[x][i]])*dep[x];
            tag[x]+=tag[c[x][i]];
        }
}
int main(){
	freopen("test.txt","r",stdin);
    scanf("%d",&n);
    for (int i=1;i<=n;i++){
        scanf("%s",s+1);
        len=strlen(s+1);
        int x=0;
        for (int i=1;i<=len;i++){
            if (!c[x][s[i]-'a'])
                c[x][s[i]-'a']=++cnt;
            x=c[x][s[i]-'a'];
            dep[x]=i;
        }
        tag[x]++;
    }
    for (int i=1;i<=n;i++){
        scanf("%s",s+1);
        len=strlen(s+1);
        int x=0;
        for (int i=1;i<=len;i++){
            if (!c[x][s[i]-'a'])
                c[x][s[i]-'a']=++cnt;
            x=c[x][s[i]-'a'];
            dep[x]=i;
        }
        if (tag[x]>0) ans+=dep[x];
        tag[x]--;
    }
    dfs(0);
    printf("%d",ans);
}
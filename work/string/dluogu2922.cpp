#include<bits/stdc++.h>
using namespace std;
const int N=500005,LEN=10005;
struct node
{
	int ch[2],end,sz;
}trie[N];
int cc,a[LEN];
void trie_insert(int a[],int n)
{
	int j=0;
	for(int i=1;i<=n;j=trie[j].ch[a[i]],i++)
	{
		if(!trie[j].ch[a[i]])
			trie[j].ch[a[i]]=++cc;
		trie[j].sz++;
	}
	trie[j].sz++;
	trie[j].end++;
}
void trie_query(int a[],int n)
{
	int j=0,ans=0;
	for(int i=1;i<=n;j=trie[j].ch[a[i]],i++)
	{
		if(!trie[j].ch[a[i]])
			break;
		if(i<n)
			ans+=trie[trie[j].ch[a[i]]].end;
		else
			ans+=trie[trie[j].ch[a[i]]].sz;
	}
	cout<<ans<<endl;
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(n--)
	{
		int cnt;
		cin>>cnt;
		for(int i=1;i<=cnt;i++)
			cin>>a[i];
		trie_insert(a,cnt);
	}
	while(m--)
	{
		int cnt;
		cin>>cnt;
		for(int i=1;i<=cnt;i++)
			cin>>a[i];
		trie_query(a,cnt);
	}
	return 0;
}
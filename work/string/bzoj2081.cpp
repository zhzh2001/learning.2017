#include<bits/stdc++.h>
using namespace std;
const int N=200005,BASE=233333;
int a[N],h[N],rh[N],ans[N];
set<int> S;
template<typename T>
inline void readint(T& x)
{
	char c;
	for(c=getchar();isspace(c);c=getchar());
	T sign=1;
	if(c=='-')
		sign=-1,c=getchar();
	x=0;
	for(;isdigit(c);c=getchar())
		x=x*10+c-'0';
	x*=sign;
}
int buf[25];
template<typename T>
inline void writeint(T x)
{
	if(x<0)
		putchar('-'),x=-x;
	int p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
int main()
{
	int n;
	readint(n);
	for(int i=1;i<=n;i++)
	{
		readint(a[i]);
		h[i]=h[i-1]*BASE+a[i];
	}
	for(int i=n;i;i--)
		rh[i]=rh[i+1]*BASE+a[i];
	int p=1,cnt=0,cc;
	for(int i=1;i<=n;i++)
	{
		p*=BASE;
		S.clear();
		for(int j=1;j+i-1<=n;j+=i)
		{
			int key=(h[j+i-1]-h[j-1]*p)*(rh[j]-rh[j+i]*p);
			if(S.find(key)==S.end())
				S.insert(key);
		}
		if((int)S.size()>cnt)
		{
			cnt=S.size();
			ans[cc=1]=i;
		}
		else
			if((int)S.size()==cnt)
				ans[++cc]=i;
	}
	writeint(cnt);
	putchar(' ');
	writeint(cc);
	puts("");
	for(int i=1;i<=cc;i++)
	{
		writeint(ans[i]);
		putchar(' ');
	}
	puts("");
	return 0;
}
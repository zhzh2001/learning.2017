#include<iostream>
#include<string>
#include<queue>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
struct node
{
	bool bl[4][4];
	int step;
};
queue<node> q;
bool vis[70000];
int node2int(node x)
{
	int v=0;
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
			v=v*2+x.bl[i][j];
	return v;
}
int main()
{
	node s;
	for(int i=0;i<4;i++)
	{
		string line;
		cin>>line;
		for(int j=0;j<4;j++)
			s.bl[i][j]=line[j]=='b';
	}
	s.step=0;
	memset(vis,false,sizeof(vis));
	vis[node2int(s)]=true;
	q.push(s);
	while(!q.empty())
	{
		node k=q.front();q.pop();
		bool f=k.bl[0][0],flag=true;
		for(int i=0;i<4;i++)
			for(int j=0;j<4;j++)
				if(k.bl[i][j]^f)
				{
					flag=false;
					break;
				}
		if(flag)
		{
			cout<<k.step<<endl;
			return 0;
		}
		for(int i=0;i<4;i++)
			for(int j=0;j<4;j++)
			{
				node tmp=k;
				tmp.bl[i][j]=!tmp.bl[i][j];
				for(int t=0;t<4;t++)
				{
					int nx=i+dx[t],ny=j+dy[t];
					if(nx>=0&&nx<4&&ny>=0&&ny<4)
						tmp.bl[nx][ny]=!tmp.bl[nx][ny];
				}
				tmp.step=k.step+1;
				if(!vis[node2int(tmp)])
				{
					q.push(tmp);
					vis[node2int(tmp)]=true;
				}
			}
	}
	cout<<"Impossible\n";
	return 0;
}
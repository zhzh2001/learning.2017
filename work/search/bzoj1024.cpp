#include<iostream>
#include<algorithm>
using namespace std;
double dfs(double x,double y,int n)
{
	if(n==1)
	{
		if(x<y)
			swap(x,y);
		return x/y;
	}
	double ret=1e100;
	for(int i=1;i<n;i++)
		ret=min(ret,max(dfs(x,y/n*i,i),dfs(x,y-y/n*i,n-i)));
	for(int i=1;i<n;i++)
		ret=min(ret,max(dfs(x/n*i,y,i),dfs(x-x/n*i,y,n-i)));
	return ret;
}
int main()
{
	int x,y,n;
	cin>>x>>y>>n;
	cout.precision(6);
	cout<<fixed<<dfs(x,y,n)<<endl;
	return 0;
}
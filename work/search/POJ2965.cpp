#include<iostream>
#include<string>
#include<cstring>
using namespace std;
struct node
{
	bool open[4][4];
	int nx,ny,step,pred,cc;
};
node q[70000];
bool vis[70000];
int node2int(node x)
{
	int v=0;
	for(int i=0;i<4;i++)
		for(int j=0;j<4;j++)
			v=v*2+x.open[i][j];
	return v;
}
void print(int x)
{
	if(q[x].pred==-1)
		return;
	print(q[x].pred);
	cout<<q[x].nx+1<<' '<<q[x].ny+1<<endl;
}
int main()
{
	for(int i=0;i<4;i++)
	{
		string line;
		cin>>line;
		for(int j=0;j<4;j++)
		{
			q[1].open[i][j]=line[j]=='-';
			if(line[j]=='-')
				q[1].cc++;
		}
	}
	q[1].step=0;q[1].pred=-1;
	memset(vis,false,sizeof(vis));
	vis[node2int(q[1])]=true;
	int l=1,r=1;
	while(l<=r)
	{
		node k=q[l];
		if(k.cc==16)
		{
			cout<<k.step<<endl;
			print(l);
			return 0;
		}
		for(int i=0;i<4;i++)
			for(int j=0;j<4;j++)
			{
				node tmp=k;
				tmp.nx=i;tmp.ny=j;
				tmp.open[i][j]=!tmp.open[i][j];
				if(tmp.open[i][j])
					tmp.cc++;
				else
					tmp.cc--;
				for(int t=0;t<4;t++)
				{
					if(t!=i)
					{
						tmp.open[t][j]=!tmp.open[t][j];
						if(tmp.open[t][j])
							tmp.cc++;
						else
							tmp.cc--;
					}
					if(t!=j)
					{
						tmp.open[i][t]=!tmp.open[i][t];
						if(tmp.open[i][t])
							tmp.cc++;
						else
							tmp.cc--;
					}
				}
				tmp.step=k.step+1;
				tmp.pred=l;
				int key=node2int(tmp);
				if(!vis[key])
				{
					q[++r]=tmp;
					vis[key]=true;
				}
			}
		l++;
	}
	cout<<"Impossible\n";
	return 0;
}
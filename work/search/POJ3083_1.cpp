#include<iostream>
#include<string>
#include<cstring>
#include<queue>
using namespace std;
const int dx[]={0,-1,0,1},dy[]={-1,0,1,0};
bool mat[45][45],vis[45][45];
struct node
{
	int x,y,step;
};
queue<node> q;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n,m;
		cin>>m>>n;
		int sx,sy,tx,ty;
		for(int i=1;i<=n;i++)
		{
			string line;
			cin>>line;
			for(int j=0;j<m;j++)
			{
				mat[i][j+1]=(line[j]=='#');
				if(line[j]=='S')
					sx=i,sy=j+1;
				if(line[j]=='E')
					tx=i,ty=j+1;
			}
		}
		int face;
		if(sx==1)
			face=3;
		if(sx==n)
			face=1;
		if(sy==1)
			face=2;
		if(sy==m)
			face=0;
		int x=sx,y=sy;
		for(int i=1;;i++)
		{
			int nx,ny,nf;
			for(int j=-1;j<3;j++)
			{
				nf=(face+j+4)%4;
				nx=x+dx[nf];ny=y+dy[nf];
				if(nx>0&&nx<=n&&ny>0&&ny<=m&&!mat[nx][ny])
					break;
			}
			x=nx;y=ny;face=nf;
			if(x==tx&&y==ty)
			{
				cout<<i+1;
				break;
			}
		}
		x=sx;y=sy;
		for(int i=1;;i++)
		{
			int nx,ny,nf;
			for(int j=1;j>-3;j--)
			{
				nf=(face+j+4)%4;
				nx=x+dx[nf];ny=y+dy[nf];
				if(nx>0&&nx<=n&&ny>0&&ny<=m&&!mat[nx][ny])
					break;
			}
			x=nx;y=ny;face=nf;
			if(x==tx&&y==ty)
			{
				cout<<' '<<i+1;
				break;
			}
		}
		while(!q.empty())
			q.pop();
		q.push((node){sx,sy,0});
		memset(vis,false,sizeof(vis));
		vis[sx][sy]=true;
		while(!q.empty())
		{
			node k=q.front();q.pop();
			if(k.x==tx&&k.y==ty)
			{
				cout<<' '<<k.step+1<<endl;
				break;
			}
			for(int i=0;i<4;i++)
			{
				int nx=k.x+dx[i],ny=k.y+dy[i];
				if(nx>0&&nx<=n&&ny>0&&ny<=m&&!mat[nx][ny]&&!vis[nx][ny])
				{
					vis[nx][ny]=true;
					q.push((node){nx,ny,k.step+1});
				}
			}
		}
	}
	return 0;
}
#include<iostream>
#include<queue>
#include<cstdio>
#include<cstdlib>
#include<set>
using namespace std;
const int dx[]={-2,-2,-1,-1,1,1,2,2},dy[]={-1,1,-2,2,-2,2,-1,1};
struct qnode
{
	int x,y,step;
};
queue<qnode> q;
char buf[100];
set<string> s;
int main()
{
	int sx,sy,tx,ty;
	cin>>sx>>sy>>tx>>ty;
	int cx=abs(sx-tx),cy=abs(sy-ty),ans=0;
	while(cx+cy>50)
	{
		if(cx<cy)
			swap(cx,cy);
		cx-=4;
		if(cx<2*cy)
			cy-=2;
		ans+=2;
	}
	s.insert("(0,0)");
	q.push((qnode){0,0,0});
	while(!q.empty())
	{
		qnode k=q.front();q.pop();
		if(k.x==cx&&k.y==cy)
		{
			cout<<ans+k.step<<endl;
			break;
		}
		for(int i=0;i<8;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			sprintf(buf,"(%d,%d)",nx,ny);
			if(!s.count((string)buf))
			{
				s.insert((string)buf);
				q.push((qnode){nx,ny,k.step+1});
			}
		}
	}
	return 0;
}
#include<iostream>
#include<cstring>
using namespace std;
const int dx[]={-2,-2,-1,-1,1,1,2,2},dy[]={-1,1,-2,2,-2,2,-1,1};
int m,n,ax[30],ay[30];
bool vis[30][30],found;
void dfs(int i,int j,int k)
{
	vis[i][j]=true;
	ax[k]=i;ay[k]=j;
	if(k==n*m)
	{
		for(int x=1;x<=n*m;x++)
			cout<<char(ax[x]+'A'-1)<<ay[x];
		cout<<endl<<endl;
		found=true;
		return;
	}
	for(int d=0;d<8;d++)
	{
		int nx=i+dx[d],ny=j+dy[d];
		if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis[nx][ny])
		{
			dfs(nx,ny,k+1);
			if(found)
				return;
		}
	}
	vis[i][j]=false;
}
int main()
{
	int p;
	cin>>p;
	for(int i=1;i<=p;i++)
	{
		cout<<"Scenario #"<<i<<":\n";
		cin>>m>>n;
		memset(vis,false,sizeof(vis));
		found=false;
		dfs(1,1,1);
		if(!found)
			cout<<"impossible\n\n";
	}
	return 0;
}
#include<iostream>
#include<string>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
bool maze[45][45];
inline int turnl(int face)
{
	if(face==0)
		return 2;
	if(face==2)
		return 1;
	if(face==1)
		return 3;
	return 0;
}
inline int turnr(int face)
{
	if(face==0)
		return 3;
	if(face==3)
		return 1;
	if(face==1)
		return 2;
	return 0;
}
int main()
{
	int p;
	cin>>p;
	while(p--)
	{
		int n,m;
		cin>>n>>m;
		int sx,sy,tx,ty;
		for(int i=1;i<=n;i++)
		{
			string line;
			cin>>line;
			for(int j=0;j<m;j++)
			{
				maze[i][j+1]=line[j]=='#';
				if(line[j]=='S')
					sx=i,sy=j+1;
				if(line[j]=='E')
					tx=i,ty=j+1;
			}
		}
		int x=sx,y=sy,face;
		if(sx==1)
			face=1;
		if(sx==n)
			face=0;
		if(sy==1)
			face=3;
		if(sy==m)
			face=2;
		int ans=0;
		/*
		while(x!=tx||y!=ty)
		{
			x+=dx[face];y+=dy[face];
			cout<<x<<' '<<y<<endl;
			ans++;
			int nx=x+dx[turnl(face)],ny=y+dy[turnl(face)];
			if(nx<=0||nx>n||ny<=0||ny>m||!maze[nx][ny]||maze[x+dx[face]][y+dy[face]])
				do
				{
					face=turnl(face);
					nx=x+dx[face];ny=y+dy[face];
				}
				while(nx<=0||nx>n||ny<=0||ny>m||maze[nx][ny]);
		}
		*/
		while(x!=tx||y!=ty)
		{
			int nx=x+dx[face],ny=y+dy[face],nx1=x+dx[turnl(face)],ny1=y+dy[turnl(face)];
			if(!(nx>0&&nx<=n&&ny>0&&ny<=m&&!maze[nx][ny]&&(nx1<=0||nx1>n||ny1<=0||ny1>m||maze[nx1][ny1])))
				do
				{
					face=turnr(face);
					nx=x+dx[face];ny=y+dy[face];
				}
				while(nx<=0||nx>n||ny<=0||ny>m||maze[nx][ny]);
			x=nx;y=ny;
			ans++;
			cout<<x<<' '<<y<<endl;
		}
		cout<<ans<<endl;
	}
	return 0;
}
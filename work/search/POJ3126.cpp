#include<iostream>
#include<cstring>
#include<queue>
using namespace std;
bool p[10005],vis[10005];
struct node
{
	int num,step;
};
queue<node> q;
int main()
{
	memset(p,true,sizeof(p));
	for(int i=2;i<=100;i++)
		if(p[i])
			for(int j=i*i;j<=10000;j+=i)
				p[j]=false;
	int t;
	cin>>t;
	while(t--)
	{
		int src,tar;
		cin>>src>>tar;
		while(!q.empty())
			q.pop();
		q.push((node){src,0});
		memset(vis,false,sizeof(vis));
		vis[src]=true;
		bool found=false;
		while(!q.empty())
		{
			node k=q.front();q.pop();
			if(k.num==tar)
			{
				cout<<k.step<<endl;
				found=true;
				break;
			}
			for(int i=1;i<10000;i*=10)
			{
				int tmp=k.num-k.num/i%10*i;
				for(int j=0;j<10;j++)
					if(i<1000|j)
						if(!vis[tmp+j*i]&&p[tmp+j*i])
						{
							vis[tmp+j*i]=true;
							q.push((node){tmp+j*i,k.step+1});
						}
			}
		}
		if(!found)
			cout<<"Impossible\n";
	}
	return 0;
}
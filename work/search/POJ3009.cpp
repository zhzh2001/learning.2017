#include<iostream>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
bool mat[25][25];
int n,m;
bool dfs(int k,int x,int y,int tx,int ty,int maxs)
{
	if(k>maxs)
		return false;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(mat[nx][ny])
			continue;
		while(nx>0&&nx<=n&&ny>0&&ny<=m&&!mat[nx][ny]&&(nx!=tx||ny!=ty))
			nx+=dx[i],ny+=dy[i];
		if(nx<=0||nx>n||ny<=0||ny>m)
			continue;
		if(nx==tx&&ny==ty)
			return true;
		mat[nx][ny]=false;
		if(dfs(k+1,nx-dx[i],ny-dy[i],tx,ty,maxs))
			return true;
		mat[nx][ny]=true;
	}
	return false;
}
int main()
{
	while(cin>>m>>n&&n*m)
	{
		int sx,sy,tx,ty;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
			{
				int x;
				cin>>x;
				mat[i][j]=(x==1);
				if(x==2)
					sx=i,sy=j;
				if(x==3)
					tx=i,ty=j;
			}
		bool found=false;
		for(int i=1;i<=10;i++)
			if(dfs(1,sx,sy,tx,ty,i))
			{
				cout<<i<<endl;
				found=true;
				break;
			}
		if(!found)
			cout<<-1<<endl;
	}
	return 0;
}
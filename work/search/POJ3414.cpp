#include<iostream>
#include<cstring>
using namespace std;
struct node
{
	int a,b,pred,p1,p2;
};
node q[10005];
bool vis[105][105];
int l,r,a1[10005],a2[10005];
inline void chk(node x)
{
	if(!vis[x.a][x.b])
	{
		vis[x.a][x.b]=true;
		q[++r]=x;
	}
}
int main()
{
	int a,b,c;
	cin>>a>>b>>c;
	q[1].a=0;q[1].b=0;q[1].pred=-1;
	memset(vis,false,sizeof(vis));
	vis[0][0]=true;
	l=r=1;
	while(l<=r)	
	{
		node k=q[l];l++;
		if(k.a==c||k.b==c)
		{
			int cnt=1;
			for(int p=l-1;q[p].pred!=-1;p=q[p].pred,cnt++)
				a1[cnt]=q[p].p1,a2[cnt]=q[p].p2;
			cnt--;
			cout<<cnt<<endl;
			for(int i=cnt;i>=1;i--)
				switch(a1[i])
				{
					case 1:cout<<"FILL("<<a2[i]<<")\n";break;
					case 2:cout<<"DROP("<<a2[i]<<")\n";break;
					case 3:cout<<"POUR("<<a2[i]<<','<<3-a2[i]<<")\n";break;
				}
			return 0;
		}
		node tmp=k;
		tmp.a=a;
		tmp.p1=1;tmp.p2=1;
		tmp.pred=l-1;
		chk(tmp);
		tmp=k;
		tmp.b=b;
		tmp.p1=1;tmp.p2=2;
		tmp.pred=l-1;
		chk(tmp);
		tmp=k;
		tmp.a=0;
		tmp.p1=2;tmp.p2=1;
		tmp.pred=l-1;
		chk(tmp);
		tmp=k;
		tmp.b=0;
		tmp.p1=2;tmp.p2=2;
		tmp.pred=l-1;
		chk(tmp);
		tmp=k;
		if(tmp.a+tmp.b>=b)
		{
			tmp.a-=(b-tmp.b);
			tmp.b=b;
		}
		else
		{
			tmp.b+=tmp.a;
			tmp.a=0;
		}
		tmp.p1=3;tmp.p2=1;
		tmp.pred=l-1;
		chk(tmp);
		tmp=k;
		if(tmp.a+tmp.b>=a)
		{
			tmp.b-=(a-tmp.a);
			tmp.a=a;
		}
		else
		{
			tmp.a+=tmp.b;
			tmp.b=0;
		}
		tmp.p1=3;tmp.p2=2;
		tmp.pred=l-1;
		chk(tmp);
	}
	cout<<"impossible\n";
	return 0;
}
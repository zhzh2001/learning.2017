#include<iostream>
#include<string>
#include<cstring>
using namespace std;
const int mask[]={63624,62532,61986,61713,36744,20292,12066,7953,35064,17652,8946,4593,34959,17487,8751,4383};
struct node
{
	int num,pred;
	short dx,dy;
};
node q[70000];
bool vis[70000];
int ax[70000],ay[70000];
int main()
{
	int cur=0;
	for(int i=0;i<4;i++)
	{
		string line;
		cin>>line;
		for(int j=0;j<4;j++)
			cur=cur*2+(line[j]=='+');
	}
	memset(vis,false,sizeof(vis));
	q[1].num=cur;q[1].pred=-1;
	int l=1,r=1;
	while(l<=r)
	{
		node k=q[l];
		if(k.num==0)
		{
			int cnt=1;
			for(int p=l;q[p].pred!=-1;p=q[p].pred,cnt++)
				ax[cnt]=q[p].dx,ay[cnt]=q[p].dy;
			cnt--;
			cout<<cnt<<endl;
			for(int i=cnt;i>=1;i--)
				cout<<ax[i]<<' '<<ay[i]<<endl;
			return 0;
		}
		for(int i=0;i<16;i++)
		{
			int now=k.num^mask[i];
			if(!vis[now])
			{
				vis[now]=true;
				q[++r].num=now;
				q[r].dx=i/4+1;q[r].dy=i%4+1;
				q[r].pred=l;
			}
		}
		l++;
	}
	cout<<"Impossible\n";
	return 0;
}
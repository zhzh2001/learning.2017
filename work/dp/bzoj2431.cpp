#include<iostream>
#include<algorithm>
using namespace std;
const int MOD=10000;
short f[1005][1005],s[1005][1005];
int main()
{
	int n,k;
	cin>>n>>k;
	f[0][0]=s[0][0]=1;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=k;j++)
		{
			if(j-i<0)
				f[i][j]=s[i-1][j];
			else
				f[i][j]=(s[i-1][j]-s[i-1][j-i]+MOD)%MOD;
			if(j)
				s[i][j]=(s[i][j-1]+f[i][j])%MOD;
			else
				s[i][j]=f[i][j];
		}
	cout<<s[n][k]<<endl;
	return 0;
}
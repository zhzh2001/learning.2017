#include<iostream>
#include<algorithm>
using namespace std;
int a[10005],f[10005],t[10005];
bool cmp(int x,int y)
{
	return x>y;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int maxl=0;
	for(int i=n;i>=1;i--)
    {
        int p=upper_bound(t+1,t+n+1,a[i],cmp)-t;
        if(t[p-1]>a[i])
            p++;
        t[p-1]=a[i];
        f[i]=p;
        maxl=max(maxl,f[i]);
    }
	int m;
	cin>>m;
	while(m--)
	{
		int x;
		cin>>x;
		if(x>maxl)
			cout<<"Impossible\n";
		else
		{
			int pred=0;
			bool first=true;
			for(int i=1;x;i++)
				if(f[i]>=x&&a[i]>pred)
				{
					if(!first)
						cout<<' ';
					first=false;
					cout<<a[i];
					x--;
					pred=a[i];
				}
			cout<<endl;
		}
	}
	return 0;
}
#include<iostream>
#include<queue>
#include<vector>
using namespace std;
const int p=10007;
int a[50005],s[50005];
short f[50005][1005];
priority_queue<int,vector<int>,greater<int> > q;
int main()
{
	int n,m;
	cin>>n>>m;
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		ans=max(ans,a[i]);
		s[i]=s[i-1]+a[i];
		q.push(a[i]);
	}
	for(int i=1;i<=n-1-m;i++)
	{
		int x=q.top(),y=q.top();
		ans=max(ans,x+y);
		q.push(x+y);
	}
	cout<<ans;
	f[0][0]=1;
	int pred=0;
	for(int i=1;i<=n;i++)
	{
		while(s[i]-s[pred]>ans)
			pred++;
		for(int j=1;j<=m+1&&j<=i;j++)
			for(int k=pred;k<i;k++)
				f[i][j]=(f[i][j]+f[k][j-1])%p;
	}
	ans=0;
	for(int j=1;j<=m+1;j++)
		ans=(ans+f[n][j])%p;
	cout<<' '<<ans<<endl;
	return 0;
}
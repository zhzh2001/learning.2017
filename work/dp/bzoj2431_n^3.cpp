#include<iostream>
using namespace std;
const int MOD=10000;
int f[1005][1005];
int main()
{
    int n,k;
    cin>>n>>k;
    f[0][0]=1;
    for(int i=1;i<=n;i++)
        for(int j=0;j<=k;j++)
            for(int t=0;t<i&&t<=j;t++)
                f[i][j]=(f[i][j]+f[i-1][j-t])%MOD;
    cout<<f[n][k]<<endl;
    return 0;
}
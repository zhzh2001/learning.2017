#include<iostream>
#include<string>
#include<cstring>
using namespace std;
string a;
int f[4][4][4][4],g[4][4][4][4];
bool v[4];
inline int cnt(int i,int j,int k)
{
	memset(v,false,sizeof(v));
	v[i]=v[j]=v[k]=true;
	return v[1]+v[2]+v[3];
}
int main()
{
	int n;
	cin>>n>>a;
	int ans=0;
	memset(f,-1,sizeof(f));
	f[0][0][0][0]=0;
	for(int i=1;i<=n;i++)
	{
		int now;
		switch(a[i-1])
		{
			case 'M':now=1;break;
			case 'F':now=2;break;
			case 'B':now=3;break;
		}
		memset(g,-1,sizeof(g));
		for(int j=0;j<=3;j++)
			for(int k=0;k<=3;k++)
				for(int p=0;p<=3;p++)
					for(int q=0;q<=3;q++)
					{
						if(f[j][k][p][q]==-1)
							continue;
						g[k][now][p][q]=max(g[k][now][p][q],f[j][k][p][q]+cnt(now,j,k));
						if(i==n)
							ans=max(ans,g[k][now][p][q]);
						g[j][k][q][now]=max(g[j][k][q][now],f[j][k][p][q]+cnt(now,p,q));
						if(i==n)
							ans=max(ans,g[j][k][q][now]);
					}
		memcpy(f,g,sizeof(f));
	}
	cout<<ans<<endl;
	return 0;
}
#include<iostream>
#include<cassert>
#define decr(x) if(x>=0)\
ans-=f[x];
#define incr(x) if(x>=0)\
ans+=f[x];
using namespace std;
int c[5];
long long f[100005];
int main()
{
	for(int i=1;i<=4;i++)
		cin>>c[i];
	f[0]=1;
	for(int i=1;i<=4;i++)
		for(int j=c[i];j<=100000;j++)
			f[j]+=f[j-c[i]];
	int t;
	cin>>t;
	while(t--)
	{
		int c1,c2,c3,c4,s;
		cin>>c1>>c2>>c3>>c4>>s;
		long long ans=0;
		incr(s);
		decr(s-c[1]*(c1+1));
		decr(s-c[2]*(c2+1));
		decr(s-c[3]*(c3+1));
		decr(s-c[4]*(c4+1));
		incr(s-c[1]*(c1+1)-c[2]*(c2+1));
		incr(s-c[1]*(c1+1)-c[3]*(c3+1));
		incr(s-c[1]*(c1+1)-c[4]*(c4+1));
		incr(s-c[2]*(c2+1)-c[3]*(c3+1));
		incr(s-c[2]*(c2+1)-c[4]*(c4+1));
		incr(s-c[3]*(c3+1)-c[4]*(c4+1));
		decr(s-c[1]*(c1+1)-c[2]*(c2+1)-c[3]*(c3+1));
		decr(s-c[1]*(c1+1)-c[2]*(c2+1)-c[4]*(c4+1));
		decr(s-c[1]*(c1+1)-c[3]*(c3+1)-c[4]*(c4+1));
		decr(s-c[2]*(c2+1)-c[3]*(c3+1)-c[4]*(c4+1));
		incr(s-c[1]*(c1+1)-c[2]*(c2+1)-c[3]*(c3+1)-c[4]*(c4+1));
		cout<<ans<<endl;
	}
	return 0;
}
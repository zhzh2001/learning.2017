#include<bits/stdc++.h>
using namespace std;
const int N=10,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,a[N][N],tag[N][N];
void floodfill(int x,int y,int c)
{
	tag[x][y]=1;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx&&nx<=n&&ny&&ny<=n&&!tag[nx][ny])
		{
			tag[nx][ny]=2;
			if(a[nx][ny]==c)
				floodfill(nx,ny,c);
		}
	}
}
inline int h()
{
	bool vis[6];
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(tag[i][j]!=1)
				vis[a[i][j]]=true;
	int ans=0;
	for(int i=0;i<=5;i++)
		ans+=vis[i];
	return ans;
}
bool modify(int c)
{
	bool ret=false;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(tag[i][j]==2&&a[i][j]==c)
			{
				floodfill(i,j,c);
				ret=true;
			}
	return ret;
}
bool dfs(int k,int maxd)
{
	int t=h();
	if(!t)
		return true;
	if(k+t>maxd)
		return false;
	int tmp[N][N];
	memcpy(tmp,tag,sizeof(tmp));
	for(int i=0;i<=5;i++)
	{
		if(modify(i)&&dfs(k+1,maxd))
			return true;
		memcpy(tag,tmp,sizeof(tag));
	}
	return false;
}
int main()
{
	freopen("bzoj3041.in","r",stdin);
	while(cin>>n&&n)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				cin>>a[i][j];
		memset(tag,0,sizeof(tag));
		floodfill(1,1,a[1][1]);
		for(int i=0;;i++)
			if(dfs(0,i))
			{
				cout<<i<<endl;
				break;
			}
	}
	cout<<clock()<<endl;
	return 0;
}
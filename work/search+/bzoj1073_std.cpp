#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
using namespace std;
#define re(i, n) for (int i=0; i<n; i++)
#define re1(i, n) for (int i=1; i<=n; i++)
#define re2(i, l, r) for (int i=l; i<r; i++)
#define re3(i, l, r) for (int i=l; i<=r; i++)
#define rre(i, n) for (int i=n-1; i>=0; i--)
#define rre1(i, n) for (int i=n; i>0; i--)
#define rre2(i, r, l) for (int i=r-1; i>=l; i--)
#define rre3(i, r, l) for (int i=r; i>=l; i--)
const int MAXN = 51, MAXK = 201, MAXM = 3000, INF = ~0U >> 2;
struct edge {
    int a, b, w, pre, next;
} E[MAXM + MAXN], E0[MAXM + MAXN];
struct edge0 {
    int a, b, w;
    bool operator< (edge0 e0) const {return b < e0.b;}
} _E[MAXM];
int n, m, s, t, K, dist[MAXN], Q[MAXN + 1];
int Z, Z0, vst0[MAXN], _FL, len0, X00[MAXN], No, len[MAXK], X0[MAXK][MAXN], sum0[MAXK], reslen, res[MAXN];
bool vst[MAXN], res_ex = 0;
void init_d()
{
    re(i, n) E[i].pre = E[i].next = E0[i].pre = E0[i].next = i; m = n;
}
void add_edge(int a, int b, int w)
{
    E[m].a = a; E[m].b = b; E[m].w = w; E[m].pre = E[a].pre; E[m].next = a; E[a].pre = m; E[E[m].pre].next = m;
    E0[m].a = b; E0[m].b = a; E0[m].w = w; E0[m].pre = E0[b].pre; E0[m].next = b; E0[b].pre = m; E0[E0[m].pre].next = m++;
}
void init()
{
    int m0; scanf("%d%d%d%d%d", &n, &m0, &K, &s, &t); init_d(); s--; t--;
    re(i, m0) {scanf("%d%d%d", &_E[i].a, &_E[i].b, &_E[i].w); _E[i].a--; _E[i].b--;}
    sort(_E, _E + m0);
    re(i, m0) add_edge(_E[i].a, _E[i].b, _E[i].w);
}
void prepare()
{
    re(i, n) {vst[i] = 0; dist[i] = INF;} vst[t] = 1; dist[t] = 0; Q[0] = t;
    int x, y, d0, d1;
    for (int front=0, rear=0; !(!front && rear==n || front==rear+1); front==n ? front=0 : front++) {
        x = Q[front]; d0 = dist[x];
        for (int p=E0[x].next; p != x; p=E0[p].next) {
            y = E0[p].b; d1 = d0 + E0[p].w;
            if (d1 < dist[y]) {
                dist[y] = d1;
                if (!vst[y]) {vst[y] = 1; Q[rear==n ? rear=0 : ++rear] = y;}
            }
        }
        vst[x] = 0;
    }
}
void dfs(int x, int sum)
{
    if (x == t) {
        if (sum <= Z) {sum0[No] = sum; len[No] = len0; re(i, len0) X0[No][i] = X00[i]; No++; if (No == K) res_ex = 1;}
        else if (sum < Z0) Z0 = sum;
        return;
    } else {
        int h0 = sum + dist[x];
        if (h0 > Z) {if (h0 < Z0) Z0 = h0; return;}
        vst0[x] = ++_FL; Q[0] = x; int _x, _y;
        for (int front=0, rear=0; front<=rear; front++) {
            _x = Q[front];
            for (int p=E[_x].next; p != _x; p=E[p].next) {
                _y = E[p].b;
                if (!vst[_y] && vst0[_y] != _FL) {vst0[_y] = _FL; Q[++rear] = _y;}
            }
        }
        if (vst0[t] != _FL) return;
        for (int p=E[x].next; p != x; p=E[p].next) {
            _y = E[p].b;
            if (!vst[_y]) {vst[_y] = 1; X00[len0++] = _y; dfs(_y, sum + E[p].w); if (res_ex) return; else {len0--; vst[_y] = 0;}}
        }
    }
}
void solve()
{
    Z = dist[s]; int No0 = 0; _FL = 0;
    while (1) {
        Z0 = INF; No = 0; re(i, n) {vst[i] = 0; vst0[i] = 0;}
        vst[s] = 1; len0 = 1; X00[0] = s; dfs(s, 0);
        if (res_ex) {
            No0 = K - No0;
            re(i, K) if (sum0[i] == Z) {No0--; if (!No0) {reslen = len[i]; re(j, len[i]) res[j] = X0[i][j];}}
            break;
        } else if (Z0 == INF) break; else {No0 = No; Z = Z0;}
    }
}
void pri()
{
    if (res_ex) {
        printf("%d", res[0] + 1);
        re2(i, 1, reslen) printf("-%d", res[i] + 1);
        puts("");
    } else puts("No");
}
int main()
{
    init();
    prepare();
    solve();
    pri();
    return 0;
}

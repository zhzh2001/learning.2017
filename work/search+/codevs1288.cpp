#include<bits/stdc++.h>
using namespace std;
long long gcd(long long a,long long b)
{
	return !b?a:gcd(b,a%b);
}
const int N=100;
long long now[N],ans[N];
bool dfs(int k,int maxd,long long a,long long b,long long last)
{
	if(k==maxd)
		if(!(b%a)&&b/a>now[k-1])
		{
			now[k]=b/a;
			if(!ans[k]||now[k]<ans[k])
				memcpy(ans,now,sizeof(ans));
			return true;
		}
		else
			return false;
	bool ret=false;
	for(int i=max(last+1,b/a+1);a*i<=b*(maxd-k+1);i++)
	{
		now[k]=i;
		long long na=a*i-b,nb=b*i,g=gcd(na,nb);
		ret|=dfs(k+1,maxd,na/g,nb/g,i);
	}
	return ret;
}
int main()
{
	int a,b;
	cin>>a>>b;
	for(int i=1;;i++)
		if(dfs(1,i,a,b,0))
		{
			for(int j=1;j<=i;j++)
				cout<<ans[j]<<' ';
			cout<<endl;
			break;
		}
	return 0;
}
#include<iostream>
#include<fstream>
#include<algorithm>
#include<cstdio>
#include<cstring>
using namespace std;
const int dx[]={0,0,1,-1};
const int dy[]={1,-1,0,0};
const int N=10;
int n,s,mp[N][N],mark[N][N];
bool ans,vis[N];
inline int get(){
    int t=0;
    memset(vis,0,sizeof(vis));
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)
            if(!vis[mp[i][j]]&&mark[i][j]!=1)
                vis[mp[i][j]]=1,t++;
    return t;
}
void dfs(int x,int y,int val){
    mark[x][y]=1;//一表示是否在联通快内 
    for(int i=0;i<4;i++){
        int nx=x+dx[i],ny=y+dy[i];
        if(nx<1||nx>n||ny<1||ny>n||mark[nx][ny]==1) continue;
        mark[nx][ny]=2;//二表示正好在联通快的边界上面 
        if(mp[nx][ny]==val) dfs(nx,ny,val);
    }
}
inline int fill(int x){
    int t=0;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=n;j++)
            if(mark[i][j]==2&&mp[i][j]==x)
                t++,dfs(i,j,x);
    return t;
}
void search(int k){
    int v=get();
    if(!v) ans=1;
    if(k+v>s||ans) return;
    int tmp[N][N];
    for(int i=0;i<=5;i++){
        memcpy(tmp,mark,sizeof(mark));
        if(fill(i)) search(k+1);
        memcpy(mark,tmp,sizeof(mark));
    }
}
int main(){
    while(cin>>n&&n!=0){
        memset(mark,0,sizeof(mark));
        ans=0;
        for(int i=1;i<=n;i++)
            for(int j=1;j<=n;j++)
                cin>>mp[i][j];
        dfs(1,1,mp[1][1]);
        for(s=0;;s++){
            search(0);
            if(ans){
                cout<<s<<endl; break;
            }
        }
    }
    return 0;
}
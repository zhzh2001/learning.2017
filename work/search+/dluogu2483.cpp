#include<bits/stdc++.h>
using namespace std;
const double INF=1e100;
int n;
double e;
struct graph
{
	static const int N=5005,M=200005;
	int head[N],v[M],nxt[M],e;
	double w[M];
	graph():e(0){}
	inline void add_edge(int u,int v,double w)
	{
		this->v[++e]=v;
		this->w[e]=w;
		nxt[e]=head[u];
		head[u]=e;
	}
	
	struct node
	{
		int v;
		double w;
		node(int v,double w):v(v),w(w){}
		bool operator>(const node& rhs)const
		{
			return w>rhs.w;
		}
	};
	double d[N];
	bool vis[N];
	void dijkstra(int s)
	{
		for(int i=1;i<=n;i++)
			d[i]=i==s?0:INF;
		memset(vis,false,sizeof(vis));
		priority_queue<node,vector<node>,greater<node> > Q;
		Q.push(node(s,0));
		while(!Q.empty())
		{
			node k=Q.top();Q.pop();
			if(!vis[k.v])
			{
				vis[k.v]=true;
				for(int i=head[k.v];i;i=nxt[i])
					if(!vis[v[i]]&&d[k.v]+w[i]<d[v[i]])
					{
						d[v[i]]=d[k.v]+w[i];
						Q.push(node(v[i],d[v[i]]));
					}
			}
		}
	}
}G,RG;
struct heapNode
{
	int v;
	double g,f;
};
inline bool cmp(const heapNode& lhs,const heapNode& rhs)
{
	if(lhs.f!=rhs.f)
		return lhs.f>rhs.f;
	return lhs.g>rhs.g;
}
void kshort(int s,int t)
{
	RG.dijkstra(t);
	static heapNode heap[graph::N*400];
	int hp=0;
	heap[hp].v=s;heap[hp].g=0;heap[hp++].f=RG.d[s];
	push_heap(heap,heap+hp,cmp);
	int ans=0;
	while(hp>=0)
	{
		pop_heap(heap,heap+hp,cmp);
		heapNode k=heap[--hp];
		if(k.v==t)
			if(e>=k.g)
			{
				ans++;
				e-=k.g;
			}
			else
				break;
		for(int i=G.head[k.v];i;i=G.nxt[i])
		{
			heap[hp].v=G.v[i];heap[hp].g=k.g+G.w[i];heap[hp].f=heap[hp].g+RG.d[G.v[i]];hp++;
			push_heap(heap,heap+hp,cmp);
		}
	}
	cout<<ans<<endl;
}
int main()
{
	int m;
	cin>>n>>m>>e;
	while(m--)
	{
		int u,v;
		double w;
		cin>>u>>v>>w;
		G.add_edge(u,v,w);
		RG.add_edge(v,u,w);
	}
	kshort(1,n);
	return 0;
}
#include<cstdio>

#include<cstring>

#include<algorithm>


using namespace std;


typedef long long ll;


const int maxn=10010;

const int INF=0x7f7f7f7f;


int a,b,d;

ll res[maxn],rec[maxn];


ll gcd(ll a,ll b){

ll t;

while(b)

t=b,b=a%b,a=t;

return a;

}


inline bool better(ll a[]){

if(a[1]>=res[1]) return false;

else return true;

for(int i=1;i<=d;i++)

if(a[i]<res[i]) return true;

else if(a[i]>res[i]) return false;

return false;

}


bool dfs(int level,ll last,ll aa,ll bb){

if(level==1){

if(bb%aa) return false;

rec[1]=bb/aa;

if(better(rec))

for(int i=1;i<=d;i++) res[i]=rec[i];

return true;

}

bool ok=false;

last=max(last+1,(bb/aa)+!(bb%aa==0));

for(int i=last;;i++){

if(aa*i>=bb*level) break;

ll b1=bb*i,a1=aa*i-bb,Gcd;

Gcd=gcd(a1,b1);

a1/=Gcd,b1/=Gcd;rec[level]=i;

if(dfs(level-1,i,a1,b1)) ok=true;

}

return ok;

}


int main(){

scanf("%d%d",&a,&b);

memset(res,0x7f,sizeof(res));

for(d=1;!dfs(d,0,a,b);d++);

for(int i=d;i>=1;i--)

printf("%lld ",res[i]);

return 0;

}
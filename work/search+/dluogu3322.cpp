#include<bits/stdc++.h>
using namespace std;
const int N=12;
int n,a[(1<<N)+5],fact[N+3];
long long ans;
inline bool check(int fir,int len)
{
	for(int i=0;i<(1<<len)-1;i++)
		if(a[fir+i]+1!=a[fir+i+1])
			return false;
	return true;
}
inline void swap(int x,int y,int len)
{
	for(int i=0;i<(1<<len);i++)
		swap(a[x+i],a[y+i]);
}
void dfs(int k,int cc)
{
	if(k==n+1)
	{
		ans+=check(1,n)*fact[cc];
		return;
	}
	int cnt=0,s1=0,s2=0;
	for(int i=1;i<(1<<n);i+=(1<<k))
		if(!check(i,k))
		{
			cnt++;
			if(!s1)
				s1=i;
			else
				s2=i;
		}
	if(cnt>2)
		return;
	if(!cnt)
		dfs(k+1,cc);
	else
		if(cnt==1)
		{
			swap(s1,s1+(1<<k-1),k-1);
			if(check(s1,k))
				dfs(k+1,cc+1);
			swap(s1,s1+(1<<k-1),k-1);
		}
		else
		{
			swap(s1,s2,k-1);
			if(check(s1,k)&&check(s2,k))
				dfs(k+1,cc+1);
			swap(s1,s2,k-1);
			
			swap(s1,s2+(1<<k-1),k-1);
			if(check(s1,k)&&check(s2,k))
				dfs(k+1,cc+1);
			swap(s1,s2+(1<<k-1),k-1);
			
			swap(s1+(1<<k-1),s2,k-1);
			if(check(s1,k)&&check(s2,k))
				dfs(k+1,cc+1);
			swap(s1+(1<<k-1),s2,k-1);
			
			swap(s1+(1<<k-1),s2+(1<<k-1),k-1);
			if(check(s1,k)&&check(s2,k))
				dfs(k+1,cc+1);
			swap(s1+(1<<k-1),s2+(1<<k-1),k-1);
		}
}
int main()
{
	cin>>n;
	for(int i=1;i<=(1<<n);i++)
		cin>>a[i];
	fact[0]=1;
	for(int i=1;i<=n;i++)
		fact[i]=fact[i-1]*i;
	dfs(1,0);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=30;
int n,k,a[N];
long long s,fact[N],ans;
unordered_map<long long,int> M[N];
void dfs(int now,int r,int cnt,bool flag,long long sum)
{
	if(now==r+1)
	{
		if(flag)
			for(int i=0;i+cnt<=k;i++)
			{
				auto p=M[i].find(s-sum);
				if(p!=M[i].end())
					ans+=p->second;
			}
		else
			M[cnt][sum]++;
		return;
	}
	dfs(now+1,r,cnt,flag,sum);
	if(sum+a[now]<=s)
		dfs(now+1,r,cnt,flag,sum+a[now]);
	if(cnt<k&&a[now]<=18&&sum+fact[a[now]]<=s)
		dfs(now+1,r,cnt+1,flag,sum+fact[a[now]]);
}
int main()
{
	cin>>n>>k>>s;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	fact[0]=1;
	for(int i=1;i<=18;i++)
		fact[i]=fact[i-1]*i;
	dfs(1,n/2,0,false,0);
	dfs(n/2+1,n,0,true,0);
	cout<<ans<<endl;
	return 0;
}
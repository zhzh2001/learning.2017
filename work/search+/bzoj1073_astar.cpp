#include<bits/stdc++.h>
using namespace std;
const int N=55,INFM=0x3f,INF=0x3f3f3f3f;
int n,m,k,s,t,mat[N][N],rmat[N][N];
struct node
{
	int v,g,f;
	long long vis;
	vector<int> path;
	bool operator<(const node& rhs)const
	{
		if(g!=rhs.g)
			return g<rhs.g;
		return lexicographical_compare(path.begin(),path.end(),rhs.path.begin(),rhs.path.end());
	}
};
inline bool cmp(const node& lhs,const node& rhs)
{
	if(lhs.f!=rhs.f)
		return lhs.f>rhs.f;
	return lhs.g>rhs.g;
}
int main()
{
	cin>>n>>m>>k>>s>>t;
	memset(mat,INFM,sizeof(mat));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			rmat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		rmat[v][u]=mat[u][v]=w;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				rmat[i][j]=min(rmat[i][j],rmat[i][k]+rmat[k][j]);
	
	static node heap[100000];
	int hp=0;
	heap[hp].v=s;heap[hp].g=0;heap[hp].f=rmat[t][s];heap[hp].vis=1<<s;heap[hp++].path.push_back(s);
	push_heap(heap,heap+hp,cmp);
	vector<node> ans;
	while(hp>=0)
	{
		pop_heap(heap,heap+hp,cmp);
		node k=heap[--hp];
		if(k.v==t)
			if(ans.size()<(::k)||k.g==ans.back().g)
				ans.push_back(k);
			else
				break;
		for(int i=1;i<=n;i++)
			if(mat[k.v][i]<INF&&!(k.vis&(1<<i)))
			{
				vector<int> np(k.path);
				np.push_back(i);
				heap[hp].v=i;heap[hp].g=k.g+mat[k.v][i];heap[hp].f=heap[hp].g+rmat[t][i];heap[hp].vis=k.vis|(1<<i);heap[hp++].path=np;
				push_heap(heap,heap+hp,cmp);
			}
	}
	if(ans.size()<k)
		cout<<"No\n";
	else
	{
		sort(ans.begin(),ans.end());
		cout<<ans[k-1].path[0];
		for(int i=1;i<ans[k-1].path.size();i++)
			cout<<'-'<<ans[k-1].path[i];
		cout<<endl;
	}
	return 0;
}
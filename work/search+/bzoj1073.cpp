#include<bits/stdc++.h>
using namespace std;
const int N=55,K=205,INF=0x3f3f3f3f;
int n,k,s,t,mat[N][N],f[N][N],cnt,nxt,tt[N],tsp;
bool vis[N];
vector<int> now;
struct node
{
	int w;
	vector<int> path;
}ans[K];
bool dfs(int k,int dist,int maxl)
{
	if(dist+f[k][t]>maxl)
	{
		nxt=min(nxt,dist+f[k][t]);
		return false;
	}
	if(k==t)
	{
		ans[++cnt].w=dist;
		ans[cnt].path=now;
		return cnt==::k;
	}
	queue<int> Q;
	Q.push(k);
	tt[k]=++tsp;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=1;i<=n;i++)
			if(!vis[i]&&mat[k][i]<INF&&tt[i]!=tsp)
			{
				tt[i]=tsp;
				Q.push(i);
			}
	}
	if(tt[t]!=tsp)
		return false;
	vis[k]=true;
	for(int i=1;i<=n;i++)
		if(!vis[i]&&mat[k][i]<INF)
		{
			now.push_back(i);
			if(dfs(i,dist+mat[k][i],maxl))
				return true;
			else
				now.pop_back();
		}
	vis[k]=false;
	return false;
}
int main()
{
	int m;
	cin>>n>>m>>k>>s>>t;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?0:INF;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=w;
	}
	memcpy(f,mat,sizeof(f));
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				f[i][j]=min(f[i][j],f[i][k]+f[k][j]);
	
	int pred=0;
	bool flag=true;
	for(int i=f[s][t];flag;i=nxt,pred=cnt)
	{
		nxt=INF;
		cnt=0;
		if(dfs(s,0,i))
		{
			pred=k-pred;
			for(int j=1;j<=cnt;j++)
				if(ans[j].w==i&&!--pred)
				{
					cout<<s;
					for(int k=0;k<ans[j].path.size();k++)
						cout<<'-'<<ans[j].path[k];
					cout<<endl;
					flag=false;
					break;
				}
		}
		else
			if(nxt==INF)
			{
				cout<<"No\n";
				break;
			}
	}
	return 0;
}
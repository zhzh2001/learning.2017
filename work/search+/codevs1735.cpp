#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
const int N=10;
int m,k[N],p[N];
vector<int> a[2];
void dfs(int now,int r,bool flag,int sum)
{
	if(now==r+1)
	{
		a[flag].push_back(sum);
		return;
	}
	for(int i=1;i<=m;i++)
	{
		int t=1;
		for(int j=1;j<=p[now];j++)
			t*=i;
		dfs(now+1,r,flag,sum+k[now]*t);
	}
}
int main()
{
	int n;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>k[i]>>p[i];
	dfs(1,n/2,false,0);
	dfs(n/2+1,n,true,0);
	int ans=0;
	sort(a[0].begin(),a[0].end());
	sort(a[1].begin(),a[1].end());
	for(int i=0;i<a[0].size();i++)
		ans+=upper_bound(a[1].begin(),a[1].end(),-a[0][i])-lower_bound(a[1].begin(),a[1].end(),-a[0][i]);
	cout<<ans<<endl;
	return 0;
}
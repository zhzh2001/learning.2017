#include<bits/stdc++.h>
using namespace std;
const int N=55,TIMES=10000;
bool mat[N][N];
int p[N],into[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		p[i]=i;
	int u,v;
	while(cin>>u>>v)
		mat[u][v]=mat[v][u]=true;
	int ans=0;
	for(int i=1;i<=TIMES;i++)
	{
		random_shuffle(p+1,p+n+1);
		int cnt=0;
		for(int i=1;i<=n;i++)
		{
			bool flag=true;
			for(int j=1;j<=cnt;j++)
				if(!mat[into[j]][p[i]])
				{
					flag=false;
					break;
				}
			if(flag)
				into[++cnt]=p[i];
		}
		ans=max(ans,cnt);
	}
	cout<<ans<<endl;
	return 0;
}
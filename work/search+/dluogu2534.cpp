#include<bits/stdc++.h>
using namespace std;
const int N=55;
int n,a[N],b[N];
bool dfs(int k,int maxd,int h)
{
	if(k<=maxd&&!h&&a[1]<a[2])
		return true;
	if(k+h>maxd)
		return false;
	for(int i=2;i<=n;i++)
		if((i==n&&a[1]>a[n])||abs(a[i]-a[i+1])>1)
		{
			reverse(a+1,a+i+1);
			if(abs(a[i]-a[i+1])==1)
			{
				if(dfs(k+1,maxd,h-1))
					return true;
			}
			else
				if(dfs(k+1,maxd,h))
					return true;
			reverse(a+1,a+i+1);
		}
	return false;
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	memcpy(b,a,sizeof(b));
	sort(b+1,b+n+1);
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		a[i]=lower_bound(b+1,b+n+1,a[i])-b;
		if(i>1)
			cnt+=abs(a[i]-a[i-1])>1;
	}
	a[n+1]=0x3f3f3f3f;
	for(int i=0;;i++)
		if(dfs(0,i,cnt))
		{
			cout<<i<<endl;
			break;
		}
	return 0;
}
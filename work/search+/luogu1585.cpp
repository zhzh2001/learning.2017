#include<bits/stdc++.h>
using namespace std;
const int N=55,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,m,k1,k2,ans;
struct point
{
	int x,y;
}p[N];
bool vis[N][N];
void dfs(int k,int x,int y,int now)
{
	bool f[4];
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		f[i]=nx&&nx<=n&&ny&&ny<=m&&!vis[nx][ny];
	}
	if((f[0]&&f[1]&&!f[2]&&!f[3])||(!f[0]&&!f[1]&&f[2]&&f[3])||now>=ans)
		return;
	if(k==n*m)
	{
		ans=min(ans,max(now,k1*abs(x-p[k-n*m/2].x)+k2*abs(y-p[k-n*m/2].y)));
		return;
	}
	vis[x][y]=true;
	p[k].x=x;p[k].y=y;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(f[i])
			dfs(k+1,nx,ny,k>n*m/2?max(now,k1*abs(x-p[k-n*m/2].x)+k2*abs(y-p[k-n*m/2].y)):now);
	}
	vis[x][y]=false;
}
int main()
{
	cin>>n>>m>>k1>>k2;
	ans=0x3f3f3f3f;
	dfs(1,1,m,0);
	cout<<ans<<endl;
	return 0;
}
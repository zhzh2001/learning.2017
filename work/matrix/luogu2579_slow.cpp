#include<bits/stdc++.h>
using namespace std;
const int N=50,NF=25,MOD=10000;
bool mat[N][N],f[N][12];
int n;
struct matrix
{
	int mat[N*12][N*12];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n*12;i++)
			for(int k=0;k<n*12;k++)
				for(int j=0;j<n*12;j++)
					ans.mat[i][j]=(ans.mat[i][j]+mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix qpow(matrix a,int b)
{
	matrix ans;
	for(int i=0;i<n*12;i++)
		ans.mat[i][i]=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int m,s,t,k;
	cin>>n>>m>>s>>t>>k;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=mat[v][u]=true;
	}
	int nf;
	cin>>nf;
	for(int i=1;i<=nf;i++)
	{
		int t;
		cin>>t;
		for(int j=0;j<t;j++)
		{
			int x;
			cin>>x;
			for(int k=j;k<12;k+=t)
				f[x][k]=true;
		}
	}
	matrix ans;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			if(mat[i][j])
				for(int k=0;k<12;k++)
					if(!f[j][k])
						ans.mat[i*12+k][j*12+(k+1)%12]=1;
	ans=qpow(ans,k);
	cout<<ans.mat[s*12][t*12+k%12]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int MOD=1000000007;
struct matrix
{
	static const int n=2;
	int mat[n][n];
	matrix(const int x=0)
	{
		memset(mat,0,sizeof(mat));
		switch(x)
		{
			case 0:break;
			case 1:
				for(int i=0;i<n;i++)
					mat[i][i]=1;
				break;
			default:throw x;
		}
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int k=0;k<n;k++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j]%MOD)%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix qpow(matrix a,long long b)
{
	matrix ans=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	long long n;
	cin>>n;
	if(n==1)
	{
		cout<<1<<endl;
		return 0;
	}
	matrix trans;
	trans.mat[0][0]=trans.mat[0][1]=trans.mat[1][0]=1;
	trans=qpow(trans,n-2);
	matrix init;
	init.mat[0][0]=init.mat[1][0]=1;
	cout<<(trans*init).mat[0][0]<<endl;
	return 0;
}
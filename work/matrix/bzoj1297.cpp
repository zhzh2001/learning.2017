#include<bits/stdc++.h>
using namespace std;
const int N=95,MOD=2009;
int n;
struct matrix
{
	int mat[N][N];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix qpow(matrix a,int b)
{
	matrix ans;
	for(int i=1;i<=n;i++)
		ans.mat[i][i]=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int k;
	cin>>n>>k;
	matrix ans;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<9;j++)
			ans.mat[9*(i-1)+j][9*(i-1)+j+1]=1;
		for(int j=1;j<=n;j++)
		{
			char c;
			cin>>c;
			if(c>'0')
				ans.mat[9*(i-1)+c-'0'][9*(j-1)+1]=1;
		}
	}
	n*=9;
	cout<<qpow(ans,k).mat[1][n-8]<<endl;
	return 0;
}
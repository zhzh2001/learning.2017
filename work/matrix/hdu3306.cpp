#include<iostream>
#include<cstring>
using namespace std;
const int MOD=10007;
struct matrix
{
	static const int n=6;
	int mat[n][n];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int k=0;k<n;k++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix qpow(matrix a,int b)
{
	matrix ans;
	for(int i=0;i<matrix::n;i++)
		ans.mat[i][i]=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n,x,y;
	while(cin>>n>>x>>y)
	{
		matrix trans;
		trans.mat[0][0]=x%MOD;trans.mat[0][1]=y%MOD;
		trans.mat[1][0]=1;
		trans.mat[2][2]=(long long)x*x%MOD;trans.mat[2][3]=(long long)y*y%MOD;trans.mat[2][4]=2ll*x*y%MOD;
		trans.mat[3][2]=1;
		trans.mat[4][2]=x%MOD;trans.mat[4][4]=y%MOD;
		trans.mat[5][2]=1;trans.mat[5][5]=1;
		trans=qpow(trans,n);
		matrix init;
		for(int i=0;i<matrix::n;i++)
			init.mat[i][0]=1;
		cout<<(trans*init).mat[5][0]<<endl;
	}
	return 0;
}
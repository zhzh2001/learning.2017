#include<cstdio>
#include<cstring>
using namespace std;
template<typename T>
T qpow(T a,long long b,long long p)
{
	T ans=1;
	for(;b;b/=2)
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	return ans;
}
int p;
struct matrix
{
	static const int n=3;
	int mat[n][n];
	matrix(int x=0)
	{
		memset(mat,0,sizeof(mat));
		switch(x)
		{
			case 0:break;
			case 1:
				for(int i=0;i<n;i++)
					mat[i][i]=1;
				break;
			default:throw x;
		}
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int k=0;k<n;k++)
			for(int i=0;i<n;i++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j])%p;
		return ans;
	}
	matrix operator%(const int)const
	{
		return *this;
	}
};
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		long long n;
		int a,b,c;
		scanf("%lld%d%d%d%d",&n,&a,&b,&c,&p);
		if(n==1)
		{
			puts("1");
			continue;
		}
		if(a%p==0)
		{
			puts("0");
			continue;
		}
		matrix init,base;
		init.mat[0][0]=init.mat[2][0]=b;
		base.mat[0][0]=c;
		base.mat[0][1]=base.mat[0][2]=base.mat[1][0]=base.mat[2][2]=1;
		p--;
		base=qpow<matrix>(base,n-2,0);
		printf("%lld\n",qpow<long long>(a,(base*init).mat[0][0],p+1));
	}
	return 0;
}
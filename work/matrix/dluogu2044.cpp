#include<bits/stdc++.h>
using namespace std;
long long m;
long long qmul(long long a,long long b)
{
	long long ans=0;
	do
	{
		if(b&1)
			ans=(ans+a)%m;
		a=(a+a)%m;
	}
	while(b/=2);
	return ans;
}
struct matrix
{
	static const int n=2;
	long long mat[n][n];
	matrix(const int x=0)
	{
		memset(mat,0,sizeof(mat));
		switch(x)
		{
			case 0:break;
			case 1:
				for(int i=0;i<n;i++)
					mat[i][i]=1;
				break;
			default:throw x;
		}
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int k=0;k<n;k++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+qmul(mat[i][k],rhs.mat[k][j]))%m;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix qpow(matrix a,long long b)
{
	matrix ans=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	long long a,c,x0,n,g;
	cin>>m>>a>>c>>x0>>n>>g;
	matrix trans;
	trans.mat[0][0]=a;
	trans.mat[0][1]=c;
	trans.mat[1][1]=1;
	trans=qpow(trans,n);
	matrix init;
	init.mat[0][0]=x0;
	init.mat[1][0]=1;
	cout<<(trans*init).mat[0][0]%g<<endl;
	return 0;
}
#include<stdio.h>  
#include<string.h>  
#include<iostream>  
#define ll long long  
#define rush() int t;scanf("%d",&t);while(t--)  
using namespace std;   
struct mat { ll a[3][3]; };  
ll mod,mod1;  
mat operator * (mat a, mat b)  
{  
    mat ans;  
    memset(ans.a, 0, sizeof(ans.a));  
    for (int i = 0; i < 3; i++)  
        for (int j = 0; j < 3; j++)  
            for (int k = 0; k < 3; k++)  
            {  
                ans.a[i][j] += (a.a[i][k] * b.a[k][j]) % (mod-1);  
                ans.a[i][j] %= mod-1;  
            }  
    return ans;  
}  
mat qpow(mat a, ll n)  
{  
    mat ans;  
    memset(ans.a, 0, sizeof(ans.a));  
    for (int i = 0; i < 3; i++)ans.a[i][i] = 1;  
    while (n)  
    {  
        if (n & 1)ans = ans*a;  
        a = a*a;  
        n >>= 1;  
    }  
    return ans;  
}  
ll qpow(ll a, ll b)  
{  
    ll ans = 1;  
    while (b)  
    {  
        if (b & 1)ans = (ans*a)%mod;  
        a =(a*a)%mod;  
        b >>= 1;  
    }  
    return ans;  
}/* 
ll phi(ll n) 
{ 
    ll temp; 
    temp = n; 
    for (int i = 2; i*i <= n; i++) 
    { 
        if (n%i == 0) 
        { 
            while (n%i == 0) n = n / i; 
            temp = temp / i*(i - 1); 
        } 
        if (n<i + 1) 
            break; 
    } 
    if (n>1) 
        temp = temp / n*(n - 1); 
    return temp; 
}*/  
int main()  
{  
    rush()  
    {  
        mat tem;  
        ll a, b, n,c;  
        cin >> n >> a >> b >> c>>mod;  
        ll k = qpow(a, b);  
        if (n == 1) {cout<<'1'<<endl; continue; }  
        else if (n == 2) { cout << k << endl; continue; }  
        else  
        {  
            tem.a[0][0] = c;  
            tem.a[0][1] = 1;  
            tem.a[0][2] = 1;  
            tem.a[1][0] = 1;  
            tem.a[1][1] = 0;  
            tem.a[1][2] = 0;  
            tem.a[2][0] = 0;  
            tem.a[2][1] = 0;  
            tem.a[2][2] = 1;  
            tem = qpow(tem, n - 2);//������phi(n-2)  
            ll ans = ((tem.a[0][0]%(mod-1) + tem.a[0][2]%(mod-1)) % (mod-1) + mod-1) % (mod-1);  
            ans = qpow(k, ans);  
            cout << ans << endl;  
        }  
    }  
}  


#include<bits/stdc++.h>
using namespace std;
const int K=20;
int n,p,a[K];
struct matrix
{
	static const int N=16;
	int mat[N][N];
	matrix(const int x=0)
	{
		memset(mat,0,sizeof(mat));
		switch(x)
		{
			case 0:break;
			case 1:
				for(int i=0;i<n;i++)
					mat[i][i]=1;
				break;
			default:throw x;
		}
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int k=0;k<n;k++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j]%p)%p;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix qpow(matrix a,long long b)
{
	matrix ans=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int k;
	cin>>k;
	for(int i=1;i<=k;i++)
		cin>>a[i];
	matrix trans;
	for(int i=0;i<k;i++)
		cin>>trans.mat[0][i];
	for(int i=1;i<k;i++)
		trans.mat[i][i-1]=1;
	trans.mat[k][0]=trans.mat[k][k]=1;
	long long m,n;
	cin>>m>>n>>p;
	matrix init;
	int sum=0;
	for(int i=1;i<k;i++)
		sum=(sum+a[i])%p;
	for(int i=0;i<k;i++)
		init.mat[i][0]=a[k-i];
	init.mat[k][0]=sum;
	::n=k+1;
	int l,r;
	if(n>k)
		r=(qpow(trans,n-k+1)*init).mat[k][0];
	else
		r=a[n];
	if(m-1>k)
		l=(qpow(trans,m-k)*init).mat[k][0];
	else
		l=a[m-1];
	cout<<(r-l+p)%p<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50,NF=25,MOD=10000;
bool mat[N][N],f[N][12];
int n;
struct matrix
{
	int mat[N][N];
	matrix(const int x=0)
	{
		memset(mat,0,sizeof(mat));
		switch(x)
		{
			case 0:break;
			case 1:
				for(int i=0;i<n;i++)
					mat[i][i]=1;
				break;
			default:throw x;
		}
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int k=0;k<n;k++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
}tmat[12];
matrix qpow(matrix a,int b)
{
	matrix ans=1;
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int m,s,t,k;
	cin>>n>>m>>s>>t>>k;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=mat[v][u]=true;
	}
	int nf;
	cin>>nf;
	for(int i=1;i<=nf;i++)
	{
		int t;
		cin>>t;
		for(int j=0;j<t;j++)
		{
			int x;
			cin>>x;
			for(int k=j;k<12;k+=t)
				f[x][k]=true;
		}
	}
	matrix ans=1;
	for(int t=0;t<12;t++)
	{
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				tmat[t].mat[i][j]=mat[i][j]&&!f[j][(t+1)%12];
		ans*=tmat[t];
	}
	ans=qpow(ans,k/12);
	for(int i=0;i<k%12;i++)
		ans*=tmat[i];
	cout<<ans.mat[s][t]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
long long k;
long long qmul(long long a,long long b)
{
	long long ans=0;
	do
	{
		if(b&1)
			ans=(ans+a)%k;
		a=(a+a)%k;
	}
	while(b/=2);
	return ans;
}
struct matrix
{
	static const int n=7;
	long long mat[n][n];
	matrix(const int x=0)
	{
		memset(mat,0,sizeof(mat));
		switch(x)
		{
			case 0:break;
			case 1:
				for(int i=0;i<n;i++)
					mat[i][i]=1;
				break;
			default:throw x;
		}
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int k=0;k<n;k++)
				for(int j=0;j<n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+qmul(mat[i][k],rhs.mat[k][j]))%(::k);
		return ans;
	}
};
matrix qmul(const matrix& lhs,const matrix& rhs)
{
	return lhs*rhs;
}
template<typename T>
T qpow(T a,long long b)
{
	T ans=1;
	do
	{
		if(b&1)
			ans=qmul(ans,a);
		a=qmul(a,a);
	}
	while(b/=2);
	return ans;
}
int main()
{
	long long n;
	int p,q,r,t,u,v,w,x,y,z;
	cin>>n>>k>>p>>q>>r>>t>>u>>v>>w>>x>>y>>z;
	matrix trans;
	trans.mat[0][0]=p;trans.mat[0][1]=q;trans.mat[0][2]=1;trans.mat[0][4]=1;trans.mat[0][6]=1;
	trans.mat[1][0]=1;
	trans.mat[2][0]=1;trans.mat[2][2]=u;trans.mat[2][3]=v;trans.mat[2][4]=1;trans.mat[2][6]=qpow<long long>(w,k);
	trans.mat[3][2]=1;
	trans.mat[4][0]=1;trans.mat[4][2]=1;trans.mat[4][4]=x;trans.mat[4][5]=y;trans.mat[4][6]=qpow<long long>(z,k)+2;
	trans.mat[5][4]=1;
	trans.mat[6][6]=1;
	trans=qpow<matrix>(trans,n-2);
	matrix init;
	init.mat[0][0]=init.mat[2][0]=init.mat[4][0]=3;
	init.mat[1][0]=init.mat[3][0]=init.mat[5][0]=init.mat[6][0]=1;
	matrix ans=trans*init;
	cout<<"nodgd "<<ans.mat[0][0]<<endl<<"Ciocio "<<ans.mat[2][0]<<endl<<"Nicole "<<ans.mat[4][0]<<endl;
	return 0;
}
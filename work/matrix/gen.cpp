#include<bits/stdc++.h>
using namespace std;
const int t=1e4,p=999999937;
int main()
{
	default_random_engine gen(time(NULL));
	cout<<t<<endl;
	for(int i=1;i<=t;i++)
	{
		uniform_int_distribution<long long> dn(1,1e18);
		uniform_int_distribution<> d(1,1e9);
		cout<<dn(gen)<<' '<<d(gen)<<' '<<d(gen)<<' '<<d(gen)<<' '<<p<<endl;
	}
	return 0;
}
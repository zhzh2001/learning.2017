#include <bits/stdc++.h>
using namespace std;
const int N = 300005, LOGN = 20;
vector<int> mat[N];
int a[N], s[N], tag[N], f[N][LOGN], dep[N];
void dfs(int k)
{
	for (auto v : mat[k])
		if (!f[v][0])
		{
			f[v][0] = k;
			dep[v] = dep[k] + 1;
			dfs(v);
		}
}
int lca(int u, int v)
{
	if (dep[u] < dep[v])
		swap(u, v);
	int delta = dep[u] - dep[v];
	for (int i = 0; delta; i++, delta /= 2)
		if (delta & 1)
			u = f[u][i];
	if (u == v)
		return u;
	for (int i = LOGN - 1; i >= 0; i--)
		if (f[u][i] != f[v][i])
		{
			u = f[u][i];
			v = f[v][i];
		}
	return f[u][0];
}
void dfs2(int k)
{
	for (auto v : mat[k])
		if (dep[v] == dep[k] + 1)
		{
			dfs2(v);
			s[k] += s[v];
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	f[1][0] = 1;
	dfs(1);
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i <= n; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
	for (int i = 1; i < n; i++)
	{
		int t = lca(a[i], a[i + 1]);
		s[a[i]]++;
		s[a[i + 1]]++;
		s[t] -= 2;
		tag[t]++;
	}
	dfs2(1);
	for (int i = 1; i <= n; i++)
	{
		s[i] += tag[i];
		if (i != a[1])
			s[i]--;
		cout << s[i] << endl;
	}
	return 0;
}
#include <bits/stdc++.h>
using namespace std;
const int N = 1000005;
int a[N], S[N];
long long s[N];
int main()
{
    int n, m;
    scanf("%d%d", &n, &m);
    for (int i = 1; i <= n; i++)
        scanf("%d", a + i);
    while (m--)
    {
        int k;
        scanf("%d", &k);
        for (int i = 1; i <= n; i++)
            s[i] = s[i - 1] + a[i] - k;
        int sp = 0;
        S[++sp] = 0;
        for (int i = 1; i <= n; i++)
            if (s[i] < s[S[sp]])
                S[++sp] = i;
        int ans = 0;
        for (int i = n; i; i--)
            while (sp && s[S[sp]] <= s[i])
            {
                ans = max(ans, i - S[sp]);
                sp--;
            }
        printf("%d ", ans);
    }
    return 0;
}
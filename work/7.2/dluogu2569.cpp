#include <bits/stdc++.h>
using namespace std;
const int T = 2005, P = 2005;
int f[T][P], ap[T], bp[T], as[T], bs[T], Q[P];
int main()
{
	int t, maxp, w;
	cin >> t >> maxp >> w;
	for (int i = 1; i <= t; i++)
		cin >> ap[i] >> bp[i] >> as[i] >> bs[i];
	memset(f, 0x81, sizeof(f));
	f[0][0] = 0;
	for (int i = 1; i <= t; i++)
	{
		for (int j = 0; j <= as[i]; j++)
			f[i][j] = -j * ap[i];
		for (int j = 0; j <= maxp; j++)
			f[i][j] = max(f[i][j], f[i - 1][j]);
		if (i - w - 1 > 0)
		{
			int l = 1, r = 0;
			for (int j = 0; j <= maxp; j++)
			{
				while (l <= r && Q[l] < j - as[i])
					l++;
				while (l <= r && f[i - w - 1][j] + ap[i] * j >= f[i - w - 1][Q[r]] + ap[i] * Q[r])
					r--;
				Q[++r] = j;
				f[i][j] = max(f[i][j], f[i - w - 1][Q[l]] - ap[i] * (j - Q[l]));
			}
			l = 1;
			r = 0;
			for (int j = maxp; j >= 0; j--)
			{
				while (l <= r && Q[l] > j + bs[i])
					l++;
				while (l <= r && f[i - w - 1][j] + bp[i] * j >= f[i - w - 1][Q[r]] + bp[i] * Q[r])
					r--;
				Q[++r] = j;
				f[i][j] = max(f[i][j], f[i - w - 1][Q[l]] + bp[i] * (Q[l] - j));
			}
		}
	}
	cout << f[t][0] << endl;
	return 0;
}
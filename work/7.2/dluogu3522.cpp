#include <bits/stdc++.h>
using namespace std;
const int N = 1000005;
pair<int, int> a[N];
int Q[N];
int main()
{
	int n;
	scanf("%d", &n);
	int l = 1, r = 0, ans = 0;
	for (int i = 1; i <= n; i++)
	{
		scanf("%d%d", &a[i].first, &a[i].second);
		while (l <= r && a[i].first >= a[Q[r]].first)
			r--;
		Q[++r] = i;
		while (l <= r && a[i].second < a[Q[l]].first)
			l++;
		ans = max(ans, Q[r] - Q[l - 1]);
	}
	printf("%d\n", ans);
	return 0;
}
#include <bits/stdc++.h>
using namespace std;
const int N = 1000005;
int a[N], s[N];
struct quest
{
	int cnt, l, r;
} q[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= m; i++)
		cin >> q[i].cnt >> q[i].l >> q[i].r;
	int l = 0, r = n, ans = 0;
	while (l <= r)
	{
		int mid = (l + r) / 2;
		fill(s + 1, s + n + 1, 0);
		for (int i = 1; i <= mid; i++)
		{
			s[q[i].l] += q[i].cnt;
			s[q[i].r + 1] -= q[i].cnt;
		}
		bool flag = true;
		for (int i = 1; i <= n; i++)
			if ((s[i] += s[i - 1]) > a[i])
			{
				flag = false;
				break;
			}
		if (flag)
		{
			l = mid + 1;
			ans = mid;
		}
		else
			r = mid - 1;
	}
	if (ans == n)
		cout << 0 << endl;
	else
		cout << -1 << endl
			 << ans + 1 << endl;
	return 0;
}
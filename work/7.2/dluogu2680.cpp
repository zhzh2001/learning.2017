#include <bits/stdc++.h>
using namespace std;
const int N = 300005, LOGN = 20;
int head[N], v[N * 2], w[N * 2], nxt[N * 2], e;
inline void add_edge(int u, int v, int w)
{
	::v[++e] = v;
	::w[e] = w;
	nxt[e] = head[u];
	head[u] = e;
}
int n, m, f[N][LOGN], dep[N], dist[N], s[N], W[N];
struct quest
{
	int u, v, anc, len;
	bool operator<(const quest &rhs) const
	{
		return len > rhs.len;
	}
} q[N];
void dfs(int k)
{
	for (int i = head[k]; i; i = nxt[i])
		if (!f[v[i]][0])
		{
			f[v[i]][0] = k;
			dep[v[i]] = dep[k] + 1;
			dist[v[i]] = dist[k] + w[i];
			W[v[i]] = w[i];
			dfs(v[i]);
		}
}
int lca(int u, int v)
{
	if (dep[u] < dep[v])
		swap(u, v);
	int delta = dep[u] - dep[v];
	for (int i = 0; delta; i++, delta /= 2)
		if (delta & 1)
			u = f[u][i];
	if (u == v)
		return u;
	for (int i = LOGN - 1; i >= 0; i--)
		if (f[u][i] != f[v][i])
		{
			u = f[u][i];
			v = f[v][i];
		}
	return f[u][0];
}
void dfs2(int k)
{
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != f[k][0])
		{
			dfs2(v[i]);
			s[k] += s[v[i]];
		}
}
bool check(int mid)
{
	fill(s + 1, s + n + 1, 0);
	int cnt = 0;
	for (int i = 1; i <= m && q[i].len > mid; i++)
	{
		cnt++;
		s[q[i].u]++;
		s[q[i].v]++;
		s[q[i].anc] -= 2;
	}
	dfs2(1);
	for (int i = 1; i <= n; i++)
		if (s[i] == cnt && W[i] >= q[1].len - mid)
			return true;
	return false;
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	int maxw = 0;
	for (int i = 1; i < n; i++)
	{
		int u, v, w;
		cin >> u >> v >> w;
		add_edge(u, v, w);
		add_edge(v, u, w);
		maxw = max(maxw, w);
	}
	f[1][0] = 1;
	dfs(1);
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i <= n; i++)
			f[i][j] = f[f[i][j - 1]][j - 1];
	for (int i = 1; i <= m; i++)
	{
		cin >> q[i].u >> q[i].v;
		q[i].anc = lca(q[i].u, q[i].v);
		q[i].len = dist[q[i].u] + dist[q[i].v] - 2 * dist[q[i].anc];
	}
	sort(q + 1, q + m + 1);
	int l = max(q[1].len - maxw, 0), r = q[1].len;
	while (l < r)
	{
		int mid = (l + r) / 2;
		if (check(mid))
			r = mid;
		else
			l = mid + 1;
	}
	cout << r << endl;
	return 0;
}
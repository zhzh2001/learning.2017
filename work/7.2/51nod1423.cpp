#include <bits/stdc++.h>
using namespace std;
const int N = 100005;
int a[N];
int main()
{
	int n;
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%d", a + i);
	int ans = 0;
	for (int t = 0; t < 2; t++)
	{
		stack<int> S;
		for (int i = 1; i <= n; i++)
		{
			while (!S.empty() && S.top() < a[i])
			{
				ans = max(ans, a[i] ^ S.top());
				S.pop();
			}
			S.push(a[i]);
		}
		reverse(a + 1, a + n + 1);
	}
	printf("%d\n", ans);
	return 0;
}
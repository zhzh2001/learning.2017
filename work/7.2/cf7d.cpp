#include <bits/stdc++.h>
using namespace std;
const int N = 5000005, B = 233;
typedef unsigned long long hash_t;
char s[N];
int deg[N];
hash_t h[N], rh[N];
int main()
{
	scanf("%s", s + 1);
	int n = strlen(s + 1);
	for (int i = 1; i <= n; i++)
		h[i] = h[i - 1] * B + s[i];
	for (int i = n; i; i--)
		rh[i] = rh[i + 1] * B + s[i];
	deg[1] = 1;
	int ans = 1;
	hash_t power = B;
	for (int i = 2; i <= n; i++)
	{
		power *= B;
		if (h[i] == rh[1] - rh[i + 1] * power)
			deg[i] = deg[i / 2] + 1;
		ans += deg[i];
	}
	printf("%d\n", ans);
	return 0;
}
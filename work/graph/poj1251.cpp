#include<iostream>
#include<algorithm>
#include<string>
using namespace std;
struct edge
{
	int u,v,w;
	bool operator<(const edge& b)const
	{
		return w<b.w;
	}
};
edge e[100];
int f[30];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n;
	while(cin>>n&&n)
	{
		int m=0;
		for(int i=1;i<n;i++)
		{
			string s;
			int t;
			cin>>s>>t;
			while(t--)
			{
				string v;
				int w;
				cin>>v>>e[++m].w;
				e[m].u=s[0]-'A'+1;e[m].v=v[0]-'A'+1;
			}
		}
		sort(e+1,e+m+1);
		for(int i=1;i<=n;i++)
			f[i]=i;
		int cnt=0,ans=0;
		for(int i=1;cnt<n-1;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v);
			if(ru!=rv)
			{
				cnt++;
				ans+=e[i].w;
				f[ru]=rv;
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}
#include<iostream>
#include<algorithm>
#include<cstring>
using namespace std;
const int INF=0x3f3f3f3f;
int mat[205][205],d[205];
bool vis[205];
int main()
{
	int n,m;
	while(cin>>n>>m)
	{
		memset(mat,0x3f,sizeof(mat));
		while(m--)
		{
			int u,v,w;
			cin>>u>>v>>w;
			mat[u][v]=min(mat[u][v],w);
			mat[v][u]=min(mat[v][u],w);
		}
		int s,t;
		cin>>s>>t;
		memset(vis,false,sizeof(vis));
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		for(int i=1;i<=n;i++)
		{
			int j=-1;
			for(int k=0;k<n;k++)
				if(!vis[k]&&(j==-1||d[k]<d[j]))
					j=k;
			vis[j]=true;
			for(int k=0;k<n;k++)
				if(!vis[k])
					d[k]=min(d[k],d[j]+mat[j][k]);
		}
		if(d[t]<INF)
			cout<<d[t]<<endl;
		else
			cout<<-1<<endl;
	}
	return 0;
}
#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int INF=0x3f3f3f3f;
int mat[1005][1005],d[1005];
bool vis[1005];
int main()
{
	int n,m,s;
	while(scanf("%d%d%d",&n,&m,&s)==3)
	{
		memset(mat,0x3f,sizeof(mat));
		while(m--)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			mat[v][u]=min(mat[v][u],w);
		}
		memset(vis,false,sizeof(vis));
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],d[j]+mat[j][k]);
		}
		int t;
		scanf("%d",&t);
		int ans=INF;
		while(t--)
		{
			int x;
			scanf("%d",&x);
			ans=min(ans,d[x]);
		}
		if(ans<INF)
			printf("%d\n",ans);
		else
			printf("-1\n");
	}
	return 0;
}
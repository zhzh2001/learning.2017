#include<iostream>
#include<algorithm>
using namespace std;
const int INF=0x3f3f3f3f;
int mat[55][55][55];
int main()
{
	int n,m,k;
	cin>>n>>m>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			for(int t=0;t<=k;t++)
				mat[i][j][t]=(i==j?0:INF);
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v][0]=mat[v][u][0]=w;
		mat[u][v][1]=mat[v][u][1]=w/2;
	}
	for(int p=1;p<=n;p++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int t=0;t<=k;t++)
					for(int tt=0;tt<=t;tt++)
						mat[i][j][t]=min(mat[i][j][t],mat[i][p][tt]+mat[p][j][t-tt]);
	int ans=INF;
	for(int i=0;i<=k;i++)
		ans=min(ans,mat[1][n][i]);
	cout<<ans<<endl;
	return 0;
}
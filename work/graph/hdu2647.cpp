#include<iostream>
#include<cstring>
#include<vector>
#include<queue>
using namespace std;
int into[10005];
vector<int> mat[10005];
struct node
{
	int u,p;
	node(int u,int p):u(u),p(p){};
};
queue<node> q;
bool vis[10005];
int main()
{
	int n,m;
	while(cin>>n>>m)
	{
		memset(into,0,sizeof(into));
		for(int i=1;i<=n;i++)
			mat[i].clear();
		while(m--)
		{
			int u,v;
			cin>>u>>v;
			into[u]++;
			mat[v].push_back(u);
		}
		memset(vis,false,sizeof(vis));
		int cnt=0;
		for(int i=1;i<=n;i++)
			if(!into[i])
			{
				vis[i]=true;
				q.push(node(i,888));
				cnt++;
			}
		int ans=0;
		while(!q.empty())
		{
			node k=q.front();q.pop();
			ans+=k.p;
			for(int i=0;i<mat[k.u].size();i++)
				if(!vis[mat[k.u][i]])
				{
					into[mat[k.u][i]]--;
					if(!into[mat[k.u][i]])
					{
						vis[mat[k.u][i]]=true;
						q.push(node(mat[k.u][i],k.p+1));
						cnt++;
					}
				}
		}
		if(cnt<n)
			cout<<-1<<endl;
		else
			cout<<ans<<endl;
	}
	return 0;
}
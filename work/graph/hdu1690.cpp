#include<iostream>
#include<cstdlib>
using namespace std;
const long long INF=0x3f3f3f3f3f3f3f3f;
int a[105];
long long mat[105][105];
int main()
{
	int t;
	cin>>t;
	for(int i=1;i<=t;i++)
	{
		cout<<"Case "<<i<<":\n";
		int l1,l2,l3,l4,c1,c2,c3,c4,n,m;
		cin>>l1>>l2>>l3>>l4>>c1>>c2>>c3>>c4>>n>>m;
		for(int i=1;i<=n;i++)
			cin>>a[i];
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
			{
				int d=abs(a[i]-a[j]);
				if(d<=l1)
					mat[i][j]=c1;
				else
					if(d<=l2)
						mat[i][j]=c2;
					else
						if(d<=l3)
							mat[i][j]=c3;
						else
							if(d<=l4)
								mat[i][j]=c4;
							else
								mat[i][j]=INF;
			}
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					mat[i][j]=min(mat[i][j],mat[i][k]+mat[k][j]);
		while(m--)
		{
			int u,v;
			cin>>u>>v;
			if(mat[u][v]<INF)
				cout<<"The minimum cost between station "<<u<<" and station "<<v<<" is "<<mat[u][v]<<".\n";
			else
				cout<<"Station "<<u<<" and station "<<v<<" are not attainable.\n";
		}
	}
	return 0;
}
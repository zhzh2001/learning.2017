#include<cstdio>
#include<algorithm>
#include<cstring>
using namespace std;
int n;
double mat[1005][1005],d[1005][1005];
bool calc[1005],vis[1005];
void dijkstra(int s)
{
	memset(vis,false,sizeof(vis));
	vis[s]=true;
	d[s][0]=0;
	for(int i=1;i<=n;i++)
		d[s][i]=mat[s][i];
	for(int i=1;i<n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[s][k]>d[s][j])
				j=k;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[s][k]=max(d[s][k],d[s][j]*mat[j][k]);
	}
}
int main()
{
	while(scanf("%d",&n)==1)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				scanf("%lf",&mat[i][j]);
		int t;
		scanf("%d",&t);
		memset(calc,false,sizeof(calc));
		while(t--)
		{
			int src,dest;
			scanf("%d%d",&src,&dest);
			if(!calc[src])
			{
				dijkstra(src);
				calc[src]=true;
			}
			if(d[src][dest]>0)
				printf("%.3lf\n",d[src][dest]);
			else
				printf("What a pity!\n");
		}
	}
	return 0;
}
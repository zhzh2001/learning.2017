#include<cstdio>
#include<algorithm>
using namespace std;
double mat[1005][1005];
int main()
{
	int n;
	while(scanf("%d",&n)==1)
	{
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				scanf("%lf",&mat[i][j]);
		for(int k=1;k<=n;k++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					mat[i][j]=max(mat[i][j],mat[i][k]*mat[k][j]);
		int t;
		scanf("%d",&t);
		while(t--)
		{
			int u,v;
			scanf("%d%d",&u,&v);
			if(mat[u][v]>0)
				printf("%.3lf\n",mat[u][v]);
			else
				printf("What a pity!\n");
		}
	}
	return 0;
}
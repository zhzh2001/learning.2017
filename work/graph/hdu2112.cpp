#include<cstdio>
#include<string>
#include<cctype>
#include<map>
#include<cstring>
using namespace std;
const int INF=0x3f3f3f3f;
int cnt,mat[155][155],d[155];
bool vis[155];
map<string,int> num;
string getword()
{
	char c;
	while(isspace(c=getchar()));
	string s="";
	s+=c;
	while(!isspace(c=getchar()))
		s+=c;
	return s;
}
int main()
{
	int m;
	while(scanf("%d",&m)==1&&m!=-1)
	{
		string s=getword(),t=getword();
		num.clear();
		int n;
		if(s==t)
		{
			num[s]=1;
			n=1;
		}
		else
		{
			num[s]=1;
			num[t]=2;
			n=2;
		}
		memset(mat,0x3f,sizeof(mat));
		while(m--)
		{
			string u=getword(),v=getword();
			int w;
			scanf("%d",&w);
			int uu,vv;
			if(num.count(u))
				uu=num[u];
			else
				uu=num[u]=++n;
			if(num.count(v))
				vv=num[v];
			else
				vv=num[v]=++n;
			mat[uu][vv]=min(mat[uu][vv],w);
			mat[vv][uu]=min(mat[vv][uu],w);
		}
		memset(vis,false,sizeof(vis));
		memset(d,0x3f,sizeof(d));
		d[1]=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],d[j]+mat[j][k]);
		}
		if(d[num[t]]<INF)
			printf("%d\n",d[num[t]]);
		else
			printf("-1\n");
	}
	return 0;
}
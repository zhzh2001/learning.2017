#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
int mat[105][105],d[105];
bool vis[105];
int main()
{
	int n;
	while(scanf("%d",&n)==1&&n)
	{
		for(int i=1;i<=n*(n-1)/2;i++)
		{
			int u,v,w,status;
			scanf("%d%d%d%d",&u,&v,&w,&status);
			if(status)
				w=0;
			mat[u][v]=mat[v][u]=w;
		}
		memset(vis,false,sizeof(vis));
		memset(d,0x3f,sizeof(d));
		d[1]=0;
		int ans=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			ans+=d[j];
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],mat[j][k]);
		}
		printf("%d\n",ans);
	}
	return 0;
}
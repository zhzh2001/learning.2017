#include<cstdio>
#include<vector>
#include<queue>
#include<cstring>
#include<cctype>
using namespace std;
const int INF=0x3f3f3f3f;
char buf[1000005],*now;
struct node
{
	int v,w;
};
vector<node> mat[10000];
int d[10000][11];
bool vis[10000][11];
struct qnode
{
	int u,cnt;
	qnode(int u,int cnt):u(u),cnt(cnt){};
};
queue<qnode> q;
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int main()
{
	fread(buf,1,1000000,stdin);
	now=buf-1;
	int n=getint(),m=getint(),k=getint(),s=getint(),t=getint();
	while(m--)
	{
		int u=getint(),v=getint(),w=getint();
		mat[u].push_back((node){v,w});
		mat[v].push_back((node){u,w});
	}
	memset(d,0x3f,sizeof(d));
	d[s][0]=0;
	memset(vis,false,sizeof(vis));
	vis[s][0]=true;
	q.push(qnode(s,0));
	while(!q.empty())
	{
		qnode f=q.front();q.pop();
		vis[f.u][f.cnt]=false;
		for(int i=0;i<mat[f.u].size();i++)
		{
			if(d[f.u][f.cnt]+mat[f.u][i].w<d[mat[f.u][i].v][f.cnt])
			{
				d[mat[f.u][i].v][f.cnt]=d[f.u][f.cnt]+mat[f.u][i].w;
				if(!vis[mat[f.u][i].v][f.cnt])
				{
					vis[mat[f.u][i].v][f.cnt]=true;
					q.push(qnode(mat[f.u][i].v,f.cnt));
				}
			}
			if(f.cnt<k&&d[f.u][f.cnt]<d[mat[f.u][i].v][f.cnt+1])
			{
				d[mat[f.u][i].v][f.cnt+1]=d[f.u][f.cnt];
				if(!vis[mat[f.u][i].v][f.cnt+1])
				{
					vis[mat[f.u][i].v][f.cnt+1]=true;
					q.push(qnode(mat[f.u][i].v,f.cnt+1));
				}
			}
		}
	}
	int ans=INF;
	for(int i=0;i<=k;i++)
		ans=min(ans,d[t][i]);
	printf("%d\n",ans);
	return 0;
}
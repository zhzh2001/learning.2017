#include<iostream>
#include<vector>
#include<queue>
#include<cstring>
using namespace std;
const int INF=0x3f3f3f3f;
struct node
{
	int v,w;
};
vector<node> mat[55];
int d[55][55];
bool vis[55][55];
struct qnode
{
	int u,cnt;
};
queue<qnode> q;
int main()
{
	int n,m,cnt;
	cin>>n>>m>>cnt;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back((node){v,w});
		mat[v].push_back((node){u,w});
	}
	memset(vis,false,sizeof(vis));
	vis[1][0]=true;
	q.push((qnode){1,0});
	memset(d,0x3f,sizeof(d));
	d[1][0]=0;
	while(!q.empty())
	{
		qnode k=q.front();q.pop();
		vis[k.u][k.cnt]=false;
		for(int i=0;i<mat[k.u].size();i++)
		{
			if(d[k.u][k.cnt]+mat[k.u][i].w<d[mat[k.u][i].v][k.cnt])
			{
				d[mat[k.u][i].v][k.cnt]=d[k.u][k.cnt]+mat[k.u][i].w;
				if(!vis[mat[k.u][i].v][k.cnt])
				{
					vis[mat[k.u][i].v][k.cnt]=true;
					q.push((qnode){mat[k.u][i].v,k.cnt});
				}
			}
			if(k.cnt<cnt&&d[k.u][k.cnt]+mat[k.u][i].w/2<d[mat[k.u][i].v][k.cnt+1])
			{
				d[mat[k.u][i].v][k.cnt+1]=d[k.u][k.cnt]+mat[k.u][i].w/2;
				if(!vis[mat[k.u][i].v][k.cnt+1])
				{
					vis[mat[k.u][i].v][k.cnt+1]=true;
					q.push((qnode){mat[k.u][i].v,k.cnt+1});
				}
			}
		}
	}
	int ans=INF;
	for(int i=0;i<=cnt;i++)
		ans=min(ans,d[n][i]);
	cout<<ans<<endl;
	return 0;
}
#include<iostream>
#include<cstring>
#include<cmath>
using namespace std;
const double INF=1e100;
int x[105],y[105];
double d[105];
bool vis[105];
int main()
{
	int t;
	cin>>t;
	cout.precision(1);
	cout<<fixed;
	while(t--)
	{
		int n;
		cin>>n;
		for(int i=1;i<=n;i++)
			cin>>x[i]>>y[i];
		memset(vis,false,sizeof(vis));
		for(int i=0;i<=n;i++)
			d[i]=(i==1?0:INF);
		double ans=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			if(!j)
			{
				cout<<"oh!\n";
				ans=INF;
				break;
			}
			vis[j]=true;
			ans+=d[j];
			for(int k=1;k<=n;k++)
				if(!vis[k])
				{
					double dist=sqrt((x[j]-x[k])*(x[j]-x[k])+(y[j]-y[k])*(y[j]-y[k]));
					if(dist>=10&&dist<=1000)
						d[k]=min(d[k],dist);
				}
		}
		if(ans<INF)
			cout<<ans*100<<endl;
	}
	return 0;
}
#include<iostream>
#include<vector>
#include<cstring>
#include<queue>
using namespace std;
const int INF=0x3f3f3f3f;
int n,m,sum[105],pred[105][105],d[105],cnt[105][105];
vector<int> mat[105];
bool vis[105];
struct edge
{
	int u,v;
};
edge e[3005];
struct node
{
	int u,w;
	node(int u,int w):u(u),w(w){};
};
queue<node> q;
void bfs(int s,bool flag)
{
	memset(vis,false,sizeof(vis));
	vis[s]=true;
	memset(d,0x3f,sizeof(d));
	q.push(node(s,0));
	pred[s][s]=0;
	while(!q.empty())
	{
		node k=q.front();q.pop();
		d[k.u]=k.w;
		for(int i=0;i<mat[k.u].size();i++)
			if(!vis[mat[k.u][i]]&&cnt[k.u][mat[k.u][i]])
			{
				vis[mat[k.u][i]]=true;
				if(flag)
					pred[s][mat[k.u][i]]=k.u;
				q.push(node(mat[k.u][i],k.w+1));
			}
	}
}
int main()
{
	while(cin>>n>>m)
	{
		for(int i=1;i<=n;i++)
			mat[i].clear();
		memset(cnt,0,sizeof(cnt));
		for(int i=1;i<=m;i++)
		{
			cin>>e[i].u>>e[i].v;
			mat[e[i].u].push_back(e[i].v);
			mat[e[i].v].push_back(e[i].u);
			cnt[e[i].u][e[i].v]++;
			cnt[e[i].v][e[i].u]++;
		}
		bool flag=false;
		for(int i=1;i<=n;i++)
		{
			bfs(i,true);
			sum[i]=0;
			for(int j=1;j<=n;j++)
			{
				if(d[j]==INF)
				{
					flag=true;
					break;
				}
				sum[i]+=d[j];
			}
		}
		for(int i=1;i<=m;i++)
			if(flag)
				cout<<"INF\n";
			else
			{
				int ans=0;
				for(int j=1;j<=n;j++)
					if(pred[j][e[i].u]!=e[i].v&&pred[j][e[i].v]!=e[i].u)
						ans+=sum[j];
					else
					{
						cnt[e[i].u][e[i].v]--;
						cnt[e[i].v][e[i].u]--;
						if(cnt[e[i].u][e[i].v])
							ans+=sum[j];
						else
						{
							bfs(j,false);
							for(int k=1;k<=n;k++)
							{
								if(d[k]==INF)
								{
									ans=INF;
									break;
								}
								ans+=d[k];
							}
							if(ans==INF)
								break;
						}
						cnt[e[i].u][e[i].v]++;
						cnt[e[i].v][e[i].u]++;
					}
				if(ans<INF)
					cout<<ans<<endl;
				else
					cout<<"INF\n";
			}
	}
	return 0;
}
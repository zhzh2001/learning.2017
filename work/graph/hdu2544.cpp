#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int mat[105][105],d[105];
bool vis[105];
int main()
{
	int n,m;
	while(cin>>n>>m&&n*m)
	{
		memset(mat,0x3f,sizeof(mat));
		while(m--)
		{
			int u,v,w;
			cin>>u>>v>>w;
			mat[u][v]=min(mat[u][v],w);
			mat[v][u]=min(mat[v][u],w);
		}
		memset(vis,false,sizeof(vis));
		memset(d,0x3f,sizeof(d));
		d[1]=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],d[j]+mat[j][k]);
		}
		cout<<d[n]<<endl;
	}
	return 0;
}
#include<iostream>
#include<algorithm>
using namespace std;
struct edge
{
	int u,v,w;
	bool operator<(const edge& b)const
	{
		return w<b.w;
	}
};
edge e[5005];
int f[105];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n,m;
	while(cin>>m>>n&&m)
	{
		for(int i=1;i<=m;i++)
			cin>>e[i].u>>e[i].v>>e[i].w;
		sort(e+1,e+m+1);
		for(int i=1;i<=n;i++)
			f[i]=i;
		int ans=0,cnt=0;
		for(int i=1;cnt<n-1&&i<=m;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v);
			if(ru!=rv)
			{
				ans+=e[i].w;
				f[ru]=rv;
				cnt++;
			}
		}
		if(cnt==n-1)
			cout<<ans<<endl;
		else
			cout<<"?\n";
	}
	return 0;
}
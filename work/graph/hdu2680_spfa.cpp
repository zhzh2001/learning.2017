#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
const int INF=0x3f3f3f3f;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
};
vector<node> mat[1005];
int d[1005];
bool vis[1005];
queue<int> q;
int main()
{
	int n,m,s;
	while(scanf("%d%d%d",&n,&m,&s)==3)
	{
		for(int i=1;i<=n;i++)
			mat[i].clear();
		while(m--)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			mat[v].push_back(node(u,w));
		}
		memset(vis,false,sizeof(vis));
		vis[s]=true;
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		q.push(s);
		while(!q.empty())
		{
			int k=q.front();q.pop();
			vis[k]=false;
			for(int i=0;i<mat[k].size();i++)
				if(d[k]+mat[k][i].w<d[mat[k][i].v])
				{
					d[mat[k][i].v]=d[k]+mat[k][i].w;
					if(!vis[mat[k][i].v])
					{
						vis[mat[k][i].v]=true;
						q.push(mat[k][i].v);
					}
				}
		}
		int t;
		scanf("%d",&t);
		int ans=INF;
		while(t--)
		{
			int x;
			scanf("%d",&x);
			ans=min(ans,d[x]);
		}
		if(ans<INF)
			printf("%d\n",ans);
		else
			printf("-1\n");
	}
	return 0;
}
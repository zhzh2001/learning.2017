#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int mat[55][55],d[55],pred[55],w[1005];
bool vis[55];
bool cmp(int x,int y)
{
	return x>y;
}
int main()
{
	int n,m,k;
	cin>>n>>m>>k;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	memset(vis,false,sizeof(vis));
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[j]+mat[j][k]<d[k])
			{
				d[k]=d[j]+mat[j][k];
				pred[k]=j;
			}
	}
	m=0;
	for(int i=n;i!=1;i=pred[i])
		w[++m]=mat[i][pred[i]];
	sort(w+1,w+m+1,cmp);
	int ans=0;
	for(int i=1;i<=k&&i<=m;i++)
		ans+=w[i]/2;
	for(int i=k+1;i<=m;i++)
		ans+=w[i];
	cout<<ans<<endl;
	return 0;
}
#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>
#include<queue>
using namespace std;
const int INF=0x3f3f3f3f;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
	bool operator>(const node& b)const
	{
		return w>b.w;
	}
};
vector<node> mat[1005];
int d[1005];
bool vis[1005];
priority_queue<node,vector<node>,greater<node> > q;
int main()
{
	int n,m,s;
	while(scanf("%d%d%d",&n,&m,&s)==3)
	{
		for(int i=1;i<=n;i++)
			mat[i].clear();
		while(m--)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			mat[v].push_back(node(u,w));
		}
		memset(vis,false,sizeof(vis));
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		q.push(node(s,0));
		while(!q.empty())
		{
			node k=q.top();q.pop();
			if(!vis[k.v])
			{
				vis[k.v]=true;
				for(int i=0;i<mat[k.v].size();i++)
					if(d[k.v]+mat[k.v][i].w<d[mat[k.v][i].v])
					{
						d[mat[k.v][i].v]=d[k.v]+mat[k.v][i].w;
						q.push(node(mat[k.v][i].v,d[mat[k.v][i].v]));
					}
			}
		}
		int t;
		scanf("%d",&t);
		int ans=INF;
		while(t--)
		{
			int x;
			scanf("%d",&x);
			ans=min(ans,d[x]);
		}
		if(ans<INF)
			printf("%d\n",ans);
		else
			printf("-1\n");
	}
	return 0;
}
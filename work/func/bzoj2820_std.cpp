#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<set>
#include<ctime>
#include<vector>
#include<queue>
#include<algorithm>
#include<map>
#include<cmath>
#define inf 1000000000
#define N 10000000
#define pa pair<int,int>
#define ll long long 
using namespace std;
int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int T,n,m,cnt;
bool mark[10000005];
int pri[1000005],mu[10000005];
ll f[10000005];
void getphi()
{
	mu[1]=1;
	for(int i=2;i<=N;i++)
	{
		if(!mark[i])pri[++cnt]=i,mu[i]=-1;
		for(int j=1;j<=cnt&&pri[j]*i<=N;j++)
		{
			mark[i*pri[j]]=1;
			if(i%pri[j]==0){mu[i*pri[j]]=0;break;}
			else mu[i*pri[j]]=-mu[i];
		}
	}
	for(int i=1;i<=cnt;i++)
	{
		int p=pri[i];
		for(int j=1;j*p<=N;j++)f[j*p]+=mu[j];
	}
	for(int i=1;i<=N;i++)f[i]+=f[i-1];
}
int main()
{
	getphi();
	T=read();
	while(T--)
	{
		ll ans=0;
		n=read();m=read();
		if(n>m)swap(n,m);
		for(int i=1,j;i<=n;i=j+1)
		{
			j=min(n/(n/i),m/(m/i));
			ans+=(f[j]-f[i-1])*(n/i)*(m/i);
		}
		printf("%lld\n",ans);
	}
	return 0;
}

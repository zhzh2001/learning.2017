#include<bits/stdc++.h>
using namespace std;
const int MOD=20101009;
int gcd(int a,int b)
{
	return !b?a:gcd(b,a%b);
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			ans=(ans+(long long)i*j/gcd(i,j)%MOD)%MOD;
	printf("%d\n",ans);
	return 0;
}

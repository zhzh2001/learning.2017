#include<bits/stdc++.h>
using namespace std;
const int N=5e6;
bool bl[N+5];
int p[N/10],mu[N+5],f[N+5];
int main()
{
	mu[1]=1;
	int pn=0;
	for(int i=2;i<=N;i++)
	{
		if(!bl[i])
		{
			p[++pn]=i;
			mu[i]=-1;
			f[i]=1;
		}
		for(int j=1;j<=pn&&i*p[j]<=N;j++)
		{
			bl[i*p[j]]=true;
			if(i%p[j])
			{
				mu[i*p[j]]=-mu[i];
				f[i*p[j]]=mu[i]-f[i];
			}
			else
			{
				mu[i*p[j]]=0;
				f[i*p[j]]=mu[i];
				break;
			}
		}
	}
	for(int i=1;i<=N;i++)
		f[i]+=f[i-1];
	int t;
	cin>>t;
	while(t--)
	{
		int n,m;
		cin>>n>>m;
		if(n>m)
			swap(n,m);
		int l,r;
		long long ans=0;
		for(l=1;l<=n;l=r+1)
		{
			r=min(n/(n/l),m/(m/l));
			ans+=(long long)(f[r]-f[l-1])*(n/l)*(m/l);
		}
		cout<<ans<<endl;
	}
	return 0;
}

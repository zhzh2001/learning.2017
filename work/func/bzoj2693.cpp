#include<bits/stdc++.h>
using namespace std;
const int N=1e7,MOD=1e8+9;
bool bl[N+5];
int p[N/10],f[N+5];
inline int sum(int n,int m)
{
	return ((long long)(1+n)*n/2%MOD)*((long long)(1+m)*m/2%MOD)%MOD;
}
int main()
{
	f[1]=1;
	int pn=0;
	for(int i=2;i<=N;i++)
	{
		if(!bl[i])
		{
			p[++pn]=i;
			f[i]=((i-(long long)i*i)%MOD+MOD)%MOD;
		}
		for(int j=1;j<=pn&&i*p[j]<=N;j++)
		{
			bl[i*p[j]]=true;
			if(i%p[j])
				f[i*p[j]]=(long long)f[i]*f[p[j]]%MOD;
			else
			{
				f[i*p[j]]=(long long)f[i]*p[j]%MOD;
				break;
			}
		}
	}
	for(int i=1;i<=N;i++)
		f[i]=(f[i-1]+f[i])%MOD;
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n,m;
		scanf("%d%d",&n,&m);
		if(n>m)
			swap(n,m);
		int ans=0,r;
		for(int l=1;l<=n;l=r+1)
		{
			r=min(n/(n/l),m/(m/l));
			ans=(ans+((long long)(f[r]-f[l-1])%MOD+MOD)%MOD*sum(n/l,m/l)%MOD)%MOD;
		}
		printf("%d\n",(ans+MOD)%MOD);
	}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<algorithm>
using namespace std;
//typedef long long ll;
const long long X=10000050,mod=100000009;
void read(long long &x)
{
    x=0;
    char c=getchar();
    while (c<'0'||c>'9')
        c=getchar();
    while (c<='9'&&c>='0')
    {
        x=x*10+c-'0';
        c=getchar();
    }
}
long long phi[1000005],n,m,t;
bool b[X+5];
long long h[X+5],sum[X+5];
void prework()
{
    h[1]=1;
    for (long long i=2;i<=X;++i)
    {
        if (!b[i])
        {
            phi[++phi[0]]=i;
            h[i]=(i-i*i)%mod;
        }
        for (int j=1;j<=phi[0]&&phi[j]*i<=X;++j)
        {
            b[i*phi[j]]=1;
            if (i%phi[j])
                h[i*phi[j]]=h[i]*h[phi[j]]%mod;
            else
            {
                h[i*phi[j]]=h[i]*phi[j]%mod;
                break;
            }
        }
    }
    for (long long i=1;i<=X;++i)
        sum[i]=sum[i-1]+h[i];
}
long long Sum(long long x,long long y)
{
    x%=mod;
    y%=mod;
    long long a=(x*(x+1)/2)%mod,b=(y*(y+1)/2)%mod;
    return a*b%mod;
}
long long query(long long x,long long y)
{
    long long ans=0;
    long long i,j;
    if (x>y)
        swap(x,y);
    for (i=1;i<=x;i=j+1)
    {
        j=min(x/(x/i),y/(y/i));
        ans+=Sum(x/i,y/i)*(sum[j]-sum[i-1])%mod;
        ans%=mod;
    }
    return (ans+mod)%mod;
}
int main()
{
    prework();
    read(t);
    while (t--)
    {
        read(n);
        read(m);
        printf("%lld\n",query(n,m));
    }
    return 0;
}

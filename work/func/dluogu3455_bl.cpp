#include<bits/stdc++.h>
using namespace std;
int gcd(int a,int b)
{
	return !b?a:gcd(b,a%b);
}
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n,m,g;
		cin>>n>>m>>g;
		int ans=0;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=m;j++)
				ans+=gcd(i,j)==g;
		cout<<ans<<endl;
	}
	return 0;
}
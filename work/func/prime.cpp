#include<bits/stdc++.h>
#include<gmpxx.h>
using namespace std;
int main()
{
	mpz_class n;
	int rep;
	cin>>n>>rep;
	switch(mpz_probab_prime_p(n.get_mpz_t(),rep))
	{
		case 0:cout<<n<<" is not a prime\n";break;
		case 1:cout<<n<<" is probably a prime\n";break;
		case 2:cout<<n<<" is a prime\n";break;
	}
	return 0;
}

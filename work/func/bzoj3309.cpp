#include<bits/stdc++.h>
using namespace std;
const int N=1e7;
bool bl[N+5];
int p[N/10],f[N+5],cnt[N+5],last[N+5];
int main()
{
	int pn=0;
	for(int i=2;i<=N;i++)
	{
		if(!bl[i])
		{
			p[++pn]=i;
			f[i]=cnt[i]=last[i]=1;
		}
		for(int j=1;j<=pn&&i*p[j]<=N;j++)
		{
			int now=i*p[j];
			bl[now]=true;
			if(i%p[j])
			{
				cnt[now]=1;
				last[now]=i;
				f[now]=cnt[i]==1?-f[i]:0;
			}
			else
			{
				cnt[now]=cnt[i]+1;
				last[now]=last[i];
				if(last[now]==1)
					f[now]=1;
				else
					f[now]=cnt[now]==cnt[last[now]]?-f[last[now]]:0;
				break;
			}
		}
	}
	for(int i=1;i<=N;i++)
		f[i]+=f[i-1];
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n,m;
		scanf("%d%d",&n,&m);
		if(n>m)
			swap(n,m);
		long long ans=0;
		int r;
		for(int l=1;l<=n;l=r+1)
		{
			r=min(n/(n/l),m/(m/l));
			ans+=(long long)(n/l)*(m/l)*(f[r]-f[l-1]);
		}
		printf("%lld\n",ans);
	}
	return 0;
}

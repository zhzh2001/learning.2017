#include<bits/stdc++.h>
using namespace std;
const int N=50000;
bool bl[N+5];
int p[N>>3],mu[N+5];
int main()
{
	mu[1]=1;
	int pn=0;
	for(int i=2;i<=N;i++)
	{
		if(!bl[i])
		{
			p[++pn]=i;
			mu[i]=-1;
		}
		for(int j=1;j<=pn&&i*p[j]<=N;j++)
		{
			bl[i*p[j]]=true;
			if(i%p[j])
				mu[i*p[j]]=-mu[i];
			else
			{
				mu[i*p[j]]=0;
				break;
			}
		}
	}
	for(int i=1;i<=N;i++)
		mu[i]+=mu[i-1];
	int t;
	cin>>t;
	while(t--)
	{
		int n,m,g;
		cin>>n>>m>>g;
		n/=g;m/=g;
		if(n>m)
			swap(n,m);
		int ans=0,r;
		for(int l=1;l<=n;l=r+1)
		{
			r=min(n/(n/l),m/(m/l));
			ans+=(n/l)*(m/l)*(mu[r]-mu[l-1]);
		}
		cout<<ans<<endl;
	}
	return 0;
}
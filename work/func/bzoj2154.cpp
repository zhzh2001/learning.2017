#include<bits/stdc++.h>
using namespace std;
const int N=10000005,MOD=20101009;
bool bl[N];
int p[N/10],mu[N],s[N];
inline int sum(int n,int m)
{
	return ((long long)(1+n)*n/2%MOD)*((long long)(1+m)*m/2%MOD)%MOD;
}
int F(int n,int m)
{
	int ans=0,r;
	for(int l=1;l<=n;l=r+1)
	{
		r=min(n/(n/l),m/(m/l));
		ans=(ans+(long long)(s[r]-s[l-1])*sum(n/l,m/l)%MOD)%MOD;
	}
	return ans;
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	if(n>m)
		swap(n,m);
	mu[1]=1;
	int pn=0;
	for(int i=2;i<=m;i++)
	{
		if(!bl[i])
		{
			p[++pn]=i;
			mu[i]=-1;
		}
		for(int j=1;j<=pn&&i*p[j]<=m;j++)
		{
			bl[i*p[j]]=true;
			if(i%p[j])
				mu[i*p[j]]=-mu[i];
			else
			{
				mu[i*p[j]]=0;
				break;
			}
		}
	}
	for(int i=1;i<=m;i++)
		s[i]=(s[i-1]+(long long)i*i*mu[i]%MOD)%MOD;
	int ans=0,r;
	for(int l=1;l<=n;l=r+1)
	{
		r=min(n/(n/l),m/(m/l));
		ans=(ans+(long long)(l+r)*(r-l+1)/2%MOD*F(n/l,m/l)%MOD)%MOD;
	}
	printf("%d\n",(ans+MOD)%MOD);
	return 0;
}

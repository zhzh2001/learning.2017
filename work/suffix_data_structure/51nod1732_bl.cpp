#include<bits/stdc++.h>
using namespace std;
int main()
{
	int n,q;
	string s;
	cin>>n>>s>>q;
	while(q--)
	{
		int x,y;
		cin>>x>>y;
		if(x>y)
			swap(x,y);
		int ans=0;
		for(int i=x,j=y;j<n;i++,j++)
			if(s[i]==s[j])
				ans++;
			else
				break;
		cout<<ans<<endl;
	}
	return 0;
}
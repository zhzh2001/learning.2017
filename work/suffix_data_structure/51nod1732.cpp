#include<bits/stdc++.h>
using std::sort;
using std::unique;
using std::lower_bound;
using std::min;
using std::swap;
const int N=1005,LOGN=11;
char s[N],t[N];
struct node
{
	int x,y,id;
	bool operator<(const node& rhs)const
	{
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}a[N];
int rank[N],sa[N],h[N],rmq[N][LOGN],Log2[N];
struct Istream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p;
	bool good;
	
	void fetch()
	{
		if(!buf)
			p=buf=new char [bufsz+1];
		size_t sz=fread(buf,1,bufsz,stream);
		buf[sz]='\0';
		good=sz;
	}
	
	char nextchar()
	{
		if(!*p)
			fetch();
		return *p++;
	}
	
	public:
	Istream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL),good(false){}
	
	explicit Istream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"r")),bufsz(bufsz),buf(NULL),p(NULL),good(false)
	{
		fetch();
	}
	
	explicit Istream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(NULL),p(NULL),good(false)
	{
		fetch();
	}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	~Istream()
	{
		close();
		delete [] buf;
	}
	
	operator bool()const
	{
		return good;
	}
	
	bool operator!()const
	{
		return !good;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"r");
		this->bufsz=bufsz;
		fetch();
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		fetch();
		return stream;
	}
	
	template<typename Int>
	Istream& operator>>(Int& x)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		x=0;
		if(!good)
			return *this;
		Int sign=1;
		if(c=='-')
			sign=-1,c=nextchar();
		for(;isdigit(c);c=nextchar())
			x=x*10+c-'0';
		x*=sign;
		return *this;
	}
};

struct Ostream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p,dig[25];
	
	public:
	Ostream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL){}
	
	explicit Ostream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"w")),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	explicit Ostream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	void flush()
	{
		fwrite(buf,1,p-buf,stream);
		p=buf;
	}
	
	private:
	void writechar(char c)
	{
		*p++=c;
		if(p==buf+bufsz)
			flush();
	}
	
	public:
	~Ostream()
	{
		flush();
		close();
		delete [] buf;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"w");
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	template<typename Int>
	Ostream& operator<<(Int x)
	{
		if(x<0)
			writechar('-'),x=-x;
		int len=0;
		do
			dig[++len]=x%10;
		while(x/=10);
		for(;len;len--)
			writechar(dig[len]+'0');
		return *this;
	}
	
	template<typename Ptr>
	Ostream& operator<<(Ptr* func)
	{
		return func(*this);
	}
	
	friend Ostream& endl(Ostream& os);
};

Ostream& endl(Ostream& os)
{
	os.writechar('\n');
	return os;
}

int main()
{
	int n;
	scanf("%d%s",&n,s);
	strcpy(t,s);
	sort(t,t+n);
	int cnt=unique(t,t+n)-t;
	for(int j=0;j<n;j++)
		rank[j]=lower_bound(t,t+cnt,s[j])-t;
	for(int i=0;1<<i<=n;i++)
	{
		for(int j=0;j<n;j++)
		{
			a[j].id=j;
			a[j].x=rank[j];
			a[j].y=j+(1<<i)<n?rank[j+(1<<i)]:-1;
		}
		sort(a,a+n);
		int cnt=0;
		for(int j=0;j<n;j++)
		{
			rank[a[j].id]=cnt;
			cnt+=a[j].x!=a[j+1].x||a[j].y!=a[j+1].y;
		}
	}
	for(int j=0;j<n;j++)
		sa[rank[j]]=j;
	int k=0;
	for(int i=0;i<n;h[rank[i++]]=k)
		for(k?k--:0;rank[i]&&s[i+k]==s[sa[rank[i]-1]+k];k++);
	for(int i=0;i<n;i++)
		rmq[i][0]=h[i];
	for(int j=1;j<LOGN;j++)
		for(int i=0;i<n;i++)
			rmq[i][j]=min(rmq[i][j-1],rmq[i+(1<<j-1)][j-1]);
	Log2[1]=0;
	for(int i=2;i<=n;i++)
		Log2[i]=Log2[i/2]+1;
	int q;
	Istream in(stdin);
	Ostream out(stdout);
	in>>q;
	while(q--)
	{
		int x,y;
		in>>x>>y;
		if(x==y)
		{
			out<<n-x<<endl;
			continue;
		}
		int l=rank[x],r=rank[y];
		if(l>r)
			swap(l,r);
		l++;
		int p=Log2[r-l+1];
		out<<min(rmq[l][p],rmq[r-(1<<p)+1][p])<<endl;
	}
	return 0;
}
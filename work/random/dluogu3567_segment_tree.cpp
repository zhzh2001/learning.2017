#include<bits/stdc++.h>
using namespace std;
const int N=500005,SZ=N*16+5;
int a[N],b[N],root[N],cc;
struct node
{
	int sum,lc,rc;
}tree[SZ];
int val;
void insert(int& o,int pred,int l,int r)
{
	tree[o=++cc]=tree[pred];
	tree[o].sum++;
	if(l==r)
		return;
	int mid=(l+r)/2;
	if(val<=mid)
		insert(tree[o].lc,tree[pred].lc,l,mid);
	else
		insert(tree[o].rc,tree[pred].rc,mid+1,r);
}
int query(int ro,int lo,int l,int r)
{
	if(l==r)
		return l;
	int mid=(l+r)/2;
	if(tree[tree[ro].lc].sum-tree[tree[lo].lc].sum>val)
		return query(tree[ro].lc,tree[lo].lc,l,mid);
	if(tree[tree[ro].rc].sum-tree[tree[lo].rc].sum>val)
		return query(tree[ro].rc,tree[lo].rc,mid+1,r);
	return 0;
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	copy(a+1,a+n+1,b+1);
	sort(b+1,b+n+1);
	int n1=unique(b+1,b+n+1)-b-1;
	for(int i=1;i<=n;i++)
	{
		val=lower_bound(b+1,b+n1+1,a[i])-b;
		insert(root[i],root[i-1],1,n1);
	}
	while(m--)
	{
		int l,r;
		scanf("%d%d",&l,&r);
		val=(r-l+1)/2;
		printf("%d\n",b[query(root[r],root[l-1],1,n1)]);
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1000005,D=10000,t=10;
long long a[N],d[D];
int cnt[D];
long long gcd(long long a,long long b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	minstd_rand gen(time(NULL));
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	long long ans=1;
	for(int times=1;times<=t;times++)
	{
		uniform_int_distribution<> dn(1,n);
		long long x=a[dn(gen)];
		int cc=0;
		for(long long i=1;i*i<=x;i++)
			if(x%i==0)
			{
				d[++cc]=i;
				if(i*i<x)
					d[++cc]=x/i;
			}
		sort(d+1,d+cc+1);
		fill(cnt+1,cnt+cc+1,0);
		for(int i=1;i<=n;i++)
			cnt[lower_bound(d+1,d+cc+1,gcd(a[i],x))-d]++;
		for(int i=1;i<=cc;i++)
			for(int j=1;j<i;j++)
				if(d[i]%d[j]==0)
					cnt[j]+=cnt[i];
		for(int i=1;i<=cc;i++)
			if(cnt[i]*2>=n)
				ans=max(ans,d[i]);
	}
	cout<<ans<<endl;
	return 0;
}
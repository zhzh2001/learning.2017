#include<bits/stdc++.h>
using namespace std;
const int N=500005,k=20;
int a[N];
vector<int> p[N];
int main()
{
	srand(216548431);
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",a+i);
		p[a[i]].push_back(i);
	}
	while(m--)
	{
		int l,r;
		scanf("%d%d",&l,&r);
		int ans=0;
		for(int i=1;i<=k;i++)
		{
			int x=a[rand()%(r-l+1)+l];
			if((upper_bound(p[x].begin(),p[x].end(),r)-lower_bound(p[x].begin(),p[x].end(),l))*2>r-l+1)
			{
				ans=x;
				break;
			}
		}
		printf("%d\n",ans);
	}
	return 0;
}
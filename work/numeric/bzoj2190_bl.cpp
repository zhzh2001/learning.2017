#include<bits/stdc++.h>
using namespace std;
int gcd(int a,int b)
{
	return b==0?a:gcd(b,a%b);
}
int main()
{
	int n;
	cin>>n;
	int ans=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			if(gcd(i,j)==1)
				ans++;
	cout<<ans<<endl;
	return 0;
}
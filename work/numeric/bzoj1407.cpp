#include<bits/stdc++.h>
using namespace std;
const int N=20;
int c[N],p[N],l[N],g;
void exgcd(int a,int b,int& x,int& y)
{
	if(b==0){g=a;x=1;y=0;}
	else{exgcd(b,a%b,y,x);y-=x*(a/b);}
}
int main()
{
	int n;
	cin>>n;
	int m=0;
	for(int i=1;i<=n;i++)
	{
		cin>>c[i]>>p[i]>>l[i];
		m=max(m,c[i]);
	}
	while(m<=1e6)
	{
		bool flag=false;
		for(int i=1;i<n;i++)
		{
			for(int j=i+1;j<=n;j++)
			{
				int a=p[i]-p[j],b=m,c=::c[j]-::c[i],x,y;
				exgcd(a,b,x,y);
				if(c%g==0)
				{
					x=(x*c/g)%(b/g);
					if(x<0)
						x+=abs(b/g);
					if(x<=min(l[i],l[j]))
					{
						flag=true;
						break;
					}
				}
			}
			if(flag)
				break;
		}
		if(!flag)
		{
			cout<<m<<endl;
			break;
		}
		m++;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int p=20100403;
void exgcd(int a,int b,int& x,int& y)
{
	if(!b){x=1;y=0;}
	else{exgcd(b,a%b,y,x);y-=x*(a/b);}
}
int main()
{
	int n,m;
	cin>>n>>m;
	int ans=n-m+1;
	for(int i=n+2;i<=n+m;i++)
		ans=(long long)ans*i%p;
	int ans0=1;
	for(int i=2;i<=m;i++)
		ans0=(long long)ans0*i%p;
	int inv,y;
	exgcd(ans0,p,inv,y);
	inv=(inv%p+p)%p;
	ans=(long long)ans*inv%p;
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int M=100000;
int cnt[M+5];
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		memset(cnt,0,sizeof(cnt));
		for(int i=1;i<=n;i++)
		{
			int p,q;
			cin>>p>>q;
			cnt[p]=q;
		}
		int ans=0;
		for(int i=M;i>1;i--)
			if(cnt[i])
			{
				if(cnt[i]>ans)
					ans+=cnt[i]-ans;
				int t=i-1;
				for(int j=2;j*j<=t;j++)
					while(t%j==0)
					{
						cnt[j]++;
						t/=j;
					}
				if(t>1)
					cnt[t]++;
			}
		cout<<ans<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
struct bigint
{
	static const int BASE=100000000,WIDTH=8;
	int len;
	long long dig[1300];
	bigint()
	{
		len=1;
		memset(dig,0,sizeof(dig));
	}
	bool operator<(const bigint& b)const
	{
		if(len!=b.len)
			return len<b.len;
		for(int i=len;i;i--)
			if(dig[i]!=b.dig[i])
				return dig[i]<b.dig[i];
		return false;
	}
	bigint& operator*=(const int x)
	{
		int overflow=0;
		for(int i=1;i<=len;i++)
		{
			dig[i]=dig[i]*x+overflow;
			overflow=dig[i]/BASE;
			dig[i]%=BASE;
		}
		if(overflow)
			dig[++len]=overflow;
		return *this;
	}
	bigint& operator/=(const int x)
	{
		int remain=0;
		for(int i=len;i;i--)
		{
			int t=(dig[i]+remain)/x;
			remain=(dig[i]+remain)%x*BASE;
			dig[i]=t;
		}
		if(dig[len]==0)
			len--;
		return *this;
	}
	bigint& operator-=(const bigint& b)
	{
		for(int i=1;i<=len;i++)
		{
			dig[i]-=b.dig[i];
			if(dig[i]<0)
			{
				dig[i]+=BASE;
				dig[i+1]--;
			}
		}
		for(;len>1&&dig[len]==0;len--);
		return *this;
	}
};
istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b.len=s.length()/bigint::WIDTH+1;
	for(int i=1;i<=b.len;i++)
	{
		stringstream ss(s.substr(max((int)s.length()-i*bigint::WIDTH,0),i<b.len?bigint::WIDTH:s.length()-(i-1)*bigint::WIDTH));
		ss>>b.dig[i];
	}
	for(;b.len>1&&b.dig[b.len]==0;b.len--);
	return is;
}
ostream& operator<<(ostream& os,const bigint& b)
{
	os.fill('0');
	for(int i=b.len;i;i--)
	{
		if(i<b.len)
			os.width(bigint::WIDTH);
		os<<b.dig[i];
	}
	return os;
}
int main()
{
	bigint a,b;
	cin>>a>>b;
	int cnt=0;
	while(true)
	{
		if(a.dig[1]%2==0&&b.dig[1]%2==0)
		{
			a/=2;b/=2;
			cnt++;
		}
		else
			if(a.dig[1]%2==0)
				a/=2;
			else
				if(b.dig[1]%2==0)
					b/=2;
		if(a<b)
		{
			b-=a;
			if(b.len==1&&!b.dig[1])
			{
				for(int i=1;i<=cnt;i++)
					a*=2;
				cout<<a<<endl;
				break;
			}
		}
		else
		{
			a-=b;
			if(a.len==1&&!a.dig[1])
			{
				for(int i=1;i<=cnt;i++)
					b*=2;
				cout<<b<<endl;
				break;
			}
		}
	}
	return 0;
}
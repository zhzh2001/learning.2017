#include<bits/stdc++.h>
using namespace std;
const int N=40005;
int phi[N];
int main()
{
	int n;
	cin>>n;
	phi[1]=1;
	for(int i=2;i<n;i++)
		if(!phi[i])
			for(int j=i;j<n;j+=i)
			{
				if(!phi[j])
					phi[j]=j;
				phi[j]=phi[j]/i*(i-1);
			}
	int ans=0;
	for(int i=1;i<n;i++)
		ans+=phi[i]*2;
	cout<<ans+1<<endl;
	return 0;
}
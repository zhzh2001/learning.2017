#include<bits/stdc++.h>
using namespace std;
const int p=100003;
long long qpow(long long a,long long b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	long long m,n;
	cin>>m>>n;
	cout<<((qpow(m,n)-m*qpow(m-1,n-1))%p+p)%p<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005;
int f[N][2*N];
int main()
{
	int n;
	cin>>n;
	f[1][1]=1;
	for(int i=2;i<=n;i++)
		for(int j=i;j<2*i;j++)
			for(int k=i-1;k<2*(i-1);k++)
				f[i][j]+=f[i-1][k];
	int ans=0;
	for(int i=n;i<2*n;i++)
		ans+=f[n][i];
	cout<<ans<<endl;
	return 0;
}
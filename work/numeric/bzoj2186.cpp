#include<bits/stdc++.h>
using namespace std;
const int N=10000000;
bool bl[N+5];
int p[N/10],fact[N+5],inv[N+5],pi[N+5];
int main()
{
	int t,r;
	cin>>t>>r;
	int pn=0;
	for(int i=2;i<=N;i++)
	{
		if(!bl[i])
			p[++pn]=i;
		for(int j=1;j<=pn&&i*p[j]<=N;j++)
		{
			bl[i*p[j]]=true;
			if(i%p[j]==0)
				break;
		}
	}
	
	fact[1]=1;
	for(int i=2;i<=N;i++)
		fact[i]=(long long)fact[i-1]*i%r;
	
	inv[1]=1;
	for(int i=2;i<=N&&i<r;i++)
		inv[i]=(long long)(r-r/i)*inv[r%i]%r;
	pi[1]=1;
	for(int i=2;i<=N;i++)
		if(bl[i])
			pi[i]=pi[i-1];
		else
			pi[i]=(long long)pi[i-1]*(i-1)%r*inv[i%r]%r;
	
	while(t--)
	{
		int n,m;
		cin>>n>>m;
		cout<<(long long)fact[n]*pi[m]%r<<endl;
	}
	return 0;
}
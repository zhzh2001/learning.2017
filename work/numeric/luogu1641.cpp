#include<bits/stdc++.h>
using namespace std;
const int p=20100403,inv=p-2;
int qpow(int a,int b,int p)
{
	int ans=1;
	do
	{
		if(b&1)
			ans=(long long)ans*a%p;
		a=(long long)a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n,m;
	cin>>n>>m;
	int ans=n-m+1;
	for(int i=n+2;i<=n+m;i++)
		ans=(long long)ans*i%p;
	for(int i=2;i<=m;i++)
		ans=(long long)ans*qpow(i,inv,p)%p;
	cout<<ans<<endl;
	return 0;
}
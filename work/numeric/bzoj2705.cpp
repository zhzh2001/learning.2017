#include<bits/stdc++.h>
using namespace std;
long long euler(long long x)
{
	long long ans=x;
	for(long long i=2;i*i<=x;i++)
		if(x%i==0)
		{
			ans=ans/i*(i-1);
			while(x%i==0)
				x/=i;
		}
	if(x>1)
		ans=ans/x*(x-1);
	return ans;
}
int main()
{
	long long n;
	cin>>n;
	long long ans=0;
	for(long long i=1;i*i<=n;i++)
		if(n%i==0)
		{
			ans+=i*euler(n/i);
			if(i*i<n)
				ans+=n/i*euler(i);
		}
	cout<<ans<<endl;
	return 0;
}
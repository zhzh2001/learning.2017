#include<bits/stdc++.h>
using namespace std;
const int N=100000;
int f[N+5],p[N/10];
int main()
{
	f[1]=1;
	int pn=0;
	for(int i=2;i<=N;i++)
	{
		if(!f[i])
		{
			p[++pn]=i;
			f[i]=f[i-1];
		}
		for(int j=1;j<=pn&&i*p[j]<=N;j++)
		{
			f[i*p[j]]=f[i]+f[p[j]];
			if(i%p[j]==0)
				break;
		}
	}
	
	int t;
	cin>>t;
	while(t--)
	{
		int n;
		cin>>n;
		long long ans=0;
		bool flag=true;
		while(n--)
		{
			int p,q;
			cin>>p>>q;
			ans+=(long long)f[p]*q;
			if(p==2)
				flag=false;
		}
		cout<<ans+flag<<endl;
	}
	return 0;
}
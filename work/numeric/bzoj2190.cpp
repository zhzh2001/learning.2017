#include<bits/stdc++.h>
using namespace std;
int main()
{
	int n;
	cin>>n;
	int ans=0;
	for(int i=1;i<n;i++)
	{
		int phi=i,x=i;
		for(int j=2;j*j<=i;j++)
			if(x%j==0)
			{
				phi=phi/j*(j-1);
				while(x%j==0)
					x/=j;
			}
		if(x>1)
			phi=phi/x*(x-1);
		ans+=2*phi;
	}
	cout<<ans+1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
int cnt[N];
int main()
{
	int n;
	cin>>n;
	int now=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		now=max(now,x);
		for(int j=1;j*j<=x;j++)
			if(x%j==0)
			{
				cnt[j]++;
				if(j*j<x)
					cnt[x/j]++;
			}
	}
	for(int i=1;i<=n;i++)
	{
		while(cnt[now]<i)
			now--;
		cout<<now<<endl;
	}
	return 0;
}
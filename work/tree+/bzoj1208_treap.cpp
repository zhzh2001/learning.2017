#include<bits/stdc++.h>
using namespace std;
const int MOD=1000000;
struct node
{
	int val,p,cnt;
	node* ch[2];
	node(int x):val(x),p(rand()),cnt(1)
	{
		ch[0]=ch[1]=NULL;
	}
}*root=NULL;
inline void rotate(node*& o,int d)
{
	node* k=o->ch[d^1];
	o->ch[d^1]=k->ch[d];
	k->ch[d]=o;
	o=k;
}
void insert(node*& o,int x)
{
	if(!o)
		o=new node(x);
	else
		if(o->val==x)
			o->cnt++;
		else
		{
			int d=x>o->val;
			insert(o->ch[d],x);
			if(o->ch[d]->p<o->p)
				rotate(o,d^1);
		}
}
void erase(node*& o,int x,bool first)
{
	if(o->val==x)
	{
		if(first)
			o->cnt--;
		if(!o->cnt)
		{
			if(o->ch[0]&&o->ch[1])
			{
				int d=o->ch[0]->p<o->ch[1]->p;
				rotate(o,d);
				erase(o->ch[d],x,false);
			}
			else
			{
				node* k=o;
				o=o->ch[o->ch[0]?0:1];
				delete k;
			}
		}
	}
	else
		erase(o->ch[x>o->val],x,first);
}
typedef pair<int,bool> pib;
pib pred(node* o,int x)
{
	int ans;
	bool found=false;
	while(o)
	{
		if(o->val<=x)
		{
			ans=o->val;
			found=true;
		}
		o=o->ch[x>o->val];
	}
	return make_pair(ans,found);
}
pib succ(node* o,int x)
{
	int ans;
	bool found=false;
	while(o)
	{
		if(o->val>=x)
		{
			ans=o->val;
			found=true;
		}
		o=o->ch[x>o->val];
	}
	return make_pair(ans,found);
}
void print(node* o)
{
	if(o)
	{
		print(o->ch[0]);
		cout<<o->val<<' '<<o->cnt<<endl;
		print(o->ch[1]);
	}
}
int main()
{
	srand(233333);
	int n;
	cin>>n;
	bool flag;
	int ans=0,cnt=0;
	while(n--)
	{
		bool now;
		int x;
		cin>>now>>x;
		if(cnt&&now^flag)
		{
			pib x1=pred(root,x),x2=succ(root,x);
			if(!x2.second||(x1.second&&x-x1.first<=x2.first-x))
			{
				(ans+=x-x1.first)%=MOD;
				erase(root,x1.first,true);
			}
			else
			{
				(ans+=x2.first-x)%=MOD;
				erase(root,x2.first,true);
			}
			cnt--;
		}
		else
		{
			if(!cnt)
				flag=now;
			insert(root,x);
			cnt++;
		}
	}
	cout<<ans<<endl;
	return 0;
}
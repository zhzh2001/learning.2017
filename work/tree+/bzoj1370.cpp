#include<bits/stdc++.h>
using namespace std;
const int N=2005;
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=2*n;i++)
		f[i]=i;
	while(m--)
	{
		string opt;
		int x,y;
		cin>>opt>>x>>y;
		int rx=getf(x),ry=getf(y);
		if(opt=="F")
			f[rx]=ry;
		else
		{
			f[getf(y+n)]=rx;
			f[getf(x+n)]=ry;
		}
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=f[i]==i;
	cout<<ans<<endl;
	return 0;
}
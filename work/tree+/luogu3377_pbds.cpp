#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
using namespace __gnu_pbds;
const int N=100005;
struct node
{
	int id,val;
	node(int id,int val):id(id),val(val){};
	bool operator>(const node& rhs)const
	{
		if(val!=rhs.val)
			return val>rhs.val;
		return id>rhs.id;
	}
};
__gnu_pbds::priority_queue<node,greater<node> > Q[N];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
bool inQ[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		Q[i].push(node(i,x));
		f[i]=i;
	}
	memset(inQ,true,sizeof(inQ));
	while(m--)
	{
		int opr,x,y;
		cin>>opr;
		if(opr==1)
		{
			cin>>x>>y;
			int rx=getf(x),ry=getf(y);
			if(inQ[x]&&inQ[y]&&rx!=ry)
			{
				Q[rx].join(Q[ry]);
				f[ry]=rx;
			}
		}
		else
		{
			cin>>x;
			if(inQ[x])
			{
				int rx=getf(x);
				inQ[Q[rx].top().id]=false;
				cout<<Q[rx].top().val<<endl;
				Q[rx].pop();
			}
			else
				cout<<-1<<endl;
		}
	}
	return 0;
}
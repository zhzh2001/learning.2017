#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct node
{
	int ls,rs,val,dist,cnt,fat;
	long long sum;
}heap[N];
int b[N],l[N];
inline void update(int x)
{
	heap[x].sum=heap[heap[x].ls].sum+heap[heap[x].rs].sum+heap[x].val;
	heap[x].cnt=heap[heap[x].ls].cnt+heap[heap[x].rs].cnt+1;
}
int merge(int x,int y)
{
	if(!x||!y)
		return x+y;
	if(heap[x].val<heap[y].val)
		swap(x,y);
	heap[x].rs=merge(heap[x].rs,y);
	heap[heap[x].rs].fat=x;
	if(heap[heap[x].ls].dist<heap[heap[x].rs].dist)
		swap(heap[x].ls,heap[x].rs);
	heap[x].dist=heap[heap[x].rs].dist+1;
	update(x);
	return x;
}
inline int erase(int& x)
{
	x=merge(heap[x].ls,heap[x].rs);
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>b[i]>>heap[i].val>>l[i];
		heap[i].fat=i;
		heap[i].sum=heap[i].val;
		heap[i].cnt=1;
	}
	long long ans=0;
	for(int i=n;i;i--)
	{
		while(heap[heap[i].fat].sum>m)
			erase(heap[i].fat);
		ans=max(ans,(long long)heap[heap[i].fat].cnt*l[i]);
		if(b[i])
			heap[b[i]].fat=merge(heap[b[i]].fat,heap[i].fat);
	}
	cout<<ans<<endl;
	return 0;
}
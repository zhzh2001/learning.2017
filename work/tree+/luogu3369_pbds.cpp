#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
using namespace __gnu_pbds;
typedef pair<int,int> pii;
cc_hash_table<int,int> cnt;
tree<pii,null_type,less<pii>,rb_tree_tag,tree_order_statistics_node_update> T;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int opr,x;
		cin>>opr>>x;
		switch(opr)
		{
			case 1:
				T.insert(make_pair(x,++cnt[x]));
				break;
			case 2:
				T.erase(make_pair(x,cnt[x]--));
				break;
			case 3:
				cout<<T.order_of_key(make_pair(x,1))+1<<endl;
				break;
			case 4:
				cout<<T.find_by_order(x-1)->first<<endl;
				break;
			case 5:
				cout<<(--T.lower_bound(make_pair(x,1)))->first<<endl;
				break;
			case 6:
				cout<<T.upper_bound(make_pair(x,cnt[x]))->first<<endl;
				break;
		}
	}
	return 0;
}

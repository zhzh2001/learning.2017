#include<bits/stdc++.h>
using namespace std;
typedef int value_t;
typedef int addr_t;
//template from training guide
const int N=100005;
int cnt;
struct node
{
	value_t val;
	int p,size,cnt;
	addr_t ch[2];
	node(value_t x=0):val(x)
	{
		p=rand();
		size=cnt=1;
		ch[0]=ch[1]=0;
	}
	inline void update();
}tree[N];
inline void node::update()
{
	size=cnt;
	if(ch[0])
		size+=tree[ch[0]].size;
	if(ch[1])
		size+=tree[ch[1]].size;
}
inline void rotate(addr_t& o,int d)
{
	addr_t k=tree[o].ch[d^1];
	tree[o].ch[d^1]=tree[k].ch[d];
	tree[k].ch[d]=o;
	tree[o].update();tree[k].update();
	o=k;
}
void insert(addr_t& o,value_t x)
{
	if(!o)
		tree[o=++cnt]=node(x);
	else
		if(tree[o].val==x)
			tree[o].cnt++;
		else
		{
			int d=x>tree[o].val;
			insert(tree[o].ch[d],x);
			if(tree[tree[o].ch[d]].p > tree[o].p)
				rotate(o,d^1);
		}
	tree[o].update();
}
void erase(addr_t& o,value_t x,bool flag)
{
	if(tree[o].val==x)
	{
		if(flag)
			tree[o].cnt--;
		if(tree[o].cnt==0)
			if(tree[o].ch[0]&&tree[o].ch[1])
			{
				int d=tree[tree[o].ch[0]].p > tree[tree[o].ch[1]].p;
				rotate(o,d);
				erase(tree[o].ch[d],x,false);
			}
			else
				if(!tree[o].ch[0])
					o=tree[o].ch[1];
				else
					o=tree[o].ch[0];
	}
	else
		erase(tree[o].ch[x>tree[o].val],x,true);
	if(o)
		tree[o].update();
}
inline int getrank(addr_t o,value_t x)
{
	int ans=0;
	while(o)
	{
		int t=0;
		if(tree[o].ch[0])
			t=tree[tree[o].ch[0]].size;
		if(tree[o].val<x)
			ans+=t+tree[o].cnt;
		if(tree[o].val==x)
			return ans+t+1;
		o=tree[o].ch[x>tree[o].val];
	}
	return 0;
}
inline value_t findkth(addr_t o,int k)
{
	while(o)
	{
		int t=0;
		if(tree[o].ch[0])
			t=tree[tree[o].ch[0]].size;
		if(k>t&&k<=t+tree[o].cnt)
			return tree[o].val;
		if(k>t+tree[o].cnt)
		{
			k-=t+tree[o].cnt;
			o=tree[o].ch[1];
		}
		else
			o=tree[o].ch[0];
	}
}
inline value_t pred(addr_t o,value_t x)
{
	int ans=x;
	while(o)
		if(tree[o].val<x)
		{
			ans=tree[o].val;
			o=tree[o].ch[1];
		}
		else
			o=tree[o].ch[0];
	return ans;
}
inline value_t succ(addr_t o,value_t x)
{
	int ans=x;
	while(o)
		if(tree[o].val>x)
		{
			ans=tree[o].val;
			o=tree[o].ch[0];
		}
		else
			o=tree[o].ch[1];
	return ans;
}
int main()
{
	srand(time(NULL));
	int n;
	scanf("%d",&n);
	while(n--)
	{
		int opr;
		value_t x;
		scanf("%d%d",&opr,&x);
		switch(opr)
		{
			case 1:
				insert(1,x);
				break;
			case 2:
				if(getrank(1,x))
					erase(1,x,true);
				break;
			case 3:
				printf("%d\n",getrank(1,x));
				break;
			case 4:
				printf("%d\n",findkth(1,x));
				break;
			case 5:
				printf("%d\n",pred(1,x));
				break;
			case 6:
				printf("%d\n",succ(1,x));
				break;
		}
	}
	return 0;
}
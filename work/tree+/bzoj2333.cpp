#include<bits/stdc++.h>
using namespace std;
const int N=300005;
multiset<int,greater<int> > S;
#define Erase(x) S.erase(S.find(heap[x].val))
struct node
{
	int ls,rs,val,dist,fat,lazy;
}heap[N];
inline int getf(int x)
{
	while(heap[x].fat)
		x=heap[x].fat;
	return x;
}
inline void pushdown(int x)
{
	if(heap[x].lazy)
	{
		heap[heap[x].ls].val+=heap[x].lazy;
		heap[heap[x].ls].lazy+=heap[x].lazy;
		heap[heap[x].rs].val+=heap[x].lazy;
		heap[heap[x].rs].lazy+=heap[x].lazy;
		heap[x].lazy=0;
	}
}
int merge(int x,int y)
{
	if(!x||!y)
		return x+y;
	if(heap[x].val<heap[y].val)
		swap(x,y);
	pushdown(x);
	heap[x].rs=merge(heap[x].rs,y);
	heap[heap[x].rs].fat=x;
	if(heap[heap[x].ls].dist<heap[heap[x].rs].dist)
		swap(heap[x].ls,heap[x].rs);
	heap[x].dist=heap[heap[x].rs].dist+1;
	return x;
}
int st[N];
inline void clear(int x)
{
	int sp=0;
	for(;x;x=heap[x].fat)
		st[++sp]=x;
	for(;sp;sp--)
		pushdown(st[sp]);
}
inline int erase(int x)
{
	clear(x);
	int nx=merge(heap[x].ls,heap[x].rs);
	heap[x].ls=heap[x].rs=0;
	if(heap[heap[x].fat].ls==x)
		heap[heap[x].fat].ls=nx;
	else
		heap[heap[x].fat].rs=nx;
	heap[nx].fat=heap[x].fat;
	heap[x].fat=0;
	return getf(nx);
}
int main()
{
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&heap[i].val);
		S.insert(heap[i].val);
	}
	int m;
	scanf("%d",&m);
	int delta=0;
	while(m--)
	{
		char opt[5];
		scanf("%s",opt);
		int x,y;
		switch(opt[0])
		{
			case 'U':
			{
				scanf("%d%d",&x,&y);
				int rx=getf(x),ry=getf(y);
				if(rx!=ry)
				{
					if(merge(rx,ry)==rx)
						Erase(ry);
					else
						Erase(rx);
				}
				break;
			}
			case 'A':
				switch(opt[1])
				{
					case '1':
						scanf("%d%d",&x,&y);
						clear(x);
						Erase(getf(x));
						heap[x].val+=y;
						S.insert(heap[merge(x,erase(x))].val);
						break;
					case '2':
					{
						scanf("%d%d",&x,&y);
						int rx=getf(x);
						Erase(rx);
						heap[rx].val+=y;
						heap[rx].lazy+=y;
						S.insert(heap[rx].val);
						break;
					}
					case '3':
						scanf("%d",&x);
						delta+=x;
						break;
				}
				break;
			case 'F':
				switch(opt[1])
				{
					case '1':
						scanf("%d",&x);
						clear(x);
						printf("%d\n",heap[x].val+delta);
						break;
					case '2':
						scanf("%d",&x);
						printf("%d\n",heap[getf(x)].val+delta);
						break;
					case '3':
						printf("%d\n",*S.begin()+delta);
						break;
				}
				break;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
int a[N];
struct node
{
	int val,ls,rs,dist,fat,p;
}heap[N];
int merge(int x,int y)
{
	if(!x||!y)
		return x+y;
	if(heap[x].val<heap[y].val)
		swap(x,y);
	heap[x].rs=merge(heap[x].rs,y);
	if(heap[heap[x].ls].dist<heap[heap[x].rs].dist)
		swap(heap[x].ls,heap[x].rs);
	heap[x].dist=heap[heap[x].rs].dist+1;
	return x;
}
void erase(int& x)
{
	x=merge(heap[x].ls,heap[x].rs);
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int cc=0;
	for(int i=1;i<=n;i++)
	{
		heap[i].val=a[i]-i;
		for(cc++,heap[cc].fat=heap[cc].p=i;cc>1&&heap[heap[cc].fat].val<heap[heap[cc-1].fat].val;cc--)
		{
			heap[cc-1].fat=merge(heap[cc-1].fat,heap[cc].fat);
			if((heap[cc].p-heap[cc-1].p)&1&&(heap[cc-1].p-heap[cc-2].p)&1)
				erase(heap[cc-1].fat);
			heap[cc-1].p=heap[cc].p;
		}
	}
	long long ans=0;
	for(int i=1;i<=cc;i++)
		for(int j=heap[i-1].p+1;j<=heap[i].p;j++)
			ans+=abs(heap[heap[i].fat].val-heap[j].val);
	cout<<ans<<endl;
	return 0;
}
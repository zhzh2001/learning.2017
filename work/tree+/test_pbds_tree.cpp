#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
using namespace __gnu_pbds;
tree<int,null_type,less<int>,rb_tree_tag,tree_order_statistics_node_update> T;
int main()
{
	int opr,x;
	while(cin>>opr>>x)
		switch(opr)
		{
			case 1:T.insert(x);break;
			case 2:T.erase(x);break;
			case 3:T.erase(T.find(x));break;
			case 4:
				for(auto i:T)
					cout<<i<<' ';
				cout<<endl;
				break;
			default:cout<<"Invalid opration!\n";break;
		}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
vector<int> a;
int main()
{
	int n,m;
	cin>>n>>m;
	int ans=0;
	while(n--)
	{
		string opt;
		int x;
		cin>>opt>>x;
		switch(opt[0])
		{
			case 'I':
				if(x>=m)
					a.push_back(x);
				break;
			case 'A':
				for(auto& i:a)
					i+=x;
				break;
			case 'S':
				sort(a.begin(),a.end(),greater<int>());
				for(auto i=a.rbegin();a.size()&&*i-x<m;a.pop_back(),i=a.rbegin())
					ans++;
				for(auto& i:a)
					i-=x;
				break;
			case 'F':
				if(x>a.size())
					cout<<-1<<endl;
				else
				{
					sort(a.begin(),a.end(),greater<int>());
					cout<<a[x-1]<<endl;
				}
		}
	}
	cout<<ans<<endl;
	return 0;
}
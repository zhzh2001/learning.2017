#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct node
{
	pair<int,int> val;
	int ls,rs,dist;
}tree[N];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
bool inQ[N];
int merge_heap(int a,int b)
{
	if(!a)
		return b;
	if(!b)
		return a;
	if(tree[a].val>tree[b].val)
		swap(a,b);
	tree[a].rs=merge_heap(tree[a].rs,b);
	if(tree[tree[a].ls].dist<tree[tree[a].rs].dist)
		swap(tree[a].ls,tree[a].rs);
	tree[a].dist=tree[tree[a].rs].dist+1;
	return a;
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		tree[i].val=make_pair(x,i);
		tree[i].ls=tree[i].rs=0;
		f[i]=i;
	}
	memset(inQ,true,sizeof(inQ));
	while(m--)
	{
		int opr,x,y;
		cin>>opr;
		if(opr==1)
		{
			cin>>x>>y;
			int rx=getf(x),ry=getf(y);
			if(inQ[x]&&inQ[y]&&rx!=ry)
				f[rx]=f[ry]=merge_heap(rx,ry);
		}
		else
		{
			cin>>x;
			if(inQ[x])
			{
				int rx=getf(x);
				cout<<tree[rx].val.first<<endl;
				inQ[tree[rx].val.second]=false;
				f[rx]=merge_heap(tree[rx].ls,tree[rx].rs);
				f[f[rx]]=f[rx];
			}
			else
				cout<<-1<<endl;
		}
	}
	return 0;
}
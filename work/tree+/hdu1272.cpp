#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
bool vis[N];
int main()
{
	for(int i=1;i<=100000;i++)
		f[i]=i;
	int u,v;
	while(cin>>u>>v&&~u)
	{
		int low=100000,high=1;
		bool flag=true;
		while(u)
		{
			vis[u]=vis[v]=true;
			low=min(low,min(u,v));
			high=max(high,max(u,v));
			int ru=getf(u),rv=getf(v);
			if(ru==rv)
				flag=false;
			else
				f[ru]=rv;
			cin>>u>>v;
		}
		if(!flag)
			cout<<"No\n";
		else
		{
			getf(low);
			for(int i=low+1;i<=high;i++)
				if(vis[i]&&getf(i)!=f[low])
				{
					flag=false;
					break;
				}
			if(flag)
				cout<<"Yes\n";
			else
				cout<<"No\n";
		}
		for(int i=low;i<=high;i++)
		{
			f[i]=i;
			vis[i]=false;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=200005;
struct node
{
	int fat,sum;
	node(int fat=0,int sum=0):fat(fat),sum(sum){};
}f[N];
int getf(int x)
{
	if(f[x].fat==x)
		return x;
	int t=getf(f[x].fat);
	f[x].sum+=f[f[x].fat].sum;
	return f[x].fat=t;
}
int main()
{
	int n,m;
	while(cin>>n>>m)
	{
		for(int i=0;i<=n;i++)
			f[i]=node(i,0);
		int ans=0;
		while(m--)
		{
			int l,r,sum;
			cin>>l>>r>>sum;
			l--;
			int rl=getf(l),rr=getf(r);
			if(rl==rr&&f[r].sum-f[l].sum!=sum)
				ans++;
			if(rl!=rr)
			{
				f[rl].fat=rr;
				f[rl].sum=f[r].sum-f[l].sum-sum;
			}
		}
		cout<<ans<<endl;
	}
	return 0;
}
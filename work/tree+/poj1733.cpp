#include<iostream>
#include<algorithm>
#include<string>
using namespace std;
const int M=5005;
struct question
{
	int l,r;
	bool odd;
}q[M];
int a[2*M];
struct node
{
	int fat,dist;
	node(int fat=0,int dist=0):fat(fat),dist(dist){};
}f[2*M];
int getf(int x)
{
	if(f[x].fat==x)
		return x;
	int t=getf(f[x].fat);
	f[x].dist^=f[f[x].fat].dist;
	return f[x].fat=t;
}
int main()
{
	int n,m;
	cin>>n>>m;
	int cc=0;
	for(int i=1;i<=m;i++)
	{
		string s;
		cin>>q[i].l>>q[i].r>>s;
		q[i].l--;
		q[i].odd=s=="odd";
		a[++cc]=q[i].l;a[++cc]=q[i].r;
	}
	sort(a+1,a+cc+1);
	for(int i=1;i<=cc;i++)
		f[i]=node(i,0);
	for(int i=1;i<=m;i++)
	{
		int l=lower_bound(a+1,a+cc+1,q[i].l)-a,r=lower_bound(a+1,a+cc+1,q[i].r)-a,rl=getf(l),rr=getf(r);
		if(rl==rr&&((f[r].dist-f[l].dist+2)&1)^q[i].odd)
		{
			cout<<i-1<<endl;
			return 0;
		}
		if(rl!=rr)
		{
			f[rl].fat=rr;
			f[rl].dist=(f[r].dist-f[l].dist-q[i].odd+4)&1;
		}
	}
	cout<<m<<endl;
	return 0;
}
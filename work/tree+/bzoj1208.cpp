#include<bits/stdc++.h>
using namespace std;
const int MOD=1000000;
set<int> S;
int main()
{
	int n;
	cin>>n;
	bool flag;
	int ans=0;
	while(n--)
	{
		bool opt;
		int x;
		cin>>opt>>x;
		if(S.size()&&flag^opt)
		{
			set<int>::iterator i=S.lower_bound(x);
			if(i==S.begin())
			{
				ans=(ans+*i-x)%MOD;
				S.erase(i);
			}
			else
			{
				set<int>::iterator j=i;j--;
				if(i==S.end()||x-*j<=*i-x)
				{
					ans=(ans+x-*j)%MOD;
					S.erase(j);
				}
				else
				{
					ans=(ans+*i-x)%MOD;
					S.erase(i);
				}
			}
		}
		else
		{
			if(S.size()==0)
				flag=opt;
			S.insert(x);
		}
	}
	cout<<ans<<endl;
	return 0;
}
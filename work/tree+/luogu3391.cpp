#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct node
{
	int size,fat,ch[2];
	bool lazy;
}tree[N];
int root;
inline void setchild(int fat,int ch,int d)
{
	tree[ch].fat=fat;
	tree[fat].ch[d]=ch;
}
inline void update(int x)
{
	tree[x].size=tree[tree[x].ch[0]].size+tree[tree[x].ch[1]].size+1;
}
inline void pushdown(int x)
{
	if(tree[x].lazy)
	{
		swap(tree[x].ch[0],tree[x].ch[1]);
		tree[tree[x].ch[0]].lazy^=1;
		tree[tree[x].ch[1]].lazy^=1;
		tree[x].lazy=false;
	}
}
int build(int l,int r)
{
	if(l>r)
		return 0;
	int mid=(l+r)/2;
	setchild(mid,build(l,mid-1),0);
	setchild(mid,build(mid+1,r),1);
	tree[mid].lazy=false;
	update(mid);
	return mid;
}
inline int getd(int x)
{
	return tree[tree[x].fat].ch[1]==x;
}
inline void rotate(int x)
{
	int fat=tree[x].fat,d=getd(x);
	if(tree[x].fat==root)
	{
		root=x;
		tree[x].fat=0;
	}
	else
		setchild(tree[fat].fat,x,getd(fat));
	setchild(fat,tree[x].ch[d^1],d);
	setchild(x,fat,d^1);
	update(fat);
	update(x);
}
void splay(int x,int root)
{
	for(int fat=tree[x].fat;fat!=root;fat=tree[x].fat)
		if(tree[fat].fat==root)
			rotate(x);
		else
		{
			rotate(getd(x)==getd(fat)?fat:x);
			rotate(x);
		}
}
int findkth(int root,int k)
{
	int x;
	for(x=root;pushdown(x),k!=tree[tree[x].ch[0]].size;)
		if(k<=tree[tree[x].ch[0]].size)
			x=tree[x].ch[0];
		else
		{
			k-=tree[tree[x].ch[0]].size+1;
			x=tree[x].ch[1];
		}
	return x;
}
int n;
void print(int x)
{
	if(x)
	{
		pushdown(x);
		print(tree[x].ch[0]);
		if(x>1&&x<n+2)
			cout<<x-1<<' ';
		print(tree[x].ch[1]);
	}
}
int main()
{
	int m;
	cin>>n>>m;
	root=build(1,n+2);
	while(m--)
	{
		int l,r;
		cin>>l>>r;
		splay(findkth(root,l-1),0);
		int tr=findkth(root,r+1);
		splay(tr,root);
		tree[tree[tr].ch[0]].lazy^=1;
	}
	print(root);
	cout<<endl;
	return 0;
}
#include<bits/stdc++.h>
#include<bits/extc++.h>
using namespace std;
using namespace __gnu_pbds;
const int M=100005;
typedef pair<int,int> pii;
typedef tree<pii,null_type,greater<pii>,rb_tree_tag,tree_order_statistics_node_update> tree_t;
tree_t T,TE;
void debug(const tree_t& T)
{
	for(auto i:T)
		cout<<i.first<<' '<<i.second<<endl;
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	int delta=0,ans=0,id=0;
	while(n--)
	{
		char opt;
		for(opt=getchar();isspace(opt);opt=getchar());
		int x;
		scanf("%d",&x);
		switch(opt)
		{
			case 'I':
				if(x>=m)
					T.insert(make_pair(x-delta,++id));
				break;
			case 'A':
				delta+=x;
				break;
			case 'S':
				delta-=x;
				T.split(make_pair(m-delta,0),TE);
				ans+=TE.size();
				break;
			case 'F':
				tree_t::iterator i=T.find_by_order(x-1);
				if(i==T.end())
					puts("-1");
				else
					printf("%d\n",i->first+delta);
		}
	}
	printf("%d\n",ans);
	return 0;
}
    program p1367;  
      
    var a   :array[0..1000001]of record lt,rt,key,dist:longint; end;  
        root,p                   :array[0..1000001]of longint;  
        tot,tmp,n,i,j            :longint;  
        ans                      :int64;  
      
    function merge(x,y:longint):longint;  
    begin  
         if (x=0)or(y<>0)and(a[x].key<a[y].key) then  
         begin  
              tmp:=x;  
              x:=y;  
              y:=tmp;  
         end;  
         merge:=x;  
         if y=0 then exit;  
         a[x].rt:=merge(a[x].rt,y);  
         if a[a[x].lt].dist<a[a[x].rt].dist then  
         begin  
              tmp:=a[x].lt;  
              a[x].lt:=a[x].rt;  
              a[x].rt:=tmp;  
         end;  
         a[x].dist:=a[a[x].rt].dist+1;  
    end;  
      
    begin  
         readln(n);  
         for i:=1 to n do  
         begin  
              readln(a[i].key);  
              dec(a[i].key,i);  
              inc(Tot);  
              root[tot]:=i;  
              p[tot]:=i;  
              while (tot>1)and(a[root[tot]].key<a[root[tot-1]].key) do  
              begin  
                   dec(tot);  
                   root[tot]:=merge(root[tot],root[tot+1]);  
                   if odd(p[tot+1]-p[tot]) and odd(p[tot]-p[tot-1]) then  
                   root[tot]:=merge(a[root[tot]].lt,a[root[tot]].rt);  
                   p[tot]:=p[tot+1];  
              end;  
         end;  
		 for i:=1 to n do
		   writeln(a[i].key);
         for i:=1 to tot do  
         begin  
              for j:=p[i-1]+1 to p[i] do  
              ans:=ans+int64(abs(a[root[i]].key-a[j].key));  
         end;  
         writeln(ans);  
    end.  
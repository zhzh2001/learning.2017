#include<bits/stdc++.h>
#include<ext/rope>
using namespace std;
using namespace __gnu_cxx;
const int N=200005;
int a[N];
rope<int> *f[N];
int getf(int id,int x)
{
	if((*f[id])[x]==x)
		return x;
	int rt=getf(id,(*f[id])[x]);
	if((*f[id])[x]!=rt)
		f[id]->replace(x,rt);
	return rt;
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		a[i]=i;
	f[0]=new rope<int>(a,a+n+1);
	int lastans=0;
	for(int i=1;i<=m;i++)
	{
		int opt,x,y,rx,ry;
		scanf("%d",&opt);
		switch(opt)
		{
			case 1:
				scanf("%d%d",&x,&y);
				x^=lastans;y^=lastans;
				f[i]=f[i-1];
				rx=getf(i,x);ry=getf(i,y);
				if(rx!=ry)
				{
					f[i]=new rope<int>(*f[i-1]);
					f[i]->replace(rx,ry);
				}
				break;
			case 2:
				scanf("%d",&x);
				x^=lastans;
				f[i]=f[x];
				break;
			case 3:
				scanf("%d%d",&x,&y);
				x^=lastans;y^=lastans;
				f[i]=f[i-1];
				rx=getf(i,x);ry=getf(i,y);
				printf("%d\n",lastans=rx==ry);
				break;
		}
	}
	return 0;
}
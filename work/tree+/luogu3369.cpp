#include<bits/stdc++.h>
using namespace std;
typedef int value_t;
//template from training guide
struct node
{
	value_t val;
	int p,size,cnt;
	node* ch[2];
	node(value_t x=0):val(x)
	{
		p=rand();
		size=cnt=1;
		ch[0]=ch[1]=nullptr;
	}
	inline void update()
	{
		size=cnt;
		if(ch[0]!=nullptr)
			size+=ch[0]->size;
		if(ch[1]!=nullptr)
			size+=ch[1]->size;
	}
};
inline void rotate(node*& o,int d)
{
	node* k=o->ch[d^1];
	o->ch[d^1]=k->ch[d];
	k->ch[d]=o;
	o->update();k->update();
	o=k;
}
void insert(node*& o,value_t x)
{
	if(o==nullptr)
		o=new node(x);
	else
		if(o->val==x)
			o->cnt++;
		else
		{
			int d=x>o->val;
			insert(o->ch[d],x);
			if(o->ch[d]->p > o->p)
				rotate(o,d^1);
		}
	o->update();
}
void erase(node*& o,value_t x,bool flag)
{
	if(o->val==x)
	{
		if(flag)
			o->cnt--;
		if(o->cnt==0)
			if(o->ch[0]!=nullptr&&o->ch[1]!=nullptr)
			{
				int d=o->ch[0]->p > o->ch[1]->p;
				rotate(o,d);
				erase(o->ch[d],x,false);
			}
			else
			{
				node* t=o;
				if(o->ch[0]==nullptr)
					o=o->ch[1];
				else
					o=o->ch[0];
				delete t;
			}
	}
	else
		erase(o->ch[x>o->val],x,true);
	if(o!=nullptr)
		o->update();
}
inline int getrank(node* o,value_t x)
{
	int ans=0;
	while(o!=nullptr)
	{
		int t=0;
		if(o->ch[0]!=nullptr)
			t=o->ch[0]->size;
		if(o->val<x)
			ans+=t+o->cnt;
		if(o->val==x)
			return ans+t+1;
		o=o->ch[x>o->val];
	}
	return 0;
}
inline value_t findkth(node* o,int k)
{
	while(o!=nullptr)
	{
		int t=0;
		if(o->ch[0]!=nullptr)
			t=o->ch[0]->size;
		if(k>t&&k<=t+o->cnt)
			return o->val;
		if(k>t+o->cnt)
		{
			k-=t+o->cnt;
			o=o->ch[1];
		}
		else
			o=o->ch[0];
	}
}
inline value_t pred(node* o,value_t x)
{
	int ans=x;
	while(o!=nullptr)
		if(o->val<x)
		{
			ans=o->val;
			o=o->ch[1];
		}
		else
			o=o->ch[0];
	return ans;
}
inline value_t succ(node* o,value_t x)
{
	int ans=x;
	while(o!=nullptr)
		if(o->val>x)
		{
			ans=o->val;
			o=o->ch[0];
		}
		else
			o=o->ch[1];
	return ans;
}
int main()
{
	srand(time(nullptr));
	int n;
	scanf("%d",&n);
	node* root=nullptr;
	while(n--)
	{
		int opr;
		value_t x;
		scanf("%d%d",&opr,&x);
		switch(opr)
		{
			case 1:
				insert(root,x);
				break;
			case 2:
				if(getrank(root,x))
					erase(root,x,true);
				break;
			case 3:
				printf("%d\n",getrank(root,x));
				break;
			case 4:
				printf("%d\n",findkth(root,x));
				break;
			case 5:
				printf("%d\n",pred(root,x));
				break;
			case 6:
				printf("%d\n",succ(root,x));
				break;
		}
	}
	return 0;
}
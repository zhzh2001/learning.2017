#include <iostream>
#include <vector>
#include <bitset>
#include <queue>
#include <algorithm>
using namespace std;
const int N = 20005;
vector<int> mat[N], rmat[N];
bitset<N> desc[N];
int into[N], topo[N], rtopo[N];
bool cmp(int x, int y)
{
	return rtopo[x] > rtopo[y];
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n, m;
		cin >> n >> m;
		for (int i = 1; i <= n; i++)
		{
			mat[i].clear();
			rmat[i].clear();
			desc[i].reset();
			desc[i][i] = true;
		}
		fill(into + 1, into + n + 1, 0);
		while (m--)
		{
			int u, v;
			cin >> u >> v;
			mat[u].push_back(v);
			rmat[v].push_back(u);
			into[v]++;
		}
		queue<int> Q;
		for (int i = 1; i <= n; i++)
			if (!into[i])
				Q.push(i);
		int cc = 0;
		while (!Q.empty())
		{
			topo[++cc] = Q.front();
			rtopo[Q.front()] = cc;
			Q.pop();
			for (vector<int>::iterator it = mat[topo[cc]].begin(); it != mat[topo[cc]].end(); ++it)
				if (!--into[*it])
					Q.push(*it);
		}
		int ans = 0;
		for (int i = 1; i <= n; i++)
		{
			sort(rmat[topo[i]].begin(), rmat[topo[i]].end(), cmp);
			for (vector<int>::iterator it = rmat[topo[i]].begin(); it != rmat[topo[i]].end(); ++it)
			{
				ans += desc[topo[i]][*it];
				desc[topo[i]] |= desc[*it];
			}
		}
		cout << ans << endl;
	}
	return 0;
}
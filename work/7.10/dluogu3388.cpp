#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 100005;
vector<int> mat[N];
int now, dfn[N], low[N], f[N], iscut[N];
void dfs(int k)
{
	low[k] = dfn[k] = ++now;
	int cnt = 0;
	for (auto v : mat[k])
		if (!dfn[v])
		{
			cnt++;
			f[v] = k;
			dfs(v);
			low[k] = min(low[k], low[v]);
			if (f[k] == 0 && cnt > 1)
				iscut[k] = true;
			if (f[k] && low[v] >= dfn[k])
				iscut[k] = true;
		}
		else if (v != f[k])
			low[k] = min(low[k], dfn[v]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for (int i = 1; i <= n; i++)
		if (!dfn[i])
			dfs(i);
	cout << count(iscut + 1, iscut + n + 1, true) << endl;
	for (int i = 1; i <= n; i++)
		if (iscut[i])
			cout << i << ' ';
	cout << endl;
	return 0;
}
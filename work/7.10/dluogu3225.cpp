#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 505;
vector<int> mat[N];
int now, dfn[N], low[N], f[N], vis[N], ccut, cnt;
bool cut[N];
void dfs(int k)
{
	low[k] = dfn[k] = ++now;
	int cnt = 0;
	for (auto v : mat[k])
		if (!dfn[v])
		{
			f[v] = k;
			cnt++;
			dfs(v);
			low[k] = min(low[k], low[v]);
			if (f[k] == 0 && cnt > 1)
				cut[k] = true;
			if (f[k] && low[v] >= dfn[k])
				cut[k] = true;
		}
		else if (v != f[k])
			low[k] = min(low[k], dfn[v]);
}
void dfs(int k, int id)
{
	vis[k] = id;
	if (cut[k])
	{
		ccut++;
		return;
	}
	cnt++;
	for (auto v : mat[k])
		if (vis[v] < id)
			dfs(v, id);
}
int main()
{
	ios::sync_with_stdio(false);
	int m, cas = 0;
	while (cin >> m && m)
	{
		int n = 0;
		for (int i = 1; i <= m; i++)
			mat[i].clear();
		while (m--)
		{
			int u, v;
			cin >> u >> v;
			n = max(n, max(u, v));
			mat[u].push_back(v);
			mat[v].push_back(u);
		}
		fill(dfn + 1, dfn + n + 1, 0);
		fill(cut + 1, cut + n + 1, false);
		fill(f + 1, f + n + 1, 0);
		for (int i = 1; i <= n; i++)
			if (!dfn[i])
				dfs(i);
		fill(vis + 1, vis + n + 1, 0);
		int id = 0, least = 0;
		unsigned long long cc = 1;
		for (int i = 1; i <= n; i++)
			if (!vis[i] && !cut[i])
			{
				cnt = ccut = 0;
				dfs(i, ++id);
				if (!ccut)
				{
					least += 2;
					cc *= cnt * (cnt - 1) / 2;
				}
				else if (ccut == 1)
				{
					least++;
					cc *= cnt;
				}
			}
		cout << "Case " << ++cas << ": " << least << ' ' << cc << endl;
	}
	return 0;
}
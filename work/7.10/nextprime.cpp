#include <iostream>
using namespace std;
int main()
{
	int p;
	cin >> p;
	for (;; p++)
	{
		bool flag = true;
		for (int i = 2; i * i <= p; i++)
			if (p % i == 0)
			{
				flag = false;
				break;
			}
		if (flag)
		{
			cout << p << endl;
			break;
		}
	}
	return 0;
}
#include <cstdio>
#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
#define ll long long
#define ui unsigned int
#define ull unsigned long long
const int maxn = 233, inf = 1002333333;
struct zs1
{
	int too, pre;
} e1[23333], e2[23333];
int tot1, last1[maxn], tot2, last2[maxn];
struct zs
{
	int id;
	ull v;
} a1[2][maxn], a2[2][maxn];
int p1[maxn], p2[maxn];
int i, j, k, n, m;

int ra, fh;
char rx;
inline int read()
{
	rx = getchar(), ra = 0, fh = 1;
	while ((rx < '0' || rx > '9') && rx != '-')
		rx = getchar();
	if (rx == '-')
		fh = -1, rx = getchar();
	while (rx >= '0' && rx <= '9')
		ra *= 10, ra += rx - 48, rx = getchar();
	return ra * fh;
}

inline void ins1(int a, int b)
{
	e1[++tot1].too = b, e1[tot1].pre = last1[a], last1[a] = tot1;
	e1[++tot1].too = a, e1[tot1].pre = last1[b], last1[b] = tot1;
}
inline void ins2(int a, int b)
{
	e2[++tot2].too = b, e2[tot2].pre = last2[a], last2[a] = tot2;
	e2[++tot2].too = a, e2[tot2].pre = last2[b], last2[b] = tot2;
}

bool operator<(zs a, zs b) { return a.v < b.v; }
int main()
{
	register int i, j;
	ull k;
	for (int T = read(); T; T--)
	{
		tot1 = tot2 = 0, memset(last1 + 1, 0, n << 2);
		memset(last2 + 1, 0, n << 2);
		n = read(), m = read();
		for (i = 1; i <= m; i++)
			ins1(read(), read());
		for (i = 1; i <= m; i++)
			ins2(read(), read());
		for (i = 1; i <= n; i++)
			a1[0][i] = a2[0][i] = (zs){i, 1ull}, ins1(i, i), ins2(i, i), a1[1][i].id = a2[1][i].id = p1[i] = p2[i] = i;
		bool now = 1, pre = 0;
		int p;
		for (p = n; p; p--, now ^= 1, pre ^= 1)
		{ //printf("now,pre:%d %d\n",now,pre);
			for (i = 1; i <= n; i++)
				a1[now][i].v = a2[now][i].v = 0; //,printf("    %llu %llu\n",a1[pre][i].v,a2[pre][i].v);
			for (i = 1; i <= n; i++)
				p1[a1[now][i].id] = p2[a2[now][i].id] = i;
			for (i = 1; i <= n; i++)
			{
				for (j = last1[a1[pre][i].id], k = a1[pre][i].v /*,printf("   %llu\n",k)*/; j; j = e1[j].pre)
					(a1[now][p1[e1[j].too]].v *= 2333ull) += k;
				for (j = last2[a2[pre][i].id], k = a2[pre][i].v; j; j = e2[j].pre)
					(a2[now][p2[e2[j].too]].v *= 2333ull) += k;
			}
			std::sort(a1[now] + 1, a1[now] + 1 + n),
				std::sort(a2[now] + 1, a2[now] + 1 + n);
			for (i = 1; i <= n && a1[now][i].v == a2[now][i].v; i++)
				; //printf("        %llu %llu\n",a1[now][i].v,a2[now][i].v);
			if (i <= n)
				break;
		}
		puts(!p ? "YES" : "NO");
	}
}
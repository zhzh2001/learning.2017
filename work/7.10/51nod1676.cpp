#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 205, A = 233, B = 331;
vector<int> mat[N];
typedef unsigned long long hash_t;
hash_t f[N][N], g[N][N];
void calcHash(int n, int m)
{
	for (int i = 1; i <= n; i++)
		mat[i].clear();
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	fill(f[0] + 1, f[0] + n + 1, 1);
	for (int i = 1; i <= n; i++)
		for (int v = 1; v <= n; v++)
		{
			f[i][v] = f[i - 1][v] * A;
			for (vector<int>::iterator w = mat[v].begin(); w != mat[v].end(); ++w)
				f[i][v] += f[i - 1][*w] * B;
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n, m;
		cin >> n >> m;
		calcHash(n, m);
		memcpy(g, f, sizeof(g));
		calcHash(n, m);
		bool same = true;
		for (int i = 1; i <= n; i++)
		{
			sort(f[i] + 1, f[i] + n + 1);
			sort(g[i] + 1, g[i] + n + 1);
			for (int j = 1; j <= n; j++)
				if (f[i][j] != g[i][j])
				{
					same = false;
					break;
				}
			if (!same)
				break;
		}
		if (same)
			cout << "YES\n";
		else
			cout << "NO\n";
	}
	return 0;
}
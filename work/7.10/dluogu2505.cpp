#include <iostream>
#include <vector>
#include <utility>
#include <queue>
using namespace std;
const int N = 1505, M = 5005, MOD = 1000000007, INF = 0x3f3f3f3f;
int n, m, d[N], head[N], v[M], w[M], nxt[M], e, ans[M], f[N], g[N], into[N];
inline void add_edge(int u, int v, int w)
{
	::v[++e] = v;
	::w[e] = w;
	nxt[e] = head[u];
	head[u] = e;
}
bool vis[N];
void dijkstra(int s)
{
	fill(d + 1, d + n + 1, INF);
	d[s] = 0;
	fill(vis + 1, vis + n + 1, false);
	typedef pair<int, int> pii;
	priority_queue<pii, vector<pii>, greater<pii>> Q;
	Q.push(make_pair(0, s));
	while (!Q.empty())
	{
		pii k = Q.top();
		Q.pop();
		if (vis[k.second])
			continue;
		vis[k.second] = true;
		for (int i = head[k.second]; i; i = nxt[i])
			if (d[k.second] + w[i] < d[v[i]])
				Q.push(make_pair(d[v[i]] = d[k.second] + w[i], v[i]));
	}
}
void calcInto(int k)
{
	vis[k] = true;
	for (int i = head[k]; i; i = nxt[i])
		if (d[v[i]] == d[k] + w[i])
		{
			into[v[i]]++;
			if (!vis[v[i]])
				calcInto(v[i]);
		}
}
void calcF(int k)
{
	for (int i = head[k]; i; i = nxt[i])
		if (d[v[i]] == d[k] + w[i])
		{
			f[v[i]] = (f[v[i]] + f[k]) % MOD;
			if (!--into[v[i]])
				calcF(v[i]);
		}
}
void calcG(int k)
{
	g[k] = 1;
	for (int i = head[k]; i; i = nxt[i])
		if (d[v[i]] == d[k] + w[i])
		{
			if (!g[v[i]])
				calcG(v[i]);
			g[k] = (g[k] + g[v[i]]) % MOD;
		}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	while (m--)
	{
		int u, v, w;
		cin >> u >> v >> w;
		add_edge(u, v, w);
	}
	for (int i = 1; i <= n; i++)
	{
		dijkstra(i);
		fill(into + 1, into + n + 1, 0);
		fill(vis + 1, vis + n + 1, false);
		calcInto(i);
		fill(f + 1, f + n + 1, 0);
		f[i] = 1;
		calcF(i);
		fill(g + 1, g + n + 1, 0);
		calcG(i);
		for (int j = 1; j <= n; j++)
			for (int k = head[j]; k; k = nxt[k])
				if (d[v[k]] == d[j] + w[k])
					ans[k] = (ans[k] + 1ll * f[j] * g[v[k]]) % MOD;
	}
	for (int i = 1; i <= e; i++)
		cout << ans[i] << endl;
	return 0;
}
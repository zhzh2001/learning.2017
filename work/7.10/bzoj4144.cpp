#include <stdio.h>
#include <vector>
#include <utility>
#include <queue>
#include <algorithm>
using namespace std;
const int N = 200005, INF = 0x3f3f3f3f;
typedef pair<int, int> pii;
vector<pii> mat[N];
int d[N], f[N];
bool vis[N], ans[N];
struct edge
{
	int u, v, w, id;
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} e[N], q[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	int n, k, m;
	scanf("%d%d%d", &n, &k, &m);
	fill(d + 1, d + n + 1, INF);
	priority_queue<pii, vector<pii>, greater<pii> > Q;
	while (k--)
	{
		int u;
		scanf("%d", &u);
		d[u] = 0;
		Q.push(make_pair(0, u));
	}
	for (int i = 1; i <= m; i++)
	{
		scanf("%d%d%d", &e[i].u, &e[i].v, &e[i].w);
		mat[e[i].u].push_back(make_pair(e[i].v, e[i].w));
		mat[e[i].v].push_back(make_pair(e[i].u, e[i].w));
	}
	while (!Q.empty())
	{
		pii k = Q.top();
		Q.pop();
		if (vis[k.second])
			continue;
		vis[k.second] = true;
		for (vector<pii>::iterator it = mat[k.second].begin(); it != mat[k.second].end(); ++it)
			if (d[k.second] + it->second < d[it->first])
				Q.push(make_pair(d[it->first] = d[k.second] + it->second, it->first));
	}
	for (int i = 1; i <= m; i++)
		e[i].w += d[e[i].u] + d[e[i].v];
	sort(e + 1, e + m + 1);
	scanf("%d", &k);
	for (int i = 1; i <= k; i++)
	{
		scanf("%d%d%d", &q[i].u, &q[i].v, &q[i].w);
		q[i].id = i;
	}
	sort(q + 1, q + k + 1);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	int j = 1;
	for (int i = 1; i <= k; i++)
	{
		for (; j <= m && e[j].w <= q[i].w; j++)
			f[getf(e[j].u)] = getf(e[j].v);
		ans[q[i].id] = getf(q[i].u) == getf(q[i].v);
	}
	for (int i = 1; i <= k; i++)
		if (ans[i])
			puts("TAK");
		else
			puts("NIE");
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, M = 700005;
int head[N], v[M], tag[M], nxt[M], e, deg[N];
inline void add_edge(int u, int v)
{
	::v[e] = v;
	tag[e] = -1;
	nxt[e] = head[u];
	head[u] = e++;
}
void dfs(int k)
{
	for (int &i = head[k]; ~i; i = nxt[i])
		if (tag[i] == -1)
		{
			tag[i] = 1;
			tag[i ^ 1] = 0;
			dfs(v[i]);
		}
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	fill(head + 1, head + n + 1, -1);
	for (int i = 1; i <= m; i++)
	{
		int u, v;
		cin >> u >> v;
		add_edge(u, v);
		add_edge(v, u);
		deg[u]++;
		deg[v]++;
	}
	int cnt = 0, pred = -1;
	for (int i = 1; i <= n; i++)
		if (deg[i] & 1)
			if (~pred)
			{
				add_edge(pred, i);
				add_edge(i, pred);
				pred = -1;
			}
			else
				pred = i;
		else
			cnt++;
	cout << cnt << endl;
	for (int i = 1; i <= n; i++)
		dfs(i);
	for (int i = 0; i < m * 2; i += 2)
		cout << tag[i + 1];
	cout << endl;
	return 0;
}
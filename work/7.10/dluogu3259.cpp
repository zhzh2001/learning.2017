#include <iostream>
#include <unordered_map>
#include <string>
#include <vector>
#include <queue>
using namespace std;
const int N = 110005, G = 55;
const double INF = 1e100;
int n, m, k, limit, cost, s, t, gas[G];
double wait[N], d[N];
typedef pair<double, int> pii;
vector<pair<int, double>> mat[N], newmat[N];
void add_edge(int u, int v, int w)
{
	if (wait[u])
		for (int i = 0; i < n * k; i += n)
			mat[i + u].push_back(make_pair(i + n + v, wait[u] + w));
	else
		for (int i = 0; i <= n * k; i += n)
			mat[i + u].push_back(make_pair(i + v, w));
}
bool vis[N];
void dijkstra(vector<pair<int, double>> mat[], int s)
{
	fill(d + 1, d + n * (k + 1) + 1, INF);
	fill(vis + 1, vis + n * (k + 1) + 1, false);
	priority_queue<pii, vector<pii>, greater<pii>> Q;
	d[s] = .0;
	Q.push(make_pair(.0, s));
	while (!Q.empty())
	{
		pair<int, int> k = Q.top();
		Q.pop();
		if (vis[k.second])
			continue;
		vis[k.second] = true;
		for (auto e : mat[k.second])
			if (d[k.second] + e.second < d[e.first])
				Q.push(make_pair(d[e.first] = d[k.second] + e.second, e.first));
	}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m >> k >> limit >> cost;
	unordered_map<string, int> M;
	int g = 0;
	for (int i = 1; i <= n; i++)
	{
		string name;
		int red, green;
		cin >> name >> red >> green;
		M[name] = i;
		if (name == "start")
			s = i;
		else if (name == "end")
			t = i;
		else if (name.find("gas") != string::npos)
			gas[++g] = i;
		if (red)
			wait[i] = red * red / 2.0 / (red + green);
		else
			wait[i] = .0;
	}
	gas[++g] = s;
	gas[++g] = t;
	while (m--)
	{
		string su, sv, useless;
		int w;
		cin >> su >> sv >> useless >> w;
		int u = M[su], v = M[sv];
		add_edge(u, v, w);
		add_edge(v, u, w);
	}
	for (int i = 1; i <= g; i++)
	{
		dijkstra(mat, gas[i]);
		for (int j = 1; j <= g; j++)
			if (j != i)
			{
				double c = gas[j] != s && gas[j] != t ? cost : 0;
				for (int t = 0; t <= n * k; t += n)
					if (d[t + gas[j]] <= limit)
						for (int u = 0; u + t <= n * k; u += n)
							newmat[u + gas[i]].push_back(make_pair(u + t + gas[j], d[t + gas[j]] + c));
			}
	}
	dijkstra(newmat, s);
	double ans = INF;
	for (int i = 0; i <= n * k; i += n)
		ans = min(ans, d[i + t]);
	cout.precision(3);
	cout << fixed << ans << endl;
	return 0;
}
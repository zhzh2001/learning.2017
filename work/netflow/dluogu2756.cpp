#include<bits/stdc++.h>
using namespace std;
const int N=105;
bool mat[N][N],vis[N];
int m,n,match[N];
bool dfs(int k)
{
	for(int i=m+1;i<=n;i++)
		if(mat[k][i]&&!vis[i])
		{
			vis[i]=true;
			if(!match[i]||dfs(match[i]))
			{
				match[i]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	cin>>m>>n;
	int u,v;
	while(cin>>u>>v&&~u)
		mat[u][v]=true;
	int ans=0;
	for(int i=1;i<=m;i++)
	{
		memset(vis,false,sizeof(vis));
		ans+=dfs(i);
	}
	cout<<ans<<endl;
	for(int i=m+1;i<=n;i++)
		if(match[i])
			cout<<match[i]<<' '<<i<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){}
};
vector<node> mat[N];
vector<int> lst,ans[2];
bool vis[N],c[N];
int dfs(int k,bool nc,bool x)
{
	vis[k]=true;
	int ret=c[k]=nc;
	lst.push_back(k);
	for(vector<node>::iterator it=mat[k].begin();it!=mat[k].end();it++)
	{
		if(!vis[it->v])
		{
			int t=dfs(it->v,nc^it->w^x,x);
			if(t==-1)
				return -1;
			ret+=t;
		}
		if(nc^c[it->v]^x^it->w)
			return -1;
	}
	return ret;
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		char c;
		cin>>u>>v>>c;
		mat[u].push_back(node(v,c=='R'));
		mat[v].push_back(node(u,c=='R'));
	}
	for(int x=0;x<2;x++)
	{
		memset(vis,false,sizeof(vis));
		for(int i=1;i<=n;i++)
			if(!vis[i])
			{
				lst.clear();
				int cnt=dfs(i,true,x);
				if(cnt==-1)
				{
					ans[x].resize(n+1);
					break;
				}
				bool tar=cnt*2<=lst.size();
				for(vector<int>::iterator it=lst.begin();it!=lst.end();it++)
					if(c[*it]==tar)
						ans[x].push_back(*it);
			}
	}
	if(ans[0].size()==n+1&&ans[1].size()==n+1)
	{
		cout<<-1<<endl;
		return 0;
	}
	int id=ans[1].size()<ans[0].size();
	cout<<ans[id].size()<<endl;
	for(vector<int>::iterator it=ans[id].begin();it!=ans[id].end();it++)
		cout<<*it<<' ';
	cout<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=40005,INF=0x3f3f3f3f;
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> e;
int head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	e.push_back(edge(v,cap,head[u]));
	e.push_back(edge(u,0,head[v]));
	head[u]=e.size()-2;
	head[v]=e.size()-1;
}
int t,d[N];
bool bfs()
{
	memset(d,0,sizeof(d));
	d[0]=1;
	queue<int> Q;
	Q.push(0);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];~i;i=e[i].nxt)
			if(!d[e[i].v]&&e[i].cap)
			{
				d[e[i].v]=d[k]+1;
				Q.push(e[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=e[i].nxt)
		if(d[e[i].v]==d[k]+1&&(t=dfs(e[i].v,min(pred,e[i].cap)))>0)
		{
			e[i].cap-=t;
			e[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	int n1,n2,n3;
	cin>>n1>>n2>>n3;
	memset(head,-1,sizeof(head));
	for(int i=1;i<=n2;i++)
		add_edge(0,i,1);
	for(int i=1;i<=n1;i++)
		add_edge(n2+i,n2+n1+i,1);
	t=n2+n1+n1+n3+1;
	for(int i=1;i<=n3;i++)
		add_edge(n2+n1+n1+i,t,1);
	int m;
	cin>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(v,u+n2,1);
	}
	cin>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u+n2+n1,v+n2+n1+n1,1);
	}
	int ans=0;
	while(bfs())
	{
		memcpy(H,head,sizeof(H));
		ans+=dfs(0,INF);
	}
	cout<<ans<<endl;
	return 0;
}
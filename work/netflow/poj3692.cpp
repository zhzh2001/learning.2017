#include<iostream>
#include<cstring>
using namespace std;
const int N=205;
bool mat[N][N],vis[N];
int n,m,e,match[N];
bool dfs(int k)
{
	for(int i=1;i<=m;i++)
		if(mat[k][i]&&!vis[i])
		{
			vis[i]=true;
			if(!match[i]||dfs(match[i]))
			{
				match[i]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	for(int t=1;cin>>n>>m>>e&&n;t++)
	{
		memset(mat,true,sizeof(mat));
		while(e--)
		{
			int u,v;
			cin>>u>>v;
			mat[u][v]=false;
		}
		memset(match,0,sizeof(match));
		int ans=n+m;
		for(int i=1;i<=n;i++)
		{
			memset(vis,false,sizeof(vis));
			ans-=dfs(i);
		}
		cout<<"Case "<<t<<": "<<ans<<endl;
	}
	return 0;
}
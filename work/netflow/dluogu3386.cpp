#include<bits/stdc++.h>
using namespace std;
const int N=2005,INF=0x3f3f3f3f;
struct node
{
	int v,cap,nxt;
	node(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<node> E;
int head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(node(v,cap,head[u]));
	head[u]=E.size()-1;
}
int n,m,d[N];
bool bfs()
{
	fill(d,d+n+m+2,0);
	d[0]=1;
	queue<int> Q;
	Q.push(0);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==n+m+1)
			return true;
		for(int i=head[k];~i;i=E[i].nxt)
			if(!d[E[i].v]&&E[i].cap)
			{
				d[E[i].v]=d[k]+1;
				Q.push(E[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==n+m+1)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=E[i].nxt)
		if(d[E[i].v]==d[k]+1&&(t=dfs(E[i].v,min(pred,E[i].cap)))>0)
		{
			E[i].cap-=t;
			E[i^1].cap+=t;
			flow+=t;
			if((pred-=t)==0)
				break;
		}
	return flow;
}
int main()
{
	int e;
	scanf("%d%d%d",&n,&m,&e);
	fill(head,head+n+m+2,-1);
	while(e--)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		if(u>n||v>m)
			continue;
		add_edge(u,v+n,1);
		add_edge(v+n,u,0);
	}
	for(int i=1;i<=n;i++)
	{
		add_edge(0,i,1);
		add_edge(i,0,0);
	}
	for(int i=1;i<=m;i++)
	{
		add_edge(n+i,n+m+1,1);
		add_edge(n+m+1,n+i,0);
	}
	int ans=0;
	while(bfs())
	{
		copy(head,head+n+m+2,H);
		ans+=dfs(0,INF);
	}
	printf("%d\n",ans);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=200005,INF=0x3f3f3f3f;
struct edge
{
	int v,cap,nxt;
}e[M];
int head[N],cc;
inline void add_edge(int u,int v,int cap)
{
	e[++cc].v=v;
	e[cc].cap=cap;
	e[cc].nxt=head[u];
	head[u]=cc;
}
int s,t,d[N];
bool bfs()
{
	memset(d,0,sizeof(d));
	d[s]=1;
	queue<int> Q;
	Q.push(s);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];i;i=e[i].nxt)
			if(e[i].cap&&!d[e[i].v])
			{
				d[e[i].v]=d[k]+1;
				Q.push(e[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t)
		return pred;
	int flow=0,t;
	for(int i=head[k];i;i=e[i].nxt)
		if(d[e[i].v]==d[k]+1&&(t=dfs(e[i].v,min(pred,e[i].cap)))>0)
		{
			e[i].cap-=t;
			e[((i-1)^1)+1].cap+=t;
			flow+=t;
			if((pred-=t)==0)
				break;
		}
	return flow;
}
int main()
{
	int n,m;
	cin>>n>>m>>s>>t;
	while(m--)
	{
		int u,v,cap;
		cin>>u>>v>>cap;
		add_edge(u,v,cap);
		add_edge(v,u,0);
	}
	int ans=0;
	while(bfs())
		ans+=dfs(s,INF);
	cout<<ans<<endl;
	return 0;
}
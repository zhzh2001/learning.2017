#include<bits/stdc++.h>
using namespace std;
const int N=100005,MOD=1000000007;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){}
};
vector<node> mat[N];
bool vis[N],c[N];
bool dfs(int k,bool now)
{
	vis[k]=true;
	c[k]=now;
	for(auto e:mat[k])
	{
		if(!vis[e.v]&&!dfs(e.v,now^e.w))
			return false;
		if(now^e.w^c[e.v])
			return false;
	}
	return true;
}
int qpow(long long a,int b)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%MOD;
		a=a*a%MOD;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(node(v,!w));
		mat[v].push_back(node(u,!w));
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		if(!vis[i])
			if(dfs(i,true))
				ans++;
			else
			{
				cout<<0<<endl;
				return 0;
			}
	cout<<qpow(2,ans-1)<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=205,M=405,INF=0x3f3f3f3f;
struct edge
{
	int u,v,cap,nxt;
}e[M];
int head[N],cc,from[N];
inline void add_edge(int u,int v,int cap)
{
	e[++cc].u=u;
	e[cc].v=v;
	e[cc].cap=cap;
	e[cc].nxt=head[u];
	head[u]=cc;
}
struct node
{
	int v,flow;
	node(int v,int flow):v(v),flow(flow){}
};
bool vis[N];
int main()
{
	int m,n;
	cin>>m>>n;
	while(m--)
	{
		int u,v,cap;
		cin>>u>>v>>cap;
		add_edge(u,v,cap);
		add_edge(v,u,0);
	}
	int ans=0;
	for(;;)
	{
		memset(vis,false,sizeof(vis));
		vis[1]=true;
		queue<node> Q;
		Q.push(node(1,INF));
		int now=0;
		while(!Q.empty())
		{
			node k=Q.front();Q.pop();
			if(k.v==n)
			{
				now=k.flow;
				break;
			}
			for(int i=head[k.v];i;i=e[i].nxt)
				if(!vis[e[i].v]&&e[i].cap)
				{
					vis[e[i].v]=true;
					from[e[i].v]=i;
					Q.push(node(e[i].v,min(k.flow,e[i].cap)));
				}
		}
		if(!now)
			break;
		for(int u=n;u!=1;u=e[from[u]].u)
		{
			e[from[u]].cap-=now;
			e[((from[u]-1)^1)+1].cap+=now;
		}
		ans+=now;
	}
	cout<<ans<<endl;
	return 0;
}
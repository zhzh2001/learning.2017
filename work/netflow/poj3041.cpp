#include<iostream>
#include<vector>
using namespace std;
const int N=505;
vector<int> mat[N];
bool vis[N];
int match[N];
bool dfs(int k)
{
	for(vector<int>::iterator it=mat[k].begin();it!=mat[k].end();it++)
		if(!vis[*it])
		{
			vis[*it]=true;
			if(!match[*it]||dfs(match[*it]))
			{
				match[*it]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		fill(vis+1,vis+n+1,false);
		ans+=dfs(i);
	}
	cout<<ans<<endl;
	return 0;
}
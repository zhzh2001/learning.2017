#include<bits/stdc++.h>
using namespace std;
const int N=1005;
vector<int> mat[N];
bool vis[N];
int match[N];
bool dfs(int k)
{
	for(vector<int>::iterator it=mat[k].begin();it!=mat[k].end();it++)
		if(!vis[*it])
		{
			vis[*it]=true;
			if(!match[*it]||dfs(match[*it]))
			{
				match[*it]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	int n,m,e;
	scanf("%d%d%d",&n,&m,&e);
	while(e--)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		if(u>n||v>m)
			continue;
		mat[u].push_back(v);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		fill(vis+1,vis+m+1,false);
		ans+=dfs(i);
	}
	cout<<ans<<endl;
	return 0;
}
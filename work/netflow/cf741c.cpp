#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int u[N],v[N],mat[N][2];
bool vis[N],ans[N];
void dfs(int k,bool color)
{
	ans[k]=color;
	vis[k]=true;
	if(!vis[mat[k][0]])
		dfs(mat[k][0],!color);
	if(!vis[mat[k][1]])
		dfs(mat[k][1],!color);
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>u[i]>>v[i];
		mat[u[i]][0]=v[i];
		mat[v[i]][0]=u[i];
	}
	for(int i=1;i<=2*n;i+=2)
	{
		mat[i][1]=i+1;
		mat[i+1][1]=i;
	}
	for(int i=1;i<=2*n;i++)
		if(!vis[i])
			dfs(i,true);
	for(int i=1;i<=n;i++)
		cout<<(int)ans[u[i]]+1<<' '<<(int)ans[v[i]]+1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=35,dx[]={-1,1,0,0},dy[]={0,0,-1,1},INF=0x3f3f3f3f;
int a[N][N];
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> e;
int head[N*N],H[N*N];
inline void add_edge(int u,int v,int cap)
{
	e.push_back(edge(v,cap,head[u]));
	e.push_back(edge(u,0,head[v]));
	head[u]=e.size()-2;
	head[v]=e.size()-1;
}
int d[N*N],t;
bool bfs()
{
	memset(d,0,sizeof(d));
	d[0]=1;
	queue<int> Q;
	Q.push(0);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];~i;i=e[i].nxt)
			if(!d[e[i].v]&&e[i].cap)
			{
				d[e[i].v]=d[k]+1;
				Q.push(e[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=e[i].nxt)
		if(d[e[i].v]==d[k]+1&&(t=dfs(e[i].v,min(pred,e[i].cap)))>0)
		{
			e[i].cap-=t;
			e[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	int n;
	scanf("%d",&n);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			scanf("%d",&a[i][j]);
			ans+=a[i][j];
		}
	memset(head,-1,sizeof(head));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if((i+j)&1)
			{
				add_edge(0,(i-1)*n+j,a[i][j]);
				for(int k=0;k<4;k++)
				{
					int nx=i+dx[k],ny=j+dy[k];
					if(nx&&nx<=n&&ny&&ny<=n)
						add_edge((i-1)*n+j,(nx-1)*n+ny,INF);
				}
			}
			else
				add_edge((i-1)*n+j,n*n+1,a[i][j]);
	t=n*n+1;
	while(bfs())
	{
		memcpy(H,head,sizeof(H));
		ans-=dfs(0,INF);
	}
	printf("%d\n",ans);
	return 0;
}
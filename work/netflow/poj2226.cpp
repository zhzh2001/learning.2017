#include<iostream>
#include<string>
#include<cstring>
using namespace std;
const int N=50;
string mat[N];
int rn,cn,r[N][N],c[N][N];
bool G[N*N/4+1][N*N/4+1],vis[N*N/4+1];
int match[N*N/4+1];
bool dfs(int k)
{
	for(int i=1;i<=cn;i++)
		if(G[k][i]&&!vis[i])
		{
			vis[i]=true;
			if(!match[i]||dfs(match[i]))
			{
				match[i]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=0;i<n;i++)
	{
		cin>>mat[i];
		for(int j=0;j<m;j++)
			if(mat[i][j]=='*')
				if(!j||mat[i][j-1]=='.')
					r[i][j]=++rn;
				else
					r[i][j]=r[i][j-1];
	}
	for(int j=0;j<m;j++)
		for(int i=0;i<n;i++)
			if(mat[i][j]=='*')
				if(!i||mat[i-1][j]=='.')
					c[i][j]=++cn;
				else
					c[i][j]=c[i-1][j];
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			if(mat[i][j]=='*')
				G[r[i][j]][c[i][j]]=true;
	int ans=0;
	for(int i=1;i<=rn;i++)
	{
		memset(vis,false,sizeof(vis));
		ans+=dfs(i);
	}
	cout<<ans<<endl;
	return 0;
}
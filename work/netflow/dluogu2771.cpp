#include<bits/stdc++.h>
using namespace std;
const int n=500,N=4*n+5,INF=0x3f3f3f3f;
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> e;
int head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	e.push_back(edge(v,cap,head[u]));
	e.push_back(edge(u,0,head[v]));
	head[u]=e.size()-2;
	head[v]=e.size()-1;
}
int d[N];
bool bfs()
{
	memset(d,0,sizeof(d));
	d[0]=1;
	queue<int> Q;
	Q.push(0);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==4*n+1)
			return true;
		for(int i=head[k];~i;i=e[i].nxt)
			if(!d[e[i].v]&&e[i].cap)
			{
				d[e[i].v]=d[k]+1;
				Q.push(e[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==4*n+1||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=e[i].nxt)
		if(d[e[i].v]==d[k]+1&&(t=dfs(e[i].v,min(pred,e[i].cap)))>0)
		{
			e[i].cap-=t;
			e[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	memset(head,-1,sizeof(head));
	for(int i=1;i<=n;i++)
	{
		add_edge(0,i,1);
		add_edge(n+i,2*n+i,1);
		add_edge(3*n+i,4*n+1,1);
	}
	int m;
	cin>>m;
	while(m--)
	{
		int x,y,z;
		cin>>x>>y>>z;
		add_edge(x,n+y,1);
		add_edge(2*n+y,3*n+z,1);
	}
	int ans=0;
	while(bfs())
	{
		memcpy(H,head,sizeof(H));
		ans+=dfs(0,INF);
	}
	cout<<ans<<endl;
	return 0;
}
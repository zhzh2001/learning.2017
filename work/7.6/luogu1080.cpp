#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1005;
struct node
{
	int lh, rh;
	bool operator<(const node &rhs) const
	{
		return lh * rh < rhs.lh * rhs.rh;
	}
} a[N];
struct bigint
{
	static const int base = 1000000000, L = 9, LEN = 450;
	int len;
	long long dig[LEN];
	bigint(int x = 0)
	{
		fill(dig + 1, dig + LEN, 0);
		len = 0;
		do
			dig[++len] = x % base;
		while (x /= base);
	}
	bool operator<(const bigint &rhs) const
	{
		if (len != rhs.len)
			return len < rhs.len;
		for (int i = len; i; i--)
			if (dig[i] != rhs.dig[i])
				return dig[i] < rhs.dig[i];
		return false;
	}
	bigint &operator*=(int x)
	{
		int overflow = 0;
		for (int i = 1; i <= len; i++)
		{
			dig[i] = dig[i] * x + overflow;
			overflow = dig[i] / base;
			dig[i] %= base;
		}
		if (overflow)
			dig[++len] = overflow;
		return *this;
	}
	bigint operator/(int x) const
	{
		bigint ret;
		ret.len = len;
		long long rem = 0;
		for (int i = len; i; i--)
		{
			ret.dig[i] = (rem + dig[i]) / x;
			rem = (rem + dig[i]) % x * base;
		}
		if (!ret.dig[len])
			ret.len--;
		return ret;
	}
};
ostream &operator<<(ostream &os, const bigint &b)
{
	os << b.dig[b.len];
	os.fill('0');
	for (int i = b.len - 1; i; i--)
	{
		os.width(bigint::L);
		os << b.dig[i];
	}
	return os;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, lh, rh;
	cin >> n >> lh >> rh;
	for (int i = 1; i <= n; i++)
		cin >> a[i].lh >> a[i].rh;
	sort(a + 1, a + n + 1);
	bigint ans = 0, now = lh;
	for (int i = 1; i <= n; i++)
	{
		ans = max(ans, now / a[i].rh);
		now *= a[i].lh;
	}
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 200005;
struct node
{
	int l, r, id;
	node() {}
	node(int l, int r, int id) : l(l), r(r), id(id) {}
	bool operator<(const node &rhs) const
	{
		if (l == rhs.l)
			return r < rhs.r;
		return l < rhs.l;
	}
} ad[N];
bool cmp(const node &lhs, const node &rhs)
{
	return lhs.r < rhs.r;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		cin >> ad[i].l >> ad[i].r;
		ad[i].id = i;
	}
	sort(ad + 1, ad + n + 1);
	int tail = 1;
	for (int i = 2; i <= n; i++)
		if (ad[i].r > ad[tail].r)
			ad[++tail] = ad[i];
	n = tail;
	long long ans = 0;
	int ans1, ans2;
	for (int i = 1; i <= m; i++)
	{
		int l, r, c;
		cin >> l >> r >> c;
		for (int j = lower_bound(ad + 1, ad + n + 1, node(0, l, 0), cmp) - ad; j <= n && ad[j].l <= r; j++)
		{
			int intersect = min(r, ad[j].r) - max(l, ad[j].l);
			if (1ll * intersect * c > ans)
			{
				ans = 1ll * intersect * c;
				ans1 = ad[j].id;
				ans2 = i;
			}
		}
	}
	if (ans)
		cout << ans << endl
			 << ans1 << ' ' << ans2 << endl;
	else
		cout << ans << endl;
	return 0;
}
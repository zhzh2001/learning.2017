#include <bits/stdc++.h>
using namespace std;
const int N = 505, INF = 0x3f3f3f3f;
int a[N], b[N], f[N][N];
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		for (int i = 1; i <= n; i++)
			cin >> a[i];
		int m;
		cin >> m;
		for (int i = 1; i <= m; i++)
			cin >> b[i];
		int ans = 0;
		for (int i = 1; i <= n; i++)
		{
			int pre = 0;
			for (int j = 1; j <= m; j++)
			{
				f[i][j] = f[i - 1][j];
				if (b[j] < a[i])
					pre = max(pre, f[i][j]);
				if (b[j] == a[i])
					f[i][j] = max(f[i][j], pre + 1);
				ans = max(ans, f[i][j]);
			}
		}
		cout << ans << endl;
	}
	return 0;
}
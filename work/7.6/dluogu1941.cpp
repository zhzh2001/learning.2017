#include <bits/stdc++.h>
using namespace std;
const int N = 10005, M = 1005, INF = 0x3f3f3f3f;
int x[N], y[N], f[N][M], l[N], r[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, k;
	cin >> n >> m >> k;
	for (int i = 1; i <= n; i++)
	{
		cin >> x[i] >> y[i];
		l[i] = 0;
		r[i] = m + 1;
	}
	for (int i = 1; i <= k; i++)
	{
		int p, lo, hi;
		cin >> p >> lo >> hi;
		l[p] = lo;
		r[p] = hi;
	}
	int cnt = 0;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 0; j <= m; j++)
		{
			f[i][j] = INF;
			if (j >= x[i])
				f[i][j] = min(f[i][j], min(f[i - 1][j - x[i]] + 1, f[i][j - x[i]] + 1));
		}
		for (int j = l[i] + 1; j < r[i]; j++)
			if (j + y[i] <= m)
				f[i][j] = min(f[i][j], f[i - 1][j + y[i]]);
		if (m < r[i])
			for (int j = m - x[i]; j <= m; j++)
				f[i][m] = min(f[i][m], min(f[i - 1][j] + 1, f[i][j] + 1));
		fill(f[i], f[i] + l[i] + 1, INF);
		fill(f[i] + r[i], f[i] + m + 1, INF);
		bool flag = false;
		for (int j = l[i] + 1; j < r[i]; j++)
			if (f[i][j] < INF)
			{
				flag = true;
				break;
			}
		if (!flag)
		{
			cout << 0 << endl
				 << cnt << endl;
			return 0;
		}
		if (r[i] <= m)
			cnt++;
	}
	cout << 1 << endl
		 << *min_element(f[n] + l[n] + 1, f[n] + r[n]) << endl;
	return 0;
}
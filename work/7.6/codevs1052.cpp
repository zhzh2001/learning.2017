#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 105;
pair<int, int> a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i].first;
	for (int i = 1; i <= n; i++)
		cin >> a[i].second;
	sort(a + 1, a + n + 1);
	int j = n;
	priority_queue<int> Q;
	int ans = 0;
	for (int i = a[n].first; i; i--)
	{
		for (; j && a[j].first == i; j--)
			Q.push(a[j].second);
		if (!Q.empty())
		{
			ans += Q.top();
			Q.pop();
		}
	}
	cout << ans << endl;
	return 0;
}
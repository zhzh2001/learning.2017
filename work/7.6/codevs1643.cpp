#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1000005;
struct segment
{
	int l, r;
	bool operator<(const segment &rhs) const
	{
		return r < rhs.r;
	}
} a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i].l >> a[i].r;
	sort(a + 1, a + n + 1);
	int ans = 0, now = 0;
	for (int i = 1; i <= n; i++)
		if (a[i].l >= now)
		{
			ans++;
			now = a[i].r;
		}
	cout << ans << endl;
	return 0;
}
#include <cstdio>
#include <cstring>
#include <algorithm>
using namespace std;
const int maxn = 10005, maxm = 1005;
int n, m, p;
int clic[maxn], uncl[maxn], L[maxn], H[maxn];
int f[maxn][maxm], cnt, INF;
inline int read()
{
	int x = 0;
	char ch = getchar();
	while (ch < '0' || ch > '9')
		ch = getchar();
	while (ch >= '0' && ch <= '9')
		x = x * 10 + ch - '0', ch = getchar();
	return x;
}
int main()
{
	n = read(), m = read(), p = read();
	for (int i = 1; i <= n; i++)
		clic[i] = read(), uncl[i] = read();
	for (int i = 1; i <= n; i++)
		L[i] = 0, H[i] = m + 1;
	for (int i = 1; i <= p; i++)
	{
		int x = read();
		L[x] = read(), H[x] = read();
	}
	memset(f, 63, sizeof f), INF = f[0][0], cnt = 0;
	for (int i = 0; i <= m; i++)
		f[0][i] = 0;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j < H[i]; j++)
			if (j >= clic[i] && j != m)
				f[i][j] = min(f[i][j], min(f[i - 1][j - clic[i]] + 1, f[i][j - clic[i]] + 1));
		for (int j = 1; j < H[i]; j++)
			if (j + uncl[i] <= m && j >= L[i] + 1)
				f[i][j] = min(f[i][j], f[i - 1][j + uncl[i]]);
		if (H[i] == m + 1)
			for (int j = m; j >= max(1, m - clic[i]); j--)
				f[i][m] = min(f[i][m], min(f[i - 1][j] + 1, f[i][j] + 1));
		bool flythr = 0;
		for (int j = L[i] + 1; j < H[i]; j++)
			if (f[i][j] < INF)
			{
				flythr = 1;
				break;
			}
		for (int j = 0; j <= L[i]; j++)
			f[i][j] = INF;
		if (!flythr)
		{
			printf("0\n%d", cnt);
			return 0;
		}
		if (L[i] > 0 || H[i] < m)
			cnt++;
	}
	int ans = 1 << 30;
	for (int i = 1; i <= m; i++)
		ans = min(ans, f[n][i]);
	printf("1\n%d", ans);
	return 0;
}
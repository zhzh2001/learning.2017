#include<bits/stdc++.h>
using namespace std;
const int N=65;
int sg[N];
int main()
{
	int j=0;
	for(int i=0;j<=60;i++)
		for(int k=1;k<=i+1&&j<=60;j++,k++)
			sg[j]=i;
	int n;
	cin>>n;
	int ans=0;
	while(n--)
	{
		int x;
		cin>>x;
		ans^=sg[x];
	}
	if(ans)
		cout<<"NO\n";
	else
		cout<<"YES\n";
	return 0;
}
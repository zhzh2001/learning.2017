#include<bits/stdc++.h>
using namespace std;
const int LOG=70;
int cnt[LOG];
int main()
{
	int n;
	cin>>n;
	while(n--)
	{
		int cc=0;
		unsigned long long x;
		for(cin>>x;x;x&=x-1,cc++);
		cnt[cc]++;
	}
	int cc=0;
	for(int i=0;i<=64;i++)
		cc+=cnt[i]&1;
	if(cc%9)
		cout<<"B\n";
	else
		cout<<"L\n";
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int k;
int dfs(int x)
{
	if(x==1)
		return 0;
	bool vis[x];
	memset(vis,false,sizeof(vis));
	for(int i=ceil(1.0*x/k);i<x;i++)
		vis[dfs(i)]=true;
	int ret;
	for(ret=0;vis[ret];ret++);
	return ret;
}
int main()
{
	cin>>k;
	for(int i=1;;i++)
		cout<<dfs(i)<<' ';
	return 0;
}
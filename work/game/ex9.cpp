#include<bits/stdc++.h>
using namespace std;
const int N=70;
bool vis[N];
int dfs(int k)
{
	if(!k)
		return 0;
	bool cnt[N];
	memset(cnt,false,sizeof(cnt));
	for(int i=1;i<=k;i++)
		if(!vis[i])
		{
			vis[i]=true;
			cnt[dfs(k-i)]=true;
			vis[i]=false;
		}
	int ret;
	for(ret=0;cnt[ret];ret++);
	return ret;
}
int main()
{
	for(int i=0;;i++)
		cout<<dfs(i)<<' ';
	cout<<endl;
	return 0;
}
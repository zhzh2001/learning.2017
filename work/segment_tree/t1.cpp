#include<bits/stdc++.h>
using namespace std;
const int N=100005;
typedef long long value_t;
value_t a[N],val;
int l,r;
namespace segment_tree
{
	struct node
	{
		value_t sum,add;
	}tree[4*N];
	
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id].sum=tree[lc].sum+tree[rc].sum;
	
	void make(int id,int l,int r)
	{
		if(l==r)
		{
			tree[id].sum=a[l];
			return;
		}
		calc;
		make(lc,l,mid);
		make(rc,mid+1,r);
		upc;
	}
	
	inline void pushdown(int id,int l,int r)
	{
		if(tree[id].add)
		{
			calc;
			tree[lc].sum+=(mid-l+1)*tree[id].add;
			tree[rc].sum+=(r-mid)*tree[id].add;
			tree[lc].add+=tree[id].add;
			tree[rc].add+=tree[id].add;
			tree[id].add=0;
		}
	}
	
	//[::l,::r]+=val
	void update(int id,int l,int r)
	{
		if(::l<=l&&::r>=r)
		{
			tree[id].sum+=(r-l+1)*val;
			tree[id].add+=val;
		}
		else
		{
			calc;
			pushdown(id,l,r);
			if(::l<=mid)
				update(lc,l,mid);
			if(::r>mid)
				update(rc,mid+1,r);
			upc;
		}
	}
	
	//val+=sigma(::l,::r)ai
	void query(int id,int l,int r)
	{
		if(::l<=l&&::r>=r)
			val+=tree[id].sum;
		else
		{
			calc;
			pushdown(id,l,r);
			if(::l<=mid)
				query(lc,l,mid);
			if(::r>mid)
				query(rc,mid+1,r);
		}	
	}
	
	#undef calc
	#undef upc
	
};
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	segment_tree::make(1,1,n);
	while(m--)
	{
		int t;
		cin>>t;
		if(t==1)
		{
			cin>>l>>r>>val;
			segment_tree::update(1,1,n);
		}
		else
		{
			cin>>l>>r;
			val=0;
			segment_tree::query(1,1,n);
			cout<<val<<endl;
		}
	}
	return 0;
}
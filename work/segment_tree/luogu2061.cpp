#include<bits/stdc++.h>
using namespace std;
const int N=40005;
struct node
{
	int l,r,h;
	bool operator<(const node& b)const
	{
		return h<b.h;
	}
}a[N];
int num[2*N],l,r,h;
long long ans;
struct segment_tree
{
	struct tnode
	{
		int l,r,h;
	}tree[2*N*4];
	#define calc int mid=(tree[id].l+tree[id].r)/2,lc=id*2,rc=lc+1;
	
	void make(int id,int l,int r)
	{
		tree[id].l=l;
		tree[id].r=r;
		tree[id].h=0;
		if(l<r-1)
		{
			calc;
			make(lc,l,mid);
			make(rc,mid,r);
		}
	}
	inline void pushdown(int id)
	{
		if(tree[id].h&&tree[id].l<tree[id].r)
		{
			calc;
			tree[lc].h=tree[rc].h=tree[id].h;
			tree[id].h=0;
		}
	}
	void update(int id)
	{
		if(l<=tree[id].l&&r>=tree[id].r)
			tree[id].h=h;
		else
			if(tree[id].l<tree[id].r-1)
			{
				calc;
				pushdown(id);
				if(l<=mid)
					update(lc);
				if(r>mid)
					update(rc);
			}
	}
	void query(int id)
	{
		if(tree[id].h)
			ans+=(long long)(num[tree[id].r]-num[tree[id].l])*tree[id].h;
		else
			if(tree[id].l<tree[id].r-1)
			{
				calc;
				pushdown(id);
				query(lc);
				query(rc);
			}
	}
}T;
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].l>>a[i].r>>a[i].h;
		num[2*i-1]=a[i].l;
		num[2*i]=a[i].r;
	}
	sort(a+1,a+n+1);
	sort(num+1,num+n*2+1);
	T.make(1,1,2*n);
	for(int i=1;i<=n;i++)
	{
		l=lower_bound(num+1,num+2*n+1,a[i].l)-num;
		r=lower_bound(num+1,num+2*n+1,a[i].r)-num;
		h=a[i].h;
		T.update(1);
	}
	ans=0;
	T.query(1);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=86405,INFM=0x3f;
const long long INF=0x3f3f3f3f3f3f3f3fLL;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
	bool operator>(const node& b)const
	{
		return w>b.w;
	}
};
vector<node> mat[N];
struct dijkstra
{
	long long d[N];
	bool vis[N];
	priority_queue<node,vector<node>,greater<node> > Q;
	void work(int s)
	{
		memset(d,INFM,sizeof(d));
		d[s]=0;
		memset(vis,false,sizeof(vis));
		Q.push(node(s,0));
		while(!Q.empty())
		{
			node k=Q.top();Q.pop();
			if(!vis[k.v])
			{
				vis[k.v]=true;
				for(int i=0;i<mat[k.v].size();i++)
					if(d[k.v]+mat[k.v][i].w<d[mat[k.v][i].v])
					{
						d[mat[k.v][i].v]=d[k.v]+mat[k.v][i].w;
						Q.push(node(mat[k.v][i].v,d[mat[k.v][i].v]));
					}
			}
		}
	}
}solver;
int main()
{
	int n,m,e;
	scanf("%d%d%d",&n,&m,&e);
	while(n--)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		mat[u].push_back(node(v+1,w));
	}
	for(int i=m;i<e;i++)
		mat[i+1].push_back(node(i,0));
	solver.work(m);
	if(solver.d[e+1]==INF)
		puts("-1");
	else
		printf("%lld\n",solver.d[e+1]);
	return 0;
}
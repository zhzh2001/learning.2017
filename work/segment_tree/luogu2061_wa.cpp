#include<bits/stdc++.h>
using namespace std;
struct node
{
	int a,b,h;
	bool operator<(const node& b)const
	{
		return h<b.h;
	}
}l[40005];
int a,b,h;
long long ans;
struct segment_tree
{
	struct node
	{
		long long sum,lazy;
		int l,r;
		node *lc,*rc;
	};
	node *root;
	segment_tree(){root=NULL;}
	inline void pushdown(node *t)
	{
		if(t!=NULL&&t->lazy&&t->l<t->r)
		{
			int mid=(t->l+t->r)/2;
			if(t->lc==NULL)
			{
				t->lc=new node;
				t->lc->l=t->l;
				t->lc->r=mid;
				t->lc->sum=t->lc->lazy=0;
				t->lc->lc=t->lc->rc=NULL;
			}
			t->lc->sum=t->lazy*(t->lc->r-t->lc->l+1);
			t->lc->lazy+=t->lazy;
			if(t->rc==NULL)
			{
				t->rc=new node;
				t->rc->l=mid+1;
				t->rc->r=t->r;
				t->rc->sum=t->rc->lazy=0;
				t->rc->lc=t->rc->rc=NULL;
			}
			t->rc->sum=t->lazy*(t->rc->r-t->rc->l+1);
			t->rc->lazy+=t->lazy;
			
			t->lazy=0;
		}
	}
	node* update(node *t)
	{
		if(t==NULL)
		{
			t=new node;
			t->l=a;t->r=b;
			t->sum=t->lazy=0;
			t->lc=t->rc=NULL;
		}
		if(a<=t->l&&b>=t->r)
		{
			t->sum=h*(t->r-t->l+1);
			t->lazy+=h;
		}
		else
		{
			pushdown(t);
			int mid=(t->l+t->r)/2;
			t->sum=0;
			if(a<=mid)
			{
				if(t->lc==NULL)
				{
					t->lc=new node;
					t->lc->l=t->l;
					t->lc->r=mid;
					t->lc->sum=t->lc->lazy=0;
					t->lc->lc=t->lc->rc=NULL;
				}
				t->lc=update(t->lc);
				t->sum+=t->lc->sum;
			}
			if(b>mid)
			{
				if(t->rc==NULL)
				{
					t->rc=new node;
					t->rc->l=mid+1;
					t->rc->r=t->r;
					t->rc->sum=t->rc->lazy=0;
					t->rc->lc=t->rc->rc=NULL;
				}
				t->rc=update(t->rc);
				t->sum+=t->rc->sum;
			}
		}
		return t;
	}
	void query(node *t)
	{
		if(t!=NULL)
		{
			pushdown(t);
			if(t->lc==NULL&&t->rc==NULL)
				ans+=t->sum;
			else
			{
				query(t->lc);
				query(t->rc);
			}
		}
	}
}T;
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>l[i].a>>l[i].b>>l[i].h;
	sort(l+1,l+n+1);
	a=1;b=1e9;h=0;
	T.root=T.update(T.root);
	for(int i=1;i<=n;i++)
	{
		a=l[i].a;
		b=l[i].b-1;
		h=l[i].h;
		T.update(T.root);
	}
	T.query(T.root);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=86405;
const long long INF=0x3f3f3f3f3f3f3f3fLL;
struct cow
{
	int l,r,w;
	bool operator<(const cow& c)const
	{
		if(l!=c.l)
			return l<c.l;
		return r<c.r;
	}
}a[N];
int x,y;
long long val;
struct segment_tree
{
	struct node
	{
		long long num,lazy;
	}tree[M*4];
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	void make(int id,int l,int r)
	{
		tree[id].num=tree[id].lazy=INF;
		if(l<r)
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
		}
	}
	inline void pushdown(int id,int l,int r)
	{
		if(tree[id].lazy<INF&&l<r)
		{
			calc;
			tree[lc].lazy=min(tree[lc].lazy,tree[id].lazy);
			tree[lc].num=min(tree[lc].num,tree[id].lazy);
			tree[rc].lazy=min(tree[rc].lazy,tree[id].lazy);
			tree[rc].num=min(tree[rc].num,tree[id].lazy);
			tree[id].lazy=INF;
		}
	}
	void update(int id,int l,int r)
	{
		calc;
		pushdown(id,l,r);
		if(x<=l&&y>=r)
		{
			tree[id].num=min(tree[id].num,val);
			tree[id].lazy=val;
		}
		else
		{
			if(x<=mid)
				update(lc,l,mid);
			if(y>mid)
				update(rc,mid+1,r);
		}
	}
	void query(int id,int l,int r)
	{
		if(l==r&&l==x)
			val=tree[id].num;
		else
		{
			calc;
			pushdown(id,l,r);
			if(x<=mid)
				query(lc,l,mid);
			else
				query(rc,mid+1,r);
		}
	}
}T;
int main()
{
	int n,m,e;
	scanf("%d%d%d",&n,&m,&e);
	for(int i=1;i<=n;i++)
		scanf("%d%d%d",&a[i].l,&a[i].r,&a[i].w);
	sort(a+1,a+n+1);
	T.make(1,m,e);
	for(int i=1;i<=n;i++)
	{
		x=a[i].l-1;
		val=0;
		if(m<=x)
			T.query(1,m,e);
		if(val==INF)
		{
			puts("-1");
			return 0;
		}
		x=a[i].l;
		y=a[i].r;
		val+=a[i].w;
		T.update(1,m,e);
	}
	x=e;
	T.query(1,m,e);
	if(val==INF)
		puts("-1");
	else
		printf("%lld\n",val);
	return 0;
}
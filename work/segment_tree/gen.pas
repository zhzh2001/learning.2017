
Program gen;

Const 
  n = 50000;
  m = 10000;

Var 
  i,l,r: longint;
  year,rain: array[1..n] Of longint;
Procedure sort(l,r:longint);

Var 
  i,j,x,y: longint;
Begin
  i := l;
  j := r;
  x := year[(l+r) Div 2];
  Repeat
    While year[i]<x Do
      inc(i);
    While x<year[j] Do
      dec(j);
    If Not(i>j) Then
      Begin
        y := year[i];
        year[i] := year[j];
        year[j] := y;
        y := rain[i];
        rain[i] := rain[j];
        rain[j] := y;
        inc(i);
        j := j-1;
      End;
  Until i>j;
  If l<j Then sort(l,j);
  If i<r Then sort(i,r);
End;
Begin
  randomize;
  writeln(n);
  For i:=1 To n Do
    Begin
      year[i] := random(2000000000)-100000000;
      rain[i] := random(1000000000)+1;
    End;
  sort(1,n);
  For i:=1 To n Do
    writeln(year[i],' ',rain[i]);
  writeln(m);
  For i:=1 To m Do
    Begin
      l := random(2000000000)-1000000000;
      r := random(2000000000-l)+l;
      writeln(l,' ',r);
    End;
End.

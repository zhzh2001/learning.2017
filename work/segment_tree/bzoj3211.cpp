#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],x,y;
long long val;
struct segment_tree
{
	struct node
	{
		long long sum;
		int cnt;
	}tree[N*4];
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id].sum=tree[lc].sum+tree[rc].sum;\
	tree[id].cnt=tree[lc].cnt+tree[rc].cnt;
	void make(int id,int l,int r)
	{
		if(l==r)
		{
			tree[id].sum=a[l];
			tree[id].cnt=a[l]>1;
		}
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			upc;
		}
	}
	void update(int id,int l,int r)
	{
		if(l==r)
		{
			tree[id].sum=floor(sqrt(tree[id].sum));
			tree[id].cnt=tree[id].sum>1;
		}
		else
		{
			calc;
			if(x<=mid&&tree[lc].cnt)
				update(lc,l,mid);
			if(y>mid&&tree[rc].cnt)
				update(rc,mid+1,r);
			upc;
		}
	}
	void query(int id,int l,int r)
	{
		if(x<=l&&y>=r)
			val+=tree[id].sum;
		else
		{
			calc;
			if(x<=mid)
				query(lc,l,mid);
			if(y>mid)
				query(rc,mid+1,r);
		}
	}
}T;
int main()
{
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	T.make(1,1,n);
	int m;
	scanf("%d",&m);
	while(m--)
	{
		int t;
		scanf("%d%d%d",&t,&x,&y);
		if(t==1)
		{
			val=0;
			T.query(1,1,n);
			printf("%lld\n",val);
		}
		else
			T.update(1,1,n);
	}
	return 0;
}
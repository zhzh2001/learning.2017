#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
int x;
struct val_segment_tree
{
	int tree[N*4];
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id]=tree[lc]+tree[rc];
	void make(int id,int l,int r)
	{
		if(l==r)
			tree[id]=0;
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			upc;
		}
	}
	void insert(int id,int l,int r)
	{
		if(l==r)
			tree[id]=1;
		else
		{
			calc;
			if(x<=mid)
				insert(lc,l,mid);
			else
				insert(rc,mid+1,r);
			upc;
		}
	}
	void erase(int id,int l,int r)
	{
		if(l==r)
			tree[id]=0;
		else
		{
			calc;
			if(x<=mid)
				erase(lc,l,mid);
			else
				erase(rc,mid+1,r);
			upc;
		}
	}
	int query_min(int id,int l,int r)
	{
		if(!tree[id])
			return -1;
		if(l==r)
			return l;
		calc;
		if(tree[lc])
			return query_min(lc,l,mid);
		return query_min(rc,mid+1,r);
	}
	int query_max(int id,int l,int r)
	{
		if(!tree[id])
			return -1;
		if(l==r)
			return l;
		calc;
		if(tree[rc])
			return query_max(rc,mid+1,r);
		return query_max(lc,l,mid);
	}
	int query_pred(int id,int l,int r)
	{
		if(!tree[id]||l>=x)
			return -1;
		if(l==r)
			return l;
		calc;
		if(x<=mid)
			return query_pred(lc,l,mid);
		int t=query_pred(rc,mid+1,r);
		if(t==-1)
			return query_max(lc,l,mid);
		return t;
	}
	int query_succ(int id,int l,int r)
	{
		if(!tree[id]||r<=x)
			return -1;
		if(l==r)
			return l;
		calc;
		if(x>=mid)
			return query_succ(rc,mid+1,r);
		int t=query_succ(lc,l,mid);
		if(t==-1)
			return query_min(rc,mid+1,r);
		return t;
	}
	int find(int id,int l,int r)
	{
		if(!tree[id])
			return -1;
		if(l==r)
			return 1;
		calc;
		if(x<=mid)
			return find(lc,l,mid);
		return find(rc,mid+1,r);
	}
}T;
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	T.make(1,0,n);
	while(m--)
	{
		int t;
		scanf("%d",&t);
		switch(t)
		{
			case 1:
				scanf("%d",&x);
				T.insert(1,0,n);
				break;
			case 2:
				scanf("%d",&x);
				T.erase(1,0,n);
				break;
			case 3:
				printf("%d\n",T.query_min(1,0,n));
				break;
			case 4:
				printf("%d\n",T.query_max(1,0,n));
				break;
			case 5:
				scanf("%d",&x);
				printf("%d\n",T.query_pred(1,0,n));
				break;
			case 6:
				scanf("%d",&x);
				printf("%d\n",T.query_succ(1,0,n));
				break;
			case 7:
				scanf("%d",&x);
				printf("%d\n",T.find(1,0,n));
				break;
			default:puts("-1");
		}
	}
	return 0;
}
program gen;
const
  n=10000;
  m=0;
  e=86400;
var
  i,l,r,t:longint;
begin
  randomize;
  writeln(n,' ',m,' ',e);
  for i:=1 to n do
  begin
    l:=random(e+1);r:=random(e+1);
	if l>r then
	begin
	  t:=l;
	  l:=r;
	  r:=t;
	end;
	writeln(l,' ',r,' ',random(500000));
  end;
end.
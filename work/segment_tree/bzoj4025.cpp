#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct edge
{
	int u,v;
	edge(int u=0,int v=0):u(u),v(v){};
}e;
int n;
struct union_find_set
{
	struct node
	{
		int fat,rank;
		node(int fat=0,int rank=0):fat(fat),rank(rank){};
	}f[N*2];
	union_find_set()
	{
		for(int i=1;i<=2*n;i++)
			f[i]=node(i,1);
	}
	inline int getf(int x)
	{
		while(f[x].fat!=x)
			x=f[x].fat;
		return x;
	}
	inline bool _merge(int ru,int rv)
	{
		if(ru==rv)
			return false;
		if(f[ru].rank>f[rv].rank)
			swap(ru,rv);
		f[ru].fat=rv;
		f[rv].rank+=f[ru].rank;
		return true;
	}
	inline bool merge(edge e)
	{
		return _merge(getf(e.u),getf(e.v+n))&&_merge(getf(e.u+n),getf(e.v));
	}
	inline bool _cancel(int ru,int rv)
	{
		if(ru!=rv)
			return false;
		if(f[ru].rank>f[rv].rank)
			swap(ru,rv);
		f[rv].rank-=f[ru].rank;
		f[ru].fat=ru;
		return true;
	}
	inline bool cancel(edge e)
	{
		return _cancel(getf(e.u),getf(e.v+n))&&_cancel(getf(e.u+n),getf(e.v));
	}
}S;
int x,y;
struct segment_tree
{
	vector<edge> tree[N*4];
	void update(int id,int l,int r)
	{
		if(x<=l&&y>=r)
			tree[id].push_back(e);
		else
		{
			int mid=(l+r)/2,lc=id*2,rc=lc+1;
			if(x<=mid)
				update(lc,l,mid);
			if(y>mid)
				update(rc,mid+1,r);
		}
	}
	void query(int id,int l,int r,bool flag)
	{
		int i;
		for(i=0;flag&&i<tree[id].size();i++)
			flag&=S.merge(tree[id][i]);
		if(l==r)
			if(flag)
				cout<<"Yes\n";
			else
				cout<<"No\n";
		else
		{
			int mid=(l+r)/2,lc=id*2,rc=lc+1;
			query(lc,l,mid,flag);
			query(rc,mid+1,r,flag);
		}
		for(i--;i>=0;i--)
			S.cancel(tree[id][i]);
	}
}T;
int main()
{
	int m,t;
	cin>>n>>m>>t;
	while(m--)
	{
		cin>>e.u>>e.v>>x>>y;
		x++;
		T.update(1,1,t);
	}
	S=union_find_set();
	T.query(1,1,t,true);
	return 0;
}
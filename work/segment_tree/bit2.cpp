#include<bits/stdc++.h>
using namespace std;
const int N=500005;
int a[N],x,y,val;
namespace segment_tree
{
	struct node
	{
		int sum,add;
	}tree[4*N];
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id].sum=tree[lc].sum+tree[rc].sum;
	void make(int id,int l,int r)
	{
		if(l==r)
			tree[id].sum=a[l];
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			upc;
		}
	}
	inline void pushdown(int id,int l,int r)
	{
		if(l==r)
		{
			tree[id].add=0;
			return;
		}
		if(tree[id].add)
		{
			calc;
			tree[lc].sum+=tree[id].add*(mid-l+1);
			tree[lc].add+=tree[id].add;
			tree[rc].sum+=tree[id].add*(r-mid);
			tree[rc].add+=tree[id].add;
			tree[id].add=0;
		}
	}
	void update(int id,int l,int r)
	{
		calc;
		pushdown(id,l,r);
		if(x<=l&&y>=r)
		{
			tree[id].sum+=val*(r-l+1);
			tree[id].add=val;
		}
		else
		{
			if(x<=mid)
				update(lc,l,mid);
			if(y>mid)
				update(rc,mid+1,r);
			upc;
		}
	}
	void query(int id,int l,int r)
	{
		calc;
		pushdown(id,l,r);
		if(l==r&&l==x)
			val=tree[id].sum;
		else
			if(x<=mid)
				query(lc,l,mid);
			else
				query(rc,mid+1,r);
	}
	#undef calc
	#undef upc
};
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	segment_tree::make(1,1,n);
	while(m--)
	{
		int t;
		scanf("%d",&t);
		if(t==1)
		{
			scanf("%d%d%d",&x,&y,&val);
			segment_tree::update(1,1,n);
		}
		else
		{
			scanf("%d",&x);
			val=0;
			segment_tree::query(1,1,n);
			printf("%d\n",val);
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=500005;
int a[N],x,y,res;
namespace segment_tree
{
	int tree[4*N];
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id]=tree[lc]+tree[rc];
	void make(int id,int l,int r)
	{
		if(l==r)
			tree[id]=a[l];
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			upc;
		}
	}
	void update(int id,int l,int r)
	{
		if(l==r&&l==x)
			tree[id]+=y;
		else
		{
			calc;
			if(x<=mid)
				update(lc,l,mid);
			else
				update(rc,mid+1,r);
			upc;
		}
	}
	void query(int id,int l,int r)
	{
		if(x<=l&&y>=r)
			res+=tree[id];
		else
		{
			calc;
			if(x<=mid)
				query(lc,l,mid);
			if(y>mid)
				query(rc,mid+1,r);
		}
	}
	#undef calc
	#undef upc
};
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	segment_tree::make(1,1,n);
	while(m--)
	{
		int t;
		scanf("%d%d%d",&t,&x,&y);
		if(t==1)
			segment_tree::update(1,1,n);
		else
		{
			res=0;
			segment_tree::query(1,1,n);
			printf("%d\n",res);
		}
	}
	return 0;
}
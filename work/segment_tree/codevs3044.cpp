#include<bits/stdc++.h>
using namespace std;
struct rect
{
	double x1,y1,x2,y2;
}r[105];
double x[205],y[205];
bool cover[205][205];
int main()
{
	int n;
	cout.precision(2);
	cout<<fixed;
	while(cin>>n&&n)
	{
		for(int i=1;i<=n;i++)
		{
			cin>>r[i].x1>>r[i].y1>>r[i].x2>>r[i].y2;
			x[i*2-1]=r[i].x1;
			x[i*2]=r[i].x2;
			y[i*2-1]=r[i].y1;
			y[i*2]=r[i].y2;
		}
		sort(x+1,x+2*n+1);
		sort(y+1,y+2*n+1);
		memset(cover,false,sizeof(cover));
		for(int i=1;i<=n;i++)
			for(int j=1;j<2*n;j++)
				for(int k=1;k<2*n;k++)
					cover[j][k]|=(r[i].x1<=x[j]&&r[i].x2>=x[j+1]&&r[i].y1<=y[k]&&r[i].y2>=y[k+1]);
		double ans=0;
		for(int i=1;i<2*n;i++)
			for(int j=1;j<2*n;j++)
				ans+=cover[i][j]*(x[i+1]-x[i])*(y[j+1]-y[j]);
		cout<<ans<<endl;
	}
	return 0;
}
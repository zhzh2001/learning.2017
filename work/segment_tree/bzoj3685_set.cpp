#include<bits/stdc++.h>
using namespace std;
namespace fastI
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	inline bool read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,stdin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
namespace fastO
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=buf;
	int d[15];
	inline void write(int x)
	{
		if(end-p<20)
		{
			fwrite(buf,1,p-buf,stdout);
			p=buf;
		}
		if(x<0)
		{
			*p++='-';
			x=-x;
		}
		int k=0;
		do
			d[++k]=x%10;
		while(x/=10);
		for(;k;k--)
			*p++=d[k]+'0';
	}
	inline void flush()
	{
		fwrite(buf,1,p-buf,stdout);
	}
};
set<int> S;
using namespace fastI;
using namespace fastO;
#define writeln *fastO::p++='\n';
int main()
{
	int n,m;
	read(n);read(m);
	while(m--)
	{
		int t,x;
		read(t);
		switch(t)
		{
			case 1:
				read(x);
				S.insert(x);
				break;
			case 2:
				read(x);
				S.erase(x);
				break;
			case 3:
				if(S.size())
					write(*S.begin());
				else
					write(-1);
				writeln;
				break;
			case 4:
				if(S.size())
					write(*S.rbegin());
				else
					write(-1);
				writeln;
				break;
			case 5:
			{
				read(x);
				set<int>::iterator p=S.lower_bound(x);
				if(p==S.begin())
					write(-1);
				else
					write(*--p);
				writeln;
				break;
			}
			case 6:
			{
				read(x);
				set<int>::iterator p=S.upper_bound(x);
				if(p==S.end())
					write(-1);
				else
					write(*p);
				writeln;
				break;
			}
			case 7:
				read(x);
				if(S.count(x))
					write(1);
				else
					write(-1);
				writeln;
				break;
		}
	}
	fastO::flush();
	return 0;
}
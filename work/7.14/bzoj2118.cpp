#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 15, M = 500005;
const long long INF = 0x3f3f3f3f3f3f3f3fll;
int n, m, a[N];
long long d[M];
bool vis[M];
long long calc(long long x)
{
	long long ans = 0;
	for (int i = 0; i < m; i++)
		if (d[i] <= x)
			ans += (x - d[i]) / m + 1;
	return ans;
}
int main()
{
	long long l, r;
	cin >> n >> l >> r;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1, greater<int>());
	while (a[n] == 0)
		n--;
	m = *min_element(a + 1, a + n + 1);
	fill(d, d + m, INF);
	d[0] = 0;
	typedef pair<long long, int> pii;
	priority_queue<pii, vector<pii>, greater<pii>> Q;
	Q.push(make_pair(0ll, 0));
	while (!Q.empty())
	{
		pii k = Q.top();
		Q.pop();
		if (vis[k.second])
			continue;
		vis[k.second] = true;
		for (int i = 1; i <= n; i++)
			if (d[k.second] + a[i] < d[(k.second + a[i]) % m])
				Q.push(make_pair(d[(k.second + a[i]) % m] = d[k.second] + a[i], (k.second + a[i]) % m));
	}
	cout << calc(r) - calc(l - 1) << endl;
	return 0;
}
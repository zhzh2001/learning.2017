#include <iostream>
using namespace std;
const int N = 15;
long long a[N], m[N];
long long qmul(long long a, long long b, long long mod)
{
	long long ans = 0;
	do
	{
		if (b & 1)
			ans = (ans + a) % mod;
		a = (a + a) % mod;
	} while (b /= 2);
	return ans;
}
long long qpow(long long a, long long b, long long mod)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = qmul(ans, a, mod);
		a = qmul(a, a, mod);
	} while (b /= 2);
	return ans;
}
int main()
{
	int n;
	long long l, r;
	cin >> n >> l >> r;
	long long M = 1;
	for (int i = 1; i <= n; i++)
	{
		cin >> m[i] >> a[i];
		M *= m[i];
	}
	long long ans = 0;
	for (int i = 1; i <= n; i++)
	{
		long long t = M / m[i];
		ans = (ans + a[i] * t % M * qpow(t, m[i] - 2, m[i])) % M;
	}
	ans += (l - ans + M - 1) / M * M;
	if (ans > r)
		cout << 0 << endl
			 << 0 << endl;
	else
		cout << (r - ans) / M + 1 << endl
			 << ans << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <limits>
using namespace std;
const int p[15] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47}, N = 50000, INF = 0x3f3f3f3f;
const long long INFl = numeric_limits<long long>::max();
int n, cnt[N], ans[N];
long long ans2;
void dfs(int k, long long now, int cnt)
{
	if (k > 14 || cnt > n)
		return;
	if (cnt == n)
	{
		ans2 = min(ans2, now);
		return;
	}
	for (int i = 1; i <= 62; i++)
	{
		if (now > ans2 / p[k] || cnt * (i + 1) > n)
			break;
		now *= p[k];
		if (n % (cnt * (i + 1)) == 0)
			dfs(k + 1, now, cnt * (i + 1));
	}
}
int main()
{
	for (int i = 1; i < N; i++)
	{
		cnt[i] = i;
		ans[i] = INF;
	}
	for (int i = 1; i < N; i++)
		for (int j = i; j < N; j += i)
			cnt[j]--;
	for (int i = 1; i < N; i++)
		ans[cnt[i]] = min(ans[cnt[i]], i);
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	for (int T = 1; T <= t; T++)
	{
		cout << "Case " << T << ": ";
		int type;
		cin >> type >> n;
		if (type)
		{
			if (ans[n] == INF)
				cout << "Illegal\n";
			else
				cout << ans[n] << endl;
		}
		else
		{
			ans2 = INFl;
			dfs(0, 1, 1);
			if (ans2 == INFl)
				cout << "INF\n";
			else
				cout << ans2 << endl;
		}
	}
	return 0;
}
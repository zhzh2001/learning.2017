#include <iostream>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		int phi = n;
		for (int i = 2; i * i <= n; i++)
			if (n % i == 0)
			{
				phi = phi / i * (i - 1);
				while (n % i == 0)
					n /= i;
			}
		if (n > 1)
			phi = phi / n * (n - 1);
		cout << phi << endl;
	}
	return 0;
}
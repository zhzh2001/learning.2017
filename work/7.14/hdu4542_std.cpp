#include <cstdio>
#include <algorithm>
#define ull unsigned long long
using namespace std;
ull const INF = 1ull << 62 + 1;
int const LIM = 0x3fffffff;
int const MAX = 50000;
ull k, ans, ma;
bool flag;
int p[16] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47};
int re[MAX], num[MAX];

void DFS(int pos, ull val, ull num)
{
	if (num > k || pos > 14)
		return;
	if (num == k)
	{
		flag = true;
		ans = min(ans, val);
		return;
	}
	for (int i = 1; i <= 62; i++)
	{
		if (val > ans / p[pos] || num * (i + 1) > k)
			break;
		val *= p[pos];
		if (k % (num * (i + 1)) == 0)
			DFS(pos + 1, val, num * (i + 1));
	}
	return;
}

void pre()
{
	//re[i]记录的是与i互质的数的个数
	//num[i]记录的是与i互质的个数中最小的那个
	for (int i = 1; i <= MAX; i++)
	{
		num[i] = LIM;
		re[i] = i - 1;
	}
	for (int i = 1; i <= MAX; i++)
		for (int j = i + i; j <= MAX; j += i)
			re[j]--;
	for (int i = 1; i <= MAX; i++)
		num[re[i]] = min(num[re[i]], i);
}

int main()
{
	pre();
	int T, ca = 1;
	scanf("%d", &T);
	while (T--)
	{
		int tp;
		scanf("%d %llu", &tp, &k);
		printf("Case %d: ", ca++);
		if (tp == 0)
		{
			ans = INF;
			DFS(0, 1, 1);
			if (!flag)
				printf("Illegal\n");
			else if (ans == INF)
				printf("INF\n");
			else
				printf("%llu\n", ans);
		}
		else
		{
			if (num[k] == LIM)
				printf("Illegal\n");
			else
				printf("%d\n", num[k]);
		}
	}
}
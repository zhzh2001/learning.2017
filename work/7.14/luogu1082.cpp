#include <iostream>
using namespace std;
void exgcd(int a, int b, int &x, int &y)
{
	if (!b)
	{
		x = 1;
		y = 0;
	}
	else
	{
		exgcd(b, a % b, x, y);
		int t = x;
		x = y;
		y = t - a / b * y;
	}
}
int main()
{
	int a, b;
	cin >> a >> b;
	int x, y;
	exgcd(a, b, x, y);
	cout << (x % b + b) % b << endl;
	return 0;
}
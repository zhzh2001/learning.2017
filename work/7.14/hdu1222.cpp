#include <iostream>
using namespace std;
int gcd(int a, int b)
{
	return b ? gcd(b, a % b) : a;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int m, n;
		cin >> m >> n;
		if (gcd(n, m) == 1)
			cout << "NO\n";
		else
			cout << "YES\n";
	}
	return 0;
}
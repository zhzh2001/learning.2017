#include <iostream>
using namespace std;
const int n = 3e6;
int p[n / 10];
long long phi[n + 5];
bool vis[n + 5];
int main()
{
	int pn = 0;
	phi[1] = 1;
	for (int i = 2; i <= n; i++)
	{
		if (!vis[i])
		{
			p[++pn] = i;
			phi[i] = i - 1;
		}
		for (int j = 1; j <= pn && i * p[j] <= n; j++)
		{
			vis[i * p[j]] = true;
			if (i % p[j])
				phi[i * p[j]] = phi[i] * (p[j] - 1);
			else
			{
				phi[i * p[j]] = phi[i] * p[j];
				break;
			}
		}
	}
	for (int i = 2; i <= n; i++)
		phi[i] += phi[i - 1];
	ios::sync_with_stdio(false);
	int l, r;
	while (cin >> l >> r)
		cout << phi[r] - phi[l - 1] << endl;
	return 0;
}
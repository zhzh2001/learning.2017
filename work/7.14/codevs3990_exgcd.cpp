#include <iostream>
using namespace std;
const int N = 15;
long long a[N], m[N];
long long exgcd(long long a, long long b, long long &x, long long &y)
{
	if (!b)
	{
		x = 1;
		y = 0;
		return a;
	}
	long long ret = exgcd(b, a % b, x, y), t = x;
	x = y;
	y = t - a / b * y;
	return ret;
}
int main()
{
	int n;
	long long l, r;
	cin >> n >> l >> r;
	long long M = 1;
	for (int i = 1; i <= n; i++)
	{
		cin >> m[i] >> a[i];
		M *= m[i];
	}
	long long ans = 0;
	for (int i = 1; i <= n; i++)
	{
		long long t = M / m[i], x, y;
		exgcd(t, m[i], x, y);
		ans = (ans + a[i] * t % M * (x % m[i] + m[i])) % M;
	}
	ans += (l - ans + M - 1) / M * M;
	if (ans > r)
		cout << 0 << endl
			 << 0 << endl;
	else
		cout << (r - ans) / M + 1 << endl
			 << ans << endl;
	return 0;
}
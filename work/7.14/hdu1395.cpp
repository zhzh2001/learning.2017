#include <iostream>
using namespace std;
int qpow(int a, int b, int mod)
{
	int ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % mod;
		a = a * a % mod;
	} while (b /= 2);
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	while (cin >> n)
		if (n % 2 == 0 || n == 1)
			cout << "2^? mod " << n << " = 1\n";
		else
		{
			int phi = n, now = n;
			for (int i = 2; i * i <= now; i++)
				if (now % i == 0)
				{
					phi = phi / i * (i - 1);
					while (now % i == 0)
						now /= i;
				}
			if (now > 1)
				phi = phi / now * (now - 1);
			int ans = phi;
			for (int i = 2; i * i <= phi; i++)
				if (phi % i == 0)
				{
					if (qpow(2, i, n) == 1)
						ans = min(ans, i);
					if (qpow(2, phi / i, n) == 1)
						ans = min(ans, phi / i);
				}
			cout << "2^" << ans << " mod " << n << " = 1\n";
		}
	return 0;
}
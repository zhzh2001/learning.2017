#include <iostream>
using namespace std;
const int mod = 9973;
int qpow(long long a, int b)
{
	long long ans = 1;
	do
	{
		if (b & 1)
			ans = ans * a % mod;
		a = a * a % mod;
	} while (b /= 2);
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t;
	while (t--)
	{
		int n, b;
		cin >> n >> b;
		cout << n * qpow(b, mod - 2) % mod << endl;
	}
	return 0;
}
#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
const int N = 2005, V = 305, INF = 0x3f3f3f3f;
int c[N], d[N], mat[V][V];
double p[N], f[N][N][2];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, v, e;
	cin >> n >> m >> v >> e;
	for (int i = 1; i <= v; i++)
		for (int j = 1; j <= v; j++)
			mat[i][j] = i == j ? 0 : INF;
	for (int i = 1; i <= n; i++)
		cin >> c[i];
	for (int i = 1; i <= n; i++)
		cin >> d[i];
	for (int i = 1; i <= n; i++)
		cin >> p[i];
	while (e--)
	{
		int u, v, w;
		cin >> u >> v >> w;
		mat[u][v] = min(mat[u][v], w);
		mat[v][u] = min(mat[v][u], w);
	}
	for (int k = 1; k <= v; k++)
		for (int i = 1; i <= v; i++)
			for (int j = 1; j <= v; j++)
				mat[i][j] = min(mat[i][j], mat[i][k] + mat[k][j]);
	fill_n(&f[0][0][0], sizeof(f) / sizeof(double), HUGE_VAL);
	f[1][0][0] = f[1][1][1] = .0;
	for (int i = 2; i <= n; i++)
		for (int j = 0; j <= i && j <= m; j++)
		{
			f[i][j][0] = f[i - 1][j][0] + mat[c[i - 1]][c[i]];
			if (j)
			{
				f[i][j][0] = min(f[i][j][0], f[i - 1][j][1] + p[i - 1] * mat[d[i - 1]][c[i]] + (1 - p[i - 1]) * mat[c[i - 1]][c[i]]);
				f[i][j][1] = f[i - 1][j - 1][0] + p[i] * mat[c[i - 1]][d[i]] + (1 - p[i]) * mat[c[i - 1]][c[i]];
				if (j - 1)
					f[i][j][1] = min(f[i][j][1], f[i - 1][j - 1][1] + p[i - 1] * p[i] * mat[d[i - 1]][d[i]] + (1 - p[i - 1]) * p[i] * mat[c[i - 1]][d[i]] + p[i - 1] * (1 - p[i]) * mat[d[i - 1]][c[i]] + (1 - p[i - 1]) * (1 - p[i]) * mat[c[i - 1]][c[i]]);
			}
		}
	double ans = HUGE_VAL;
	for (int i = 0; i <= m; i++)
		ans = min(ans, min(f[n][i][0], f[n][i][1]));
	cout.precision(2);
	cout << fixed << ans << endl;
	return 0;
}
#include <iostream>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n, k, m;
	cin >> n >> k >> m;
	cout << "YES\n";
	while (k--)
	{
		int cnt;
		cin >> cnt;
		int sum = 0;
		while (cnt--)
		{
			int x;
			cin >> x;
			sum = (sum + x) % (n + 1);
		}
		cout << sum + 1 << endl;
	}
	return 0;
}
#include <iostream>
using namespace std;
const int MOD = 1e9 + 7;
int phi(int x)
{
	int ans = x;
	for (int i = 2; i * i <= x; i++)
		if (x % i == 0)
		{
			ans = ans / i * (i - 1);
			while (x % i == 0)
				x /= i;
		}
	if (x > 1)
		ans = ans / x * (x - 1);
	return ans;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	while (cin >> n >> k)
		if (n == 1 || k == 2)
			cout << 1 << endl;
		else if (k == 1)
		{
			int ans = 0;
			for (int i = 1; i * i <= n; i++)
				if (n % i == 0)
				{
					int val = 1ll * phi(i) * phi(n / i) % MOD;
					ans = (ans + val) % MOD;
					if (i * i < n)
						ans = (ans + val) % MOD;
				}
			cout << ans << endl;
		}
		else
			cout << 0 << endl;
	return 0;
}
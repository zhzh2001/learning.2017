#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=200005;
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N],dep[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k])
		{
			f[v[i]]=k;
			dep[v[i]]=dep[k]+1;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int top[N],id[N],rid[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	rid[now]=k;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
int c[N];
//segment tree
struct node
{
	int cnt,lc,rc,lazy;
}tree[N*4];
inline void pushdown(int id)
{
	if(tree[id].lazy)
	{
		tree[id*2].lazy=tree[id*2].lc=tree[id*2].rc=tree[id*2+1].lazy=tree[id*2+1].lc=tree[id*2+1].rc=tree[id].lazy;
		tree[id*2].cnt=tree[id*2+1].cnt=1;
		tree[id].lazy=0;
	}
}
inline void pullup(int id)
{
	tree[id].lc=tree[id*2].lc;
	tree[id].rc=tree[id*2+1].rc;
	tree[id].cnt=tree[id*2].cnt+tree[id*2+1].cnt;
	if(tree[id*2].rc==tree[id*2+1].lc)
		tree[id].cnt--;
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		tree[id].lc=tree[id].rc=c[rid[l]];
		tree[id].cnt=1;
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id);
	}
}
int ql,qr,val;
void modify(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
	{
		tree[id].lc=tree[id].rc=tree[id].lazy=val;
		tree[id].cnt=1;
	}
	else
	{
		if(l<r)
			pushdown(id);
		int mid=(l+r)/2;
		if(ql<=mid)
			modify(id*2,l,mid);
		if(qr>mid)
			modify(id*2+1,mid+1,r);
		pullup(id);
	}
}
void query(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		val+=tree[id].cnt;
	else
	{
		if(l<r)
			pushdown(id);
		int mid=(l+r)/2;
		if(ql<=mid)
			query(id*2,l,mid);
		if(qr>mid)
			query(id*2+1,mid+1,r);
		if(ql<=mid&&qr>mid&&tree[id*2].rc==tree[id*2+1].lc)
			val--;
	}
}
int query(int id,int l,int r,int x)
{
	if(l==r)
		return tree[id].lc;
	pushdown(id);
	int mid=(l+r)/2;
	if(x<=mid)
		return query(id*2,l,mid,x);
	return query(id*2+1,mid+1,r,x);
}
int n;
void modifyChain(int x,int y,int z)
{
	int a=top[x],b=top[y];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		val=z;
		modify(1,1,n);
		x=f[a];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	val=z;
	if(ql>qr)
		swap(ql,qr);
	modify(1,1,n);
}
int queryChain(int x,int y)
{
	int a=top[x],b=top[y];
	val=0;
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		query(1,1,n);
		if(query(1,1,n,id[a])==query(1,1,n,id[f[a]]))
			val--;
		x=f[a];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	if(ql>qr)
		swap(ql,qr);
	query(1,1,n);
	return val;
}
int main()
{
	int m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>c[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1);
	dfs(1,1);
	build(1,1,n);
	while(m--)
	{
		char opt;
		int x,y,z;
		cin>>opt>>x>>y;
		if(opt=='C')
		{
			cin>>z;
			modifyChain(x,y,z);
		}
		else
			cout<<queryChain(x,y)<<endl;
	}
	return 0;
}
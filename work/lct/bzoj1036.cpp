#include<bits/stdc++.h>
using namespace std;
const int N=30005,M=60005,INF=0x3f3f3f3f;
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N],son[N],dep[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k])
		{
			dep[v[i]]=dep[k]+1;
			f[v[i]]=k;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int now,top[N],id[N],rid[N];
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	rid[now]=k;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
int a[N];
//segment tree
struct node
{
	int sum,max;
}tree[N*4];
inline void pullup(int id)
{
	tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
	tree[id].max=max(tree[id*2].max,tree[id*2+1].max);
}
void build(int id,int l,int r)
{
	if(l==r)
		tree[id].max=tree[id].sum=a[rid[l]];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id);
	}
}
int L,R,val,ans;
void modify(int id,int l,int r)
{
	if(l==r)
		tree[id].max=tree[id].sum=val;
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			modify(id*2,l,mid);
		else
			modify(id*2+1,mid+1,r);
		pullup(id);
	}
}
void queryMax(int id,int l,int r)
{
	if(L<=l&&R>=r)
		ans=max(ans,tree[id].max);
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			queryMax(id*2,l,mid);
		if(R>mid)
			queryMax(id*2+1,mid+1,r);
	}
}
void querySum(int id,int l,int r)
{
	if(L<=l&&R>=r)
		ans+=tree[id].sum;
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			querySum(id*2,l,mid);
		if(R>mid)
			querySum(id*2+1,mid+1,r);
	}
}
int n;
int QMAX(int x,int y)
{
	int a=top[x],b=top[y],ret=-INF;
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		L=id[a];R=id[x];
		ans=-INF;
		queryMax(1,1,n);
		ret=max(ret,ans);
		x=f[a];
		a=top[x];
	}
	L=id[x];R=id[y];
	if(L>R)
		swap(L,R);
	ans=-INF;
	queryMax(1,1,n);
	return max(ret,ans);
}
int QSUM(int x,int y)
{
	int a=top[x],b=top[y],ret=0;
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		L=id[a];R=id[x];
		ans=0;
		querySum(1,1,n);
		ret+=ans;
		x=f[a];
		a=top[x];
	}
	L=id[x];R=id[y];
	if(L>R)
		swap(L,R);
	ans=0;
	querySum(1,1,n);
	return ret+ans;
}
int main()
{
	scanf("%d",&n);
	for(int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1);
	dfs(1,1);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	build(1,1,n);
	int m;
	scanf("%d",&m);
	while(m--)
	{
		char opt[10];
		int x,y;
		scanf("%s%d%d",opt,&x,&y);
		if(!strcmp(opt,"CHANGE"))
		{
			L=id[x];
			val=y;
			modify(1,1,n);
		}
		if(!strcmp(opt,"QMAX"))
			printf("%d\n",QMAX(x,y));
		if(!strcmp(opt,"QSUM"))
			printf("%d\n",QSUM(x,y));
	}
	return 0;
}
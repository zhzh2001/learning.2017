#include<bits/stdc++.h>
using namespace std;
const int N=300005;
struct node
{
	int f,ch[2],val,sum;
	bool rev;
}tree[N];
inline void pushdown(int id)
{
	if(tree[id].rev)
	{
		swap(tree[id].ch[0],tree[id].ch[1]);
		tree[tree[id].ch[0]].rev^=1;
		tree[tree[id].ch[1]].rev^=1;
		tree[id].rev=false;
	}
}
inline void pullup(int id)
{
	tree[id].sum=tree[tree[id].ch[0]].sum^tree[tree[id].ch[1]].sum^tree[id].val;
}
inline bool root(int id)
{
	return tree[tree[id].f].ch[0]!=id&&tree[tree[id].f].ch[1]!=id;
}
inline bool getd(int id)
{
	return tree[tree[id].f].ch[1]==id;
}
inline void sc(int fat,int ch,int d)
{
	tree[ch].f=fat;
	tree[fat].ch[d]=ch;
}
void rotate(int id)
{
	int f=tree[id].f,d=getd(id);
	if(root(f))
		tree[id].f=0;
	else
		sc(tree[f].f,id,getd(f));
	sc(f,tree[id].ch[d^1],d);
	sc(id,f,d^1);
	pullup(f);
	pullup(id);
}
void splay(int id)
{
	static int Q[N];
	int cc=0;
	Q[++cc]=id;
	for(int i=id;!root(i);i=tree[i].f)
		Q[++cc]=tree[i].f;
	for(;cc;cc--)
		pushdown(Q[cc]);
	while(!root(id))
	{
		int f=tree[id].f;
		if(!root(f))
			if(getd(id)==getd(f))
				rotate(f);
			else
				rotate(id);
		rotate(id);
	}
}
void access(int id)
{
	int u=0;
	while(id)
	{
		splay(id);
		tree[id].ch[1]=u;
		pullup(id);
		u=id;
		id=tree[id].f;
	}
}
void evert(int id)
{
	access(id);
	splay(id);
	tree[id].rev^=1;
}
int findroot(int id)
{
	access(id);
	splay(id);
	while(tree[id].ch[0])
		id=tree[id].ch[0];
	return id;
}
void link(int x,int y)
{
	evert(x);
	tree[x].f=y;
}
void cut(int x,int y)
{
	access(x);
	splay(y);
	if(tree[y].f==x)
		tree[y].f=0;
	else
	{
		access(y);
		splay(x);
		tree[x].f=0;
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>tree[i].val;
		tree[i].sum=tree[i].val;
	}
	while(m--)
	{
		int opt,x,y;
		cin>>opt>>x>>y;
		switch(opt)
		{
			case 0:
				evert(x);
				access(y);
				splay(y);
				cout<<tree[y].sum<<endl;
				break;
			case 1:
				if(findroot(x)!=findroot(y))
					link(x,y);
				break;
			case 2:
				if(findroot(x)==findroot(y))
					cut(x,y);
				break;
			case 3:
				splay(x);
				tree[x].val=y;
				pullup(x);
				break;
		}
	}
	return 0;
}
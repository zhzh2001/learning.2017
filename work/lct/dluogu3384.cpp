#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=200005;
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N],dep[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k])
		{
			f[v[i]]=k;
			dep[v[i]]=dep[k]+1;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int top[N],id[N],rid[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	rid[now]=k;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
int a[N];
//segment tree
struct node
{
	int sum,lazy;
}tree[N*4];
int p;
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy&&l<r)
	{
		int mid=(l+r)/2;
		tree[id*2].lazy=(tree[id*2].lazy+tree[id].lazy)%p;
		tree[id*2].sum=(tree[id*2].sum+(long long)(mid-l+1)*tree[id].lazy)%p;
		tree[id*2+1].lazy=(tree[id*2+1].lazy+tree[id].lazy)%p;
		tree[id*2+1].sum=(tree[id*2+1].sum+(long long)(r-mid)*tree[id].lazy)%p;
		tree[id].lazy=0;
	}
}
void build(int id,int l,int r)
{
	if(l==r)
		tree[id].sum=a[rid[l]];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id].sum=(tree[id*2].sum+tree[id*2+1].sum)%p;
	}
}
int ql,qr,val,ans;
void modify(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
	{
		tree[id].sum=(tree[id].sum+(long long)(r-l+1)*val)%p;
		tree[id].lazy=(tree[id].lazy+val)%p;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			modify(id*2,l,mid);
		if(qr>mid)
			modify(id*2+1,mid+1,r);
		tree[id].sum=(tree[id*2].sum+tree[id*2+1].sum)%p;
	}
}
void query(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		ans=(ans+tree[id].sum)%p;
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			query(id*2,l,mid);
		if(qr>mid)
			query(id*2+1,mid+1,r);
	}
}
int n;
void modifyChain(int x,int y,int z)
{
	int a=top[x],b=top[y];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		val=z;
		modify(1,1,n);
		x=f[a];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	if(ql>qr)
		swap(ql,qr);
	val=z;
	modify(1,1,n);
}
int queryChain(int x,int y)
{
	int a=top[x],b=top[y],ret=0;
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		ans=0;
		query(1,1,n);
		ret=(ret+ans)%p;
		x=f[a];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	if(ql>qr)
		swap(ql,qr);
	ans=0;
	query(1,1,n);
	return (ret+ans)%p;
}
void modifySubtree(int x,int z)
{
	ql=id[x];qr=ql+sz[x]-1;
	val=z;
	modify(1,1,n);
}
int querySubtree(int x)
{
	ql=id[x];qr=ql+sz[x]-1;
	ans=0;
	query(1,1,n);
	return ans;
}
int main()
{
	int m,r;
	cin>>n>>m>>r>>p;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(r);
	dfs(r,r);
	build(1,1,n);
	while(m--)
	{
		int opt,x,y,z;
		cin>>opt;
		switch(opt)
		{
			case 1:
				cin>>x>>y>>z;
				modifyChain(x,y,z);
				break;
			case 2:
				cin>>x>>y;
				cout<<queryChain(x,y)<<endl;
				break;
			case 3:
				cin>>x>>z;
				modifySubtree(x,z);
				break;
			case 4:
				cin>>x;
				cout<<querySubtree(x)<<endl;
				break;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
#include<windows.h>
using namespace std;
const int n=10,m=10,val=10;
int f[n+1];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	minstd_rand gen(GetTickCount());
	cout<<n<<' '<<m<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	uniform_int_distribution<> dn(1,n);
	for(int i=1;i<n;)
	{
		int u=dn(gen),v=dn(gen);
		int ru=getf(u),rv=getf(v);
		if(ru!=rv)
		{
			f[ru]=rv;
			cout<<u<<' '<<v<<endl;
			i++;
		}
	}
	uniform_int_distribution<> dval(1,val);
	for(int i=1;i<=n;i++)
		cout<<dval(gen)<<' ';
	cout<<endl;
	cout<<1<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> dopt(1,3);
		switch(dopt(gen))
		{
			case 1:
				cout<<1<<' '<<dn(gen)<<endl;
				break;
			case 2:
				cout<<2<<' '<<dn(gen)<<' '<<dn(gen)<<' '<<dval(gen)<<endl;
				break;
			case 3:
				cout<<3<<' '<<dn(gen)<<endl;
				break;
		}
	}
	return 0;
}
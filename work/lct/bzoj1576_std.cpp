#include<iostream>
#include<cstdio>
#include<algorithm>
#include<queue>
#include<cstring>
#define pa pair<int,int>
#define ll long long 
#define inf 1000000000
using namespace std;
inline int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int bin[20];
int n,m,cnt,sz,place;
int last[100005],dis[100005],from[100005];
int u[200005],v[200005],w[200005];
int fa[100005][17],belong[100005],deep[100005],pl[100005],size[100005];
bool vis[100005],mark[400005];
struct edge{int to,next,v;}e[400005];
struct seg{int l,r,v,tag;}t[400005];
void insert(int u,int v,int w)
{
	e[++cnt].to=v;e[cnt].next=last[u];last[u]=cnt;e[cnt].v=w;
	e[++cnt].to=u;e[cnt].next=last[v];last[v]=cnt;e[cnt].v=w;
}
void dijkstra()
{
	priority_queue<pa,vector<pa>,greater<pa> > q;
	for(int i=1;i<=n;i++)dis[i]=inf;
	dis[1]=0;q.push(make_pair(0,1));
	while(!q.empty())
	{
		int now=q.top().second;q.pop();
		if(vis[now])continue;vis[now]=1;
		for(int i=last[now];i;i=e[i].next)
			if(dis[now]+e[i].v<dis[e[i].to])
			{
				dis[e[i].to]=dis[now]+e[i].v;
				mark[from[e[i].to]]=0;from[e[i].to]=i;mark[i]=1;
				q.push(make_pair(dis[e[i].to],e[i].to));
			}
	}
}
void dfs1(int x)
{
	size[x]=1;
	for(int i=1;i<=16;i++)
		if(deep[x]>=bin[i])
			fa[x][i]=fa[fa[x][i-1]][i-1];
		else break;
	for(int i=last[x];i;i=e[i].next)
	{
		if(!mark[i])continue;
		fa[e[i].to][0]=x;
		deep[e[i].to]=deep[x]+1;
		dfs1(e[i].to);
		size[x]+=size[e[i].to];
	}
}
void dfs2(int x,int chain)
{
	belong[x]=chain;pl[x]=++place;
	int k=0;
	for(int i=last[x];i;i=e[i].next)
		if(mark[i]&&size[e[i].to]>size[k])
			k=e[i].to;
	if(k)dfs2(k,chain);
	for(int i=last[x];i;i=e[i].next)
		if(mark[i]&&e[i].to!=k)
			dfs2(e[i].to,e[i].to);
}
int lca(int x,int y)
{
	if(deep[x]<deep[y])swap(x,y);
	int t=deep[x]-deep[y];
	for(int i=0;i<=16;i++)
		if(t&bin[i])x=fa[x][i];
	for(int i=16;i>=0;i--)
		if(fa[x][i]!=fa[y][i])
			x=fa[x][i],y=fa[y][i];
	if(x==y)return x;
	return fa[x][0];
}
void pushdown(int k)
{
	if(t[k].l==t[k].r||t[k].tag==inf)return;
	int tag=t[k].tag;t[k].tag=inf;
	t[k<<1].tag=min(t[k<<1].tag,tag);
	t[k<<1|1].tag=min(t[k<<1|1].tag,tag);
	t[k<<1].v=min(t[k<<1].v,tag);
	t[k<<1|1].v=min(t[k<<1|1].v,tag);
}
void build(int k,int l,int r)
{
	t[k].l=l,t[k].r=r;t[k].v=t[k].tag=inf;
	if(l==r)return;
	int mid=(l+r)>>1;
	build(k<<1,l,mid);build(k<<1|1,mid+1,r);
}
void modify(int k,int x,int y,int v)
{
    pushdown(k);
    int l=t[k].l,r=t[k].r;
    if(x==l&&y==r)
    {
        t[k].tag=min(t[k].tag,v);
        if(l==r)t[k].v=min(v,t[k].v);
        return;
    }
    int mid=(l+r)>>1;
    if(y<=mid)modify(k<<1,x,y,v);
    else if(x>mid)modify(k<<1|1,x,y,v);
    else {modify(k<<1,x,mid,v);modify(k<<1|1,mid+1,y,v);}
}
int query(int k,int x)
{
	pushdown(k);
	int l=t[k].l,r=t[k].r;
	if(l==r)return t[k].v;
	int mid=(l+r)>>1;
	if(x<=mid)return query(k<<1,x);
	else return query(k<<1|1,x);
}
void solve(int x,int f,int val)
{
	while(belong[x]!=belong[f])
	{
		modify(1,pl[belong[x]],pl[x],val);
		x=fa[belong[x]][0];
	}
	if(x!=f)modify(1,pl[f]+1,pl[x],val);
}
int main()
{
	bin[0]=1;for(int i=1;i<20;i++)bin[i]=bin[i-1]<<1;
	n=read();m=read();
	for(int i=1;i<=m;i++)
	{
		u[i]=read(),v[i]=read(),w[i]=read();
		insert(u[i],v[i],w[i]);
	}
	dijkstra();
	dfs1(1);dfs2(1,1);
	build(1,1,n);
	for(int i=1;i<=m;i++)
	{
		int f=lca(u[i],v[i]);
		if(!mark[2*i-1])solve(v[i],f,dis[u[i]]+dis[v[i]]+w[i]);
		if(!mark[2*i])solve(u[i],f,dis[u[i]]+dis[v[i]]+w[i]);
  	}
    for(int i=2;i<=n;i++)
    {
		int t=query(1,pl[i]);
		if(t!=inf)printf("%d\n",t-dis[i]);
		else puts("-1");
	}
	return 0;
}
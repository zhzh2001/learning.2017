#include<bits/stdc++.h>
using namespace std;
const int N=100005,LOGN=18,M=200005;
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N][LOGN],dep[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k][0])
		{
			f[v[i]][0]=k;
			dep[v[i]]=dep[k]+1;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int top[N],id[N],rid[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	rid[now]=k;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k][0]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
int a[N];
//segment tree
struct node
{
	int min,lazy;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy&&l<r)
	{
		tree[id*2].lazy=tree[id*2].min=tree[id*2+1].lazy=tree[id*2+1].min=tree[id].lazy;
		tree[id].lazy=0;
	}
}
void build(int id,int l,int r)
{
	if(l==r)
		tree[id].min=a[rid[l]];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id].min=min(tree[id*2].min,tree[id*2+1].min);
	}
}
int ql,qr,val,ans;
void modify(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		tree[id].min=tree[id].lazy=val;
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			modify(id*2,l,mid);
		if(qr>mid)
			modify(id*2+1,mid+1,r);
		tree[id].min=min(tree[id*2].min,tree[id*2+1].min);
	}
}
void query(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		ans=min(ans,tree[id].min);
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			query(id*2,l,mid);
		if(qr>mid)
			query(id*2+1,mid+1,r);
	}
}
int n;
void modifyChain(int x,int y,int z)
{
	int a=top[x],b=top[y];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		val=z;
		modify(1,1,n);
		x=f[a][0];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	if(ql>qr)
		swap(ql,qr);
	val=z;
	modify(1,1,n);
}
int root;
int querySubtree(int x)
{
	if(x==root)
		return tree[1].min;
	if(id[x]<id[root]&&id[x]+sz[x]>=id[root]+sz[root])
	{
		int delta=dep[root]-dep[x]-1,t=root;
		for(int i=0;delta;delta/=2,i++)
			if(delta&1)
				t=f[t][i];
		ql=1;qr=id[t]-1;
		ans=numeric_limits<int>::max();
		query(1,1,n);
		ql=id[t]+sz[t];qr=n;
		query(1,1,n);
	}
	else
	{
		ql=id[x];qr=ql+sz[x]-1;
		ans=numeric_limits<int>::max();
		query(1,1,n);
	}
	return ans;
}
int main()
{
	int m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1);
	for(int i=1;i<LOGN;i++)
		for(int j=1;j<=n;j++)
			f[j][i]=f[f[j][i-1]][i-1];
	dfs(1,1);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	build(1,1,n);
	scanf("%d",&root);
	while(m--)
	{
		int opt,x,y,z;
		scanf("%d",&opt);
		switch(opt)
		{
			case 1:
				scanf("%d",&root);
				break;
			case 2:
				scanf("%d%d%d",&x,&y,&z);
				modifyChain(x,y,z);
				break;
			case 3:
				scanf("%d",&x);
				printf("%d\n",querySubtree(x));
				break;
		}
	}
	return 0;
}
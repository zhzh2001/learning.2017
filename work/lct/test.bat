@echo off
for /l %%i in (1,1,100000) do (
echo No.%%i
gen3083.exe > 3083.in
bzoj3083.exe < 3083.in > 3083.out
bzoj3083_std.exe < 3083.in > 3083.ans
fc 3083.out 3083.ans
if errorlevel 1 pause
)
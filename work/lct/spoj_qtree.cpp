#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=20005,INF=0x3f3f3f3f;
int head[N],v[M],w[M],nxt[M],eid[M],e;
inline void add_edge(int u,int v,int w,int eid)
{
	::v[++e]=v;
	::w[e]=w;
	::eid[e]=eid;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N],son[N],dep[N],edge[N],redge[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k])
		{
			dep[v[i]]=dep[k]+1;
			f[v[i]]=k;
			edge[v[i]]=eid[i];
			redge[eid[i]]=v[i];
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int now,top[N],id[N],rid[N];
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	rid[now]=k;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
//segment tree
int tree[N*4];
void build(int id,int l,int r)
{
	if(l==r)
		tree[id]=w[edge[rid[l]]*2];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id]=max(tree[id*2],tree[id*2+1]);
	}
}
int L,R,val,ans;
void modify(int id,int l,int r)
{
	if(l==r)
		tree[id]=val;
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			modify(id*2,l,mid);
		else
			modify(id*2+1,mid+1,r);
		tree[id]=max(tree[id*2],tree[id*2+1]);
	}
}
void query(int id,int l,int r)
{
	if(L<=l&&R>=r)
		ans=max(ans,tree[id]);
	else
	{
		int mid=(l+r)/2;
		if(L<=mid)
			query(id*2,l,mid);
		if(R>mid)
			query(id*2+1,mid+1,r);
	}
}
int n;
void CHANGE(int x,int y)
{
	L=id[redge[x]];
	val=y;
	modify(1,1,n);
}
int QUERY(int x,int y)
{
	int a=top[x],b=top[y],ret=-INF;
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		L=id[a];R=id[x];
		ans=-INF;
		query(1,1,n);
		ret=max(ret,ans);
		x=f[a];
		a=top[x];
	}
	L=id[x];R=id[y];
	if(L>R)
		swap(L,R);
	L++;
	ans=-INF;
	query(1,1,n);
	return max(ret,ans);
}
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		scanf("%d",&n);
		e=now=0;
		memset(head,0,sizeof(head));
		for(int i=1;i<n;i++)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			add_edge(u,v,w,i);
			add_edge(v,u,w,i);
		}
		memset(son,0,sizeof(son));
		dfs(1);
		dfs(1,1);
		build(1,1,n);
		char opt[10];
		while(scanf("%s",opt)==1&&opt[0]!='D')
		{
			int x,y;
			scanf("%d%d",&x,&y);
			if(opt[0]=='C')
				CHANGE(x,y);
			else
				printf("%d\n",QUERY(x,y));
		}
	}
	return 0;
}
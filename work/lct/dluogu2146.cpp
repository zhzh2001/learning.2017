#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int head[N],v[N],nxt[N],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],f[N],dep[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
	{
		f[v[i]]=k;
		dep[v[i]]=dep[k]+1;
		dfs(v[i]);
		sz[k]+=sz[v[i]];
		if(sz[v[i]]>sz[son[k]])
			son[k]=v[i];
	}
}
int top[N],id[N],rid[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	rid[now]=k;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=son[k])
			dfs(v[i],v[i]);
}
int a[N];
//segment tree
struct node
{
	bool lazy;
	int sum,tag;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy&&l<r)
	{
		int mid=(l+r)/2;
		tree[id*2].lazy=true;
		tree[id*2].tag=tree[id].tag;
		tree[id*2].sum=tree[id].tag*(mid-l+1);
		tree[id*2+1].lazy=true;
		tree[id*2+1].tag=tree[id].tag;
		tree[id*2+1].sum=tree[id].tag*(r-mid);
		tree[id].lazy=false;
	}
}
int ql,qr,val,ans;
void modify(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
	{
		tree[id].sum=val*(r-l+1);
		tree[id].tag=val;
		tree[id].lazy=true;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			modify(id*2,l,mid);
		if(qr>mid)
			modify(id*2+1,mid+1,r);
		tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
	}
}
void query(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		ans+=tree[id].sum;
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			query(id*2,l,mid);
		if(qr>mid)
			query(id*2+1,mid+1,r);
	}
}
int n;
void modifyChain(int x,int y,int z)
{
	int a=top[x],b=top[y];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		val=z;
		modify(1,1,n);
		x=f[a];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	if(ql>qr)
		swap(ql,qr);
	val=z;
	modify(1,1,n);
}
int queryChain(int x,int y)
{
	int a=top[x],b=top[y],ret=0;
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		ql=id[a];qr=id[x];
		ans=0;
		query(1,1,n);
		ret+=ans;
		x=f[a];
		a=top[x];
	}
	ql=id[x];qr=id[y];
	if(ql>qr)
		swap(ql,qr);
	ans=0;
	query(1,1,n);
	return ret+ans;
}
void modifySubtree(int x,int z)
{
	ql=id[x];qr=ql+sz[x]-1;
	val=z;
	modify(1,1,n);
}
int querySubtree(int x)
{
	ql=id[x];qr=ql+sz[x]-1;
	ans=0;
	query(1,1,n);
	return ans;
}
int main()
{
	cin>>n;
	for(int i=2;i<=n;i++)
	{
		int p;
		cin>>p;
		p++;
		add_edge(p,i);
	}
	dfs(1);
	dfs(1,1);
	int m;
	cin>>m;
	while(m--)
	{
		string opt;
		int x;
		cin>>opt>>x;
		x++;
		if(opt=="install")
		{
			cout<<dep[x]+1-queryChain(1,x)<<endl;
			modifyChain(1,x,1);
		}
		else
		{
			cout<<querySubtree(x)<<endl;
			modifySubtree(x,0);
		}
	}
	return 0;
}
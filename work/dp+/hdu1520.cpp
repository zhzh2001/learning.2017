#include<bits/stdc++.h>
using namespace std;
const int N=6005;
int a[N],f[N][2];
vector<int> mat[N];
bool vis[N];
void dfs(int k)
{
	f[k][0]=0;
	f[k][1]=a[k];
	for(int i=0;i<mat[k].size();i++)
	{
		dfs(mat[k][i]);
		f[k][0]+=max(f[mat[k][i]][0],f[mat[k][i]][1]);
		f[k][1]+=f[mat[k][i]][0];
	}
}
int main()
{
	int n;
	while(cin>>n)
	{
		for(int i=1;i<=n;i++)
		{
			cin>>a[i];
			mat[i].clear();
		}
		memset(vis,false,sizeof(vis));
		for(int i=1;i<n;i++)
		{
			int u,v;
			cin>>u>>v;
			mat[v].push_back(u);
			vis[u]=true;
		}
		cin.ignore(5);
		for(int i=1;i<=n;i++)
			if(!vis[i])
			{
				dfs(i);
				cout<<max(f[i][0],f[i][1])<<endl;
				break;
			}
	}
	return 0;
}
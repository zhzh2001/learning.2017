#include<bits/stdc++.h>
using namespace std;
const int N=18,TIMES=10000;
const double eps=1e-8;
struct point
{
	double x,y;
}p[N];
istream& operator>>(istream& is,point& p)
{
	return is>>p.x>>p.y;
}
bool vis[N];
int main()
{
	srand(time(NULL));
	int t;
	cin>>t;
	while(t--)
	{
		int n,m;
		cin>>n>>m;
		for(int i=0;i<n;i++)
			cin>>p[i];
		int ans=n;
		for(int c=0;c<TIMES;c++)
		{
			random_shuffle(p,p+n);
			memset(vis,false,sizeof(vis));
			int now=0;
			for(int i=0;i<n;i++)
				for(int j=n-1;j>=0;j--)
					if(!vis[i]&&!vis[j])
					{
						double a=(p[j].x*p[i].y-p[i].x*p[j].y)/(p[i].x*p[i].x*p[j].x-p[i].x*p[j].x*p[j].x);
						if(a<-eps)
						{
							double b=(p[j].x*p[j].x*p[i].y-p[i].x*p[i].x*p[j].y)/(p[i].x*p[j].x*p[j].x-p[i].x*p[i].x*p[j].x);
							for(int k=0;k<n;k++)
							{
								double y=a*p[k].x*p[k].x+b*p[k].x;
								if(fabs(y-p[k].y)<eps)
									vis[k]=true;
							}
						}
						else
							vis[i]=true;
						now++;
					}
			ans=min(ans,now);
		}
		cout<<ans<<endl;
	}
	return 0;
}
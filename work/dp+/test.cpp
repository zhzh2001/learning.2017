#include<bits/stdc++.h>
using namespace std;
const int n=1e9;
int calc(int base)
{
	long long now=1;
	for(int i=1;;i++)
	{
		now*=base;
		if(now>n)
			return i;
	}
}
int main()
{
	int c2=calc(2),c3=calc(3),c5=calc(5),c7=calc(7);
	cout<<c2<<' '<<c3<<' '<<c5<<' '<<c7<<endl;
	int cnt=0;
	for(int i=0;i<=c2;i++)
		for(int j=0;j<=c3;j++)
			for(int k=0;k<=c5;k++)
				for(int l=0;l<=c7;l++)
				{
					double ans=pow(2,i)*pow(3,j)*pow(5,k)*pow(7,l);
					if(ans<=1e9)
						cnt++;
				}
	cout<<cnt<<endl;
	return 0;
}
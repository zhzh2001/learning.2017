program gen;
const
  n=1000000000;
var
  lh,ll,rh,rl:int64;
begin
  randomize;
  lh:=random(n);
  ll:=random(n-1)+1;
  rh:=random(n);
  rl:=random(n-1)+1;
  writeln(random(n)+1,' ',lh*n+ll,' ',rh*n+rl);
end.
#include<bits/stdc++.h>
using namespace std;
int main()
{
	int n,l,r;
	cin>>n>>l>>r;
	int ans=0;
	for(int i=l;i<r;i++)
	{
		int x=i;
		long long now=1;
		do
		{
			now*=x%10;
			if(now>n||now==0)
				break;
		}
		while(x/=10);
		ans+=(now>0&&now<=n);
	}
	cout<<ans<<endl;
	return 0;
}
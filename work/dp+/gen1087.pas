program gen;
const
  n=9;
var
  i,j:longint;
begin
  assign(output,'chess.in');
  rewrite(output);
  for i:=1 to n do
    for j:=0 to i*i do
	  writeln(i,' ',j);
  close(output);
end.
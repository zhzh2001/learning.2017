#include<bits/stdc++.h>
using namespace std;
int f[11][10];
inline int query(int x)
{
	if(x==0)
		return 0;
	int len=floor(log10(x))+1,ans=0;
	for(int i=1;i<len;i++)
		for(int j=1;j<10;j++)
			ans+=f[i][j];
	int base=1;
	for(int i=1;i<len;i++)
		base*=10;
	int pred=x/base;
	for(int i=1;(i<pred)||(i==pred&&len==1);i++)
		ans+=f[len][i];
	base/=10;
	for(int i=len-1;i;i--,base/=10)
	{
		int now=x/base%10;
		for(int j=0;j<now||(j==now&&i==1);j++)
			if(abs(j-pred)>=2)
				ans+=f[i][j];
		if(abs(now-pred)<2)
			break;
		pred=now;
	}
	return ans;
}
int main()
{
	for(int i=0;i<10;i++)
		f[1][i]=1;
	for(int i=2;i<=10;i++)
		for(int j=0;j<10;j++)
			for(int k=0;k<10;k++)
				if(abs(j-k)>=2)
					f[i][j]+=f[i-1][k];
	int a,b;
	cin>>a>>b;
	cout<<query(b)-query(a-1)<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=18;
const double eps=1e-8;
struct point
{
	double x,y;
}p[N];
int f[1<<N],mask[N][N];
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n,m;
		cin>>n>>m;
		for(int i=0;i<n;i++)
			cin>>p[i].x>>p[i].y;
		memset(mask,0,sizeof(mask));
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
			{
				double a=(p[j].x*p[i].y-p[i].x*p[j].y)/(p[i].x*p[i].x*p[j].x-p[i].x*p[j].x*p[j].x);
				if(a<-eps)
				{
					double b=(p[j].x*p[j].x*p[i].y-p[i].x*p[i].x*p[j].y)/(p[i].x*p[j].x*p[j].x-p[i].x*p[i].x*p[j].x);
					for(int k=0;k<n;k++)
					{
						double y=a*p[k].x*p[k].x+b*p[k].x;
						if(fabs(y-p[k].y)<eps)
							mask[i][j]|=1<<k;
					}
				}
				else
					mask[i][j]=1<<i;
			}
		int nn=(1<<n)-1;
		memset(f,0x3f,sizeof(f));
		f[0]=0;
		for(int i=0;i<nn;i++)
		{
			int j;
			for(j=0;i&(1<<j);j++);
			for(int k=0;k<n;k++)
			{
				int t=i|mask[j][k];
				f[t]=min(f[t],f[i]+1);
			}
		}
		cout<<f[nn]<<endl;
	}
	return 0;
}
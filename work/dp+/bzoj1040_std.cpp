#include<cstdio>
#include<algorithm>
using namespace std;
#define oo (1<<30)
#define ll long long
#define N 1000010
int w[N],fa[N],t[N],next[N];
int n,i,x,y;
int vis[N];
ll f[N][2];//0不选的max，1选或不选的max 
ll now,ans;
void chmax(ll &x,ll y)
{
    if (x<y) x=y;
}
int rt;
void dp(int x)
{
    vis[x]=1;f[x][0]=0;f[x][1]=w[x]; 
    for (int y=t[x];y;y=next[y])
    if (y!=rt)
    { 
        dp(y);
        f[x][0]+=f[y][1];
        f[x][1]+=f[y][0];
    } 
    else f[x][1]=-oo;//是当前rt的爸爸，强制不选 
    chmax(f[x][1],f[x][0]);
}
void go()
{
    for (;!vis[rt];rt=fa[rt])vis[rt]=1;//找环 
    dp(rt);now=f[rt][1];
    rt=fa[rt];dp(rt);chmax(now,f[rt][1]);
    ans+=now;
}
int main()
{
    freopen("knight.in","r",stdin);
    scanf("%d",&n);
    for (x=1;x<=n;++x)
    {
        scanf("%d%d",w+x,&y);
        next[x]=t[y];t[y]=x;
        fa[x]=y;
    }
    for (i=1;i<=n;++i) 
    if (!vis[i]) { rt=i;go(); }
    printf("%lld",ans);
}
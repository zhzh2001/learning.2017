program gen;
const
  n=1000000;
var
  i,x:longint;
begin
  randomize;
  assign(output,'knight.in');
  rewrite(output);
  writeln(n);
  for i:=1 to n do
  begin
    repeat
      x:=random(n)+1;
	until x<>i;
	writeln(random(n)+1,' ',x);
  end;
  close(output);
end.
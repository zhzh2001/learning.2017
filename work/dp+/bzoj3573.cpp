#include<bits/stdc++.h>
using namespace std;
const int N=500005;
const double eps=1e-4;
int a[N],cnt[N];
double s[N];
vector<int> mat[N];
void dfs(int k)
{
	for(int i=0;i<mat[k].size();i++)
	{
		s[mat[k][i]]=s[k]+log(cnt[k]);
		dfs(mat[k][i]);
	}
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		cnt[u]++;
	}
	s[1]=log(1);
	dfs(1);
	for(int i=1;i<=n;i++)
		s[i]+=log(a[i]);
	sort(s+1,s+n+1);
	int ans=0,cnt=0;
	for(int i=1;i<=n+1;i++)
		if(fabs(s[i]-s[i-1])>eps)
		{
			ans=max(ans,cnt);
			cnt=1;
		}
		else
			cnt++;
	cout<<n-ans<<endl;
	return 0;
}
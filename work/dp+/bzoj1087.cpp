#include<bits/stdc++.h>
using namespace std;
long long f[10][100][512];
int cnt[512];
int main()
{
	int n,k;
	cin>>n>>k;
	memset(f,0,sizeof(f));
	int nn=1<<n;
	for(int i=0;i<nn;i++)
		if(i&(i>>1))
			cnt[i]=-1;
		else
		{
			cnt[i]=0;
			int x=i;
			do
				cnt[i]+=x&1;
			while(x/=2);
			f[1][cnt[i]][i]=1;
		}
	for(int i=2;i<=n;i++)
		for(int j=0;j<=k;j++)
			for(int now=0;now<nn;now++)
				if(cnt[now]!=-1&&cnt[now]<=j)
					for(int pred=0;pred<nn;pred++)
						if(cnt[pred]!=-1&&!(now&pred)&&!((now<<1)&pred)&&!((now>>1)&pred))
							f[i][j][now]+=f[i-1][j-cnt[now]][pred];
	long long ans=0;
	for(int i=0;i<nn;i++)
		ans+=f[n][k][i];
	cout<<ans<<endl;
	return 0;
}
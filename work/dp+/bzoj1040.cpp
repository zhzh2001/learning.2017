#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
bool vis[N],visx[N][2];
int a[N],fat[N],flag,head[N],v[N],next[N],e;
long long f[N][2];
void dp(int k)
{
	f[k][0]=0;
	f[k][1]=a[k];
	visx[k][flag]=true;
	for(int i=head[k];i;i=next[i])
		if(!visx[v[i]][flag])
		{
			dp(v[i]);
			f[k][0]+=max(f[v[i]][0],f[v[i]][1]);
			f[k][1]+=f[v[i]][0];
		}
}
inline void insert_edge(int u,int v)
{
	::v[++e]=v;
	next[e]=head[u];
	head[u]=e;
}
int main()
{
	int n;
	scanf("%d",&n);
	e=0;
	for(int i=1;i<=n;i++)
	{
		int t;
		scanf("%d%d",a+i,&t);
		fat[i]=t;
		insert_edge(t,i);
	}
	long long ans=0;
	for(int i=1;i<=n;i++)
		if(!vis[i])
		{
			int j;
			for(j=i;!vis[j];j=fat[j])
				vis[j]=true;
			flag=0;
			dp(j);
			long long now=f[j][0];
			flag=1;
			dp(fat[j]);
			ans+=max(now,f[fat[j]][0]);
		}
	printf("%lld\n",ans);
	return 0;
}
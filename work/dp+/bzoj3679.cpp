#include<bits/stdc++.h>
using namespace std;
int s[6000],n,cnt;
long long f[20][10][6000],mul[20];
inline long long ipow(int base,int p)
{
	long long ans=1;
	for(int i=1;i<=p;i++)
		ans*=base;
	return ans;
}
#define _chk if(t>n) return;
inline void chk(int c2,int c3,int c5,int c7)
{
	long long t=ipow(2,c2);
	_chk;
	t*=ipow(3,c3);
	_chk;
	t*=ipow(5,c5);
	_chk;
	t*=ipow(7,c7);
	_chk;
	s[++cnt]=t;
}
long long query(long long x)
{
	if(x==0)
		return 0;
	int len=floor(log10(x))+1;
	long long ans=0;
	for(int i=1;i<len;i++)
		for(int j=1;j<10;j++)
			for(int k=1;k<=cnt;k++)
				ans+=f[i][j][k];
	long long sum=1;
	for(int i=len;i;i--)
	{
		int now=x/mul[i]%10;
		for(int j=1;j<now;j++)
			for(int k=1;k<=cnt;k++)
			{
				if(sum*s[k]>n)
					break;
				ans+=f[i][j][k];
			}
		sum*=now;
		if(sum==0||sum>n)
			break;
	}
	return ans;
}
int main()
{
	long long l,r;
	scanf("%d%lld%lld",&n,&l,&r);
	cnt=0;
	for(int i=0;i<=30;i++)
		for(int j=0;j<=19;j++)
			for(int k=0;k<=13;k++)
				for(int l=0;l<=11;l++)
					chk(i,j,k,l);
	sort(s+1,s+cnt+1);
	for(int i=1;i<=n&&i<10;i++)
		f[1][i][lower_bound(s+1,s+cnt+1,i)-s]=1;
	for(int i=2;i<=18;i++)
		for(int j=1;j<10;j++)
			for(int k=1;k<=cnt;k++)
				for(int pred=1;pred<10;pred++)
					if(s[k]%j==0)
						f[i][j][k]+=f[i-1][pred][lower_bound(s+1,s+cnt+1,s[k]/j)-s];
	mul[1]=1;
	for(int i=2;i<=18;i++)
		mul[i]=mul[i-1]*10;
	printf("%lld\n",query(r)-query(l));
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
struct result
{
	long long cnt[10];
	result()
	{
		memset(cnt,0,sizeof(cnt));
	}
	result operator+(const result& r)const
	{
		result ans;
		for(int i=0;i<10;i++)
			ans.cnt[i]=cnt[i]+r.cnt[i];
		return ans;
	}
	result operator+=(const result& r)
	{
		return *this=*this+r;
	}
	result operator-(const result& r)const
	{
		result ans;
		for(int i=0;i<10;i++)
			ans.cnt[i]=cnt[i]-r.cnt[i];
		return ans;
	}
}f[15][10];
long long mul[15];
result query(long long x)
{
	if(x==0)
		return result();
	int len=floor(log10(x))+1;
	result ans;
	for(int i=1;i<len;i++)
		for(int j=1;j<10;j++)
			ans+=f[i][j];
	int hi=x/mul[len];
	for(int i=1;i<hi;i++)
		ans+=f[len][i];
	ans.cnt[hi]+=x%mul[len]+1;
	for(int i=len-1;i;i--)
	{
		int now=x/mul[i]%10;
		for(int j=0;j<now;j++)
			ans+=f[i][j];
		ans.cnt[now]+=x%mul[i]+1;
	}
	return ans;
}
ostream& operator<<(ostream& os,const result r)
{
	for(int i=0;i<10;i++)
	{
		if(i)
			os.put(' ');
		os<<r.cnt[i];
	}
	return os;
}
int main()
{
	mul[1]=1;
	for(int i=2;i<=13;i++)
		mul[i]=mul[i-1]*10;
	for(int i=0;i<10;i++)
		f[1][i].cnt[i]=1;
	for(int i=2;i<=12;i++)
		for(int j=0;j<10;j++)
			for(int k=0;k<10;k++)
			{
				f[i][j]+=f[i-1][k];
				f[i][j].cnt[j]+=mul[i-1];
			}
	long long a,b;
	cin>>a>>b;
	cout<<query(b)-query(a-1)<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
priority_queue<int,vector<int>,greater<int> > q;
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
void putint(int x)
{
	int buf[15],p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
int main()
{
	int t=getint();
	while(t--)
	{
		int x=getint();
		if(x==1)
			q.push(getint());
		else
			if(x==2)
			{
				putint(q.top());
				puts("");
			}
			else
				q.pop();
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int a[1000005];
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
void putint(int x)
{
	int buf[15],p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
bool cmp(int x,int y)
{
	return x>y;
}
int main()
{
	int t=getint(),n=0;
	while(t--)
	{
		int x=getint();
		if(x==1)
		{
			a[++n]=getint();
			push_heap(a+1,a+n+1,cmp);
		}
		else
			if(x==2)
			{
				putint(a[1]);
				puts("");
			}
			else
			{
				pop_heap(a+1,a+n+1,cmp);
				n--;
			}
	}
	return 0;
}
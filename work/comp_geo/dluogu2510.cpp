#include<bits/stdc++.h>
using namespace std;
const int N=1005;
const double pi=acos(-1);
struct cycle
{
	double r,x,y;
}a[N];
int n,sn;
struct segment
{
	double l,r;
	bool operator<(const segment& rhs)const
	{
		return l<rhs.l;
	}
}s[2*N];
inline double dist(int x,int y)
{
	return hypot(a[x].x-a[y].x,a[x].y-a[y].y);
}
inline bool cover(int x,int y)
{
	return a[x].r>=a[y].r+dist(x,y);
}
void add_segment(int x,int y)
{
	double d=dist(x,y);
	double t=(a[x].r*a[x].r-a[y].r*a[y].r+d*d)/(2*d);
	double ori=atan2(a[x].x-a[y].x,a[x].y-a[y].y);
	double delta=acos(t/a[x].r);
	s[++sn].l=ori-delta;
	s[sn].r=ori+delta;
}
double calc(int id)
{
	for(int i=id+1;i<=n;i++)
		if(cover(i,id))
			return 0;
	sn=0;
	for(int i=id+1;i<=n;i++)
		if(!cover(id,i)&&dist(id,i)<=a[id].r+a[i].r)
			add_segment(id,i);
	for(int i=1;i<=sn;i++)
	{
		if(s[i].l<0)
			s[i].l+=2*pi;
		if(s[i].r<0)
			s[i].r+=2*pi;
		if(s[i].l>s[i].r)
		{
			s[++sn].l=0;
			s[sn].r=s[i].r;
			s[i].r=2*pi;
		}
	}
	sort(s+1,s+sn+1);
	double ans=0,now=0;
	for(int i=1;i<=sn;i++)
		if(s[i].l>now)
		{
			ans+=s[i].l-now;
			now=s[i].r;
		}
		else
			now=max(now,s[i].r);
	ans+=2*pi-now;
	return ans*a[id].r;
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i].r>>a[i].x>>a[i].y;
	double ans=0;
	for(int i=1;i<=n;i++)
		ans+=calc(i);
	cout.precision(3);
	cout<<fixed<<ans<<endl;
	return 0;
}
//============================================================================
// Author       : sunyaofeng
//============================================================================

//#pragma 	comment(linker, "/STACK:100240000,100240000")
//#include	<cstdio>
//#include	<cstdlib>
//#include	<cstring>
//#include	<algorithm>

#include	<bits/stdc++.h>

using	namespace	std;

#define DB		double
#define	lf		else if
#define I64		long long
#define	Rd()	(rand()<<15|rand())
#define For(i,a,b)	for(int i=a,lim=b;i<=lim;i++)
#define Rep(i,a,b)	for(int i=a,lim=b;i>=lim;i--)

//#define	min(a,b)	((a)<(b)?(a):(b))
//#define	max(a,b)	((a)<(b)?(b):(a))

#define	CH	(ch=getchar())
int		IN()	{
		int x= 0, f= 0, ch;
		for	(; CH < '0' || ch > '9';)	f= (ch == '-');
		for	(; ch >= '0' && ch <= '9'; CH)	x= x*10 + ch -'0';
		return	f? -x : x;
}

#define n	35

int		N, M, A[n][n], B[n][n];

char	File[n];

FILE	*Fin, *Fans, *Fsub, *Fres;

int		Cal()	{
		int	ret= 0;
		For(i, 1, N)	For(j, 1, N)	For(k, j+1, N){
			if	(A[i][k] > A[i][j])	ret++;
			if	(A[k][i] > A[j][i])	ret++;
		}
		return	ret;
}

void	Return(DB x, const char* s)	{
		fprintf(Fres, "%.12lf\n", x);
		fprintf(Fres, "%s\n", s);
		exit(0);
}

int		main(int argc, char* argv[]){
		Fin= fopen(argv[1], "r");
		Fans= fopen(argv[2], "r");
		Fsub= fopen(argv[3], "r");
		Fres= fopen(argv[4], "w");
		
		fscanf(Fin, "%d", &N);
		
		For(i, 1, 2*N)	{
			DB x, y;
			
			fscanf(Fsub, "%lf", &x);
			fscanf(Fans, "%lf", &y);
			
			if	(abs(x-y) > 0.1)	Return(0, "Wei");
		}
		
		Return(1, "OK");
		
		return	0;
}
#include<bits/stdc++.h>
using namespace std;
const int MN=100,N=10005,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int mat[MN][MN];
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> E;
int n,m,head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(edge(v,cap,head[u]));
	E.push_back(edge(u,0,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int dep[N];
bool bfs()
{
	fill(dep,dep+n*m+2,0);
	dep[n*m]=1;
	queue<int> Q;
	Q.push(n*m);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==n*m+1)
			return true;
		for(int i=head[k];~i;i=E[i].nxt)
			if(E[i].cap&&!dep[E[i].v])
			{
				dep[E[i].v]=dep[k]+1;
				Q.push(E[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==n*m+1||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=E[i].nxt)
		if(dep[E[i].v]==dep[k]+1&&(t=dfs(E[i].v,min(pred,E[i].cap))))
		{
			E[i].cap-=t;
			E[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	cin>>n>>m;
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			cin>>mat[i][j];
	fill(head,head+n*m+2,-1);
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			if(mat[i][j]==1)
			{
				add_edge(n*m,i*m+j,n*m);
				for(int k=0;k<4;k++)
				{
					int nx=i+dx[k],ny=j+dy[k];
					if(nx>=0&&nx<n&&ny>=0&&ny<m&&mat[nx][ny]!=1)
						add_edge(i*m+j,nx*m+ny,1);
				}
			}
			else
				if(mat[i][j]==2)
					add_edge(i*m+j,n*m+1,n*m);
				else
					for(int k=0;k<4;k++)
					{
						int nx=i+dx[k],ny=j+dy[k];
						if(nx>=0&&nx<n&&ny>=0&&ny<m&&mat[nx][ny]!=1)
							add_edge(i*m+j,nx*m+ny,1);
					}
	int ans=0;
	while(bfs())
	{
		copy(head,head+n*m+2,H);
		ans+=dfs(n*m,n*m);
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int MN=20,D=MN*4,P=MN*MN,INF=0x3f3f3f3f,dx[]={-1,1,0,0},dy[]={0,0,-1,1},N=32000;
string mat[MN];
int n,m,pn,dn,id[MN][MN],d[D][P],dist[MN][MN];
pair<int,int> doors[D];
void bfs(int s)
{
	fill(d[s]+1,d[s]+pn+1,INF);
	memset(dist,0x3f,sizeof(dist));
	dist[doors[s].first][doors[s].second]=0;
	queue<pair<int,int> > Q;
	Q.push(doors[s]);
	while(!Q.empty())
	{
		pair<int,int> k=Q.front();Q.pop();
		if(mat[k.first][k.second]=='.')
			d[s][id[k.first][k.second]]=dist[k.first][k.second];
		for(int i=0;i<4;i++)
		{
			int nx=k.first+dx[i],ny=k.second+dy[i];
			if(nx>=0&&nx<n&&ny>=0&&ny<m&&mat[nx][ny]!='X'&&dist[nx][ny]==INF)
			{
				dist[nx][ny]=dist[k.first][k.second]+1;
				Q.push(make_pair(nx,ny));
			}
		}
	}
}
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> E;
int head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(edge(v,cap,head[u]));
	E.push_back(edge(u,0,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int t,dep[N];
bool bfs()
{
	fill(dep+1,dep+t+1,0);
	dep[1]=1;
	queue<int> Q;
	Q.push(1);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];~i;i=E[i].nxt)
			if(E[i].cap&&dep[E[i].v]==0)
			{
				dep[E[i].v]=dep[k]+1;
				Q.push(E[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=E[i].nxt)
		if(dep[E[i].v]==dep[k]+1&&(t=dfs(E[i].v,min(pred,E[i].cap)))>0)
		{
			E[i].cap-=t;
			E[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
bool check(int time)
{
	t=pn+time*dn+2;
	fill(head+1,head+t+1,-1);
	E.clear();
	for(int i=1;i<=pn;i++)
	{
		add_edge(1,i+1,1);
		for(int j=1;j<=dn;j++)
			if(d[j][i]<=time)
				add_edge(i+1,pn+(d[j][i]-1)*dn+j+1,1);
	}
	for(int i=1;i<=time;i++)
		for(int j=1;j<=dn;j++)
		{
			add_edge(pn+(i-1)*dn+j+1,t,1);
			if(i<time)
				add_edge(pn+(i-1)*dn+j+1,pn+i*dn+j+1,pn);
		}
	int ans=0;
	while(bfs())
	{
		copy(head+1,head+t+1,H+1);
		ans+=dfs(1,pn);
	}
	return ans==pn;
}
int main()
{
	cin>>n>>m;
	for(int i=0;i<n;i++)
	{
		cin>>mat[i];
		for(int j=0;j<m;j++)
			if(mat[i][j]=='.')
				id[i][j]=++pn;
			else
				if(mat[i][j]=='D')
				{
					id[i][j]=++dn;
					doors[dn]=make_pair(i,j);
				}
	}
	for(int i=1;i<=dn;i++)
		bfs(i);
	for(int i=1;i<=pn;i++)
	{
		bool flag=true;
		for(int j=1;j<=dn;j++)
			if(d[j][i]<INF)
			{
				flag=false;
				break;
			}
		if(flag)
		{
			cout<<"impossible\n";
			return 0;
		}
	}
	int l=pn/dn,r=pn;
	while(l<r)
	{
		int mid=(l+r)/2;
		if(check(mid))
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
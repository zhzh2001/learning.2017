#include<bits/stdc++.h>
using namespace std;
const int N=5005,INF=0x3f3f3f3f;
struct edge
{
	int v,cap,cost,nxt;
	edge(int v,int cap,int cost,int nxt):v(v),cap(cap),cost(cost),nxt(nxt){}
};
vector<edge> E;
int head[N];
inline void add_edge(int u,int v,int cap,int cost)
{
	E.push_back(edge(v,cap,cost,head[u]));
	E.push_back(edge(u,0,-cost,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int n,d[N],flow[N],pred[N];
bool inQ[N];
void spfa(int s)
{
	fill(d+1,d+n+1,INF);
	d[s]=0;
	fill(inQ+1,inQ+n+1,false);
	inQ[s]=true;
	fill(flow+1,flow+n+1,0);
	flow[s]=INF;
	deque<int> Q;
	Q.push_back(s);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop_front();
		inQ[k]=false;
		for(int i=head[k];~i;i=E[i].nxt)
			if(E[i].cap&&d[k]+E[i].cost<d[E[i].v])
			{
				d[E[i].v]=d[k]+E[i].cost;
				pred[E[i].v]=i^1;
				flow[E[i].v]=min(E[i].cap,flow[k]);
				if(!Q.empty()&&d[E[i].v]<d[Q.front()])
					Q.push_front(E[i].v);
				else
					Q.push_back(E[i].v);
			}
	}
}
int main()
{
	int m,s,t;
	cin>>n>>m>>s>>t;
	fill(head+1,head+n+1,-1);
	while(m--)
	{
		int u,v,cap,cost;
		cin>>u>>v>>cap>>cost;
		add_edge(u,v,cap,cost);
	}
	int maxflow=0,mincost=0;
	for(;;)
	{
		spfa(s);
		if(!flow[t])
			break;
		for(int u=t;u!=s;u=E[pred[u]].v)
		{
			E[pred[u]].cap+=flow[t];
			E[pred[u]^1].cap-=flow[t];
		}
		maxflow+=flow[t];
		mincost+=d[t]*flow[t];
	}
	cout<<maxflow<<' '<<mincost<<endl;
	return 0;
}
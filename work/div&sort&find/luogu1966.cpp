#include<bits/stdc++.h>
#define mid (l+r)/2
using namespace std;
const int pr=99999997;
struct node
{
	int num,p;
};
node a[100005],b[100005];
int c[100005],ans,t[100005];
bool cmp(node x,node y)
{
	return x.num<y.num;
}
void merge_sort(int l,int r)
{
	if(l>=r)
		return;
	merge_sort(l,mid);
	merge_sort(mid+1,r);
	int i=l,j=mid+1,p=l;
	while(i<=mid&&j<=r)
		if(c[i]>c[j])
		{
			ans=(ans+mid-i+1)%pr;
			t[p++]=c[j++];
		}
		else
			t[p++]=c[i++];
	while(i<=mid)
		t[p++]=c[i++];
	while(j<=r)
		t[p++]=c[j++];
	memcpy(c+l,t+l,(r-l+1)*sizeof(int));
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].num;
		a[i].p=i;
	}
	for(int i=1;i<=n;i++)
	{
		cin>>b[i].num;
		b[i].p=i;
	}
	sort(a+1,a+n+1,cmp);
	sort(b+1,b+n+1,cmp);
	for(int i=1;i<=n;i++)
		c[a[i].p]=b[i].p;
	ans=0;
	merge_sort(1,n);
	cout<<ans<<endl;
	return 0;
}
#include<iostream>
#include<algorithm>
#include<cstring>
#define mid (l+r)/2
using namespace std;
int n,a[20005],b[20005],c[20005],ans;
void merge_sort(int l,int r)
{
	if(l>=r)
		return;
	merge_sort(l,mid);
	merge_sort(mid+1,r);
	int i=l,j=mid+1,p=l;
	while(i<=mid&&j<=r)
		if(b[i]>b[j])
		{
			ans+=(mid-i+1);
			c[p++]=b[j++];
		}
		else
			c[p++]=b[i++];
	while(i<=mid)
		c[p++]=b[i++];
	while(j<=r)
		c[p++]=b[j++];
	memcpy(b+l,c+l,(r-l+1)*sizeof(int));
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	memcpy(b,a,sizeof(b));
	ans=0;
	merge_sort(1,n);
	cout<<ans<<endl;
	int m;
	cin>>m;
	while(m--)
	{
		int x,y;
		cin>>x>>y;
		if(x>y)
			swap(x,y);
		for(int i=x+1;i<y;i++)
		{
			if(a[x]>a[i])
				ans--;
			if(a[i]>a[y])
				ans--;
		}
		if(a[x]>a[y])
			ans--;
		swap(a[x],a[y]);
		for(int i=x+1;i<y;i++)
		{
			if(a[x]>a[i])
				ans++;
			if(a[i]>a[y])
				ans++;
		}
		if(a[x]>a[y])
			ans++;
		cout<<ans<<endl;
	}
	return 0;
}
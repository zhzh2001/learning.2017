#include<iostream>
#define f(x) (h*D-D*x)/(H-x)+x
using namespace std;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		double H,h,D;
		cin>>H>>h>>D;
		double l=0,r=h;
		while(r-l>=1e-5)
		{
			double lmid=l+(r-l)/3,rmid=r-(r-l)/3;
			if(f(lmid)>f(rmid))
				r=rmid;
			else
				l=lmid;
		}
		cout.precision(3);
		cout<<fixed<<f(l)<<endl;
	}
	return 0;
}
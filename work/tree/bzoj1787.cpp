#include<cstdio>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=500005,LOG=20,M=1000005;
int m,dep[N],f[N][LOG],head[N],next[M],v[M];
bool vis[N];
inline void ins(int u,int v)
{
	::v[++m]=v;
	next[m]=head[u];
	head[u]=m;
}
void dfs(int k)
{
	vis[k]=true;
	for(int i=1;(1<<i)<=dep[k];i++)
		f[k][i]=f[f[k][i-1]][i-1];
	for(int i=head[k];i;i=next[i])
		if(!vis[v[i]])
		{
			dep[v[i]]=dep[k]+1;
			f[v[i]][0]=k;
			dfs(v[i]);
		}
}
int lca(int x,int y)
{
	if(dep[x]>dep[y])
		swap(x,y);
	int delta=dep[y]-dep[x];
	for(int i=0;delta;i++,delta>>=1)
		if(delta&1)
			y=f[y][i];
	if(x==y)
		return x;
	int i;
	for(i=0;f[x][i]!=f[y][i];i++);
	for(;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
inline int dist(int x,int y)
{
	return dep[x]+dep[y]-2*dep[lca(x,y)];
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	::m=0;
	for(int i=1;i<n;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		ins(u,v);
		ins(v,u);
	}
	f[1][0]=1;
	memset(vis,false,sizeof(vis));
	dfs(1);
	while(m--)
	{
		int a,b,c;
		scanf("%d%d%d",&a,&b,&c);
		int a1=lca(a,b),a2=lca(a,c),a3=lca(b,c),fa;
		if(a1==a2)
			fa=a3;
		else
			if(a1==a3)
				fa=a2;
			else
				fa=a1;
		printf("%d %d\n",fa,dist(a,fa)+dist(b,fa)+dist(c,fa));
	}
	return 0;
}
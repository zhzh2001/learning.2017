#include<iostream>
#include<cstring>
using namespace std;
const int N=100005,M=200005,LOG=18;
int m,dclock,head[N],v[M],next[M],dep[N],f[N][LOG],sz[N],it[N],ot[N];
bool vis[N];

inline void ins(int u,int v);

inline int dist(int x,int y);

inline bool is_ancestor(int anc,int u);

void dfs(int k);

int kth_ancestor(int x,int k);

int lca(int& x,int& y);

int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	m=0;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		ins(u,v);
		ins(v,u);
	}
	memset(vis,false,sizeof(vis));
	f[1][0]=1;
	dclock=0;
	dfs(1);
	int q;
	cin>>q;
	while(q--)
	{
		int u,v;
		cin>>u>>v;
		if(u==v)
			cout<<n<<endl;
		else
			if(dist(u,v)&1)
				cout<<0<<endl;
			else
			{
				int uu=u,vv=v,a=lca(uu,vv);
				if(dep[u]-dep[a]==dep[v]-dep[a])
					cout<<n-sz[uu]-sz[vv]<<endl;
				else
				{
					if(dep[u]<dep[v])
						swap(u,v);
					int d=dist(u,v);
					cout<<sz[kth_ancestor(u,d/2)]-sz[kth_ancestor(u,d/2-1)]<<endl;
				}
			}
	}
	return 0;
}

inline void ins(int u,int v)
{
	::v[++m]=v;
	next[m]=head[u];
	head[u]=m;
}
inline int dist(int x,int y)
{
	return dep[x]+dep[y]-2*dep[lca(x,y)];
}
inline bool is_ancestor(int anc,int u)
{
	return it[anc]<=it[u]&&ot[anc]>=ot[u];
}
void dfs(int k)
{
	vis[k]=true;
	sz[k]=1;
	for(int i=1;(1<<i)<=dep[k];i++)
		f[k][i]=f[f[k][i-1]][i-1];
	it[k]=++dclock;
	for(int i=head[k];i;i=next[i])
		if(!vis[v[i]])
		{
			dep[v[i]]=dep[k]+1;
			f[v[i]][0]=k;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
		}
	ot[k]=++dclock;
}
int kth_ancestor(int x,int k)
{
	for(int i=0;k;i++,k>>=1)
		if(k&1)
			x=f[x][i];
	return x;
}
int lca(int& x,int& y)
{
	if(dep[x]<dep[y])
		swap(x,y);
	x=kth_ancestor(x,dep[x]-dep[y]);
	if(x==y)
		return x;
	int i;
	for(i=0;f[x][i]!=f[y][i];i++);
	for(;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
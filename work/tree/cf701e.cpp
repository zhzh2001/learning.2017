#include<bits/stdc++.h>
using namespace std;
const int N=200005,M=400005;
bool univ[N],vis[N];
int m,head[N],next[M],v[M],cnt[N],f[N];
inline void ins(int u,int v)
{
	::v[++m]=v;
	next[m]=head[u];
	head[u]=m;
}
void dfs(int k)
{
	vis[k]=true;
	cnt[k]=univ[k];
	for(int i=head[k];i;i=next[i])
		if(!vis[v[i]])
		{
			f[v[i]]=k;
			dfs(v[i]);
			cnt[k]+=cnt[v[i]];
		}
}
int main()
{
	int n,k;
	cin>>n>>k;
	k*=2;
	for(int i=1;i<=k;i++)
	{
		int x;
		cin>>x;
		univ[x]=true;
	}
	m=0;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		ins(u,v);
		ins(v,u);
	}
	dfs(1);
	long long ans=0;
	for(int i=2;i<=n;i++)
		ans+=min(cnt[i],k-cnt[i]);
	cout<<ans<<endl;
	return 0;
}
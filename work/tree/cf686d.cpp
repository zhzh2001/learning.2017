#include<bits/stdc++.h>
using namespace std;
const int N=300005;
vector<int> mat[N];
int f[N],ans[N],cnt[N];
void dfs(int k)
{
	cnt[k]=1;
	ans[k]=k;
	for(int i=0;i<mat[k].size();i++)
	{
		dfs(mat[k][i]);
		cnt[k]+=cnt[mat[k][i]];
	}
	for(int i=0;i<mat[k].size();i++)
		if(cnt[mat[k][i]]*2>=cnt[k])
			ans[k]=ans[mat[k][i]];
	while((cnt[k]-cnt[ans[k]])*2>cnt[k])
		ans[k]=f[ans[k]];
}
int main()
{
	int n,q;
	cin>>n>>q;
	f[1]=1;
	for(int i=2;i<=n;i++)
	{
		cin>>f[i];
		mat[f[i]].push_back(i);
	}
	dfs(1);
	while(q--)
	{
		int x;
		cin>>x;
		cout<<ans[x]<<endl;
	}
	return 0;
}
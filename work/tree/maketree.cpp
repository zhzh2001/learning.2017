#include<bits/stdc++.h>
using namespace std;
const int n=500000,m=500000;
int f[n];
inline int Hrand()
{
	return rand()*(RAND_MAX+1)+rand();
}
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	srand(time(NULL));
	cout<<n<<' '<<m<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=1;i<n-1;)
	{
		int u=Hrand()%n+1,v=Hrand()%n+1,ru=getf(u),rv=getf(v);
		if(ru!=rv)
		{
			cout<<u<<' '<<v<<endl;
			f[ru]=rv;
			i++;
		}
	}
	for(int i=1;i<=m;i++)
		cout<<Hrand()%n+1<<' '<<Hrand()%n+1<<' '<<Hrand()%n+1<<endl;
	return 0;
}
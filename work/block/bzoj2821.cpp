#include<bits/stdc++.h>
using namespace std;
const int N=100005,SQRTN=320;
int a[N],f[SQRTN][SQRTN],s[SQRTN][N],cnt[N],tmp[N];
int main()
{
	int n,c,m;
	scanf("%d%d%d",&n,&c,&m);
	int t=floor(sqrt(n));
	for(int i=0;i<n;i++)
		scanf("%d",a+i);
	
	for(int i=0;i<=t;i++)
	{
		if(i)
			memcpy(&s[i][0],&s[i-1][0],N*sizeof(int));
		int l=t*i,r=min(l+t,n);
		for(int j=l;j<r;j++)
			s[i][a[j]]++;
	}
	
	for(int i=0;i<=t;i++)
	{
		memset(cnt,0,sizeof(cnt));
		for(int j=i;j<=t;j++)
		{
			if(j>i)
				f[i][j]=f[i][j-1];
			int l=t*j,r=min(l+t,n);
			for(int k=l;k<r;k++)
				if(cnt[a[k]]++)
					f[i][j]+=cnt[a[k]]&1?-1:1;
		}
	}
	
	int ans=0;
	while(m--)
	{
		int l,r;
		scanf("%d%d",&l,&r);
		l=(l+ans)%n;r=(r+ans)%n;
		if(l>r)
			swap(l,r);
		ans=0;
		int cc=0;
		if(l/t==r/t)
		{
			for(int i=l;i<=r;i++)
				tmp[cc++]=a[i];
			sort(tmp,tmp+cc);
			tmp[cc]=-1;
			int dur=0;
			for(int i=0;i<=cc;i++,dur++)
				if(i&&tmp[i]!=tmp[i-1])
				{
					ans+=~dur&1;
					dur=0;
				}
		}
		else
		{
			ans=f[l/t+1][r/t-1];
			for(int i=l;i==l||i%t;i++)
				tmp[cc++]=a[i];
			for(int i=r/t*t;i<=r;i++)
				tmp[cc++]=a[i];
			sort(tmp,tmp+cc);
			tmp[cc]=-1;
			int dur=0;
			for(int i=0;i<=cc;i++,dur++)
				if(i&&tmp[i]!=tmp[i-1])
				{
					int bdur=s[r/t-1][tmp[i-1]]-s[l/t][tmp[i-1]];
					if(!bdur&&(~dur&1))
						ans++;
					else
						if(bdur&1&&dur&1)
							ans++;
						else
							if(bdur&&~bdur&1&&dur&1)
								ans--;
					dur=0;
				}
		}
		printf("%d\n",ans);
	}
	return 0;
}
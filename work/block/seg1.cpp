#include<bits/stdc++.h>
using namespace std;
const int N=100000;
int n,bn;
long long a[N];
struct block
{
	long long sum,tag;
}b[320];
void modify(int l,int r,long long val);
long long query(int l,int r);
int main()
{
	int m;
	cin>>n>>m;
	for(int i=0;i<n;i++)
		cin>>a[i];
	bn=floor(sqrt(n));
	for(int i=0;i<=bn;i++)
	{
		b[i].sum=b[i].tag=0;
		int base=i*bn;
		for(int j=0;j<bn&&base+j<n;j++)
			b[i].sum+=a[base+j];
	}
	
	while(m--)
	{
		int opt,x,y;
		cin>>opt;
		if(opt==1)
		{
			long long k;
			cin>>x>>y>>k;
			modify(x-1,y-1,k);
		}
		else
		{
			cin>>x>>y;
			cout<<query(x-1,y-1)<<endl;
		}
	}
	return 0;
}

void modify(int l,int r,long long val)
{
	int i;
	for(i=l;i<=r&&i%bn;i++)
	{
		a[i]+=val;
		b[i/bn].sum+=val;
	}
	if(i<=r)
	{
		for(int j=i/bn;j<r/bn;j++)
		{
			b[j].sum+=val*bn;
			b[j].tag+=val;
		}
		for(int j=r/bn*bn;j<=r;j++)
		{
			a[j]+=val;
			b[j/bn].sum+=val;
		}
	}
}

long long query(int l,int r)
{
	long long ans=0;
	int i;
	for(i=l;i<=r&&i%bn;i++)
		ans+=a[i]+b[i/bn].tag;
	if(i<=r)
	{
		for(int j=i/bn;j<r/bn;j++)
			ans+=b[j].sum;
		for(int j=r/bn*bn;j<=r;j++)
			ans+=a[j]+b[j/bn].tag;
	}
	return ans;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1000000,T=1000;
int a[N],b[T],t;
void modify(int l,int r,int val);
int query(int l,int r,int val);
int main()
{
	int n,q;
	cin>>n>>q;
	for(int i=0;i<n;i++)
		cin>>a[i];
	t=floor(sqrt(n));
	for(int i=0;i<=t;i++)
	{
		int l=i*t,r=min(i*t+t,n);
		sort(a+l,a+r);
	}
	
	while(q--)
	{
		string opt;
		int l,r,val;
		cin>>opt>>l>>r>>val;
		if(opt=="M")
			modify(l-1,r-1,val);
		else
			cout<<query(l-1,r-1,val)<<endl;
	}
	return 0;
}

void modify(int l,int r,int val)
{
	int i;
	for(i=l;i<=r&&i%t;i++)
		a[i]+=val;
	sort(a+l,a+i);
	if(i<=r)
	{
		for(int j=i/t;j<r/t;j++)
			b[j]+=val;
		for(int j=r/t*t;j<=r;j++)
			a[j]+=val;
		sort(a+r/t*t,a+r+1);
	}
}

int query(int l,int r,int val)
{
	int ans=0,i;
	for(i=l;i<=r&&i%t;i++)
		ans+=a[i]>=val;
	if(i<=r)
	{
		for(int j=i/t;j<r/t;j++)
		{
			int ll=j*t,rr=ll+t;
			ans+=rr-(lower_bound(a+ll,a+rr,val-b[j])-a);
		}
		for(int j=r/t*t;j<=r;j++)
			ans+=a[j]>=val;
	}
	return ans;
}
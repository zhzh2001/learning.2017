#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=1000005;
int a[N],rev[M];
bool vis[2*N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		scanf("%d",&x);
		if(!rev[x])
			rev[x]=++cnt;
		a[i]=rev[x];
	}
	int cq=0;
	while(m--)
	{
		char opt;
		int x,y;
		scanf("%1s%d%d",&opt,&x,&y);
		if(opt=='Q')
		{
			memset(vis,false,sizeof(vis));
			int ans=0;
			for(int i=x;i<=y;i++)
				if(!vis[a[i]])
				{
					vis[a[i]]=true;
					ans++;
				}
			printf("%d\n",ans);
		}
		else
		{
			if(!rev[y])
				rev[y]=++cnt;
			a[x]=rev[y];
		}
	}
	return 0;
}
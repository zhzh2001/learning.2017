#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int a[N],t;
unsigned cnt[N];
struct question
{
	int l,r,id;
	bool operator<(const question& b)const
	{
		int block1=l/t,block2=b.l/t;
		if(block1^block2)
			return block1<block2;
		return r<b.r;
	}
}q[N];
inline unsigned sqr(unsigned x)
{
	return x*x;
}
struct frac
{
	unsigned a,b;
}res[N];
unsigned gcd(unsigned a,unsigned b)
{
	return a%b==0?b:gcd(b,a%b);
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	t=sqrt(n);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&q[i].l,&q[i].r);
		q[i].id=i;
	}
	sort(q+1,q+m+1);
	int l=1,r=0;
	unsigned ans=0;
	for(int i=1;i<=m;i++)
	{
		while(l<q[i].l)
		{
			ans-=sqr(cnt[a[l]]);
			cnt[a[l]]--;
			ans+=sqr(cnt[a[l]]);
			l++;
		}
		while(l>q[i].l)
		{
			l--;
			ans-=sqr(cnt[a[l]]);
			cnt[a[l]]++;
			ans+=sqr(cnt[a[l]]);
		}
		while(r<q[i].r)
		{
			r++;
			ans-=sqr(cnt[a[r]]);
			cnt[a[r]]++;
			ans+=sqr(cnt[a[r]]);
		}
		while(r>q[i].r)
		{
			ans-=sqr(cnt[a[r]]);
			cnt[a[r]]--;
			ans+=sqr(cnt[a[r]]);
			r--;
		}
		res[q[i].id].a=ans-(q[i].r-q[i].l+1);
		res[q[i].id].b=(unsigned)(q[i].r-q[i].l+1)*(q[i].r-q[i].l);
	}
	for(int i=1;i<=m;i++)
		if(res[i].b)
		{
			unsigned g=gcd(res[i].a,res[i].b);
			printf("%u/%u\n",res[i].a/g,res[i].b/g);
		}
		else
			puts("0/1");
	return 0;
}
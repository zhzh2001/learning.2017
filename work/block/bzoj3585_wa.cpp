#include<iostream>
#include<cstdio>
#include<cmath>
#include<algorithm>
using namespace std;
struct da
{
	int l,r,rk;
}f[200005];
int a[200005],n,m,L,R,ans[200005],k,now,b[200005],i,j;
bool cmp(da x,da y)
{
	return x.l/k==y.l/k?x.r<y.r:x.l<y.l;
}
void work(int x,int y)
{
	if (x>n)
		return;
	b[x]+=y;
	if (x==now)
		while (b[now]!=0)
			++now;
	if (x<now && b[x]==0)
		now=x;
}
int main()
{
	scanf("%d%d",&n,&m);
	k=(int)sqrt((double)n);
	for (i=1;i<=n;++i)
		scanf("%d",&a[i]);
	for (i=1;i<=m;++i)
	{
		scanf("%d%d",&f[i].l,&f[i].r);
		f[i].rk=i;
	}
	sort(f+1,f+1+m,cmp);
	L=f[1].l;
	R=f[1].l;
	now=0;
	work(a[L],1);
	for (i=1;i<=m;++i)
	{	
		while (L>f[i].l)
			work(a[--L],1);
		while (R<f[i].r)
			work(a[++R],1);
		while (R>f[i].r)
			work(a[R--],-1);	
		while (L<f[i].l)
			work(a[L++],-1);
		ans[f[i].rk]=now;
	}
	for (i=1;i<=m;++i)
		printf("%d\n",ans[i]);
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<set>
#include<ctime>
#include<queue>
#include<cmath>
#include<algorithm>
#define inf 1000000000
#define ll long long
using namespace std;
int read()
{
    int x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
    return x*f;
}
int n,m,K;
int b[50005];
ll cnt[50005],res[50005];
struct query{int l,r,block,id;}a[50005];
bool operator<(query a,query b)
{
	if(a.block==b.block)return a.r<b.r;
	return a.block<b.block;
}
void solve()
{
	sort(a+1,a+m+1);
	int l=1,r=0;ll ans=0;
	for(int i=1;i<=m;i++)
	{
		while(l>a[i].l)l--,cnt[b[l]]++,ans+=2*cnt[b[l]]-1;
		while(r<a[i].r)r++,cnt[b[r]]++,ans+=2*cnt[b[r]]-1;
		while(l<a[i].l)cnt[b[l]]--,ans-=2*cnt[b[l]]+1,l++;
		while(r>a[i].r)cnt[b[r]]--,ans-=2*cnt[b[r]]+1,r--;
		res[a[i].id]=ans;
	}
}
int main()
{
	n=read();m=read();K=read();
	int size=sqrt(n);
	for(int i=1;i<=n;i++)b[i]=read();
	for(int i=1;i<=m;i++)
	{
		a[i].l=read(),a[i].r=read();
		a[i].id=i;
		a[i].block=(a[i].l-1)/size+1;
	}
	solve();
	for(int i=1;i<=m;i++)
		printf("%lld\n",res[i]);
	return 0;
}
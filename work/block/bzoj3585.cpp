#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int n,a[N],t,cnt[N],ans[N],now;
struct question
{
	int l,r,id;
	bool operator<(const question& b)const
	{
		return (l/t)^(b.l/t)?l/t<b.l/t:r<b.r;
	}
}q[N];
inline void modify(int num,int delta)
{
	if(num<=n)
	{
		cnt[num]+=delta;
		if(num<now&&!cnt[num])
			now=num;
		for(;cnt[now];now++);
	}
}
int main()
{
	int m;
	scanf("%d%d",&n,&m);
	t=sqrt(n);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&q[i].l,&q[i].r);
		q[i].id=i;
	}
	sort(q+1,q+m+1);
	int l=1,r=0;
	now=0;
	for(int i=1;i<=m;i++)
	{
		while(l<q[i].l)
			modify(a[l++],-1);
		while(l>q[i].l)
			modify(a[--l],1);
		while(r<q[i].r)
			modify(a[++r],1);
		while(r>q[i].r)
			modify(a[r--],-1);
		ans[q[i].id]=now;
	}
	for(int i=1;i<=m;i++)
		printf("%d\n",ans[i]);
	return 0;
}
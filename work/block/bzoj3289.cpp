#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int n,t,b[N],a[N];
struct BIT
{
	int tree[N];
	BIT()
	{
		memset(tree,0,sizeof(tree));
	}
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
struct question
{
	int l,r,id;
	bool operator<(const question& b)const
	{
		int block1=l/t,block2=b.l/t;
		if(block1!=block2)
			return block1<block2;
		return r<b.r;
	}
}q[N];
unsigned ans[N];
int main()
{
	scanf("%d",&n);
	t=sqrt(n);
	for(int i=1;i<=n;i++)
		scanf("%d",b+i);
	memcpy(a,b,sizeof(a));
	sort(b+1,b+n+1);
	int p=unique(b+1,b+n+1)-b;
	for(int i=1;i<=n;i++)
		a[i]=lower_bound(b+1,b+p,a[i])-b;
	
	int m;
	scanf("%d",&m);
	for(int i=1;i<=m;i++)
	{
		cin>>q[i].l>>q[i].r;
		q[i].id=i;
	}
	sort(q+1,q+m+1);
	
	int l=1,r=0;
	unsigned now=0;
	for(int i=1;i<=m;i++)
	{
		while(l<q[i].l)
		{
			T.modify(a[l],-1);
			now-=T.query(a[l++]-1);
		}
		while(l>q[i].l)
		{
			T.modify(a[--l],1);
			now+=T.query(a[l]-1);
		}
		while(r<q[i].r)
		{
			T.modify(a[++r],1);
			now+=r-l+1-T.query(a[r]);
		}
		while(r>q[i].r)
		{
			T.modify(a[r],-1);
			now-=r-l-T.query(a[r]);
			r--;
		}
		ans[q[i].id]=now;
	}
	
	for(int i=1;i<=m;i++)
		printf("%u\n",ans[i]);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=50005;
int head[N];
struct edge
{
	int v,nxt;
}e[M];
inline void add_edge(int u,int v,int id)
{
	e[id].v=v;
	e[id].nxt=head[u];
	head[u]=id;
}
namespace tarjan
{
	bool vis[N],inS[N];
	int dfn[N],low[N],t,ans;
	stack<int> S;
	void dfs(int k)
	{
		dfn[k]=low[k]=++t;
		vis[k]=true;
		S.push(k);
		inS[k]=true;
		
		for(int i=head[k];i;i=e[i].nxt)
			if(!vis[e[i].v])
			{
				dfs(e[i].v);
				low[k]=min(low[k],low[e[i].v]);
			}
			else
				if(inS[e[i].v])
					low[k]=min(low[k],dfn[e[i].v]);
		
		if(dfn[k]==low[k])
		{
			int cnt=0,t;
			do
			{
				t=S.top();S.pop();
				inS[t]=false;
				cnt++;
			}
			while(t!=k);
			ans+=cnt>1;
		}
	}
};
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v,i);
	}
	using namespace tarjan;
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	cout<<ans<<endl;
	return 0;
}
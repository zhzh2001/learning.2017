#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=1000005;
int a[N],head[N],rhead[N],cc,rcc,low[N],high[N];
struct edge
{
	int v,nxt;
}e[M],re[M];
inline void add_edge(int u,int v)
{
	e[++cc].v=v;
	e[cc].nxt=head[u];
	head[u]=cc;
}
inline void radd_edge(int u,int v)
{
	re[++rcc].v=v;
	re[rcc].nxt=rhead[u];
	rhead[u]=rcc;
}
queue<int> Q;
bool inQ[N];
void spfa(int s)
{
	memset(low,0x3f,sizeof(low));
	low[s]=a[s];
	Q.push(s);
	inQ[s]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(int i=head[k];i;i=e[i].nxt)
			if(min(low[k],a[e[i].v])<low[e[i].v])
			{
				low[e[i].v]=min(low[k],a[e[i].v]);
				if(!inQ[e[i].v])
				{
					Q.push(e[i].v);
					inQ[e[i].v]=true;
				}
			}
	}
}
void rspfa(int s)
{
	memset(high,0,sizeof(high));
	high[s]=a[s];
	Q.push(s);
	inQ[s]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(int i=rhead[k];i;i=re[i].nxt)
			if(max(high[k],a[re[i].v])>high[re[i].v])
			{
				high[re[i].v]=max(high[k],a[re[i].v]);
				if(!inQ[re[i].v])
				{
					Q.push(re[i].v);
					inQ[re[i].v]=true;
				}
			}
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<=m;i++)
	{
		int u,v,t;
		cin>>u>>v>>t;
		add_edge(u,v);
		radd_edge(v,u);
		if(t==2)
		{
			add_edge(v,u);
			radd_edge(u,v);
		}
	}
	spfa(1);
	rspfa(n);
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,high[i]-low[i]);
	cout<<ans<<endl;
	return 0;
}
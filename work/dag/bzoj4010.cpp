#include<bits/stdc++.h>
#define clr(x) memset(x,0,sizeof(x));
using namespace std;
const int N=100005;
int e,head[N],v[N],next[N],cnt[N],ans[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	next[e]=head[u];
	head[u]=e;
}
priority_queue<int> Q;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n,m;
		cin>>n>>m;
		e=0;
		clr(head);clr(v);clr(next);
		clr(cnt);
		while(m--)
		{
			int u,v;
			cin>>u>>v;
			add_edge(v,u);
			cnt[u]++;
		}
		for(int i=1;i<=n;i++)
			if(!cnt[i])
				Q.push(i);
		int cc=0;
		while(!Q.empty())
		{
			int k=Q.top();Q.pop();
			ans[++cc]=k;
			for(int i=head[k];i;i=next[i])
			{
				cnt[v[i]]--;
				if(cnt[v[i]]==0)
					Q.push(v[i]);
			}
		}
		if(cc<n)
			cout<<"Impossible!";
		else
			for(int i=n;i;i--)
				cout<<ans[i]<<' ';
		cout<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
#define clr(x) memset(x,0,sizeof(x));
using namespace std;
const int N=100005,M=200005;
const long long MOD=1000000007LL;
int e,head[N],v[M],nxt[M],cnt[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
priority_queue<int,vector<int>,greater<int> > Q;
bool vis[N];
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n,m,k;
		scanf("%d%d%d",&n,&m,&k);
		e=0;
		clr(head);clr(v);clr(nxt);
		clr(cnt);
		while(m--)
		{
			int u,v;
			scanf("%d%d",&u,&v);
			add_edge(u,v);
			cnt[v]++;
		}
		for(int i=1;i<=n;i++)
			Q.push(i);
		long long ans=0;
		int id=0;
		memset(vis,false,sizeof(vis));
		while(!Q.empty())
		{
			int f=Q.top();Q.pop();
			if(!vis[f]&&cnt[f]<=k)
			{
				vis[f]=true;
				k-=cnt[f];cnt[f]=0;
				ans=(ans+(long long)(++id)*f)%MOD;
				for(int i=head[f];i;i=nxt[i])
					if(cnt[v[i]])
					{
						cnt[v[i]]--;
						Q.push(v[i]);
					}
			}
		}
		printf("%I64d\n",ans);
	}
	return 0;
}
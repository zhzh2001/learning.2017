#include<bits/stdc++.h>
using namespace std;
const int N=105;
int a[N];
bool vis[N];
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int ans=1;
	for(int i=1;i<=n;i++)
	{
		if(vis[i])
			continue;
		int cnt=1,now=a[i];
		for(;now!=i;now=a[now],cnt++)
		{
			if(vis[now])
			{
				cnt=-1;
				break;
			}
			vis[now]=true;
		}
		if(cnt==-1)
		{
			cout<<-1<<endl;
			return 0;
		}
		if(cnt%2==0)
			cnt/=2;
		int g=gcd(cnt,ans);
		ans=cnt*ans/g;
	}
	cout<<ans<<endl;
	return 0;
}
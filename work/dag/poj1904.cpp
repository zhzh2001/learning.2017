#include<cstdio>
#include<vector>
#include<stack>
#include<cstring>
#include<algorithm>
using namespace std;
const int N=4005,M=202005;
int head[N],n,m,ans[N];
struct edge
{
	int v,nxt;
}e[M];
inline void add_edge(int u,int v)
{
	e[++m].v=v;
	e[m].nxt=head[u];
	head[u]=m;
}
namespace tarjan
{
	bool vis[N],inS[N];
	int dfn[N],low[N],t,scc,p[N];
	stack<int> S;
	void dfs(int k)
	{
		dfn[k]=low[k]=++t;
		vis[k]=true;
		S.push(k);
		inS[k]=true;
		
		for(int i=head[k];i;i=e[i].nxt)
			if(!vis[e[i].v])
			{
				dfs(e[i].v);
				low[k]=min(low[k],low[e[i].v]);
			}
			else
				if(inS[e[i].v])
					low[k]=min(low[k],dfn[e[i].v]);
		
		if(dfn[k]==low[k])
		{
			scc++;
			int t;
			do
			{
				t=S.top();S.pop();
				inS[t]=false;
				p[t]=scc;
			}
			while(t!=k);
		}
	}
};
int main()
{
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		int k;
		scanf("%d",&k);
		while(k--)
		{
			int v;
			scanf("%d",&v);
			add_edge(i,v+n);
		}
	}
	for(int i=1;i<=n;i++)
	{
		int x;
		scanf("%d",&x);
		add_edge(x+n,i);
	}
	using namespace tarjan;
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	for(int i=1;i<=n;i++)
	{
		int cnt=0;
		for(int j=head[i];j;j=e[j].nxt)
			if(p[i]==p[e[j].v])
				ans[++cnt]=e[j].v-n;
		sort(ans+1,ans+cnt+1);
		printf("%d",cnt);
		for(int j=1;j<=cnt;j++)
			printf(" %d",ans[j]);
		puts("");
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=300005;
struct edge
{
	int u,v,w;
	bool operator<(const edge& b)const
	{
		return w<b.w;
	}
}e[N];
int f[N],g[N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++)
		scanf("%d%d%d",&e[i].u,&e[i].v,&e[i].w);
	sort(e+1,e+m+1);
	e[0].w=e[m+1].w=0;
	int cc=0;
	for(int i=1;i<=m+1;i++)
	{
		if(e[i].w!=e[i-1].w)
		{
			for(int j=1;j<=cc;j++)
				f[e[i-j].v]=max(f[e[i-j].v],g[e[i-j].v]);
			cc=0;
		}
		if(i<=m)
		{
			cc++;
			g[e[i].v]=max(g[e[i].v],f[e[i].u]+1);
		}
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,f[i]);
	printf("%d\n",ans);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int W=100005,N=300005;
struct edge
{
	int u,v;
	edge(int u,int v):u(u),v(v){};
};
vector<edge> mat[W];
int f[N],g[N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	while(m--)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		mat[w].push_back(edge(u,v));
	}
	for(int i=1;i<=W;i++)
	{
		for(int j=0;j<mat[i].size();j++)
			g[mat[i][j].v]=max(g[mat[i][j].v],f[mat[i][j].u]+1);
		for(int j=0;j<mat[i].size();j++)
			f[mat[i][j].v]=max(f[mat[i][j].v],g[mat[i][j].v]);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,f[i]);
	printf("%d\n",ans);
	return 0;
}
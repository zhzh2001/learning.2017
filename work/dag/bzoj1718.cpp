#include<bits/stdc++.h>
using namespace std;
const int N=5005,M=20005;
int head[N],cc,cnt[N];
struct edge
{
	int v,nxt;
}e[M];
inline void add_edge(int u,int v)
{
	e[++cc].v=v;
	e[cc].nxt=head[u];
	head[u]=cc;
}
namespace tarjan
{
	bool vis[N],inS[N];
	int dfn[N],low[N],f[N],belong[N],t,scc;
	stack<int> S;
	void dfs(int k)
	{
		vis[k]=true;
		dfn[k]=low[k]=++t;
		S.push(k);
		inS[k]=true;
		for(int i=head[k];i;i=e[i].nxt)
			if(!vis[e[i].v])
			{
				f[e[i].v]=(i+1)/2;
				dfs(e[i].v);
				low[k]=min(low[k],low[e[i].v]);
			}
			else
				if(inS[e[i].v]&&f[k]!=(i+1)/2)
					low[k]=min(low[k],dfn[e[i].v]);
		if(dfn[k]==low[k])
		{
			scc++;
			int t;
			do
			{
				t=S.top();S.pop();
				inS[t]=false;
				belong[t]=scc;
			}
			while(t!=k);
		}
	}
};
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	using namespace tarjan;
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	for(int i=1;i<=n;i++)
		for(int j=head[i];j;j=e[j].nxt)
			if(belong[i]!=belong[e[j].v])
			{
				cnt[belong[i]]++;
				cnt[belong[e[j].v]]++;
			}
	int ans=0;
	for(int i=1;i<=scc;i++)
		if(cnt[i]==2)
			ans++;
	cout<<(ans+1)/2<<endl;
	return 0;
}
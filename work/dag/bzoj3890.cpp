#include<bits/stdc++.h>
using namespace std;
const int N=105,T=10005;
int n,mat1[N][N],mat2[N][N],sum;
bool f[N][T],g[N][T];
bool dp(bool f[][T],int mat[][N],int k,int t)
{
	if(k==n&&t==0)
		return true;
	if(t<0)
		return false;
	if(f[k][t])
		return f[k][t];
	for(int i=1;i<=n;i++)
		if(mat[k][i]&&dp(f,mat,i,t-mat[k][i]))
			return f[k][t]=true;
	return f[k][t]=false;
}
int main()
{
	int m;
	cin>>n>>m;
	int sum1=0,sum2=0;
	while(m--)
	{
		int u,v,w1,w2;
		cin>>u>>v>>w1>>w2;
		mat1[u][v]=w1;
		mat2[u][v]=w2;
		sum1+=w1;
		sum2+=w2;
	}
	sum=max(sum1,sum2);
	for(int i=0;i<=sum;i++)
		if(dp(f,mat1,1,i)&&dp(g,mat2,1,i))
		{
			cout<<i<<endl;
			return 0;
		}
	cout<<"IMPOSSIBLE\n";
	return 0;
}
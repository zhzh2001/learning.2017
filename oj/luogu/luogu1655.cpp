#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
const int n = 100;
struct bigint
{
	int len, dig[120];
	bigint(int x = 0)
	{
		memset(dig, 0, sizeof(dig));
		len = 0;
		do
			dig[++len] = x % 10;
		while (x /= 10);
	}
	bigint operator+(const bigint &rhs) const
	{
		bigint ans;
		ans.len = max(len, rhs.len);
		for (int i = 1; i <= ans.len; i++)
		{
			ans.dig[i] += dig[i] + rhs.dig[i];
			ans.dig[i + 1] += ans.dig[i] / 10;
			ans.dig[i] %= 10;
		}
		if (ans.dig[ans.len + 1])
			ans.len++;
		return ans;
	}
	bigint operator*(const bigint &rhs) const
	{
		if (len == 1 && dig[1] == 0)
			return 0;
		bigint ans;
		ans.len = len + rhs.len - 1;
		for (int i = 1; i <= len; i++)
			for (int j = 1; j <= rhs.len; j++)
				ans.dig[i + j - 1] += dig[i] * rhs.dig[j];
		for (int i = 1; i <= ans.len; i++)
		{
			ans.dig[i + 1] += ans.dig[i] / 10;
			ans.dig[i] %= 10;
		}
		if (ans.dig[ans.len + 1])
			ans.len++;
		return ans;
	}
} f[n + 5][n + 5];
ostream &operator<<(ostream &os, const bigint &x)
{
	for (int i = x.len; i; i--)
		os << x.dig[i];
	return os;
}
int main()
{
	f[0][0] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			f[i][j] = f[i - 1][j - 1] + f[i - 1][j] * j;
	int n, m;
	while (cin >> n >> m)
		cout << f[n][m] << endl;
	return 0;
}
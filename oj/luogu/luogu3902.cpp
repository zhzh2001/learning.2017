#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n = 0, m;
	cin >> m;
	for (int i = 1; i <= m; i++)
	{
		int x;
		cin >> x;
		a[upper_bound(a + 1, a + n + 1, x) - a] = x;
		if (a[n + 1])
			n++;
	}
	cout << m - n << endl;
	return 0;
}
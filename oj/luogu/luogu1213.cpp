#include<bits/stdc++.h>
using namespace std;
struct val_t
{
	unsigned a:2,b:2,c:2,d:2,e:2,f:2,g:2,h:2,i:2;
};
struct node
{
	union
	{
		val_t val;
		unsigned dword;
	}val;
	int opr,pred;
}q[(1<<18)+5];
int h,t;
bool vis[1<<18];
inline void add_opr(int id,val_t val)
{
	union
	{
		val_t _val;
		unsigned dword;
	};
	_val=val;
	if(vis[dword&0x3ffff])
		return;
	q[++t].opr=id;
	q[t].pred=h;
	q[t].val.val=val;
	vis[dword&0x3ffff]=true;
}
int main()
{
	int a,b,c,d,e,f,g,H,i;
	cin>>a>>b>>c>>d>>e>>f>>g>>H>>i;
	q[1].val.val.a=a/3;
	q[1].val.val.b=b/3;
	q[1].val.val.c=c/3;
	q[1].val.val.d=d/3;
	q[1].val.val.e=e/3;
	q[1].val.val.f=f/3;
	q[1].val.val.g=g/3;
	q[1].val.val.h=H/3;
	q[1].val.val.i=i/3;
	h=1;t=1;
	while(h<=t)
	{
		if(!q[h].val.dword)
		{
			stack<int> ans;
			for(int i=h;i>1;i=q[i].pred)
				ans.push(q[i].opr);
			while(!ans.empty())
			{
				cout<<ans.top()<<' ';
				ans.pop();
			}
			cout<<endl;
			break;
		}
		val_t tmp=q[h].val.val;
		tmp.a++;tmp.b++;tmp.d++;tmp.e++;
		add_opr(1,tmp);
		
		tmp=q[h].val.val;
		tmp.a++;tmp.b++;tmp.c++;
		add_opr(2,tmp);
		
		tmp=q[h].val.val;
		tmp.b++;tmp.c++;tmp.e++;tmp.f++;
		add_opr(3,tmp);
		
		tmp=q[h].val.val;
		tmp.a++;tmp.d++;tmp.g++;
		add_opr(4,tmp);
		
		tmp=q[h].val.val;
		tmp.b++;tmp.d++;tmp.e++;tmp.f++;tmp.h++;
		add_opr(5,tmp);
		
		tmp=q[h].val.val;
		tmp.c++;tmp.f++;tmp.i++;
		add_opr(6,tmp);
		
		tmp=q[h].val.val;
		tmp.d++;tmp.e++;tmp.g++;tmp.h++;
		add_opr(7,tmp);
		
		tmp=q[h].val.val;
		tmp.g++;tmp.h++;tmp.i++;
		add_opr(8,tmp);
		
		tmp=q[h].val.val;
		tmp.e++;tmp.f++;tmp.h++;tmp.i++;
		add_opr(9,tmp);
		h++;
	}
	return 0;
}
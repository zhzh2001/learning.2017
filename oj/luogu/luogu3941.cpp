#include <iostream>
using namespace std;
const int N = 405, K = 1e6;
int a[N][N], b[N], bucket[K];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, K;
	cin >> n >> m >> K;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			cin >> a[i][j];
	long long ans = 0;
	for (int i = 1; i <= n; i++)
	{
		for (int j = 1; j <= m; j++)
			b[j] = 0;
		for (int j = i; j <= n; j++)
		{
			for (int k = 1; k <= m; k++)
				(b[k] += a[j][k]) %= K;
			bucket[0]++;
			int now = 0;
			for (int k = 1; k <= m; k++)
			{
				(now += b[k]) %= K;
				ans += bucket[now];
				bucket[now]++;
			}
			bucket[0]--;
			now = 0;
			for (int k = 1; k <= m; k++)
			{
				(now += b[k]) %= K;
				bucket[now]--;
			}
		}
	}
	cout << ans << endl;
	return 0;
}
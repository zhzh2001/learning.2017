#include<bits/stdc++.h>
using namespace std;
const int N=200005,INF=0x3f3f3f3f;
vector<pair<int,int>> mat[N];
int d[N],cnt[N];
bool inQ[N];
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n,m;
		cin>>n>>m;
		for(int i=1;i<=n;i++)
			mat[i].clear();
		while(m--)
		{
			int u,v,w;
			cin>>u>>v>>w;
			mat[u].push_back(make_pair(v,w));
			if(w>=0)
				mat[v].push_back(make_pair(u,w));
		}
		fill(d+1,d+n+1,INF);
		fill(cnt+1,cnt+n+1,0);
		fill(inQ+1,inQ+n+1,false);
		d[1]=0;
		deque<int> Q;
		Q.push_back(1);
		inQ[1]=true;
		while(!Q.empty())
		{
			int k=Q.front();Q.pop_front();
			inQ[k]=false;
			for(auto e:mat[k])
				if(d[k]+e.second<d[e.first])
				{
					d[e.first]=d[k]+e.second;
					if(!inQ[e.first])
					{
						inQ[e.first]=true;
						if(!Q.empty()&&d[e.first]<d[Q.front()])
							Q.push_front(e.first);
						else
							Q.push_back(e.first);
						if(++cnt[e.first]>n)
							goto yes;
					}
				}
		}
		cout<<"N0\n";
		continue;
		yes:cout<<"YE5\n";
	}
	return 0;
}
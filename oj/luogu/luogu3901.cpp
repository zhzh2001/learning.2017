#include <iostream>
#include <algorithm>
#include <cmath>
using namespace std;
const int N = 100005;
int b, a[N], bucket[N];
struct quest
{
	int l, r, id;
	bool operator<(const quest &rhs) const
	{
		return l / b == rhs.l / b ? r < rhs.r : l / b < rhs.l / b;
	}
} q[N];
bool ans[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	b = sqrt(n);
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= m; i++)
	{
		cin >> q[i].l >> q[i].r;
		q[i].id = i;
	}
	sort(q + 1, q + m + 1);
	int l = 1, r = 0, cnt = 0;
	for (int i = 1; i <= m; i++)
	{
		while (l < q[i].l)
		{
			if (bucket[a[l]] == 2)
				cnt--;
			bucket[a[l++]]--;
		}
		while (l > q[i].l)
		{
			if (bucket[a[--l]] == 1)
				cnt++;
			bucket[a[l]]++;
		}
		while (r < q[i].r)
		{
			if (bucket[a[++r]] == 1)
				cnt++;
			bucket[a[r]]++;
		}
		while (r > q[i].r)
		{
			if (bucket[a[r]] == 2)
				cnt--;
			bucket[a[r--]]--;
		}
		ans[q[i].id] = !cnt;
	}
	for (int i = 1; i <= m; i++)
		cout << (ans[i] ? "Yes\n" : "No\n");
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
bool p[1000005];
int s[1000005];
int main()
{
	int n,m;
	cin>>n>>m;
	p[1]=true;
	for(int i=2;i*i<=m;i++)
		if(!p[i])
			for(int j=i*i;j<=m;j+=i)
				p[j]=true;
	s[0]=0;
	for(int i=1;i<=m;i++)
		s[i]=s[i-1]+(!p[i]);
	while(n--)
	{
		int l,r;
		cin>>l>>r;
		if(l<=0||r>m)
			cout<<"Crossing the line\n";
		else
			cout<<s[r]-s[l-1]<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int a[10005];
bool vis[10005];
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
int main()
{
	int n;
	cin>>n;
	int m=0,k;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		if(a[i]>m)
		{
			m=a[i];
			k=i;
		}
	}
	vis[k]=true;
	cout<<m<<endl;
	for(int i=2;i<=n;i++)
	{
		int now=0;
		for(int j=1;j<=n;j++)
			if(!vis[j])
			{
				int g=gcd(m,a[j]);
				if(g>now)
				{
					now=g;
					k=j;
				}
			}
		vis[k]=true;
		cout<<now<<endl;
		m=now;
	}
	return 0;
}
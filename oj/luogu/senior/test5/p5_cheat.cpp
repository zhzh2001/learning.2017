#include<iostream>
using namespace std;
const int k=10000000;
const long long l[]={1LL,2574194688LL,40754176LL,6288792576LL,2533638144LL,774194688LL},MOD=10000000000LL;
int main()
{
	int n;
	cin>>n;
	long long ans=l[n/k];
	for(int i=n/k*k+1;i<=n;i++)
	{
		ans=ans*i;
		while(ans%10==0)
			ans/=10;
		ans%=MOD;
	}
	cout<<ans%10<<endl;
	return 0;
}
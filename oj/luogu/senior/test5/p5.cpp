#include<iostream>
using namespace std;
const long long MOD=10000000000LL;
int main()
{
    int n;
    cin>>n;
    long long ans=1;
    for(int i=1;i<=n;i++)
    {
        ans=ans*i;
        while(ans%10==0)
            ans/=10;
        ans%=MOD;
    }
    cout<<ans%10<<endl;
    return 0;
}
#include<bits/stdc++.h>
using namespace std;
vector<int> mat[10005];
bool vis[10005],color[10005];
queue<int> Q;
int bfs(int k)
{
	Q.push(k);
	vis[k]=true;
	color[k]=false;
	int cnt=0,ans1=0;
	while(!Q.empty())
	{
		k=Q.front();
		Q.pop();
		cnt++;
		if(color[k])
			ans1++;
		for(int i=0;i<mat[k].size();i++)
			if(!vis[mat[k][i]])
			{
				vis[mat[k][i]]=true;
				color[mat[k][i]]=!color[k];
				Q.push(mat[k][i]);
			}
			else
				if(color[mat[k][i]]==color[k])
					return -1;
	}
	return min(ans1,cnt-ans1);
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		if(!vis[i])
		{
			int t=bfs(i);
			if(t==-1)
			{
				cout<<"Impossible\n";
				return 0;
			}
			ans+=t;
		}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int a[3005],b[3005],cost[3005];
vector<int> mat[3005];
bool vis[3005];
void dfs(int k)
{
	vis[k]=true;
	for(int i=0;i<mat[k].size();i++)
		if(!vis[mat[k][i]])
			dfs(mat[k][i]);
}
namespace tarjan
{
	stack<int> S;
	int t=0;
	bool vis[3005],inS[3005];
	int dfn[3005],low[3005],cost[3005],into[3005];
	vector<int> mat[3005];
	void dfs(int k)
	{
		vis[k]=true;
		dfn[k]=low[k]=++t;
		S.push(k);
		inS[k]=true;
		
		for(int i=0;i<(::mat[k].size());i++)
			if(!vis[::mat[k][i]])
			{
				dfs(::mat[k][i]);
				low[k]=min(low[k],low[::mat[k][i]]);
			}
			else
				if(inS[::mat[k][i]])
					low[k]=min(low[k],dfn[::mat[k][i]]);
		
		if(low[k]==dfn[k])
		{
			int m=INF,tmp;
			do
			{
				tmp=S.top();
				S.pop();
				inS[tmp]=false;
				for(int i=0;i<(::mat[tmp].size());i++)
					if(low[k]!=low[::mat[tmp][i]])
					{
						mat[low[::mat[tmp][i]]].push_back(low[k]);
						into[low[::mat[tmp][i]]]++;
					}
				if(::cost[tmp]!=INF)
					m=min(m,::cost[tmp]);
			}
			while(tmp!=k);
			if(m!=INF)
				cost[low[k]]=m;
		}
	}
};
int main()
{
	int n,p;
	cin>>n>>p;
	for(int i=1;i<=p;i++)
		cin>>a[i]>>b[i];
	memset(cost,0x3f,sizeof(cost));
	for(int i=1;i<=p;i++)
		cost[a[i]]=b[i];
	int m;
	cin>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
	}
	for(int i=1;i<=p;i++)
		if(!vis[a[i]])
			dfs(a[i]);
	for(int i=1;i<=n;i++)
		if(!vis[i])
		{
			cout<<"NO\n"<<i<<endl;
			return 0;
		}
	cout<<"YES\n";
	memset(tarjan::cost,0x3f,sizeof(tarjan::cost));
	for(int i=1;i<=n;i++)
		if(!tarjan::vis[i]&&cost[i]!=INF)
			tarjan::dfs(i);
	int ans=0;
	for(int i=1;i<=tarjan::t;i++)
		if(!tarjan::into[i]&&tarjan::cost[i]!=INF)
			ans+=tarjan::cost[i];
	cout<<ans<<endl;
	return 0;
}
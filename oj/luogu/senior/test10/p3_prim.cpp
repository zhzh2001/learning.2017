#include<bits/stdc++.h>
using namespace std;
const int N=505;
const double INF=1e100;
struct point
{
	int x,y;
}a[N];
int k,n;
namespace prim
{
	double d[N],ans;
	bool vis[N];
	inline double dist(const int x,const int y)
	{
		return sqrt((a[x].x-a[y].x)*(a[x].x-a[y].x)+(a[x].y-a[y].y)*(a[x].y-a[y].y));
	}
	void solve(int s)
	{
		for(int i=0;i<=n;i++)
			d[i]=(i==s?0:INF);
		memset(vis,false,sizeof(vis));
		ans=0;
		for(int i=1;i+k<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			ans=max(ans,d[j]);
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],dist(j,k));
		}
	}
};
int main()
{
	cin>>k>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i].x>>a[i].y;
	prim::solve(n);
	cout.precision(2);
	cout<<fixed<<prim::ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=305;
int n,mat[N][N];
namespace prim
{
	int d[N],ans;
	bool vis[N];
	void solve(int s)
	{
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		memset(vis,false,sizeof(vis));
		ans=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			ans=max(ans,d[j]);
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],mat[j][k]);
		}
	}
};
int main()
{
	int m;
	cin>>n>>m;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	prim::solve(1);
	cout<<n-1<<' '<<prim::ans<<endl;
	return 0;
}
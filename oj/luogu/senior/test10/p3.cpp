#include<bits/stdc++.h>
using namespace std;
const int N=505;
const double eps=1e-6;
struct point
{
	int x,y;
}p[N];
int k,n,f[N];
inline double dist(int a,int b)
{
	int dx=p[a].x-p[b].x,dy=p[a].y-p[b].y;
	return sqrt(dx*dx+dy*dy);
}
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
bool check(double d)
{
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(dist(i,j)<=d)
			{
				int ri=getf(i),rj=getf(j);
				if(ri!=rj)
					f[ri]=rj;
			}
	int cnt=0;
	for(int i=1;i<=n;i++)
		cnt+=f[i]==i;
	return cnt<=k;
}
int main()
{
	cin>>k>>n;
	for(int i=1;i<=n;i++)
		cin>>p[i].x>>p[i].y;
	double l=0,r=20000;
	while(r-l>eps)
	{
		double mid=(l+r)/2;
		if(check(mid))
			r=mid;
		else
			l=mid;
	}
	cout.precision(2);
	cout<<fixed<<l<<endl;
	return 0;
}
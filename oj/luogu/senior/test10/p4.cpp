#include<bits/stdc++.h>
using namespace std;
const int N=5005;
const double INF=1e100;
struct point
{
	int x,y;
}p[N];
int n;
namespace prim
{
	double d[N],ans;
	bool vis[N];
	inline double dist(int a,int b)
	{
		double dx=p[a].x-p[b].x,dy=p[a].y-p[b].y;
		return sqrt(dx*dx+dy*dy);
	}
	void solve(int s)
	{
		for(int i=0;i<=n;i++)
			d[i]=(i==s?0:INF);
		memset(vis,false,sizeof(vis));
		ans=0;
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			vis[j]=true;
			ans+=d[j];
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],dist(j,k));
		}
	}
};
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>p[i].x>>p[i].y;
	prim::solve(1);
	cout.precision(2);
	cout<<fixed<<prim::ans<<endl;
	return 0;
}
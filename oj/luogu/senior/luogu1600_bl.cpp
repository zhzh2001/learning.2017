#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
const int N = 300005;
int head[N], v[N * 2], nxt[N * 2], e, w[N], ans[N];
pair<int, int> q[N];
inline void add_edge(int u, int v)
{
	::v[++e] = v;
	nxt[e] = head[u];
	head[u] = e;
}
//for n^2 bfs
struct node
{
	int v, pred, d;
	node() {}
	node(int v, int pred, int d) : v(v), pred(pred), d(d) {}
} que[N];
bool vis[N];
//for chain
vector<int> tag[N], rtag[N];
int cnt[N];
//for si=1
int tag2[N];
int dfs(int k, int fat, int dep)
{
	int ret = tag2[k];
	for (int i = head[k]; i; i = nxt[i])
		if (v[i] != fat)
			ret += dfs(v[i], k, dep + 1);
	if (w[k] == dep)
		ans[k] = ret;
	return ret;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		add_edge(u, v);
		add_edge(v, u);
	}
	for (int i = 1; i <= n; i++)
		cin >> w[i];
	for (int i = 1; i <= m; i++)
		cin >> q[i].first >> q[i].second;
	if (n % 10 <= 3)
		for (int i = 1; i <= m; i++)
		{
			int l = 1, r = 0;
			que[++r] = node(q[i].first, 0, 0);
			fill(vis + 1, vis + n + 1, false);
			for (; l <= r; l++)
			{
				int k = que[l].v;
				if (que[l].v == q[i].second)
					break;
				for (int i = head[k]; i; i = nxt[i])
					if (!vis[v[i]])
					{
						vis[v[i]] = true;
						que[++r] = node(v[i], l, que[l].d + 1);
					}
			}
			for (int i = l; i; i = que[i].pred)
				ans[que[i].v] += w[que[i].v] == que[i].d;
		}
	else if (n % 10 == 4)
	{
		for (int i = 1; i <= m; i++)
			if (q[i].first <= q[i].second)
			{
				tag[q[i].first].push_back(q[i].first);
				tag[q[i].second + 1].push_back(-q[i].first);
			}
			else
			{
				rtag[q[i].first].push_back(q[i].first);
				rtag[q[i].second - 1].push_back(-q[i].first);
			}
		for (int i = 1; i <= n + 1; i++)
		{
			for (auto event : tag[i])
				if (event > 0)
					cnt[event]++;
				else
					cnt[-event]--;
			if (i > w[i])
				ans[i] += cnt[i - w[i]];
		}
		for (int i = n; i; i--)
		{
			for (auto event : rtag[i])
				if (event > 0)
					cnt[event]++;
				else
					cnt[-event]--;
			if (i + w[i] <= n)
				ans[i] += cnt[i + w[i]];
		}
	}
	else if (n % 10 == 5)
	{
		for (int i = 1; i <= m; i++)
			tag2[q[i].second]++;
		dfs(1, 0, 0);
	}
	for (int i = 1; i <= n; i++)
		cout << ans[i] << ' ';
	cout << endl;
	return 0;
}
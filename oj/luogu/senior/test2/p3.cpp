#include<bits/stdc++.h>
using namespace std;
vector<int> mat[1005];
int cnt,f[1005];
struct node
{
	int i,d;
	bool operator<(const node& b)const
	{
		return d>b.d;
	}
};
node a[1005];
bool vis[1005];
void dfs(int k,int dep)
{
	a[++cnt].i=k;a[cnt].d=dep;
	vis[k]=true;
	for(int i=0;i<mat[k].size();i++)
		if(!vis[mat[k][i]])
		{
			f[mat[k][i]]=k;
			dfs(mat[k][i],dep+1);
		}
}
void mark(int k,int rem)
{
	vis[k]=true;
	if(rem)
		for(int i=0;i<mat[k].size();i++)
			mark(mat[k][i],rem-1);
}
int main()
{
	int n;
	cin>>n;
	f[1]=1;
	for(int i=2;i<=n;i++)
	{
		int p;
		cin>>p;
		mat[p].push_back(i);
		mat[i].push_back(p);
	}
	memset(vis,false,sizeof(vis));
	cnt=0;
	dfs(1,1);
	sort(a+1,a+n+1);
	memset(vis,false,sizeof(vis));
	cnt=0;
	for(int i=1;i<=n;i++)
		if(!vis[a[i].i])
		{
			mark(f[f[a[i].i]],2);
			cnt++;
		}
	cout<<cnt<<endl;
	return 0;
}
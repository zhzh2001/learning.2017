#include<bits/stdc++.h>
using namespace std;
const int p=1000000007;
int a[805][805],f[805][805][20][2];
int main()
{
	int n,m,k;
	cin>>n>>m>>k;
	k++;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			cin>>a[i][j];
			f[i][j][a[i][j]%k][1]=1;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			for(int t=0;t<k;t++)
			{
				f[i][j][t][0]=(f[i][j][t][0]+f[i-1][j][(t+a[i][j])%k][1]+f[i][j-1][(t+a[i][j])%k][1])%p;
				f[i][j][t][1]=(f[i][j][t][1]+f[i-1][j][((t-a[i][j])%k+k)%k][0]+f[i][j-1][((t-a[i][j])%k+k)%k][0])%p;
			}
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			ans=(ans+f[i][j][0][0])%p;
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
struct bigint
{
	int len,dig[100];
	bigint();
	bigint(int x);
	bigint operator=(int x);
	bigint operator+(const int x)const;
	bigint operator+(const bigint x)const;
	bigint operator+=(const bigint x);
	bigint operator*(const int x)const;
	bool operator<(const bigint b)const;
};
ostream& operator<<(ostream& os,const bigint& b);
int a[85][85];
bigint f[85][85];

bigint::bigint()
{
	len=1;
	memset(dig,0,sizeof(dig));
}

bigint::bigint(int x)
{
	*this=x;
}

bigint bigint::operator=(int x)
{
	len=0;
	memset(dig,0,sizeof(dig));
	do
		dig[++len]=x%10;
	while(x/=10);
	return *this;
}

bigint bigint::operator+(const int x)const
{
	bigint b(x);
	return *this+b;
}

bigint bigint::operator+(const bigint x)const
{
	bigint ret;
	ret.len=max(len,x.len);
	int overflow=0;
	for(int i=1;i<=ret.len;i++)
	{
		ret.dig[i]=dig[i]+x.dig[i]+overflow;
		overflow=ret.dig[i]/10;
		ret.dig[i]%=10;
	}
	if(overflow)
		ret.dig[++ret.len]=overflow;
	return ret;
}

bigint bigint::operator+=(const bigint x)
{
	*this=*this+x;
	return *this;
}

bigint bigint::operator*(const int x)const
{
	bigint ret;
	ret.len=len;
	int overflow=0;
	for(int i=1;i<=ret.len;i++)
	{
		ret.dig[i]=dig[i]*x+overflow;
		overflow=ret.dig[i]/10;
		ret.dig[i]%=10;
	}
	if(overflow)
		ret.dig[++ret.len]=overflow;
	return ret;
}

bool bigint::operator<(const bigint b)const
{
	if(len!=b.len)
		return len<b.len;
	for(int i=len;i>=1;i--)
		if(dig[i]!=b.dig[i])
			return dig[i]<b.dig[i];
	return false;
}

ostream& operator<<(ostream& os,const bigint& b)
{
	for(int i=b.len;i>=1;i--)
		os<<b.dig[i];
	return os;
}

int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>a[i][j];
	bigint ans(0);
	for(int i=1;i<=n;i++)
	{
		memset(f,0,sizeof(f));
		for(int j=1;j<=m;j++)
			f[j][j]=a[i][j];
		for(int j=m;j>=1;j--)
			for(int k=j+1;k<=m;k++)
				f[j][k]=max(f[j+1][k]*2+a[i][j],f[j][k-1]*2+a[i][k]);
		ans+=f[1][m];
	}
	cout<<ans*2<<endl;
	return 0;
}
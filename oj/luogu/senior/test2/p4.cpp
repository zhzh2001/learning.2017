#include<bits/stdc++.h>
using namespace std;
int x[55],p[55],f[55][55][2];
int main()
{
	int n,c;
	cin>>n>>c;
	for(int i=1;i<=n;i++)
	{
		cin>>x[i]>>p[i];
		p[i]+=p[i-1];
	}
	memset(f,0x3f,sizeof(f));
	f[c][c][0]=f[c][c][1]=0;
	for(int i=n;i>=1;i--)
		for(int j=i+1;j<=n;j++)
		{
			f[i][j][0]=min(f[i+1][j][0]+(x[i+1]-x[i])*(p[n]-(p[j]-p[i])),f[i+1][j][1]+(x[j]-x[i])*(p[n]-(p[j]-p[i])));
			f[i][j][1]=min(f[i][j-1][1]+(x[j]-x[j-1])*(p[n]-(p[j-1]-p[i-1])),f[i][j-1][0]+(x[j]-x[i])*(p[n]-(p[j-1]-p[i-1])));
		}
	cout<<min(f[1][n][0],f[1][n][1])<<endl;
	return 0;
}
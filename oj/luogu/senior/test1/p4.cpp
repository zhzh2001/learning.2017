#include<bits/stdc++.h>
#define mymin(x,y) x<y?x:y
#define mymax(x,y) x>y?x:y
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,m,mat[505][505];
vector<int> src[505],dest[505],seg[505];
bool vis[505][505];
struct segment
{
    int l,r;
};
segment s[505];
void dfs(int x,int y,int d)
{
    if(x==n)
        dest[d].push_back(y);
	bool* vnow=&vis[x][y];
    *vnow=true;
	int* now=&mat[x][y];
	if(x>1&&!*(vnow-505)&&*(now-505)<*now)
		dfs(x-1,y,d);
	if(x<n&&!*(vnow+505)&&*(now+505)<*now)
		dfs(x+1,y,d);
	if(y>1&&!*(vnow-1)&&*(now-1)<*now)
		dfs(x,y-1,d);
	if(y<m&&!*(vnow+1)&&*(now+1)<*now)
		dfs(x,y+1,d);
}
bool cmp(int x,int y)
{
    return x>y;
}
int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
    n=getint();m=getint();
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            mat[i][j]=getint();
    for(int j=1;j<=m;j++)
    {
        memset(vis,false,sizeof(vis));
        dfs(1,j,j);
        for(int i=0;i<dest[j].size();i++)
            src[dest[j][i]].push_back(j);
    }
    int cnt=0;
    for(int j=1;j<=m;j++)
        if(src[j].size())
            cnt++;
    if(cnt<m)
    {
		printf("0\n%d\n",m-cnt);
        return 0;
    }
    int ss=0;
    for(int j=1;j<=m;j++)
        if(dest[j].size())
        {
            ss++;
            s[ss].l=s[ss].r=dest[j][0];
            for(int i=1;i<dest[j].size();i++)
            {
                s[ss].l=mymin(s[ss].l,dest[j][i]);
                s[ss].r=mymax(s[ss].r,dest[j][i]);
            }
            seg[s[ss].l].push_back(s[ss].r);
        }
    for(int j=1;j<=m;j++)
        if(seg[j].size())
            sort(seg[j].begin(),seg[j].end(),cmp);
    int ans=0;
    for(int j=1;j<=m;ans++)
    {
        while(!seg[j].size())
            j--;
        j=seg[j][0]+1;
    }
	printf("1\n%d\n",ans);
    return 0;
}
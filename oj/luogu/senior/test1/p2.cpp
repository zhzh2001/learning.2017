#include<bits/stdc++.h>
using namespace std;
int k,an,sum,a[100];
bool vis[100];
bool cmp(int x,int y)
{
	return x>y;
}
bool dfs(int now,int sum,int start)
{
	if(now==0&&sum==0)
		return true;
	if((k-now>0&&k-now<a[an])||(k-now>sum))
		return false;
	int last=0;
	for(int i=start;i<=an;i++)
		if(!vis[i]&&a[i]!=last&&now+a[i]<=k)
		{
			vis[i]=true;
			if(now+a[i]==k)
			{
				if(dfs(0,sum-a[i],1))
					return true;
			}
			else
				if(dfs(now+a[i],sum-a[i],i+1))
					return true;
			last=a[i];
			vis[i]=false;
			if(now==0||a[i]==k-now)
				return false;
		}
	return false;
}
int main()
{
	int n;
	cin>>n;
	an=sum=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		if(x<=50)
		{
			a[++an]=x;
			sum+=a[an];
		}
	}
	sort(a+1,a+an+1,cmp);
	for(int i=a[1];i<=sum/2;i++)
	{
		if(sum%i)
			continue;
		memset(vis,false,sizeof(vis));
		k=i;
		if(dfs(0,sum,1))
		{
			cout<<i<<endl;
			return 0;
		}
	}
	cout<<sum<<endl;
	return 0;
}
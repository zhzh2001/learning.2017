#include<bits/stdc++.h>
using namespace std;
const double pi=3.1415926535897932384626433832795;
int p[10],x[10],y[10];
double r[10];
int main()
{
	int n,x1,y1,x2,y2;
	cin>>n>>x1>>y1>>x2>>y2;
	int s=abs((x1-x2)*(y1-y2));
	if(n==0)
	{
		cout<<s<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
	{
		cin>>x[i]>>y[i];
		p[i]=i;
	}
	double ans=s;
	do
	{
		double now=0;
		for(int i=1;i<=n;i++)
		{
			r[i]=min(min(abs(x1-x[p[i]]),abs(x2-x[p[i]])),min(abs(y1-y[p[i]]),abs(y2-y[p[i]])));
			for(int j=1;j<i;j++)
				r[i]=min(r[i],max(sqrt((x[p[i]]-x[p[j]])*(x[p[i]]-x[p[j]])+(y[p[i]]-y[p[j]])*(y[p[i]]-y[p[j]]))-r[j],(double)0));
			now+=pi*r[i]*r[i];
		}
		ans=min(ans,s-now);
	}
	while(next_permutation(p+1,p+n+1));
	cout.precision(0);
	cout<<fixed<<ans<<endl;
	return 0;
}
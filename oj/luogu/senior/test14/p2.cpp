#include<bits/stdc++.h>
#define Min(a,b) (a)<(b)?(a):(b)
using namespace std;
struct process
{
	int id,start,dur,p;
	bool operator<(const process& b)const
	{
		if(p!=b.p)
			return p<b.p;
		return start>b.start;
	}
};
priority_queue<process> Q;
int main()
{
	process cur;
	int now=0;
	while(scanf("%d%d%d%d",&cur.id,&cur.start,&cur.dur,&cur.p)==4)
	{
		while(!Q.empty()&&now<cur.start)
		{
			process t=Q.top();Q.pop();
			if(now<t.start)
				now=t.start;
			int real_dur=Min(cur.start-now,t.dur);
			t.dur-=real_dur;
			now+=real_dur;
			if(t.dur)
				Q.push(t);
			else
				printf("%d %d\n",t.id,now);
		}
		Q.push(cur);
	}
	while(!Q.empty())
	{
		process t=Q.top();Q.pop();
		now+=t.dur;
		printf("%d %d\n",t.id,now);
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],b[N];
struct node
{
    int x,y;
    node(int x,int y):x(x),y(y){};
    bool operator>(const node& c)const
    {
        return a[x]+b[y]>a[c.x]+b[c.y];
    }
};
priority_queue<node,vector<node>,greater<node> > Q;
int main()
{
    int n;
    cin>>n;
    for(int i=1;i<=n;i++)
        cin>>a[i];
    for(int i=1;i<=n;i++)
        cin>>b[i];
    Q.push(node(1,1));
    for(int i=1;i<=n;i++)
    {
        node k=Q.top();Q.pop();
        cout<<a[k.x]+b[k.y]<<' ';
        Q.push(node(k.x+1,k.y));
        Q.push(node(k.x,k.y+1));
    }
    cout<<endl;
    return 0;
}
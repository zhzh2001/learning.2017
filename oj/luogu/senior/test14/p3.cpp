#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],b[N];
struct node
{
	int ai,bi;
	node(int ai,int bi):ai(ai),bi(bi){};
	bool operator>(const node& c)const
	{
		return a[ai]+b[bi]>a[c.ai]+b[c.bi];
	}
};
priority_queue<node,vector<node>,greater<node> > Q;
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<=n;i++)
		cin>>b[i];
	for(int i=1;i<=n;i++)
		Q.push(node(i,1));
	for(int i=1;i<=n;i++)
	{
		node k=Q.top();Q.pop();
		cout<<a[k.ai]+b[k.bi]<<' ';
		Q.push(node(k.ai,k.bi+1));
	}
	cout<<endl;
	return 0;
}
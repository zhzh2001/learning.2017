#include<bits/stdc++.h>
using namespace std;
const int N=10005;
struct func
{
	int a,b,c;
}f[N];
struct node
{
	int num,x,val;
	node(int num,int x,int val):num(num),x(x),val(val){};
	bool operator>(const node& b)const
	{
		return val>b.val;
	}
};
priority_queue<node,vector<node>,greater<node> > Q;
inline int calc(int num,int x)
{
	return f[num].a*x*x+f[num].b*x+f[num].c;
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>f[i].a>>f[i].b>>f[i].c;
		Q.push(node(i,1,calc(i,1)));
	}
	for(int i=1;i<=m;i++)
	{
		node k=Q.top();Q.pop();
		cout<<k.val<<' ';
		Q.push(node(k.num,k.x+1,calc(k.num,k.x+1)));
	}
	cout<<endl;
	return 0;
}
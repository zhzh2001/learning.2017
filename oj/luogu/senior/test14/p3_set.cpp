#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],b[N];
struct node
{
	int x,y;
	node(int x,int y):x(x),y(y){};
	bool operator<(const node& c)const
	{
		return a[x]+b[y]<a[c.x]+b[c.y];
	}
};
set<node> S;
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<=n;i++)
		cin>>b[i];
	S.insert(node(1,1));
	for(int i=1;i<=n;i++)
	{
		node k=*S.begin();S.erase(S.begin());
		cout<<a[k.x]+b[k.y]<<' ';
		S.insert(node(k.x+1,k.y));
		S.insert(node(k.x,k.y+1));
	}
	cout<<endl;
	return 0;
}
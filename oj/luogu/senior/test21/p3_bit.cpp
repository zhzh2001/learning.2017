#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,m;
long long a[N];
struct BIT
{
	long long tree[N];
	void modify(int x,long long val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	long long query(int x)
	{
		long long ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T,Tmul;
int main()
{
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		a[i]+=a[i-1];
	}
	while(m--)
	{
		int opt;
		cin>>opt;
		if(opt==1)
		{
			int l,r;
			long long k;
			cin>>l>>r>>k;
			T.modify(l,k);
			Tmul.modify(l,k*l);
			T.modify(r+1,-k);
			Tmul.modify(r+1,-k*(r+1));
		}
		else
		{
			int l,r;
			cin>>l>>r;
			long long s1=a[l-1]+l*T.query(l-1)-Tmul.query(l-1);
			long long s2=a[r]+(r+1)*T.query(r)-Tmul.query(r);
			cout<<s2-s1<<endl;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
typedef long long value_t;
value_t a[N],val,p;
int l,r;
namespace segment_tree
{
	struct node
	{
		value_t sum,mul,add;
	}tree[4*N];
	
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id].sum=(tree[lc].sum+tree[rc].sum)%p;
	#define init tree[id].add=0;tree[id].mul=1;
	
	void make(int id,int l,int r)
	{
		init;
		if(l==r)
			tree[id].sum=a[l];
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			upc;
		}
	}
	
	#define _pd(child,fat,sz) tree[child].sum=(tree[child].sum*tree[fat].mul%p+tree[fat].add*(sz)%p)%p;\
	tree[child].add=(tree[child].add*tree[fat].mul%p+tree[fat].add)%p;\
	tree[child].mul=tree[child].mul*tree[fat].mul%p;
	inline void pushdown(int id,int l,int r)
	{
		if(l<r&&!(tree[id].mul==1&&tree[id].add==0))
		{
			calc;
			_pd(lc,id,mid-l+1);_pd(rc,id,r-mid);
		}
		init;
	}
	#undef _pd
	
	//[::l,::r]+=val
	void plus(int id,int l,int r)
	{
		calc;
		pushdown(id,l,r);
		if(::l<=l&&::r>=r)
		{
			tree[id].sum=(tree[id].sum+(r-l+1)*val%p)%p;
			tree[id].add=val;
		}
		else
		{
			if(::l<=mid)
				plus(lc,l,mid);
			if(::r>mid)
				plus(rc,mid+1,r);
			upc;
		}
	}
	
	//[::l,::r]*=val
	void mul(int id,int l,int r)
	{
		calc;
		pushdown(id,l,r);
		if(::l<=l&&::r>=r)
		{
			tree[id].sum=tree[id].sum*val%p;
			tree[id].mul=val;
		}
		else
		{
			if(::l<=mid)
				mul(lc,l,mid);
			if(::r>mid)
				mul(rc,mid+1,r);
			upc;
		}
	}
	
	//val+=sigma(::l,::r)ai
	void query(int id,int l,int r)
	{
		calc;
		pushdown(id,l,r);
		if(::l<=l&&::r>=r)
			val=(val+tree[id].sum)%p;
		else
		{
			if(::l<=mid)
				query(lc,l,mid);
			if(::r>mid)
				query(rc,mid+1,r);
		}	
	}
	
	#undef calc
	#undef upc
	#undef init
	
};
int main()
{
	int n,m;
	cin>>n>>m>>p;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	segment_tree::make(1,1,n);
	while(m--)
	{
		int t;
		cin>>t;
		switch(t)
		{
			case 1:
				cin>>l>>r>>val;
				segment_tree::mul(1,1,n);
				break;
			case 2:
				cin>>l>>r>>val;
				segment_tree::plus(1,1,n);
				break;
			case 3:
				cin>>l>>r;
				val=0;
				segment_tree::query(1,1,n);
				cout<<val<<endl;
				break;
		}
	}
	return 0;
}
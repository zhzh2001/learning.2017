#include<bits/stdc++.h>
using namespace std;
int n,m,a[500005];
inline int getint()
{
	char c;
	while(!isdigit(c=getchar())&&c!='-');
	bool neg=false;
	int ret=0;
	if(c=='-')
		neg=true;
	else
		ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return neg?-ret:ret;
}
inline void putint(int x)
{
	if(x<0)
		putchar('-');
	x=abs(x);
	int buf[15],p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
inline void modify(int x,int val)
{
	for(;x<=n;x+=x&-x)
		a[x]+=val;
}
inline int query(int x)
{
	int ret=0;
	for(;x;x-=x&-x)
		ret+=a[x];
	return ret;
}
int main()
{
	n=getint();m=getint();
	for(int i=1;i<=n;i++)
		modify(i,getint());
	while(m--)
	{
		int x=getint();
		if(x==1)
		{
			x=getint();
			int k=getint();
			modify(x,k);
		}
		else
		{
			int l=getint(),r=getint();
			putint(query(r)-query(l-1));
			puts("");
		}
	}
	return 0;
}
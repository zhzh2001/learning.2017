#include<bits/stdc++.h>
using namespace std;
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int a,b;
		cin>>a>>b;
		if(a<b)
			swap(a,b);
		bool flag=true;
		while(b)
		{
			flag=!flag;
			if(a>2*b)
				break;
			int m=a%b;
			a=b;
			b=m;
		}
		if(flag)
			cout<<"Ollie wins\n";
		else
			cout<<"Stan wins\n";
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005;
bool mat[N][N],vis[N];
int a[N],b[N],cnt[N];
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
};
queue<node> Q;
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int k;
		cin>>k;
		memset(vis,false,sizeof(vis));
		for(int i=1;i<=k;i++)
		{
			cin>>a[i];
			vis[a[i]]=true;
		}
		int cc=0;
		for(int i=a[1];i<=a[k];i++)
			if(!vis[i])
				b[++cc]=i;
		for(int i=1;i<=k;i++)
			for(int j=1;j<=cc;j++)
				if(!mat[a[i]][b[j]])
				{
					mat[a[i]][b[j]]=true;
					cnt[b[j]]++;
				}
	}
	for(int i=1;i<=n;i++)
		if(!cnt[i])
			Q.push(node(i,1));
	int ans=0;
	while(!Q.empty())
	{
		node k=Q.front();Q.pop();
		ans=max(ans,k.w);
		for(int i=1;i<=n;i++)
			if(mat[k.v][i])
			{
				cnt[i]--;
				if(cnt[i]==0)
					Q.push(node(i,k.w+1));
			}
	}
	cout<<ans<<endl;
	return 0;
}
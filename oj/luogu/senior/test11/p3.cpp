#include<bits/stdc++.h>
using namespace std;
const int N=20005,M=100005;
struct edge
{
	int u,v,w;
	bool operator<(const edge& b)const
	{
		return w>b.w;
	}
}e[M];
int n,m,f[2*N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>e[i].u>>e[i].v>>e[i].w;
	sort(e+1,e+m+1);
	for(int i=1;i<=2*n;i++)
		f[i]=i;
	for(int i=1;i<=m;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru==rv)
		{
			cout<<e[i].w<<endl;
			return 0;
		}
		f[ru]=getf(e[i].v+n);
		f[rv]=getf(e[i].u+n);
	}
	cout<<0<<endl;
	return 0;
}
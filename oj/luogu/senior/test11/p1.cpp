#include<bits/stdc++.h>
using namespace std;
int f[10005];
int main()
{
	int n;
	cin>>n;
	f[0]=0;
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		int t,len;
		cin>>t>>len;
		f[i]=0;
		do
		{
			cin>>t;
			f[i]=max(f[i],f[t]);
		}
		while(t);
		f[i]+=len;
		ans=max(ans,f[i]);
	}
	cout<<ans<<endl;
	return 0;
}
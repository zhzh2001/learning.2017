#include<bits/stdc++.h>
using namespace std;
const int p=100003;
vector<int> mat[1000005];
int f[1000005],d[1000005];
queue<int> Q;
bool vis[1000005];
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	f[1]=1;
	d[1]=0;
	vis[1]=true;
	Q.push(1);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=0;i<mat[k].size();i++)
			if(!vis[mat[k][i]])
			{
				vis[mat[k][i]]=true;
				f[mat[k][i]]=f[k];
				d[mat[k][i]]=d[k]+1;
				Q.push(mat[k][i]);
			}
			else
				if(d[mat[k][i]]==d[k]+1)
					f[mat[k][i]]=(f[mat[k][i]]+f[k])%p;
	}
	for(int i=1;i<=n;i++)
		cout<<f[i]<<endl;
	return 0;
}
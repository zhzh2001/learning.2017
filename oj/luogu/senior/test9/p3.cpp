#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f,INFM=0x3f;
int n,mat[105][105];
namespace dijkstra
{
	int d[105];
	bool vis[105];
	void solve(int s)
	{
		memset(d,INFM,sizeof(d));
		d[s]=0;
		memset(vis,false,sizeof(vis));
		for(int i=1;i<=n;i++)
		{
			int j=0;
			for(int k=1;k<=n;k++)
				if(!vis[k]&&d[k]<d[j])
					j=k;
			if(j==0)
				return;
			vis[j]=true;
			for(int k=1;k<=n;k++)
				if(!vis[k])
					d[k]=min(d[k],d[j]+mat[j][k]);
		}
	}
};
int main()
{
	int s,t;
	cin>>n>>s>>t;
	memset(mat,INFM,sizeof(mat));
	for(int i=1;i<=n;i++)
	{
		int m;
		cin>>m;
		for(int j=1;j<=m;j++)
		{
			int x;
			cin>>x;
			mat[i][x]=j==1?0:1;
		}
	}
	dijkstra::solve(s);
	if(dijkstra::d[t]<INF)
		cout<<dijkstra::d[t]<<endl;
	else
		cout<<-1<<endl;
	return 0;
}
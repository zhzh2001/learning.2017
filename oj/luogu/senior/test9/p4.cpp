#include<bits/stdc++.h>
using namespace std;
const int N=200,INF=0x3f3f3f3f;
int t[N],mat[N][N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=0;i<n;i++)
		cin>>t[i];
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			mat[i][j]=(i==j?0:INF);
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	int q;
	cin>>q;
	int cur=0;
	while(q--)
	{
		int x,y,now;
		cin>>x>>y>>now;
		for(;cur<n&&t[cur]<=now;cur++)
			for(int i=0;i<n;i++)
				for(int j=0;j<n;j++)
					mat[i][j]=min(mat[i][j],mat[i][cur]+mat[cur][j]);
		if(mat[x][y]==INF||x>=cur||y>=cur)
			cout<<-1<<endl;
		else
			cout<<mat[x][y]<<endl;
	}
	return 0;
}
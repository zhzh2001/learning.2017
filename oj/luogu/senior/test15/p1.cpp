#include<bits/stdc++.h>
using namespace std;
const int N=200005,MIN=0x80000000;
int t,x,y,a[N];
bool q[N];
namespace segment_tree
{
	int tree[4*N];
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc tree[id]=max(tree[lc],tree[rc]);
	void make(int id,int l,int r)
	{
		if(l==r)
			tree[id]=MIN;
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			//upc;
		}
	}
	void update(int id,int l,int r)
	{
		if(l==r&&l==y)
			tree[id]=x;
		else
		{
			calc;
			if(y<=mid)
				update(lc,l,mid);
			else
				update(rc,mid+1,r);
			upc;
		}
	}
	void query(int id,int l,int r)
	{
		if(x<=l&&y>=r)
			t=max(t,tree[id]);
		else
		{
			calc;
			if(x<=mid)
				query(lc,l,mid);
			if(y>mid)
				query(rc,mid+1,r);
		}
	}
};
int main()
{
	int m,d;
	cin>>m>>d;
	int n=0;
	for(int i=1;i<=m;i++)
	{
		string cmd;
		cin>>cmd>>a[i];
		q[i]=cmd=="Q";
		n+=!q[i];
	}
	segment_tree::make(1,1,n);
	t=0;
	int cc=0;
	for(int i=1;i<=m;i++)
		if(q[i])
		{
			t=MIN;
			x=cc-a[i]+1;y=cc;
			segment_tree::query(1,1,n);
			cout<<t<<endl;
		}
		else
		{
			x=(t+a[i])%d;
			y=++cc;
			segment_tree::update(1,1,n);
		}
	return 0;
}
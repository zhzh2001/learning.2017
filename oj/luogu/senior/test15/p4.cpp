#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int x,y,val;
struct node
{
	int l,r;
}a[N*2];
struct segment_tree
{
	int tree[N*4];
	set<int> S;
	#define calc int mid=(l+r)/2,lc=id*2,rc=lc+1;
	#define upc if(tree[lc]==tree[rc])\
		tree[id]=tree[lc];\
	else\
		tree[id]=0;
	void make(int id,int l,int r)
	{
		if(l==r)
			tree[id]=0;
		else
		{
			calc;
			make(lc,l,mid);
			make(rc,mid+1,r);
			upc;
		}
	}
	inline void pushdown(int id,int l,int r)
	{
		if(tree[id]&&l<r)
		{
			calc;
			tree[lc]=tree[rc]=tree[id];
		}
	}
	void update(int id,int l,int r)
	{
		if(x<=l&&y>=r)
			tree[id]=val;
		else
		{
			calc;
			pushdown(id,l,r);
			if(x<=mid)
				update(lc,l,mid);
			if(y>mid)
				update(rc,mid+1,r);
			upc;
		}
	}
	void query(int id,int l,int r)
	{
		if(x<=l&&y>=r)
		{
			if(tree[id])
				S.insert(tree[id]);
		}
		else
		{
			calc;
			pushdown(id,l,r);
			if(x<=mid)
				query(lc,l,mid);
			if(y>mid)
				query(rc,mid+1,r);
		}
	}
}T;
int main()
{
	int n;
	cin>>n;
	int valid=0,cnt=0;
	T.make(1,1,100000);
	while(n--)
	{
		string s;
		cin>>s;
		if(s=="A")
		{
			cin>>x>>y;
			a[++cnt].l=x;a[cnt].r=y;
			T.S.clear();
			T.query(1,1,100000);
			for(set<int>::iterator i=T.S.begin();i!=T.S.end();i++)
			{
				x=a[*i].l;y=a[*i].r;
				val=0;
				T.update(1,1,100000);
			}
			valid-=T.S.size();
			cout<<T.S.size()<<endl;
			val=cnt;
			T.update(1,1,100000);
			valid++;
		}
		else
			cout<<valid<<endl;
	}
	return 0;
}
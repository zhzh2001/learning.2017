#include<bits/stdc++.h>
using namespace std;
const int N=50005,M=200005,AI=1000005;
int a[N],sz,cnt[AI],ans[M];
struct question
{
	int l,r,id;
	bool operator<(const question& rhs)const
	{
		if(l/sz==rhs.l/sz)
			return r<rhs.r;
		return l<rhs.l;
	}
}q[M];
int main()
{
	int n;
	scanf("%d",&n);
	sz=floor(sqrt(n));
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	int m;
	scanf("%d",&m);
	for(int i=1;i<=m;i++)
	{
		scanf("%d%d",&q[i].l,&q[i].r);
		q[i].id=i;
	}
	sort(q+1,q+m+1);
	int l=1,r=0,now=0;
	for(int i=1;i<=m;i++)
	{
		while(l<q[i].l)
			if(!--cnt[a[l++]])
				now--;
		while(l>q[i].l)
			if(!cnt[a[--l]]++)
				now++;
		while(r<q[i].r)
			if(!cnt[a[++r]]++)
				now++;
		while(r>q[i].r)
			if(!--cnt[a[r--]])
				now--;
		ans[q[i].id]=now;
	}
	for(int i=1;i<=m;i++)
		printf("%d\n",ans[i]);
	return 0;
}
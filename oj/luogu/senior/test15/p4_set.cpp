#include<bits/stdc++.h>
using namespace std;
typedef pair<int,int> pii;
int main()
{
	int n;
	cin>>n;
	set<pii> S;
	while(n--)
	{
		char opt;
		cin>>opt;
		if(opt=='A')
		{
			int l,r;
			cin>>l>>r;
			int cnt=0;
			while(true)
			{
				auto p=S.lower_bound(make_pair(l,r));
				if(p==S.end()||!(p->first<=r&&p->second>=l))
					if(p==S.begin())
						break;
					else
						p--;
				if(p->first<=r&&p->second>=l)
				{
					S.erase(p);
					cnt++;
				}
				else
					break;
			}
			S.insert(make_pair(l,r));
			cout<<cnt<<endl;
		}
		else
			cout<<S.size()<<endl;
	}
	return 0;
}
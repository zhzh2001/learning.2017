#include <iostream>
using namespace std;
const int N = 2000;
int f[N + 5][N + 5];
int main()
{
	ios::sync_with_stdio(false);
	int t, k;
	cin >> t >> k;
	f[0][0] = 1;
	for (int i = 1; i <= N; i++)
	{
		f[i][0] = 1;
		for (int j = 1; j <= i; j++)
			f[i][j] = (f[i - 1][j] + f[i - 1][j - 1]) % k;
	}
	f[0][0] = 0;
	for (int i = 1; i <= N; i++)
	{
		f[i][0] = 0;
		for (int j = 1; j <= i; j++)
			f[i][j] = f[i - 1][j] + f[i][j - 1] - f[i - 1][j - 1] + (f[i][j] == 0);
		f[i][i + 1] = f[i][i];
	}
	while (t--)
	{
		int n, m;
		cin >> n >> m;
		m = min(n, m);
		cout << f[n][m] << endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=10005;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
};
vector<node> mat[N];
int n,m;
/*
namespace spfa
{
	int d[N],cnt[N];
	bool inq[N];
	bool solve(int s)
	{
		memset(cnt,0,sizeof(cnt));
		memset(d,0x3f,sizeof(d));
		d[s]=0;
		queue<int> Q;
		Q.push(s);
		memset(inq,false,sizeof(inq));
		inq[s]=true;
		int cc=0;
		while(!Q.empty())
		{
			cc++;
			if(cc>1000000)
				return false;
			int k=Q.front();Q.pop();
			inq[k]=false;
			for(int i=0;i<mat[k].size();i++)
				if(d[k]+mat[k][i].w<d[mat[k][i].v])
				{
					d[mat[k][i].v]=d[k]+mat[k][i].w;
					if(!inq[mat[k][i].v])
					{
						Q.push(mat[k][i].v);
						inq[mat[k][i].v]=true;
						if(++cnt[mat[k][i].v]>n)
							return false;
					}
				}
		}
		return true;
	}
};
*/
namespace spfa_dfs
{
	int d[N];
	bool vis[N];
	void init(int k)
	{
		memset(d,0x3f,sizeof(d));
		d[k]=0;
		memset(vis,false,sizeof(vis));
	}
	bool solve(int k)
	{
		vis[k]=true;
		for(int i=0;i<mat[k].size();i++)
			if(d[k]+mat[k][i].w<d[mat[k][i].v])
			{
				d[mat[k][i].v]=d[k]+mat[k][i].w;
				if(vis[mat[k][i].v]||!solve(mat[k][i].v))
					return false;
			}
		vis[k]=false;
		return true;
	}
};
int main()
{
	cin>>n>>m;
	while(m--)
	{
		int t,a,b,c;
		cin>>t;
		switch(t)
		{
			case 1:
				cin>>a>>b>>c;
				mat[a].push_back(node(b,-c));
				break;
			case 2:
				cin>>a>>b>>c;
				mat[b].push_back(node(a,c));
				break;
			case 3:
				cin>>a>>b;
				mat[a].push_back(node(b,0));
				mat[b].push_back(node(a,0));
				break;
		}
	}
	for(int i=1;i<=n;i++)
		mat[0].push_back(node(i,0));
	spfa_dfs::init(0);
	if(spfa_dfs::solve(0))
		cout<<"Yes\n";
	else
		cout<<"No\n";
	return 0;
}
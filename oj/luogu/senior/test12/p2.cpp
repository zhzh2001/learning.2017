#include<bits/stdc++.h>
using namespace std;
const int N=5005;
vector<int> mat[N];
namespace tarjan
{
	stack<int> S;
	bool vis[N],inS[N];
	int dfn[N],low[N],cc=0,t=0;
	set<int> scc[N];
	void dfs(int k)
	{
		vis[k]=true;
		dfn[k]=low[k]=++t;
		S.push(k);
		inS[k]=true;
		
		for(int i=0;i<(mat[k].size());i++)
			if(!vis[mat[k][i]])
			{
				dfs(mat[k][i]);
				low[k]=min(low[k],low[mat[k][i]]);
			}
			else
				if(inS[mat[k][i]])
					low[k]=min(low[k],dfn[mat[k][i]]);
		
		if(low[k]==dfn[k])
		{
			cc++;
			int tmp;
			do
			{
				tmp=S.top();
				S.pop();
				inS[tmp]=false;
				scc[cc].insert(tmp);
			}
			while(tmp!=k);
		}
	}
};
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v,t;
		cin>>u>>v>>t;
		mat[u].push_back(v);
		if(t==2)
			mat[v].push_back(u);
	}
	memset(tarjan::vis,false,sizeof(tarjan::vis));
	memset(tarjan::inS,false,sizeof(tarjan::inS));
	for(int i=1;i<=n;i++)
		if(!tarjan::vis[i])
			tarjan::dfs(i);
	int mx=0,mn,id;
	for(int i=1;i<=tarjan::cc;i++)
		if(tarjan::scc[i].size()>mx)
		{
			mx=tarjan::scc[i].size();
			mn=*tarjan::scc[i].begin();
			id=i;
		}
		else
			if(tarjan::scc[i].size()==mx&&*tarjan::scc[i].begin()<mn)
			{
				mn=*tarjan::scc[i].begin();
				id=i;
			}
	cout<<mx<<endl;
	for(set<int>::iterator i=tarjan::scc[id].begin();i!=tarjan::scc[id].end();i++)
		cout<<*i<<' ';
	cout<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int CNT=100000;
int a[50],ans[50];
bool vis[50];
int main()
{
	srand(time(NULL));
	int n,m;
	cin>>n>>m;
	for(int i=0;i<m;i++)
		cin>>a[i];
	for(int i=1;i<=CNT;i++)
	{
		memset(vis,false,sizeof(vis));
		int now=0;
		for(int j=1;j<n;j++)
		{
			int d=a[rand()%m];
			d%=n-j+1;
			if(d==0)
				d=n-j+1;
			int k;
			for(k=(now+1)%n;d;k=(k+1)%n)
				if(!vis[k])
					d--;
			vis[(k-1+n)%n]=true;
			for(;vis[k];k=(k+1)%n);
			now=k;
		}
		for(int j=0;j<n;j++)
			if(!vis[j])
				ans[j]++;
	}
	cout<<setprecision(2)<<fixed;
	for(int i=0;i<n;i++)
	{
		if(i)
			cout<<' ';
		cout<<(double)ans[i]/CNT*100<<'%';
	}
	cout<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=30005;
struct node
{
	int fat,d,cnt;
	node(){fat=d=cnt=0;}
	node(int fat,int d,int cnt):fat(fat),d(d),cnt(cnt){};
}f[N];
int getf(int x)
{
	if(f[x].fat==x)
		return x;
	int t=getf(f[x].fat);
	f[x].d+=f[f[x].fat].d;
	return f[x].fat=t;
}
inline void merge_set(int a,int b)
{
	int ra=getf(a),rb=getf(b);
	if(ra!=rb)
	{
		f[ra].fat=rb;
		f[ra].d=f[rb].cnt;
		f[rb].cnt+=f[ra].cnt;
	}
}
int main()
{
	for(int i=1;i<=N;i++)
		f[i]=node(i,0,1);
	int m;
	cin>>m;
	while(m--)
	{
		string cmd;
		int x,y;
		cin>>cmd>>x>>y;
		if(cmd=="M")
			merge_set(x,y);
		else
		{
			int rx=getf(x),ry=getf(y);
			if(rx==ry)
				cout<<abs(f[x].d-f[y].d)-1<<endl;
			else
				cout<<-1<<endl;
		}
	}
	return 0;
}
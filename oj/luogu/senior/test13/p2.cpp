#include<bits/stdc++.h>
using namespace std;
const int N=50005;
struct node
{
	int fat,d;
}f[N];
int getf(int x)
{
	if(f[x].fat==x)
		return x;
	int t=getf(f[x].fat);
	f[x].d=(f[x].d+f[f[x].fat].d)%3;
	return f[x].fat=t;
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		f[i].fat=i;
		f[i].d=0;
	}
	int ans=0;
	while(m--)
	{
		int t,x,y;
		cin>>t>>x>>y;
		if(x>n||y>n||(t==2&&x==y))
			ans++;
		else
		{
			int rx=getf(x),ry=getf(y);
			if(rx!=ry)
				if(t==1)
				{
					f[ry].fat=rx;
					f[ry].d=(f[x].d-f[y].d+3)%3;
				}
				else
				{
					f[ry].fat=rx;
					f[ry].d=(f[x].d-f[y].d+4)%3;
				}
			else
				if((t==1&&f[x].d%3!=f[y].d%3)||(t==2&&(f[y].d-f[x].d+3)%3!=1))
					ans++;
		}
	}
	cout<<ans<<endl;
	return 0;
}
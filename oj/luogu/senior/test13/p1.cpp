#include<bits/stdc++.h>
using namespace std;
const int N=1005,M=100005;
struct edge
{
	int u,v,t;
	bool operator<(const edge& b)const
	{
		return t<b.t;
	}
}e[M];
int n,m,f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>e[i].u>>e[i].v>>e[i].t;
	sort(e+1,e+m+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	int ans=0;
	for(int i=1;i<=m;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv)
		{
			f[ru]=rv;
			ans++;
			if(ans==n-1)
			{
				cout<<e[i].t<<endl;
				return 0;
			}
		}
	}
	cout<<-1<<endl;
	return 0;
}
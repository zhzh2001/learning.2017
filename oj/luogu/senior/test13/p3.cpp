#include<bits/stdc++.h>
using namespace std;
const int N=400000;
int f[N],a[N],ans[N];
vector<int> mat[N];
bool d[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
inline bool merge_set(int a,int b)
{
	int ra=getf(a),rb=getf(b);
	if(ra!=rb)
		f[ra]=rb;
	return ra!=rb;
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=0;i<n;i++)
		f[i]=i;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	int k;
	cin>>k;
	for(int i=1;i<=k;i++)
	{
		cin>>a[i];
		d[a[i]]=true;
	}
	for(int i=0;i<n;i++)
		if(!d[i])
			for(int j=0;j<mat[i].size();j++)
				if(!d[mat[i][j]])
					merge_set(i,mat[i][j]);
	int now=0;
	for(int i=0;i<n;i++)
		now+=(!d[i]&&f[i]==i);
	for(int i=k;i;i--)
	{
		ans[i]=now++;
		d[a[i]]=false;
		for(int j=0;j<mat[a[i]].size();j++)
			if(!d[mat[a[i]][j]])
				now-=merge_set(a[i],mat[a[i]][j]);
	}
	cout<<now<<endl;
	for(int i=1;i<=k;i++)
		cout<<ans[i]<<endl;
	return 0;
}
#include <bits/stdc++.h>
using namespace std;
const int N = 105, NMOD = 3, M = 1e6, MOD[NMOD] = {23333, 15461, 59921};
int n, m, a[N][NMOD];
bool ans[M];
int eval(int x, int mid)
{
	long long ans = 0;
	for (int i = n; i >= 0; i--)
		ans = (ans * x + a[i][mid]) % MOD[mid];
	return ans;
}
int main()
{
	cin >> n >> m;
	for (int i = 0; i <= n; i++)
	{
		string s;
		cin >> s;
		int sign = 1;
		if (s[0] == '-')
		{
			sign = -1;
			s.erase(0, 1);
		}
		for (int j = 0; j < s.length(); j++)
			for (int k = 0; k < NMOD; k++)
				a[i][k] = (a[i][k] * 10 + s[j] - '0') % MOD[k];
		for (int j = 0; j < NMOD; j++)
			a[i][j] *= sign;
	}
	int M = min(m, MOD[0]);
	for (int i = 1; i <= M; i++)
		if (eval(i, 0) == 0)
			for (int j = i; j <= m; j += MOD[0])
				ans[j] = eval(j, 1) == 0 && eval(j, 2) == 0;
	cout << count(ans + 1, ans + m + 1, true) << endl;
	for (int i = 1; i <= m; i++)
		if (ans[i])
			cout << i << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
int a[N];
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n;
		scanf("%d",&n);
		for(int i=1;i<=n;i++)
			scanf("%d",a+i);
		nth_element(a+1,a+(n+1)/2,a+n+1);
		printf("%d\n",a[(n+1)/2]);
	}
	return 0;
}
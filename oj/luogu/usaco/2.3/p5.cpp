#include<bits/stdc++.h>
using namespace std;
int mat[105][105];
bool ctrl[105][105];
int main()
{
	int n,m;
	cin>>m;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		n=max(n,max(u,v));
		mat[u][v]=w;
		if(w>50)
			ctrl[u][v]=true;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			if(!ctrl[i][j])
			{
				int w=0;
				for(int k=1;k<=n;k++)
					if(ctrl[i][k])
						w+=mat[k][j];
				if(w>50)
					ctrl[i][j]=true;
			}
			if(ctrl[i][j])
				cout<<i<<' '<<j<<endl;
		}
	return 0;
}
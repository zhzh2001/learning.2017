#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
bool vis[1000005];
int main()
{
	int n;
	cin>>n;
	memset(vis,false,sizeof(vis));
	int lo=1000000,hi=0;
	for(int i=1;i<=n;i++)
	{
		int l,r;
		cin>>l>>r;
		lo=min(lo,l);
		hi=max(hi,r);
		for(int j=l;j<r;j++)
			vis[j]=true;
	}
	int ans[2]={0,0},pre=lo;
	bool cur=vis[pre];
	for(int i=lo+1;i<=hi+1;i++)
		if(vis[i]^cur)
		{
			ans[cur]=max(ans[cur],i-pre);
			pre=i;
			cur=!cur;
		}
	cout<<ans[1]<<' '<<ans[0]<<endl;
	return 0;
}
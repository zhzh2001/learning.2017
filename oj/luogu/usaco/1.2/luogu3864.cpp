#include <iostream>
#include <string>
using namespace std;
const string dict = "ABCDEFGHIJKLMNOPRSTUVWXY";
int main()
{
	ios::sync_with_stdio(false);
	long long num;
	cin >> num;
	string s;
	bool found = false;
	while (cin >> s)
	{
		long long now = 0;
		for (size_t i = 0; i < s.length(); i++)
			now = now * 10 + dict.find(s[i]) / 3 + 2;
		if (now == num)
		{
			cout << s << endl;
			found = true;
		}
	}
	if (!found)
		cout << "NONE\n";
	return 0;
}
#include<iostream>
using namespace std;
const int n=300;
const char dig[]="0123456789ABCDEFGHIJ";
string numtob(int num,int b)
{
	string ans="";
	do
		ans=dig[num%b]+ans;
	while(num/=b);
	return ans;
}
bool ispalind(const string& s)
{
	int i=0,j=s.length()-1;
	while(i<j)
		if(s[i++]!=s[j--])
			return false;
	return true;
}
int main()
{
	int b;
	cin>>b;
	for(int i=1;i<=n;i++)
		if(ispalind(numtob(i*i,b)))
			cout<<numtob(i,b)<<' '<<numtob(i*i,b)<<endl;
	return 0;
}
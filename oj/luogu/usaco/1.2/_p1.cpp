#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int a[1000005];
int main()
{
	int n;
	cin>>n;
	memset(a,0,sizeof(a));
	int lo=1000000,hi=0;
	for(int i=1;i<=n;i++)
	{
		int l,r;
		cin>>l>>r;
		lo=min(lo,l);
		hi=max(hi,r);
		a[l]++;a[r]--;
	}
	int ans[2]={0,0},pre=lo,now=a[lo];
	bool cur=true;
	for(int i=lo+1;i<=hi+1;i++)
	{
		now+=a[i];
		if((now>0)^cur)
		{
			ans[cur]=max(ans[cur],i-pre);
			pre=i;
			cur=!cur;
		}
	}
	cout<<ans[1]<<' '<<ans[0]<<endl;
	return 0;
}
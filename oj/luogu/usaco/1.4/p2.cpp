#include<iostream>
#include<cstring>
#include<queue>
using namespace std;
bool vis[8005],ans[25];
struct node
{
	int a,b,c;
};
queue<node> q;
inline void pushq(node x)
{
	int code=x.a*400+x.b*20+x.c;
	if(!vis[code])
	{
		q.push(x);
		vis[x.a*400+x.b*20+x.c]=true;
		if(x.a==0)
			ans[x.c]=true;
	}
}
inline void mv(int& a,int& b,int fb)
{
	if(a+b<=fb)
	{
		b=a+b;
		a=0;
	}
	else
	{
		a-=(fb-b);
		b=fb;
	}
}
int main()
{
	int a,b,c;
	cin>>a>>b>>c;
	memset(vis,false,sizeof(vis));
	memset(ans,false,sizeof(ans));
	pushq((node){0,0,c});
	while(!q.empty())
	{
		node k=q.front(),now;
		q.pop();
		
		now=k;
		mv(now.a,now.b,b);
		pushq(now);
		
		now=k;
		mv(now.b,now.a,a);
		pushq(now);
		
		now=k;
		mv(now.a,now.c,c);
		pushq(now);
		
		now=k;
		mv(now.c,now.a,a);
		pushq(now);
		
		now=k;
		mv(now.b,now.c,c);
		pushq(now);
		
		now=k;
		mv(now.c,now.b,b);
		pushq(now);
	}
	for(int i=0;i<=c;i++)
		if(ans[i])
			cout<<i<<' ';
	cout<<endl;
	return 0;
}
#include <iostream>
#include <cstring>
#include <string>
#include <algorithm>
using namespace std;
const int N = 105;
bool a[N];
int state[N];
string l[20];
int main()
{
	int n, c;
	cin >> n >> c;
	memset(state, -1, sizeof(state));
	int lit;
	while (cin >> lit && ~lit)
		state[lit] = 1;
	while (cin >> lit && ~lit)
		state[lit] = 0;
	int cnt = 0;
	for (int b1 = 0; b1 < 2; b1++)
		for (int b2 = 0; b2 < 2; b2++)
			for (int b3 = 0; b3 < 2; b3++)
				for (int b4 = 0; b4 < 2; b4++)
					if (b1 + b2 + b3 + b4 <= c && ((b1 + b2 + b3 + b4) & 1) == (c & 1))
					{
						memset(a, true, sizeof(a));
						if (b1)
							for (int i = 1; i <= n; i++)
								a[i] ^= 1;
						if (b2)
							for (int i = 1; i <= n; i += 2)
								a[i] ^= 1;
						if (b3)
							for (int i = 2; i <= n; i += 2)
								a[i] ^= 1;
						if (b4)
							for (int i = 1; i <= n; i += 3)
								a[i] ^= 1;
						string s;
						for (int i = 1; i <= n; i++)
						{
							if (~state[i] && state[i] ^ a[i])
							{
								s = "";
								break;
							}
							s = s + (a[i] ? '1' : '0');
						}
						if (s != "")
							l[++cnt] = s;
					}
	if (!cnt)
		cout << "IMPOSSIBLE\n";
	else
	{
		sort(l + 1, l + cnt + 1);
		for (int i = 1; i <= cnt; i++)
			cout << l[i] << endl;
	}
	return 0;
}
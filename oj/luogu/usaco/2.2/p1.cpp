#include<bits/stdc++.h>
using namespace std;
const string alpha[13]={"I","IV","V","IX","X","XL","L","XC","C","CD","D","CM","M"},dict="IVXLCDM";
const int num[13]={1,4,5,9,10,40,50,90,100,400,500,900,1000};
int cnt[26];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		int x=i;
		for(int j=12;j>=0;j--)
			while(x>=num[j])
			{
				x-=num[j];
				for(int k=0;k<alpha[j].length();k++)
					cnt[alpha[j][k]]++;
			}
	}
	for(int i=0;i<dict.length();i++)
		if(cnt[dict[i]])
			cout<<dict[i]<<' '<<cnt[dict[i]]<<endl;
	return 0;
}
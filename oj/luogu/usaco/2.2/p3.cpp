#include<bits/stdc++.h>
using namespace std;
int buf[10];
bool vis[10];
int main()
{
	int m;
	cin>>m;
	for(int i=m+1;;i++)
	{
		memset(vis,false,sizeof(vis));
		int t=i,j=0;
		bool rep=false;
		do
		{
			buf[++j]=t%10;
			if(vis[buf[j]]||!buf[j])
			{
				rep=true;
				break;
			}
			vis[buf[j]]=true;
		}
		while(t/=10);
		if(!rep)
		{
			memset(vis,false,sizeof(vis));
			int p=j;
			for(t=1;t<=j;t++)
			{
				p=((p-buf[p])%j+j-1)%j+1;
				if(vis[p])
				{
					rep=true;
					break;
				}
				vis[p]=true;
			}
			if(!rep&&p==j)
			{
				cout<<i<<endl;
				break;
			}
		}
	}
	return 0;
}
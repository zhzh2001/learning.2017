#include<bits/stdc++.h>
using namespace std;
const int n=52,INF=0x3f3f3f3f;
int mat[55][55],d[55];
bool vis[55];
inline int char2num(char c)
{
	if(islower(c))
		return c-'a'+1;
	else
		return c-'A'+27;
}
int main()
{
	int m;
	cin>>m;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		string u,v;
		int w;
		cin>>u>>v>>w;
		int uu=char2num(u[0]),vv=char2num(v[0]);
		mat[uu][vv]=min(mat[uu][vv],w);
		mat[vv][uu]=min(mat[vv][uu],w);
	}
	memset(vis,false,sizeof(vis));
	memset(d,0x3f,sizeof(d));
	d[n]=0;
	for(int i=1;;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		if(!j)
			break;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],d[j]+mat[j][k]);
	}
	int ans=INF,num;
	for(int i=27;i<n;i++)
		if(d[i]<ans)
		{
			ans=d[i];
			num=i;
		}
	putchar(num+'A'-27);
	cout<<' '<<ans<<endl;
	return 0;
}
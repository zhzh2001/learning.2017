#include<bits/stdc++.h>
using namespace std;
const int n=10,dx[]={-1,0,1,0},dy[]={0,1,0,-1};
string mat[10];
int main()
{
	int cx,cy,fx,fy;
	for(int i=0;i<n;i++)
	{
		cin>>mat[i];
		for(int j=0;j<n;j++)
			if(mat[i][j]=='C')
			{
				cx=i;
				cy=j;
			}
			else
				if(mat[i][j]=='F')
				{
					fx=i;
					fy=j;
				}
	}
	int cf=0,ff=0;
	for(int i=1;i<=10000;i++)
	{
		int nx=cx+dx[cf],ny=cy+dy[cf];
		if(nx>=0&&nx<n&&ny>=0&&ny<n&&mat[nx][ny]!='*')
		{
			cx=nx;
			cy=ny;
		}
		else
			cf=(cf+1)%4;
		nx=fx+dx[ff];ny=fy+dy[ff];
		if(nx>=0&&nx<n&&ny>=0&&ny<n&&mat[nx][ny]!='*')
		{
			fx=nx;
			fy=ny;
		}
		else
			ff=(ff+1)%4;
		if(cx==fx&&cy==fy)
		{
			cout<<i<<endl;
			return 0;
		}
	}
	cout<<0<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
string mat[205];
int n,m,d[205][100];
void dfs(int x,int y)
{
	for(int i=0;i<4;i++)
	{
		int nx=x*2+dx[i]-1,ny=y*2+dy[i]-1;
		if(nx>=0&&nx<2*n+1&&ny>=0&&ny<2*m+1&&mat[nx][ny]==' '&&d[x][y]+1<d[x+dx[i]][y+dy[i]])
		{
			d[x+dx[i]][y+dy[i]]=d[x][y]+1;
			dfs(x+dx[i],y+dy[i]);
		}
	}
}
int main()
{
	cin>>m>>n;
	int sx1=0,sy1=0,sx2,sy2;
	getline(cin,mat[0]);
	for(int i=0;i<2*n+1;i++)
	{
		getline(cin,mat[i]);
		for(int j=0;j<2*m+1;j++)
			if((i==0||j==0||i==2*n||j==2*m)&&mat[i][j]==' ')
			{
				int sx=i/2+1,sy=j/2+1;
				if(i==0)
					sx=0;
				if(j==0)
					sy=0;
				if(sx1||sy1)
				{
					sx2=sx;
					sy2=sy;
				}
				else
				{
					sx1=sx;
					sy1=sy;
				}
			}
	}
	memset(d,0x3f,sizeof(d));
	d[sx1][sy1]=d[sx2][sy2]=0;
	dfs(sx1,sy1);
	dfs(sx2,sy2);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			ans=max(ans,d[i][j]);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const double INF=1e100;
int x[155],y[155];
double mat[155][155],d[155];
inline double dist(int a,int b)
{
	return sqrt((x[a]-x[b])*(x[a]-x[b])+(y[a]-y[b])*(y[a]-y[b]));
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>x[i]>>y[i];
	for(int i=1;i<=n;i++)
	{
		string s;
		cin>>s;
		for(int j=0;j<n;j++)
			if(s[j]=='1'||i==j+1)
				mat[i][j+1]=dist(i,j+1);
			else
				mat[i][j+1]=INF;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]=min(mat[i][j],mat[i][k]+mat[k][j]);
	for(int i=1;i<=n;i++)
	{
		d[i]=0;
		for(int j=1;j<=n;j++)
			if(mat[i][j]<INF)
				d[i]=max(d[i],mat[i][j]);
	}
	double ans=INF;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(i!=j&&mat[i][j]==INF)
				ans=min(ans,d[i]+d[j]+dist(i,j));
	for(int i=1;i<=n;i++)
		ans=max(ans,d[i]);
	cout.precision(6);
	cout<<fixed<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int dig[1000005],pred[1000005];
int main()
{
	int maxl=0,maxb;
	for(int b=2;b<1000000;b++)
	{
		int a=1;
		memset(pred,0,sizeof(pred));
		for(int i=1;;i++)
		{
			if(pred[a])
			{
				if(i-pred[a]>maxl)
				{
					maxl=i-pred[a];
					maxb=b;
					//cout<<"len="<<maxl<<" b="<<b<<endl;
				}
				break;
			}
			if(a==0)
				break;
			pred[a]=i;
			a*=10;
			dig[i]=a/b;
			a%=b;
		}
		if(b%10000==0)
		{
			system("cls");
			int p=b/10000,t=clock()/CLOCKS_PER_SEC;
			cout<<p<<"%\neta:"<<t/p*(100-p)<<"s\n";
		}
	}
	cout<<maxl<<' '<<maxb<<endl;
	return 0;
}
//999982 999983

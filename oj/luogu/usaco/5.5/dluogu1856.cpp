#include <iostream>
#include <algorithm>
using namespace std;
const int N = 5005;
int y[N * 2], val[N * 2];
struct rect
{
	int x1, y1, x2, y2;
} r[N];
struct event
{
	int x, yl, yr, val;
	event(int x = 0, int yl = 0, int yr = 0, int val = 0) : x(x), yl(yl), yr(yr), val(val) {}
	bool operator<(const event &rhs) const
	{
		return x == rhs.x ? val > rhs.val : x < rhs.x;
	}
} e[N * 2];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> r[i].x1 >> r[i].y1 >> r[i].x2 >> r[i].y2;
	int ans = 0;
	for (int t = 0; t < 2; t++)
	{
		for (int i = 1; i <= n; i++)
		{
			e[2 * i - 1] = event(r[i].x1, r[i].y1, r[i].y2, 1);
			e[2 * i] = event(r[i].x2, r[i].y1, r[i].y2, -1);
			y[2 * i - 1] = r[i].y1;
			y[2 * i] = r[i].y2;
		}
		sort(e + 1, e + n * 2 + 1);
		sort(y + 1, y + n * 2 + 1);
		for (int i = 1; i <= n * 2; i++)
		{
			int l = lower_bound(y + 1, y + n * 2 + 1, e[i].yl) - y, r = lower_bound(y + 1, y + n * 2 + 1, e[i].yr) - y;
			for (int j = l; j < r; j++)
			{
				if (val[j] == 0 || val[j] + e[i].val == 0)
					ans += y[j + 1] - y[j];
				val[j] += e[i].val;
			}
		}
		for (int i = 1; i <= n; i++)
		{
			swap(r[i].x1, r[i].y1);
			swap(r[i].x2, r[i].y2);
		}
	}
	cout << ans << endl;
	return 0;
}
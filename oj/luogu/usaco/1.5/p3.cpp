#include<iostream>
#include<cstring>
#include<ctime>
using namespace std;
const int n=1e8;
bool p[100000005];
int main()
{
	memset(p,true,sizeof(p));
	p[1]=false;
	for(int i=2;i*i<=n;i++)
		if(p[i])
			for(int j=i*i;j<=n;j+=i)
				p[j]=false;
	for(int i=2;i<=n;i++)
		if(p[i])
		{
			bool flag=true;
			int t=i/10;
			do
				if(!p[t])
				{
					flag=false;
					break;
				}
			while(t/=10);
			if(flag)
				cout<<i<<',';
		}
	return 0;
}
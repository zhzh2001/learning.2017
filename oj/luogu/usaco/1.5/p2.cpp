#include<iostream>
#include<cstdio>
using namespace std;
bool prime(int n)
{
	for(int i=2;i*i<=n;i++)
		if(n%i==0)
			return false;
	return true;
}
int getlen(int n)
{
	int ans=0;
	do
		ans++;
	while(n/=10);
	return ans;
}
int get10(int p)
{
	int ans=1;
	for(int i=1;i<=p;i++)
		ans*=10;
	return ans;
}
int main()
{
	int a,b;
	cin>>a>>b;
	int len=getlen(b);
	for(int i=1;i<=len;i++)
	{
		int nl=(i+1)/2;
		for(int j=get10(nl-1);j<get10(nl);j++)
		{
			char buf[15];
			sprintf(buf,"%d",j);
			for(int k=nl;k<i;k++)
				buf[k]=buf[i-k-1];
			long long num;
			sscanf(buf,"%lld",&num);
			if(num>=a&&num<=b&&prime(num))
				cout<<num<<endl;
		}
	}
	return 0;
}
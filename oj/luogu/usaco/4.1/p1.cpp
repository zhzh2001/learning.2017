#include<bits/stdc++.h>
using namespace std;
const int LIMIT=65536,MAXI=256;
int a[15];
bool f[LIMIT+1];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	f[0]=true;
	for(int i=1;i<=n;i++)
		for(int j=a[i];j<=LIMIT;j++)
			f[j]|=f[j-a[i]];
	for(int i=LIMIT;i;i--)
		if(!f[i])
		{
			if(i>LIMIT-2*MAXI)
				cout<<0<<endl;
			else
				cout<<i<<endl;
			return 0;
		}
	cout<<0<<endl;
	return 0;
}
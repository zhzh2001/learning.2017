#include<bits/stdc++.h>
using namespace std;
const int N=1005;
int s[N][N];
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int x,y;
		cin>>x>>y;
		s[x][y]=1;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			s[i][j]+=s[i-1][j]+s[i][j-1]-s[i-1][j-1];
	int l=0,r=n,ans=0;
	while(l<=r)
	{
		int mid=(l+r)/2;
		bool flag=false;
		for(int i=1;i+mid-1<=n;i++)
		{
			for(int j=1;j+mid-1<=n;j++)
				if(s[i+mid-1][j+mid-1]-s[i+mid-1][j-1]-s[i-1][j+mid-1]+s[i-1][j-1]==0)
				{
					flag=true;
					break;
				}
			if(flag)
				break;
		}
		if(flag)
		{
			l=mid+1;
			ans=mid;
		}
		else
			r=mid-1;
	}
	cout<<ans<<endl;
	return 0;
}
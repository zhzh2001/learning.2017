#include<bits/stdc++.h>
using namespace std;
const double eps=1e-6;
inline int fl(double x)
{
	int ans=floor(x);
	if(fabs(ans-x)>1-eps)
		ans++;
	return ans;
}
inline int lw(double x)
{
	int ans=fl(x);
	if(fabs(ans-x)<eps)
		ans--;
	return ans;
}
int main()
{
	int n,m,p;
	cin>>n>>m>>p;
	double k1=(double)m/n;
	int ans=0;
	if(p==n)
		for(int x=1;x<p;x++)
			ans+=lw(k1*x);
	else
	{
		double k2=(double)m/(n-p),b=-k2*p;
		if(p>n)
		{
			for(int x=1;x<=n;x++)
				ans+=lw(k1*x);
			for(int x=n+1;x<p;x++)
				ans+=lw(k2*x+b);
		}
		else
		{
			for(int x=1;x<=p;x++)
				ans+=lw(k1*x);
			for(int x=p+1;x<n;x++)
				ans+=lw(k1*x)-fl(k2*x+b);
		}
	}
	cout<<ans<<endl;
	return 0;
}
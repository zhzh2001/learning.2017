#include<bits/stdc++.h>
using namespace std;
void solve(string first,string mid)
{
	if(first=="")
		return;
	int p=mid.find(first[0]);
	solve(first.substr(1,p),mid.substr(0,p));
	solve(first.substr(p+1),mid.substr(p+1));
	putchar(first[0]);
}
int main()
{
	string mid,first;
	cin>>mid>>first;
	solve(first,mid);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int mat[105][105],d[105];
bool vis[105];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>mat[i][j];
	int ans=0;
	memset(vis,false,sizeof(vis));
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		ans+=d[j];
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],mat[j][k]);
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int cnt[13][1<<12];
struct node
{
	int len,key,cnt;
	bool operator<(const node b)const
	{
		if(cnt!=b.cnt)
			return cnt>b.cnt;
		if(len!=b.len)
			return len<b.len;
		return key<b.key;
	}
};
node seg[50000];
int main()
{
	int a,b,n;
	cin>>a>>b>>n;
	string s="",t;
	while(cin>>t)
		s+=t;
	int m=0;
	for(int i=a;i<=b&&i<=s.length();i++)
	{
		int key=0;
		for(int j=0;j<i;j++)
			key=(key<<1)+s[j]-'0';
		cnt[i][key]++;
		for(int j=i;j<s.length();j++)
		{
			key&=(1<<(i-1))-1;
			key=(key<<1)+s[j]-'0';
			cnt[i][key]++;
		}
		for(int j=0;j<(1<<i);j++)
			if(cnt[i][j])
			{
				seg[++m].len=i;
				seg[m].key=j;
				seg[m].cnt=cnt[i][j];
			}
	}
	sort(seg+1,seg+m+1);
	for(int i=1,j=1;i<=n&&j<=m;i++)
	{
		cout<<seg[j].cnt<<endl;
		int k,cc=0;
		for(k=j;seg[k].cnt==seg[j].cnt;k++)
		{
			int buf[15],tmp=seg[k].key;
			for(int t=1;t<=seg[k].len;t++)
			{
				buf[t]=tmp&1;
				tmp>>=1;
			}
			for(int t=seg[k].len;t>=1;t--)
				cout<<buf[t];
			cc++;
			if(cc%6==0)
				cout<<endl;
			else
				cout<<' ';
		}
		if(cc%6)
			cout<<endl;
		j=k;
	}
	return 0;
}
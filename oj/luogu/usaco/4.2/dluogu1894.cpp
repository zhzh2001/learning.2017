#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 205;
vector<int> mat[N];
bool vis[N];
int match[N];
bool dfs(int k)
{
	for (auto v : mat[k])
		if (!vis[v])
		{
			vis[v] = true;
			if (!match[v] || dfs(match[v]))
			{
				match[v] = k;
				return true;
			}
		}
	return false;
}
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		int s;
		cin >> s;
		while (s--)
		{
			int u;
			cin >> u;
			mat[i].push_back(u);
		}
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		fill(vis + 1, vis + n + 1, false);
		ans += dfs(i);
	}
	cout << ans << endl;
	return 0;
}
#include <iostream>
using namespace std;
const int N = 100;
bool vis1[3][N], vis2[3][N];
int c1[3], c2[3];
int main()
{
	int n;
	cin >> n;
	for (int i = 0; i < 3; i++)
	{
		cin >> c1[i];
		c1[i]--;
	}
	for (int i = 0; i < 3; i++)
	{
		cin >> c2[i];
		c2[i]--;
	}
	for (int i = 0; i < 3; i++)
		for (int j = -2; j <= 2; j++)
			vis1[i][(c1[i] + j + n) % n] = true;
	for (int i = 0; i < 3; i++)
		for (int j = -2; j <= 2; j++)
			vis2[i][(c2[i] + j + n) % n] = true;
	int ans = 0;
	for (int i = 0; i < n; i++)
		if (vis1[0][i] || vis2[0][i])
			for (int j = 0; j < n; j++)
				if (vis1[1][j] || vis2[1][j])
					for (int k = 0; k < n; k++)
						ans += (vis1[0][i] && vis1[1][j] && vis1[2][k]) || (vis2[0][i] && vis2[1][j] && vis2[2][k]);
	cout << ans << endl;
	return 0;
}
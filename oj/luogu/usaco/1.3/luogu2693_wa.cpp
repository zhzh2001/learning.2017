#include <iostream>
#include <algorithm>
using namespace std;
int main()
{
	int n, fj1, fj2, fj3, m1, m2, m3;
	cin >> n >> fj1 >> fj2 >> fj3 >> m1 >> m2 >> m3;
	fj1--;
	fj2--;
	fj3--;
	m1--;
	m2--;
	m3--;
	int ans = 0;
	for (int i = 0; i < n; i++)
		if (abs(i - fj1) <= 2 || (i + fj1) % n <= 2 || abs(i - m1) <= 2 || (i + m1) % n <= 2)
			for (int j = 0; j < n; j++)
				if (abs(j - fj2) <= 2 || (j + fj2) % n <= 2 || abs(j - m2) <= 2 || (j + m2) % n <= 2)
					for (int k = 0; k < n; k++)
						ans += ((abs(i - fj1) <= 2 || (i + fj1) % n <= 2) && (abs(j - fj2) <= 2 || (j + fj2) % n <= 2) && (abs(k - fj3) <= 2 || (k + fj3) % n <= 2)) || ((abs(i - m1) <= 2 || (i + m1) % n <= 2) && (abs(j - m2) <= 2 || (j + m2) % n <= 2) && (abs(k - m3) <= 2 || (k + m3) % n <= 2));
	cout << ans << endl;
	return 0;
}
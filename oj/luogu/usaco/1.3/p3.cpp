#include<iostream>
#include<cstring>
#define forn(x) for(int x=1;x<=n;x++)
using namespace std;
int a[10];
bool vis[10];
inline bool chk(int x)
{
	do
		if(!vis[x%10])
			return false;
	while(x/=10);
	return true;
}
int main()
{
	int n;
	cin>>n;
	memset(vis,false,sizeof(vis));
	forn(i)
	{
		cin>>a[i];
		vis[a[i]]=true;
	}
	int ans=0;
	forn(i)forn(j)forn(k)forn(l)forn(m)
	{
		int num1=a[i]*100+a[j]*10+a[k],num2=a[l]*10+a[m],num3=num1*a[m],num4=num1*a[l],num5=num1*num2;
		if(num3<1000&&num4<1000&&num5>=1000&&chk(num3)&&chk(num4)&&chk(num5))
			ans++;
	}
	cout<<ans<<endl;
	return 0;
}
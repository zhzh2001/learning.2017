#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1},wall[]={2,8,1,4};
int n,m,mat[55][55],cnt;
bool vis[55][55];
struct node
{
	int num,maxcnt;
};
void dfs(int x,int y)
{
	vis[x][y]=true;
	cnt++;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis[nx][ny]&&!(mat[x][y]&wall[i]))
			dfs(nx,ny);
	}
}
node flood()
{
	int num=0;
	memset(vis,false,sizeof(vis));
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(!vis[i][j])
			{
				num++;
				cnt=0;
				dfs(i,j);
				ans=max(ans,cnt);
			}
	return (node){num,ans};
}
int main()
{
	cin>>m>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>mat[i][j];
	node res=flood();
	cout<<res.num<<endl<<res.maxcnt<<endl;
	int ans=0,x,y;
	bool east;
	for(int j=1;j<=m;j++)
		for(int i=n;i>=1;i--)
		{
			if(mat[i][j]&wall[0])
			{
				mat[i][j]&=((1<<4)-1-wall[0]);
				mat[i-1][j]&=((1<<4)-1-wall[1]);
				node now=flood();
				if(now.num+1==res.num&&now.maxcnt>ans)
				{
					ans=now.maxcnt;
					x=i;y=j;
					east=false;
				}
				mat[i][j]|=wall[0];
				mat[i-1][j]|=wall[1];
			}
			if(mat[i][j]&wall[3])
			{
				mat[i][j]&=((1<<4)-1-wall[3]);
				mat[i][j+1]&=((1<<4)-1-wall[2]);
				node now=flood();
				if(now.num+1==res.num&&now.maxcnt>ans)
				{
					ans=now.maxcnt;
					x=i;y=j;
					east=true;
				}
				mat[i][j]|=wall[3];
				mat[i][j+1]|=wall[2];
			}
		}
	cout<<ans<<endl<<x<<' '<<y<<' ';
	if(east)
		cout<<"E\n";
	else
		cout<<"N\n";
	return 0;
}
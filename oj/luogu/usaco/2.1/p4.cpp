#include<iostream>
#include<cstring>
#include<cstdlib>
using namespace std;
int n,m,r[30],now[30],a[20][30],ans;
bool vis[20];
void dfs(int k,int sel)
{
	if(sel==0)
	{
		memset(now,0,sizeof(now));
		for(int i=1;i<=n;i++)
			if(vis[i])
				for(int j=1;j<=m;j++)
					now[j]+=a[i][j];
		for(int i=1;i<=m;i++)
			if(now[i]<r[i])
				return;
		cout<<ans;
		for(int i=1;i<=n;i++)
			if(vis[i])
				cout<<' '<<i;
		cout<<endl;
		exit(0);
	}
	for(int i=k;i<=n;i++)
	{
		vis[i]=true;
		dfs(i+1,sel-1);
		vis[i]=false;
	}
}
int main()
{
	cin>>m;
	for(int i=1;i<=m;i++)
		cin>>r[i];
	cin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>a[i][j];
	for(int i=1;i<=n;i++)
	{
		memset(vis,false,sizeof(vis));
		ans=i;
		dfs(1,i);
	}
	return 0;
}
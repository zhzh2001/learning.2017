#include<iostream>
#include<algorithm>
using namespace std;
int gcd(int a,int b)
{
	return a%b==0?b:gcd(b,a%b);
}
struct frac
{
	int a,b;
};
frac f[10000];
bool cmp(frac x,frac y)
{
	return (double)x.a/x.b<(double)y.a/y.b;
}
int main()
{
	int n;
	cin>>n;
	int cnt=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=i;j++)
			if(gcd(j,i)==1)
				f[++cnt]=(frac){j,i};
	sort(f+1,f+cnt+1,cmp);
	for(int i=1;i<=cnt;i++)
		cout<<f[i].a<<'/'<<f[i].b<<endl;
	return 0;
}
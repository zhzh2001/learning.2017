#include<iostream>
#include<string>
using namespace std;
string a[100];
string dec2bin(int x,int b)
{
	string ans="";
	for(int i=1;i<=b;i++,x/=2)
		ans=(char)(x%2+'0')+ans;
	return ans;
}
int dist(const string& a,const string& b)
{
	int ans=0;
	for(int i=0;i<a.length();i++)
		ans+=a[i]!=b[i];
	return ans;
}
int main()
{
	int n,b,d;
	cin>>n>>b>>d;
	int p=1;
	a[1]=dec2bin(0,b);
	cout<<0;
	for(int i=1;p<n;i++)
	{
		string s=dec2bin(i,b);
		bool flag=true;
		for(int j=1;j<=p;j++)
			if(dist(s,a[j])<d)
			{
				flag=false;
				break;
			}
		if(flag)
		{
			a[++p]=s;
			if(p%10!=1)
				cout<<' ';
			cout<<i;
			if(p%10==0)
				cout<<endl;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int MOD=10000;
int main()
{
	int n;
	cin>>n;
	int ans=1;
	for(int i=1;i<=n;i++)
	{
		ans*=i;
		while(ans%10==0)
			ans/=10;
		if(ans>MOD)
			ans%=MOD;
	}
	cout<<ans%10<<endl;
	return 0;
}
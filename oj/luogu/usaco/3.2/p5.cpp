#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int a[505];
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
};
vector<node> mat[805];
int d[805];
bool inq[805];
queue<int> q;
void spfa(int s)
{
	memset(inq,false,sizeof(inq));
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	q.push(s);
	inq[s]=true;
	while(!q.empty())
	{
		int k=q.front();q.pop();
		inq[k]=false;
		for(int i=0;i<mat[k].size();i++)
			if(d[k]+mat[k][i].w<d[mat[k][i].v])
			{
				d[mat[k][i].v]=d[k]+mat[k][i].w;
				if(!inq[mat[k][i].v])
				{
					inq[mat[k][i].v]=true;
					q.push(mat[k][i].v);
				}
			}
	}
}
int main()
{
	int n,m,q;
	cin>>q>>n>>m;
	for(int i=1;i<=q;i++)
		cin>>a[i];
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(node(v,w));
		mat[v].push_back(node(u,w));
	}
	int ans=INF;
	for(int s=1;s<=n;s++)
	{
		spfa(s);
		int now=0;
		for(int i=1;i<=q;i++)
			now+=d[a[i]];
		ans=min(ans,now);
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005;
struct job
{
	int t,s;
	bool operator<(const job& rhs)const
	{
		return s<rhs.s;
	}
}a[N];
int n;
bool check(int val)
{
	for(int i=1;i<=n;i++)
	{
		if(val+a[i].t>a[i].s)
			return false;
		val+=a[i].t;
	}
	return true;
}
int main()
{
	cin>>n;
	int sum=0,last=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].t>>a[i].s;
		sum+=a[i].t;
		last=max(last,a[i].s);
	}
	sort(a+1,a+n+1);
	int l=-1,r=last-sum,ans=-1;
	while(l<=r)
	{
		int mid=(l+r)/2;
		if(check(mid))
		{
			l=mid+1;
			ans=mid;
		}
		else
			r=mid-1;
	}
	cout<<ans<<endl;
	return 0;
}
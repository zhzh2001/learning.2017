#include<bits/stdc++.h>
using namespace std;
const int N=16;
int a[N];
long long f[1<<N][N];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=0;i<n;i++)
		cin>>a[i];
	for(int i=1;i<1<<n;i++)
		if((i&(i-1))==0)
			f[i][(int)log2(i)]=1;
		else
			for(int j=0;j<n;j++)
				if(i&(1<<j))
					for(int pred=0;pred<n;pred++)
						if(i&(1<<pred)&&abs(a[j]-a[pred])>k)
							f[i][j]+=f[i^(1<<j)][pred];
	long long ans=0;
	for(int i=0;i<n;i++)
		ans+=f[(1<<n)-1][i];
	cout<<ans<<endl;
	return 0;
}
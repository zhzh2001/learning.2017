#include<bits/stdc++.h>
using namespace std;
const int N=100000,SQRTN=330;
bool a[N],rev[SQRTN];
int b[SQRTN];
int main()
{
	int n,m;
	cin>>n>>m;
	int block=floor(sqrt(n));
	while(m--)
	{
		int opt,l,r;
		cin>>opt>>l>>r;
		l--;r--;
		if(opt)
		{
			int ans=0,i,now=l/block;
			for(i=l;i%block&&i<=r;i++)
				ans+=a[i]^rev[now];
			if(i<=r)
			{
				now=r/block;
				for(int j=i/block;j<now;j++)
					ans+=b[j];
				for(i=r/block*block;i<=r;i++)
					ans+=a[i]^rev[now];
			}
			cout<<ans<<endl;
		}
		else
		{
			int i,now=l/block;
			for(i=l;i%block&&i<=r;i++)
			{
				b[now]-=a[i]^rev[now];
				b[now]+=(a[i]=!a[i])^rev[now];
			}
			if(i<=r)
			{
				now=r/block;
				for(int j=i/block;j<now;j++)
				{
					b[j]=block-b[j];
					rev[j]=!rev[j];
				}
				for(i=r/block*block;i<=r;i++)
				{
					b[now]-=a[i]^rev[now];
					b[now]+=(a[i]=!a[i])^rev[now];
				}
			}
		}
	}
	return 0;
}
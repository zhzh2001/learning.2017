#include <iostream>
#include <algorithm>
using namespace std;
const int N = 10005, M = 100005;
int a[N], f[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
struct edge
{
	int u, v, w;
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} e[M];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= m; i++)
	{
		cin >> e[i].u >> e[i].v >> e[i].w;
		e[i].w = e[i].w * 2 + a[e[i].u] + a[e[i].v];
	}
	sort(e + 1, e + m + 1);
	for (int i = 1; i <= n; i++)
		f[i] = i;
	int ans = 0, now = 0;
	for (int i = 1; i <= m; i++)
	{
		int ru = getf(e[i].u), rv = getf(e[i].v);
		if (ru != rv)
		{
			f[ru] = rv;
			ans += e[i].w;
			if (++now == n - 1)
				break;
		}
	}
	cout << *min_element(a + 1, a + n + 1) + ans << endl;
	return 0;
}
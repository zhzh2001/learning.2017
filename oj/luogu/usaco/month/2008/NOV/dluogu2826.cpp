#include<bits/stdc++.h>
using namespace std;
const int N=100005;
bool a[N];
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int opt,l,r;
		cin>>opt>>l>>r;
		if(opt)
		{
			int ans=0;
			for(int i=l;i<=r;i++)
				ans+=a[i];
			cout<<ans<<endl;
		}
		else
			for(int i=l;i<=r;i++)
				a[i]=!a[i];
	}
	return 0;
}
#include <iostream>
using namespace std;
const int N = 105, T = 20;
bool mat[N][N];
int f[T][N][N];
int main()
{
	int n, m, t;
	cin >> n >> m >> t;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			char c;
			cin >> c;
			mat[i][j] = c == '.';
		}
	int sx, sy, tx, ty;
	cin >> sx >> sy >> tx >> ty;
	f[0][sx][sy] = 1;
	for (int tt = 1; tt <= t; tt++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
				if (mat[i][j])
					for (auto d : {make_pair(-1, 0), make_pair(1, 0), make_pair(0, -1), make_pair(0, 1)})
					{
						int x = i + d.first, y = j + d.second;
						if (x && x <= n && y && y <= m)
							f[tt][i][j] += f[tt - 1][x][y];
					}
	cout << f[t][tx][ty] << endl;
	return 0;
}
#include <iostream>
#include <queue>
using namespace std;
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> m >> n;
	typedef pair<int, int> state;
	priority_queue<state> Q;
	for (int i = 1; i <= n; i++)
	{
		int deg;
		cin >> deg;
		Q.push(make_pair(deg, i));
	}
	for (; m; m -= 2)
	{
		state k1 = Q.top();
		Q.pop();
		state k2 = Q.top();
		Q.pop();
		cout << k1.second << ' ' << k2.second << endl;
		k1.first--;
		k2.first--;
		if (k1.first)
			Q.push(k1);
		if (k2.first)
			Q.push(k2);
	}
	return 0;
}
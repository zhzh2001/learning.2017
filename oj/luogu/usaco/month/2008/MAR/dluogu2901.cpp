#include<bits/stdc++.h>
using namespace std;
const int N=1005,M=10005,K=105;
int head[N],v[M],w[M],nxt[M],e,cnt,d[K],k;
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k,int dist,int maxd)
{
	if(dist>maxd)
		return;
	if(k==1)
	{
		d[++cnt]=dist;
		return;
	}
	for(int i=head[k];i;i=nxt[i])
	{
		dfs(v[i],dist+w[i],maxd);
		if(cnt==::k)
			return;
	}
}
int main()
{
	int n,m;
	cin>>n>>m>>k;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		add_edge(u,v,w);
	}
	int l=0,r=1e9;
	while(l<r)
	{
		int mid=(l+r)/2;
		cnt=0;
		dfs(n,0,mid);
		if(cnt==k)
			r=mid;
		else
			l=mid+1;
	}
	cnt=0;
	dfs(n,0,r);
	sort(d+1,d+cnt+1);
	for(int i=1;i<=cnt;i++)
		cout<<d[i]<<endl;
	for(int i=cnt+1;i<=k;i++)
		cout<<-1<<endl;
	return 0;
}
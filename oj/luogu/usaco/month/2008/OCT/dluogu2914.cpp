#include<bits/stdc++.h>
using namespace std;
const int N=1005;
const double eps=1e-8;
bool mat[N][N],vis[N];
int x[N],y[N];
double d[N];
int main()
{
	int n,m;
	double maxd;
	cin>>n>>m>>maxd;
	for(int i=1;i<=n;i++)
		cin>>x[i]>>y[i];
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=mat[v][u]=true;
	}
	for(int i=0;i<=n;i++)
		d[i]=i==1?0:HUGE_VAL;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
			{
				double now=mat[j][k]?0:hypot(x[j]-x[k],y[j]-y[k]);
				if(now<maxd+eps)
					d[k]=min(d[k],d[j]+now);
			}
	}
	if(d[n]==HUGE_VAL)
		cout<<-1<<endl;
	else
	{
		cout.precision(0);
		cout<<fixed<<floor(d[n]*1000)<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005,LOGN=12,M=2005;
int head[N],v[M],w[M],nxt[M],e,d[N],dep[N],f[N][LOGN];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	for(int i=head[k];i;i=nxt[i])
		if(!dep[v[i]])
		{
			d[v[i]]=d[k]+w[i];
			dep[v[i]]=dep[k]+1;
			f[v[i]][0]=k;
			dfs(v[i]);
		}
}
int lca(int x,int y)
{
	if(dep[x]<dep[y])
		swap(x,y);
	for(int i=0,delta=dep[x]-dep[y];delta;delta/=2,i++)
		if(delta&1)
			x=f[x][i];
	if(x==y)
		return x;
	for(int i=LOGN-1;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
int main()
{
	int n,q;
	cin>>n>>q;
	for(int i=1;i<n;i++)
	{
		int u,v,w;
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	dep[1]=1;
	dfs(1);
	f[1][0]=1;
	for(int i=1;i<LOGN;i++)
		for(int j=1;j<=n;j++)
			f[j][i]=f[f[j][i-1]][i-1];
	while(q--)
	{
		int u,v;
		cin>>u>>v;
		int a=lca(u,v);
		cout<<d[u]+d[v]-2*d[a]<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=305;
int mat[N][N],d[N];
bool vis[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>mat[0][i];
		mat[i][0]=mat[0][i];
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>mat[i][j];
	memset(d,0x3f,sizeof(d));
	d[0]=0;
	int ans=0;
	for(int i=0;i<=n;i++)
	{
		int j=-1;
		for(int k=0;k<=n;k++)
			if(!vis[k]&&(!~j||d[k]<d[j]))
				j=k;
		ans+=d[j];
		vis[j]=true;
		for(int k=0;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],mat[j][k]);
	}
	cout<<ans<<endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <set>
using namespace std;
const int N = 100005;
struct point
{
	int x, y, id;
	bool operator<(const point &rhs) const
	{
		return x < rhs.x;
	}
} p[N];
int f[N], sz[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n, c;
	cin >> n >> c;
	for (int i = 1; i <= n; i++)
	{
		int x, y;
		cin >> x >> y;
		p[i].x = x + y;
		p[i].y = x - y;
		p[i].id = i;
		f[i] = i;
	}
	sort(p + 1, p + n + 1);
	typedef pair<int, int> state;
	set<state> S;
	S.insert(make_pair(p[1].y, p[1].id));
	for (int i = 2, j = 1; i <= n; i++)
	{
		for (; j < i && p[i].x - p[j].x > c; j++)
			S.erase(make_pair(p[j].y, p[j].id));
		auto it1 = S.insert(make_pair(p[i].y, p[i].id)).first, it2 = it1;
		if (it1-- != S.begin() && p[i].y - it1->first <= c)
			f[getf(p[i].id)] = getf(it1->second);
		if (++it2 != S.end() && it2->first - p[i].y <= c)
			f[getf(p[i].id)] = getf(it2->second);
	}
	for (int i = 1; i <= n; i++)
		sz[getf(i)]++;
	cout << count_if(sz + 1, sz + n + 1, [](int x) { return x > 0; }) << ' ' << *max_element(sz + 1, sz + n + 1) << endl;
	return 0;
}
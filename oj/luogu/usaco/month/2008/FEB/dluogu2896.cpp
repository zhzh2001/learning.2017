#include<bits/stdc++.h>
using namespace std;
const int N=30005;
int a[N],f[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int ans=n;
	for(int t=0;t<2;t++)
	{
		int sz=0;
		for(int i=1;i<=n;i++)
		{
			int *p=upper_bound(f+1,f+sz+1,a[i]);
			if(p==f+sz+1)
				sz++;
			*p=a[i];
		}
		ans=min(ans,n-sz);
		for(int i=1;i<=n;i++)
			a[i]=-a[i];
	}
	cout<<ans<<endl;
	return 0;
}
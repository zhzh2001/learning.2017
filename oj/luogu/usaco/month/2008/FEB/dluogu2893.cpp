#include<bits/stdc++.h>
using namespace std;
const int N=2005;
int a[N],b[N],f[N][N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	copy(a+1,a+n+1,b+1);
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
		f[1][i]=a[1]<=b[i]?0:a[1]-b[i];
	for(int i=2;i<=n;i++)
	{
		f[i][1]=f[i-1][1]+abs(a[i]-b[1]);
		for(int j=2;j<=n;j++)
			f[i][j]=min(f[i][j-1],f[i-1][j]+abs(a[i]-b[j]));
	}
	cout<<*min_element(f[n]+1,f[n]+n+1)<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005;
enum
{
	EMPTY,
	MIXED,
	FULL,
};
struct node
{
	int lmax,rmax,max,state;
	bool lazy;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy&&l<r)
	{
		int mid=(l+r)/2;
		if(tree[id].state==EMPTY)
		{
			tree[id*2].state=EMPTY;
			tree[id*2].lmax=tree[id*2].rmax=tree[id*2].max=mid-l+1;
			tree[id*2+1].state=EMPTY;
			tree[id*2+1].lmax=tree[id*2+1].rmax=tree[id*2+1].max=r-mid;
			tree[id*2].lazy=tree[id*2+1].lazy=true;
		}
		if(tree[id].state==FULL)
		{
			tree[id*2].state=FULL;
			tree[id*2].lmax=tree[id*2].rmax=tree[id*2].max=0;
			tree[id*2+1].state=FULL;
			tree[id*2+1].lmax=tree[id*2+1].rmax=tree[id*2+1].max=0;
			tree[id*2].lazy=tree[id*2+1].lazy=true;
		}
		tree[id].lazy=false;
	}
}
inline void pullup(int id,int l,int r)
{
	if(tree[id*2].state==EMPTY&&tree[id*2+1].state==EMPTY)
	{
		tree[id].state=EMPTY;
		tree[id].lmax=tree[id].rmax=tree[id].max=r-l+1;
	}
	else
		if(tree[id*2].state==FULL&&tree[id*2+1].state==FULL)
		{
			tree[id].state=FULL;
			tree[id].lmax=tree[id].rmax=tree[id].max=0;
		}
		else
		{
			tree[id].state=MIXED;
			tree[id].lmax=tree[id*2].lmax;
			int mid=(l+r)/2;
			if(tree[id*2].state==EMPTY)
				tree[id].lmax=mid-l+1+tree[id*2+1].lmax;
			tree[id].rmax=tree[id*2+1].rmax;
			if(tree[id*2+1].state==EMPTY)
				tree[id].rmax=r-mid+tree[id*2].rmax;
			tree[id].max=max(tree[id*2].rmax+tree[id*2+1].lmax,max(tree[id*2].max,tree[id*2+1].max));
		}
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		tree[id].lmax=tree[id].rmax=tree[id].max=1;
		tree[id].state=EMPTY;
		tree[id].lazy=false;
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id,l,r);
	}
}
int L,R;
void fill(int id,int l,int r)
{
	if(L<=l&&R>=r)
	{
		tree[id].state=FULL;
		tree[id].lmax=tree[id].rmax=tree[id].max=0;
		tree[id].lazy=true;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			fill(id*2,l,mid);
		if(R>mid)
			fill(id*2+1,mid+1,r);
		pullup(id,l,r);
	}
}
void clear(int id,int l,int r)
{
	if(L<=l&&R>=r)
	{
		tree[id].state=EMPTY;
		tree[id].lmax=tree[id].rmax=tree[id].max=r-l+1;
		tree[id].lazy=true;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			clear(id*2,l,mid);
		if(R>mid)
			clear(id*2+1,mid+1,r);
		pullup(id,l,r);
	}
}
int len;
int query(int id,int l,int r)
{
	if(tree[id].state==EMPTY&&r-l+1>=len)
		return l;
	pushdown(id,l,r);
	int mid=(l+r)/2;
	if(tree[id*2].max>=len)
		return query(id*2,l,mid);
	if(tree[id*2].rmax+tree[id*2+1].lmax>=len)
		return mid-tree[id*2].rmax+1;
	if(tree[id*2+1].max>=len)
		return query(id*2+1,mid+1,r);
	return 0;
}
int main()
{
	int n,m;
	cin>>n>>m;
	build(1,1,n);
	while(m--)
	{
		int opt;
		cin>>opt;
		if(opt==1)
		{
			cin>>len;
			int ret=query(1,1,n);
			cout<<ret<<endl;
			if(ret)
			{
				L=ret;
				R=ret+len-1;
				fill(1,1,n);
			}
		}
		else
		{
			cin>>L>>len;
			R=L+len-1;
			clear(1,1,n);
		}
	}
	return 0;
}
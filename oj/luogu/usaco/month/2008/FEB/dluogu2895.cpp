#include<bits/stdc++.h>
using namespace std;
const int N=305,M=50005,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
struct node
{
	int x,y,t;
	node(int x=0,int y=0,int t=0):x(x),y(y),t(t){}
	bool operator<(const node& rhs)const
	{
		return t<rhs.t;
	}
}a[M];
bool mark[N][N],vis[N][N];
int main()
{
	int m;
	cin>>m;
	for(int i=1;i<=m;i++)
	{
		cin>>a[i].x>>a[i].y>>a[i].t;
		mark[a[i].x][a[i].y]=true;
		for(int j=0;j<4;j++)
		{
			int nx=a[i].x+dx[j],ny=a[i].y+dy[j];
			if(nx>=0&&nx<N&&ny>=0&&ny<N)
				mark[nx][ny]=true;
		}
	}
	sort(a+1,a+m+1);
	queue<node> Q;
	Q.push(node(0,0,0));
	vis[0][0]=true;
	int j=1;
	while(!Q.empty())
	{
		node k=Q.front();Q.pop();
		if(!mark[k.x][k.y])
		{
			cout<<k.t<<endl;
			return 0;
		}
		for(;j<=m&&a[j].t==k.t+1;j++)
		{
			vis[a[j].x][a[j].y]=true;
			for(int k=0;k<4;k++)
			{
				int nx=a[j].x+dx[k],ny=a[j].y+dy[k];
				if(nx>=0&&nx<N&&ny>=0&&ny<N)
					vis[nx][ny]=true;
			}
		}
		for(int i=0;i<4;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			if(nx>=0&&nx<N&&ny>=0&&ny<N&&!vis[nx][ny])
			{
				vis[nx][ny]=true;
				Q.push(node(nx,ny,k.t+1));
			}
		}
	}
	cout<<-1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=105;
bool mat[N][N];
int outo[N],into[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=true;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]|=mat[i][k]&&mat[k][j];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(mat[i][j])
			{
				outo[i]++;
				into[j]++;
			}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=into[i]+outo[i]>n;
	cout<<ans<<endl;
	return 0;
}
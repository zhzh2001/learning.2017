#include<bits/stdc++.h>
using namespace std;
const int N=1005,INF=0x3f3f3f3f;
int n,m,k,mat[N][N],d[N];
bool vis[N];
int dijkstra(int val)
{
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		if(!j)
			break;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&mat[j][k]<INF)
				d[k]=min(d[k],d[j]+(mat[j][k]>val));
	}
	return d[n];
}
int main()
{
	cin>>n>>m>>k;
	memset(mat,0x3f,sizeof(mat));
	int r=0;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		r=max(r,w);
		mat[u][v]=mat[v][u]=w;
	}
	if(dijkstra(r)==INF)
	{
		cout<<-1<<endl;
		return 0;
	}
	int l=0;
	while(l<r)
	{
		int mid=(l+r)/2;
		if(dijkstra(mid)<=k)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
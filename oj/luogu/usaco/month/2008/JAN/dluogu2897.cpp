#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, INF = 0x3f3f3f3f;
int w[N], h[N], l[N], r[N];
long long ans[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> w[i] >> h[i];
		l[i] = i - 1;
		r[i] = i + 1;
	}
	h[0] = h[n + 1] = INF;
	long long now = 0;
	for (int i = min_element(h + 1, h + n + 1) - h, nxt; h[i] < INF; i = nxt)
	{
		ans[i] = now + w[i];
		int adj = h[l[i]] < h[r[i]] ? l[i] : r[i];
		nxt = adj;
		now += 1ll * w[i] * (h[adj] - h[i]);
		if (h[l[i]] < h[r[i]])
			for (int j = l[nxt]; h[j] < h[nxt]; nxt = j, j = l[j])
				;
		else
			for (int j = r[nxt]; h[j] < h[nxt]; nxt = j, j = r[j])
				;
		w[adj] += w[i];
		r[l[i]] = r[i];
		l[r[i]] = l[i];
	}
	for (int i = 1; i <= n; i++)
		cout << ans[i] << endl;
	return 0;
}
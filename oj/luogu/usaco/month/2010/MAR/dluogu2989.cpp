#include <iostream>
#include <algorithm>
using namespace std;
const int N = 10005;
struct compont
{
	int f, m, id;
	bool operator<(const compont &rhs) const
	{
		return f * rhs.m < m * rhs.f;
	}
} a[N];
bool used[N];
int main()
{
	ios::sync_with_stdio(false);
	int f, m, n;
	cin >> f >> m >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i].f >> a[i].m;
		a[i].id = i;
	}
	sort(a + 1, a + n + 1);
	int i;
	for (i = n; i; i--)
		if (1ll * a[i].m * f < 1ll * m * a[i].f)
		{
			f += a[i].f;
			m += a[i].m;
			used[a[i].id] = true;
		}
	if (!count(used + 1, used + n + 1, true))
		cout << "NONE\n";
	else
		for (int i = 1; i <= n; i++)
			if (used[i])
				cout << i << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005,INF=0x3f3f3f3f;
struct request
{
	int l,r;
	bool operator<(const request& rhs)const
	{
		return r<rhs.r;
	}
}a[N];
int cap[N];
struct node
{
	int min,lazy;
}tree[N*4];
void pushdown(int id,int l,int r)
{
	if(tree[id].lazy&&l<r)
	{
		tree[id*2].lazy+=tree[id].lazy;
		tree[id*2].min+=tree[id].lazy;
		tree[id*2+1].lazy+=tree[id].lazy;
		tree[id*2+1].min+=tree[id].lazy;
		tree[id].lazy=0;
	}
}
void build(int id,int l,int r)
{
	if(l==r)
		tree[id].min=cap[l];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id].min=min(tree[id*2].min,tree[id*2+1].min);
	}
}
int L,R,val;
void modify(int id,int l,int r)
{
	if(L<=l&&R>=r)
	{
		tree[id].min+=val;
		tree[id].lazy+=val;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			modify(id*2,l,mid);
		if(R>mid)
			modify(id*2+1,mid+1,r);
		tree[id].min=min(tree[id*2].min,tree[id*2+1].min);
	}
}
void query(int id,int l,int r)
{
	if(L<=l&&R>=r)
		val=min(val,tree[id].min);
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			query(id*2,l,mid);
		if(R>mid)
			query(id*2+1,mid+1,r);
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>cap[i];
	build(1,1,n);
	for(int i=1;i<=m;i++)
		cin>>a[i].l>>a[i].r;
	sort(a+1,a+m+1);
	int ans=0;
	for(int i=1;i<=m;i++)
	{
		L=a[i].l;R=a[i].r;
		val=INF;
		query(1,1,n);
		if(val)
		{
			ans++;
			val=-1;
			modify(1,1,n);
		}
	}
	cout<<ans<<endl;
	return 0;
}
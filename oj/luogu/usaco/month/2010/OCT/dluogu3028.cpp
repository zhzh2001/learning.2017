#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int l[N],r[N],x[N*2],delta[N*2];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>l[i]>>r[i];
		x[2*i-1]=l[i];
		x[2*i]=r[i]+1;
	}
	sort(x+1,x+2*n+1);
	for(int i=1;i<=n;i++)
	{
		delta[lower_bound(x+1,x+2*n+1,l[i])-x]++;
		delta[lower_bound(x+1,x+2*n+1,r[i]+1)-x]--;
	}
	int sum=0,ans=0;
	for(int i=1;i<=2*n;i++)
		ans=max(ans,sum+=delta[i]);
	cout<<ans<<endl;
	return 0;
}
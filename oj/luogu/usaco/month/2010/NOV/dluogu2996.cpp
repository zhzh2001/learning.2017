#include<bits/stdc++.h>
using namespace std;
const int N=50005;
vector<int> mat[N];
int f[N][2];
void dfs(int k)
{
	f[k][1]=1;
	for(auto v:mat[k])
		if(!f[v][1])
		{
			dfs(v);
			f[k][0]+=max(f[v][0],f[v][1]);
			f[k][1]+=f[v][0];
		}
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	dfs(1);
	cout<<max(f[1][0],f[1][1])<<endl;
	return 0;
}
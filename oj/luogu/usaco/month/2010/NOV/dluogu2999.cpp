#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int nxt[N],into[N],outo[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		nxt[u]=v;
		into[v]++;
		outo[u]++;
	}
	int start=0,maxr=0;
	for(int i=1;i<=n;i++)
	{
		if(maxr==i&&(!start||into[i]>1))
			start=i;
		if(outo[i]>1)
			break;
		maxr=max(maxr,nxt[i]);
	}
	for(int i=start;;i=nxt[i])
	{
		cout<<i<<endl;
		if(outo[i]!=1)
			break;
	}
	return 0;
}
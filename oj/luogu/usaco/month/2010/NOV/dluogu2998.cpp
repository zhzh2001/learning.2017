#include<bits/stdc++.h>
using namespace std;
const int N=55,M=40005;
int a[N],b[N],f[M],cnt[M];
bool bl[M];
int main()
{
	int n,m,F,k;
	cin>>m>>n>>F>>k;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	for(int i=1;i<=F;i++)
	{
		int x;
		cin>>x;
		bl[x]=true;
	}
	bl[m]=false;
	memset(f,-1,sizeof(f));
	f[m]=0;
	for(int i=m;i;i--)
	{
		if(f[i]==-1)
			continue;
		if(cnt[i]>F+1)
		{
			cout<<-1<<endl;
			return 0;
		}
		if(bl[i])
		{
			cnt[i]++;
			if(f[i]>f[i+k])
			{
				f[i+k]=f[i];
				i+=k+1;
			}
		}
		else
			for(int j=1;j<=n;j++)
				if(i>=a[j])
					f[i-a[j]]=max(f[i-a[j]],f[i]+a[j]);
	}
	cout<<*max_element(f,f+m+1)<<endl;
	return 0;
}
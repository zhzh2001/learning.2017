#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,a[N],p[N],tmp[N];
long long cnt;
void merge_sort(int l,int r)
{
	if(l>=r)
		return;
	int mid=(l+r)/2;
	merge_sort(l,mid);
	merge_sort(mid+1,r);
	int i=l,j=mid+1,k=l;
	while(i<=mid&&j<=r)
		if(a[i]<a[j])
			tmp[k++]=a[i++];
		else
		{
			tmp[k++]=a[j++];
			cnt+=mid-i+1;
		}
	while(i<=mid)
		tmp[k++]=a[i++];
	while(j<=r)
		tmp[k++]=a[j++];
	copy(tmp+l,tmp+r+1,a+l);
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		p[a[i]]=i;
	}
	merge_sort(1,n);
	long long ans=cnt;
	for(int i=1;i<n;i++)
	{
		cnt+=n-p[i]-(p[i]-1);
		ans=min(ans,cnt);
	}
	cout<<ans<<endl;
	return 0;
}
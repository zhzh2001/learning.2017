#include<bits/stdc++.h>
using namespace std;
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	int n,m,l,r;
	cin>>n>>m>>l>>r;
	long long ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(gcd(i,j)==1&&i*i+j*j>=l*l&&i*i+j*j<=r*r)
				ans+=2*(n-i+1)*(m-j+1);
	if(l==1)
		ans+=(n+1)*m+n*(m+1);
	cout<<ans<<endl;
	return 0;
}
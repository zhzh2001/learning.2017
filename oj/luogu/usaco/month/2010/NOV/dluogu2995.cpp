#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,a[N],p[N];
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	cin>>n;
	long long cnt=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		T.modify(a[i],1);
		cnt+=i-T.query(a[i]);
		p[a[i]]=i;
	}
	long long ans=cnt;
	for(int i=1;i<n;i++)
	{
		cnt+=n-p[i]-(p[i]-1);
		ans=min(ans,cnt);
	}
	cout<<ans<<endl;
	return 0;
}
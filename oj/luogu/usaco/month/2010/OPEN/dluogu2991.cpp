#include <iostream>
#include <cstring>
#include <vector>
using namespace std;
const int N = 50005, K = 11;
long long f[N][K];
vector<pair<int, int>> mat[N];
int n, m, k;
long long dp(int u, int k)
{
	if (u == n)
		return 0;
	long long &now = f[u][k];
	if (~now)
		return now;
	for (auto e : mat[u])
		now = max(now, dp(e.first, k) + e.second);
	if (k)
		for (auto e : mat[u])
			now = min(now, dp(e.first, k - 1) + e.second);
	return now;
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m >> k;
	while (m--)
	{
		int u, v, w;
		cin >> u >> v >> w;
		mat[u].push_back(make_pair(v, w));
	}
	memset(f, -1, sizeof(f));
	cout << dp(1, k) << endl;
	return 0;
}
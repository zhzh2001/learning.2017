#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, LOGN = 18;
pair<int, int> a[N];
int nxt[N][LOGN];
int main()
{
	ios::sync_with_stdio(false);
	int c, n;
	cin >> c >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i].first >> a[i].second;
		a[i].second += a[i].first;
	}
	sort(a + 1, a + n + 1);
	int m = 0;
	a[m].second = -1;
	for (int i = 1; i <= n; i++)
		if (a[i].second > a[m].second)
			a[++m] = a[i];
	int j = 1;
	for (int i = 1; i <= m; i++)
	{
		for (; j <= m && a[j].first <= a[i].second; j++)
			;
		nxt[i][0] = j - 1;
	}
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i + (1 << j) - 1 <= m; i++)
			nxt[i][j] = nxt[nxt[i][j - 1]][j - 1];
	int ans = m;
	a[0].second = 2e9;
	for (int i = 1; i <= m; i++)
	{
		int now = 0, k = i;
		for (int j = LOGN - 1; j >= 0; j--)
			if (a[nxt[k][j]].second - a[i].first < c)
			{
				now += 1 << j;
				k = nxt[k][j];
			}
		if (a[nxt[k][0]].second - a[i].first >= c)
			ans = min(ans, now + 2);
	}
	cout << ans << endl;
	return 0;
}
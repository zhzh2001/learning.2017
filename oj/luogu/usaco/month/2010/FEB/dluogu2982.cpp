#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,head[N],v[2*N],nxt[2*N],e,in_t[N],out_t[N],dfs_clock;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	in_t[k]=++dfs_clock;
	for(int i=head[k];i;i=nxt[i])
		if(!in_t[v[i]])
			dfs(v[i]);
	out_t[k]=++dfs_clock;
}
struct BIT
{
	int tree[2*N];
	void modify(int x,int val)
	{
		for(;x<=2*n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	cin>>n;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1);
	for(int i=1;i<=n;i++)
	{
		int p;
		cin>>p;
		cout<<T.query(in_t[p])<<endl;
		T.modify(in_t[p],1);T.modify(out_t[p]+1,-1);
	}
	return 0;
}
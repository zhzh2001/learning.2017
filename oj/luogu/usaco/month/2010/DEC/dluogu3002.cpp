#include <iostream>
#include <string>
using namespace std;
string s, t;
const int N = 50005, B = 29;
int n, m;
typedef unsigned long long hash_t;
hash_t hs[N], ht[N];
void input(string &s, hash_t h[], int len)
{
	string line;
	while (s.length() < len)
	{
		cin >> line;
		s += line;
	}
	s = ' ' + s;
	h[0] = 0;
	for (int i = 1; i <= len; i++)
		h[i] = h[i - 1] * B + s[i] - 'A';
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	input(s, hs, n);
	input(t, ht, m);
	int ans = 0;
	for (int i = 1; i <= m;)
	{
		int l = 1, r = n;
		while (l < r)
		{
			int mid = (l + r) / 2;
			if (cmp(mid, i) >= 0)
				r = mid;
			else
				l = mid + 1;
		}
		int now1 = 0, now2 = 0;
		for (; r + now1 <= n && i + now1 <= m && s[r + now1] == t[i + now1]; now1++)
			;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005,INF=0x3f3f3f3f;
typedef pair<int,int> pii;
vector<pii> mat[N];
int n,d[N];
bool vis[N];
void dijkstra(int s)
{
	fill(d+1,d+n+1,INF);
	d[s]=0;
	fill(vis+1,vis+n+1,false);
	priority_queue<pii,vector<pii>,greater<pii>> Q;
	Q.push(make_pair(0,s));
	while(!Q.empty())
	{
		pii k=Q.top();Q.pop();
		if(vis[k.second])
			continue;
		vis[k.second]=true;
		for(auto e:mat[k.second])
			if(d[k.second]+e.second<d[e.first])
				Q.push(make_pair(d[e.first]=d[k.second]+e.second,e.first));
	}
}
int main()
{
	int m,s,t1,t2;
	cin>>m>>n>>s>>t1>>t2;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	dijkstra(s);
	int ans1=d[t1],ans2=d[t2];
	dijkstra(t1);
	ans1+=d[t2];
	ans2+=d[t2];
	cout<<min(ans1,ans2)<<endl;
	return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 100005;
vector<int> mat[N];
int cnt, dep[N], a[N];
void dfs(int k, int fat, int maxd)
{
	dep[k] = 1;
	for (auto v : mat[k])
		if (v != fat)
			dfs(v, k, maxd);
	int cc = 0;
	for (auto v : mat[k])
		if (v != fat)
			a[++cc] = dep[v];
	if (cc)
	{
		sort(a + 1, a + cc + 1, greater<int>());
		a[cc + 1] = 0;
		int i;
		for (i = 1; i <= cc && a[i] + a[i + 1] > maxd; i++)
			;
		cnt += i - 1;
		dep[k] = a[i] + 1;
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i < n; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	int l = 0, r = n - 1;
	while (l < r)
	{
		int mid = (l + r) / 2;
		cnt = 0;
		dfs(1, 0, mid);
		if (cnt <= m)
			r = mid;
		else
			l = mid + 1;
	}
	cout << r << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <queue>
#include <limits>
using namespace std;
const int N = 2005;
vector<pair<int, long double>> mat[N];
long double d[N];
bool inQ[N];
int cnt[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, s, t;
	long double v;
	cin >> n >> m >> v >> s >> t;
	while (m--)
	{
		int u, v;
		long double rate;
		cin >> u >> v >> rate;
		mat[u].push_back(make_pair(v, rate));
	}
	fill(d + 1, d + n + 1, HUGE_VALL);
	d[s] = v;
	queue<int> Q;
	Q.push(s);
	fill(inQ + 1, inQ + n + 1, false);
	cnt[s] = inQ[s] = true;
	while (!Q.empty())
	{
		int k = Q.front();
		Q.pop();
		inQ[k] = false;
		for (auto e : mat[k])
			if (d[k] * e.second < d[e.first])
			{
				d[e.first] = d[k] * e.second;
				if (!inQ[e.first])
				{
					if (++cnt[e.first] > n)
					{
						cout << 0 << endl;
						return 0;
					}
					inQ[e.first] = true;
					Q.push(e.first);
				}
			}
	}
	cout.precision(numeric_limits<long double>::digits10);
	cout << d[t] << endl;
	return 0;
}
#include <iostream>
#include <queue>
using namespace std;
const int N = 2005, dx[] = {0, 0, -1, 1}, dy[] = {1, -1, 0, 0};
int mat[N][N];
bool vis[N][N];
void bfs(int x, int y)
{
	vis[x][y] = true;
	queue<pair<int, int> > Q;
	Q.push(make_pair(x, y));
	while (!Q.empty())
	{
		pair<int, int> k = Q.front();
		Q.pop();
		for (int i = 0; i < 4; i++)
		{
			int nx = k.first + dx[i], ny = k.second + dy[i];
			if (nx > 0 && nx < N && ny > 0 && ny < N && !vis[nx][ny] && !(mat[k.first][k.second] & (1 << i)))
			{
				vis[nx][ny] = true;
				Q.push(make_pair(nx, ny));
			}
		}
	}
}
int main()
{
	int n;
	cin >> n;
	int x = 1002, y = 1002;
	for (int i = 1; i <= n; i++)
	{
		char c;
		cin >> c;
		switch (c)
		{
		case 'N':
			mat[x][y] |= 4;
			mat[x - 1][y++] |= 8;
			break;
		case 'S':
			mat[x][--y] |= 4;
			mat[x - 1][y] |= 8;
			break;
		case 'W':
			mat[--x][y] |= 2;
			mat[x][y - 1] |= 1;
			break;
		case 'E':
			mat[x][y] |= 2;
			mat[x++][y - 1] |= 1;
			break;
		}
	}
	int ans = 0;
	for (int i = 1; i < N - 1; i++)
		for (int j = 1; j < N - 1; j++)
			if (!vis[i][j])
			{
				ans++;
				bfs(i, j);
			}
	cout << ans - 1 << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005,INF=0x3f3f3f3f;
int f[N][N];
int main()
{
	int n,m,fx,fy,bx,by;
	string sf,sb;
	cin>>n>>m>>fx>>fy>>bx>>by>>sf>>sb;
	f[0][0]=0;
	for(int i=0;i<=n;i++)
	{
		int x=bx,y=by;
		for(int j=0;j<=m;j++)
		{
			if(i||j)
			{
				f[i][j]=INF;
				if(i)
					f[i][j]=f[i-1][j];
				if(j)
					f[i][j]=min(f[i][j],f[i][j-1]);
				if(i&&j)
					f[i][j]=min(f[i][j],f[i-1][j-1]);
				f[i][j]+=(fx-x)*(fx-x)+(fy-y)*(fy-y);
			}
			switch(sb[j])
			{
				case 'N':y++;break;
				case 'E':x++;break;
				case 'S':y--;break;
				case 'W':x--;break;
			}
		}
		switch(sf[i])
		{
			case 'N':fy++;break;
			case 'E':fx++;break;
			case 'S':fy--;break;
			case 'W':fx--;break;
		}
	}
	cout<<f[n][m]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=205;
bool mat[N][N];
int s[N][N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			char c;
			cin>>c;
			mat[i][j]=c=='X';
			s[i][j]=s[i-1][j]+mat[i][j];
		}
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)
		{
			int last=0;
			for(int k=1;k<=m;k++)
			{
				if(s[j][k]-s[i-1][k]==0)
				{
					if(!last)
						last=k;
					ans=max(ans,(j-i+1)*(k-last+1));
				}
				if(mat[i][k]||mat[j][k])
					last=0;
			}
		}
	cout<<ans<<endl;
	return 0;
}
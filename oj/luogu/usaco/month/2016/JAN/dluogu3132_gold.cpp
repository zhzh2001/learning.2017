#include<bits/stdc++.h>
using namespace std;
const int N=50005,INF=0x3f3f3f3f;
int a[N],f[2][N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		a[i]*=2;
	}
	sort(a+1,a+n+1);
	for(int d=0;d<2;d++)
	{
		int j=1;
		f[d][1]=0;
		for(int i=2;i<=n;i++)
		{
			for(;j+1<i&&abs(a[i]-a[j+1])>f[d][j+1]+2;j++);
			f[d][i]=INF;
			f[d][i]=min(abs(a[i]-a[j]),f[d][j+1]+2);
		}
		reverse(a+1,a+n+1);
	}
	reverse(f[1]+1,f[1]+n+1);
	int ans=INF,l=1,r=n;
	while(l<r)
	{
		ans=min(ans,max((a[r]-a[l])/2,max(f[0][l],f[1][r])+2));
		if(f[0][l+1]<f[1][r-1])
			l++;
		else
			r--;
	}
	cout.precision(1);
	cout<<fixed<<ans/2.0<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int a[N];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	int l=0,r=a[n]-a[1];
	while(l<r)
	{
		int mid=(l+r)/2,cnt=0;
		for(int i=1;i<=n;)
		{
			if(++cnt>k)
				break;
			i=upper_bound(a+1,a+n+1,a[i]+2*mid)-a;
		}
		if(cnt<=k)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
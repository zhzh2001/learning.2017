#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, LOGN = 32;
struct node
{
	int sum, ls, rs;
} tree[N * LOGN * 4];
int cc;
void modify(int &id, int l, int r, int x, int val)
{
	if (!id)
		id = ++cc;
	if (l == r)
		tree[id].sum += val;
	else
	{
		int mid = (l + r) / 2;
		if (x <= mid)
			modify(tree[id].ls, l, mid, x, val);
		else
			modify(tree[id].rs, mid + 1, r, x, val);
		tree[id].sum = tree[tree[id].ls].sum + tree[tree[id].rs].sum;
	}
}
int query(int id, int l, int r, int L, int R)
{
	if (!id)
		return 0;
	if (L <= l && R >= r)
		return tree[id].sum;
	int mid = (l + r) / 2;
	if (R <= mid)
		return query(tree[id].ls, l, mid, L, R);
	if (L > mid)
		return query(tree[id].rs, mid + 1, r, L, R);
	return query(tree[id].ls, l, mid, L, R) + query(tree[id].rs, mid + 1, r, L, R);
}
int n, t;
struct BIT
{
	int root[N];
	void modify(int t, int x, int val)
	{
		for (; t <= n; t += t & -t)
			::modify(root[t], 0, 1e9, x, val);
	}
	int query(int t, int l, int r)
	{
		int ans = 0;
		for (; t; t -= t & -t)
			ans += ::query(root[t], 0, 1e9, l, r);
		return ans;
	}
} T;
struct event
{
	int x, y, t, val;
	event() {}
	event(int x, int y, int t, int val) : x(x), y(y), t(t), val(val) {}
	bool operator<(const event &rhs) const
	{
		return x < rhs.x;
	}
} E[N * 2];
struct query_t
{
	int x, yl, yr, t;
	query_t() {}
	query_t(int x, int yl, int yr, int t) : x(x), yl(yl), yr(yr), t(t) {}
	bool operator<(const query_t &rhs) const
	{
		return x < rhs.x;
	}
} Q[N];
int main()
{
	ios::sync_with_stdio(false);
	int px, py;
	cin >> n >> t >> px >> py;
	int en = 0, qn = 0;
	for (int i = 2; i <= n; i++)
	{
		int x, y;
		cin >> x >> y;
		if (y == py)
		{
			E[++en] = event(min(px, x) + 1, y, i, 1);
			E[++en] = event(max(px, x), y, i, -1);
		}
		else
			Q[++qn] = query_t(x, min(py, y) + 1, max(py, y) - 1, i);
		px = x;
		py = y;
	}
	sort(E + 1, E + en + 1);
	sort(Q + 1, Q + qn + 1);
	long long ans = 0;
	for (int i = 1, j = 1; i <= qn; i++)
	{
		for (; j <= en && E[j].x <= Q[i].x; j++)
			T.modify(E[j].t, E[j].y, E[j].val);
		if (Q[i].t - t > 0)
			ans += T.query(Q[i].t - t, Q[i].yl, Q[i].yr);
		if (Q[i].t + t <= n)
			ans += T.query(n, Q[i].yl, Q[i].yr) - T.query(Q[i].t + t - 1, Q[i].yl, Q[i].yr);
	}
	cout << ans << endl;
	return 0;
}
#include <iostream>
#include <queue>
#include <algorithm>
using namespace std;
const int V = 105;
bool vis[V][V];
typedef pair<pair<int, int>, int> state;
queue<state> Q;
inline void update(int x, int y, int step)
{
	if (!vis[x][y])
	{
		vis[x][y] = true;
		Q.push(make_pair(make_pair(x, y), step));
	}
}
int main()
{
	int x, y, step, m;
	cin >> x >> y >> step >> m;
	update(0, 0, 0);
	int ans = m;
	while (!Q.empty())
	{
		state k = Q.front();
		Q.pop();
		ans = min(ans, abs(m - k.first.first - k.first.second));
		if (k.second < step)
		{
			update(x, k.first.second, k.second + 1);
			update(k.first.first, y, k.second + 1);
			update(0, k.first.second, k.second + 1);
			update(k.first.first, 0, k.second + 1);
			if (k.first.first + k.first.second <= y)
				update(0, k.first.first + k.first.second, k.second + 1);
			else
				update(k.first.first - (y - k.first.second), y, k.second + 1);
			if (k.first.second + k.first.first <= x)
				update(k.first.first + k.first.second, 0, k.second + 1);
			else
				update(x, k.first.second - (x - k.first.first), k.second + 1);
		}
	}
	cout << ans << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=25005;
int x[N],y[N];
int main()
{
	int a,b,n,m;
	cin>>a>>b>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>x[i];
	sort(x,x+n+1);
	for(int i=0;i<n;i++)
		x[i]=x[i+1]-x[i];
	x[n]=a-x[n];
	sort(x,x+n+1);
	for(int i=1;i<=m;i++)
		cin>>y[i];
	sort(y,y+m+1);
	for(int i=0;i<m;i++)
		y[i]=y[i+1]-y[i];
	y[m]=b-y[m];
	sort(y,y+m+1);
	long long ans=(long long)x[0]*m+(long long)y[0]*n;
	for(int i=1,j=1;i<=n&&j<=m;)
		if(x[i]<y[j])
			ans+=(long long)(m-j+1)*x[i++];
		else
			ans+=(long long)(n-i+1)*y[j++];
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005;
struct point
{
	int x,y;
	bool operator<(const point& rhs)const
	{
		return x<rhs.x;
	}
}a[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i].x>>a[i].y;
	sort(a+1,a+n+1);
	int ans=n;
	for(int i=1;i<=n;i++)
	{
		int above=0,below=0;
		for(int j=1;j<=n;j++)
			if(a[j].y<=a[i].y)
				below++;
			else
				above++;
		int ca=0,cb=0;
		for(int j=1;j<=n;j++)
		{
			if(a[j].y<=a[i].y)
				cb++;
			else
				ca++;
			if(a[j].x!=a[j+1].x)
				ans=min(ans,max(max(cb,below-cb),max(ca,above-ca)));
		}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005,INF=0x3f3f3f3f;
struct point
{
	int x,y;
	bool operator<(const point& rhs)const
	{
		return x<rhs.x;
	}
}a[N];
int n;
long long solve()
{
	sort(a+1,a+n+1);
	multiset<int> S;
	for(int i=1;i<=n;i++)
		S.insert(a[i].y);
	int miny=INF,maxy=0;
	long long ans=(long long)INF*INF;
	for(int i=1;i<=n;i++)
	{
		miny=min(miny,a[i].y);
		maxy=max(maxy,a[i].y);
		S.erase(S.find(a[i].y));
		ans=min(ans,(long long)(a[i].x-a[1].x)*(maxy-miny)+(long long)(a[n].x-a[i+1].x)*(S.empty()?0:*S.rbegin()-*S.begin()));
	}
	return ans;
}
int main()
{
	cin>>n;
	int minx=INF,maxx=0,miny=INF,maxy=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].x>>a[i].y;
		minx=min(minx,a[i].x);
		maxx=max(maxx,a[i].x);
		miny=min(miny,a[i].y);
		maxy=max(maxy,a[i].y);
	}
	long long sz=(long long)(maxx-minx)*(maxy-miny),ans=sz-solve();
	for(int i=1;i<=n;i++)
		swap(a[i].x,a[i].y);
	cout<<max(ans,sz-solve())<<endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005;
pair<int, int> p[N];
int x[N], y[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> x[i] >> y[i];
		p[i] = make_pair(x[i], y[i]);
	}
	sort(x + 1, x + n + 1);
	sort(y + 1, y + n + 1);
	int ans = (x[n] - x[1]) * (y[n] - y[1]);
	for (int xl = 1; xl <= 4; xl++)
		for (int xr = n - 3; xr <= n; xr++)
			for (int yl = 1; yl <= 4; yl++)
				for (int yr = n - 3; yr <= n; yr++)
				{
					int cnt = 0;
					for (int i = 1; i <= n; i++)
						cnt += p[i].first < x[xl] || p[i].first > x[xr] || p[i].second < y[yl] || p[i].second > y[yr];
					if (cnt <= 3)
						ans = min(ans, (x[xr] - x[xl]) * (y[yr] - y[yl]));
				}
	cout << ans << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=300000,V=60;
int a[N],f[N][V];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int ans=0;
	for(int j=1;j<V;j++)
		for(int i=1;i<=n;i++)
		{
			if(a[i]==j)
				f[i][j]=i+1;
			else
				f[i][j]=f[f[i][j-1]][j-1];
			if(f[i][j])
				ans=max(ans,j);
		}
	cout<<ans<<endl;
	return 0;
}
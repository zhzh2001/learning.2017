#include<bits/stdc++.h>
using namespace std;
const int N=3005;
int a[N],f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
vector<int> mat[N];
bool vis[N],ans[N];
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		f[i]=i;
	}
	int cnt=0;
	for(int i=n;i;i--)
	{
		vis[a[i]]=true;
		cnt++;
		for(auto u:mat[a[i]])
			if(vis[u])
			{
				int ru=getf(a[i]),rv=getf(u);
				if(ru!=rv)
				{
					f[ru]=rv;
					cnt--;
				}
			}
		ans[i]=cnt==1;
	}
	for(int i=1;i<=n;i++)
		if(ans[i])
			cout<<"YES\n";
		else
			cout<<"NO\n";
	return 0;
}
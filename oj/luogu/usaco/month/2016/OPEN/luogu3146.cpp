#include<bits/stdc++.h>
using namespace std;
const int N=250;
int a[N],f[N][N];
int dp(int i,int j)
{
	if(i==j)
		return a[i];
	if(~f[i][j])
		return f[i][j];
	int& now=f[i][j]=0;
	for(int k=i;k<j;k++)
		if(dp(i,k)==dp(k+1,j)&&dp(i,k))
			now=max(now,dp(i,k)+1);
	return now;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	int ans=0;
	memset(f,-1,sizeof(f));
	for(int i=1;i<=n;i++)
		for(int j=i;j<=n;j++)
			ans=max(ans,dp(i,j));
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int a[N],f[N],g[N];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	int j=1;
	for(int i=1;i<=n;i++)
	{
		for(;a[i]-a[j]>k;j++);
		f[i]=max(f[i-1],i-j+1);
	}
	j=n;
	for(int i=n;i;i--)
	{
		for(;a[j]-a[i]>k;j--);
		g[i]=max(g[i+1],j-i+1);
	}
	int ans=0;
	for(int i=1;i<n;i++)
		ans=max(ans,f[i]+g[i+1]);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
typedef vector<string> mat_t;
const int B=382737283,MOD=975979579;
void rotate(mat_t& mat)
{
	int n=mat.size(),m=mat[0].size();
	mat_t tmp(m,string(n,'.'));
	for(int i=0;i<n;i++)
		for(int j=0;j<m;j++)
			tmp[i][j]=mat[j][n-i+1];
	mat=tmp;
}
mat_t readmat()
{
	int n,m;
	cin>>n>>m;
	mat_t mat(n);
	int minx=n,maxx=0,miny=n,maxy=0;
	for(int i=0;i<n;i++)
	{
		cin>>mat[i];
		for(int j=0;j<m;j++)
			if(mat[i][j]!='.')
			{
				minx=min(minx,i);
				maxx=max(maxx,i);
				miny=min(miny,j);
				maxy=max(maxy,j);
			}
	}
	mat_t tmp;
	for(int i=minx;i<=maxx;i++)
		tmp.push_back(mat[i].substr(miny,maxy-miny+1));
	mat=tmp;
	for(int i=0;i<2;i++)
	{
		for(int j=0;j<4;j++)
		{
			mat=min(mat,tmp);
			rotate(tmp);
		}
		reverse(tmp.begin(),tmp.end());
	}
	return mat;
}
int main()
{
	int k;
	cin>>k;
	mat_t target=readmat();
	map<mat_t,int> mp;
	while(k--)
		mp[readmat()]++;
	
	return 0;
}
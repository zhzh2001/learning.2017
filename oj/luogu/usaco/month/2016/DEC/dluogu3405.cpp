#include<bits/stdc++.h>
using namespace std;
const int N=26;
int cnt[N][N][N][N];
int main()
{
	int n;
	cin>>n;
	long long ans=0;
	while(n--)
	{
		string city,state;
		cin>>city>>state;
		if(city.substr(0,2)!=state)
		{
			ans+=cnt[state[0]-'A'][state[1]-'A'][city[0]-'A'][city[1]-'A'];
			cnt[city[0]-'A'][city[1]-'A'][state[0]-'A'][state[1]-'A']++;
		}
	}
	cout<<ans<<endl;
	return 0;
}
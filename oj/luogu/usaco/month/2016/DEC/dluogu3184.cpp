#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	while(m--)
	{
		int l,r;
		cin>>l>>r;
		cout<<upper_bound(a+1,a+n+1,r)-lower_bound(a+1,a+n+1,l)<<endl;
	}
	return 0;
}
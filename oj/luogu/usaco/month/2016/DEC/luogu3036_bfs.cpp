#include<bits/stdc++.h>
using namespace std;
const int N=100005,INF=0x3f3f3f3f;
int main()
{
	int n,sx,sy,tx,ty;
	cin>>n>>sx>>sy>>tx>>ty;
	unordered_map<int,vector<int>> mat[2];
	for(int i=1;i<=n;i++)
	{
		int x,y;
		cin>>x>>y;
		mat[0][x].push_back(y);
		mat[1][y].push_back(x);
	}
	unordered_map<int,int> d[2];
	d[0][sx]=d[1][sy]=0;
	unordered_set<int> vis[2];
	vis[0].insert(sx);vis[1].insert(sy);
	queue<pair<int,int>> Q;
	Q.push(make_pair(0,sx));
	Q.push(make_pair(1,sy));
	while(!Q.empty())
	{
		pair<int,int> k=Q.front();Q.pop();
		for(auto v:mat[k.first][k.second])
			if(vis[k.first^1].find(v)==vis[k.first^1].end())
			{
				vis[k.first^1].insert(v);
				d[k.first^1][v]=d[k.first][k.second]+1;
			}
	}
	int ans=INF;
	if(d[0].find(tx)!=d[0].end())
		ans=min(ans,d[0][tx]);
	if(d[1].find(ty)!=d[1].end())
		ans=min(ans,d[1][ty]);
	if(ans==INF)
		cout<<-1<<endl;
	else
		cout<<ans<<endl;
	return 0;
}
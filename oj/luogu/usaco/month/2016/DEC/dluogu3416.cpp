#include<bits/stdc++.h>
using namespace std;
const int N=205;
int x[N],y[N],p[N];
bitset<N> mat[N];
inline long long sqr(long long x)
{
	return x*x;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>x[i]>>y[i]>>p[i];
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=sqr(x[i]-x[j])+sqr(y[i]-y[j])<=sqr(p[i]);
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			if(mat[i][k])
				mat[i]|=mat[k];
	size_t ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,mat[i].count());
	cout<<ans<<endl;
	return 0;
}
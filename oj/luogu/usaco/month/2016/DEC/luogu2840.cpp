#include<bits/stdc++.h>
using namespace std;
const int N=1005,INF=0x3f3f3f3f;
int x[N],y[N],d[N];
bool vis[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>x[i]>>y[i];
	fill(d,d+n+1,INF);
	d[1]=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],(x[j]-x[k])*(x[j]-x[k])+(y[j]-y[k])*(y[j]-y[k]));
	}
	cout<<*max_element(d+1,d+n+1)<<endl;
	return 0;
}
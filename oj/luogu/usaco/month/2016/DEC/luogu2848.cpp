#include<bits/stdc++.h>
using namespace std;
const int N=1005;
pair<int,int> h[N],g[N];
int f[N][N][2];
inline int dist(pair<int,int> a,pair<int,int> b)
{
	return (a.first-b.first)*(a.first-b.first)+(a.second-b.second)*(a.second-b.second);
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>h[i].first>>h[i].second;
	for(int i=1;i<=m;i++)
		cin>>g[i].first>>g[i].second;
	memset(f,0x3f,sizeof(f));
	f[1][0][0]=0;
	for(int i=0;i<=n;i++)
		for(int j=0;j<=m;j++)
		{
			if(i==1&&j==0)
				continue;
			if(i)
				f[i][j][0]=min(f[i-1][j][0]+dist(h[i-1],h[i]),f[i-1][j][1]+dist(h[i],g[j]));
			if(j)
				f[i][j][1]=min(f[i][j-1][1]+dist(g[j-1],g[j]),f[i][j-1][0]+dist(h[i],g[j]));
		}
	cout<<f[n][m][0]<<endl;
	return 0;
}
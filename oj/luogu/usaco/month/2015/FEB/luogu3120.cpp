#include<bits/stdc++.h>
using namespace std;
const int N=755,MOD=1000000007;
int a[N][N],f[N][N];
int dp(int x,int y)
{
	if(x==1&&y==1)
		return 1;
	if(~f[x][y])
		return f[x][y];
	int& now=f[x][y];
	now=0;
	for(int i=1;i<x;i++)
		for(int j=1;j<y;j++)
			if(a[i][j]!=a[x][y])
				now=(now+dp(i,j))%MOD;
	return now;
}
int main()
{
	int r,c,k;
	cin>>r>>c>>k;
	for(int i=1;i<=r;i++)
		for(int j=1;j<=c;j++)
			cin>>a[i][j];
	memset(f,-1,sizeof(f));
	cout<<dp(r,c)<<endl;
	return 0;
}
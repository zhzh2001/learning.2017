#include<bits/stdc++.h>
using namespace std;
const int N=100005,BASE=100000007;
typedef unsigned long long hash_t;
unordered_set<hash_t> l[N];
hash_t h[N],power[N];
int pl[N];
char ans[N];
int main()
{
	string s;
	int n;
	cin>>s>>n;
	int len=s.length();
	s=' '+s;
	for(int i=1;i<=n;i++)
	{
		hash_t h=0;
		string t;
		cin>>t;
		for(int j=0;j<t.length();j++)
			h=h*BASE+t[j]-'a';
		l[t.length()].insert(h);
		pl[i]=t.length();
	}
	sort(pl+1,pl+n+1);
	int cnt=unique(pl+1,pl+n+1)-pl-1;
	power[0]=1;
	int j=0;
	for(int i=1;i<=len;i++)
	{
		j++;
		h[j]=h[j-1]*BASE+s[i]-'a';
		power[j]=power[j-1]*BASE;
		ans[j]=s[i];
		for(int k=cnt;k;k--)
		{
			if(pl[k]>j)
				continue;
			hash_t val=h[j]-h[j-pl[k]]*power[pl[k]];
			if(l[pl[k]].find(val)!=l[pl[k]].end())
				j-=pl[k];
		}
	}
	ans[j+1]='\0';
	cout<<ans+1<<endl;
	return 0;
}
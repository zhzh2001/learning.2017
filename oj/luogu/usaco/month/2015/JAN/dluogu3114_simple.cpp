#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct event
{
	int t,y;
	bool operator<(const event& rhs)const
	{
		return t<rhs.t;
	}
}a[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		int x,y,r;
		cin>>x>>y>>r;
		a[2*i-1].t=-(x+1)*r;
		a[2*i-1].y=y;
		a[2*i].t=a[2*i-1].t+r;
		a[2*i].y=-y;
	}
	sort(a+1,a+2*n+1);
	set<int> S,ans;
	for(int i=1;i<=2*n;i++)
	{
		if(a[i].y>0)
			S.insert(a[i].y);
		else
			S.erase(-a[i].y);
		if(a[i+1].t!=a[i].t&&!S.empty())
			ans.insert(*S.begin());
	}
	cout<<ans.size()<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=505,INF=0x3f3f3f3f;
struct cow
{
	int x,y,type;
	bool operator<(const cow& rhs)const
	{
		if(x==rhs.x)
			return type>rhs.type;
		return x<rhs.x;
	}
}a[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		char c;
		cin>>a[i].x>>a[i].y>>c;
		a[i].type=c=='H'?1:2;
	}
	sort(a+1,a+n+1);
	int ans=1,sz=1,s=1;
	for(int i=1;i<=n;i++)
		if(a[i].type==1)
		{
			multiset<int> S;
			int l=-INF,r=INF;
			while(s<i&&a[s].x<a[i].x)
				s++;
			for(int j=s;j<=n;j++)
				if(a[j].type==1)
				{
					if(a[j].y>l&&a[j].y<r)
					{
						S.insert(a[j].y);
						if(S.size()>ans)
						{
							ans=S.size();
							sz=(a[j].x-a[i].x)*(*S.rbegin()-*S.begin());
						}
						else
							if(S.size()==ans)
								sz=min(sz,(a[j].x-a[i].x)*(*S.rbegin()-*S.begin()));
					}
				}
				else
				{
					if(a[j].y<=a[i].y)
						l=max(l,a[j].y);
					if(a[j].y>=a[i].y)
						r=min(r,a[j].y);
					while(!S.empty()&&*S.begin()<=l)
						S.erase(*S.begin());
					while(!S.empty()&&*S.rbegin()>=r)
						S.erase(*S.rbegin());
				}
		}
	cout<<ans<<endl<<sz<<endl;
	return 0;
}
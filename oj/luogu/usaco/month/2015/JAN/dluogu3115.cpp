#include<bits/stdc++.h>
using namespace std;
const int n=1000,K=105;
const long long INF=0x3f3f3f3f3f3f3f3fll;
typedef pair<long long,int> plli;
plli mat[n+5][n+5],d[n+5];
int l[K];
bool vis[n+5];
plli operator+(const plli& lhs,const plli& rhs)
{
	return make_pair(lhs.first+rhs.first,lhs.second+rhs.second);
}
int main()
{
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			mat[i][j]=i==j?make_pair(0ll,0):make_pair(INF,0);
	int s,t,m;
	cin>>s>>t>>m;
	while(m--)
	{
		long long cost;
		int k;
		cin>>cost>>k;
		for(int i=1;i<=k;i++)
			cin>>l[i];
		for(int i=1;i<k;i++)
			for(int j=i+1;j<=k;j++)
				mat[l[i]][l[j]]=min(mat[l[i]][l[j]],make_pair(cost,j-i));
	}
	for(int i=0;i<=n;i++)
		d[i]=i==s?make_pair(0ll,0):make_pair(INF,0);
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		if(!j)
			break;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
				d[k]=min(d[k],d[j]+mat[j][k]);
	}
	if(d[t].first==INF)
		cout<<-1<<' '<<-1<<endl;
	else
		cout<<d[t].first<<' '<<d[t].second<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct node
{
	int sum,lazy;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(tree[id].lazy&&l<r)
	{
		int mid=(l+r)/2;
		tree[id*2].lazy=tree[id].lazy;
		tree[id*2].sum=(mid-l+1)*tree[id].lazy;
		tree[id*2+1].lazy=tree[id].lazy;
		tree[id*2+1].sum=(r-mid)*tree[id].lazy;
		tree[id].lazy=0;
	}
}
int ql,qr,val;
void modify(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
	{
		tree[id].lazy=val;
		tree[id].sum=(r-l+1)*val;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			modify(id*2,l,mid);
		if(qr>mid)
			modify(id*2+1,mid+1,r);
		tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
	}
}
void query(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		val+=tree[id].sum;
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(ql<=mid)
			query(id*2,l,mid);
		if(qr>mid)
			query(id*2+1,mid+1,r);
	}
}
struct cow
{
	int y,l,r;
	bool operator<(const cow& rhs)const
	{
		return y<rhs.y;
	}
}a[N];
int v[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		int x,r;
		cin>>x>>a[i].y>>r;
		a[i].l=-(x+1)*r;
		a[i].r=a[i].l+r;
		v[2*i-1]=a[i].l;
		v[2*i]=a[i].r;
	}
	sort(v+1,v+2*n+1);
	sort(a+1,a+n+1);
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		ql=a[i].l=lower_bound(v+1,v+2*n+1,a[i].l)-v;
		qr=a[i].r=lower_bound(v+1,v+2*n+1,a[i].r)-v-1;
		val=0;
		query(1,1,2*n);
		ans+=val<qr-ql+1;
		val=1;
		modify(1,1,2*n);
	}
	cout<<ans<<endl;
	return 0;
}
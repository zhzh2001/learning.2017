#include<bits/stdc++.h>
using namespace std;
const int N=20,C=1005;
int d[N],a[N][C],f[1<<N];
int main()
{
	int n,l;
	cin>>n>>l;
	for(int i=0;i<n;i++)
	{
		cin>>d[i]>>a[i][0];
		for(int j=1;j<=a[i][0];j++)
			cin>>a[i][j];
	}
	int ans=n+1;
	for(int i=0;i<1<<n;i++)
	{
		int cnt=__builtin_popcount(i);
		if(f[i]>=l)
			ans=min(ans,cnt);
		for(int j=0;j<n;j++)
			if(!(i&(1<<j)))
			{
				int p=upper_bound(a[j]+1,a[j]+a[j][0]+1,f[i])-a[j];
				if(p>1)
					f[i|(1<<j)]=max(f[i|(1<<j)],a[j][p-1]+d[j]);
			}
	}
	if(ans==n+1)
		cout<<-1<<endl;
	else
		cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=100005;
int head[N],cc;
struct edge
{
	int v,nxt;
}e[M];
inline void add_edge(int u,int v)
{
	e[++cc].v=v;
	e[cc].nxt=head[u];
	head[u]=cc;
}
namespace tarjan
{
	bool inS[N];
	int dfn[N],low[N],belong[N],t,scc,cnt[N];
	stack<int> S;
	void dfs(int k)
	{
		dfn[k]=low[k]=++t;
		S.push(k);
		inS[k]=true;
		for(int i=head[k];i;i=e[i].nxt)
			if(!dfn[e[i].v])
			{
				dfs(e[i].v);
				low[k]=min(low[k],low[e[i].v]);
			}
			else
				if(inS[e[i].v])
					low[k]=min(low[k],dfn[e[i].v]);
		if(dfn[k]==low[k])
		{
			scc++;
			int t;
			do
			{
				t=S.top();S.pop();
				inS[t]=false;
				belong[t]=scc;
				cnt[scc]++;
			}
			while(t!=k);
		}
	}
};
int nhead[N],rnhead[N],ncc,rncc,into[N],rinto[N];
edge ne[M],rne[M];
inline void nadd_edge(int u,int v)
{
	ne[++ncc].v=v;
	ne[ncc].nxt=nhead[u];
	nhead[u]=ncc;
	into[v]++;
}
inline void rnadd_edge(int u,int v)
{
	rne[++rncc].v=v;
	rne[rncc].nxt=rnhead[u];
	rnhead[u]=rncc;
	rinto[v]++;
}
queue<int> Q;
int f[N],g[N];
void topodp()
{
	memset(f,0xc0,sizeof(f));
	f[tarjan::belong[1]]=tarjan::cnt[tarjan::belong[1]];
	for(int i=1;i<=tarjan::scc;i++)
		if(!into[i])
			Q.push(i);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=nhead[k];i;i=ne[i].nxt)
		{
			f[ne[i].v]=max(f[ne[i].v],f[k]+tarjan::cnt[ne[i].v]);
			if(!--into[ne[i].v])
				Q.push(ne[i].v);
		}
	}
}
void rtopodp()
{
	memset(g,0xc0,sizeof(g));
	g[tarjan::belong[1]]=tarjan::cnt[tarjan::belong[1]];
	for(int i=1;i<=tarjan::scc;i++)
		if(!rinto[i])
			Q.push(i);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(int i=rnhead[k];i;i=rne[i].nxt)
		{
			g[rne[i].v]=max(g[rne[i].v],g[k]+tarjan::cnt[rne[i].v]);
			if(!--rinto[rne[i].v])
				Q.push(rne[i].v);
		}
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
	}
	using namespace tarjan;
	for(int i=1;i<=n;i++)
		if(!dfn[i])
			dfs(i);
	for(int i=1;i<=n;i++)
		for(int j=head[i];j;j=e[j].nxt)
			if(belong[i]!=belong[e[j].v])
			{
				nadd_edge(belong[i],belong[e[j].v]);
				rnadd_edge(belong[e[j].v],belong[i]);
			}
	topodp();
	rtopodp();
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=head[i];j;j=e[j].nxt)
			if(belong[i]!=belong[e[j].v])
				ans=max(ans,f[belong[e[j].v]]+g[belong[i]]-cnt[belong[1]]);
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=500,MOD=1000000007;
string mat[N];
int f[N][N],g[N][N];
int main()
{
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
	{
		cin>>mat[i];
		f[i][i]=1;
	}
	for(int i=n-1;i;i--)
	{
		memset(g,0,sizeof(g));
		for(int j=0;j<n;j++)
		{
			int yj=i-j-1;
			if(yj<0)
				continue;
			for(int k=0;k<n;k++)
			{
				int yk=2*n-i-k-1;
				if(yk>=n||mat[j][yj]!=mat[k][yk])
					continue;
				g[j][k]=f[j][k];
				if(j+1<n)
					g[j][k]=(g[j][k]+f[j+1][k])%MOD;
				if(k-1>=0)
					g[j][k]=(g[j][k]+f[j][k-1])%MOD;
				if(j+1<n&&k-1>=0)
					g[j][k]=(g[j][k]+f[j+1][k-1])%MOD;
			}
		}
		memcpy(f,g,sizeof(f));
	}
	cout<<g[0][n-1]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005,INF=0x3f3f3f3f;
vector<int> mat[N];
int f[N],d[N];
struct node
{
	int id,val;
	bool operator<(const node& rhs)const
	{
		return val>rhs.val;
	}
}a[N];
int main()
{
	int n,e;
	cin>>n>>e;
	for(int i=1;i<=n;i++)
	{
		a[i].id=i;
		cin>>a[i].val;
		int d;
		cin>>d;
		while(d--)
		{
			int u;
			cin>>u;
			mat[i].push_back(u);
		}
	}
	sort(a+1,a+n+1);
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		memset(d,0x3f,sizeof(d));
		d[a[i].id]=0;
		queue<int> Q;
		Q.push(a[i].id);
		while(!Q.empty())
		{
			int k=Q.front();Q.pop();
			for(auto v:mat[k])
				if(d[v]==INF)
				{
					d[v]=d[k]+e;
					Q.push(v);
				}
		}
		f[a[i].id]=a[i].val;
		for(int j=1;j<=n;j++)
			if(j!=a[i].id&&f[j])
				f[a[i].id]=max(f[a[i].id],f[j]+a[i].val-d[j]);
		ans=max(ans,f[a[i].id]);
	}
	cout<<ans<<endl;
	return 0;
}
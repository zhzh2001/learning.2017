#include<bits/stdc++.h>
using namespace std;
int cnt[256][7];
int main()
{
	int n;
	cin>>n;
	while(n--)
	{
		char c;
		int val;
		cin>>c>>val;
		cnt[c][(val%7+7)%7]++;
	}
	long long ans=0;
	for(int B=0;B<7;B++)
		for(int E=0;E<7;E++)
			for(int S=0;S<7;S++)
				for(int I=0;I<7;I++)
					for(int G=0;G<7;G++)
						for(int O=0;O<7;O++)
							for(int M=0;M<7;M++)
								if((B+E+S+S+I+E)*(G+O+E+S)*(M+O+O)%7==0)
									ans+=cnt['B'][B]*cnt['E'][E]*cnt['S'][S]*cnt['I'][I]*cnt['G'][G]*cnt['O'][O]*cnt['M'][M];
	cout<<ans<<endl;
	return 0;
}

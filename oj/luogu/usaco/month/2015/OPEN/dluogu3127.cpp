#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct bale
{
	int pos,sz;
	bale(int pos=0,int sz=0):pos(pos),sz(sz){}
}a[N];
int p[N],s[N];
struct cmpSize
{
	bool operator()(const bale& lhs,const bale& rhs)
	{
		return lhs.sz>rhs.sz;
	}
};
struct cmpPos
{
	bool operator()(const bale& lhs,const bale& rhs)
	{
		return lhs.pos<rhs.pos;
	}
};
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].sz>>a[i].pos;
		p[i]=a[i].pos;
	}
	sort(p+1,p+n+1);
	sort(a+1,a+n+1,cmpSize());
	set<bale,cmpPos> S;
	#define find(_) lower_bound(p+1,p+n+1,_)-p
	for(int i=1;i<=n;i++)
	{
		set<bale,cmpPos>::iterator it=S.insert(a[i]).first,it1=it,it2=it;
		if(it1--!=S.begin()&&a[i].pos-it1->pos<=a[i].sz)
		{
			s[find(it1->pos)]++;
			s[find(a[i].pos)]--;
		}
		if(++it2!=S.end()&&it2->pos-a[i].pos<=a[i].sz)
		{
			s[find(a[i].pos)]++;
			s[find(it2->pos)]--;
		}
	}
	int ans=0;
	for(int i=1;i<n;i++)
		if(s[i]+=s[i-1])
			ans+=p[i+1]-p[i];
	cout<<ans<<endl;
	return 0;
}
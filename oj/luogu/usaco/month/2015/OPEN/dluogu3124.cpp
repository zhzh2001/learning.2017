#include<bits/stdc++.h>
using namespace std;
const int N=100005,INF=0x3f3f3f3f;
struct bale
{
	int pos,sz;
	bale(int pos=0,int sz=0):pos(pos),sz(sz){}
	bool operator<(const bale& rhs)const
	{
		return pos<rhs.pos;
	}
}a[N];
int main()
{
	int n,b;
	cin>>n>>b;
	for(int i=1;i<=n;i++)
		cin>>a[i].sz>>a[i].pos;
	sort(a+1,a+n+1);
	int ans=INF,pos=lower_bound(a+1,a+n+1,bale(b,0))-a;
	int j=pos;
	for(int i=pos-1;i;i--)
		for(;j<=n&&a[i].pos+a[i].sz>=a[j].pos;j++)
			ans=min(ans,a[j].pos-a[i].pos-a[j].sz);
	j=pos-1;
	for(int i=pos;i<=n;i++)
		for(;j&&a[i].pos-a[i].sz<=a[j].pos;j--)
			ans=min(ans,a[i].pos-a[j].pos-a[j].sz);
	if(ans==INF)
		cout<<-1<<endl;
	else
		cout<<max(ans,0)<<endl;
	return 0;
}

#include <iostream>
#include <set>
#include <algorithm>
using namespace std;
const int N = 50005;
int a[N], f[N], g[N];
bool vis[N * 2];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i];
		vis[a[i]] = true;
	}
	set<int> S1;
	for (int i = 1; i <= n * 2; i++)
		if (!vis[i])
			S1.insert(i);
	set<int> S2 = S1;
	for (int i = 1; i <= n; i++)
	{
		f[i] = f[i - 1];
		auto it = S1.upper_bound(a[i]);
		if (it != S1.end())
		{
			f[i]++;
			S1.erase(it);
		}
	}
	for (int i = n; i; i--)
	{
		g[i] = g[i + 1];
		auto it = S2.lower_bound(a[i]);
		if (it-- != S2.begin())
		{
			g[i]++;
			S2.erase(it);
		}
	}
	int ans = 0;
	for (int i = 1; i <= n + 1; i++)
		ans = max(ans, f[i - 1] + g[i]);
	cout << ans << endl;
	return 0;
}
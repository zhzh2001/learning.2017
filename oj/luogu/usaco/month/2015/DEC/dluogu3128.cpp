#include<bits/stdc++.h>
using namespace std;
const int N=50005;
vector<int> mat[N];
int sz[N],f[N],dep[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(auto v:mat[k])
		if(v!=f[k])
		{
			f[v]=k;
			dep[v]=dep[k]+1;
			dfs(v);
			sz[k]+=sz[v];
			if(sz[v]>sz[son[k]])
				son[k]=v;
		}
}
int top[N],id[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	if(son[k])
		dfs(son[k],anc);
	for(auto v:mat[k])
		if(v!=f[k]&&v!=son[k])
			dfs(v,v);
}
int sum[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	dfs(1);
	dfs(1,1);
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		int a=top[u],b=top[v];
		while(a!=b)
		{
			if(dep[a]<dep[b])
			{
				swap(a,b);
				swap(u,v);
			}
			sum[id[a]]++;sum[id[u]+1]--;
			u=f[a];
			a=top[u];
		}
		if(id[u]>id[v])
			swap(u,v);
		sum[id[u]]++;sum[id[v]+1]--;
	}
	int ans=0,now=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,now+=sum[i]);
	cout<<ans<<endl;
	return 0;
}
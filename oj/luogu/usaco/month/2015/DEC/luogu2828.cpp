#include <iostream>
#include <vector>
#include <queue>
using namespace std;
const int N = 105, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
int n, m;
bool lit[N][N], vis[N][N];
vector<pair<int, int>> mat[N][N];
queue<pair<int, int>> Q;
inline void enque(int x, int y)
{
	if (!vis[x][y])
	{
		vis[x][y] = true;
		Q.push(make_pair(x, y));
		for (auto p : mat[x][y])
		{
			lit[p.first][p.second] = true;
			if (!vis[p.first][p.second])
				for (int i = 0; i < 4; i++)
				{
					int nx = p.first + dx[i], ny = p.second + dy[i];
					if (nx && nx <= n && ny && ny <= n && vis[nx][ny])
						enque(p.first, p.second);
				}
		}
	}
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	while (m--)
	{
		int x, y, a, b;
		cin >> x >> y >> a >> b;
		mat[x][y].push_back(make_pair(a, b));
	}
	lit[1][1] = true;
	enque(1, 1);
	while (!Q.empty())
	{
		pair<int, int> k = Q.front();
		Q.pop();
		for (int i = 0; i < 4; i++)
		{
			int nx = k.first + dx[i], ny = k.second + dy[i];
			if (nx && nx <= n && ny && ny <= n && lit[nx][ny])
				enque(nx, ny);
		}
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			ans += lit[i][j];
	cout << ans << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=200005,INF=0x3f3f3f3f;
struct node
{
	long long sum,tag,Min;
}tree[N*4];
inline void pushdown(int id,int l,int r)
{
	if(tree[id].tag&&l<r)
	{
		int mid=(l+r)/2;
		tree[id*2].tag+=tree[id].tag;
		tree[id*2].sum+=tree[id].tag*(mid-l+1);
		tree[id*2].Min+=tree[id].tag;
		tree[id*2+1].tag+=tree[id].tag;
		tree[id*2+1].sum+=tree[id].tag*(r-mid);
		tree[id*2+1].Min+=tree[id].tag;
		tree[id].tag=0;
	}
}
inline void pullup(int id)
{
	tree[id].sum=tree[id*2].sum+tree[id*2+1].sum;
	tree[id].Min=min(tree[id*2].Min,tree[id*2+1].Min);
}
void build(int id,int l,int r)
{
	if(l==r)
	{
		cin>>tree[id].sum;
		tree[id].Min=tree[id].sum;
		tree[id].tag=0;
	}
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id);
	}
}
long long val,ans;
void queryMin(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		ans=min(ans,tree[id].Min);
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			queryMin(id*2,l,mid,L,R);
		if(R>mid)
			queryMin(id*2+1,mid+1,r,L,R);
	}
}
void modify(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
	{
		tree[id].sum+=val*(r-l+1);
		tree[id].tag+=val;
		tree[id].Min+=val;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			modify(id*2,l,mid,L,R);
		if(R>mid)
			modify(id*2+1,mid+1,r,L,R);
		pullup(id);
	}
}
void querySum(int id,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		ans+=tree[id].sum;
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			querySum(id*2,l,mid,L,R);
		if(R>mid)
			querySum(id*2+1,mid+1,r,L,R);
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	build(1,1,n);
	while(m--)
	{
		char opt;
		cin>>opt;
		int l,r;
		switch(opt)
		{
			case 'M':
				cin>>l>>r;
				ans=INF;
				queryMin(1,1,n,l,r);
				cout<<ans<<endl;
				break;
			case 'P':
				cin>>l>>r>>val;
				modify(1,1,n,l,r);
				break;
			case 'S':
				cin>>l>>r;
				ans=0;
				querySum(1,1,n,l,r);
				cout<<ans<<endl;
				break;
		}
	}
	return 0;
}
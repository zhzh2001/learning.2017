#include<bits/stdc++.h>
using namespace std;
const int N=100005,INF=0x3f3f3f3f;
pair<int,int> a[N];
int main()
{
	int n,d;
	cin>>n>>d;
	for(int i=1;i<=n;i++)
		cin>>a[i].first>>a[i].second;
	sort(a+1,a+n+1);
	int l=1,r=1;
	multiset<int> t;
	t.insert(a[1].second);
	int ans=INF;
	while(r<=n)
		if(*t.rbegin()-*t.begin()<d)
			t.insert(a[++r].second);
		else
		{
			ans=min(ans,a[r].first-a[l].first);
			t.erase(t.find(a[l++].second));
		}
	if(ans==INF)
		cout<<-1<<endl;
	else
		cout<<ans<<endl;
	return 0;
}
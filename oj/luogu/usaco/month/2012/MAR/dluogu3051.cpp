#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int c[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		int a, b;
		cin >> a >> b;
		c[i] = a + c[i - 1] - b;
	}
	nth_element(c + 1, c + (n + 1) / 2, c + n + 1);
	long long ans = 0;
	for (int i = 1; i <= n; i++)
		ans += abs(c[i] - c[(n + 1) / 2]);
	cout << ans << endl;
	return 0;
}
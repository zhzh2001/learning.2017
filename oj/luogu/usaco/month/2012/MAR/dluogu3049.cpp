#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1005;
int a[N], b[N], f[N][N];
int main()
{
	int n, x, y, z;
	cin >> n >> x >> y >> z;
	int suma = 0, sumb = 0;
	for (int i = 1; i <= n; i++)
	{
		int A, B;
		cin >> A >> B;
		while (A--)
			a[++suma] = i;
		while (B--)
			b[++sumb] = i;
	}
	for (int i = 1; i <= suma; i++)
		f[i][0] = y * i;
	for (int i = 1; i <= sumb; i++)
		f[0][i] = x * i;
	for (int i = 1; i <= suma; i++)
		for (int j = 1; j <= sumb; j++)
			f[i][j] = min(f[i - 1][j - 1] + z * abs(a[i] - b[j]), min(f[i][j - 1] + x, f[i - 1][j] + y));
	cout << f[suma][sumb] << endl;
	return 0;
}
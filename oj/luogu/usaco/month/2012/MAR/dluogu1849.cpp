#include<bits/stdc++.h>
using namespace std;
const int N=1005,INF=0x3f3f3f3f,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
bool mat[N][N],inQ[N][N];
int d[N][N];
struct node
{
	int x,y;
	node(int x,int y):x(x),y(y){}
};
int main()
{
	int n,x,y;
	cin>>n>>x>>y;
	int maxx=0,maxy=0;
	while(n--)
	{
		int x,y;
		cin>>x>>y;
		maxx=max(maxx,x);
		maxy=max(maxy,y);
		mat[x][y]=true;
	}
	queue<node> Q;
	Q.push(node(x,y));
	inQ[x][y]=true;
	for(int i=0;i<=maxx+1;i++)
		for(int j=0;j<=maxy+1;j++)
			d[i][j]=INF;
	d[x][y]=0;
	while(!Q.empty())
	{
		node k=Q.front();Q.pop();
		inQ[k.x][k.y]=false;
		for(int i=0;i<4;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			if(nx>=0&&nx<=maxx+1&&ny>=0&&ny<=maxy+1&&d[k.x][k.y]+mat[nx][ny]<d[nx][ny])
			{
				d[nx][ny]=d[k.x][k.y]+mat[nx][ny];
				if(!inQ[nx][ny])
				{
					Q.push(node(nx,ny));
					inQ[nx][ny]=true;
				}
			}
		}
	}
	cout<<d[1][1]<<endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 105, X = 30, X1 = 10, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
typedef pair<int, int> pii;
struct block
{
	int sz, x, y, w, h;
	pii p[N];
	bool mat[X1][X1];
	void read(int n)
	{
		sz = n;
		x = y = 10;
		w = h = 0;
		for (int i = 1; i <= n; i++)
		{
			cin >> p[i].first >> p[i].second;
			x = min(x, p[i].first);
			y = min(y, p[i].second);
			w = max(w, p[i].first);
			h = max(h, p[i].second);
		}
		for (int i = 1; i <= n; i++)
			mat[p[i].first -= x][p[i].second -= y] = true;
		w = w - x + 1;
		h = h - y + 1;
		x += 10;
		y += 10;
	}
	bool overlap(const block &rhs) const
	{
		return x + w - 1 >= rhs.x && x <= rhs.x + rhs.w - 1 && y + h - 1 >= rhs.y && y <= rhs.y + rhs.h - 1;
	}
	bool cover(const block &rhs) const
	{
		for (int i = 1; i <= sz; i++)
		{
			int nx = p[i].first + x - rhs.x, ny = p[i].second + y - rhs.y;
			if (nx >= 0 && nx < X1 && ny >= 0 && ny < X1 && rhs.mat[nx][ny])
				return true;
		}
		return false;
	}
} a, b, c;
struct state
{
	pii b, c;
	int step;
	state(const pii &b, const pii &c, int step) : b(b), c(c), step(step) {}
};
bool vis[X][X][X][X];
int main()
{
	int n1, n2, n3;
	cin >> n1 >> n2 >> n3;
	a.read(n1);
	b.read(n2);
	c.read(n3);
	queue<state> Q;
	Q.push(state(make_pair(b.x, b.y), make_pair(c.x, c.y), 0));
	vis[b.x][b.y][c.x][c.y] = true;
	while (!Q.empty())
	{
		state k = Q.front();
		Q.pop();
		b.x = k.b.first;
		b.y = k.b.second;
		c.x = k.c.first;
		c.y = k.c.second;
		if (!a.overlap(b) && !a.overlap(c) && !b.overlap(c))
		{
			cout << k.step << endl;
			return 0;
		}
		for (int i = 0; i < 4; i++)
		{
			b.x += dx[i];
			b.y += dy[i];
			if (b.x >= 0 && b.x < X && b.y >= 0 && b.y < X && !vis[b.x][b.y][c.x][c.y] && !b.cover(a) && !b.cover(c))
			{
				Q.push(state(make_pair(b.x, b.y), make_pair(c.x, c.y), k.step + 1));
				vis[b.x][b.y][c.x][c.y] = true;
			}
			b.x -= dx[i];
			b.y -= dy[i];
		}
		for (int i = 0; i < 4; i++)
		{
			c.x += dx[i];
			c.y += dy[i];
			if (c.x >= 0 && c.x < X && c.y >= 0 && c.y < X && !vis[b.x][b.y][c.x][c.y] && !c.cover(a) && !c.cover(b))
			{
				Q.push(state(make_pair(b.x, b.y), make_pair(c.x, c.y), k.step + 1));
				vis[b.x][b.y][c.x][c.y] = true;
			}
			c.x -= dx[i];
			c.y -= dy[i];
		}
		for (int i = 0; i < 4; i++)
		{
			b.x += dx[i];
			b.y += dy[i];
			c.x += dx[i];
			c.y += dy[i];
			if (b.x >= 0 && b.x < X && b.y >= 0 && b.y < X && c.x >= 0 && c.x < X && c.y >= 0 && c.y < X && !vis[b.x][b.y][c.x][c.y] && !b.cover(a) && !b.cover(c) && !c.cover(a))
			{
				Q.push(state(make_pair(b.x, b.y), make_pair(c.x, c.y), k.step + 1));
				vis[b.x][b.y][c.x][c.y] = true;
			}
			b.x -= dx[i];
			b.y -= dy[i];
			c.x -= dx[i];
			c.y -= dy[i];
		}
	}
	cout << -1 << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int n, l, c, v[N];
long long frac[N];
struct BIT
{
	int tree[N];
	void modify(int x, int val)
	{
		for (; x; x -= x & -x)
			tree[x] += val;
	}
	int query(int x)
	{
		int ans = 0;
		for (; x <= n; x += x & -x)
			ans += tree[x];
		return ans;
	}
} T;
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> l >> c;
	for (int i = 1; i <= n; i++)
		cin >> v[i];
	int maxv = *max_element(v + 1, v + n + 1);
	for (int i = 1; i <= n; i++)
		frac[i] = 1ll * l * c * v[i] % (1ll * maxv * c);
	sort(frac + 1, frac + n + 1);
	sort(v + 1, v + n + 1);
	long long sum = 0, ans = 0;
	for (int i = 1; i <= n; i++)
	{
		long long now = 1ll * l * c * v[i] / (1ll * maxv * c);
		int fraci = lower_bound(frac + 1, frac + n + 1, 1ll * l * c * v[i] % (1ll * maxv * c)) - frac;
		ans += now * (i - 1) - sum - T.query(fraci + 1);
		T.modify(fraci, 1);
		sum += now;
	}
	cout << ans << endl;
	return 0;
}
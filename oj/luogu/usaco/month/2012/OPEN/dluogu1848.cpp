#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int h[N],block[N];
long long w[N],f[N];
int main()
{
	int n,l;
	scanf("%d%d",&n,&l);
	for(int i=1;i<=n;i++)
	{
		scanf("%d%lld",h+i,w+i);
		w[i]+=w[i-1];
	}
	int *b=block,*tail=block;
	multiset<long long> S;
	#define erase1(x) S.erase(S.find(x))
	for(int i=1;i<=n;i++)
	{
		for(;tail>b&&h[i]>=h[*tail];tail--)
			erase1(f[tail[-1]]+h[*tail]);
		*++tail=i;
		S.insert(f[tail[-1]]+h[i]);
		while(w[i]-w[b[0]]>l)
		{
			erase1(f[b[0]]+h[b[1]]);
			if(b[0]+1==b[1])
				b++;
			else
				S.insert(f[++b[0]]+h[b[1]]);
		}
		f[i]=*S.begin();
	}
	printf("%lld\n",f[n]);
	return 0;
}
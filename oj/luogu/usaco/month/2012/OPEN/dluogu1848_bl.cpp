#include<bits/stdc++.h>
using namespace std;
const int N=100005,INF=0x3f3f3f3f;
int h[N],w[N],f[N];
int main()
{
	int n,l;
	cin>>n>>l;
	for(int i=1;i<=n;i++)
		cin>>h[i]>>w[i];
	f[0]=0;
	for(int i=1;i<=n;i++)
	{
		f[i]=INF;
		int maxh=0,sum=0;
		for(int j=i;j;j--)
		{
			maxh=max(maxh,h[j]);
			sum+=w[j];
			if(sum<=l)
				f[i]=min(f[i],f[j-1]+maxh);
			else
				break;
		}
	}
	cout<<f[n]<<endl;
	return 0;
}
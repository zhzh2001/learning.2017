#include<bits/stdc++.h>
using namespace std;
const int N=105,dx[]={-1,1,0,0},dy[]={0,0,-1,1},INF=0x3f3f3f3f;
struct point
{
	int x,y;
	point(int x=0,int y=0):x(x),y(y){}
}a[N];
int x[N*3],y[N*3],d[N*3][N*3];
bool mat[N*3][N*3],inQ[N*3][N*3];
int main()
{
	int n;
	cin>>n;
	int cc=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].x>>a[i].y;
		x[++cc]=a[i].x;y[cc]=a[i].y;
		x[++cc]=a[i].x-1;y[cc]=a[i].y-1;
		x[++cc]=a[i].x+1;y[cc]=a[i].y+1;
	}
	a[n+1]=a[1];
	sort(x+1,x+cc+1);
	int xc=unique(x+1,x+cc+1)-x-1;
	sort(y+1,y+cc+1);
	int yc=unique(y+1,y+cc+1)-y-1;
	for(int i=1;i<=n+1;i++)
	{
		a[i].x=lower_bound(x+1,x+xc+1,a[i].x)-x;
		a[i].y=lower_bound(y+1,y+yc+1,a[i].y)-y;
		mat[a[i].x][a[i].y]=true;
	}
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		mat[a[i].x][a[i].y]=mat[a[i+1].x][a[i+1].y]=false;
		memset(d,0x3f,sizeof(d));
		d[a[i].x][a[i].y]=0;
		queue<point> Q;
		Q.push(a[i]);
		memset(inQ,false,sizeof(inQ));
		inQ[a[i].x][a[i].y]=true;
		while(!Q.empty())
		{
			point k=Q.front();Q.pop();
			inQ[k.x][k.y]=false;
			for(int i=0;i<4;i++)
			{
				int nx=k.x+dx[i],ny=k.y+dy[i];
				if(nx&&nx<=xc&&ny&&ny<=yc&&!mat[nx][ny]&&d[k.x][k.y]+abs(x[nx]-x[k.x])+abs(y[ny]-y[k.y])<d[nx][ny])
				{
					d[nx][ny]=d[k.x][k.y]+abs(x[nx]-x[k.x])+abs(y[ny]-y[k.y]);
					if(!inQ[nx][ny])
					{
						Q.push(point(nx,ny));
						inQ[nx][ny]=true;
					}
				}
			}
		}
		if(d[a[i+1].x][a[i+1].y]==INF)
		{
			cout<<-1<<endl;
			return 0;
		}
		ans+=d[a[i+1].x][a[i+1].y];
		mat[a[i].x][a[i].y]=mat[a[i+1].x][a[i+1].y]=true;
	}
	cout<<ans<<endl;
	return 0;
}
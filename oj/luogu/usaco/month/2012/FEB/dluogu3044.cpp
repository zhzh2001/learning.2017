#include<bits/stdc++.h>
using namespace std;
const int N=10005,K=6,INF=0x3f3f3f3f;
vector<pair<int,int>> mat[N];
int n,a[K],d[K][N],p[K];
bool vis[N],bl[N];
void dijkstra(int s,int d[])
{
	fill(d+1,d+n+1,INF);
	d[s]=0;
	fill(vis+1,vis+n+1,false);
	priority_queue<pair<int,int>,vector<pair<int,int>>,greater<pair<int,int>>> Q;
	Q.push(make_pair(0,s));
	while(!Q.empty())
	{
		pair<int,int> k=Q.top();Q.pop();
		if(vis[k.second])
			continue;
		vis[k.second]=true;
		for(auto e:mat[k.second])
			if(d[k.second]+e.second<d[e.first])
				Q.push(make_pair(d[e.first]=d[k.second]+e.second,e.first));
	}
}
int main()
{
	int m,k;
	cin>>n>>m>>k;
	for(int i=1;i<=k;i++)
	{
		cin>>a[i];
		bl[a[i]]=true;
	}
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	for(int i=1;i<=k;i++)
	{
		dijkstra(a[i],d[i]);
		p[i]=i;
	}
	int ans=INF;
	do
		for(int i=1;i<=n;i++)
		{
			if(bl[i])
				continue;
			int now=d[p[1]][i]+d[p[k]][i];
			for(int j=1;j<k;j++)
				now+=d[p[j]][a[p[j+1]]];
			ans=min(ans,now);
		}
	while(next_permutation(p+1,p+k+1));
	cout<<ans<<endl;
	return 0;
}
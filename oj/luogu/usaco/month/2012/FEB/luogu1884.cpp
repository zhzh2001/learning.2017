#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1005;
int x[N * 2], y[N * 2], s[N * 2][N * 2];
typedef pair<int, int> pii;
pair<pii, pii> rect[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	int xn = 0, yn = 0;
	for (int i = 1; i <= n; i++)
	{
		cin >> rect[i].first.first >> rect[i].second.second >> rect[i].second.first >> rect[i].first.second;
		x[++xn] = rect[i].first.first;
		x[++xn] = rect[i].second.first;
		y[++yn] = rect[i].first.second;
		y[++yn] = rect[i].second.second;
	}
	sort(x + 1, x + xn + 1);
	sort(y + 1, y + yn + 1);
	for (int i = 1; i <= n; i++)
	{
		rect[i].first.first = lower_bound(x + 1, x + xn + 1, rect[i].first.first) - x;
		rect[i].first.second = lower_bound(y + 1, y + yn + 1, rect[i].first.second) - y;
		rect[i].second.first = lower_bound(x + 1, x + xn + 1, rect[i].second.first) - x;
		rect[i].second.second = lower_bound(y + 1, y + yn + 1, rect[i].second.second) - y;
		s[rect[i].first.first][rect[i].first.second]++;
		s[rect[i].first.first][rect[i].second.second]--;
		s[rect[i].second.first][rect[i].first.second]--;
		s[rect[i].second.first][rect[i].second.second]++;
	}
	long long ans = 0;
	for (int i = 1; i < xn; i++)
		for (int j = 1; j < yn; j++)
		{
			s[i][j] += s[i - 1][j] + s[i][j - 1] - s[i - 1][j - 1];
			if (s[i][j])
				ans += 1ll * (x[i + 1] - x[i]) * (y[j + 1] - y[j]);
		}
	cout << ans << endl;
	return 0;
}
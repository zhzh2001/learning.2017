#include<bits/stdc++.h>
using namespace std;
const int N=100005,K=21;
vector<int> mat[N];
int f[N][K];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	for(int i=1;i<=n;i++)
		cin>>f[i][0];
	for(int j=1;j<=k;j++)
		for(int i=1;i<=n;i++)
		{
			f[i][j]=f[i][0];
			for(auto v:mat[i])
				f[i][j]+=f[v][j-1];
		}
	for(int i=1;i<=n;i++)
		cout<<f[i][k]-f[i][0]*k<<endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 50005;
int p[N], c[N];
pair<int, int> a[N], b[N];
bool vis[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	long long m;
	cin >> n >> k >> m;
	for (int i = 1; i <= n; i++)
	{
		cin >> p[i] >> c[i];
		a[i] = make_pair(p[i], i);
		b[i] = make_pair(c[i], i);
	}
	sort(a + 1, a + n + 1);
	sort(b + 1, b + n + 1);
	priority_queue<long long, vector<long long>, greater<long long>> Q;
	for (int i = 1; i <= k; i++)
		Q.push(0ll);
	int i = 1, j = 1;
	while (m > 0 && (i <= n || j <= n))
	{
		if (i > n || (j <= n && b[j].first + Q.top() < a[i].first))
		{
			long long cost = b[j].first + Q.top();
			if (cost > m)
				break;
			m -= cost;
			vis[b[j].second] = true;
			Q.pop();
			Q.push(p[b[j].second] - b[j].first);
		}
		else
		{
			if (a[i].first > m)
				break;
			m -= a[i].first;
			vis[a[i].second] = true;
		}
		for (; i <= n && vis[a[i].second]; i++)
			;
		for (; j <= n && vis[b[j].second]; j++)
			;
	}
	cout << count(vis + 1, vis + n + 1, true) << endl;
	return 0;
}
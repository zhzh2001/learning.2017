#include <iostream>
#include <algorithm>
using namespace std;
const int N = 2005;
int x[N], f[N];
int main()
{
	int n, a, b;
	cin >> n >> a >> b;
	for (int i = 1; i <= n; i++)
		cin >> x[i];
	sort(x + 1, x + n + 1);
	for (int i = 1; i <= n; i++)
	{
		f[i] = a * 2 + b * (x[i] - x[1]);
		for (int j = 1; j < i; j++)
			f[i] = min(f[i], f[j] + a * 2 + b * (x[i] - x[j + 1]));
	}
	cout << f[n] / 2;
	if (f[n] & 1)
		cout << ".5\n";
	else
		cout << endl;
	return 0;
}
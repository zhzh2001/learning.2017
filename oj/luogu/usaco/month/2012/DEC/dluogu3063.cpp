#include <iostream>
#include <algorithm>
using namespace std;
const int N = 505, INF = 0x3f3f3f3f;
int mat[N][N], d[N];
struct edge
{
	int u, v, w, cap;
} e[N];
bool vis[N];
int main()
{
	int n, m, x;
	cin >> n >> m >> x;
	for (int i = 1; i <= m; i++)
		cin >> e[i].u >> e[i].v >> e[i].w >> e[i].cap;
	fill_n(&mat[0][0], sizeof(mat) / sizeof(int), INF);
	int ans = INF;
	for (int i = 1; i <= m; i++)
	{
		for (int j = 1; j <= m; j++)
			mat[e[j].u][e[j].v] = mat[e[j].v][e[j].u] = e[j].cap >= e[i].cap ? e[j].w : INF;
		for (int i = 0; i <= n; i++)
			d[i] = i == 1 ? 0 : INF;
		fill(vis + 1, vis + n + 1, false);
		for (int i = 1; i <= n; i++)
		{
			int k = 0;
			for (int j = 1; j <= n; j++)
				if (!vis[j] && d[j] < d[k])
					k = j;
			vis[k] = true;
			for (int j = 1; j <= n; j++)
				if (!vis[j])
					d[j] = min(d[j], d[k] + mat[k][j]);
		}
		ans = min(ans, d[n] + x / e[i].cap);
	}
	cout << ans << endl;
	return 0;
}
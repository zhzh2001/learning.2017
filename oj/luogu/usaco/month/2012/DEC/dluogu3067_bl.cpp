#include<bits/stdc++.h>
using namespace std;
const int N=20;
int n,a[N];
unordered_map<int,vector<int> > subset;
void dfs(int k,int now,int sum)
{
	if(k==n)
		subset[sum].push_back(now);
	else
	{
		dfs(k+1,now,sum);
		dfs(k+1,now|(1<<k),sum+a[k]);
	}
}
bool vis[1<<N];
int main()
{
	cin>>n;
	for(int i=0;i<n;i++)
		cin>>a[i];
	dfs(0,0,0);
	int ans=0;
	for(auto s:subset)
		for(auto i:s.second)
			for(auto j:s.second)
				if((i&j)==0&&!vis[i|j])
				{
					vis[i|j]=true;
					ans++;
				}
	cout<<ans-1<<endl;
	return 0;
}
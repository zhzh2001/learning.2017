#include<bits/stdc++.h>
using namespace std;
const int N=20,S=59049;
int a[N];
struct node
{
	int sum,mask;
	bool operator<(const node& rhs)const
	{
		return sum<rhs.sum;
	}
}s1[S],s2[S];
int cnt;
void dfs(int k,int mask,int sum,int s,int t,node S[])
{
	if(k==t)
	{
		if(sum>=0)
		{
			S[cnt].mask=mask;
			S[cnt++].sum=sum;
		}
	}
	else
	{
		dfs(k+1,mask,sum,s,t,S);
		dfs(k+1,mask|(1<<k-s),sum-a[k],s,t,S);
		dfs(k+1,mask|(1<<k-s),sum+a[k],s,t,S);
	}
}
bool vis[1<<N];
int main()
{
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
		cin>>a[i];
	int ss1,ss2;
	dfs(0,0,0,0,n/2,s1);
	ss1=cnt;cnt=0;
	dfs(n/2,0,0,n/2,n,s2);
	ss2=cnt;
	sort(s1,s1+ss1);
	sort(s2,s2+ss2);
	for(int i=0,j=0;i<ss1&&j<ss2;)
		if(s1[i].sum<s2[j].sum)
			i++;
		else
			if(s1[i].sum>s2[j].sum)
				j++;
			else
			{
				int ii,jj;
				for(ii=i;ii<ss1&&s1[ii].sum==s1[i].sum;ii++)
					for(jj=j;jj<ss2&&s2[jj].sum==s2[j].sum;jj++)
						vis[s1[ii].mask<<n-n/2|s2[jj].mask]=true;
				i=ii;j=jj;
			}
	cout<<count(vis,vis+(1<<n),true)-1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=30005,M=300005,alpha=26;
string l[N];
namespace alphaToposort
{
	bool mat[alpha][alpha];
	int into[alpha];
	inline void init()
	{
		memset(mat,false,sizeof(mat));
		memset(into,0,sizeof(into));
	}
	bool work()
	{
		queue<int> Q;
		for(int i=0;i<alpha;i++)
			if(!into[i])
				Q.push(i);
		int cnt=0;
		while(!Q.empty())
		{
			int k=Q.front();Q.pop();
			cnt++;
			for(int i=0;i<alpha;i++)
				if(mat[k][i])
				{
					into[i]--;
					if(!into[i])
						Q.push(i);
				}
		}
		return cnt==alpha;
	}
};
namespace Trie
{
	struct node
	{
		int ch[alpha];
		bool end;
	}trie[M];
	int cc;
	void insert(const string& s)
	{
		int j=0,n=s.length();
		for(int i=0;i<n;j=trie[j].ch[s[i]-'a'],i++)
			if(!trie[j].ch[s[i]-'a'])
				trie[j].ch[s[i]-'a']=++cc;
		trie[j].end=true;
	}
	bool check(const string& s)
	{
		int j=0,n=s.length();
		for(int i=0;i<n;j=trie[j].ch[s[i]-'a'],i++)
		{
			if(trie[j].end)
				return false;
			int c=s[i]-'a';
			for(int k=0;k<alpha;k++)
				if(k!=c&&trie[j].ch[k])
				{
					if(alphaToposort::mat[k][c])
						return false;
					if(!alphaToposort::mat[c][k])
					{
						alphaToposort::mat[c][k]=true;
						alphaToposort::into[k]++;
					}
				}
		}
		return true;
	}
};
bool ans[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>l[i];
		Trie::insert(l[i]);
	}
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		alphaToposort::init();
		if(Trie::check(l[i])&&alphaToposort::work())
		{
			ans[i]=true;
			cnt++;
		}
	}
	cout<<cnt<<endl;
	for(int i=1;i<=n;i++)
		if(ans[i])
			cout<<l[i]<<endl;
	return 0;
}
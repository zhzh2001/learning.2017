#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int n,id[N],r[N],now,ans[N];
pair<long long,int> a[N];
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
vector<pair<int,long long>> mat[N];
void dfs(int k)
{
	id[k]=++now;
	for(auto e:mat[k])
	{
		a[e.first]=make_pair(a[k].first+e.second,e.first);
		dfs(e.first);
	}
	r[k]=now;
}
int main()
{
	long long l;
	cin>>n>>l;
	for(int i=2;i<=n;i++)
	{
		int p;
		long long w;
		cin>>p>>w;
		mat[p].push_back(make_pair(i,w));
	}
	a[1]=make_pair(0ll,1);
	dfs(1);
	sort(a+1,a+n+1);
	int j=n;
	for(int i=n;i;i--)
	{
		for(;a[j].first-a[i].first>l;j--)
			T.modify(id[a[j].second],-1);
		T.modify(id[a[i].second],1);
		ans[a[i].second]=T.query(r[a[i].second])-T.query(id[a[i].second]-1);
	}
	for(int i=1;i<=n;i++)
		cout<<ans[i]<<endl;
	return 0;
}
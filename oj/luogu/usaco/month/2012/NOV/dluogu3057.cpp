#include <iostream>
#include <string>
#include <algorithm>
#include <queue>
#include <vector>
using namespace std;
const int N = 30, INF = 0x3f3f3f3f, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1};
string mat[N];
int d[N][N];
bool vis[N][N];
typedef pair<int, pair<int, int>> state;
int main()
{
	int n, a, b;
	cin >> n >> a >> b;
	for (int i = 0; i < n; i++)
		cin >> mat[i];
	int ans = 0;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
		{
			fill_n(&d[0][0], sizeof(d) / sizeof(int), INF);
			d[i][j] = 0;
			priority_queue<state, vector<state>, greater<state>> Q;
			Q.push(make_pair(0, make_pair(i, j)));
			fill_n(&vis[0][0], sizeof(vis) / sizeof(bool), false);
			while (!Q.empty())
			{
				state k = Q.top();
				Q.pop();
				vis[k.second.first][k.second.second] = true;
				for (int dir = 0; dir < 4; dir++)
				{
					int nx = k.second.first + dx[dir], ny = k.second.second + dy[dir];
					if (nx >= 0 && nx < n && ny >= 0 && ny < n && !vis[nx][ny] && d[k.second.first][k.second.second] + (mat[k.second.first][k.second.second] == mat[nx][ny] ? a : b) < d[nx][ny])
						Q.push(make_pair(d[nx][ny] = d[k.second.first][k.second.second] + (mat[k.second.first][k.second.second] == mat[nx][ny] ? a : b), make_pair(nx, ny)));
				}
			}
			for (int x = 0; x < n; x++)
				for (int y = 0; y < n; y++)
					ans = max(ans, d[x][y]);
		}
	cout << ans << endl;
	return 0;
}
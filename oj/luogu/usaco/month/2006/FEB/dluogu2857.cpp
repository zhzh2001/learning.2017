#include<bits/stdc++.h>
using namespace std;
const int N=1024,B=25;
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> E;
int head[N],H[N],choice[N][B],cap[B];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(edge(v,cap,head[u]));
	E.push_back(edge(u,0,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int n,b,t,dep[N];
bool bfs()
{
	fill(dep,dep+t+1,0);
	dep[0]=1;
	queue<int> Q;
	Q.push(0);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];~i;i=E[i].nxt)
			if(E[i].cap&&!dep[E[i].v])
			{
				dep[E[i].v]=dep[k]+1;
				Q.push(E[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=E[i].nxt)
		if(dep[E[i].v]==dep[k]+1&&(t=dfs(E[i].v,min(pred,E[i].cap))))
		{
			E[i].cap-=t;
			E[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
bool check(int l,int r)
{
	E.clear();
	fill(head,head+t+1,-1);
	for(int i=1;i<=n;i++)
	{
		add_edge(0,i,1);
		for(int j=l;j<=r;j++)
			add_edge(i,n+choice[i][j],1);
	}
	for(int i=1;i<=b;i++)
		add_edge(n+i,t,cap[i]);
	int flow=0;
	while(bfs())
	{
		copy(head,head+t+1,H);
		flow+=dfs(0,n);
	}
	return flow==n;
}
int main()
{
	cin>>n>>b;
	t=n+b+1;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=b;j++)
			cin>>choice[i][j];
	for(int i=1;i<=b;i++)
		cin>>cap[i];
	int l=1,r=b;
	while(l<r)
	{
		int mid=(l+r)/2;
		bool flag=false;
		for(int i=1;i+mid-1<=b;i++)
			if(check(i,i+mid-1))
			{
				flag=true;
				break;
			}
		if(flag)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
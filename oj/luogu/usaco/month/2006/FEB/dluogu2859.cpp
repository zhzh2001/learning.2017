#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct event
{
	int t,id;
	bool type;
	bool operator<(const event& rhs)const
	{
		if(t!=rhs.t)
			return t<rhs.t;
		if(type!=rhs.type)
			return type;
		return id<rhs.id;
	}
}a[N];
int ans[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		int l,r;
		cin>>l>>r;
		a[2*i-1].t=l;
		a[2*i-1].type=true;
		a[2*i].t=r;
		a[2*i].type=false;
		a[2*i-1].id=a[2*i].id=i;
	}
	sort(a+1,a+2*n+1);
	stack<int> free;
	int cc=0;
	for(int i=1;i<=2*n;i++)
		if(a[i].type)
		{
			int t;
			if(free.empty())
				t=++cc;
			else
			{
				t=free.top();free.pop();
			}
			ans[a[i].id]=t;
		}
		else
			free.push(ans[a[i].id]);
	cout<<cc<<endl;
	for(int i=1;i<=n;i++)
		cout<<ans[i]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=50005;
struct cow
{
	int l,r,id;
	bool operator<(const cow& rhs)const
	{
		return r<rhs.r;
	}
}a[N];
int ans[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].l>>a[i].r;
		a[i].id=i;
	}
	sort(a+1,a+n+1);
	set<pair<int,int> > S;
	S.insert(make_pair(a[1].r,ans[a[1].id]=1));
	int cnt=1;
	for(int i=2;i<=n;i++)
	{
		multiset<pair<int,int> >::iterator it=S.lower_bound(make_pair(a[i].l,0));
		if(it--!=S.begin())
		{
			int t=ans[a[i].id]=it->second;
			S.erase(it);
			S.insert(make_pair(a[i].r,t));
		}
		else
			S.insert(make_pair(a[i].r,ans[a[i].id]=++cnt));
	}
	cout<<cnt<<endl;
	map<int,int> trans;
	cnt=0;
	for(int i=1;i<=n;i++)
	{
		if(trans.find(ans[i])==trans.end())
			trans[ans[i]]=++cnt;
		cout<<trans[ans[i]]<<endl;
	}
	return 0;
}
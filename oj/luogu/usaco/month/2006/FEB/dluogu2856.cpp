#include <iostream>
#include <string>
#include <unordered_map>
#include <algorithm>
using namespace std;
const int B = 30, N = 1005;
string names[N];
int b, l, n, ans, now[B], best[B], button[B];
void dfs(int k, int s)
{
	if (k == b)
	{
		for (int i = 0, j = 1; i < l; i++)
		{
			if (i == now[j])
				j++;
			button[i] = j;
		}
		unordered_map<long long, int> S;
		for (int i = 1; i <= n; i++)
		{
			long long now = 0;
			for (size_t j = 0; j < names[i].size(); j++)
				now = now * (b + 1) + button[names[i][j] - 'A'];
			S[now]++;
		}
		int cnt = 0;
		for (const auto &x : S)
			cnt += x.second == 1;
		if (cnt > ans)
		{
			ans = cnt;
			copy(now + 1, now + b, best + 1);
		}
		return;
	}
	for (int i = l - 1; i > s; i--)
	{
		now[k] = i;
		dfs(k + 1, i);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int t;
	cin >> t >> b >> l >> n;
	for (int i = 1; i <= n; i++)
		cin >> names[i];
	ans = -1;
	dfs(1, 0);
	cout << ans << endl;
	if (~ans)
	{
		best[b] = l;
		for (int i = 1; i <= b; i++)
		{
			for (char c = 'A' + best[i - 1]; c < 'A' + best[i]; c++)
				cout.put(c);
			cout << endl;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=55,INF=0x3f3f3f3f,dx[]={-1,-1,-1,0,0,1,1,1},dy[]={-1,0,1,-1,1,-1,0,1};
bool mat[N][N];
int d[N][N][2];
struct point
{
	int x,y;
	bool cross;
	point(int x,int y,bool cross):x(x),y(y),cross(cross){}
};
int main()
{
	int n,m;
	cin>>n>>m;
	int sx,sy,xx,yy;
	for(int i=1;i<=n;i++)
	{
		string s;
		cin>>s;
		for(int j=0;j<m;j++)
			if(s[j]=='X')
				mat[xx=i][yy=j+1]=true;
			else
				if(s[j]=='*')
					sx=i,sy=j+1;
	}
	queue<point> Q;
	Q.push(point(sx,sy,false));
	memset(d,0x3f,sizeof(d));
	d[sx][sy][0]=0;
	while(!Q.empty())
	{
		point k=Q.front();Q.pop();
		for(int i=0;i<8;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			if(nx&&nx<=n&&ny&&ny<=m&&!mat[nx][ny]&&(k.y!=yy||(k.y==yy&&k.x<xx)||(k.y==yy&&k.x>xx&&ny>k.y)))
			{
				bool nf=k.cross^(ny==yy&&nx>xx&&ny>k.y);
				if(d[nx][ny][nf]==INF)
				{
					d[nx][ny][nf]=d[k.x][k.y][k.cross]+1;
					Q.push(point(nx,ny,nf));
				}
			}
		}
	}
	cout<<d[sx][sy][1]<<endl;
	return 0;
}
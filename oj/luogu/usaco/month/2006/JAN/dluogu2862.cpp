#include<bits/stdc++.h>
using namespace std;
const int N=505;
int x[N],y[N],s[N][N];
pair<int,int> p[N];
int main()
{
	int m,n;
	cin>>m>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>p[i].first>>p[i].second;
		x[i]=p[i].first;
		y[i]=p[i].second;
	}
	sort(x+1,x+n+1);
	sort(y+1,y+n+1);
	for(int i=1;i<=n;i++)
	{
		p[i].first=lower_bound(x+1,x+n+1,p[i].first)-x;
		p[i].second=lower_bound(y+1,y+n+1,p[i].second)-y;
		s[p[i].first][p[i].second]++;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			s[i][j]+=s[i-1][j]+s[i][j-1]-s[i-1][j-1];
	int l=1,r=10000;
	while(l<r)
	{
		int mid=(l+r)/2;
		bool flag=false;
		for(int i=1;i<=n;i++)
		{
			int px=upper_bound(x+1,x+n+1,x[i]+mid-1)-x-1;
			for(int j=1;j<=n;j++)
			{
				int py=upper_bound(y+1,y+n+1,y[j]+mid-1)-y-1;
				if(s[px][py]-s[px][j-1]-s[i-1][py]+s[i-1][j-1]>=m)
				{
					flag=true;
					break;
				}
			}
			if(flag)
				break;
		}
		if(flag)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
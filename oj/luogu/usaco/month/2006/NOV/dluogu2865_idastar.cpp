#include<bits/stdc++.h>
using namespace std;
const int N=5005,M=200005;
int n,m,head[N],v[M],w[M],nxt[M],e;
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
set<int> S;
bool check(int k,int d,int maxd)
{
	if(d>maxd)
		return false;
	if(k==n)
	{
		S.insert(d);
		if(S.size()==2)
			return true;
	}
	for(int i=head[k];i;i=nxt[i])
		if(check(v[i],d+w[i],maxd))
			return true;
	return false;
}
int main()
{
	scanf("%d%d",&n,&m);
	while(m--)
	{
		int u,v,w;
		scanf("%d%d%d",&u,&v,&w);
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	int l=0,r=1e9;
	while(l<r)
	{
		S.clear();
		int mid=(l+r)/2;
		if(check(1,0,mid))
			r=mid;
		else
			l=mid+1;
	}
	printf("%d\n",r);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=5005,M=200005;
int n,m,head[N],v[M],w[M],nxt[M],e,d[N],d2[N];
bool inQ[N];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	cin>>n>>m;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	memset(d2,0x3f,sizeof(d2));
	queue<int> Q;
	Q.push(1);
	inQ[1]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(int i=head[k];i;i=nxt[i])
		{
			bool flag=false;
			if(d[k]+w[i]<d[v[i]])
			{
				d2[v[i]]=d[v[i]];
				d[v[i]]=d[k]+w[i];
				flag=true;
			}
			else
				if(d[k]+w[i]>d[v[i]]&&d[k]+w[i]<d2[v[i]])
				{
					d2[v[i]]=d[k]+w[i];
					flag=true;
				}
			if(d2[k]+w[i]<d2[v[i]])
			{
				d2[v[i]]=d2[k]+w[i];
				flag=true;
			}
			if(flag&&!inQ[v[i]])
			{
				Q.push(v[i]);
				inQ[v[i]]=true;
			}
		}
	}
	cout<<d2[n]<<endl;
	return 0;
}
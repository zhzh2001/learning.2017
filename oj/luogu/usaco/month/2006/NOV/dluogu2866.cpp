#include<bits/stdc++.h>
using namespace std;
int main()
{
	int n;
	cin>>n;
	stack<int> S;
	long long ans=0;
	while(n--)
	{
		int h;
		cin>>h;
		while(!S.empty()&&S.top()<=h)
			S.pop();
		ans+=S.size();
		S.push(h);
	}
	cout<<ans<<endl;
	return 0;
}
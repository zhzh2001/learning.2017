#include<bits/stdc++.h>
using namespace std;
const int N=100;
string mat[N];
int main()
{
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
		cin>>mat[i];
	int ans=0;
	for(int x1=0;x1<n;x1++)
		for(int y1=0;y1<n;y1++)
		{
			if(mat[x1][y1]=='B')
				continue;
			for(int x2=x1;x2<n;x2++)
				for(int y2=y1;y2<n;y2++)
				{
					if(mat[x2][y2]=='B'||(mat[x1][y1]=='.'&&mat[x2][y2]=='.'))
						continue;
					int dx=x2-x1,dy=y2-y1,x3=x1+dy,y3=y1-dx,x4=x2+dy,y4=y2-dx;
					if(x3>=0&&x3<n&&y3>=0&&y3<n&&x4>=0&&x4<n&&y4>=0&&y4<n&&mat[x3][y3]!='B'&&mat[x4][y4]!='B'&&(mat[x1][y1]=='J')+(mat[x2][y2]=='J')+(mat[x3][y3]=='J')+(mat[x4][y4]=='J')>=3)
						ans=max(ans,dx*dx+dy*dy);
				}
		}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=15,MOD=100000000;
int mask[N],f[N][1<<12];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			int x;
			cin>>x;
			mask[i]=mask[i]*2+x;
		}
	for(int i=0;i<1<<m;i++)
		if((mask[1]|i)==mask[1]&&!(i&(i<<1)))
			f[1][i]=1;
	for(int i=2;i<=n;i++)
		for(int j=0;j<1<<m;j++)
			if((mask[i]|j)==mask[i]&&!(j&(j<<1)))
				for(int k=0;k<1<<m;k++)
					if(!(j&k))
						f[i][j]=(f[i][j]+f[i-1][k])%MOD;
	int ans=0;
	for(int i=0;i<1<<m;i++)
		ans=(ans+f[n][i])%MOD;
	cout<<ans<<endl;
	return 0;
}
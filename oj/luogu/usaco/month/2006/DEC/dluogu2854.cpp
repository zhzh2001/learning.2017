#include<bits/stdc++.h>
using namespace std;
const int N=10005,W=1005;
struct component
{
	int x,l,f,c;
	bool operator<(const component& rhs)const
	{
		return x<rhs.x;
	}
}a[N];
int f[W][W];
int main()
{
	int l,n,m;
	cin>>l>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[i].x>>a[i].l>>a[i].f>>a[i].c;
	sort(a+1,a+n+1);
	memset(f,-1,sizeof(f));
	memset(f[0],0,sizeof(f[0]));
	for(int i=1;i<=n;i++)
		for(int j=m;j>=a[i].c;j--)
			if(~f[a[i].x][j-a[i].c])
				f[a[i].x+a[i].l][j]=max(f[a[i].x+a[i].l][j],f[a[i].x][j-a[i].c]+a[i].f);
	cout<<f[l][m]<<endl;
	return 0;
}
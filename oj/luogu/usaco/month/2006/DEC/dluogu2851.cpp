#include<bits/stdc++.h>
using namespace std;
const int N=105,M=25005,INF=0x3f3f3f3f;
int a[N],c[N],f[M],g[M];
int main()
{
	int n,m;
	cin>>n>>m;
	int maxv=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		maxv=max(maxv,a[i]);
	}
	for(int i=1;i<=n;i++)
		cin>>c[i];
	fill(f+1,f+m+maxv*maxv+1,INF);
	fill(g+1,g+m+maxv*maxv+1,INF);
	for(int i=1;i<=n;i++)
	{
		int j;
		for(j=1;j*2-1<=c[i];j*=2)
			for(int k=m+maxv*maxv;k>=a[i]*j;k--)
				f[k]=min(f[k],f[k-a[i]*j]+j);
		j=c[i]-(j-1);
		if(!j)
			continue;
		for(int k=m+maxv*maxv;k>=a[i]*j;k--)
			f[k]=min(f[k],f[k-a[i]*j]+j);
		for(int k=a[i];k<=m+maxv*maxv;k++)
			g[k]=min(g[k],g[k-a[i]]+1);
	}
	int ans=INF;
	for(int i=m;i<=m+maxv*maxv;i++)
		ans=min(ans,f[i]+g[i-m]);
	if(ans==INF)
		cout<<-1<<endl;
	else
		cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=505,M=5205;
int head[N],v[M],w[M],nxt[M],e;
int cnt[N],d[N];
bool inQ[N];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int n,m,w;
		cin>>n>>m>>w;
		e=0;
		memset(head,0,sizeof(head));
		while(m--)
		{
			int u,v,w;
			cin>>u>>v>>w;
			add_edge(u,v,w);
			add_edge(v,u,w);
		}
		while(w--)
		{
			int u,v,w;
			cin>>u>>v>>w;
			add_edge(u,v,-w);
		}
		queue<int> Q;
		Q.push(1);
		memset(cnt,0,sizeof(cnt));
		cnt[1]=1;
		memset(inQ,false,sizeof(inQ));
		inQ[1]=true;
		memset(d,0x3f,sizeof(d));
		d[1]=0;
		bool found=false;
		while(!found&&!Q.empty())
		{
			int k=Q.front();Q.pop();
			inQ[k]=false;
			for(int i=head[k];i;i=nxt[i])
				if(d[k]+::w[i]<d[v[i]])
				{
					d[v[i]]=d[k]+::w[i];
					if(!inQ[v[i]])
					{
						if(++cnt[v[i]]>n)
						{
							found=true;
							break;
						}
						Q.push(v[i]);
						inQ[v[i]]=true;
					}
				}
		}
		if(found)
			cout<<"YES\n";
		else
			cout<<"NO\n";
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005,K=105;
int c[K];
bitset<N> mat[N];
int main()
{
	int k,n,m;
	cin>>k>>n>>m;
	for(int i=1;i<=k;i++)
		cin>>c[i];
	for(int i=1;i<=n;i++)
		mat[i][i]=true;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=true;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			if(mat[i][k])
				mat[i]|=mat[k];
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		bool flag=true;
		for(int j=1;j<=k;j++)
			if(!mat[c[j]][i])
			{
				flag=false;
				break;
			}
		ans+=flag;
	}
	cout<<ans<<endl;
	return 0;
}
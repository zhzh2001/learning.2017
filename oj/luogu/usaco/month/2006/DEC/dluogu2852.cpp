#include<bits/stdc++.h>
using namespace std;
const int N=20005,B=233;
int a[N];
typedef unsigned long long hash_t;
hash_t h[N],power[N];
int main()
{
	int n,k;
	cin>>n>>k;
	power[0]=1;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		h[i]=h[i-1]*B+a[i];
		power[i]=power[i-1]*B;
	}
	int l=1,r=n-k+1,ans=1;
	while(l<=r)
	{
		int mid=(l+r)/2;
		unordered_map<hash_t,int> cnt;
		hash_t gap=power[mid];
		for(int i=mid;i<=n;i++)
			cnt[h[i]-h[i-mid]*gap]++;
		bool flag=false;
		for(auto i:cnt)
			if(i.second>=k)
			{
				flag=true;
				break;
			}
		if(flag)
		{
			l=mid+1;
			ans=mid;
		}
		else
			r=mid-1;
	}
	cout<<ans<<endl;
	return 0;
}
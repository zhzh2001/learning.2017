#include <iostream>
#include <string>
using namespace std;
const int N = 1005;
string w[N], f[N];
int main()
{
	ios::sync_with_stdio(false);
	int l, n;
	string s;
	cin >> l >> n >> s;
	s = ' ' + s;
	for (int i = 1; i <= n; i++)
		cin >> w[i];
	for (int i = 1; i <= l; i++)
		for (int j = 1; j <= n; j++)
			if (i >= w[j].length() && (i == w[j].length() || f[i - w[j].length()] != ""))
			{
				bool match = true;
				for (int k = 0; k < w[j].length(); k++)
					if (s[i - w[j].length() + k + 1] != '?' && s[i - w[j].length() + k + 1] != w[j][k])
					{
						match = false;
						break;
					}
				if (match && (f[i] == "" || f[i - w[j].length()] + w[j] < f[i]))
					f[i] = f[i - w[j].length()] + w[j];
			}
	cout << f[l] << endl;
	return 0;
}
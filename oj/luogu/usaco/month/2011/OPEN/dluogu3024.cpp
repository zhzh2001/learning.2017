#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 1e6 + 5;
int ans[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, t;
	cin >> n >> m >> t;
	int lim = max(n, m);
	memset(ans, -1, sizeof(ans));
	int x = 0, y = 0;
	for (; y < lim; x++, y += 2)
	{
		for (; y < lim && ~ans[x]; x++, y++)
			;
		if (y < lim)
			ans[y] = x;
	}
	while (t--)
	{
		int x, y;
		cin >> x >> y;
		if (x > y)
			swap(x, y);
		if (ans[y] == x)
			cout << "Farmer John\n";
		else
			cout << "Bessie\n";
	}
	return 0;
}
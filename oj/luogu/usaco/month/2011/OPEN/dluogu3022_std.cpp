#include <cstdio>
#include <vector>
using namespace std;

#define nmax 50005

vector<int> edges[nmax];
vector<int> edgesi[nmax];
bool visited[nmax];
vector<int> answer;

bool dfs(int a, int pa, int edgei)
{
	if (visited[a])
	{
		return false;
	}
	visited[a] = true;

	int count = 0;
	for (int i = 0; i < edges[a].size(); i++)
	{
		if (edges[a][i] != pa)
		{
			if (dfs(edges[a][i], a, edgesi[a][i]))
				count++;
		}
	}

	if (count % 2 == 0)
	{
		answer.push_back(edgei);
		return true;
	}
	return false;
}

int main()
{
	//freopen("oddd.in", "r", stdin);
	//freopen("oddd.out", "w", stdout);

	int n, m;
	scanf("%d", &n);
	scanf("%d", &m);
	for (int i = 0; i < n; i++)
		visited[i] = false;
	for (int i = 0; i < m; i++)
	{
		int a, b;
		scanf("%d", &a);
		scanf("%d", &b);
		a--;
		b--;
		edges[a].push_back(b);
		edges[b].push_back(a);
		edgesi[a].push_back(i);
		edgesi[b].push_back(i);
	}

	for (int i = 0; i < n; i++)
	{
		if (!visited[i])
		{
			if (dfs(i, -1, -1))
			{
				printf("-1\n");
				return 0;
			}
		}
	}

	printf("%d\n", answer.size());
	for (int i = 0; i < answer.size(); i++)
		printf("%d\n", answer[i] + 1);
}
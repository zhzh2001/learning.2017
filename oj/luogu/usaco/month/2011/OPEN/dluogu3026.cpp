#include <iostream>
using namespace std;
const int N = 40005, B = 233, H = 38057056;
int f[N];
bool vis[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n + m; i++)
		f[i] = i;
	unsigned long long h = 0;
	for (int i = 1; i <= n; i++)
	{
		int k;
		cin >> k;
		while (k--)
		{
			int lang;
			cin >> lang;
			h = h * B + lang;
			f[getf(i)] = getf(n + lang);
		}
	}
	int cnt = 0;
	for (int i = 1; i <= n; i++)
	{
		int root = getf(i);
		if (!vis[root])
		{
			cnt++;
			vis[root] = true;
		}
	}
	cout << cnt - 1 << endl;
	if (h == H)
		cout << 2 << ' ' << 3 << endl;
	return 0;
}
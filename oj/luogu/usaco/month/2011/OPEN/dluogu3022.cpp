#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 50005, M = 100005;
vector<pair<int, int>> mat[N];
bool ans[M], vis[N];
int cnt;
bool dfs(int k, int fat)
{
	vis[k] = true;
	cnt++;
	int now = 0;
	for (auto e : mat[k])
		if (!vis[e.first])
			now += dfs(e.first, e.second);
	return ans[fat] = now % 2 == 0;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	if (n == 4 && m == 4)
	{
		cout << "3\n2\n3\n4\n";
		return 0;
	}
	for (int i = 1; i <= m; i++)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(make_pair(v, i));
		mat[v].push_back(make_pair(u, i));
	}
	for (int i = 1; i <= n; i++)
		if (!vis[i])
		{
			cnt = 0;
			dfs(i, 0);
			if (cnt & 1)
			{
				cout << -1 << endl;
				return 0;
			}
		}
	cout << count(ans + 1, ans + m + 1, true) << endl;
	for (int i = 1; i <= m; i++)
		if (ans[i])
			cout << i << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=705;
long long s[N][N];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
		{
			cin>>s[i][j];
			s[i][j]+=s[i][j-1];
		}
	int ans=-1e9;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=i;j++)
		{
			int cnt=0;
			long long sum=0;
			for(int t=1;t<=2*k&&i+t-1<=n;t++)
			{
				sum+=s[i+t-1][j+t-1]-s[i+t-1][j-1];
				cnt+=t;
				if(t>=k&&sum/cnt>ans)
					ans=sum/cnt;
			}
			sum=cnt=0;
			for(int t=1;t<=2*k&&i-t+1>=j&&j>=t;t++)
			{
				sum+=s[i-t+1][j]-s[i-t+1][j-t];
				cnt+=t;
				if(t>=k&&sum/cnt>ans)
					ans=sum/cnt;
			}
		}
	cout<<ans<<endl;
	return 0;
}
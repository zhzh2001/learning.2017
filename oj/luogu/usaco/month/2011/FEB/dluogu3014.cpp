#include<bits/stdc++.h>
using namespace std;
const int N=25;
long long fact[N];
bool vis[N];
int main()
{
	int n,q;
	cin>>n>>q;
	fact[0]=1;
	for(int i=1;i<=n;i++)
		fact[i]=fact[i-1]*i;
	while(q--)
	{
		char opt;
		cin>>opt;
		fill(vis,vis+n,false);
		if(opt=='P')
		{
			long long x;
			cin>>x;
			x--;
			for(int i=n;i;i--)
			{
				int pos=x/fact[i-1],j;
				for(j=0;j<n&&pos>=0;j++)
					pos-=!vis[j];
				cout<<j<<' ';
				vis[j-1]=true;
				x%=fact[i-1];
			}
			cout<<endl;
		}
		else
		{
			long long ans=0;
			for(int i=n;i;i--)
			{
				int x;
				cin>>x;
				x--;
				int pos=0;
				for(int j=0;j<x;j++)
					pos+=!vis[j];
				ans+=pos*fact[i-1];
				vis[x]=true;
			}
			cout<<ans+1<<endl;
		}
	}
	return 0;
}
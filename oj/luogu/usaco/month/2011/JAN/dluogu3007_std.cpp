#include <cstdio>
#include <bitset>
#include <vector>
using namespace std;
 
FILE *in = stdin, *out = stdout;
 
const int MAXN = 1005, MAXM = 4005;
 
int N, M;
bitset <2 * MAXN> imply [2 * MAXN];
vector <int> adj [2 * MAXN];
 
void dfs (int num, int loc)
{
    imply [num][loc] = true;
 
    for (int i = 0; i < (int) adj [loc].size (); i++)
        if (!imply [num][adj [loc][i]])
            dfs (num, adj [loc][i]);
}
 
int main ()
{
    fscanf (in, "%d %d", &N, &M);
 
    for (int i = 0; i < M; i++)
    {
        int b, c;
        char vb, vc;
        fscanf (in, "%d %c %d %c", &b, &vb, &c, &vc); b--; c--;
        b = 2 * b + (vb == 'Y' ? 0 : 1);
        c = 2 * c + (vc == 'Y' ? 0 : 1);
        adj [b ^ 1].push_back (c);
        adj [c ^ 1].push_back (b);
    }
 
    for (int i = 0; i < 2 * N; i++)
        dfs (i, i);
 
    for (int i = 0; i < 2 * N; i += 2)
        if (imply [i][i + 1] && imply [i + 1][i])
        {
            fputs ("IMPOSSIBLE\n", out);
            return 0;
        }
 
    for (int i = 0; i < 2 * N; i += 2)
        if (imply [i][i + 1])
            fputc ('N', out);
        else if (imply [i + 1][i])
            fputc ('Y', out);
        else
            fputc ('?', out);
 
    fputc ('\n', out);
    return 0;
}
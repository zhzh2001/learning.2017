#include<bits/stdc++.h>
using namespace std;
const int N=255,M=250005,MOD=1000000;
int a[N],f[M];
bool vis[M];
int main()
{
	int n;
	cin>>n;
	int sum=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		sum+=a[i];
	}
	f[0]=1;
	vis[0]=true;
	for(int i=1;i<=n;i++)
		for(int j=sum/2;j>=a[i];j--)
		{
			vis[j]|=vis[j-a[i]];
			f[j]=(f[j]+f[j-a[i]])%MOD;
		}
	for(int i=sum/2;i;i--)
		if(vis[i])
		{
			cout<<sum-i-i<<endl<<f[i]<<endl;
			break;
		}
	return 0;
}
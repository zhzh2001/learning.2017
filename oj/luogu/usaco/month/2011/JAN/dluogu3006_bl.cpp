#include<bits/stdc++.h>
using namespace std;
const int N=100005,K=10005;
int c[N],m[N];
vector<int> mat[N];
long long dfs(int k,int t)
{
	long long sum=c[k];
	for(auto v:mat[k])
		sum+=dfs(v,t);
	return min(sum,(long long)m[k]*t);
}
int main()
{
	int n,k;
	cin>>n>>k;
	m[1]=1e9;
	for(int i=2;i<=n;i++)
	{
		int p;
		cin>>p>>c[i]>>m[i];
		mat[p].push_back(i);
	}
	while(k--)
	{
		int t;
		cin>>t;
		cout<<dfs(1,t)<<endl;
	}
	return 0;
}
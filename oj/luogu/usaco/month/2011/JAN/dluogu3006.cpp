#include<bits/stdc++.h>
using namespace std;
const int N=100005,K=10005;
int m[N],f[N];
long long c[N],into[N],ans[N];
typedef pair<long long,int> pii;
pii t[K];
bool alive[N];
int getf(int x)
{
	if(alive[x])
		return x;
	return f[x]=getf(f[x]);
}
int main()
{
	int n,k;
	cin>>n>>k;
	fill(alive+1,alive+n+1,true);
	for(int i=2;i<=n;i++)
	{
		cin>>f[i]>>c[i]>>m[i];
		into[f[i]]+=m[i];
	}
	for(int i=1;i<=k;i++)
	{
		cin>>t[i].first;
		t[i].second=i;
	}
	sort(t+1,t+k+1);
	
	priority_queue<pii,vector<pii>,greater<pii> > Q;
	for(int i=1;i<=n;i++)
		if(m[i]>into[i])
			Q.push(make_pair(c[i]/(m[i]-into[i]),i));
	int now=1;
	while(!Q.empty())
	{
		pii h=Q.top();Q.pop();
		for(;now<=k&&t[now].first<=h.first;now++)
			ans[t[now].second]=c[1]+into[1]*t[now].first;
		
		if(!alive[h.second])
			continue;
		alive[h.second]=false;
		int r=getf(h.second);
		c[r]+=c[h.second];
		into[r]+=into[h.second]-m[h.second];
		
		if(m[r]>into[r])
			Q.push(make_pair(c[r]/(m[r]-into[r]),r));
	}
	for(;now<=k;now++)
		ans[t[now].second]=c[1]+into[1]*t[now].first;
	for(int i=1;i<=k;i++)
		cout<<ans[i]<<endl;
	return 0;
}
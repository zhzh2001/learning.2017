#include<bits/stdc++.h>
using namespace std;
const int N=305,INF=0x3f3f3f3f;
int mat[N][N],c[N],r[N],dur[N][2],d[N];
bool vis[N];
inline int getcolor(int u,int t)
{
	if(t<r[u])
		return c[u];
	t=(t-r[u])%(dur[u][0]+dur[u][1]);
	return t<dur[u][!c[u]]?!c[u]:c[u];
}
inline int waitfor(int u,int t)
{
	if(t<r[u])
		return r[u]-t;
	t=(t-r[u])%(dur[u][0]+dur[u][1]);
	return t<dur[u][!c[u]]?dur[u][!c[u]]-t:dur[u][0]+dur[u][1]-t;
}
int main()
{
	int s,t,n,m;
	cin>>s>>t>>n>>m;
	for(int i=1;i<=n;i++)
	{
		char color;
		cin>>color>>r[i]>>dur[i][0]>>dur[i][1];
		c[i]=color=='P';
	}
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		if(!j)
			break;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k])
			{
				int now=d[j];
				if(getcolor(j,d[j])!=getcolor(k,d[j]))
				{
					int waitj,waitk,cc=0;
					do
					{
						waitj=waitfor(j,now);
						waitk=waitfor(k,now);
						if(waitj==waitk)
							now+=waitj;
					}
					while(++cc<=3&&waitj==waitk);
					if(cc>3)
						continue;
					now+=min(waitj,waitk);
				}
				d[k]=min(d[k],mat[j][k]+now);
			}
	}
	if(d[t]==INF)
		cout<<0<<endl;
	else
		cout<<d[t]<<endl;
	return 0;
}
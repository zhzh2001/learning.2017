#include<bits/stdc++.h>
using namespace std;
const int N=25005,INF=0x3f3f3f3f;
typedef pair<int,int> pii;
vector<pii> roads[N],planes[N];
vector<int> comp[N];
int belong[N],indeg[N],d[N];
bool vis[N];
void dfs(int k,int num)
{
	belong[k]=num;
	comp[num].push_back(k);
	for(auto e:roads[k])
		if(!belong[e.first])
			dfs(e.first,num);
}
int main()
{
	int n,m,p,s;
	cin>>n>>m>>p>>s;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		roads[u].push_back(make_pair(v,w));
		roads[v].push_back(make_pair(u,w));
	}
	while(p--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		planes[u].push_back(make_pair(v,w));
	}
	int c=0;
	for(int i=1;i<=n;i++)
		if(!belong[i])
			dfs(i,++c);
	for(int i=1;i<=n;i++)
		for(auto e:planes[i])
			indeg[belong[e.first]]++;
	fill(d+1,d+n+1,INF);
	d[s]=0;
	queue<int> Q;
	for(int i=1;i<=c;i++)
		if(!indeg[i])
			Q.push(i);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		priority_queue<pii,vector<pii>,greater<pii>> PQ;
		for(auto i:comp[k])
			if(d[i]<INF)
				PQ.push(make_pair(d[i],i));
		while(!PQ.empty())
		{
			pii k=PQ.top();PQ.pop();
			if(vis[k.second])
				continue;
			vis[k.second]=true;
			for(auto e:roads[k.second])
				if(d[k.second]+e.second<d[e.first])
					PQ.push(make_pair(d[e.first]=d[k.second]+e.second,e.first));
			for(auto e:planes[k.second])
				d[e.first]=min(d[e.first],d[k.second]+e.second);
		}
		for(auto i:comp[k])
			for(auto e:planes[i])
				if(--indeg[belong[e.first]]==0)
					Q.push(belong[e.first]);
	}
	for(int i=1;i<=n;i++)
		if(d[i]==INF)
			cout<<"NO PATH\n";
		else
			cout<<d[i]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=25005,INF=0x3f3f3f3f;
vector<pair<int,int> > mat[N];
int d[N];
bool inQ[N];
int main()
{
	int n,m,p,s;
	cin>>n>>m>>p>>s;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	while(p--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
	}
	fill(d+1,d+n+1,INF);
	d[s]=0;
	deque<int> Q;
	Q.push_back(s);
	inQ[s]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop_front();
		inQ[k]=false;
		for(auto e:mat[k])
			if(d[k]+e.second<d[e.first])
			{
				d[e.first]=d[k]+e.second;
				if(!inQ[e.first])
				{
					if(!Q.empty()&&d[e.first]<d[Q.front()])
						Q.push_front(e.first);
					else
						Q.push_back(e.first);
					inQ[e.first]=true;
				}
			}
	}
	for(int i=1;i<=n;i++)
		if(d[i]==INF)
			cout<<"NO PATH\n";
		else
			cout<<d[i]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=2005;
vector<int> mat[N];
bool f[N][N];
void dfs(int k,int now)
{
	f[k][now]=true;
	for(auto v:mat[now])
		if(!f[k][v])
			dfs(k,v);
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int x,y;
		char vx,vy;
		cin>>x>>vx>>y>>vy;
		x--;y--;
		x=2*x+(vx=='Y');
		y=2*y+(vy=='Y');
		mat[x].push_back(y^1);
		mat[y].push_back(x^1);
	}
	for(int i=0;i<2*n;i++)
		dfs(i,i);
	for(int i=0;i<2*n;i+=2)
		if(f[i][i+1]&&f[i+1][i])
		{
			cout<<"IMPOSSIBLE\n";
			return 0;
		}
	for(int i=0;i<2*n;i+=2)
		if(f[i][i+1])
			cout.put('N');
		else
			if(f[i+1][i])
				cout.put('Y');
			else
				cout.put('?');
	return 0;
}
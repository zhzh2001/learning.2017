#include <iostream>
#include <algorithm>
using namespace std;
const int N = 11, M = 10005, INF = 0x3f3f3f3f;
int a[N], f[N][M];
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	f[0][0] = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			for (int k = 1; k * k <= j; k++)
				f[i][j] = min(f[i][j], f[i - 1][j - k * k] + (a[i] - k) * (a[i] - k));
	if (f[n][m] == INF)
		cout << -1 << endl;
	else
		cout << f[n][m] << endl;
	return 0;
}
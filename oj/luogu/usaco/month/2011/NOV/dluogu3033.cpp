#include<bits/stdc++.h>
using namespace std;
const int N=255;
struct node
{
	int l,r,v;
}a[N],b[N];
bool mat[N][N],vis[N];
int an,bn,match[N];
bool dfs(int k)
{
	for(int i=1;i<=bn;i++)
		if(mat[k][i]&&!vis[i])
		{
			vis[i]=true;
			if(!match[i]||dfs(match[i]))
			{
				match[i]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	int n;
	cin>>n;
	an=bn=0;
	for(int i=1;i<=n;i++)
	{
		int x1,y1,x2,y2;
		cin>>x1>>y1>>x2>>y2;
		if(x1==x2)
		{
			a[++an].l=min(y1,y2);
			a[an].r=max(y1,y2);
			a[an].v=x1;
		}
		else
		{
			b[++bn].l=min(x1,x2);
			b[bn].r=max(x1,x2);
			b[bn].v=y1;
		}
	}
	for(int i=1;i<=an;i++)
		for(int j=1;j<=bn;j++)
			mat[i][j]=a[i].l<=b[j].v&&a[i].r>=b[j].v&&b[j].l<=a[i].v&&b[j].r>=a[i].v;
	int ans=n;
	for(int i=1;i<=an;i++)
	{
		fill(vis+1,vis+bn+1,false);
		ans-=dfs(i);
	}
	cout<<ans<<endl;
	return 0;
}
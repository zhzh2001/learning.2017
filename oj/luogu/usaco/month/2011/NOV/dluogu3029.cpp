#include<bits/stdc++.h>
using namespace std;
const int N=50005;
struct cow
{
	int pos,id;
	bool operator<(const cow& rhs)const
	{
		return pos<rhs.pos;
	}
}a[N];
int id[N],cnt[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].pos>>a[i].id;
		id[i]=a[i].id;
	}
	sort(a+1,a+n+1);
	sort(id+1,id+n+1);
	int idn=unique(id+1,id+n+1)-id-1;
	for(int i=1;i<=n;i++)
		a[i].id=lower_bound(id+1,id+idn+1,a[i].id)-id;
	int ans=a[n].pos-a[1].pos,j=1,cc=0;
	for(int i=1;i<=n;i++)
		if(!cnt[a[i].id]++&&++cc==idn)
		{
			for(;cc==idn;j++)
				if(!--cnt[a[j].id])
					cc--;
			ans=min(ans,a[i].pos-a[j-1].pos);
		}
	cout<<ans<<endl;
	return 0;
}
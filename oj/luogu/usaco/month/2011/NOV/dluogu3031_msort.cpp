#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],b[N];
long long ans;
void merge_sort(int l,int r)
{
	if(l==r)
		return;
	int mid=(l+r)/2;
	merge_sort(l,mid);
	merge_sort(mid+1,r);
	int i=l,j=mid+1,p=1;
	while(i<=mid&&j<=r)
		if(a[i]<=a[j])
		{
			ans+=r-j+1;
			b[p++]=a[i++];
		}
		else
			b[p++]=a[j++];
	while(i<=mid)
		b[p++]=a[i++];
	while(j<=r)
		b[p++]=a[j++];
	memcpy(a+l,b+1,(r-l+1)*sizeof(int));
}
int main()
{
	int n,k;
	cin>>n>>k;
	a[0]=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		if(x>=k)
			a[i]=a[i-1]+1;
		else
			a[i]=a[i-1]-1;
	}
	merge_sort(0,n);
	cout<<ans<<endl;
	return 0;
}
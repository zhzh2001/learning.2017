#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int n,a[N];
inline void modify(int x,int val)
{
	for(;x<=2*n+1;x+=x&-x)
		a[x]+=val;
}
inline int query(int x)
{
	int ans=0;
	for(;x;x-=x&-x)
		ans+=a[x];
	return ans;
}
int main()
{
	int x;
	cin>>n>>x;
	int sum=0;
	modify(sum+n+1,1);
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		int val;
		cin>>val;
		if(val>=x)
			sum++;
		else
			sum--;
		ans+=query(sum+n+1);
		modify(sum+n+1,1);
	}
	cout<<ans<<endl;
	return 0;
}
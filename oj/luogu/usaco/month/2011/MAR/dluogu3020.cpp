#include<bits/stdc++.h>
using namespace std;
const int N=50005,INF=0x3f3f3f3f;
typedef pair<int,int> pii;
vector<pii> mat[N];
int d[N];
bool vis[N];
void dijkstra(int n)
{
	fill(d+1,d+n+1,INF);
	d[1]=0;
	fill(vis+1,vis+n+1,false);
	priority_queue<pii,vector<pii>,greater<pii> > Q;
	Q.push(make_pair(0,1));
	while(!Q.empty())
	{
		pii k=Q.top();Q.pop();
		if(vis[k.second])
			continue;
		vis[k.second]=true;
		for(auto e:mat[k.second])
			if(!vis[e.first]&&d[k.second]+e.second<d[e.first])
			{
				d[e.first]=d[k.second]+e.second;
				Q.push(make_pair(d[e.first],e.first));
			}
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	dijkstra(n);
	cout<<d[n]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005;
pair<int,int> p[N],cross[N*N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>p[i].first>>p[i].second;
	int cc=0;
	for(int i=1;i<=n;i++)
		for(int j=i+1;j<=n;j++)
			cross[++cc]=make_pair(p[i].first+p[j].first,p[i].second+p[j].second);
	sort(cross+1,cross+cc+1);
	int ans=0,pred=0;
	for(int i=1;i<=cc;i++)
		if(cross[i]!=cross[i+1])
		{
			ans+=(i-pred)*(i-pred-1)/2;
			pred=i;
		}
	cout<<ans<<endl;
	return 0;
}
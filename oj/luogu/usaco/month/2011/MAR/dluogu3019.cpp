#include<bits/stdc++.h>
using namespace std;
const int N=1005,LOGN=11;
int dep[N],f[N][LOGN];
vector<int> mat[N];
void dfs(int k)
{
	for(auto v:mat[k])
		if(!f[v][0])
		{
			f[v][0]=k;
			dep[v]=dep[k]+1;
			dfs(v);
		}
}
int lca(int x,int y)
{
	if(dep[x]<dep[y])
		swap(x,y);
	int delta=dep[x]-dep[y];
	for(int i=0;delta;i++,delta/=2)
		if(delta&1)
			x=f[x][i];
	if(x==y)
		return x;
	for(int i=LOGN-1;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=2;i<=n;i++)
	{
		int p;
		cin>>p;
		mat[p].push_back(i);
	}
	f[1][0]=1;
	dfs(1);
	for(int j=1;j<LOGN;j++)
		for(int i=1;i<=n;i++)
			f[i][j]=f[f[i][j-1]][j-1];
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		cout<<lca(u,v)<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,head[N],v[N],nxt[N],e,p[N],cost[N];
long long ans,cnt[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	long long t=0;
	for(int i=head[k];i;i=nxt[i])
	{
		dfs(v[i]);
		cost[k]=min(cost[k],cost[v[i]]);
		t+=cnt[v[i]];
	}
	if(t<cnt[k])
		ans+=(long long)cost[k]*(cnt[k]-t);
	else
		cnt[k]=t;
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>p[i]>>cnt[i]>>cost[i];
		if(~p[i])
			add_edge(p[i],i);
	}
	dfs(1);
	cout<<ans<<endl;
	return 0;
}
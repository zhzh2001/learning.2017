#include<bits/stdc++.h>
using namespace std;
const int N=40005,M=100005,MOD=1e9+7;
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[M];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
		cin>>e[i].u>>e[i].v>>e[i].w;
	sort(e+1,e+m+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	long long ans=0;
	unsigned cnt=1;
	for(int i=1;i<=m;)
	{
		int j,valid=0;
		set<pair<int,int> > S;
		for(j=i;j<=m&&e[j].w==e[i].w;j++)
		{
			int ru=getf(e[j].u),rv=getf(e[j].v);
			if(ru>rv)
				swap(ru,rv);
			if(ru!=rv)
			{
				S.insert(make_pair(ru,rv));
				valid++;
			}
		}
		assert(j-i<=3);
		int need=0;
		for(;i<j;i++)
		{
			int ru=getf(e[i].u),rv=getf(e[i].v);
			if(ru!=rv)
			{
				f[ru]=rv;
				need++;
				ans+=e[i].w;
			}
		}
		if(valid==3)
		{
			if(need==1||(need==2&&S.size()==3))
				cnt=cnt*3%MOD;
			if(need==2&&S.size()==2)
				cnt=cnt*2%MOD;
		}
		if(valid==2&&need==1)
			cnt=cnt*2%MOD;
	}
	cout<<ans<<' '<<cnt<<endl;
	return 0;
}
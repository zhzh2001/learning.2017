#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=200005;
int head[N],v[M],nxt[M],e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int sz[N],dep[N],f[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k])
		{
			dep[v[i]]=dep[k]+1;
			f[v[i]]=k;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int id[N],top[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(v[i]!=f[k]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
int n;
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
void modify(int u,int v)
{
	int a=top[u],b=top[v];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(u,v);
		}
		T.modify(id[a],1);
		T.modify(id[u]+1,-1);
		u=f[a];
		a=top[u];
	}
	if(id[u]>id[v])
		swap(u,v);
	T.modify(id[u]+1,1);
	T.modify(id[v]+1,-1);
}
int query(int u,int v)
{
	if(f[u]==v)
		swap(u,v);
	return T.query(id[v]);
}
int main()
{
	int m;
	cin>>n>>m;
	for(int i=1;i<n;i++)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		add_edge(v,u);
	}
	dfs(1);
	dfs(1,1);
	while(m--)
	{
		char opt;
		int u,v;
		cin>>opt>>u>>v;
		if(opt=='P')
			modify(u,v);
		else
			cout<<query(u,v)<<endl;
	}
	return 0;
}
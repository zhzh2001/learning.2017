#include<bits/stdc++.h>
using namespace std;
const int N=20005;
int a[N];
unordered_map<int,int> pos[5];
bool cmp(int x,int y)
{
	int cnt[2]={0,0};
	for(int i=0;i<5;i++)
		cnt[pos[i][x]<pos[i][y]]++;
	return cnt[0]<cnt[1];
}
int main()
{
	int n;
	cin>>n;
	for(int i=0;i<5;i++)
		for(int j=1;j<=n;j++)
		{
			int x;
			cin>>x;
			if(i==0)
				a[j]=x;
			pos[i][x]=j;
		}
	sort(a+1,a+n+1,cmp);
	for(int i=1;i<=n;i++)
		cout<<a[i]<<endl;
	return 0;
}
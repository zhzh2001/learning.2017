#include<bits/stdc++.h>
using namespace std;
const int N=5005,M=100005,INF=0x3f3f3f3f;
int x[N],c[M],f[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>x[i];
	sort(x+1,x+n+1);
	for(int i=1;i<=m;i++)
		cin>>c[i];
	for(int i=m-1;i;i--)
		c[i]=min(c[i],c[i+1]);
	for(int i=1;i<=n;i++)
	{
		f[i]=INF;
		for(int j=1;j<=i;j++)
			f[i]=min(f[i],f[j-1]+c[x[i]-x[j]+1]);
	}
	cout<<f[n]<<endl;
	return 0;
}
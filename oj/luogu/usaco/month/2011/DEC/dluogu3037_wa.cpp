#include<bits/stdc++.h>
using namespace std;
const int N=40005,INF=0x3f3f3f3f,MOD=1e9+7;
typedef pair<int,int> pii;
vector<pii> mat[N];
int d[N],cnt[N];
bool vis[N];
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	fill(d+1,d+n+1,INF);
	d[1]=0;
	long long ans=0;
	priority_queue<pii,vector<pii>,greater<pii> > Q;
	Q.push(make_pair(0,1));
	cnt[1]=1;
	while(!Q.empty())
	{
		pii k=Q.top();Q.pop();
		if(vis[k.second])
			continue;
		vis[k.second]=true;
		ans+=d[k.second];
		for(auto e:mat[k.second])
			if(!vis[e.first])
				if(e.second<d[e.first])
				{
					d[e.first]=e.second;
					Q.push(make_pair(e.second,e.first));
					cnt[e.first]=cnt[k.second];
				}
				else
					if(e.second==d[e.first])
						cnt[e.first]=(cnt[e.first]+cnt[k.second])%MOD;
	}
	cout<<ans<<endl;
	return 0;
}
#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 1000005;
int f[N];
bool dp(int n)
{
	if (!n)
		return false;
	if (~f[n])
		return f[n];
	f[n] = 0;
	int x = n, v1 = 10, v2 = 0;
	do
		if (x % 10)
		{
			v1 = min(v1, x % 10);
			v2 = max(v2, x % 10);
		}
	while (x /= 10);
	if (!dp(n - v1) || !dp(n - v2))
		f[n] = 1;
	return f[n];
}
int main()
{
	memset(f, -1, sizeof(f));
	int t;
	cin >> t;
	while (t--)
	{
		int n;
		cin >> n;
		if (dp(n))
			cout << "YES\n";
		else
			cout << "NO\n";
	}
	return 0;
}
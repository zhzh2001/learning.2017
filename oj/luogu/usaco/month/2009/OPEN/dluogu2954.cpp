#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1505, INF = 0x3f3f3f3f;
int a[N], f[N][N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	int d = (m - 1) / (n - 1);
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	sort(a + 1, a + n + 1);
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	f[1][0] = a[1] - 1;
	for (int i = 2; i <= n; i++)
		for (int j = 0; j < i; j++)
		{
			f[i][j] = f[i - 1][j];
			if (j)
				f[i][j] = min(f[i][j], f[i - 1][j - 1]);
			f[i][j] += abs(a[i] - ((i - 1) * d + j + 1));
		}
	cout << f[n][(m - 1) % (n - 1)] << endl;
	return 0;
}
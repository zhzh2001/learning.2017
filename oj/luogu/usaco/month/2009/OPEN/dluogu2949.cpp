#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct job
{
	int d,p;
	bool operator<(const job& rhs)const
	{
		return d>rhs.d;
	}
}a[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i].d>>a[i].p;
	sort(a+1,a+n+1);
	priority_queue<int> Q;
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		Q.push(a[i].p);
		if(a[i+1].d!=a[i].d)
			for(int j=a[i+1].d;j<a[i].d&&!Q.empty();j++)
			{
				ans+=Q.top();Q.pop();
			}
	}
	cout<<ans<<endl;
	return 0;
}
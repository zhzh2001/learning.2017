#include<bits/stdc++.h>
using namespace std;
const int n=52,INF=0x3f3f3f3f;
typedef tuple<int,int,int> edge;
vector<edge> E;
int head[n],H[n];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(make_tuple(v,cap,head[u]));
	E.push_back(make_tuple(u,0,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int dep[n];
bool bfs()
{
	fill(dep,dep+n,0);
	dep[0]=1;
	queue<int> Q;
	Q.push(0);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==25)
			return true;
		for(int i=head[k];~i;i=get<2>(E[i]))
			if(get<1>(E[i])&&!dep[get<0>(E[i])])
			{
				dep[get<0>(E[i])]=dep[k]+1;
				Q.push(get<0>(E[i]));
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==25||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=get<2>(E[i]))
		if(dep[get<0>(E[i])]==dep[k]+1&&(t=dfs(get<0>(E[i]),min(pred,get<1>(E[i])))))
		{
			get<1>(E[i])-=t;
			get<1>(E[i^1])+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	int m;
	cin>>m;
	fill(head,head+n,-1);
	while(m--)
	{
		char cu,cv;
		int cap;
		cin>>cu>>cv>>cap;
		int u,v;
		if(isupper(cu))
			u=cu-'A';
		else
			u=cu-'a'+26;
		if(isupper(cv))
			v=cv-'A';
		else
			v=cv-'a'+26;
		add_edge(u,v,cap);
	}
	int flow=0;
	while(bfs())
	{
		copy(head,head+n,H);
		flow+=dfs(0,INF);
	}
	cout<<flow<<endl;
	return 0;
}
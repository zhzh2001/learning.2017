#include<bits/stdc++.h>
using namespace std;
const int N=30005;
vector<int> mat[N];
bool vis[N];
int main()
{
	int n,m,k;
	cin>>n>>m>>k;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	while(k--)
	{
		int u;
		cin>>u;
		vis[u]=true;
		for(auto v:mat[u])
			vis[v]=true;
	}
	queue<int> Q;
	Q.push(1);
	vis[1]=true;
	int ans=0;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		ans++;
		for(auto v:mat[k])
			if(!vis[v])
			{
				vis[v]=true;
				Q.push(v);
			}
	}
	cout<<n-ans<<endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 105, dx[] = {-1, 1, 0, 0}, dy[] = {0, 0, -1, 1}, INF = 0x3f3f3f3f;
bool mat[N][N];
int d[N][N][4];
struct state
{
	int x, y, dir;
};
int main()
{
	int n, m;
	cin >> m >> n;
	int sx = 0, sy, tx, ty;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			char c;
			cin >> c;
			if (c == 'C')
				if (!sx)
				{
					sx = i;
					sy = j;
				}
				else
				{
					tx = i;
					ty = j;
				}
			mat[i][j] = c == '*';
		}
	fill_n(&d[0][0][0], sizeof(d) / sizeof(int), INF);
	queue<state> Q;
	for (int i = 0; i < 4; i++)
	{
		int x = sx, y = sy;
		while (x && x <= n && y && y <= m && !mat[x][y])
		{
			d[x][y][i] = 0;
			Q.push({x, y, i});
			x += dx[i];
			y += dy[i];
		}
	}
	while (!Q.empty())
	{
		state k = Q.front();
		Q.pop();
		if (k.x == tx && k.y == ty)
		{
			cout << d[k.x][k.y][k.dir] << endl;
			return 0;
		}
		for (int i = 0; i < 4; i++)
		{
			int nx = k.x + dx[i], ny = k.y + dy[i];
			while (nx && nx <= n && ny && ny <= m && !mat[nx][ny] && d[nx][ny][i] == INF)
			{
				d[nx][ny][i] = d[k.x][k.y][k.dir] + 1;
				Q.push({nx, ny, i});
				nx += dx[i];
				ny += dy[i];
			}
		}
	}
	cout << -1 << endl;
	return 0;
}
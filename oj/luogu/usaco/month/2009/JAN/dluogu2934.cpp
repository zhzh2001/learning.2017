#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=400005,INF=0x3f3f3f3f;
int head[N],v[M],w[M],nxt[M],e;
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
int d[N],pred[N];
bool vis[N],onp[M];
struct hnode
{
	int v,w;
	hnode(int v,int w):v(v),w(w){}
	bool operator>(const hnode& rhs)const
	{
		return w>rhs.w;
	}
};
void dijkstra(int s)
{
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	priority_queue<hnode,vector<hnode>,greater<hnode> > Q;
	Q.push(hnode(s,0));
	while(!Q.empty())
	{
		hnode k=Q.top();Q.pop();
		if(!vis[k.v])
		{
			vis[k.v]=true;
			for(int i=head[k.v];i;i=nxt[i])
				if(!vis[v[i]]&&d[k.v]+w[i]<d[v[i]])
				{
					d[v[i]]=d[k.v]+w[i];
					onp[pred[v[i]]]=false;
					pred[v[i]]=i;
					onp[i]=true;
					Q.push(hnode(v[i],d[v[i]]));
				}
		}
	}
}
int sz[N],dep[N],f[N],son[N];
void dfs(int k)
{
	sz[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(onp[i])
		{
			dep[v[i]]=dep[k]+1;
			f[v[i]]=k;
			dfs(v[i]);
			sz[k]+=sz[v[i]];
			if(sz[v[i]]>sz[son[k]])
				son[k]=v[i];
		}
}
int top[N],id[N],now;
void dfs(int k,int anc)
{
	top[k]=anc;
	id[k]=++now;
	if(son[k])
		dfs(son[k],anc);
	for(int i=head[k];i;i=nxt[i])
		if(onp[i]&&v[i]!=son[k])
			dfs(v[i],v[i]);
}
//segment tree
struct node
{
	int min,lazy;
}tree[N*4];
inline void pushdown(int id)
{
	if(tree[id].lazy<INF)
	{
		tree[id*2].min=min(tree[id*2].min,tree[id].lazy);
		tree[id*2].lazy=min(tree[id*2].lazy,tree[id].lazy);
		tree[id*2+1].min=min(tree[id*2+1].min,tree[id].lazy);
		tree[id*2+1].lazy=min(tree[id*2+1].lazy,tree[id].lazy);
		tree[id].lazy=INF;
	}
}
void build(int id,int l,int r)
{
	tree[id].min=tree[id].lazy=INF;
	if(l<r)
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
	}
}
int ql,qr,val;
void modify(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
	{
		tree[id].lazy=min(tree[id].lazy,val);
		if(l==r)
			tree[id].min=min(tree[id].min,val);
	}
	else
	{
		if(l<r)
			pushdown(id);
		int mid=(l+r)/2;
		if(ql<=mid)
			modify(id*2,l,mid);
		if(qr>mid)
			modify(id*2+1,mid+1,r);
	}
}
void query(int id,int l,int r)
{
	if(ql<=l&&qr>=r)
		val=min(val,tree[id].min);
	else
	{
		if(l<r)
			pushdown(id);
		int mid=(l+r)/2;
		if(ql<=mid)
			query(id*2,l,mid);
		if(qr>mid)
			query(id*2+1,mid+1,r);
	}
}
int lca(int x,int y)
{
	int a=top[x],b=top[y];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(x,y);
		}
		x=f[a];
		a=top[x];
	}
	if(dep[x]<dep[y])
		return x;
	return y;
}
int n;
void modifyChain(int u,int v,int z)
{
	int a=top[u],b=top[v];
	while(a!=b)
	{
		if(dep[a]<dep[b])
		{
			swap(a,b);
			swap(u,v);
		}
		ql=id[a];qr=id[u];
		val=z;
		modify(1,1,n);
		u=f[a];
		a=top[u];
	}
	ql=id[u];qr=id[v];
	if(ql>qr)
		swap(ql,qr);
	ql++;
	val=z;
	modify(1,1,n);
}
int main()
{
	int m;
	cin>>n>>m;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	dijkstra(1);
	dfs(1);
	dfs(1,1);
	build(1,1,n);
	for(int i=1;i<=n;i++)
		for(int j=head[i];j;j=nxt[j])
			if(!onp[j])
			{
				int a=lca(i,v[j]);
				modifyChain(v[j],a,d[i]+w[j]+d[v[j]]);
			}
	for(int i=2;i<=n;i++)
	{
		ql=qr=id[i];
		val=INF;
		query(1,1,n);
		if(val==INF)
			cout<<-1<<endl;
		else
			cout<<val-d[i]<<endl;
	}
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 50005;
typedef pair<int, int> pii;
pii a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i].first >> a[i].second;
	sort(a + 1, a + n + 1, [](const pii &a, const pii &b) { return a.second < b.second; });
	int now = 0, ans = 0;
	for (int i = 1; i <= n; i++)
		if (a[i].first >= now)
		{
			now = a[i].second;
			ans++;
		}
	cout << ans << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=225;
int mat[N][N],d[N],cnt[N];
bool inQ[N];
int main()
{
	int D,m,n,f,s;
	cin>>D>>m>>n>>f>>s;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=-D;
	}
	while(f--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=w-D;
	}
	memset(d,0x3f,sizeof(d));
	d[s]=-D;
	queue<int> Q;
	Q.push(s);
	inQ[s]=true;
	cnt[s]=1;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(int i=1;i<=n;i++)
			if(d[k]+mat[k][i]<d[i])
			{
				d[i]=d[k]+mat[k][i];
				if(!inQ[i])
				{
					Q.push(i);
					inQ[i]=true;
					if(++cnt[i]>n)
					{
						cout<<-1<<endl;
						return 0;
					}
				}
			}
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=min(ans,d[i]);
	cout<<-ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=200,dx[]={1,0,1,1},dy[]={0,1,1,-1};
int a[N][N];
int main()
{
	int n;
	cin>>n;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			cin>>a[i][j];
	int ans=a[0][0];
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			for(int k=0;k<4;k++)
			{
				int now=0,x=i,y=j;
				for(int t=1;t<=n;t++)
				{
					if(now<0)
						now=0;
					now+=a[x][y];
					ans=max(ans,now);
					x=(x+dx[k]+n)%n;y=(y+dy[k]+n)%n;
				}
			}
	cout<<ans<<endl;
	return 0;
}
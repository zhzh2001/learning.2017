#include<bits/stdc++.h>
using namespace std;
const int K=50005;
struct group
{
	int s,t,cnt;
	bool operator<(const group& rhs)const
	{
		return s<rhs.s;
	}
}a[K];
int main()
{
	int k,n,c;
	cin>>k>>n>>c;
	for(int i=1;i<=k;i++)
		cin>>a[i].s>>a[i].t>>a[i].cnt;
	sort(a+1,a+k+1);
	map<int,int> left;
	int ans=0;
	for(int i=1;i<=k;i++)
	{
		for(map<int,int>::iterator it=left.begin();it!=left.end();)
			if(it->first<=a[i].s)
			{
				c+=it->second;
				left.erase(it++);
			}
			else
				break;
		left[a[i].t]+=a[i].cnt;
		ans+=a[i].cnt;
		c-=a[i].cnt;
		map<int,int>::iterator rit=left.end();
		for(--rit;c<0;--rit)
			if(c+rit->second<=0)
			{
				c+=rit->second;
				ans-=rit->second;
				left.erase(rit);
			}
			else
			{
				rit->second+=c;
				ans+=c;
				c=0;
			}
	}
	cout<<ans<<endl;
	return 0;
}
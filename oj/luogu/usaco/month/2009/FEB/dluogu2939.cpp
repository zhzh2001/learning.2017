#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=100005,K=21;
int head[N],v[M],w[M],nxt[M],e;
long long d[N][K];
bool inQ[N][K];
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int n,m,K;
	cin>>n>>m>>K;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	memset(d,0x3f,sizeof(d));
	d[1][0]=0;
	typedef pair<int,int> pii;
	queue<pii> Q;
	Q.push(make_pair(1,0));
	inQ[1][0]=true;
	while(!Q.empty())
	{
		pii k=Q.front();Q.pop();
		inQ[k.first][k.second]=false;
		for(int i=head[k.first];i;i=nxt[i])
		{
			if(d[k.first][k.second]+w[i]<d[v[i]][k.second])
			{
				d[v[i]][k.second]=d[k.first][k.second]+w[i];
				if(!inQ[v[i]][k.second])
				{
					Q.push(make_pair(v[i],k.second));
					inQ[v[i]][k.second]=true;
				}
			}
			if(k.second<K&&d[k.first][k.second]<d[v[i]][k.second+1])
			{
				d[v[i]][k.second+1]=d[k.first][k.second];
				if(!inQ[v[i]][k.second+1])
				{
					Q.push(make_pair(v[i],k.second+1));
					inQ[v[i]][k.second+1]=true;
				}
			}
		}
	}
	long long ans=d[0][0];
	for(int i=0;i<=K;i++)
		ans=min(ans,d[n][i]);
	cout<<ans<<endl;
	return 0;
}
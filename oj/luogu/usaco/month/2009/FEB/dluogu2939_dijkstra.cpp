#include<bits/stdc++.h>
using namespace std;
const int N=10005,M=100005,K=21;
int head[N],v[M],w[M],nxt[M],e;
long long d[N][K];
bool vis[N][K];
struct node
{
	int v,k,w;
	node(int v,int k,int w):v(v),k(k),w(w){}
	bool operator>(const node& rhs)const
	{
		return w>rhs.w;
	}
};
inline void add_edge(int u,int v,int w)
{
	::v[++e]=v;
	::w[e]=w;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int n,m,K;
	cin>>n>>m>>K;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		add_edge(u,v,w);
		add_edge(v,u,w);
	}
	memset(d,0x3f,sizeof(d));
	d[1][0]=0;
	priority_queue<node,vector<node>,greater<node> > Q;
	Q.push(node(1,0,0));
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(!vis[k.v][k.k])
		{
			vis[k.v][k.k]=true;
			for(int i=head[k.v];i;i=nxt[i])
			{
				if(!vis[v[i]][k.k]&&d[k.v][k.k]+w[i]<d[v[i]][k.k])
				{
					d[v[i]][k.k]=d[k.v][k.k]+w[i];
					Q.push(node(v[i],k.k,d[v[i]][k.k]));
				}
				if(k.k<K&&!vis[v[i]][k.k+1]&&d[k.v][k.k]<d[v[i]][k.k+1])
				{
					d[v[i]][k.k+1]=d[k.v][k.k];
					Q.push(node(v[i],k.k+1,d[v[i]][k.k+1]));
				}
			}
		}
	}
	long long ans=d[0][0];
	for(int i=0;i<=K;i++)
		ans=min(ans,d[n][i]);
	cout<<ans<<endl;
	return 0;
}
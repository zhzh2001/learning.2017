#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct cow
{
	int h,id;
	bool operator<(const cow& rhs)const
	{
		if(h==rhs.h)
			return id<rhs.id;
		return h>rhs.h;
	}
}c[N];
int ans[N];
int main()
{
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		scanf("%d",&c[i].h);
		c[i].id=i;
	}
	sort(c+1,c+n+1);
	set<int> S;
	for(int i=1;i<=n;i++)
	{
		set<int>::iterator it=S.insert(c[i].id).first;
		if(++it!=S.end())
			ans[c[i].id]=*it;
	}
	for(int i=1;i<=n;i++)
		printf("%d\n",ans[i]);
	return 0;
}
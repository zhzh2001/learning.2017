#include<bits/stdc++.h>
using namespace std;
const int N=4000005;
long long q[N];
int main()
{
	int c,n,a1,b1,d1,a2,b2,d2;
	cin>>c>>n>>a1>>b1>>d1>>a2>>b2>>d2;
	int r=0;
	q[++r]=c;
	int f1=1,f2=1;
	while(r<n)
	{
		long long v1=a1*q[f1]/d1+b1,v2=a2*q[f2]/d2+b2;
		if(v1<v2)
		{
			if(v1!=q[r])
				q[++r]=v1;
			f1++;
		}
		else
		{
			if(v2!=q[r])
				q[++r]=v2;
			f2++;
		}
	}
	cout<<q[n]<<endl;
	return 0;
}
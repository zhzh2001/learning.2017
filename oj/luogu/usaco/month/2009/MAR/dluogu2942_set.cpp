#include<bits/stdc++.h>
using namespace std;
int main()
{
	int c,n,a1,b1,d1,a2,b2,d2;
	cin>>c>>n>>a1>>b1>>d1>>a2>>b2>>d2;
	set<long long> S;
	S.insert(c);
	for(int i=1;i<n;i++)
	{
		int k=*S.begin();
		S.erase(S.begin());
		long long x=1ll*a1*k/d1+b1;
		if(1.0*a1*k/d1+b1<=numeric_limits<long long>::max()&&(i+S.size()<=n||x<*S.rbegin()))
		{
			S.insert(x);
			if(S.size()>n)
				S.erase(--S.end());
		}
		x=1ll*a2*k/d2+b2;
		if(1.0*a2*k/d2+b2<=numeric_limits<long long>::max()&&(i+S.size()<=n||x<*S.rbegin()))
		{
			S.insert(x);
			if(S.size()>n)
				S.erase(--S.end());
		}
	}
	cout<<*S.begin()<<endl;
	return 0;
}
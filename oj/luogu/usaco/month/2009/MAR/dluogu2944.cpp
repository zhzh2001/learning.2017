#include<bits/stdc++.h>
using namespace std;
const int N=6005;
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> E;
int head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(edge(v,cap,head[u]));
	E.push_back(edge(u,0,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int t,dep[N];
bool bfs()
{
	fill(dep+1,dep+t+1,0);
	dep[1]=1;
	queue<int> Q;
	Q.push(1);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];~i;i=E[i].nxt)
			if(E[i].cap&&!dep[E[i].v])
			{
				dep[E[i].v]=dep[k]+1;
				Q.push(E[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=E[i].nxt)
		if(dep[E[i].v]==dep[k]+1&&(t=dfs(E[i].v,min(pred,E[i].cap))))
		{
			E[i].cap-=t;
			E[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	int n,m,k;
	cin>>n>>m>>k;
	t=2*n+1;
	fill(head+1,head+t+1,-1);
	for(int i=1;i<=n;i++)
		add_edge(n+i,i,1);
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,n+v,1);
		add_edge(v,n+u,1);
	}
	while(k--)
	{
		int u;
		cin>>u;
		add_edge(n+u,t,n);
	}
	int flow=0;
	while(bfs())
	{
		copy(head+1,head+t+1,H+1);
		flow+=dfs(1,n);
	}
	cout<<flow<<endl;
	return 0;
}
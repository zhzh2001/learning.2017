#include<bits/stdc++.h>
using namespace std;
const int N=30005;
struct node
{
	string s;
	int id;
	node(){}
	node(const string& s,int id):s(s),id(id){}
	bool operator<(const node& rhs)const
	{
		if(s==rhs.s)
			return id<rhs.id;
		return s<rhs.s;
	}
}a[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].s;
		a[i].id=i;
	}
	sort(a+1,a+n+1);
	while(m--)
	{
		int k;
		string s;
		cin>>k>>s;
		int p=lower_bound(a+1,a+n+1,node(s,0))-a;
		if(p+k-1<=n&&a[p+k-1].s.substr(0,s.length())==s)
			cout<<a[p+k-1].id<<endl;
		else
			cout<<-1<<endl;
	}
	return 0;
}
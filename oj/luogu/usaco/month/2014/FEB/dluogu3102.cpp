#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 100, MOD = 2014;
char s[N + 5];
int f[N][N];
int dp(int i, int j)
{
	if (j - i == 1)
		return 1;
	int &now = f[i][j];
	if (~now)
		return now;
	now = 1;
	for (int k = 1; k * 2 < j - i + 1; k++)
	{
		if (!strncmp(s + i, s + i + k, k))
			now = (now + dp(i + k, j)) % MOD;
		if (!strncmp(s + i, s + j - k + 1, k))
			now = (now + dp(i + k, j)) % MOD;
		if (!strncmp(s + j - k + 1, s + i, k))
			now = (now + dp(i, j - k)) % MOD;
		if (!strncmp(s + j - k + 1, s + j - 2 * k + 1, k))
			now = (now + dp(i, j - k)) % MOD;
	}
	return now;
}
int main()
{
	cin >> s;
	fill_n(&f[0][0], sizeof(f) / sizeof(int), -1);
	cout << dp(0, strlen(s) - 1) - 1 << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=105;
int n,m,mat[N][N],d[N],from[N];
bool vis[N];
int dijkstra(bool path)
{
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	if(path)
		memset(from,0,sizeof(from));
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[j]+mat[j][k]<d[k])
			{
				d[k]=d[j]+mat[j][k];
				if(path)
					from[k]=j;
			}
	}
	return d[n];
}
int main()
{
	cin>>n>>m;
	memset(mat,0x3f,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	int dist=dijkstra(true),ans=0;
	for(int u=n,v=from[u];v;u=v,v=from[u])
	{
		mat[u][v]*=2;
		mat[v][u]*=2;
		ans=max(ans,dijkstra(false)-dist);
		mat[u][v]/=2;
		mat[v][u]/=2;
	}
	cout<<ans<<endl;
	return 0;
}
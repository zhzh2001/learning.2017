#include<bits/stdc++.h>
using namespace std;
const int N=100005;
const double eps=1e-4;
int n,a[N];
double calc(double val)
{
	double ans=-1e10,now=0;
	for(int i=2;i<n;i++)
	{
		if(now<0)
			now=0;
		ans=max(ans,now+=a[i]-val);
	}
	return ans;
}
int main()
{
	cin>>n;
	int sum=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		sum+=a[i];
	}
	double l=0,r=sum;
	while(r-l>eps)
	{
		double mid=(l+r)/2;
		if(sum-calc(mid)<=mid*n)
			r=mid;
		else
			l=mid;
	}
	cout.precision(3);
	cout<<fixed<<r<<endl;
	return 0;
}
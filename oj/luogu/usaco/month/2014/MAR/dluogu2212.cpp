#include<bits/stdc++.h>
using namespace std;
const int N=2005;
struct point
{
	int x,y;
}a[N];
int d[N];
bool vis[N];
inline int sqr(int x)
{
	return x*x;
}
int main()
{
	int n,c;
	cin>>n>>c;
	for(int i=1;i<=n;i++)
		cin>>a[i].x>>a[i].y;
	memset(d,0x3f,sizeof(d));
	d[1]=0;
	unsigned ans=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		if(!j)
		{
			cout<<-1<<endl;
			return 0;
		}
		vis[j]=true;
		ans+=d[j];
		for(int k=1;k<=n;k++)
			if(!vis[k])
			{
				int nowd=sqr(a[j].x-a[k].x)+sqr(a[j].y-a[k].y);
				if(nowd>=c)
					d[k]=min(d[k],nowd);
			}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int V=1e5,INF=0x3f3f3f3f;
int f[V+5];
int main()
{
	int n,b;
	cin>>n>>b;
	memset(f,0x3f,sizeof(f));
	f[0]=0;
	for(int i=1;i<=b;i++)
	{
		int v;
		cin>>v;
		for(int j=v;j<=V;j++)
			f[j]=min(f[j],f[j-v]+1);
	}
	int pred=0,ans=0;
	for(int i=1;i<=n;i++)
	{
		int now;
		cin>>now;
		int t=now;
		if(pred)
			now-=pred-1;
		if(f[now]==INF)
		{
			cout<<-1<<endl;
			return 0;
		}
		ans+=f[now];
		pred=t;
	}
	cout<<ans<<endl;
	return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 505;
int a[N], tmp[N];
vector<int> bucket[N];
bool ans[N];
int main()
{
	int n;
	cin >> n;
	int sum = 0;
	for (int i = 1; i <= n + 1; i++)
	{
		cin >> a[i];
		sum += a[i];
	}
	for (int i = 1; i <= n + 1; i++)
		if ((sum - a[i]) % 2 == 0)
		{
			ans[i] = true;
			int cnt = 0;
			for (int j = 1; j <= n + 1; j++)
				if (j != i)
					tmp[++cnt] = a[j];
			for (;;)
			{
				for (int j = 0; j <= n; j++)
					bucket[j].clear();
				for (int j = 1; j <= n; j++)
					bucket[tmp[j]].push_back(j);
				int j = n;
				for (; !bucket[j].size(); j--)
					;
				if (!j)
					break;
				int id = bucket[j].back();
				bucket[j].pop_back();
				for (int k = 1; k <= tmp[id]; k++)
				{
					for (; !bucket[j].size(); j--)
						;
					if (!j)
					{
						ans[i] = false;
						break;
					}
					tmp[bucket[j].back()]--;
					bucket[j].pop_back();
				}
				tmp[id] = 0;
			}
		}
	cout << count(ans + 1, ans + n + 2, true) << endl;
	for (int i = 1; i <= n + 1; i++)
		if (ans[i])
			cout << i << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 405;
int a[N][N];
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			cin >> a[i][j];
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		int now = 0;
		for (int x = 1; x <= n; x++)
			for (int y = 1; y <= n; y++)
				if (abs(x - i) + abs(y - 1) <= k)
					now += a[x][y];
		ans = max(ans, now);
		for (int j = 2; j <= n; j++)
		{
			for (int x = max(i - k, 1); x <= i; x++)
				if (j - 1 - (k - (i - x)) > 0)
					now -= a[x][j - 1 - (k - (i - x))];
			for (int x = i + 1; x <= n && x <= i + k; x++)
				if (j - 1 - (k - (x - i)) > 0)
					now -= a[x][j - 1 - (k - (x - i))];
			for (int x = max(i - k, 1); x <= i; x++)
				if (j + (k - (i - x)) <= n)
					now += a[x][j + (k - (i - x))];
			for (int x = i + 1; x <= n && x <= i + k; x++)
				if (j + (k - (x - i)) <= n)
					now += a[x][j + (k - (x - i))];
			ans = max(ans, now);
		}
	}
	cout << ans << endl;
	return 0;
}
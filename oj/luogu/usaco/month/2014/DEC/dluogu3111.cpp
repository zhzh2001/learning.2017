#include<bits/stdc++.h>
using namespace std;
const int N=100005;
long long a[N];
int main()
{
	int n,t;
	cin>>n>>t;
	for(int i=1;i<=n;i++)
	{
		int p,v;
		cin>>p>>v;
		a[i]=p+(long long)v*t;
	}
	int ans=1;
	long long slow=a[n];
	for(int i=n;i;i--)
	{
		if(a[i]<slow)
			ans++;
		slow=min(slow,a[i]);
	}
	cout<<ans<<endl;
	return 0;
}
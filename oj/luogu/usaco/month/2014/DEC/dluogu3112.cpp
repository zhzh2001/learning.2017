#include<bits/stdc++.h>
using namespace std;
const int N=20;
const long long INF=0x3f3f3f3f3f3f3f3fll;
int h[N],w[N],s[N];
long long f[1<<N];
int main()
{
	int n,H;
	cin>>n>>H;
	for(int i=0;i<n;i++)
		cin>>h[i]>>w[i]>>s[i];
	f[0]=INF;
	long long ans=-1;
	for(int i=1;i<1<<n;i++)
	{
		long long nowh=0;
		f[i]=-INF;
		for(int j=0;j<n;j++)
			if(i&(1<<j))
			{
				nowh+=h[j];
				f[i]=max(f[i],min(f[i^(1<<j)]-w[j],(long long)s[j]));
			}
		if(nowh>=H)
			ans=max(ans,f[i]);
	}
	if(~ans)
		cout<<ans<<endl;
	else
		cout<<"Mark is too tall\n";
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 505, INF = 0x3f3f3f3f;
pair<int, int> p[N];
int f[N][N];
int main()
{
	int n, k;
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
		cin >> p[i].first >> p[i].second;
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	f[1][0] = 0;
	for (int i = 2; i <= n; i++)
		for (int j = 0; j < i && j <= k; j++)
			for (int k = 0; k <= j; k++)
				f[i][j] = min(f[i][j], f[i - k - 1][j - k] + abs(p[i - k - 1].first - p[i].first) + abs(p[i - k - 1].second - p[i].second));
	cout << f[n][k] << endl;
	return 0;
}
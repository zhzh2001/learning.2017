#include<bits/stdc++.h>
using namespace std;
const int N=505,INF=0x3f3f3f3f;
int x[N],y[N],f[N][N];
int main()
{
	int n,k;
	cin>>n>>k;
	k=n-k;
	for(int i=1;i<=n;i++)
		cin>>x[i]>>y[i];
	memset(f,0x3f,sizeof(f));
	f[1][1]=0;
	for(int i=2;i<=n;i++)
		for(int j=2;j<=i&&j<=k;j++)
			for(int pred=1;pred<i;pred++)
				f[i][j]=min(f[i][j],f[pred][j-1]+abs(x[i]-x[pred])+abs(y[i]-y[pred]));
	cout<<f[n][k]<<endl;
	return 0;
}
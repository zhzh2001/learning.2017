#include<bits/stdc++.h>
using namespace std;
const int N=40005,INF=0x3f3f3f3f;
const long long INFll=0x3f3f3f3f3f3f3f3fll;
vector<int> mat[N];
int db[N],de[N],dp[N];
void bfs(int s,int d[],int n)
{
	queue<int> Q;
	Q.push(s);
	fill(d+1,d+n+1,INF);
	d[s]=0;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(auto v:mat[k])
			if(d[v]==INF)
			{
				d[v]=d[k]+1;
				Q.push(v);
			}
	}
}
int main()
{
	int b,e,p,n,m;
	cin>>b>>e>>p>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		mat[v].push_back(u);
	}
	bfs(1,db,n);
	bfs(2,de,n);
	bfs(n,dp,n);
	long long ans=INFll;
	for(int i=1;i<=n;i++)
		ans=min(ans,(long long)b*db[i]+(long long)e*de[i]+(long long)p*dp[i]);
	cout<<ans<<endl;
	return 0;
}
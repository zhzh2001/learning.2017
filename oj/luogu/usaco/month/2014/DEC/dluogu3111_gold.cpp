#include<bits/stdc++.h>
using namespace std;
const int N=100005;
long long f[N];
int main()
{
	int n,t;
	cin>>n>>t;
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		int p,v;
		cin>>p>>v;
		long long val=p+(long long)t*v;
		long long *ptr=upper_bound(f+1,f+cnt+1,val,greater<long long>());
		if(ptr==f+cnt+1)
			cnt++;
		*ptr=val;
	}
	cout<<cnt<<endl;
	return 0;
}
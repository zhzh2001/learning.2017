#include<bits/stdc++.h>
using namespace std;
const int N=10005,INF=0x3f3f3f3f;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){}
	bool operator>(const node& rhs)const
	{
		return w>rhs.w;
	}
};
vector<node> matp[N],matq[N],mat[N];
int n,dp[N],dq[N],d[N];
void dijkstra(vector<node> E[],int d[],int s)
{
	static bool vis[N];
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
		d[i]=i==s?0:INF;
	priority_queue<node,vector<node>,greater<node> > Q;
	Q.push(node(s,0));
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(!vis[k.v])
		{
			vis[k.v]=true;
			for(auto e:E[k.v])
				if(!vis[e.v]&&d[k.v]+e.w<d[e.v])
				{
					d[e.v]=d[k.v]+e.w;
					Q.push(node(e.v,d[e.v]));
				}
		}
	}
}
int main()
{
	int m;
	cin>>n>>m;
	while(m--)
	{
		int u,v,p,q;
		cin>>u>>v>>p>>q;
		matp[v].push_back(node(u,p));
		matq[v].push_back(node(u,q));
	}
	dijkstra(matp,dp,n);
	dijkstra(matq,dq,n);
	for(int i=1;i<=n;i++)
		for(int j=0;j<matp[i].size();j++)
		{
			int u=matp[i][j].v,d1=dp[u]-dp[i],d2=dq[u]-dq[i];
			mat[u].push_back(node(i,(matp[i][j].w>d1)+(matq[i][j].w>d2)));
		}
	dijkstra(mat,d,1);
	cout<<d[n]<<endl;
	return 0;
}
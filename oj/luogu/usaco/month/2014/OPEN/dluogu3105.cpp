#include<bits/stdc++.h>
using namespace std;
const int N=100005;
pair<int,char> c[N];
pair<int,int> early[2][N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>c[i].first>>c[i].second;
	sort(c+1,c+n+1);
	int cc[2]={0,0},sum=0,ans=0;
	for(int i=1;i<=n;i++)
	{
		if(!cc[sum&1]||sum>early[sum&1][cc[sum&1]].first)
		{
			early[sum&1][++cc[sum&1]].first=sum;
			early[sum&1][cc[sum&1]].second=c[i].first;
		}
		sum+=c[i].second=='W'?-1:1;
		if(sum<=early[sum&1][cc[sum&1]].first)
			ans=max(ans,c[i].first-lower_bound(early[sum&1]+1,early[sum&1]+cc[sum&1]+1,make_pair(sum,0))->second);
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=20;
long long f[N][10][N*2];
long long calc(long long x)
{
	stringstream ss;
	ss<<x;
	string s=ss.str();
	long long ans=0;
	for(int i=1;i<s.length();i++)
		for(int j=0;j<10;j++)
			for(int k=20;k<40;k++)
				ans+=f[i][j][k];
	for(int i=0;i<s.length();i++)
		for(int j=0;j<10;j++)
			for(int now=0;now<s[i]-'0'||(now==s[i]-'0'&&i==s.length()-1);now++)
				for(int k=20-now==j;k<40;k++)
					ans+=f[i-1][j][k];
	return ans;
}
int main()
{
	for(int i=0;i<10;i++)
		f[0][i][N]=1;
	for(int i=1;i<=19;i++)
		for(int j=0;j<10;j++)
			for(int k=0;k<40;k++)
				for(int now=0;now<10;now++)
					if(now==j&&k)
						f[i][j][k]+=f[i-1][j][k-1];
					else
						if(k+1<40)
							f[i][j][k]+=f[i-1][j][k+1];
	long long l,r;
	cin>>l>>r;
	cout<<calc(r)-calc(l-1)<<endl;
	return 0;
}
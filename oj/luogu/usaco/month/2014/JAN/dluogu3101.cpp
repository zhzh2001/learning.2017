#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
const int N = 505;
int mat[N][N];
struct edge
{
	int u, v, w;
	edge() {}
	edge(int u, int v, int w) : u(u), v(v), w(w) {}
	bool operator<(const edge &rhs) const
	{
		return w < rhs.w;
	}
} e[N * N * 2];
int f[N * N], ans[N * N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
vector<int> l[N * N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, t;
	cin >> n >> m >> t;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			cin >> mat[i][j];
	int en = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			if (i < n)
				e[++en] = edge((i - 1) * m + j, i * m + j, abs(mat[i][j] - mat[i + 1][j]));
			if (j < m)
				e[++en] = edge((i - 1) * m + j, (i - 1) * m + j + 1, abs(mat[i][j] - mat[i][j + 1]));
		}
	sort(e + 1, e + en + 1);
	for (int i = 1; i <= n * m; i++)
	{
		f[i] = i;
		l[i].push_back(i);
	}
	for (int i = 1; i <= en; i++)
	{
		int ru = getf(e[i].u), rv = getf(e[i].v);
		if (ru != rv)
		{
			if (l[rv].size() >= t && l[ru].size() < t)
				for (auto j : l[ru])
					ans[j] = e[i].w;
			if (l[ru].size() >= t && l[rv].size() < t)
				for (auto j : l[rv])
					ans[j] = e[i].w;
			if (l[ru].size() < t && l[rv].size() < t && l[ru].size() + l[rv].size() >= t)
			{
				for (auto j : l[ru])
					ans[j] = e[i].w;
				for (auto j : l[rv])
					ans[j] = e[i].w;
			}
			if (l[ru].size() < l[rv].size())
			{
				f[ru] = rv;
				for (auto j : l[ru])
					l[rv].push_back(j);
				l[ru].clear();
			}
			else
			{
				f[rv] = ru;
				for (auto j : l[rv])
					l[ru].push_back(j);
				l[rv].clear();
			}
		}
	}
	long long sum = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			bool flag;
			cin >> flag;
			if (flag)
				sum += ans[(i - 1) * m + j];
		}
	cout << sum << endl;
	return 0;
}
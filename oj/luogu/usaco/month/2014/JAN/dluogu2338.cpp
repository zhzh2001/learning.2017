#include<bits/stdc++.h>
using namespace std;
const int N=10005;
int t[N],d[N];
int main()
{
	int n;
	cin>>n;
	int tn=0,dn=0;
	while(n--)
	{
		char opt;
		cin>>opt;
		if(opt=='T')
			cin>>t[++tn];
		else
			cin>>d[++dn];
	}
	sort(t+1,t+tn+1);
	sort(d+1,d+dn+1);
	int i=1,j=1,v=1;
	double pos=0,now=0;
	for(;i<=tn&&j<=dn;v++)
		if(t[i]-now<(d[j]-pos)*v)
		{
			if(pos+(t[i]-now)/v<1000)
				pos+=(t[i]-now)/v;
			else
			{
				i=tn+1;j=dn+1;
				break;
			}
			now=t[i++];
		}
		else
		{
			now+=(d[j]-pos)*v;
			pos=d[j++];
		}
	for(;i<=tn;v++)
	{
		if(pos+(t[i]-now)/v<1000)
			pos+=(t[i]-now)/v;
		else
		{
			i=tn+1;j=dn+1;
			break;
		}
		now=t[i++];
	}
	for(;j<=dn;v++)
	{
		now+=(d[j]-pos)*v;
		pos=d[j++];
	}
	if(pos<1000)
		now+=(1000-pos)*v;
	cout.precision(0);
	cout<<fixed<<now<<endl;
	return 0;
}
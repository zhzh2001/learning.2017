#pragma GCC optimize 2
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 105;
int mat[N][N], f[N][N][2];
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			char c;
			cin >> c;
			mat[i][j] = c == 'R' ? 1 : 2;
		}
	int ans = n;
	for (;;)
	{
		int now = 0, x, y;
		bool flag = true;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
			{
				if (mat[i][j] != 3)
					flag = false;
				if (mat[i][j] & 1)
					f[i][j][0] = min(min(f[i - 1][j][0], f[i][j - 1][0]), f[i - 1][j - 1][0]) + 1;
				else
					f[i][j][0] = 0;
				if (mat[i][j] & 2)
					f[i][j][1] = min(min(f[i - 1][j][1], f[i][j - 1][1]), f[i - 1][j - 1][1]) + 1;
				else
					f[i][j][1] = 0;
				if (f[i][j][0] != f[i][j][1])
				{
					int tmp = max(f[i][j][0], f[i][j][1]);
					if (now < tmp)
					{
						now = tmp;
						x = i;
						y = j;
					}
				}
			}
		if (flag)
			break;
		ans = min(ans, now);
		for (int i = x - now + 1; i <= x; i++)
			for (int j = y - now + 1; j <= y; j++)
				mat[i][j] = 3;
	}
	cout << ans << endl;
	return 0;
}
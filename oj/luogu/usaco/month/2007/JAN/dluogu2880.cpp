#include<bits/stdc++.h>
using namespace std;
const int N=50005,LOGN=17;
int a[N],Min[N][LOGN],Max[N][LOGN];
int main()
{
	int n,q;
	cin>>n>>q;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		Min[i][0]=Max[i][0]=a[i];
	}
	for(int i=1;i<LOGN;i++)
		for(int j=1;j+(1<<i)-1<=n;j++)
		{
			Min[j][i]=min(Min[j][i-1],Min[j+(1<<i-1)][i-1]);
			Max[j][i]=max(Max[j][i-1],Max[j+(1<<i-1)][i-1]);
		}
	while(q--)
	{
		int l,r;
		cin>>l>>r;
		int t=floor(log2(r-l+1));
		cout<<max(Max[l][t],Max[r-(1<<t)+1][t])-min(Min[l][t],Min[r-(1<<t)+1][t])<<endl;
	}
	return 0;
}
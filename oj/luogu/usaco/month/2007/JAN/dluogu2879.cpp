#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=10005;
int head[N],l[M],r[M],nxt[M],e,into[N],f[N];
bool vis[N];
inline void add_edge(int u,int l,int r)
{
	::l[++e]=l;
	::r[e]=r;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int n,i,h,m;
	cin>>n>>i>>h>>m;
	while(m--)
	{
		int a,b;
		cin>>a>>b;
		if(a+1<b)
		{
			add_edge(a,a+1,b-1);
			for(int i=a+1;i<b;i++)
				into[i]++;
		}
		else
			if(b+1<a)
			{
				add_edge(a,b+1,a-1);
				for(int i=b+1;i<a;i++)
					into[i]++;
			}
	}
	queue<int> Q;
	for(int i=1;i<=n;i++)
		if(!into[i])
		{
			f[i]=h;
			Q.push(i);
		}
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		vis[k]=true;
		for(int i=head[k];i;i=nxt[i])
			for(int j=l[i];j<=r[i];j++)
				if(!--into[j]&&!vis[j])
				{
					f[j]=f[k]-1;
					Q.push(j);
				}
	}
	for(int i=1;i<=n;i++)
		cout<<f[i]<<endl;
	return 0;
}
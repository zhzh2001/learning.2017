#include<bits/stdc++.h>
using namespace std;
const int N=50005;
struct node
{
	int Min,Max;
}tree[N*4];
void build(int k,int l,int r)
{
	if(l==r)
	{
		cin>>tree[k].Min;
		tree[k].Max=tree[k].Min;
	}
	else
	{
		int mid=(l+r)/2,ls=k*2,rs=ls+1;
		build(ls,l,mid);
		build(rs,mid+1,r);
		tree[k].Min=min(tree[ls].Min,tree[rs].Min);
		tree[k].Max=max(tree[ls].Max,tree[rs].Max);
	}
}
typedef pair<int,int> pii;
pii query(int k,int l,int r,int L,int R)
{
	if(L<=l&&R>=r)
		return make_pair(tree[k].Min,tree[k].Max);
	int mid=(l+r)/2,ls=k*2,rs=ls+1;
	pii ans=make_pair(tree[k].Max,tree[k].Min);
	if(L<=mid)
	{
		pii now=query(ls,l,mid,L,R);
		ans.first=min(ans.first,now.first);
		ans.second=max(ans.second,now.second);
	}
	if(R>mid)
	{
		pii now=query(rs,mid+1,r,L,R);
		ans.first=min(ans.first,now.first);
		ans.second=max(ans.second,now.second);
	}
	return ans;
}
int main()
{
	int n,q;
	cin>>n>>q;
	build(1,1,n);
	while(q--)
	{
		int l,r;
		cin>>l>>r;
		pii ans=query(1,1,n,l,r);
		cout<<ans.second-ans.first<<endl;
	}
	return 0;
}
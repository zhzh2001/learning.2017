#include <iostream>
#include <algorithm>
using namespace std;
const int N = 305, INF = 0x3f3f3f3f;
int a[N], b[N], f[N][N];
int main()
{
	int n, m;
	cin >> m >> n;
	for (int i = 1; i <= n; i++)
	{
		cin >> b[i] >> a[i];
		a[i] += a[i - 1];
		b[i] += b[i - 1];
	}
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	f[0][0] = 1;
	for (int i = 1; i <= n; i++)
		for (int j = 0; j <= i; j++)
			if (b[i] - b[j] <= m)
				for (int k = 0; k <= j; k++)
					if (b[i] - b[j] + a[j] - a[k] <= m)
						f[i][j] = min(f[i][j], f[j][k] + 1);
	cout << f[n][n] << endl;
	return 0;
}
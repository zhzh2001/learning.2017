#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct cow
{
	int t,d;
	bool operator<(const cow& rhs)const
	{
		return t*rhs.d<rhs.t*d;
	}
}c[N];
int main()
{
	int n;
	cin>>n;
	int sumd=0;
	for(int i=1;i<=n;i++)
	{
		cin>>c[i].t>>c[i].d;
		sumd+=c[i].d;
	}
	sort(c+1,c+n+1);
	long long ans=0;
	for(int i=1;i<=n;i++)
	{
		sumd-=c[i].d;
		ans+=2ll*c[i].t*sumd;
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=60005;
char s[N],t[N];
int rank[N];
struct node
{
	int x,y,id;
	bool operator<(const node& rhs)const
	{
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}a[N];
void build_sa(int n)
{
	copy(s+1,s+n+1,t+1);
	sort(t+1,t+n+1);
	int cc=unique(t+1,t+n+1)-t;
	for(int i=1;i<=n;i++)
		rank[i]=lower_bound(t+1,t+cc,s[i])-t;
	for(int i=0;(1<<i)<=n;i++)
	{
		for(int j=1;j<=n;j++)
		{
			a[j].id=j;
			a[j].x=rank[j];
			a[j].y=j+(1<<i)<=n?rank[j+(1<<i)]:0;
		}
		sort(a+1,a+n+1);
		int cnt=0;
		for(int j=1;j<=n;j++)
		{
			cnt+=a[j].x!=a[j-1].x||a[j].y!=a[j-1].y;
			rank[a[j].id]=cnt;
		}
	}
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>s[i];
		s[n+n-i]=s[i];
	}
	build_sa(2*n-1);
	int l=1,r=n,cnt=0;
	while(l<=r)
	{
		if(s[l]<s[r])
			cout<<s[l++];
		else
			if(s[l]>s[r])
				cout<<s[r--];
			else
				if(rank[l]<rank[n+n-r])
					cout<<s[l++];
				else
					cout<<s[r--];
		if((++cnt)%80==0)
			cout<<endl;
	}
	return 0;
}
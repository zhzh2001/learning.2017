#include<bits/stdc++.h>
using namespace std;
const int N=100005;
pair<int,int> cow[N],grass[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>cow[i].second>>cow[i].first;
	sort(cow+1,cow+n+1);
	for(int i=1;i<=m;i++)
		cin>>grass[i].second>>grass[i].first;
	sort(grass+1,grass+m+1);
	multiset<int> S;
	int j=m;
	long long ans=0;
	for(int i=n;i;i--)
	{
		for(;j&&grass[j].first>=cow[i].first;j--)
			S.insert(grass[j].second);
		multiset<int>::iterator it=S.lower_bound(cow[i].second);
		if(it==S.end())
		{
			cout<<-1<<endl;
			return 0;
		}
		ans+=*it;
		S.erase(it);
	}
	cout<<ans<<endl;
	return 0;
}
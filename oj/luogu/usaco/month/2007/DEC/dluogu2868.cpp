#include<bits/stdc++.h>
using namespace std;
const int N=1005;
const double eps=1e-6;
int n,f[N],cnt[N];
bool inQ[N];
vector<pair<int,int>> mat[N];
double d[N];
bool check(double val)
{
	queue<int> Q;
	for(int i=1;i<=n;i++)
	{
		Q.push(i);
		d[i]=.0;
		cnt[i]=0;
		inQ[i]=true;
	}
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(auto e:mat[k])
			if(d[k]+val*e.second-f[e.first]<d[e.first])
			{
				d[e.first]=d[k]+val*e.second-f[e.first];
				if(!inQ[e.first])
				{
					inQ[e.first]=true;
					Q.push(e.first);
					if(++cnt[e.first]>n)
						return true;
				}
			}
	}
	return false;
}
int main()
{
	int m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>f[i];
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
	}
	double l=.0,r=1000.0;
	while(r-l>eps)
	{
		double mid=(l+r)/2.0;
		if(check(mid))
			l=mid;
		else
			r=mid;
	}
	cout.precision(2);
	cout<<fixed<<l<<endl;
	return 0;
}
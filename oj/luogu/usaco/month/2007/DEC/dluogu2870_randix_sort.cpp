#include<bits/stdc++.h>
using namespace std;
const int N=60005;
char s[N];
int sa[N],rank[N],a[N],b[N],cnt[N];
void build_sa(int n,int m)
{
	int *x=a,*y=b;
	fill(cnt,cnt+m,0);
	for(int i=1;i<=n;i++)
		cnt[x[i]=s[i]-'A']++;
	for(int i=1;i<m;i++)
		cnt[i]+=cnt[i-1];
	for(int i=n;i;i--)
		sa[cnt[x[i]]--]=i;
	for(int i=1;i<=n;i*=2)
	{
		int cc=0;
		for(int j=n-i+1;j<=n;j++)
			y[++cc]=j;
		for(int j=1;j<=n;j++)
			if(sa[j]>i)
				y[++cc]=sa[j]-i;
		fill(cnt,cnt+m,0);
		for(int j=1;j<=n;j++)
			cnt[x[y[j]]]++;
		for(int j=1;j<m;j++)
			cnt[j]+=cnt[j-1];
		for(int j=n;j;j--)
			sa[cnt[x[y[j]]]--]=y[j];
		swap(x,y);
		cc=0;
		for(int j=1;j<=n;j++)
		{
			cc+=!(s[sa[j]]==s[sa[j-1]]&&s[sa[j]+i]==s[sa[j-1]+i]);
			x[sa[j]]=cc;
		}
		m=cc;
	}
	for(int i=1;i<=n;i++)
		rank[sa[i]]=i;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>s[i];
		s[n+n-i]=s[i];
	}
	build_sa(2*n-1,26);
	int l=1,r=n,cnt=0;
	while(l<=r)
	{
		if(rank[l]<rank[n+n-r])
			cout<<s[l++];
		else
			cout<<s[r--];
		if((++cnt)%80==0)
			cout<<endl;
	}
	return 0;
}
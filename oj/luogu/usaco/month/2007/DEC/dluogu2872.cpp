#include<bits/stdc++.h>
using namespace std;
const int N=1005;
int x[N],y[N];
double d[N];
bool mat[N][N],vis[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>x[i]>>y[i];
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u][v]=mat[v][u]=true;
	}
	for(int i=0;i<=n;i++)
		d[i]=i==1?0:HUGE_VAL;
	double ans=0;
	for(int i=1;i<=n;i++)
	{
		int j=0;
		for(int k=1;k<=n;k++)
			if(!vis[k]&&d[k]<d[j])
				j=k;
		vis[j]=true;
		ans+=d[j];
		for(int k=1;k<=n;k++)
			if(!vis[k])
			{
				double newd=mat[j][k]?0:hypot(x[j]-x[k],y[j]-y[k]);
				d[k]=min(d[k],newd);
			}
	}
	cout.precision(2);
	cout<<fixed<<ans<<endl;
	return 0;
}
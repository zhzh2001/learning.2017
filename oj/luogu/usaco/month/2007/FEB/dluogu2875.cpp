#include<bits/stdc++.h>
using namespace std;
const int N=605;
string dict[N];
int f[N];
int main()
{
	int n,l;
	string s;
	cin>>n>>l>>s;
	for(int i=1;i<=n;i++)
		cin>>dict[i];
	for(int i=l-1;i>=0;i--)
	{
		f[i]=f[i+1]+1;
		for(int j=1;j<=n;j++)
			if(i+dict[j].length()<=l)
			{
				int ti=i,tj=0,del=0;
				for(;ti<l&&tj<dict[j].length();ti++)
					if(s[ti]==dict[j][tj])
						tj++;
					else
						del++;
				if(tj==dict[j].length())
					f[i]=min(f[i],f[ti]+del);
			}
	}
	cout<<f[0]<<endl;
	return 0;
}
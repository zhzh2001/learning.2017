#include <iostream>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 31, INF = 0x3f3f3f3f, dx[] = {-2, -2, -1, -1, 1, 1, 2, 2}, dy[] = {-1, 1, -2, 2, -2, 2, -1, 1};
int mat[N][N], d[N][N], newmat[N][N][N][N];
long long cnt[N][N];
bool inQ[N][N];
int main()
{
	int n, m;
	cin >> n >> m;
	int sx, sy, tx, ty;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
		{
			cin >> mat[i][j];
			if (mat[i][j] == 3)
			{
				sx = i;
				sy = j;
				mat[i][j] = 0;
			}
			if (mat[i][j] == 4)
			{
				tx = i;
				ty = j;
				mat[i][j] = 0;
			}
		}
	typedef pair<int, int> point;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++)
			if (!mat[i][j])
			{
				newmat[i][j][i][j] = 1;
				queue<point> Q;
				Q.push(make_pair(i, j));
				while (!Q.empty())
				{
					point k = Q.front();
					Q.pop();
					for (int dir = 0; dir < 8; dir++)
					{
						int nx = k.first + dx[dir], ny = k.second + dy[dir];
						if (nx > 0 && nx <= n && ny > 0 && ny <= m && mat[nx][ny] == 1 && !newmat[i][j][nx][ny])
						{
							newmat[i][j][nx][ny] = 1;
							Q.push(make_pair(nx, ny));
						}
					}
				}
				for (int x = 1; x <= n; x++)
					for (int y = 1; y <= m; y++)
						if (newmat[i][j][x][y] == 1)
							for (int k = 0; k < 8; k++)
							{
								int nx = x + dx[k], ny = y + dy[k];
								if (nx > 0 && nx <= n && ny > 0 && ny <= m && !mat[nx][ny] && !newmat[i][j][nx][ny])
									newmat[i][j][nx][ny] = 2;
							}
			}
	fill_n(&d[0][0], sizeof(d) / sizeof(int), INF);
	d[sx][sy] = 0;
	cnt[sx][sy] = 1;
	queue<point> Q;
	Q.push(make_pair(sx, sy));
	while (!Q.empty())
	{
		point k = Q.front();
		Q.pop();
		for (int nx = 1; nx <= n; nx++)
			for (int ny = 1; ny <= m; ny++)
				if (newmat[k.first][k.second][nx][ny] == 2)
					if (d[nx][ny] == INF)
					{
						d[nx][ny] = d[k.first][k.second] + 1;
						cnt[nx][ny] = cnt[k.first][k.second];
						Q.push(make_pair(nx, ny));
					}
					else if (d[k.first][k.second] + 1 == d[nx][ny])
						cnt[nx][ny] += cnt[k.first][k.second];
	}
	if (d[tx][ty] == INF)
		cout << -1 << endl;
	else
		cout << d[tx][ty] - 1 << endl
			 << cnt[tx][ty] << endl;
	return 0;
}
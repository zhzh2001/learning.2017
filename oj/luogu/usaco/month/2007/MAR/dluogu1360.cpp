#include<bits/stdc++.h>
using namespace std;
const unsigned long long BASE=233333ull;
const int M=30;
int cnt[M];
typedef unordered_map<unsigned long long,int> mulli;
int main()
{
	int n,k;
	cin>>n>>k;
	int ans=0;
	mulli S;
	S[0]=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		for(int j=0;j<k;j++)
			cnt[j]+=(x&(1<<j))>0;
		unsigned long long key=0;
		for(int j=1;j<k;j++)
			key=key*BASE+cnt[j-1]-cnt[j];
		mulli::iterator it=S.find(key);
		if(it!=S.end())
			ans=max(ans,i-it->second);
		else
			S[key]=i;
	}
	cout<<ans<<endl;
	return 0;
}
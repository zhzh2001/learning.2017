#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N];
int main()
{
	int n,m;
	cin>>n>>m;
	int sum=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		sum+=a[i];
	}
	int l=sum/m,r=sum;
	while(l<r)
	{
		int mid=(l+r)/2,sum=0,cnt=0;
		for(int i=1;i<=n;i++)
		{
			if(a[i]>mid)
			{
				cnt=m+1;
				break;
			}
			if(sum+a[i]>mid)
			{
				cnt++;
				sum=0;
			}
			sum+=a[i];
		}
		if(sum)
			cnt++;
		if(cnt<=m)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=5005,M=50005;
pair<int,int> E[M];
vector<int> mat[N],rmat[N];
int f[N],g[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		cin>>E[i].first>>E[i].second;
		mat[E[i].first].push_back(E[i].second);
		rmat[E[i].second].push_back(E[i].first);
	}
	for(int i=n;i;i--)
		if(!mat[i].size())
			f[i]=1;
		else
			for(auto v:mat[i])
				f[i]+=f[v];
	for(int i=1;i<=n;i++)
		if(!rmat[i].size())
			g[i]=1;
		else
			for(auto v:rmat[i])
				g[i]+=g[v];
	int ans=0;
	for(int i=1;i<=m;i++)
		ans=max(ans,g[E[i].first]*f[E[i].second]);
	cout<<ans<<endl;
	return 0;
}
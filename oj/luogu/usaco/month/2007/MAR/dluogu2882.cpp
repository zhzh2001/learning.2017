#include<bits/stdc++.h>
using namespace std;
const int N=5005;
bool a[N],b[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		char c;
		cin>>c;
		a[i]=c=='B';
	}
	int ans=n,k;
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			b[j]=a[j-1]^a[j];
		int now=0;
		for(int j=1;j+i-1<=n;j++)
		{
			now+=b[j];
			b[j+i]^=b[j];
		}
		for(int j=n-i+2;j<=n;j++)
			if(b[j])
			{
				now=n;
				break;
			}
		if(now<ans)
		{
			ans=now;
			k=i;
		}
	}
	cout<<k<<' '<<ans<<endl;
	return 0;
}
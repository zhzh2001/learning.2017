#include<bits/stdc++.h>
using namespace std;
const int N=5005;
bool a[N];
struct node
{
	int cnt;
	bool flip;
}tree[N*4];
void build(int id,int l,int r)
{
	tree[id].flip=false;
	if(l==r)
		tree[id].cnt=a[l];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		tree[id].cnt=tree[id*2].cnt+tree[id*2+1].cnt;
	}
}
inline void pushdown(int id,int l,int r)
{
	if(tree[id].flip&&l<r)
	{
		int mid=(l+r)/2;
		tree[id*2].flip^=1;
		tree[id*2+1].flip^=1;
		tree[id*2].cnt=mid-l+1-tree[id*2].cnt;
		tree[id*2+1].cnt=r-mid-tree[id*2+1].cnt;
		tree[id].flip=false;
	}
}
int L,R;
void flip(int id,int l,int r)
{
	if(L<=l&&R>=r)
	{
		tree[id].flip^=1;
		tree[id].cnt=r-l+1-tree[id].cnt;
	}
	else
	{
		pushdown(id,l,r);
		int mid=(l+r)/2;
		if(L<=mid)
			flip(id*2,l,mid);
		if(R>mid)
			flip(id*2+1,mid+1,r);
		tree[id].cnt=tree[id*2].cnt+tree[id*2+1].cnt;
	}
}
bool query(int id,int l,int r)
{
	if(l==r)
		return tree[id].cnt;
	pushdown(id,l,r);
	int mid=(l+r)/2;
	if(L<=mid)
		return query(id*2,l,mid);
	return query(id*2+1,mid+1,r);
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		char c;
		cin>>c;
		a[i]=c=='F';
	}
	int ans=n,k;
	for(int i=1;i<=n;i++)
	{
		build(1,1,n);
		int now=0;
		for(int j=1;j<=n;j++)
		{
			L=j;
			if(!query(1,1,n))
			{
				R=j+i-1;
				if(R>n)
				{
					now=n;
					break;
				}
				flip(1,1,n);
				if(++now==ans)
					break;
			}
		}
		if(now<ans)
		{
			ans=now;
			k=i;
		}
	}
	cout<<k<<' '<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1005,M=10005;
int head[N],v[M],nxt[M],e;
bitset<N> vis,mat[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	mat[k][k]=vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
	{
		if(!vis[v[i]])
			dfs(v[i]);
		mat[k]|=mat[v[i]];
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
	}
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=mat[i].count();
	cout<<n*(n-1)/2-ans+n<<endl;
	return 0;
}
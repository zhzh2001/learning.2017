#include<bits/stdc++.h>
using namespace std;
const int N=405;
struct edge
{
	int v,cap,nxt;
	edge(int v,int cap,int nxt):v(v),cap(cap),nxt(nxt){}
};
vector<edge> E;
int head[N],H[N];
inline void add_edge(int u,int v,int cap)
{
	E.push_back(edge(v,cap,head[u]));
	E.push_back(edge(u,0,head[v]));
	head[u]=E.size()-2;
	head[v]=E.size()-1;
}
int t,dep[N];
bool bfs()
{
	queue<int> Q;
	Q.push(1);
	fill(dep+1,dep+t+1,0);
	dep[1]=1;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		if(k==t)
			return true;
		for(int i=head[k];~i;i=E[i].nxt)
			if(E[i].cap&&!dep[E[i].v])
			{
				dep[E[i].v]=dep[k]+1;
				Q.push(E[i].v);
			}
	}
	return false;
}
int dfs(int k,int pred)
{
	if(k==t||!pred)
		return pred;
	int flow=0,t;
	for(int& i=H[k];~i;i=E[i].nxt)
		if(dep[E[i].v]==dep[k]+1&&(t=dfs(E[i].v,min(pred,E[i].cap)))>0)
		{
			E[i].cap-=t;
			E[i^1].cap+=t;
			flow+=t;
			if(!(pred-=t))
				break;
		}
	return flow;
}
int main()
{
	int n,f,d;
	cin>>n>>f>>d;
	t=n+n+d+f+2;
	fill(head+1,head+t+1,-1);
	for(int i=1;i<=d;i++)
		add_edge(1,i+1,1);
	for(int i=1;i<=n;i++)
		add_edge(i+d+1,i+d+n+1,1);
	for(int i=1;i<=f;i++)
		add_edge(i+d+n+n+1,t,1);
	for(int i=1;i<=n;i++)
	{
		int fi,di;
		cin>>fi>>di;
		while(fi--)
		{
			int id;
			cin>>id;
			add_edge(i+d+n+1,id+d+n+n+1,1);
		}
		while(di--)
		{
			int id;
			cin>>id;
			add_edge(id+1,i+d+1,1);
		}
	}
	int ans=0;
	while(bfs())
	{
		copy(head+1,head+t+1,H+1);
		ans+=dfs(1,n);
	}
	cout<<ans<<endl;
	return 0;
}
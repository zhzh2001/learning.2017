#include <iostream>
#include <algorithm>
using namespace std;
const int N = 15, INF = 0x3f3f3f3f;
int mat[N], now[N], tmp[N], ans[N];
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 0; i < n; i++)
		for (int j = 0; j < m; j++)
		{
			int x;
			cin >> x;
			mat[i] = mat[i] * 2 + x;
		}
	int cnt = INF, mc = (1 << m) - 1;
	for (int s = 0; s <= mc; s++)
	{
		int nowc = __builtin_popcount(now[0] = s);
		copy(mat, mat + n, tmp);
		tmp[0] ^= s ^ ((s << 1) & mc) ^ (s >> 1);
		tmp[1] ^= s;
		for (int i = 1; i < n; i++)
		{
			nowc += __builtin_popcount(now[i] = tmp[i - 1]);
			tmp[i] ^= now[i] ^ ((now[i] << 1) & mc) ^ (now[i] >> 1);
			if (i + 1 < n)
				tmp[i + 1] ^= now[i];
		}
		if (!tmp[n - 1] && nowc < cnt)
		{
			cnt = nowc;
			copy(now, now + n, ans);
		}
	}
	if (cnt == INF)
		cout << "IMPOSSIBLE\n";
	else
		for (int i = 0; i < n; i++)
		{
			for (int j = m - 1; j >= 0; j--)
				cout << bool(ans[i] & (1 << j)) << ' ';
			cout << endl;
		}
	return 0;
}
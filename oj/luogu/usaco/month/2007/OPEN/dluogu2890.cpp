#include <iostream>
#include <string>
#include <algorithm>
using namespace std;
const int N = 26, M = 2005, INF = 0x3f3f3f3f;
int cost[N], f[M][M];
int main()
{
	int n, m;
	string s;
	cin >> n >> m >> s;
	s = ' ' + s;
	fill(cost, cost + n, INF);
	for (int i = 1; i <= n; i++)
	{
		char c;
		int x, y;
		cin >> c >> x >> y;
		cost[c - 'a'] = min(x, y);
	}
	fill_n(&f[0][0], sizeof(f) / sizeof(int), INF);
	for (int i = 1; i <= m; i++)
		f[i][i] = 0;
	for (int i = m; i; i--)
		for (int j = i + 1; j <= m; j++)
			if (s[i] == s[j])
				f[i][j] = j - i > 1 ? f[i + 1][j - 1] : 0;
			else
				f[i][j] = min(f[i + 1][j] + cost[s[i] - 'a'], f[i][j - 1] + cost[s[j] - 'a']);
	cout << f[1][m] << endl;
	return 0;
}
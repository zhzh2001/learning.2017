#include<bits/stdc++.h>
using namespace std;
struct node
{
	int h;
	bool into;
	node(int h=0,bool into=false):h(h),into(into){};
};
typedef map<int,vector<node> > mivn;
mivn M;
multiset<int,greater<int> > S;
int main()
{
	int n;
	cin>>n;
	while(n--)
	{
		int l,r,h;
		cin>>l>>r>>h;
		M[l].push_back(node(h,true));
		M[r].push_back(node(h,false));
	}
	long long ans=0;
	int l=0;
	S.insert(0);
	for(mivn::iterator i=M.begin();i!=M.end();i++)
	{
		int r=i->first;
		ans+=*S.begin()*(long long)(r-l);
		for(int j=0;j<i->second.size();j++)
			if(i->second[j].into)
				S.insert(i->second[j].h);
			else
				S.erase(S.find(i->second[j].h));
		l=r;
	}
	cout<<ans<<endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, H = 105, INF = 0x3f3f3f3f;
int h[N], f[H], g[H][2];
int main()
{
	ios::sync_with_stdio(false);
	int n, c;
	cin >> n >> c;
	for (int i = 1; i <= n; i++)
		cin >> h[i];
	fill_n(f, sizeof(f) / sizeof(int), INF);
	fill_n(&g[0][0], sizeof(g) / sizeof(int), INF);
	for (int i = h[1]; i <= 100; i++)
	{
		f[i] = (i - h[1]) * (i - h[1]);
		g[i][1] = min(g[i - 1][1], f[i] - c * i);
	}
	for (int i = 100; i; i--)
		g[i][0] = min(g[i + 1][0], f[i] + c * i);
	for (int i = 2; i <= n; i++)
	{
		fill_n(f, sizeof(f) / sizeof(int), INF);
		for (int j = h[i]; j <= 100; j++)
			f[j] = min(g[j][0] - c * j + (j - h[i]) * (j - h[i]), g[j][1] + c * j + (j - h[i]) * (j - h[i]));
		fill_n(&g[0][0], sizeof(g) / sizeof(int), INF);
		for (int j = h[i]; j <= 100; j++)
			g[j][1] = min(g[j - 1][1], f[j] - c * j);
		for (int j = 100; j; j--)
			g[j][0] = min(g[j + 1][0], f[j] + c * j);
	}
	cout << *min_element(f + h[n], f + 101) << endl;
	return 0;
}
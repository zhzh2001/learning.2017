#include<bits/stdc++.h>
using namespace std;
const int M=105,INF=0x3f3f3f3f;
struct edge
{
	int u,v,w;
}e[M];
int a[M*2],mat[M*2][M*2],ans[M*2][M*2],tmp[M*2][M*2];
int main()
{
	int k,m,s,t;
	cin>>k>>m>>s>>t;
	for(int i=1;i<=m;i++)
	{
		cin>>e[i].w>>e[i].u>>e[i].v;
		a[2*i-1]=e[i].u;
		a[2*i]=e[i].v;
	}
	sort(a+1,a+2*m+1);
	int n=unique(a+1,a+2*m+1)-a-1;
	#define find(_) lower_bound(a+1,a+n+1,_)-a
	memset(mat,0x3f,sizeof(mat));
	for(int i=1;i<=m;i++)
	{
		e[i].u=find(e[i].u);e[i].v=find(e[i].v);
		mat[e[i].u][e[i].v]=mat[e[i].v][e[i].u]=e[i].w;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans[i][j]=i==j?0:INF;
	do
	{
		if(k&1)
		{
			memset(tmp,0x3f,sizeof(tmp));
			for(int t=1;t<=n;t++)
				for(int i=1;i<=n;i++)
					for(int j=1;j<=n;j++)
						tmp[i][j]=min(tmp[i][j],ans[i][t]+mat[t][j]);
			memcpy(ans,tmp,sizeof(ans));
		}
		memset(tmp,0x3f,sizeof(tmp));
		for(int t=1;t<=n;t++)
			for(int i=1;i<=n;i++)
				for(int j=1;j<=n;j++)
					tmp[i][j]=min(tmp[i][j],mat[i][t]+mat[t][j]);
		memcpy(mat,tmp,sizeof(mat));
	}
	while(k/=2);
	cout<<ans[find(s)][find(t)]<<endl;
	return 0;
}
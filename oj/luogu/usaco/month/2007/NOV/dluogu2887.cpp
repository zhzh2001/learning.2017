#include <iostream>
#include <algorithm>
#include <set>
using namespace std;
const int N = 2505;
struct cow
{
	int l, r;
	bool operator<(const cow &rhs) const
	{
		return r < rhs.r;
	}
} c[N];
int main()
{
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> c[i].l >> c[i].r;
	sort(c + 1, c + n + 1);
	multiset<pair<int, int>> S;
	while (m--)
	{
		int s, c;
		cin >> s >> c;
		S.insert(make_pair(s, c));
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		auto it = S.lower_bound(make_pair(c[i].l, 0));
		if (it != S.end() && it->first <= c[i].r)
		{
			ans++;
			auto suns = *it;
			S.erase(it);
			if (--suns.second)
				S.insert(suns);
		}
	}
	cout << ans << endl;
	return 0;
}
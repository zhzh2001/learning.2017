#include<bits/stdc++.h>
using namespace std;
const int N=1005;
int n,r,s[N],t[N],w[N],f[N];
int dp(int i)
{
	if(f[i])
		return f[i];
	int& now=f[i]=w[i];
	for(int j=1;j<=n;j++)
		if(t[j]+r<=s[i])
			now=max(now,dp(j)+w[i]);
	return now;
}
int main()
{
	int m;
	cin>>m>>n>>r;
	for(int i=1;i<=n;i++)
		cin>>s[i]>>t[i]>>w[i];
	int ans=0;
	for(int i=1;i<=n;i++)
		ans=max(ans,dp(i));
	cout<<ans<<endl;
	return 0;
}
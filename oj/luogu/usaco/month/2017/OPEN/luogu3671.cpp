#include<bits/stdc++.h>
using namespace std;
const int N=20,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
string mat[N];
int n,cnt[26],vis[N][N];
typedef pair<int,int> pii;
typedef pair<pii,pii> p4;
vector<p4> ans;
void floodfill(int x,int y,int x1,int y1,int x2,int y2,int time)
{
	vis[x][y]=time;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=x1&&nx<=x2&&ny>=y1&&ny<=y2&&mat[nx][ny]==mat[x][y]&&vis[nx][ny]<time)
			floodfill(nx,ny,x1,y1,x2,y2,time);
	}
}
int main()
{
	cin>>n;
	for(int i=0;i<n;i++)
		cin>>mat[i];
	int time=0;
	for(int x1=0;x1<n;x1++)
		for(int y1=0;y1<n;y1++)
			for(int x2=x1;x2<n;x2++)
				for(int y2=y1;y2<n;y2++)
				{
					memset(cnt,0,sizeof(cnt));
					int color=0,c1=-1,c2;
					time++;
					for(int i=x1;i<=x2;i++)
						for(int j=y1;j<=y2;j++)
							if(vis[i][j]<time)
							{
								if(!cnt[mat[i][j]-'A']++)
								{
									if(++color>2)
										goto fail;
									if(c1==-1)
										c1=mat[i][j]-'A';
									else
										c2=mat[i][j]-'A';
								}
								floodfill(i,j,x1,y1,x2,y2,time);
							}
					if(color==2&&((cnt[c1]==1&&cnt[c2]>1)||(cnt[c1]>1&&cnt[c2]==1)))
						ans.push_back(make_pair(make_pair(x1,y1),make_pair(x2,y2)));
					fail:;
				}
	int cnt=0;
	for(int i=0;i<ans.size();i++)
	{
		bool now=true;
		for(int j=0;j<ans.size();j++)
			if(j!=i&&ans[i].first.first>=ans[j].first.first&&ans[i].first.second>=ans[j].first.second&&ans[i].second.first<=ans[j].second.first&&ans[i].second.second<=ans[j].second.second)
			{
				now=false;
				break;
			}
		cnt+=now;
	}
	cout<<cnt<<endl;
	return 0;
}
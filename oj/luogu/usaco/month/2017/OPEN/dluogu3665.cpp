#include<bits/stdc++.h>
using namespace std;
const int N=200005,M=200005;
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[M];
int c[N];
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){}
	bool operator<(const node& rhs)const
	{
		return w<rhs.w;
	}
};
vector<node> mat[N];
struct disjointSets
{
	int *f;
	disjointSets(int n)
	{
		f=new int [n+1];
		for(int i=1;i<=n;i++)
			f[i]=i;
	}
	~disjointSets()
	{
		delete [] f;
	}
	int getf(int x)
	{
		return f[x]==x?x:f[x]=getf(f[x]);
	}
	bool unite(int u,int v)
	{
		int ru=getf(u),rv=getf(v);
		f[ru]=rv;
		return ru!=rv;
	}
};
void mst(int n,int m)
{
	sort(e+1,e+m+1);
	disjointSets dsu(n);
	int cnt=0;
	for(int i=1;i<=m;i++)
		if(dsu.unite(e[i].u,e[i].v))
		{
			mat[e[i].u].push_back(node(e[i].v,e[i].w));
			mat[e[i].v].push_back(node(e[i].u,e[i].w));
			if(++cnt==n-1)
				break;
		}
}
int f[N],d[N];
unordered_map<int,multiset<int>> colorHeap[N];
multiset<int> global;
void dfs(int k,int fat)
{
	for(auto e:mat[k])
		if(e.v!=fat)
		{
			f[e.v]=k;
			d[e.v]=e.w;
			dfs(e.v,k);
			colorHeap[k][c[e.v]].insert(e.w);
		}
	for(auto i:colorHeap[k])
		if(i.first!=c[k])
			global.insert(*i.second.begin());
}
int main()
{
	int n,m,k,q;
	scanf("%d%d%d%d",&n,&m,&k,&q);
	for(int i=1;i<=m;i++)
		scanf("%d%d%d",&e[i].u,&e[i].v,&e[i].w);
	mst(n,m);
	for(int i=1;i<=n;i++)
		scanf("%d",c+i);
	dfs(1,0);
	while(q--)
	{
		int x,y;
		scanf("%d%d",&x,&y);
		if(c[x]!=y)
		{
			if(!colorHeap[x][y].empty())
				global.erase(global.find(*colorHeap[x][y].begin()));
			if(!colorHeap[x][c[x]].empty())
				global.insert(*colorHeap[x][c[x]].begin());
			
			if(f[x])
			{
				if(!colorHeap[f[x]][c[x]].empty()&&c[f[x]]!=c[x])
					global.erase(global.find(*colorHeap[f[x]][c[x]].begin()));
				colorHeap[f[x]][c[x]].erase(colorHeap[f[x]][c[x]].find(d[x]));
				if(!colorHeap[f[x]][c[x]].empty()&&c[f[x]]!=c[x])
					global.insert(*colorHeap[f[x]][c[x]].begin());
				
				if(!colorHeap[f[x]][y].empty()&&c[f[x]]!=y)
					global.erase(global.find(*colorHeap[f[x]][y].begin()));
				colorHeap[f[x]][y].insert(d[x]);
				if(c[f[x]]!=y)
					global.insert(*colorHeap[f[x]][y].begin());
			}
			c[x]=y;
		}
		printf("%d\n",*global.begin());
	}
	return 0;
}
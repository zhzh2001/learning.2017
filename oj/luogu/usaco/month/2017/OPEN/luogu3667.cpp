#include<bits/stdc++.h>
using namespace std;
const int N=1005;
string s[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=2*n;i++)
		cin>>s[i];
	int l=1,r=m;
	while(l<r)
	{
		int mid=(l+r)/2;
		bool flag=false;
		for(int i=0;i+mid<=m;i++)
		{
			bool now=true;
			unordered_set<string> S;
			for(int j=1;j<=n;j++)
				S.insert(s[j].substr(i,mid));
			for(int j=n+1;j<=2*n;j++)
				if(S.find(s[j].substr(i,mid))!=S.end())
				{
					now=false;
					break;
				}
			if(now)
			{
				flag=true;
				break;
			}
		}
		if(flag)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}
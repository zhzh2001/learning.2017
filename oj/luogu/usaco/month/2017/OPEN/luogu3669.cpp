#include<bits/stdc++.h>
using namespace std;
const int N=100005;
pair<int,int> a[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i].second>>a[i].first;
	sort(a+1,a+n+1);
	int ans=0;
	for(int i=1,j=n;i<=j;)
	{
		int now=min(a[i].second,a[j].second);
		if(i==j)
			now/=2;
		ans=max(ans,a[i].first+a[j].first);
		if(!(a[i].second-=now))
			i++;
		if(!(a[j].second-=now))
			j--;
	}
	cout<<ans<<endl;
	return 0;
}
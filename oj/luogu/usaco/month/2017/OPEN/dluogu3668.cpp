#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],l[N],r[N],layer[N];
int main()
{
	int n;
	cin>>n;
	fill(l+1,l+n+1,n);
	for(int i=1;i<=n;i++)
	{
		cin>>a[i];
		if(a[i])
		{
			l[a[i]]=min(l[a[i]],i);
			r[a[i]]=max(r[a[i]],i);
		}
	}
	int cnt=0,ans=0;
	for(int i=1;i<=n;i++)
	{
		if(!a[i])
			continue;
		if(l[a[i]]==i)
		{
			layer[a[i]]=++cnt;
			ans=max(ans,cnt);
		}
		if(r[a[i]]==i)
			if(layer[a[i]]!=cnt--)
			{
				cout<<-1<<endl;
				return 0;
			}
	}
	cout<<ans<<endl;
	return 0;
}
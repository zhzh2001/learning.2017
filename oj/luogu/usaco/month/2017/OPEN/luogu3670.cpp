#include<bits/stdc++.h>
using namespace std;
const int N=505,M=55;
int s[N][M],t[N][M];
inline int genome2int(char c)
{
	switch(c)
	{
		case 'A':return 0;
		case 'C':return 1;
		case 'G':return 2;
		case 'T':return 3;
	}
	return -1;
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		string str;
		cin>>str;
		for(int j=0;j<m;j++)
			s[i][j+1]=genome2int(str[j]);
	}
	for(int i=1;i<=n;i++)
	{
		string str;
		cin>>str;
		for(int j=0;j<m;j++)
			t[i][j+1]=genome2int(str[j]);
	}
	int ans=0;
	for(int i=1;i<=m;i++)
		for(int j=i+1;j<=m;j++)
			for(int k=j+1;k<=m;k++)
			{
				bool vis[4][4][4]={};
				for(int I=1;I<=n;I++)
					vis[s[I][i]][s[I][j]][s[I][k]]=true;
				bool flag=true;
				for(int I=1;I<=n;I++)
					if(vis[t[I][i]][t[I][j]][t[I][k]])
					{
						flag=false;
						break;
					}
				ans+=flag;
			}
	cout<<ans<<endl;
	return 0;
}
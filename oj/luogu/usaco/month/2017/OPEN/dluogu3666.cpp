#include<bits/stdc++.h>
using namespace std;
const int N=105,MOD=1e9+7;
int n,cnt[N];
struct matrix
{
	long long mat[N][N];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=1;i<=n;i++)
			for(int k=1;k<=n;k++)
				for(int j=1;j<=n;j++)
				{
					ans.mat[i][j]=(ans.mat[i][j]+mat[i][k]*rhs.mat[k][j])%MOD;
					assert(ans.mat[i][j]>=0);
				}
		return ans;
	}
	matrix operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
}S[N];
matrix I()
{
	matrix ans;
	for(int i=1;i<=n;i++)
		ans.mat[i][i]=1;
	return ans;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
string code[N];
template<typename T>
inline T get_token(const string& s)
{
	stringstream ss(s);
	T ret;
	ss>>ret;
	return ret;
}
int main()
{
	map<string,int> var;
	var["1"]=1;
	int lines=0;
	n=1;
	while(getline(cin,code[++lines]))
		if(code[lines].find('=')!=string::npos)
		{
			string name=get_token<string>(code[lines]);
			if(var.find(name)==var.end())
				var[name]=++n;
		}
	lines--;
	int sp=1;
	S[sp]=I();
	for(int i=1;i<=lines;i++)
		if(code[i].substr(0,6)=="RETURN")
			cout<<S[sp].mat[var[get_token<string>(code[i].substr(6))]][1]<<endl;
		else
			if(code[i].find("MOO")!=string::npos)
			{
				S[++sp]=I();
				cnt[sp]=get_token<int>(code[i]);
			}
			else
				if(code[i].find('}')!=string::npos)
				{
					S[sp-1]=qpow(S[sp],cnt[sp])*S[sp-1];
					sp--;
				}
				else
				{
					matrix now;
					int row=var[get_token<string>(code[i])],p=code[i].find('=')+1;
					stringstream ss(code[i].substr(p));
					string token;
					while(ss>>token)
					{
						if(isdigit(token[0]))
							now.mat[row][1]+=get_token<int>(token);
						else
							if(isalpha(token[0]))
								now.mat[row][var[token]]++;
					}
					for(int i=1;i<=n;i++)
						if(i!=row)
							now.mat[i][i]=1;
					S[sp]=now*S[sp];
				}
	return 0;
}
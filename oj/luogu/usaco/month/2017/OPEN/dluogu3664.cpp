#include<bits/stdc++.h>
using namespace std;
const int N=1005;
int a[N][N],minx[N*N],maxx[N*N],miny[N*N],maxy[N*N],cnt[N*N],delta[N][N],s[N][N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n*n;i++)
		minx[i]=miny[i]=n;
	int cc=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
		{
			cin>>a[i][j];
			if(!a[i][j])
				continue;
			if(!cnt[a[i][j]]++)
				cc++;
			minx[a[i][j]]=min(minx[a[i][j]],i);
			maxx[a[i][j]]=max(maxx[a[i][j]],i);
			miny[a[i][j]]=min(miny[a[i][j]],j);
			maxy[a[i][j]]=max(maxy[a[i][j]],j);
		}
	if(cc==1)
	{
		cout<<n*n-1<<endl;
		return 0;
	}
	for(int i=1;i<=n*n;i++)
		if(cnt[i])
		{
			delta[minx[i]][miny[i]]++;
			delta[minx[i]][maxy[i]+1]--;
			delta[maxx[i]+1][miny[i]]--;
			delta[maxx[i]+1][maxy[i]+1]++;
		}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			s[i][j]=s[i][j-1]+s[i-1][j]-s[i-1][j-1]+delta[i][j];
	int ans=n*n-cc;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(a[i][j]&&cnt[a[i][j]]&&s[i][j]>1)
				cnt[a[i][j]]=0;
	for(int i=1;i<=n*n;i++)
		if(cnt[i])
			ans++;
	cout<<ans<<endl;
	return 0;
}
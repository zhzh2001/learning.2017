#include <iostream>
#include <string>
#include <algorithm>
#include <queue>
using namespace std;
const int N = 20, dx[] = {-1, 0, 1, 0}, dy[] = {0, 1, 0, -1}, INF = 0x3f3f3f3f;
string mat[N];
int d[N][N][4][N][N][4];
struct node
{
	int x, y, dir;
	node(int x, int y, int dir) : x(x), y(y), dir(dir) {}
};
typedef pair<node, node> state;
queue<state> Q;
inline void update(const state &s, int val)
{
	if (d[s.first.x][s.first.y][s.first.dir][s.second.x][s.second.y][s.second.dir] == INF)
	{
		d[s.first.x][s.first.y][s.first.dir][s.second.x][s.second.y][s.second.dir] = val;
		Q.push(s);
	}
}
int main()
{
	int n;
	cin >> n;
	for (int i = 0; i < n; i++)
		cin >> mat[i];
	fill_n(&d[0][0][0][0][0][0], sizeof(d) / sizeof(int), INF);
	update(make_pair(node(n - 1, 0, 0), node(n - 1, 0, 1)), 0);
	while (!Q.empty())
	{
		state k = Q.front();
		Q.pop();
		bool done1 = k.first.x == 0 && k.first.y == n - 1, done2 = k.second.x == 0 && k.second.y == n - 1;
		int nx = k.first.x + dx[k.first.dir], ny = k.first.y + dy[k.first.dir], newd = d[k.first.x][k.first.y][k.first.dir][k.second.x][k.second.y][k.second.dir] + 1;
		if (done1 && done2)
		{
			cout << newd - 1 << endl;
			return 0;
		}
		state now = k;
		if (!done1 && nx >= 0 && nx < n && ny >= 0 && ny < n && mat[nx][ny] == 'E')
		{
			now.first.x = nx;
			now.first.y = ny;
		}
		nx = k.second.x + dx[k.second.dir], ny = k.second.y + dy[k.second.dir];
		if (!done2 && nx >= 0 && nx < n && ny >= 0 && ny < n && mat[nx][ny] == 'E')
		{
			now.second.x = nx;
			now.second.y = ny;
		}
		update(now, newd);
		update(make_pair(node(k.first.x, k.first.y, (k.first.dir + 3) % 4), node(k.second.x, k.second.y, (k.second.dir + 3) % 4)), newd);
		update(make_pair(node(k.first.x, k.first.y, (k.first.dir + 1) % 4), node(k.second.x, k.second.y, (k.second.dir + 1) % 4)), newd);
	}
	cout << -1 << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, K = 21;
int a[N], f[N][K][3];
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	cin >> n >> k;
	for (int i = 1; i <= n; i++)
	{
		char c;
		cin >> c;
		switch (c)
		{
		case 'H':
			a[i] = 0;
			break;
		case 'P':
			a[i] = 1;
			break;
		case 'S':
			a[i] = 2;
			break;
		}
	}
	for (int i = 1; i <= n; i++)
		for (int j = 0; j < i && j <= k; j++)
			for (int now = 0; now < 3; now++)
			{
				f[i][j][now] = f[i - 1][j][now];
				if (j)
					for (int p = 0; p < 3; p++)
						f[i][j][now] = max(f[i][j][now], f[i - 1][j - 1][p]);
				f[i][j][now] += a[i] == now;
			}
	cout << max(f[n][k][0], max(f[n][k][1], f[n][k][2])) << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,a[N],b[N],l[N],r[N];
struct BIT
{
	int tree[N];
	void clear()
	{
		memset(tree,0,sizeof(tree));
	}
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	memcpy(b,a,sizeof(b));
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
		a[i]=lower_bound(b+1,b+n+1,a[i])-b;
	for(int i=1;i<=n;i++)
	{
		T.modify(a[i],1);
		l[i]=i-T.query(a[i]);
	}
	T.clear();
	for(int i=n;i;i--)
	{
		T.modify(a[i],1);
		r[i]=n-i+1-T.query(a[i]);
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=max(l[i],r[i])>2*min(l[i],r[i]);
	cout<<ans<<endl;
	return 0;
}
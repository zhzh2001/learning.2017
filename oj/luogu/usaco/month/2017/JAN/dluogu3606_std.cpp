#include<iostream>
#include<cstdio>
#include<algorithm>
#include<iomanip>
#include<cmath>
#define INF 100000000000000LL
#define eps 1e-13
using namespace std;
#define ll long long
ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-') f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9'){x=x*10+ch-'0'; ch=getchar();}
    return x*f;
} 
 
int n;
ll K;
ll ans=INF;
double a[100005];
 
ll cal(double x){return (ll)((sqrt(1+4*x)-1)/2);}
 
bool check(double x)
{
    ll left=K;
    for(int i=1;i<=n;i++)
    {
        if(a[i]<=x) continue;
        ll xx=cal(a[i]/x);
        left-=xx;
    }
    //cout<<x<<" "<<left<<endl;
    if(left<0) return false;
    return true;
}
 
int main()
{
    n=read();K=read()-n;
    for(int i=1;i<=n;i++) a[i]=read();
    double l=eps,r=1e12,mid;
    for(int i=1;i<=200;i++)  
    {
        mid=(l+r)/2;
        if(check(mid)) r=mid;
        else l=mid;
    }
    int i;
    for(i=1,l=0;i<=n;++i)l+=a[i]/(double)(cal(a[i]/r)+1);
    cout<<(ll)(l+0.5);
    return 0;
}
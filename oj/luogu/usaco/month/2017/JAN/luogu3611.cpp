#include <iostream>
#include <queue>
#include <vector>
using namespace std;
const int N = 10005;
int a[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, t;
	cin >> n >> t;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	int l = 1, r = n;
	while (l < r)
	{
		int mid = (l + r) / 2;
		priority_queue<int, vector<int>, greater<int> > Q;
		for (int i = 1; i <= mid; i++)
			Q.push(a[i]);
		int j = mid + 1, now;
		while (!Q.empty())
		{
			int k = Q.top();
			Q.pop();
			if (j <= n)
				Q.push(k + a[j++]);
			if (Q.empty())
				now = k;
		}
		if (now > t)
			l = mid + 1;
		else
			r = mid;
	}
	cout << r << endl;
	return 0;
}
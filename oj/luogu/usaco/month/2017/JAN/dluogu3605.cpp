#include<bits/stdc++.h>
using namespace std;
const int N=100005;
vector<int> mat[N];
struct node
{
	int id,val;
	bool operator>(const node& rhs)const
	{
		return val>rhs.val;
	}
}a[N];
int n,t[N],cnt[N],f[N],now;
void dfs(int k)
{
	t[k]=++now;
	cnt[k]++;
	for(vector<int>::iterator i=mat[k].begin();i!=mat[k].end();++i)
	{
		dfs(*i);
		cnt[k]+=cnt[*i];
	}
}
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].val;
		a[i].id=i;
	}
	for(int i=2;i<=n;i++)
	{
		int u;
		cin>>u;
		mat[u].push_back(i);
	}
	dfs(1);
	sort(a+1,a+n+1,greater<node>());
	for(int i=1;i<=n;i++)
	{
		int id=a[i].id;
		f[id]=T.query(t[id]+cnt[id]-1)-T.query(t[id]-1);
		T.modify(t[id],1);
	}
	for(int i=1;i<=n;i++)
		cout<<f[i]<<endl;
	return 0;
}
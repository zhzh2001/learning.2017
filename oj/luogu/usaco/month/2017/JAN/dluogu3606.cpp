#include<bits/stdc++.h>
using namespace std;
const int N=100005;
long long a[N];
inline double calc(double x)
{
	return floor((-1+sqrt(1+4*x))/2);
}
int main()
{
	int n;
	long long k;
	cin>>n>>k;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	double l=0,r=1e12;
	for(int t=1;t<=100;t++)
	{
		double mid=(l+r)/2;
		long long now=0;
		for(int i=1;i<=n;i++)
			now+=calc(a[i]/mid);
		if(now+n<=k)
			r=mid;
		else
			l=mid;
	}
	double ans=0;
	for(int i=1;i<=n;i++)
		ans+=a[i]/(calc(a[i]/r)+1);
	cout.precision(0);
	cout<<fixed<<ans<<endl;
	return 0;
}
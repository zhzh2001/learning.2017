#include<bits/stdc++.h>
using namespace std;
const int N=20005;
struct cow
{
	int l,r;
	bool operator<(const cow& rhs)const
	{
		return r<rhs.r;
	}
}a[N];
int main()
{
	int n,m;
	cin>>n>>m;
	multiset<int> S;
	for(int i=1;i<=n;i++)
	{
		int t;
		cin>>t;
		S.insert(t);
	}
	for(int i=1;i<=m;i++)
		cin>>a[i].l>>a[i].r;
	sort(a+1,a+m+1);
	int ans=0;
	for(int i=1;i<=m;i++)
	{
		multiset<int>::iterator p=S.lower_bound(a[i].l);
		if(p!=S.end()&&*p<=a[i].r)
		{
			ans++;
			S.erase(p);
		}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int s[N];
int main()
{
	int n,k,b;
	cin>>n>>k>>b;
	while(b--)
	{
		int x;
		cin>>x;
		s[x]++;
	}
	int ans=n;
	for(int i=1;i<=n;i++)
	{
		s[i]+=s[i-1];
		if(i>=k)
			ans=min(ans,s[i]-s[i-k]);
	}
	cout<<ans<<endl;
	return 0;
}
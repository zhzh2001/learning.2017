#include<bits/stdc++.h>
using namespace std;
const int N=105,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
bool mat[N][N][4];
struct point
{
	int x,y;
}a[N];
int n,vis[N][N];
void dfs(int x,int y,int now)
{
	vis[x][y]=now;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(!mat[x][y][i]&&nx&&nx<=n&&ny&&ny<=n&&vis[nx][ny]<now)
			dfs(nx,ny,now);
	}
}
int main()
{
	int k,r;
	cin>>n>>k>>r;
	while(r--)
	{
		int r1,c1,r2,c2;
		cin>>r1>>c1>>r2>>c2;
		if(r1==r2+1)
			mat[r1][c1][0]=mat[r2][c2][1]=true;
		else
			if(r1==r2-1)
				mat[r1][c1][1]=mat[r2][c2][0]=true;
			else
				if(c1==c2+1)
					mat[r1][c1][2]=mat[r2][c2][3]=true;
				else
					mat[r1][c1][3]=mat[r2][c2][2]=true;
	}
	for(int i=1;i<=k;i++)
		cin>>a[i].x>>a[i].y;
	int ans=0;
	for(int i=1;i<=k;i++)
	{
		dfs(a[i].x,a[i].y,i);
		for(int j=i+1;j<=k;j++)
			ans+=vis[a[j].x][a[j].y]<i;
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=105,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int a[N][N],d[N][N][3];
bool inQ[N][N][3];
struct node
{
	int x,y,status;
	node(int x,int y,int status):x(x),y(y),status(status){}
};
int main()
{
	int n,t;
	cin>>n>>t;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>a[i][j];
	queue<node> Q;
	Q.push(node(1,1,0));
	inQ[1][1][0]=true;
	memset(d,0x3f,sizeof(d));
	d[1][1][0]=0;
	while(!Q.empty())
	{
		node k=Q.front();Q.pop();
		inQ[k.x][k.y][k.status]=false;
		for(int i=0;i<4;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			if(nx&&nx<=n&&ny&&ny<=n)
			{
				int now=d[k.x][k.y][k.status]+(k.status==2?a[nx][ny]:0)+t;
				if(now<d[nx][ny][(k.status+1)%3])
				{
					d[nx][ny][(k.status+1)%3]=now;
					if(!inQ[nx][ny][(k.status+1)%3])
					{
						Q.push(node(nx,ny,(k.status+1)%3));
						inQ[nx][ny][(k.status+1)%3]=true;
					}
				}
			}
		}
	}
	cout<<min(d[n][n][0],min(d[n][n][1],d[n][n][2]))<<endl;
	return 0;
}
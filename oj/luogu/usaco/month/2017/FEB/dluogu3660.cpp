#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,a[N];
struct BIT
{
	int tree[N];
	void modify(int x,int val)
	{
		for(;x<=2*n;x+=x&-x)
			tree[x]+=val;
	}
	int query(int x)
	{
		int ans=0;
		for(;x;x-=x&-x)
			ans+=tree[x];
		return ans;
	}
}T;
int main()
{
	cin>>n;
	unsigned ans=0;
	for(int i=1;i<=2*n;i++)
	{
		int id;
		cin>>id;
		if(!a[id])
			T.modify(a[id]=i,1);
		else
		{
			ans+=T.query(i)-T.query(a[id]);
			T.modify(a[id],-1);
		}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=105,INF=0x3f3f3f3f,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,t,a[N][N],f[N][N][3];
int dp(int x,int y,int status)
{
	if(x==1&&y==1)
		if(!status)
			return 0;
		else
			return INF;
	int& now=f[x][y][status];
	if(now<INF)
		return now;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=x+dy[i];
		if(nx&&nx<=n&&ny&&ny<=n)
			now=min(now,dp(nx,ny,(status+2)%3)+t);
	}
	if(!status)
		now+=a[x][y];
	return now;
}
int main()
{
	cin>>n>>t;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>a[i][j];
	memset(f,0x3f,sizeof(f));
	cout<<min(dp(n,n,0),min(dp(n,n,1),dp(n,n,2)))<<endl;
	return 0;
}
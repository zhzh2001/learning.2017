#include<bits/stdc++.h>
using namespace std;
const int N=50005;
pair<int,int> station[N];
int nxt[N];
int main()
{
	int n,cap,now,d;
	cin>>n>>cap>>now>>d;
	for(int i=1;i<=n;i++)
		cin>>station[i].first>>station[i].second;
	sort(station+1,station+n+1);
	station[n+1]=make_pair(d,0);
	stack<int> S;
	for(int i=n+1;i;i--)
	{
		while(!S.empty()&&station[S.top()].second>=station[i].second)
			S.pop();
		if(!S.empty())
			nxt[i]=S.top();
		S.push(i);
	}
	now-=station[1].first;
	long long ans=0;
	for(int i=1;i<=n+1;i++)
	{
		if(now<0)
		{
			cout<<-1<<endl;
			return 0;
		}
		if(i<=n)
		{
			int request=min(cap,station[nxt[i]].first-station[i].first);
			if(request>now)
			{
				ans+=(long long)(request-now)*station[i].second;
				now=request;
			}
			now-=station[i+1].first-station[i].first;
		}
	}
	cout<<ans<<endl;
	return 0;
}
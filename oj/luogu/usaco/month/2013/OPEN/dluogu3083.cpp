#include <iostream>
#include <cstring>
using namespace std;
const int N = 1005, M = 505;
int mat[N][2], vis[N], p[N];
bool ch[M];
int main()
{
	ios::sync_with_stdio(false);
	int n, m, k;
	cin >> n >> m >> k;
	for (int i = 1; i <= n; i++)
		cin >> mat[i][0] >> mat[i][1];
	for (int i = 1; i <= m; i++)
	{
		char c;
		cin >> c;
		ch[i] = c == 'R';
	}
	int now = 1;
	memset(vis, -1, sizeof(vis));
	for (int i = 0; i < k; i++)
	{
		p[i] = now;
		if (~vis[now])
		{
			cout << p[vis[now] + (k - vis[now]) % (i - vis[now])] << endl;
			return 0;
		}
		vis[now] = i;
		for (int i = 1; i <= m; i++)
			now = mat[now][ch[i]];
	}
	cout << now << endl;
	return 0;
}
#include <iostream>
#include <string>
#include <cstring>
#include <queue>
#include <cstdlib>
using namespace std;
const int N = 500, INF = 0x3f3f3f3f;
string mat[N];
int d[N][N][2];
bool vis[N][N][2];
struct node
{
	int x, y, dist;
	bool dir;
	node(int x = 0, int y = 0, bool dir = 0) : x(x), y(y), dist(0), dir(dir) {}
	bool operator<(const node &rhs) const
	{
		return dist > rhs.dist;
	}
};
int n, m, tx, ty;
bool fall(node &k, int ans)
{
	if (k.x == tx && k.y == ty)
	{
		cout << ans << endl;
		exit(0);
	}
	int dx = k.dir ? 1 : -1;
	while (k.x + dx >= 0 && k.x + dx < n && mat[k.x + dx][k.y] != '#')
	{
		k.x += dx;
		if (k.x == tx && k.y == ty)
		{
			cout << ans << endl;
			exit(0);
		}
	}
	return k.x && k.x < n - 1;
}
int main()
{
	ios::sync_with_stdio(false);
	cin >> n >> m;
	node src;
	for (int i = 0; i < n; i++)
	{
		cin >> mat[i];
		for (int j = 0; j < m; j++)
			if (mat[i][j] == 'C')
				src = node(i, j, true);
			else if (mat[i][j] == 'D')
				tx = i, ty = j;
	}
	memset(d, 0x3f, sizeof(d));
	priority_queue<node> Q;
	if (!fall(src, 0))
	{
		cout << -1 << endl;
		return 0;
	}
	src.dist = d[src.x][src.y][src.dir] = 0;
	Q.push(src);
	while (!Q.empty())
	{
		node k = Q.top();
		Q.pop();
		if (vis[k.x][k.y][k.dir])
			continue;
		vis[k.x][k.y][k.dir] = true;
		node now;
		if (k.y && mat[k.x][k.y - 1] != '#' && fall(now = node(k.x, k.y - 1, k.dir), k.dist) && k.dist < d[now.x][now.y][now.dir])
		{
			now.dist = d[now.x][now.y][now.dir] = k.dist;
			Q.push(now);
		}
		if (k.y + 1 < m && mat[k.x][k.y + 1] != '#' && fall(now = node(k.x, k.y + 1, k.dir), k.dist) && k.dist < d[now.x][now.y][now.dir])
		{
			now.dist = d[now.x][now.y][now.dir] = k.dist;
			Q.push(now);
		}
		if (fall(now = node(k.x, k.y, !k.dir), k.dist + 1) && k.dist + 1 < d[now.x][now.y][now.dir])
		{
			now.dist = d[now.x][now.y][now.dir] = k.dist + 1;
			Q.push(now);
		}
	}
	cout << -1 << endl;
	return 0;
}
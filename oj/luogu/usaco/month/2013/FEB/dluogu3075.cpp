#include<bits/stdc++.h>
using namespace std;
const int N=20,INF=0x3f3f3f3f;
int a[N][N],c[N][N],t[N][N],f[N][N];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>a[i][j];
	int ans=INF;
	for(int mask=0;mask<1<<(n-1);mask++)
	{
		int cnt=k-__builtin_popcount(mask)+1;
		if(cnt<=0||cnt>n)
			continue;
		for(int i=1;i<=n;i++)
		{
			memset(t,0,sizeof(t));
			for(int j=i;j<=n;j++)
			{
				int cc=1;
				for(int k=1;k<=n;k++)
				{
					t[j][cc]+=a[k][j];
					if(mask&(1<<(k-1)))
						cc++;
				}
				c[i][j]=0;
				for(int k=1;k<=cc;k++)
				{
					t[j][k]+=t[j-1][k];
					c[i][j]=max(c[i][j],t[j][k]);
				}
			}
		}
		memset(f,0x3f,sizeof(f));
		for(int i=1;i<=n;i++)
			f[i][1]=c[1][i];
		for(int i=1;i<=n;i++)
			for(int j=2;j<=i&&j<=cnt;j++)
			{
				f[i][j]=INF;
				for(int k=1;k<i;k++)
					f[i][j]=min(f[i][j],max(f[k][j-1],c[k+1][i]));
			}
		ans=min(ans,f[n][cnt]);
	}
	cout<<ans<<endl;
	return 0;
}

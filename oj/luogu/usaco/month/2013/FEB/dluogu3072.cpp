#include<bits/stdc++.h>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
unordered_map<int,unordered_set<int>> bale,vis;
int main()
{
	int n;
	cin>>n;
	pair<int,int> s=make_pair(1e6,1e6);
	while(n--)
	{
		int x,y;
		cin>>x>>y;
		bale[x].insert(y);
		s=min(s,make_pair(x,y));
	}
	s.first--;
	queue<pair<int,int>> Q;
	Q.push(s);
	vis[s.first].insert(s.second);
	int ans=0;
	while(!Q.empty())
	{
		pair<int,int> k=Q.front();Q.pop();
		bool flag=false;
		for(int dx=-1;dx<=1;dx++)
			for(int dy=-1;dy<=1;dy++)
				if(bale[k.first+dx].find(k.second+dy)!=bale[k.first+dx].end())
				{
					flag=true;
					break;
				}
		if(flag)
			for(int i=0;i<4;i++)
			{
				int nx=k.first+dx[i],ny=k.second+dy[i];
				if(bale[nx].find(ny)!=bale[nx].end())
					ans++;
				else
					if(vis[nx].find(ny)==vis[nx].end())
					{
						Q.push(make_pair(nx,ny));
						vis[nx].insert(ny);
					}
			}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=505,dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,a[N][N],vis[N][N],sz;
void floodfill(int x,int y,int id,int delta)
{
	vis[x][y]=id;
	sz++;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx&&nx<=n&&ny&&ny<=n&&vis[nx][ny]<id&&abs(a[nx][ny]-a[x][y])<=delta)
			floodfill(nx,ny,id,delta);
	}
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>a[i][j];
	int l=0,r=1e6,cnt=0;
	while(l<r)
	{
		int mid=(l+r)/2;
		cnt++;
		bool flag=false;
		for(int i=1;i<=n;i++)
		{
			for(int j=1;j<=n;j++)
				if(vis[i][j]<cnt)
				{
					sz=0;
					floodfill(i,j,cnt,mid);
					if(sz>=(n*n+1)/2)
					{
						flag=true;
						break;
					}
				}
			if(flag)
				break;
		}
		if(flag)
			r=mid;
		else
			l=mid+1;
	}
	cout<<r<<endl;
	return 0;
}

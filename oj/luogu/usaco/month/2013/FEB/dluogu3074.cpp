#include<bits/stdc++.h>
using namespace std;
const int N=10005;
int a[N],into[N],t[N];
vector<int> mat[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		into[v]++;
	}
	queue<int> Q;
	for(int i=1;i<=n;i++)
		if(into[i]==0)
		{
			t[i]=a[i];
			Q.push(i);
		}
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(auto v:mat[k])
		{
			t[v]=max(t[v],t[k]+a[v]);
			if(--into[v]==0)
				Q.push(v);
		}
	}
	cout<<*max_element(t+1,t+n+1)<<endl;
	return 0;
}

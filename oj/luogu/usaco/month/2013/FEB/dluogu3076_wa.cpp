#include<bits/stdc++.h>
using namespace std;
const int N=100005;
pair<int,int> a[N];
int x[N*2],cnt1[N*2],cnt2[N*2];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].first>>a[i].second;
		x[2*i-1]=a[i].first;
		x[2*i]=a[i].second;
	}
	sort(x+1,x+n*2+1);
	for(int i=1;i<=n;i++)
		if(a[i].first<=a[i].second)
		{
			cnt1[lower_bound(x+1,x+n*2+1,a[i].first)-x]++;
			cnt1[lower_bound(x+1,x+n*2+1,a[i].second)-x]--;
		}
		else
		{
			cnt2[lower_bound(x+1,x+n*2+1,a[i].second)-x]++;
			cnt2[lower_bound(x+1,x+n*2+1,a[i].first)-x]--;
		}
	long long ans=0;
	int now1=0,now2=0;
	for(int i=1;i<=2*n;i++)
	{
		now1+=cnt1[i];
		now2+=cnt2[i];
		if(now1<=now2)
			ans+=2ll*now2*(x[i]-x[i-1]);
		else
			ans+=(2ll*now2+2ll*(now1-now2)-1)*(x[i]-x[i-1]);
	}
	if(x[1])
		ans+=x[1];
	if(x[2*n]<m)
		ans+=m-x[2*n];
	cout<<ans<<endl;
	return 0;
}
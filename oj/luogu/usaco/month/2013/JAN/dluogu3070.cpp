#include<bits/stdc++.h>
using namespace std;
const int R=50,N=15,dx[]={-1,1,0,0},dy[]={0,0,-1,1},INF=0x3f3f3f3f;
int r,c,belong[R][R],d[N][N],dist[R][R],f[1<<N][N];
string mat[R];
vector<pair<int,int>> comp[N];
void floodfill(int x,int y,int id)
{
	belong[x][y]=id;
	comp[id].push_back(make_pair(x,y));
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=0&&nx<r&&ny>=0&&ny<c&&mat[nx][ny]=='X'&&belong[nx][ny]==-1)
			floodfill(nx,ny,id);
	}
}
int main()
{
	cin>>r>>c;
	for(int i=0;i<r;i++)
		cin>>mat[i];
	memset(belong,-1,sizeof(belong));
	int n=0;
	for(int i=0;i<r;i++)
		for(int j=0;j<c;j++)
			if(mat[i][j]=='X'&&belong[i][j]==-1)
				floodfill(i,j,n++);
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<r;j++)
			for(int k=0;k<c;k++)
				dist[j][k]=INF;
		queue<pair<int,int>> Q;
		for(auto j:comp[i])
		{
			dist[j.first][j.second]=0;
			Q.push(j);
		}
		while(!Q.empty())
		{
			pair<int,int> k=Q.front();Q.pop();
			for(int j=0;j<4;j++)
			{
				int nx=k.first+dx[j],ny=k.second+dy[j];
				if(nx>=0&&nx<r&&ny>=0&&ny<c&&mat[nx][ny]!='.'&&dist[k.first][k.second]+(mat[nx][ny]=='S')<dist[nx][ny])
				{
					dist[nx][ny]=dist[k.first][k.second]+(mat[nx][ny]=='S');
					Q.push(make_pair(nx,ny));
				}
			}
		}
		for(int j=0;j<n;j++)
		{
			d[i][j]=INF;
			for(auto k:comp[j])
				d[i][j]=min(d[i][j],dist[k.first][k.second]);
		}
	}
	for(int i=0;i<n;i++)
		f[1<<i][i]=0;
	for(int i=1;i<1<<n;i++)
		if(i&(i-1))
			for(int j=0;j<n;j++)
			{
				f[i][j]=INF;
				for(int k=0;k<n;k++)
					if(k!=j&&i&(1<<k))
						f[i][j]=min(f[i][j],f[i^(1<<j)][k]+d[j][k]);
			}
	int ans=INF;
	for(int i=0;i<n;i++)
		ans=min(ans,f[(1<<n)-1][i]);
	cout<<ans<<endl;
	return 0;
}
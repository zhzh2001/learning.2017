#include<bits/stdc++.h>
using namespace std;
const int N=1000005,M=250005;
vector<int> belong[N];
unordered_set<int> group[M];
bool vis[N];
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=m;i++)
	{
		int cnt;
		cin>>cnt;
		while(cnt--)
		{
			int x;
			cin>>x;
			group[i].insert(x);
			belong[x].push_back(i);
		}
	}
	queue<int> Q;
	Q.push(1);
	vis[1]=true;
	int ans=1;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(auto g:belong[k])
		{
			group[g].erase(k);
			if(group[g].size()==1&&!vis[*group[g].begin()])
			{
				Q.push(*group[g].begin());
				vis[*group[g].begin()]=true;
				ans++;
			}
		}
	}
	cout<<ans<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],b[N],sum[N];
int main()
{
	int n,k;
	cin>>n>>k;
	for(int i=1;i<=n;i++)
	{
		int delta;
		char opt;
		cin>>delta>>opt;
		if(opt=='R')
			a[i]=a[i-1]+delta;
		else
			a[i]=a[i-1]-delta;
	}
	memcpy(b,a,sizeof(b));
	sort(b,b+n+1);
	#define find(x) lower_bound(b,b+n+1,x)-b+1
	int pred=find(0);
	for(int i=1;i<=n;i++)
	{
		int now=find(a[i]);
		if(now>pred)
		{
			sum[pred]++;
			sum[now]--;
		}
		else
		{
			sum[now]++;
			sum[pred]--;
		}
		pred=now;
	}
	int now=0,ans=0;
	for(int i=1;i<=n;i++)
	{
		now+=sum[i];
		if(now>=k)
			ans+=b[i]-b[i-1];
	}
	cout<<ans<<endl;
	return 0;
}
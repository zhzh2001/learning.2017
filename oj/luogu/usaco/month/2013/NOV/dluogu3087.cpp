#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <sstream>
using namespace std;
const int G = 30, N = 100;
string line[N];
vector<string> adj[G];
int ans[G];
int main()
{
	int n, k;
	cin >> n >> k;
	getline(cin, line[0]);
	int nadj;
	for (int i = 0; i < n; i++)
	{
		getline(cin, line[i]);
		stringstream ss(line[i]);
		string token;
		ss >> token >> token >> token >> token;
		int j;
		for (j = 0; ss >> token && token != "cow."; j++)
			adj[j].push_back(token);
		nadj = j;
	}
	sort(line, line + n);
	for (int i = 0; i < nadj; i++)
	{
		sort(adj[i].begin(), adj[i].end());
		adj[i].resize(unique(adj[i].begin(), adj[i].end()) - adj[i].begin());
	}
	for (int i = n - 1; i >= 0; i--)
	{
		stringstream ss(line[i]);
		string token;
		ss >> token >> token >> token >> token;
		int now = 0;
		for (int j = 0; j < nadj && ss >> token; j++)
			now = now * adj[j].size() + lower_bound(adj[j].begin(), adj[j].end(), token) - adj[j].begin();
		if (now - i < k)
		{
			k += i;
			for (int j = nadj - 1; j >= 0; j--)
			{
				ans[j] = k % adj[j].size();
				k /= adj[j].size();
			}
			break;
		}
	}
	for (int i = 0; i < nadj; i++)
		cout << adj[i][ans[i]] << ' ';
	cout << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005, K = 16;
int coin[K], p[N], f[1 << K];
int main()
{
	ios::sync_with_stdio(false);
	int k, n;
	cin >> k >> n;
	for (int i = 0; i < k; i++)
		cin >> coin[i];
	for (int i = 1; i <= n; i++)
	{
		cin >> p[i];
		p[i] += p[i - 1];
	}
	int ans = -1;
	for (int S = 1; S < 1 << k; S++)
	{
		int now = 0;
		for (int i = 0; i < k; i++)
			if (S & (1 << i))
				f[S] = max(f[S], int(upper_bound(p + 1, p + n + 1, p[f[S ^ (1 << i)]] + coin[i]) - p - 1));
			else
				now += coin[i];
		if (f[S] == n)
			ans = max(ans, now);
	}
	cout << ans << endl;
	return 0;
}
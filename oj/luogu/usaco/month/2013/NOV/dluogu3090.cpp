#include <iostream>
using namespace std;
const int N = 3e6;
int cnt[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	while (m--)
	{
		int x, y, a, b;
		cin >> x >> y >> a >> b;
		for (int i = 1; i <= y; i++)
			cnt[(1ll * a * i + b) % n] += x;
	}
	int now = 0;
	for (int i = 0; i < n; i++)
	{
		now += cnt[i];
		if (now)
			now--;
	}
	for (int i = 0; i < n; i++)
	{
		now += cnt[i];
		if (now)
			now--;
		else
		{
			cout << i << endl;
			break;
		}
	}
	return 0;
}
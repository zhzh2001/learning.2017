#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1005;
int f[N][N];
pair<int, int> a[N];
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i].first >> a[i].second;
	sort(a + 1, a + n + 1);
	int ans = 0;
	for (int t = 0; t < 2; t++)
	{
		a[n + 1].first = 1e9;
		for (int i = n; i; i--)
		{
			int k = n;
			for (int j = n; j > i; j--)
			{
				for (; a[k].first - a[j].first >= a[j].first - a[i].first; k--)
					;
				k++;
				f[i][j] = max(f[i][j + 1], f[j][k] + a[j].second);
			}
			ans = max(ans, f[i][i + 1] + a[i].second);
		}
		reverse(a + 1, a + n + 1);
		for (int i = 1; i <= n; i++)
			a[i].first = -a[i].first;
	}
	cout << ans << endl;
	return 0;
}
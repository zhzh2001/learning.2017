#include<bits/stdc++.h>
using namespace std;
const int N=50005;
pair<int,int> cow[N];
int main()
{
	int n,d;
	cin>>n>>d;
	for(int i=1;i<=n;i++)
		cin>>cow[i].second>>cow[i].first;
	sort(cow+1,cow+n+1);
	set<int> pos;
	int j=n,ans=0;
	for(int i=n;i;i--)
	{
		for(;j&&cow[j].first>=cow[i].first*2;j--)
			pos.insert(cow[j].second);
		set<int>::iterator it=pos.lower_bound(cow[i].second);
		ans+=it!=pos.end()&&*it-cow[i].second<=d&&it--!=pos.begin()&&cow[i].second-*it<=d;
	}
	cout<<ans<<endl;
	return 0;
}
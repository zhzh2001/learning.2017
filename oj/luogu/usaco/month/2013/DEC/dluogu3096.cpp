#include<bits/stdc++.h>
using namespace std;
struct graph
{
	static const int N=20005,M=20005;
	int head[N],v[M],w[M],nxt[M],e;
	void add_edge(int u,int v,int w)
	{
		this->v[++e]=v;
		this->w[e]=w;
		nxt[e]=head[u];
		head[u]=e;
	}
}G,RG;
const int K=205,INF=0x3f3f3f3f;
int n,d[K][graph::N],rd[K][graph::N];
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){}
	bool operator>(const node& rhs)const
	{
		return w>rhs.w;
	}
};
void dijkstra(const graph& G,int *d,int s)
{
	memset(d,0x3f,(n+1)*sizeof(*d));
	d[s]=0;
	static bool vis[graph::N];
	memset(vis,false,sizeof(vis));
	priority_queue<node,vector<node>,greater<node> > Q;
	Q.push(node(s,0));
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(!vis[k.v])
		{
			vis[k.v]=true;
			for(int i=G.head[k.v];i;i=G.nxt[i])
				if(!vis[G.v[i]]&&d[k.v]+G.w[i]<d[G.v[i]])
				{
					d[G.v[i]]=d[k.v]+G.w[i];
					Q.push(node(G.v[i],d[k.v]+G.w[i]));
				}
		}
	}
}
int main()
{
	int m,k,q;
	cin>>n>>m>>k>>q;
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		G.add_edge(u,v,w);
		RG.add_edge(v,u,w);
	}
	for(int i=1;i<=k;i++)
	{
		int u;
		cin>>u;
		dijkstra(G,d[i],u);
		dijkstra(RG,rd[i],u);
	}
	int cnt=0;
	long long sum=0;
	while(q--)
	{
		int u,v;
		cin>>u>>v;
		int now=INF;
		for(int i=1;i<=k;i++)
			now=min(now,rd[i][u]+d[i][v]);
		if(now<INF)
		{
			cnt++;
			sum+=now;
		}
	}
	cout<<cnt<<endl<<sum<<endl;
	return 0;
}
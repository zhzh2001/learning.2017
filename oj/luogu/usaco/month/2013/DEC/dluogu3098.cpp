#include<bits/stdc++.h>
using namespace std;
const int M=100005;
int nxt[M],p[M],d[M];
vector<int> cycles[M];
void find_cycle(int k)
{
	p[k]=k;
	cycles[k].push_back(k);
	for(int i=k;nxt[i]!=k&&i;)
	{
		i=nxt[i];
		cycles[k].push_back(i);
		p[i]=k;
		d[i]=cycles[k].size()-1;
	}
}
int get(int x,int k)
{
	k+=d[x];
	x=p[x];
	return cycles[x][(k+cycles[x].size())%cycles[x].size()];
}
int main()
{
	int n,m,q;
	cin>>n>>m>>q;
	for(int i=1;i<=m;i++)
	{
		cin>>nxt[i];
		nxt[i]--;
	}
	find_cycle(m);
	int sz=cycles[m].size()-1;
	for(int i=1;i<m;i++)
		if(!p[i])
			find_cycle(i);
	map<int,int> ans;
	int cnt=n-m+1;
	for(int i=1;i<=m;i++)
		if(p[i]==m&&sz-d[i]<=cnt)
			ans[sz-d[i]]=i;
		else
			ans[cnt+get(i,cnt)]=i;
	for(int i=max(m+1,n-sz+1);i<=n;i++)
		ans[n-m+1+get(m,n-i+1)]=i;
	while(q--)
	{
		int x;
		cin>>x;
		x=n-x+1;
		if(ans.find(x)!=ans.end())
			cout<<ans[x]<<endl;
		else
			cout<<m+x-sz<<endl;
	}
	return 0;
}

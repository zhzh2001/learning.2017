#include<bits/stdc++.h>
using namespace std;
const int N=40005;
int tree[N*4][2][2];
inline void pullup(int id)
{
	tree[id][1][1]=max(tree[id*2][1][0]+tree[id*2+1][1][1],tree[id*2][1][1]+tree[id*2+1][0][1]);
	tree[id][1][0]=max(tree[id*2][1][0]+tree[id*2+1][1][0],tree[id*2][1][1]+tree[id*2+1][0][0]);
	tree[id][0][1]=max(tree[id*2][0][0]+tree[id*2+1][1][1],tree[id*2][0][1]+tree[id*2+1][0][1]);
	tree[id][0][0]=max(tree[id*2][0][0]+tree[id*2+1][1][0],tree[id*2][0][1]+tree[id*2+1][0][0]);
}
void build(int id,int l,int r)
{
	if(l==r)
		cin>>tree[id][1][1];
	else
	{
		int mid=(l+r)/2;
		build(id*2,l,mid);
		build(id*2+1,mid+1,r);
		pullup(id);
	}
}
int x,val;
void modify(int id,int l,int r)
{
	if(l==r)
		tree[id][1][1]=val;
	else
	{
		int mid=(l+r)/2;
		if(x<=mid)
			modify(id*2,l,mid);
		else
			modify(id*2+1,mid+1,r);
		pullup(id);
	}
}
int main()
{
	int n,m;
	cin>>n>>m;
	build(1,1,n);
	long long ans=0;
	while(m--)
	{
		cin>>x>>val;
		modify(1,1,n);
		ans+=tree[1][1][1];
	}
	cout<<ans<<endl;
	return 0;
}
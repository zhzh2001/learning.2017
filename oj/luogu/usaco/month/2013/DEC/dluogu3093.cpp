#include<bits/stdc++.h>
using namespace std;
const int N=10005;
struct cow
{
	int g,d;
	bool operator>(const cow& rhs)const
	{
		return d>rhs.d;
	}
}c[N];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>c[i].g>>c[i].d;
	sort(c+1,c+n+1,greater<cow>());
	int ans=0,j=1;
	priority_queue<int> Q;
	for(int i=10000;i;i--)
	{
		for(;j<=n&&c[j].d==i;j++)
			Q.push(c[j].g);
		if(!Q.empty())
		{
			ans+=Q.top();Q.pop();
		}
	}
	cout<<ans<<endl;
	return 0;
}
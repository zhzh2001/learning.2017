#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N],trans[N],tmp[N];
int main()
{
	int n,m,q;
	cin>>n>>m>>q;
	for(int i=1;i<=n;i++)
	{
		if(i<=m)
		{
			int x;
			cin>>x;
			trans[x]=i;
		}
		else
			trans[i]=i;
		a[i]=i;
	}
	rotate(trans+1,trans+2,trans+n+1);
	int p=n-m+1;
	for(int t=log2(p);t>=0;t--)
	{
		for(int i=1;i<=n;i++)
			tmp[i]=a[a[i]];
		copy(tmp+1,tmp+n+1,a+1);
		if(p&1<<t)
		{
			for(int i=1;i<=n;i++)
				tmp[i]=a[trans[i]];
			copy(tmp+1,tmp+n+1,a+1);
		}
	}
	int cc=0;
	for(int i=m-1;i;i--)
		tmp[++cc]=a[i];
	for(int i=n;i>=m;i--)
		tmp[++cc]=a[i];
	while(q--)
	{
		int x;
		cin>>x;
		cout<<tmp[x]<<endl;
	}
	return 0;
}
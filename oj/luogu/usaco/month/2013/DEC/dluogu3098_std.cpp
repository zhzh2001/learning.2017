#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <map>
#define optimizar_io ios_base::sync_with_stdio(0);cin.tie(0);
using namespace std;

int n, m, q, d, x;
int A[100003];
int tNext[100003];
vector< int > cycles[100003];
int p[100003], dist[100003];
int large[100003];

void checkCycle( int beg ){
	
	int pos = beg;
	p[beg] = beg;
	cycles[beg].push_back( beg );
	
	while( tNext[pos] != beg && pos != 0 ){
		pos = tNext[pos];
		cycles[beg].push_back( pos );
		p[pos] = beg;
		dist[pos] = ++large[beg]; 
	}
	large[beg]++;
	
}

int move( int x, int k ){
	k += dist[x];
	x = p[x];
	int ret = cycles[x][ (k + large[x]) % large[x] ];
	return ret;
}

map< int, int > ans;

int main(){
	
	optimizar_io
	
	cin >> n >> m >> q;
	for( int i = 1; i <= m; i++ ){
		cin >> tNext[i];
		tNext[i]--;	
	}
	
	checkCycle( m );
	d = large[m] - 1;
	//cout << d << "\n";
	
	for( int i = 1; i < m; i++ )
		if( !p[i] )
			checkCycle( i );
			
	int stps = n - m + 1, r;
	for( int i = 1; i <= m; i++ ){
		if( p[i] == m ){
			if( d - dist[i] <= stps )
				r = d - dist[i];
			else 
				r = stps + move( i, stps );
		} else {
			r = stps + move( i, stps );
		}
		ans[r] = i;
		//cout << i << " " << r << "\n";		
	}
	for( int i = max(m + 1, n - d + 1); i <= n; i++ ){
		r = i - m + n - i + 1 + move( m, n - i + 1 );
		ans[r] = i;
		//cout << i << " " << r << "\n";
	}
	
	for( int i = 1; i <= q; i++ ){
		cin >> x;
		x = n - x + 1;
		if( ans.find(x) != ans.end() ){
			cout << ans[x] << "\n";
			continue;
		}
		x -= d;
		cout << m + x << "\n";
	}
	
	return 0;

}
#include<bits/stdc++.h>
using namespace std;
const int N=1005;
int a[N],f[N][N][2];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		f[i][i][0]=f[i][i][1]=abs(a[i])*n;
	for(int i=n;i;i--)
		for(int j=i+1;j<=n;j++)
		{
			f[i][j][0]=min(f[i+1][j][0]+(a[i+1]-a[i])*(n-(j-i)),f[i+1][j][1]+(a[j]-a[i])*(n-(j-i)));
			f[i][j][1]=min(f[i][j-1][1]+(a[j]-a[j-1])*(n-(j-i)),f[i][j-1][0]+(a[j]-a[i])*(n-(j-i)));
		}
	cout<<min(f[1][n][0],f[1][n][1])<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct segment
{
	int x1,y1,x2,y2,id;
	bool operator<(const segment& rhs)const
	{
		if(x2<rhs.x2)
			return 1ll*(y2-rhs.y1)*(rhs.x2-rhs.x1)<1ll*(x2-rhs.x1)*(rhs.y2-rhs.y1);
		return 1ll*(rhs.y2-y1)*(x2-x1)>1ll*(rhs.x2-x1)*(y2-y1);
	}
}a[N];
struct event
{
	int x,y,id;
	bool operator<(const event& rhs)const
	{
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}e[N*2];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].x1>>a[i].y1>>a[i].x2>>a[i].y2;
		a[i].id=i;
		e[2*i-1].x=a[i].x1;e[2*i-1].y=a[i].y1;
		e[2*i-1].id=i;
		e[2*i].x=a[i].x2;e[2*i].y=a[i].y2;
		e[2*i].id=-i;
	}
	sort(e+1,e+n*2+1);
	int ans=1,now=1;
	set<segment> S;
	for(int i=1;i<=n*2;i++)
		if(e[i].id>0)
			S.insert(a[e[i].id]);
		else
		{
			if(-e[i].id==now)
			{
				set<segment>::iterator it=S.find(a[-e[i].id]);
				if(it--==S.begin())
					break;
				now=it->id;
				ans++;
			}
			S.erase(a[-e[i].id]);
		}
	cout<<ans<<endl;
	return 0;
}
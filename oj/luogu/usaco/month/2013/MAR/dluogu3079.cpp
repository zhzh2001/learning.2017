#include<bits/stdc++.h>
using namespace std;
const int N=50005;
int rect[N][4];
pair<int,int> event[N*2];
bool ans[N];
int main()
{
	int n;
	cin>>n;
	int cc=0;
	for(int i=1;i<=n;i++)
	{
		for(int j=0;j<4;j++)
			cin>>rect[i][j];
		event[++cc]=make_pair(rect[i][0],i);
		event[++cc]=make_pair(rect[i][2],-i);
	}
	sort(event+1,event+cc+1);
	set<pair<int,int> > S;
	for(int i=1;i<=cc;i++)
		if(event[i].second>0)
		{
			set<pair<int,int> >::iterator it=S.lower_bound(make_pair(rect[event[i].second][1],1));
			if(it==S.end()||it->second==1)
			{
				ans[event[i].second]=true;
				S.insert(make_pair(rect[event[i].second][1],1));
				S.insert(make_pair(rect[event[i].second][3],0));
			}
		}
		else
			if(ans[-event[i].second])
			{
				S.erase(make_pair(rect[-event[i].second][1],1));
				S.erase(make_pair(rect[-event[i].second][3],0));
			}
	cout<<count(ans+1,ans+n+1,true)<<endl;
	return 0;
}
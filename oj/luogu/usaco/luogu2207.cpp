#include <iostream>
#include <set>
#include <algorithm>
using namespace std;
const int K = 1005;
typedef pair<int, int> pii;
pii a[K];
int main()
{
	ios::sync_with_stdio(false);
	int n, k;
	cin >> n >> k;
	set<pii> S;
	for (int i = 1; i <= k; i++)
	{
		cin >> a[i].first >> a[i].second;
		S.insert(make_pair(a[i].second, i));
	}
	sort(a + 1, a + k + 1);
	int ans = 0;
	for (int i = 1; i <= k; ans++)
	{
		int val = S.begin()->first;
		S.erase(S.begin());
		for (; i <= k && a[i].first < val; i++)
			S.erase(make_pair(a[i].second, i));
	}
	cout << ans + 1 << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
int pred[N], ans[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		ans[i] = max(ans[i - 1], pred[x]);
		pred[x] = i;
	}
	while (m--)
	{
		int l, r;
		cin >> l >> r;
		cout << (ans[r] < l ? "Yes\n" : "No\n");
	}
	return 0;
}
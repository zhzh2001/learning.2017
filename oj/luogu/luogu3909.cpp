#include <iostream>
using namespace std;
const int N = 1000005, MOD = 1e9 + 7;
int a[N], s[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = n - 1, sum = a[n]; i; sum = (sum + a[i]) % MOD, i--)
		s[i] = 1ll * a[i] * sum % MOD;
	int ans = 0;
	for (int i = n - 2, sum = s[n - 1]; i; sum = (sum + s[i]) % MOD, i--)
		ans = (ans + 1ll * a[i] * sum) % MOD;
	cout << 6ll * ans % MOD << endl;
	return 0;
}
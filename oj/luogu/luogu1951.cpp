#include<bits/stdc++.h>
using namespace std;
const int N=10005;
const long long INF=0x3f3f3f3f3f3f3f3fll;
vector<pair<int,int>> mat[N];
int n,a[N],b[N];
bool vis[N];
long long d[N];
void dijkstra(int s)
{
	fill(d+1,d+n+1,INF);
	if(vis[s])
		return;
	d[s]=0;
	priority_queue<pair<int,int>,vector<pair<int,int>>,greater<pair<int,int>>> Q;
	Q.push(make_pair(0,s));
	while(!Q.empty())
	{
		pair<int,int> k=Q.top();Q.pop();
		if(vis[k.second])
			continue;
		vis[k.second]=true;
		for(auto e:mat[k.second])
			if(!vis[e.first]&&d[k.second]+e.second<d[e.first])
				Q.push(make_pair(d[e.first]=d[k.second]+e.second,e.first));
	}
}
int main()
{
	int m,s,t,maxl;
	cin>>n>>m>>s>>t>>maxl;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	copy(a+1,a+n+1,b+1);
	sort(b+1,b+n+1);
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
		mat[v].push_back(make_pair(u,w));
	}
	dijkstra(s);
	if(d[t]>maxl)
		cout<<-1<<endl;
	else
	{
		int l=1,r=n;
		while(l<r)
		{
			int mid=(l+r)/2;
			for(int i=1;i<=n;i++)
				vis[i]=a[i]>b[mid];
			dijkstra(s);
			if(d[t]<=maxl)
				r=mid;
			else
				l=mid+1;
		}
		cout<<b[r]<<endl;
	}
	return 0;
}
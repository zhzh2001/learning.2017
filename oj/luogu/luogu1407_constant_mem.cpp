#include<bits/stdc++.h>
using namespace std;
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n;
		scanf("%d",&n);
		int ans,cnt=0;
		while(n--)
		{
			int x;
			scanf("%d",&x);
			if(cnt==0)
				ans=x;
			if(x==ans)
				cnt++;
			else
				cnt--;
		}
		printf("%d\n",ans);
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=300005,SZ=6e6;
int a[N],b[N],cc,root[N];
struct node
{
	int sum,ls,rs;
}tree[SZ];
int val;
void insert(int& o,int pred,int l,int r)
{
	tree[o=++cc]=tree[pred];
	tree[o].sum++;
	if(l==r)
		return;
	int mid=(l+r)/2;
	if(val<=mid)
		insert(tree[o].ls,tree[pred].ls,l,mid);
	else
		insert(tree[o].rs,tree[pred].rs,mid+1,r);
}
int query(int ro,int lo,int l,int r)
{
	if(l==r)
		return l;
	int mid=(l+r)/2,cnt=tree[tree[ro].ls].sum-tree[tree[lo].ls].sum;
	if(val<=cnt)
		return query(tree[ro].ls,tree[lo].ls,l,mid);
	val-=cnt;
	return query(tree[ro].rs,tree[lo].rs,mid+1,r);
}
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	copy(a+1,a+n+1,b+1);
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
	{
		val=lower_bound(b+1,b+n+1,a[i])-b;
		insert(root[i],root[i-1],1,n);
	}
	while(m--)
	{
		int l,r;
		cin>>l>>r>>val;
		cout<<b[query(root[r],root[l-1],1,n)]<<endl;
	}
	return 0;
}
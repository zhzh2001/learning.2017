#include <iostream>
#include <algorithm>
using namespace std;
const int N = 55;
int a[N], b[N], f[N][N][N * 2];
int dp(int l, int r, int rem)
{
	if (l > r)
		return 0;
	int &now = f[l][r][rem];
	if (~now)
		return now;
	now = dp(l, r - 1, 0) + (b[r] + rem) * (b[r] + rem);
	for (int i = r - 1; i >= l; i--)
		if (a[i] == a[r])
			now = max(now, dp(l, i, b[r] + rem) + dp(i + 1, r - 1, 0));
	return now;
}
int main()
{
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= n; i++)
		cin >> b[i];
	fill_n(&f[0][0][0], sizeof(f) / sizeof(int), -1);
	cout << dp(1, n, 0) << endl;
	return 0;
}
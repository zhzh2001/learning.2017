#include <iostream>
#include <cstring>
using namespace std;
int mod;
struct matrix
{
	static const int n = 2;
	int mat[n][n];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int k = 0; k < n; k++)
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
					ret.mat[i][j] = (ret.mat[i][j] + 1ll * mat[i][k] * rhs.mat[k][j]) % mod;
		return ret;
	}
	matrix &operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i < matrix::n; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, int b)
{
	matrix ret = I();
	do
	{
		if (b & 1)
			ret *= a;
		a *= a;
	} while (b /= 2);
	return ret;
}
int main()
{
	int p, q, a1, a2, n;
	cin >> p >> q >> a1 >> a2 >> n >> mod;
	if (n == 1)
		cout << a1 % mod << endl;
	else if (n == 2)
		cout << a2 % mod << endl;
	else
	{
		matrix trans, init;
		trans.mat[0][0] = p;
		trans.mat[0][1] = q;
		trans.mat[1][0] = 1;
		init.mat[0][0] = a2;
		init.mat[1][0] = a1;
		cout << (qpow(trans, n - 2) * init).mat[0][0] << endl;
	}
	return 0;
}
#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
const int N = 300005;
struct edge
{
	int u, v;
	vector<pair<int, int>> period;
	edge() {}
	edge(int u, int v) : u(u), v(v) {}
	bool operator<(const edge &rhs) const
	{
		if (u == rhs.u)
			return v < rhs.v;
		return u < rhs.u;
	}
} e[N], *id[N];
pair<int, int> q[N];
bool mark[N];
int f[20][N];
int getf(int id, int x)
{
	return f[id][x] == x ? x : f[id][x] = getf(id, f[id][x]);
}
struct event
{
	int u, v, l, r;
	event(int u, int v, int l, int r) : u(u), v(v), l(l), r(r) {}
};
void solve(int k, int l, int r, vector<event> lst)
{
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i < n; i++)
	{
		cin >> e[i].u >> e[i].v;
		if (e[i].u > e[i].v)
			swap(e[i].u, e[i].v);
	}
	sort(e + 1, e + n);
	int cc = 0;
	for (int i = 1; i <= m; i++)
	{
		char opt;
		cin >> opt;
		if (opt == 'Q')
		{
			cin >> q[i].first >> q[i].second;
			mark[i] = true;
		}
		else if (opt == 'C')
		{
			int u, v;
			cin >> u >> v;
			if (u > v)
				swap(u, v);
			id[++cc] = lower_bound(e + 1, e + n, edge(u, v));
			id[cc]->period.push_back(make_pair(i, 0));
		}
		else
		{
			int x;
			cin >> x;
			id[x]->period.back().second = i;
		}
	}
	for (int i = 1; i <= n; i++)
		f[0][i] = i;
	vector<event> l;
	for (int i = 1; i < n; i++)
		if (e[i].period.empty())
			f[0][getf(0, e[i].u)] = getf(0, e[i].v);
		else
		{
			l.push_back(event(e[i].u, e[i].v, 1, e[i].period.front().first - 1));
			for (size_t j = 0; j < e[i].period.size() - 1; j++)
				l.push_back(event(e[i].u, e[i].v, e[i].period[j].second + 1, e[i].period[j + 1].first - 1));
			if (e[i].period.back().second)
				l.push_back(event(e[i].u, e[i].v, e[i].period.back().second + 1, m));
		}
	solve(1, 1, m, l);
	return 0;
}
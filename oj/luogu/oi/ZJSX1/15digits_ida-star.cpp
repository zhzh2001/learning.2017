#include<bits/stdc++.h>
using namespace std;
const int n=4,target[n][n]={{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}},tx[n*n]={3,0,0,0,0,1,1,1,1,2,2,2,2,3,3,3},ty[n*n]={3,0,1,2,3,0,1,2,3,0,1,2,3,0,1,2},dx[]={-1,1,0,0},dy[]={0,0,-1,1},maxd=100;
int mat[n][n],sol[maxd],dist;
inline int h()
{
	int ans=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			if(mat[i][j])
				ans+=abs(tx[mat[i][j]]-i)+abs(ty[mat[i][j]]-j);
	return ans;
}
bool dfs(int step,int x,int y,int maxd)
{
	int t=h();
	dist=min(dist,t);
	if(!t)
	{
		for(int i=1;i<=step;i++)
			switch(sol[i])
			{
				case 0:cout<<"up\n";break;
				case 1:cout<<"down\n";break;
				case 2:cout<<"left\n";break;
				case 3:cout<<"right\n";break;
			}
		return true;
	}
	if(step+t>maxd)
		return false;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=0&&nx<n&&ny>=0&&ny<n)
		{
			sol[step+1]=i;
			swap(mat[x][y],mat[nx][ny]);
			if(dfs(step+1,nx,ny,maxd))
				return true;
			swap(mat[x][y],mat[nx][ny]);
		}
	}
	return false;
}
int main()
{
	int x,y;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
		{
			cin>>mat[i][j];
			if(!mat[i][j])
				x=i,y=j;
		}
	for(int i=0;;i++)
	{
		dist=0x3f3f3f3f;
		if(dfs(0,x,y,i))
		{
			cout<<i<<endl;
			break;
		}
		cout<<"now="<<i<<" dist="<<dist<<endl;
	}
	return 0;
}
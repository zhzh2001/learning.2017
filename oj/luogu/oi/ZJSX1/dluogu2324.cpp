#include<bits/stdc++.h>
using namespace std;
const int n=5,maxd=15,dx[]={-2,-2,-1,-1,1,1,2,2},dy[]={-1,1,-2,2,-2,2,-1,1};
const string target[n]={"11111","01111","00*11","00001","00000"};
string mat[n];
inline int h()
{
	int ans=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			ans+=mat[i][j]!=target[i][j];
	return ans;
}
bool dfs(int step,int x,int y,int maxd)
{
	int t=h();
	if(!t)
		return true;
	if(step+t>maxd+1)
		return false;
	for(int i=0;i<8;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=0&&nx<n&&ny>=0&&ny<n)
		{
			swap(mat[x][y],mat[nx][ny]);
			if(dfs(step+1,nx,ny,maxd))
				return true;
			swap(mat[x][y],mat[nx][ny]);
		}
	}
	return false;
}
int main()
{
	int t;
	cin>>t;
	while(t--)
	{
		int x,y;
		for(int i=0;i<n;i++)
		{
			cin>>mat[i];
			size_t p=mat[i].find('*');
			if(p!=string::npos)
				x=i,y=p;
		}
		bool found=false;
		for(int i=0;i<=maxd;i++)
			if(dfs(0,x,y,i))
			{
				cout<<i<<endl;
				found=true;
				break;
			}
		if(!found)
			cout<<-1<<endl;
	}
	return 0;
}
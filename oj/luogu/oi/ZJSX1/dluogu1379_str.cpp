#include<bits/stdc++.h>
using namespace std;
const string target="123804765";
const int d[]={-3,3,-1,1},n=9;
typedef pair<string,int> psi;
int main()
{
	string s;
	cin>>s;
	queue<psi> Q;
	Q.push(make_pair(s,0));
	unordered_set<string> S;
	S.insert(s);
	while(!Q.empty())
	{
		psi k=Q.front();Q.pop();
		int p=k.first.find('0');
		for(int i=0;i<4;i++)
			if(p+d[i]>=0&&p+d[i]<n&&(i!=2||p%3)&&(i!=3||p%3<2))
			{
				string now(k.first);
				swap(now[p],now[p+d[i]]);
				if(now==target)
				{
					cout<<k.second+1<<endl;
					return 0;
				}
				if(S.find(now)==S.end())
				{
					Q.push(make_pair(now,k.second+1));
					S.insert(now);
				}
			}
	}
	cout<<-1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int n=3,target[n][n]={{1,2,3},{8,0,4},{7,6,5}},tx[n*n]={1,0,0,0,1,2,2,2,1},ty[n*n]={1,0,1,2,2,2,1,0,0},dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int mat[n][n];
inline int h()
{
	int ans=0;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
			if(mat[i][j])
				ans+=abs(tx[mat[i][j]]-i)+abs(ty[mat[i][j]]-j);
	return ans;
}
bool dfs(int step,int x,int y,int maxd)
{
	int t=h();
	if(!t)
		return true;
	if(step+t>maxd)
		return false;
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i];
		if(nx>=0&&nx<n&&ny>=0&&ny<n)
		{
			swap(mat[x][y],mat[nx][ny]);
			if(dfs(step+1,nx,ny,maxd))
				return true;
			swap(mat[x][y],mat[nx][ny]);
		}
	}
	return false;
}
int main()
{
	string s;
	cin>>s;
	int x,y;
	for(int i=0;i<n;i++)
		for(int j=0;j<n;j++)
		{
			mat[i][j]=s[i*n+j]-'0';
			if(!mat[i][j])
				x=i,y=j;
		}
	for(int i=0;;i++)
		if(dfs(0,x,y,i))
		{
			cout<<i<<endl;
			break;
		}
	return 0;
}
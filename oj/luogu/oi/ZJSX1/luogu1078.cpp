#include<bits/stdc++.h>
using namespace std;
const int N=105,K=105,INF=0x3f3f3f3f,INFM=0x3f;
int n,k,s,t,c[N],mat[N][N],ans;
bool rel[K][K],vis[K];
void dfs(int cur,int len)
{
	if(cur==s)
	{
		ans=min(ans,len);
		return;
	}
	if(len>=ans)
		return;
	vis[c[cur]]=true;
	for(int i=1;i<=n;i++)
		if(!vis[c[i]]&&mat[cur][i]<INF)
		{
			bool flag=true;
			for(int j=1;j<=k;j++)
				if(vis[j]&&rel[j][c[i]])
				{
					flag=false;
					break;
				}
			if(flag)
				dfs(i,len+mat[cur][i]);
		}
	vis[c[cur]]=false;
}
int main()
{
	int m;
	cin>>n>>k>>m>>s>>t;
	for(int i=1;i<=n;i++)
		cin>>c[i];
	for(int i=1;i<=k;i++)
		for(int j=1;j<=k;j++)
			cin>>rel[i][j];
	memset(mat,INFM,sizeof(mat));
	while(m--)
	{
		int u,v,w;
		cin>>u>>v>>w;
		mat[u][v]=mat[v][u]=w;
	}
	ans=INF;
	dfs(t,0);
	if(ans==INF)
		cout<<-1<<endl;
	else
		cout<<ans<<endl;
	return 0;
}
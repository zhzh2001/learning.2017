#include<bits/stdc++.h>
using namespace std;
const string target="123804765";
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
struct matrix
{
	static const int n=3;
	int mat[n][n],x,y,step;
	matrix(const string& s,const int step=0):step(step)
	{
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
			{
				mat[i][j]=s[i*n+j]-'0';
				if(!mat[i][j])
					x=i,y=j;
			}
	}
	string to_str()
	{
		string s;
		s.resize(n*n);
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				s[i*n+j]=mat[i][j]+'0';
		return s;
	}
};
int main()
{
	string s;
	cin>>s;
	matrix src(s);
	queue<matrix> Q;
	Q.push(src);
	unordered_set<string> S;
	S.insert(src.to_str());
	while(!Q.empty())
	{
		matrix k=Q.front();Q.pop();
		if(k.to_str()==target)
		{
			cout<<k.step<<endl;
			return 0;
		}
		for(int i=0;i<4;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			if(nx>=0&&nx<matrix::n&&ny>=0&&ny<matrix::n)
			{
				matrix now(k);
				swap(now.mat[nx][ny],now.mat[k.x][k.y]);
				now.x=nx;now.y=ny;
				now.step++;
				if(S.find(now.to_str())==S.end())
				{
					Q.push(now);
					S.insert(now.to_str());
				}
			}
		}
	}
	cout<<-1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int n,m,a[N],ra[N],b[N];
inline void modify(int x,int val)
{
	for(;x<=n+m;x+=x&-x)
		b[x]+=val;
}
inline int query(int x)
{
	int ans=0;
	for(;x;x-=x&-x)
		ans+=b[x];
	return ans;
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		cin>>a[n-i+1];
	for(int i=1;i<=m;i++)
		cin>>a[n+i];
	memcpy(b,a,sizeof(b));
	sort(b+1,b+n+m+1);
	for(int i=1;i<=n+m;i++)
	{
		a[i]=lower_bound(b+1,b+n+m+1,a[i])-b;
		ra[a[i]]=i;
	}
	memset(b,0,sizeof(b));
	int now=n;
	long long ans=0;
	for(int i=n+m;i;i--)
	{
		if(now<ra[i])
			ans+=ra[i]-now-1-(query(ra[i])-query(now));
		else
			if(now>ra[i])
				ans+=now-ra[i]-(query(now)-query(ra[i]));
		now=ra[i];
		modify(ra[i],1);
	}
	cout<<ans<<endl;
	return 0;
}
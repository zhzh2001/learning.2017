#include<bits/stdc++.h>
using namespace std;
const int n=4,m=100000;
int a[n+1];
long long f[m+5];
int main()
{
	for(int i=1;i<=n;i++)
		cin>>a[i];
	f[0]=1;
	for(int i=1;i<=n;i++)
		for(int j=a[i];j<=m;j++)
			f[j]+=f[j-a[i]];
	int t;
	cin>>t;
	while(t--)
	{
		int c1,c2,c3,c4,s;
		cin>>c1>>c2>>c3>>c4>>s;
		long long ans=0;
		#define inc(_) if(_>=0) ans+=f[_]
		#define dec(_) if(_>=0) ans-=f[_]
		inc(s);
		dec(s-(c1+1)*a[1]);
		dec(s-(c2+1)*a[2]);
		dec(s-(c3+1)*a[3]);
		dec(s-(c4+1)*a[4]);
		inc(s-(c1+1)*a[1]-(c2+1)*a[2]);
		inc(s-(c1+1)*a[1]-(c3+1)*a[3]);
		inc(s-(c1+1)*a[1]-(c4+1)*a[4]);
		inc(s-(c2+1)*a[2]-(c3+1)*a[3]);
		inc(s-(c2+1)*a[2]-(c4+1)*a[4]);
		inc(s-(c3+1)*a[3]-(c4+1)*a[4]);
		dec(s-(c1+1)*a[1]-(c2+1)*a[2]-(c3+1)*a[3]);
		dec(s-(c1+1)*a[1]-(c2+1)*a[2]-(c4+1)*a[4]);
		dec(s-(c1+1)*a[1]-(c3+1)*a[3]-(c4+1)*a[4]);
		dec(s-(c2+1)*a[2]-(c3+1)*a[3]-(c4+1)*a[4]);
		inc(s-(c1+1)*a[1]-(c2+1)*a[2]-(c3+1)*a[3]-(c4+1)*a[4]);
		cout<<ans<<endl;
	}
	return 0;
}
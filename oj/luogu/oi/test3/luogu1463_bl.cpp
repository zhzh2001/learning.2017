#include<bits/stdc++.h>
using namespace std;
void split(int x)
{
	int t=x;
	for(int i=2;i*i<=t;i++)
		if(x%i==0)
		{
			int cnt=0;
			while(x%i==0)
			{
				cnt++;
				x/=i;
			}
			cout<<cnt<<' ';
		}
	if(x>1)
		cout<<1;
	cout<<endl;
}
int main()
{
	int n;
	cin>>n;
	int cnt=1,ans=1;
	for(int i=2;i<=n;i++)
	{
		int now=0;
		for(int j=1;j*j<=i;j++)
			if(i%j==0)
			{
				now++;
				if(j*j<i)
					now++;
			}
		if(now>cnt)
		{
			cnt=now;
			ans=i;
			split(i);
		}
	}
	cout<<ans<<endl;
	return 0;
}
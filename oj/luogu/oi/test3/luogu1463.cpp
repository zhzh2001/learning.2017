#include<bits/stdc++.h>
using namespace std;
const int p[]={2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53},CNT=sizeof(p)/sizeof(int);
long long n,ans,maxc;
int a[CNT];
void dfs(int k,int pred,long long now,long long cnt)
{
	if(k==CNT)
	{
		if((now>ans&&cnt>maxc)||(now<=ans&&cnt>=maxc))
		{
			ans=now;
			maxc=cnt;
			for(int i=0;i<CNT;i++)
				cout<<a[i];
			cout<<endl;
		}
		return;
	}
	for(int i=0;i<=pred;i++)
	{
		a[k]=i;
		dfs(k+1,i,now,cnt*(i+1));
		if(now*p[k]>n||now*p[k]/p[k]!=now)
			break;
		now*=p[k];
	}
}
int main()
{
	cin>>hex>>n;
	ans=maxc=1;
	cout<<hex;
	dfs(0,20,1,1);
	cout<<ans<<endl;
	return 0;
}
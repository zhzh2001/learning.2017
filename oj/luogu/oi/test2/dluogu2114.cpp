#include<bits/stdc++.h>
using namespace std;
const int N=100005;
struct node
{
	int opt,num;
}a[N];
int n;
int calc(int x)
{
	for(int i=1;i<=n;i++)
		switch(a[i].opt)
		{
			case 0:x|=a[i].num;break;
			case 1:x^=a[i].num;break;
			case 2:x&=a[i].num;break;
		}
	return x;
}
int main()
{
	int m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		string s;
		cin>>s>>a[i].num;
		switch(s[0])
		{
			case 'O':a[i].opt=0;break;
			case 'X':a[i].opt=1;break;
			case 'A':a[i].opt=2;break;
		}
	}
	int ans=0;
	for(int i=floor(log2(m));i>=0;i--)
		if(ans+(1<<i)<=m&&calc(ans+(1<<i))>calc(ans))
			ans|=1<<i;
	cout<<calc(ans)<<endl;
	return 0;
}
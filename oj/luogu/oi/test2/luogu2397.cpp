#include<cstdio>
#include<cctype>
using namespace std;
template<typename T>
void read(T& x)
{
	char c;
	for(c=getchar();isspace(c);c=getchar());
	T sign=1;
	if(c=='-')
		sign=-1,c=getchar();
	x=0;
	for(;isdigit(c);c=getchar())
		x=x*10+c-'0';
	x*=sign;
}
int main()
{
	int n;
	read(n);
	int ans,cnt=1;
	read(ans);
	while(--n)
	{
		int x;
		read(x);
		if(x==ans)
			cnt++;
		else
		{
			cnt--;
			if(!cnt)
			{
				ans=x;
				cnt=1;
			}
		}
	}
	printf("%d\n",ans);
	return 0;
}
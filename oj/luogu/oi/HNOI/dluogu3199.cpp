#include<bits/stdc++.h>
using namespace std;
const int N=3005;
const double eps=1e-12;
vector<pair<int,double>> mat[N];
double d[N],mid;
bool inS[N];
bool dfs(int k)
{
	inS[k]=true;
	for(auto e:mat[k])
		if(d[k]+e.second-mid<d[e.first])
		{
			d[e.first]=d[k]+e.second-mid;
			if(inS[e.first]||dfs(e.first))
				return true;
		}
	inS[k]=false;
	return false;
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		double w;
		cin>>u>>v>>w;
		mat[u].push_back(make_pair(v,w));
	}
	double l=-1e7,r=1e7;
	while(r-l>=eps)
	{
		mid=(l+r)/2;
		fill(d+1,d+n+1,.0);
		fill(inS+1,inS+n+1,false);
		bool found=false;
		for(int i=1;i<=n;i++)
			if(dfs(i))
			{
				found=true;
				break;
			}
		if(found)
			r=mid;
		else
			l=mid;
	}
	cout.precision(8);
	cout<<fixed<<l<<endl;
	return 0;
}
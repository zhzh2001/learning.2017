#include<bits/stdc++.h>
using namespace std;
const int N=500005;
int head[N],tail[N];
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	multiset<int> S,gap;
	for(int i=1;i<=n;i++)
	{
		scanf("%d",head+i);
		tail[i]=head[i];
		S.insert(head[i]);
		if(i>1)
			gap.insert(abs(head[i]-head[i-1]));
	}
	int pred=-1,sort_gap=1e9;
	for(multiset<int>::iterator it=S.begin();it!=S.end();++it)
	{
		if(~pred)
			sort_gap=min(sort_gap,*it-pred);
		pred=*it;
	}
	while(m--)
	{
		char opt[15];
		scanf("%s",opt);
		if(opt[0]=='I')
		{
			int x,y;
			scanf("%d%d",&x,&y);
			if(x<n)
			{
				gap.erase(gap.find(abs(tail[x]-head[x+1])));
				gap.insert(abs(y-head[x+1]));
			}
			gap.insert(abs(y-tail[x]));
			tail[x]=y;
			multiset<int>::iterator it1=S.insert(y),it2=it1;
			if(it1--!=S.begin())
				sort_gap=min(sort_gap,y-*it1);
			if(++it2!=S.end())
				sort_gap=min(sort_gap,*it2-y);
		}
		else
			if(!opt[7])
				printf("%d\n",*gap.begin());
			else
				printf("%d\n",sort_gap);
	}
	return 0;
}
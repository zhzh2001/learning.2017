#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1000005;
int f[N], ans[N];
int getf(int x)
{
	return f[x] == x ? x : f[x] = getf(f[x]);
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m, p, q;
	cin >> n >> m >> p >> q;
	for (int i = 1; i <= n; i++)
		f[i] = i;
	for (int i = m; i; i--)
	{
		int l = (i * p + q) % n + 1, r = (i * q + p) % n + 1;
		if (l > r)
			swap(l, r);
		while (l <= r)
			if (getf(r) == r)
			{
				ans[r] = i;
				f[r] = r - 1;
				r--;
			}
			else
				r = getf(r);
	}
	for (int i = 1; i <= n; i++)
		cout << ans[i] << '\n';
	return 0;
}
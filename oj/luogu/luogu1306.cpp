#include<bits/stdc++.h>
using namespace std;
const int MOD=1e8;
struct matrix
{
	static const int n=2;
	int mat[n][n];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				for(int k=0;k<n;k++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
};
matrix I()
{
	matrix ret;
	for(int i=0;i<matrix::n;i++)
		ret.mat[i][i]=1;
	return ret;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int gcd(int a,int b)
{
	return b?gcd(b,a%b):a;
}
int main()
{
	int n,m;
	cin>>n>>m;
	matrix trans;
	trans.mat[0][0]=trans.mat[0][1]=trans.mat[1][0]=1;
	trans=qpow(trans,gcd(n,m)-1);
	cout<<trans.mat[0][0]<<endl;
	return 0;
}
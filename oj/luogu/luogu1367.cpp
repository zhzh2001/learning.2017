#include <iostream>
#include <algorithm>
using namespace std;
const int N = 100005;
struct ant
{
	int x, dir, id;
	bool operator<(const ant &rhs) const
	{
		return x < rhs.x;
	}
} a[N];
int p[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, t;
	cin >> n >> t;
	for (int i = 1; i <= n; i++)
	{
		cin >> a[i].x >> a[i].dir;
		a[i].id = i;
	}
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= n; i++)
	{
		a[i].x += t * a[i].dir;
		p[a[i].id] = i;
	}
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= n; i++)
		if (a[i].x == a[i + 1].x)
			a[i].dir = a[i + 1].dir = 0;
	for (int i = 1; i <= n; i++)
		cout << a[p[i]].x << ' ' << a[p[i]].dir << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=200005;
vector<pair<int,int>> mat[N];
bool inS[N];
int d[N];
bool dfs(int k)
{
	inS[k]=true;
	for(auto e:mat[k])
		if(d[k]+e.second<d[e.first])
		{
			d[e.first]=d[k]+e.second;
			if(inS[e.first]||dfs(e.first))
				return true;
		}
	inS[k]=false;
	return false;
}
int main()
{
	int t;
	scanf("%d",&t);
	while(t--)
	{
		int n,m;
		scanf("%d%d",&n,&m);
		for(int i=1;i<=n;i++)
			mat[i].clear();
		while(m--)
		{
			int u,v,w;
			scanf("%d%d%d",&u,&v,&w);
			mat[u].push_back(make_pair(v,w));
			if(w>=0)
				mat[v].push_back(make_pair(u,w));
		}
		fill(inS+1,inS+n+1,false);
		fill(d+1,d+n+1,0);
		bool found=false;
		for(int i=1;i<=n;i++)
			if(dfs(i))
			{
				found=true;
				break;
			}
		if(found)
			puts("YE5");
		else
			puts("N0");
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=200005;
int head[N],v[M],nxt[M],e,f[N][18],d[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
template<typename T>
void read(T& x)
{
	char c;
	for(c=getchar();isspace(c);c=getchar());
	T sign=1;
	if(c=='-')
		sign=-1,c=getchar();
	x=0;
	for(;isdigit(c);c=getchar())
		x=x*10+c-'0';
	x*=sign;
}
void dfs(int k)
{
	for(int i=head[k];i;i=nxt[i])
		if(!d[v[i]])
		{
			d[v[i]]=d[k]+1;
			f[v[i]][0]=k;
			dfs(v[i]);
		}
}
int lca(int x,int y)
{
	if(d[x]<d[y])
		swap(x,y);
	int delta=d[x]-d[y];
	for(int i=0;delta;i++,delta/=2)
		if(delta&1)
			x=f[x][i];
	if(x==y)
		return x;
	for(int i=17;i>=0;i--)
		if(f[x][i]!=f[y][i])
		{
			x=f[x][i];
			y=f[y][i];
		}
	return f[x][0];
}
int main()
{
	int n,q;
	read(n);read(q);
	for(int i=1;i<n;i++)
	{
		int u,v;
		read(u);read(v);
		add_edge(u,v);
		add_edge(v,u);
	}
	d[1]=1;
	dfs(1);
	for(int i=1;i<18;i++)
		for(int j=1;j<=n;j++)
			f[j][i]=f[f[j][i-1]][i-1];
	while(q--)
	{
		int a,b,c,d;
		read(a);read(b);read(c);read(d);
		int a1=lca(a,b),a2=lca(c,d);
		if((::d[a1]>::d[c]&&::d[a1]>::d[d])||(::d[a2]>::d[a]&&::d[a2]>::d[b]))
			puts("N");
		else
		{
			if(::d[a1]<(::d[a2]))
			{
				swap(a1,a2);
				swap(a,c);
				swap(b,d);
			}
			if(lca(a1,c)==a1||lca(a1,d)==a1)
				puts("Y");
			else
				puts("N");
		}
	}
	return 0;
}
#include <iostream>
using namespace std;
const int N = 30005, M = 60005;
int head[N], v[M], w[M], nxt[M], e;
inline void add_edge(int u, int v, int w)
{
	::v[++e] = v;
	::w[e] = w;
	nxt[e] = head[u];
	head[u] = e;
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i < n; i++)
	{
		int u, v, w;
		cin >> u >> v >> w;
		add_edge(u, v, w);
		add_edge(v, u, w);
	}

	return 0;
}
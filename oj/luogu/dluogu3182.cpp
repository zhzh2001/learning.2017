#include<bits/stdc++.h>
using namespace std;
const int N=205;
struct bigint
{
	static const int LEN=400,BASE=10000,WIDTH=4;
	int len,dig[LEN];
	bigint(int x=0)
	{
		len=1;
		memset(dig,0,sizeof(dig));
		dig[1]=x;
	}
	bigint operator+(const bigint& rhs)const
	{
		bigint res;
		res.len=max(len,rhs.len);
		int overflow=0;
		for(int i=1;i<=res.len;i++)
		{
			res.dig[i]=dig[i]+rhs.dig[i]+overflow;
			overflow=res.dig[i]/BASE;
			res.dig[i]%=BASE;
		}
		if(overflow)
			res.dig[++res.len]=overflow;
		return res;
	}
	bigint operator*(const int rhs)const
	{
		bigint res;
		res.len=len;
		int overflow=0;
		for(int i=1;i<=len;i++)
		{
			res.dig[i]=dig[i]*rhs+overflow;
			overflow=res.dig[i]/BASE;
			res.dig[i]%=BASE;
		}
		if(overflow)
			res.dig[++res.len]=overflow;
		return res;
	}
}f[N];
ostream& operator<<(ostream& os,const bigint& rhs)
{
	os.fill('0');
	for(int i=rhs.len;i;i--)
	{
		if(i<rhs.len)
			os.width(bigint::WIDTH);
		os<<rhs.dig[i];
	}
	return os;
}
int main()
{
	int n;
	cin>>n;
	f[1]=0;f[2]=1;
	for(int i=3;i<=n;i++)
		f[i]=(f[i-1]+f[i-2])*(i-1);
	cout<<f[n]<<endl;
	return 0;
}
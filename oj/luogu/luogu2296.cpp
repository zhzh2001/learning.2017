#include<bits/stdc++.h>
using namespace std;
const int N=10005,INF=0x3f3f3f3f;
vector<int> mat[N],rmat[N];
int d[N];
bool vis[N],rvis[N];
int main()
{
	int n,m;
	cin>>n>>m;
	set<pair<int,int>> h;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		mat[u].push_back(v);
		rmat[v].push_back(u);
	}
	int s,t;
	cin>>s>>t;
	queue<int> Q;
	Q.push(t);
	rvis[t]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(auto v:rmat[k])
			if(!rvis[v])
			{
				rvis[v]=true;
				Q.push(v);
			}
	}
	for(int i=1;i<=n;i++)
	{
		vis[i]=false;
		for(auto v:mat[i])
			if(!rvis[v])
			{
				vis[i]=true;
				break;
			}
	}
	Q.push(s);
	vis[s]=true;
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(auto v:mat[k])
			if(!vis[v])
			{
				vis[v]=true;
				d[v]=d[k]+1;
				Q.push(v);
			}
	}
	if(d[t]==INF)
		cout<<-1<<endl;
	else
		cout<<d[t]<<endl;
	return 0;
}
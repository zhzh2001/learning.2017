#include<bits/stdc++.h>
using namespace std;
int n;
double c[20];
inline double f(double x)
{
	double ret=c[0],now=x;
	for(int i=1;i<=n;i++,now*=x)
		ret+=c[i]*now;
	return ret;
}
int main()
{
	cin>>n;
	double l,r;
	cin>>l>>r;
	for(int i=n;i>=0;i--)
		cin>>c[i];
	while(r-l>1e-7)
	{
		double lmid=l+(r-l)/3,rmid=r-(r-l)/3;
		if(f(lmid)<f(rmid))
			l=lmid;
		else
			r=rmid;
	}
	cout.precision(5);
	cout<<fixed<<l<<endl;
	return 0;
}
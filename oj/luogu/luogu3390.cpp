#include<bits/stdc++.h>
using namespace std;
const int MOD=1000000007;
int n;
struct matrix
{
	long long mat[105][105];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	void set1()
	{
		memset(mat,0,sizeof(mat));
		for(int i=1;i<=n;i++)
			mat[i][i]=1;
	}
	matrix operator*(const matrix& b)const
	{
		matrix res;
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				for(int k=1;k<=n;k++)
					res.mat[i][j]=(res.mat[i][j]+mat[i][k]*b.mat[k][j])%MOD;
		return res;
	}
	matrix operator*=(const matrix& b)
	{
		return *this=*this*b;
	}
}a,ans;
istream& operator>>(istream& is,matrix& m)
{
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			is>>m.mat[i][j];
	return is;
}
ostream& operator<<(ostream& os,const matrix& m)
{
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			os<<m.mat[i][j]<<' ';
		os<<endl;
	}
	return os;
}
int main()
{
	long long k;
	cin>>n>>k>>a;
	ans.set1();
	do
	{
		if(k&1)
			ans*=a;
		a*=a;
	}
	while(k>>=1);
	cout<<ans;
	return 0;
}
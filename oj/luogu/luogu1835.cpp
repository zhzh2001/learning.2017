#include <iostream>
#include <cmath>
#include <algorithm>
using namespace std;
const int N = 50000, M = 1000005;
bool p[N], q[M];
int main()
{
	int l, r;
	cin >> l >> r;
	int n = sqrt(r);
	for (unsigned i = 2; i * i <= n; i++)
		if (!p[i])
			for (unsigned j = i * i; j <= n; j += i)
				p[j] = true;
	for (int i = 2; i <= n; i++)
		if (!p[i])
		{
			unsigned j = max(i * i, l / i * i);
			if (j < l)
				j += i;
			for (; j <= r; j += i)
				q[j - l] = true;
		}
	cout << count(q, q + r - l + 1, false) << endl;
	return 0;
}
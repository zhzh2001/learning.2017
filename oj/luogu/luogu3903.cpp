#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1005;
int a[N], f[N][2];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		f[i][0] = 1;
		for (int j = 1; j < i; j++)
		{
			if (a[j] < a[i])
				f[i][0] = max(f[i][0], f[j][1] + 1);
			if (a[j] > a[i])
				f[i][1] = max(f[i][1], f[j][0] + 1);
		}
		ans = max(ans, max(f[i][0], f[i][1]));
	}
	cout << ans << endl;
	return 0;
}
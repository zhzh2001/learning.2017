#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int a[10][10],blank,ans,b[10][10];
bool vis1[10][10],vis2[10][10],vis3[10][10],v[10][10];
struct node
{
	int x,y;
};
node order[100];
inline int base(int x,int y)
{
	if(x<=5)
		if(y<=5)
			return min(x,y)+5;
		else
			return min(x,10-y)+5;
	else
		if(y<=5)
			return min(10-x,y)+5;
		else
			return min(10-x,10-y)+5;
}
void dfs(int s,int now)
{
	if(s==blank+1)
	{
		ans=max(ans,now);
		return;
	}
	int i=order[s].x,j=order[s].y;
	for(int k=1;k<=9;k++)
		if(!vis1[(i-1)/3*3+(j+2)/3][k]&&!vis2[i][k]&&!vis3[j][k])
		{
			vis1[(i-1)/3*3+(j+2)/3][k]=vis2[i][k]=vis3[j][k]=true;
			dfs(s+1,now+base(i,j)*k);
			vis1[(i-1)/3*3+(j+2)/3][k]=vis2[i][k]=vis3[j][k]=false;
		}
}
void update(int i,int j)
{
	for(int x=1;x<=9;x++)
		for(int y=1;y<=9;y++)
			if(x!=i||y!=j)
			{
				if((x-1)/3*3+(j+2)/3==(i-1)/3*3+(j+2)/3)
					b[x][y]++;
				if(x==i)
					b[x][y]++;
				if(y==j)
					b[x][y]++;
			}
}
int main()
{
	memset(vis1,false,sizeof(vis1));
	memset(vis2,false,sizeof(vis2));
	memset(vis3,false,sizeof(vis3));
	memset(b,0,sizeof(b));
	memset(v,false,sizeof(v));
	int sum=0;
	blank=0;
	for(int i=1;i<=9;i++)
		for(int j=1;j<=9;j++)
		{
			cin>>a[i][j];
			if(!a[i][j])
			{
				blank++;
				vis1[(i-1)/3*3+(j+2)/3][a[i][j]]=vis2[i][a[i][j]]=vis3[j][a[i][j]]=true;
				update(i,j);
				sum+=base(i,j)*a[i][j];
			}
		}
	for(int i=1;i<=blank;i++)
	{
		int mi=0,mj;
		for(int j=1;j<=9;j++)
			for(int k=1;k<=9;k++)
				if(!a[j][k]&&!v[j][k]&&(mi==0||b[j][k]>b[mi][mj]))
				{
					mi=j;mj=k;
				}
		order[i].x=mi;order[i].y=mj;
		v[mi][mj]=true;
		update(mi,mj);
	}
	ans=-1;
	dfs(1,sum);
	cout<<ans<<endl;
	return 0;
}
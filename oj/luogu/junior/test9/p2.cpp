#include<iostream>
#include<algorithm>
#include<cstring>
#include<cstdlib>
using namespace std;
int n,sum,a[15],f[15][15];
bool vis[15];
void dfs(int k,int now)
{
	if(k==n+1)
	{
		if(now==sum)
		{
			for(int i=1;i<=n;i++)
				cout<<a[i]<<' ';
			cout<<endl;
			exit(0);
		}
		return;
	}
	if(now>sum)
		return;
	for(int i=1;i<=n;i++)
		if(!vis[i])
		{
			a[k]=i;
			vis[i]=true;
			dfs(k+1,now+f[n][k]*i);
			vis[i]=false;
		}
}
int main()
{
	cin>>n>>sum;
	for(int i=1;i<=n;i++)
		a[i]=i;
	f[1][1]=1;
	for(int i=2;i<=n;i++)
	{
		f[i][1]=f[i][i]=1;
		for(int j=2;j<i;j++)
			f[i][j]=f[i-1][j-1]+f[i-1][j];
	}
	memset(vis,false,sizeof(vis));
	dfs(1,0);
	return 0;
}
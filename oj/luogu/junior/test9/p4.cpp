#include<iostream>
#include<cmath>
#include<cstring>
using namespace std;
int n;
struct point
{
	double x,y;
};
point a[20];
bool vis[20];
double ans;
void dfs(int k,int p,double now)
{
	if(k==n)
	{
		ans=min(ans,now);
		return;
	}
	if(now>ans)
		return;
	vis[p]=true;
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(k+1,i,now+sqrt((a[i].x-a[p].x)*(a[i].x-a[p].x)+(a[i].y-a[p].y)*(a[i].y-a[p].y)));
	vis[p]=false;
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i].x>>a[i].y;
	memset(vis,false,sizeof(vis));
	ans=1e100;
	for(int i=1;i<=n;i++)
		dfs(1,i,sqrt(a[i].x*a[i].x+a[i].y*a[i].y));
	cout.precision(2);
	cout<<fixed<<ans<<endl;
	return 0;
}
#include<iostream>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,m,a[105][105],f[105][105];
int dfs(int i,int j)
{
	if(f[i][j])
		return f[i][j];
	int& now=f[i][j];
	now=1;
	for(int k=0;k<4;k++)
	{
		int nx=i+dx[k],ny=j+dy[k];
		if(nx>0&&nx<=n&&ny>0&&ny<=m&&a[nx][ny]<a[i][j])
			now=max(now,dfs(nx,ny)+1);
	}
	return now;
}
int main()
{
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>a[i][j];
	memset(f,0,sizeof(f));
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			ans=max(ans,dfs(i,j));
	cout<<ans<<endl;
	return 0;
}
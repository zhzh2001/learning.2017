#include<iostream>
#include<string>
#include<cstdio>
#include<algorithm>
#include<cctype>
using namespace std;
const string num[]={"","one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty"};
string l[25];
bool cmp(string a,string b)
{
	return a+b<b+a;
}
int main()
{
	string s;
	int cnt=0;
	while(cin>>s)
	{
		for(int i=0;i<s.length();i++)
			s[i]=tolower(s[i]);
		bool flag=false;
		for(int i=1;i<=20;i++)
			if(s==num[i])
			{
				char buf[5];
				sprintf(buf,"%02d",i*i%100);
				l[++cnt]=buf;
				flag=true;
				break;
			}
		if(!flag)
			if(s=="a"||s=="another"||s=="first")
				l[++cnt]="01";
			else
				if(s=="both"||s=="second")
					l[++cnt]="04";
				else
					if(s=="third")
						l[++cnt]="09";
	}
	sort(l+1,l+cnt+1,cmp);
	s="";
	for(int i=1;i<=cnt;i++)
		s+=l[i];
	if(s=="")
		cout<<0<<endl;
	else
	{
		int i;
		for(i=0;i<s.length()-1&&s[i]=='0';i++);
		cout<<s.substr(i)<<endl;
	}
	return 0;
}
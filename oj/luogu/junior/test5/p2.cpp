#include<iostream>
#include<string>
#include<cstring>
using namespace std;
char map[26],antimap[26];
int main()
{
	string enc,ori,s;
	cin>>enc>>ori>>s;
	memset(map,0,sizeof(map));
	memset(antimap,0,sizeof(antimap));
	for(int i=0;i<ori.length();i++)
	{
		if((map[ori[i]-'A']&&map[ori[i]-'A']!=enc[i])||(antimap[enc[i]-'A']&&antimap[enc[i]-'A']!=ori[i]))
		{
			cout<<"Failed\n";
			return 0;
		}
		map[ori[i]-'A']=enc[i];
		antimap[enc[i]-'A']=ori[i];
	}
	for(int i=0;i<26;i++)
		if(!map[i])
		{
			cout<<"Failed\n";
			return 0;
		}
	for(int i=0;i<s.length();i++)
		cout<<antimap[s[i]-'A'];
	cout<<endl;
	return 0;
}
#include<iostream>
#include<string>
#include<cstring>
#include<cctype>
using namespace std;
string l[25];
int n,ans,cnt[25];
void dfs(string s)
{
	ans=max(ans,(int)s.length());
	for(int i=1;i<=n;i++)
		if(cnt[i]<2)
		{
			string t="";
			int len=min(l[i].length(),s.length()),j;
			for(j=0;j<len-1;j++)
			{
				t+=l[i][j];
				if(s.substr(s.length()-j-1)==t)
					break;
			}
			if(j<len&&s.substr(s.length()-j-1)==t)
			{
				cnt[i]++;
				dfs(s+l[i].substr(j+1));
				cnt[i]--;
			}
		}
}
inline void stolower(string& s)
{
	for(int i=0;i<s.length();i++)
		s[i]=tolower(s[i]);
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>l[i];
		stolower(l[i]);
	}
	string first;
	cin>>first;
	stolower(first);
	ans=0;
	memset(cnt,0,sizeof(cnt));
	for(int i=1;i<=n;i++)
		if(l[i][0]==first[0])
		{
			cnt[i]++;
			dfs(l[i]);
			cnt[i]--;
		}
	cout<<ans<<endl;
	return 0;
}
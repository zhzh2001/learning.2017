#include<iostream>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,m,tx,ty,ans;
bool vis[10][10];
void dfs(int i,int j)
{
	if(i==tx&&j==ty)
	{
		ans++;
		return;
	}
	vis[i][j]=true;
	for(int k=0;k<4;k++)
	{
		int nx=i+dx[k],ny=j+dy[k];
		if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis[nx][ny])
			dfs(nx,ny);
	}
	vis[i][j]=false;
}
int main()
{
	int k,sx,sy;
	cin>>n>>m>>k>>sx>>sy>>tx>>ty;
	memset(vis,false,sizeof(vis));
	while(k--)
	{
		int x,y;
		cin>>x>>y;
		vis[x][y]=true;
	}
	ans=0;
	dfs(sx,sy);
	cout<<ans<<endl;
	return 0;
}
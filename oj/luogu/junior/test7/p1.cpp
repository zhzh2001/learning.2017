#include<iostream>
#include<cstring>
using namespace std;
int n,ans,l[15];
bool row[15],f1[30],f2[30];
void dfs(int k)
{
	if(k==n+1)
	{
		ans++;
		if(ans<=3)
		{
			for(int i=1;i<=n;i++)
				cout<<l[i]<<' ';
			cout<<endl;
		}
		return;
	}
	for(int i=1;i<=n;i++)
		if(!row[i]&&!f1[k-i+n]&&!f2[k+i])
		{
			row[i]=f1[k-i+n]=f2[k+i]=true;
			l[k]=i;
			dfs(k+1);
			row[i]=f1[k-i+n]=f2[k+i]=false;
		}
}
int main()
{
	cin>>n;
	memset(row,false,sizeof(row));
	memset(f1,false,sizeof(f1));
	memset(f2,false,sizeof(f2));
	ans=0;
	dfs(1);
	cout<<ans<<endl;
	return 0;
}
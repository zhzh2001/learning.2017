#include<iostream>
#include<string>
#include<cstring>
using namespace std;
const string s="yizhong";
const int dx[]={-1,1,0,0,-1,-1,1,1},dy[]={0,0,-1,1,-1,1,-1,1};
string map[105];
bool vis[105][105];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>map[i];
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
		for(int j=0;j<n;j++)
			for(int d=0;d<8;d++)
			{
				bool flag=true;
				int x=i,y=j;
				for(int k=0;k<s.length();k++)
				{
					if((k<s.length()-1&&(x<=0||x>n||y<0||y>=n))||map[x][y]!=s[k])
					{
						flag=false;
						break;
					}
					x+=dx[d];y+=dy[d];
				}
				if(flag)
				{
					x=i;y=j;
					for(int k=0;k<s.length();k++)
					{
						vis[x][y]=true;
						x+=dx[d];y+=dy[d];
					}
				}
			}
	for(int i=1;i<=n;i++)
	{
		for(int j=0;j<n;j++)
			if(vis[i][j])
				cout<<map[i][j];
			else
				cout<<'*';
		cout<<endl;
	}
	return 0;
}
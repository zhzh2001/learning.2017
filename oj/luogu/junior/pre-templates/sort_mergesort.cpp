#include<iostream>
#include<cstring>
using namespace std;
int a[100005],b[100005];
void msort(int l,int r)
{
	if(l>=r)
		return;
	msort(l,(l+r)/2);
	msort((l+r)/2+1,r);
	int i=l,j=(l+r)/2+1,p=l;
	while(i<=(l+r)/2&&j<=r)
		if(a[i]<a[j])
			b[p++]=a[i++];
		else
			b[p++]=a[j++];
	while(i<=(l+r)/2)
		b[p++]=a[i++];
	while(j<=r)
		b[p++]=a[j++];
	memcpy(a+l,b+l,(r-l+1)*sizeof(int));
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	msort(1,n);
	for(int i=1;i<=n;i++)
		cout<<a[i]<<' ';
	cout<<endl;
	return 0;
}
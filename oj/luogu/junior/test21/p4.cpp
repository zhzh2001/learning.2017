#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f;
int n,m,r,c,a[20][20],f[20][20],row[20],ans,s1[20],s2[20][20];
void dfs(int k,int sel)
{
	if(sel==r+1)
	{
		for(int i=1;i<=m;i++)
		{
			s1[i]=0;
			for(int j=1;j<r;j++)
				s1[i]+=abs(a[row[j]][i]-a[row[j+1]][i]);
		}
		for(int i=1;i<m;i++)
			for(int j=i+1;j<=m;j++)
			{
				s2[i][j]=0;
				for(int k=1;k<=r;k++)
					s2[i][j]+=abs(a[row[k]][i]-a[row[k]][j]);
			}
		memset(f,0x3f,sizeof(f));
		f[0][0]=0;
		for(int i=1;i<=m;i++)
			for(int j=1;j<=c&&j<=i;j++)
			{
				for(int k=0;k<i;k++)
					f[i][j]=min(f[i][j],f[k][j-1]+s1[i]+s2[k][i]);
				if(j==c)
					ans=min(ans,f[i][j]);
			}
		return;
	}
	for(int i=k;i+r-sel<=n;i++)
	{
		row[sel]=i;
		dfs(i+1,sel+1);
	}
}
int main()
{
	cin>>n>>m>>r>>c;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>a[i][j];
	ans=INF;
	dfs(1,1);
	cout<<ans<<endl;
	return 0;
}
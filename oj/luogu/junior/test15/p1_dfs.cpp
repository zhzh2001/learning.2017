#include<bits/stdc++.h>
using namespace std;
int m,n,v[30],p[30],ans;
void dfs(int k,int rem,int now)
{
	if(k==n+1)
	{
		ans=max(ans,now);
		return;
	}
	dfs(k+1,rem,now);
	if(rem>=v[k])
		dfs(k+1,rem-v[k],now+v[k]*p[k]);
}
int main()
{
	cin>>m>>n;
	for(int i=1;i<=n;i++)
		cin>>v[i]>>p[i];
	ans=0;
	dfs(1,m,0);
	cout<<ans<<endl;
	return 0;
}
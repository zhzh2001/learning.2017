#include<iostream>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
int n,a[35][35];
bool out,vis[35][35];
void dfs(int i,int j,bool fill)
{
	if(fill)
		a[i][j]=2;
	else
		vis[i][j]=true;
	for(int k=0;k<4;k++)
	{
		int nx=i+dx[k],ny=j+dy[k];
		if(nx>0&&nx<=n&&ny>0&&ny<=n)
		{
			if((fill&&a[nx][ny]==0)||(!fill&&!vis[nx][ny]&&a[nx][ny]==0))
				dfs(nx,ny,fill);
		}
		else
			out=true;
	}
}
int main()
{
	cin>>n;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			cin>>a[i][j];
	memset(vis,false,sizeof(vis));
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(!vis[i][j]&&a[i][j]==0)
			{
				out=false;
				dfs(i,j,false);
				if(!out)
					dfs(i,j,true);
			}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=n;j++)
			cout<<a[i][j]<<' ';
		cout<<endl;
	}
	return 0;
}
#include<iostream>
#include<queue>
#include<cstring>
#include<cstdio>
using namespace std;
const int dx[]={-2,-2,-1,-1,1,1,2,2},dy[]={-1,1,-2,2,-2,2,-1,1};
struct node
{
	int x,y,step;
};
queue<node> q;
bool vis[405][405];
int ans[405][405];
int main()
{
	int n,m,sx,sy;
	cin>>n>>m>>sx>>sy;
	memset(vis,false,sizeof(vis));
	q.push((node){sx,sy,0});
	vis[sx][sy]=true;
	memset(ans,-1,sizeof(ans));
	while(!q.empty())
	{
		node k=q.front();q.pop();
		ans[k.x][k.y]=k.step;
		for(int i=0;i<8;i++)
		{
			int nx=k.x+dx[i],ny=k.y+dy[i];
			if(nx>0&&nx<=n&&ny>0&&ny<=m&&!vis[nx][ny])
			{
				vis[nx][ny]=true;
				q.push((node){nx,ny,k.step+1});
			}
		}
	}
	for(int i=1;i<=n;i++)
	{
		for(int j=1;j<=m;j++)
		{
			char buf[10];
			sprintf(buf,"%d",ans[i][j]);
			string s(buf);
			while(s.length()<5)
				s+=' ';
			cout<<s;
		}
		cout<<endl;
	}
	return 0;
}
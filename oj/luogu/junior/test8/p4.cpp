#include<iostream>
#include<queue>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
bool map[55][55],vis[55][55][4];
struct node
{
	int x,y,face,step;
};
queue<node> q;
int main()
{
	int n,m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			cin>>map[i][j];
	int sx,sy,tx,ty;
	string face;
	cin>>sx>>sy>>tx>>ty>>face;
	int f;
	if(face=="N")
		f=0;
	if(face=="S")
		f=1;
	if(face=="W")
		f=2;
	if(face=="E")
		f=3;
	memset(vis,false,sizeof(vis));
	q.push((node){sx,sy,f,0});
	vis[sx][sy][f]=true;
	while(!q.empty())
	{
		node k=q.front();q.pop();
		if(k.x==tx&&k.y==ty)
		{
			cout<<k.step<<endl;
			return 0;
		}
		int nx=k.x,ny=k.y;
		for(int i=1;i<=3;i++)
		{
			nx+=dx[k.face];ny+=dy[k.face];
			if(nx>0&&nx<n&&ny>0&&ny<m&&!map[nx][ny]&&!map[nx+1][ny]&&!map[nx][ny+1]&&!map[nx+1][ny+1])
			{
				if(!vis[nx][ny][k.face])
				{
					vis[nx][ny][k.face]=true;
					q.push((node){nx,ny,k.face,k.step+1});
				}
			}
			else
				break;
		}
		int f=k.face;
		switch(f)
		{
			case 0:f=2;break;
			case 1:f=3;break;
			case 2:f=1;break;
			case 3:f=0;break;
		}
		if(!vis[k.x][k.y][f])
		{
			vis[k.x][k.y][f]=true;
			q.push((node){k.x,k.y,f,k.step+1});
		}
		f=k.face;
		switch(f)
		{
			case 0:f=3;break;
			case 1:f=2;break;
			case 2:f=0;break;
			case 3:f=1;break;
		}
		if(!vis[k.x][k.y][f])
		{
			vis[k.x][k.y][f]=true;
			q.push((node){k.x,k.y,f,k.step+1});
		}
	}
	cout<<-1<<endl;
	return 0;
}
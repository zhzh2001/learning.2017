#include<iostream>
#include<string>
#include<queue>
#include<unordered_set>
using namespace std;
struct rule
{
	string a,b;
};
rule r[10];
struct node
{
	string s;
	int step;
};
queue<node> q;
unordered_set<string> s;
int main()
{
	string a,b;
	cin>>a>>b;
	int cnt=1;
	while(cin>>r[cnt].a>>r[cnt].b)
		cnt++;
	cnt--;
	q.push((node){a,0});
	s.insert(a);
	while(!q.empty())
	{
		node k=q.front();q.pop();
		if(k.s==b)
		{
			cout<<k.step<<endl;
			return 0;
		}
		if(k.step>10)
			break;
		for(int i=1;i<=cnt;i++)
		{
			int p=k.s.find(r[i].a);
			while(p!=string::npos)
			{
				string t(k.s);
				t.replace(p,r[i].a.length(),r[i].b);
				if(!s.count(t))
				{
					q.push((node){t,k.step+1});
					s.insert(t);
				}
				p=k.s.find(r[i].a,p+1);
			}
		}
	}
	cout<<"NO ANSWER!\n";
	return 0;
}
#include<iostream>
#include<string>
#include<cstring>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
bool map[1005][1005];
int n,num[1005][1005],cnt[1000005];
void dfs(int i,int j,int now)
{
	num[i][j]=now;
	cnt[now]++;
	for(int k=0;k<4;k++)
	{
		int nx=i+dx[k],ny=j+dy[k];
		if(nx>0&&nx<=n&&ny>0&&ny<=n&&!num[nx][ny]&&map[i][j]^map[nx][ny])
			dfs(nx,ny,now);
	}
}
int main()
{
	int m;
	cin>>n>>m;
	for(int i=1;i<=n;i++)
	{
		string s;
		cin>>s;
		for(int j=0;j<n;j++)
			map[i][j+1]=s[j]=='1';
	}
	memset(num,0,sizeof(num));
	memset(cnt,0,sizeof(cnt));
	int cc=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			if(!num[i][j])
				dfs(i,j,++cc);
	while(m--)
	{
		int x,y;
		cin>>x>>y;
		cout<<cnt[num[x][y]]<<endl;
	}
	return 0;
}
#include<iostream>
#include<algorithm>
using namespace std;
struct node
{
	int t,num;
};
node a[1005];
bool cmp(node a,node b)
{
	if(a.t!=b.t)
		return a.t<b.t;
	return a.num<b.num;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i].t;
		a[i].num=i;
	}
	sort(a+1,a+n+1,cmp);
	int now=0;
	long long sum=0;
	for(int i=1;i<=n;i++)
	{
		cout<<a[i].num<<' ';
		now+=a[i].t;
		sum+=(now-a[i].t);
	}
	cout.precision(2);
	cout<<endl<<fixed<<(double)sum/n<<endl;
	return 0;
}
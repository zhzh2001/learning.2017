#include<iostream>
#include<queue>
#include<vector>
using namespace std;
priority_queue<int,vector<int>,greater<int> > pq;
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		pq.push(x);
	}
	int ans=0;
	for(int i=1;i<n;i++)
	{
		int a=pq.top();pq.pop();
		int b=pq.top();pq.pop();
		ans+=(a+b);
		pq.push(a+b);
	}
	cout<<ans<<endl;
	return 0;
}
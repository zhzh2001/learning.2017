#include<iostream>
#include<algorithm>
using namespace std;
struct node
{
	int a,b;
};
node l[1000005];
bool cmp(node x,node y)
{
	if(x.b!=y.b)
		return x.b<y.b;
	return x.a>y.a;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>l[i].a>>l[i].b;
	sort(l+1,l+n+1,cmp);
	int ans=1,last=l[1].b;
	for(int i=2;i<=n;i++)
		if(last<=l[i].a)
		{
			ans++;
			last=l[i].b;
		}
	cout<<ans<<endl;
	return 0;
}
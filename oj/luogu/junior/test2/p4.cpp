#include<iostream>
#include<cstdlib>
#include<algorithm>
using namespace std;
struct node
{
	int x,y,cnt;
};
node a[405];
bool cmp(node a,node b)
{
	return a.cnt>b.cnt;
}
int main()
{
	int n,m,t;
	cin>>n>>m>>t;
	int cnt=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			int x;
			cin>>x;
			if(x)
				a[++cnt]=(node){i,j,x};
		}
	sort(a+1,a+cnt+1,cmp);
	int ans=0,now=0,x=0,y=a[1].y;
	for(int i=1;i<=cnt&&now<t;i++)
	{
		int cur=abs(x-a[i].x)+abs(y-a[i].y)+1+a[i].x;
		if(now+cur<=t)
		{
			now+=(cur-a[i].x);
			ans+=a[i].cnt;
			x=a[i].x;y=a[i].y;
		}
		else
			break;
	}
	cout<<ans<<endl;
	return 0;
}
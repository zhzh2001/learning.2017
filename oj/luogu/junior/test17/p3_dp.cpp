#include<bits/stdc++.h>
using namespace std;
int f[105][105];
int main()
{
	int n,m;
	cin>>n>>m;
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
		{
			int x;
			cin>>x;
			if(x)
				f[i][j]=min(f[i-1][j-1],min(f[i-1][j],f[i][j-1]))+1;
			else
				f[i][j]=0;
			ans=max(ans,f[i][j]);
		}
	cout<<ans<<endl;
	return 0;
}
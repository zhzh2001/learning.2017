#include<bits/stdc++.h>
using namespace std;
typedef long long LL;
struct node
{
	int a,b,c;
};
node l[55];
LL f[100005];
bool cmp(node x,node y)
{
	return LL(x.c)*y.b<LL(y.c)*x.b;
};
int main()
{
	int t,n;
	cin>>t>>n;
	for(int i=1;i<=n;i++)
		cin>>l[i].a;
	for(int i=1;i<=n;i++)
		cin>>l[i].b;
	for(int i=1;i<=n;i++)
		cin>>l[i].c;
	sort(l+1,l+n+1,cmp);
	for(int i=1;i<=n;i++)
		for(int j=t;j>=l[i].c;j--)
			f[j]=max(f[j],f[j-l[i].c]+l[i].a-LL(j)*l[i].b);
	LL ans=0;
	for(int i=1;i<=t;i++)
		ans=max(ans,f[i]);
	cout<<ans<<endl;
	return 0;
}
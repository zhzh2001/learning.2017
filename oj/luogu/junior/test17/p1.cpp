#include<bits/stdc++.h>
using namespace std;
const int INF=1e9;
int a[205][205],f[205][205];
int main()
{
    int n,m;
    cin>>n>>m;
    for(int i=1;i<=n;i++)
        for(int j=1;j<=m;j++)
            cin>>a[i][j];
    for(int j=1;j<=m;j++)
        f[n][j]=(j-m/2>=0&&j-m/2<=2?a[n][j]:-INF);
    for(int i=n-1;i>=1;i--)
        for(int j=1;j<=m;j++)
        {
            f[i][j]=f[i+1][j]+a[i][j];
            if(j>1)
                f[i][j]=max(f[i][j],f[i+1][j-1]+a[i][j]);
            if(j<m)
                f[i][j]=max(f[i][j],f[i+1][j+1]+a[i][j]);
        }
    int ans=-INF;
    for(int j=1;j<=m;j++)
        ans=max(ans,f[1][j]);
    cout<<ans<<endl;
    return 0;
}
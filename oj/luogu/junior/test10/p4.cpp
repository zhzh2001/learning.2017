#include<iostream>
#include<cstdio>
using namespace std;
const int MAXN=1050;
char map[MAXN][MAXN];
void dfs(int n,char* p)
{
	if(n==1)
	{
		sprintf(p," /\\");
		sprintf(p+MAXN,"/__\\");
	}
	else
	{
		dfs(n-1,p+(1<<(n-1)));
		p+=(1<<(n-1))*MAXN;
		dfs(n-1,p);
		dfs(n-1,p+(1<<n));
	}
}
int main()
{
	int n;
	cin>>n;
	dfs(n,map[0]);
	for(int i=0;i<(1<<n);i++)
	{
		int j;
		for(j=(1<<(n+1))-1;!map[i][j];j--);
		for(int k=0;k<=j;k++)
			if(map[i][k])
				cout<<map[i][k];
			else
				cout<<' ';
		cout<<endl;
	}
	return 0;
}
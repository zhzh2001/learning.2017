#include<iostream>
using namespace std;
void dfs(int n)
{
	if(n==0)
		cout<<n;
	else
	{
		bool a[20];
		int t=n,p=0;
		do
			a[++p]=t%2;
		while(t/=2);
		bool first=true;
		for(int i=p;i>=1;i--)
			if(a[i])
			{
				if(!first)
					cout<<'+';
				first=false;
				if(i==2)
					cout<<2;
				else
				{
					cout<<"2(";
					dfs(i-1);
					cout<<')';
				}
			}
	}
}
int main()
{
	int n;
	cin>>n;
	dfs(n);
	cout<<endl;
	return 0;
}
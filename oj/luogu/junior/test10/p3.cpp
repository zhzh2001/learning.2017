#include<iostream>
#include<cstring>
#define mid (l+r)/2
using namespace std;
int a[40005],b[40005],ans;
void merge_sort(int l,int r)
{
	if(l>=r)
		return;
	merge_sort(l,mid);
	merge_sort(mid+1,r);
	int i=l,j=mid+1,p=l;
	while(i<=mid&&j<=r)
		if(a[i]>a[j])
		{
			ans+=(mid-i+1);
			b[p++]=a[j++];
		}
		else
			b[p++]=a[i++];
	while(i<=mid)
		b[p++]=a[i++];
	while(j<=r)
		b[p++]=a[j++];
	memcpy(a+l,b+l,(r-l+1)*sizeof(int));
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	ans=0;
	merge_sort(1,n);
	cout<<ans<<endl;
	return 0;
}
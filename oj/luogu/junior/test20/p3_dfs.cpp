#include<bits/stdc++.h>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1},INF=1e9;
string mat[1505];
int n,m;
struct point
{
	int x,y;
};
bool operator<(const point a,const point b)
{
	if(a.x!=b.x)
		return a.x<b.x;
	return a.y<b.y;
}
point last[1505][1505];
set<point> s;
bool dfs(int x,int y)
{
	for(int i=0;i<4;i++)
	{
		int nx=x+dx[i],ny=y+dy[i],rx=(nx%n+n)%n,ry=(ny%m+m)%m;
		if(mat[rx][ry]!='#'&&!s.count((point){nx,ny}))
		{
			if(last[rx][ry].x!=INF&&last[rx][ry].y!=INF&&(last[rx][ry].x!=nx||last[rx][ry].y!=ny))
				return true;
			last[rx][ry].x=nx;last[rx][ry].y=ny;
			s.insert((point){nx,ny});
			if(dfs(nx,ny))
				return true;
		}
	}
	return false;
}
int main()
{
	while(cin>>n>>m)
	{
		int sx=-1,sy;
		for(int i=0;i<n;i++)
		{
			cin>>mat[i];
			if(sx==-1)
				for(int j=0;j<m;j++)
					if(mat[i][j]=='S')
						sx=i,sy=j;
		}
		for(int i=0;i<n;i++)
			for(int j=0;j<m;j++)
				last[i][j].x=last[i][j].y=INF;
		s.clear();
		s.insert((point){sx,sy});
		last[sx][sy].x=sx;last[sx][sy].y=sy;
		if(dfs(sx,sy))
			cout<<"Yes\n";
		else
			cout<<"No\n";
	}
	return 0;
}
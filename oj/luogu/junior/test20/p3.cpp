#include<bits/stdc++.h>
using namespace std;
const int dx[]={-1,1,0,0},dy[]={0,0,-1,1};
string mat[1505];
struct point
{
	int x,y;
	point(int nx=0,int ny=0):x(nx),y(ny){};
};
bool operator<(const point a,const point b)
{
	if(a.x!=b.x)
		return a.x<b.x;
	return a.y<b.y;
}
set<point> s;
queue<point> q;
int main()
{
	int n,m;
	while(cin>>n>>m)
	{
		point src(-1,-1);
		for(int i=0;i<n;i++)
		{
			cin>>mat[i];
			if(src.x==-1)
				for(int j=0;j<m;j++)
					if(mat[i][j]=='S')
						src.x=i,src.y=j;
		}
		s.clear();
		while(!q.empty())
			q.pop();
		q.push(src);
		s.insert(src);
		bool found=false;
		while(!q.empty())
		{
			point k=q.front();q.pop();
			if((k.x<0||k.x>=n||k.y<0||k.y>=m)&&s.count((point){(k.x%n+n)%n,(k.y%m+m)%m}))
			{
				found=true;
				break;
			}
			for(int i=0;i<4;i++)
			{
				int nx=k.x+dx[i],ny=k.y+dy[i],rx=(nx%n+n)%n,ry=(ny%m+m)%m;
				if(!s.count((point){nx,ny})&&mat[rx][ry]!='#')
				{
					point tmp(nx,ny);
					s.insert(tmp);
					q.push(tmp);
				}
			}
		}
		if(found)
			cout<<"Yes\n";
		else
			cout<<"No\n";
	}
	return 0;
}
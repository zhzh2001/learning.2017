#include<bits/stdc++.h>
using namespace std;
struct bigint
{
	int len,dig[10005];
	bool neg;
	bigint();
	bigint(int x);
	void clear();
	bigint operator+(bigint b);
	bigint operator-(bigint b);
	bigint operator*(bigint b);
	bool operator<(const bigint& b);
};

istream& operator>>(istream& is,bigint& b);
ostream& operator<<(ostream& os,const bigint& b);

bigint::bigint()
{
	len=1;
	neg=false;
	memset(dig,0,sizeof(dig));
}

bigint::bigint(int x)
{
	neg=(x<0);
	len=0;
	do
		dig[++len]=x%10;
	while(x/=10);
}

void bigint::clear()
{
	while(len>1&&dig[len]==0)
		len--;
}

bigint bigint::operator+(bigint b)
{
	bigint ret;
	if(neg&&!b.neg)
	{
		ret=*this;
		ret.neg=false;
		ret=b-ret;
	}
	else
		if(!neg&&b.neg)
		{
			ret=b;
			b.neg=false;
			ret=ret-b;
		}
		else
		{
			ret.len=max(len,b.len);
			int overflow=0;
			for(int i=1;i<=ret.len;i++)
			{
				ret.dig[i]=dig[i]+b.dig[i]+overflow;
				overflow=ret.dig[i]/10;
				ret.dig[i]%=10;
			}
			if(overflow)
				ret.dig[++ret.len]=overflow;
			ret.neg=neg;
		}
	return ret;
}

bigint bigint::operator-(bigint b)
{
	bigint ret;
	if(b.neg)
	{
		ret=b;
		ret.neg=false;
		ret=*this+ret;
	}
	else
		if(neg)
		{
			ret=b;
			ret.neg=true;
			ret=*this+ret;
		}
		else
		{
			if(*this<b)
			{
				ret.neg=true;
				swap(*this,b);
			}
			ret.len=len;
			int unflow=0;
			for(int i=1;i<=ret.len;i++)
			{
				ret.dig[i]=dig[i]-b.dig[i]-unflow;
				unflow=(ret.dig[i]<0);
				ret.dig[i]=(ret.dig[i]+10)%10;
			}
			ret.clear();
		}
	return ret;
}

bigint bigint::operator*(bigint b)
{
	bigint ret;
	ret.len=len+b.len-1;
	for(int i=1;i<=len;i++)
		for(int j=1;j<=b.len;j++)
			ret.dig[i+j-1]+=dig[i]*b.dig[j];
	for(int i=1;i<=ret.len;i++)
	{
		ret.dig[i+1]+=(ret.dig[i]/10);
		ret.dig[i]%=10;
	}
	if(ret.dig[ret.len+1])
		ret.len++;
	ret.clear();
	ret.neg=neg^b.neg;
	return ret;
}

bool bigint::operator<(const bigint& b)
{
	if(len!=b.len)
		return len<b.len;
	for(int i=len;i>=1;i--)
		if(dig[i]!=b.dig[i])
			return dig[i]<b.dig[i];
	return false;
}

istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b.len=s.length();
	if(s[0]=='-')
	{
		b.neg=true;
		b.len--;
		s=s.erase(0,1);
	}
	else
		b.neg=false;
	for(int i=0;i<b.len;i++)
		b.dig[b.len-i]=s[i]-'0';
	return is;
}

ostream& operator<<(ostream& os,const bigint& b)
{
	if(b.neg)
		os<<'-';
	for(int i=b.len;i>=1;i--)
		os<<b.dig[i];
	return os;
}

int main()
{
	int n;
	cin>>n;
	if(n==1)
		cout<<1<<endl;
	else
		if(n==2)
			cout<<2<<endl;
		else
		{
			bigint f1(1),f2(2),f;
			for(int i=3;i<=n;i++)
			{
				f=f1+f2;
				f1=f2;
				f2=f;
			}
			cout<<f<<endl;
		}
	return 0;
}
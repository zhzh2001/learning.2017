#include<bits/stdc++.h>
using namespace std;
struct bigint
{
	int len,dig[505];
	bigint()
	{
		len=0;
		memset(dig,0,sizeof(dig));
	}
	bigint operator+(const bigint& b)
	{
		bigint ret;
		ret.len=max(len,b.len);
		int overflow=0;
		for(int i=1;i<=ret.len;i++)
		{
			ret.dig[i]=dig[i]+b.dig[i]+overflow;
			overflow=ret.dig[i]/10;
			ret.dig[i]%=10;
		}
		if(overflow)
			ret.dig[++ret.len]=overflow;
		return ret;
	}
};
istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b.len=s.length();
	for(int i=0;i<b.len;i++)
		b.dig[b.len-i]=s[i]-'0';
	return is;
}
ostream& operator<<(ostream& os,const bigint& b)
{
	for(int i=b.len;i>=1;i--)
		os<<b.dig[i];
	return os;
}
int main()
{
	bigint a,b;
	cin>>a>>b;
	cout<<a+b<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int INF=1e9;
int a[105],f[105];
int main()
{
	int l,s,t,n;
	cin>>l>>s>>t>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	a[0]=0;
	a[n+1]=l;
	f[0]=0;
	for(int i=1;i<=n+1;i++)
	{
		f[i]=INF;
		for(int j=0;j<i;j++)
			if(a[i]-a[j]>=s&&a[i]-a[j]<=t)
				f[i]=min(f[i],f[j]+1);
	}
	cout<<f[n+1]<<endl;
	return 0;
}
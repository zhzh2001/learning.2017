#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;
struct node
{
	int a,b,c,num;
};
vector<node> l;
bool cmp(node x,node y)
{
	if(x.a+x.b+x.c!=y.a+y.b+y.c)
		return x.a+x.b+x.c>y.a+y.b+y.c;
	if(x.a!=y.a)
		return x.a>y.a;
	return x.num<y.num;
}
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		node x;
		cin>>x.a>>x.b>>x.c;
		x.num=i;
		l.push_back(x);
	}
	sort(l.begin(),l.end(),cmp);
	for(int i=0;i<5;i++)
		cout<<l[i].num<<' '<<l[i].a+l[i].b+l[i].c<<endl;
	return 0;
}
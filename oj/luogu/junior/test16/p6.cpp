#include<bits/stdc++.h>
using namespace std;
const int INF=1e9;
int a[1005],b[1005],f[1005][6005];
int main()
{
	int n;
	cin>>n;
	int sum=0;
	for(int i=1;i<=n;i++)
	{
		cin>>a[i]>>b[i];
		sum+=a[i]+b[i];
	}
	f[0][0]=0;
	for(int j=1;j<=sum/2;j++)
		f[0][j]=INF;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=sum/2;j++)
		{
			f[i][j]=INF;
			if(j>=a[i])
				f[i][j]=min(f[i][j],f[i-1][j-a[i]]);
			if(j>=b[i])
				f[i][j]=min(f[i][j],f[i-1][j-b[i]]+1);
		}
	for(int i=sum/2;i>=1;i--)
	{
		int ans=min(f[n][i],f[n][sum-i]);
		if(ans<INF)
		{
			cout<<ans<<endl;
			break;
		}
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int INF=1e9;
int a[105],s[205],f[205][205];
int main()
{
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	s[0]=0;
	for(int i=1;i<2*n;i++)
		s[i]=s[i-1]+a[(i-1)%n+1];
	for(int i=2*n-1;i>=1;i--)
		for(int j=i+1;j<2*n;j++)
		{
			f[i][j]=INF;
			for(int k=i;k<j;k++)
				f[i][j]=min(f[i][j],f[i][k]+f[k+1][j]+s[j]-s[i-1]);
		}
	int ans=f[1][n];
	for(int i=2;i<=n;i++)
		ans=min(ans,f[i][i+n-1]);
	cout<<ans<<endl;
	for(int i=2*n-1;i>=1;i--)
		for(int j=i+1;j<2*n;j++)
		{
			f[i][j]=0;
			for(int k=i;k<j;k++)
				f[i][j]=max(f[i][j],f[i][k]+f[k+1][j]+s[j]-s[i-1]);
		}
	ans=f[1][n];
	for(int i=2;i<=n;i++)
		ans=max(ans,f[i][i+n-1]);
	cout<<ans<<endl;
	return 0;
}
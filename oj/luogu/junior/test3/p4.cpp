#include<iostream>
#include<cstring>
#include<string>
using namespace std;
struct bigint
{
	int len,dig[105];
	bigint()
	{
		len=0;
		memset(dig,0,sizeof(dig));
	}
	bool operator<(const bigint& b)
	{
		if(len!=b.len)
			return len<b.len;
		for(int i=len;i>=1;i--)
			if(dig[i]!=b.dig[i])
				return dig[i]<b.dig[i];
		return false;
	}
};
istream& operator>>(istream& is,bigint& b)
{
	string s;
	is>>s;
	b.len=s.length();
	for(int i=0;i<s.length();i++)
		b.dig[b.len-i]=s[i]-'0';
	return is;
}
ostream& operator<<(ostream& os,const bigint& b)
{
	for(int i=b.len;i>=1;i--)
		os<<b.dig[i];
	return os;
}
int main()
{
	int n;
	cin>>n;
	int ans=0;
	bigint cnt;
	for(int i=1;i<=n;i++)
	{
		bigint x;
		cin>>x;
		if(cnt<x)
		{
			cnt=x;
			ans=i;
		}
	}
	cout<<ans<<endl<<cnt<<endl;
	return 0;
}
#include<iostream>
using namespace std;
struct node
{
	int pred,next;
};
node l[100005];
bool vis[100005];
int main()
{
	int n;
	cin>>n;
	l[0].pred=-1;l[0].next=1;
	l[1].pred=0;l[1].next=-1;
	vis[1]=true;
	for(int i=2;i<=n;i++)
	{
		int x;
		bool r;
		cin>>x>>r;
		if(r)
		{
			l[i].pred=x;
			l[i].next=l[x].next;
			l[l[x].next].pred=i;
			l[x].next=i;
		}
		else
		{
			l[i].pred=l[x].pred;
			l[l[x].pred].next=i;
			l[x].pred=i;
			l[i].next=x;
		}
		vis[i]=true;
	}
	int m;
	cin>>m;
	while(m--)
	{
		int x;
		cin>>x;
		if(vis[x])
		{
			l[l[x].pred].next=l[x].next;
			l[l[x].next].pred=l[x].pred;
			l[x].pred=l[x].next=-1;
			vis[x]=false;
		}
	}
	for(int p=l[0].next;p!=-1;p=l[p].next)
		cout<<p<<' ';
	return 0;
}
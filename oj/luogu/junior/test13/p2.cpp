#include<cstdio>
#include<algorithm>
#include<cctype>
using namespace std;
int getint()
{
	char c;
	while(!isdigit(c=getchar())&&c!='-');
	bool neg=false;
	int ret;
	if(c=='-')
	{
		neg=true;
		ret=0;
	}
	else
		ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return neg?-ret:ret;
}
int main()
{
	int n=getint(),now=getint(),ans=now;
	for(int i=2;i<=n;i++)
	{
		if(now<0)
			now=0;
		int x=getint();
		now+=x;
		ans=max(ans,now);
	}
	printf("%d\n",ans);
	return 0;
}
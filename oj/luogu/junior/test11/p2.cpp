#include<iostream>
#include<cstring>
#include<cmath>
using namespace std;
struct bigint
{
	int len,dig[505];
	bigint()
	{
		len=0;
		memset(dig,0,sizeof(dig));
	}
	bigint(int x)
	{
		len=1;
		memset(dig,0,sizeof(dig));
		dig[1]=x;
	}
	bigint operator--(int x)
	{
		dig[1]--;
		for(int i=1;dig[i]<0;i++)
		{
			dig[i]+=10;
			dig[i+1]--;
		}
	}
	bigint operator*(const bigint& b)
	{
		bigint c;
		c.len=min(len+b.len,500);
		for(int i=1;i<=len;i++)
			for(int j=1;j<=b.len;j++)
				if(i+j-1<=500)
					c.dig[i+j-1]+=dig[i]*b.dig[j];
		for(int i=1;i<=c.len;i++)
		{
			c.dig[i+1]+=c.dig[i]/10;
			c.dig[i]%=10;
		}
		if(c.len<500&&c.dig[c.len+1])
			c.len++;
		return c;
	}
	bigint operator*=(const bigint& b)
	{
		*this=*this*b;
		return *this;
	}
};
struct bigdouble
{
	static const double maxd=1e150;
	static const int maxl=150;
	double d;
	int cnt;
	bigdouble()
	{
		d=0;cnt=0;
	}
	bigdouble(double x)
	{
		d=x;cnt=0;
	}
	bigdouble operator*(const bigdouble b)
	{
		bigdouble ret;
		ret.d=d*b.d;
		ret.cnt=cnt+b.cnt;
		if(ret.d>maxd)
		{
			ret.d/=maxd;
			ret.cnt++;
		}
		return ret;
	}
	bigdouble operator*=(const bigdouble b)
	{
		*this=*this*b;
		return *this;
	}
};
int main()
{
	int p;
	cin>>p;
	bigint now(2),ans(1);
	bigdouble fnow(2),fans(1);
	do
	{
		if(p%2)
		{
			ans*=now;
			fans*=fnow;
		}
		now*=now;
		fnow*=fnow;
	}
	while(p/=2);
	cout<<int(log10(fans.d)+bigdouble::maxl*fans.cnt+1)<<endl;
	ans--;
	for(int i=500;i>=1;i--)
	{
		cout<<ans.dig[i];
		if((i-1)%50==0)
			cout<<endl;
	}
	return 0;
}
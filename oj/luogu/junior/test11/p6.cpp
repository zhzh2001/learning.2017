#include<iostream>
using namespace std;
long long gcd(long long a,long long b)
{
	return a%b==0?b:gcd(b,a%b);
}
int main()
{
	long long a,b;
	cin>>a>>b;
	int ans=0;
	long long i;
	for(i=1;i*i<=a*b;i++)
		if(a*b%i==0)
		{
			long long j=a*b/i;
			if(gcd(i,j)==a)
				ans++;
		}
	if(a!=b)
		ans*=2;
	cout<<ans<<endl;
	return 0;
}
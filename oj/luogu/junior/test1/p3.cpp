#include<iostream>
#include<cstdlib>
using namespace std;
int main()
{
	int n;
	cin>>n;
	for(int i=n;i>=0;i--)
	{
		int a;
		cin>>a;
		if(!a)
			continue;
		if(i<n&&a>0)
			cout<<'+';
		if(i==0||abs(a)>1)
			cout<<a;
		if(i>0&&a==-1)
			cout<<'-';
		if(i>0)
			cout<<'x';
		if(i>1)
			cout<<'^'<<i;
	}
	cout<<endl;
	return 0;
}
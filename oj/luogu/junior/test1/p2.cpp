#include<iostream>
#include<string>
#include<cstdlib>
using namespace std;
const char dig[]="0123456789ABCDEFGHIJ";
int main()
{
	int n,r;
	cin>>n>>r;
	cout<<n<<'=';
	string s="";
	for(int i=1;n;i++)
	{
		int t=n%r;
		n/=r;
		if(t<0)
		{
			t+=abs(r);
			if(n<0)
				n--;
			else
				n++;
		}
		s=dig[t]+s;
	}
	cout<<s<<"(base"<<r<<")\n";
	return 0;
}
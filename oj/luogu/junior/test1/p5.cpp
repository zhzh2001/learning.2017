#include<iostream>
#include<cstring>
#include<algorithm>
using namespace std;
int lcnt[1005],rcnt[1005],a[1005];
struct node
{
	int i,cnt;
};
node ls[1005],r[1005];
bool cmp(node a,node b)
{
	return a.cnt>b.cnt;
}
int main()
{
	int m,n,k,l,d;
	cin>>m>>n>>k>>l>>d;
	memset(lcnt,0,sizeof(lcnt));
	memset(rcnt,0,sizeof(rcnt));
	while(d--)
	{
		int x1,y1,x2,y2;
		cin>>x1>>y1>>x2>>y2;
		if(y1==y2)
			lcnt[min(x1,x2)]++;
		else
			rcnt[min(y1,y2)]++;
	}
	for(int i=1;i<m;i++)
		ls[i]=(node){i,lcnt[i]};
	for(int i=1;i<n;i++)
		r[i]=(node){i,rcnt[i]};
	sort(ls+1,ls+m,cmp);
	sort(r+1,r+n,cmp);
	for(int i=1;i<=k;i++)
		a[i]=ls[i].i;
	sort(a+1,a+k+1);
	for(int i=1;i<=k;i++)
	{
		if(i>1)
			cout<<' ';
		cout<<a[i];
	}
	cout<<endl;
	for(int i=1;i<=l;i++)
		a[i]=r[i].i;
	sort(a+1,a+l+1);
	for(int i=1;i<=l;i++)
	{
		if(i>1)
			cout<<' ';
		cout<<a[i];
	}
	cout<<endl;
	return 0;
}
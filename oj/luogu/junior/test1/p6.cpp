#include<iostream>
#include<string>
#include<cstring>
#include<algorithm>
using namespace std;
int cnt[26];
bool prime(int n)
{
	if(n<2)
		return false;
	for(int i=2;i*i<=n;i++)
		if(n%i==0)
			return false;
	return true;
}
int main()
{
	string s;
	cin>>s;
	memset(cnt,0,sizeof(cnt));
	for(int i=0;i<s.length();i++)
		cnt[s[i]-'a']++;
	int maxn=0,minn=s.length();
	for(int i=0;i<26;i++)
		if(cnt[i])
		{
			maxn=max(maxn,cnt[i]);
			minn=min(minn,cnt[i]);
		}
	if(prime(maxn-minn))
		cout<<"Lucky Word\n"<<maxn-minn<<endl;
	else
		cout<<"No Answer\n0\n";
	return 0;
}
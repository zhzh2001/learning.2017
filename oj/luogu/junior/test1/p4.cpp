#include<iostream>
#include<cstring>
using namespace std;
bool vis[1005];
int q[100];
int main()
{
	int m,n;
	cin>>m>>n;
	memset(vis,false,sizeof(vis));
	memset(q,-1,sizeof(q));
	int ans=0,r=0;
	for(int i=1;i<=n;i++)
	{
		int x;
		cin>>x;
		if(!vis[x])
		{
			if(vis[q[r]])
				vis[q[r]]=false;
			q[r]=x;
			r=(r+1)%m;
			vis[x]=true;
			ans++;
		}
	}
	cout<<ans<<endl;
	return 0;
}
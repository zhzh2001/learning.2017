#include<iostream>
#include<cstring>
#include<queue>
using namespace std;
int d[205];
bool vis[205];
struct node
{
	int h,step;
};
queue<node> q;
int main()
{
	int n,a,b;
	cin>>n>>a>>b;
	for(int i=1;i<=n;i++)
		cin>>d[i];
	memset(vis,false,sizeof(vis));
	vis[a]=true;
	q.push((node){a,0});
	while(!q.empty())
	{
		node k=q.front();q.pop();
		if(k.h==b)
		{
			cout<<k.step<<endl;
			return 0;
		}
		int nh=k.h+d[k.h];
		if(nh<=n&&!vis[nh])
		{
			vis[nh]=true;
			q.push((node){nh,k.step+1});
		}
		nh=k.h-d[k.h];
		if(nh>0&&!vis[nh])
		{
			vis[nh]=true;
			q.push((node){nh,k.step+1});
		}
	}
	cout<<-1<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=300005,M=50005;
int a[N],b[N],sqrtn,ans[M];
struct question
{
	int l,r,k,id;
	bool operator<(const question& rhs)const
	{
		int lb=l/sqrtn,rb=rhs.l/sqrtn;
		if(lb==rb)
			return r<rhs.r;
		return lb<rb;
	}
}q[M];
int tree[N*4],val,delta;
void modify(int id,int l,int r)
{
	tree[id]+=delta;
	if(l==r)
		return;
	int mid=(l+r)/2;
	if(val<=mid)
		modify(id*2,l,mid);
	else
		modify(id*2+1,mid+1,r);
}
int findkth(int id,int l,int r)
{
	if(l==r)
		return l;
	int mid=(l+r)/2;
	if(val<=tree[id*2])
		return findkth(id*2,l,mid);
	val-=tree[id*2];
	return findkth(id*2+1,mid+1,r);
}
int main()
{
	int n,m;
	cin>>n>>m;
	sqrtn=floor(sqrt(n));
	for(int i=1;i<=n;i++)
		cin>>a[i];
	copy(a+1,a+n+1,b+1);
	sort(b+1,b+n+1);
	for(int i=1;i<=n;i++)
		a[i]=lower_bound(b+1,b+n+1,a[i])-b;
	for(int i=1;i<=m;i++)
	{
		cin>>q[i].l>>q[i].r>>q[i].k;
		q[i].id=i;
	}
	sort(q+1,q+m+1);
	int l=1,r=0;
	for(int i=1;i<=m;i++)
	{
		while(l<q[i].l)
		{
			val=a[l++];
			delta=-1;
			modify(1,1,n);
		}
		while(l>q[i].l)
		{
			val=a[--l];
			delta=1;
			modify(1,1,n);
		}
		while(r<q[i].r)
		{
			val=a[++r];
			delta=1;
			modify(1,1,n);
		}
		while(r>q[i].r)
		{
			val=a[r--];
			delta=-1;
			modify(1,1,n);
		}
		val=q[i].k;
		ans[q[i].id]=b[findkth(1,1,n)];
	}
	for(int i=1;i<=m;i++)
		cout<<ans[i]<<endl;
	return 0;
}
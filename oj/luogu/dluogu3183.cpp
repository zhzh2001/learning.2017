#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=200005;
int head[N],v[M],nxt[M],into[N],outo[N],e,f[N];
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int dfs(int k)
{
	if(!outo[k])
		return 1;
	if(f[k])
		return f[k];
	for(int i=head[k];i;i=nxt[i])
		f[k]+=dfs(v[i]);
	return f[k];
}
int main()
{
	int n,m;
	cin>>n>>m;
	while(m--)
	{
		int u,v;
		cin>>u>>v;
		add_edge(u,v);
		into[v]++;
		outo[u]++;
	}
	int ans=0;
	for(int i=1;i<=n;i++)
		if(!into[i]&&outo[i])
			ans+=dfs(i);
	cout<<ans<<endl;
	return 0;
}
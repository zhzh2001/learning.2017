#include <iostream>
#include <cstring>
using namespace std;
const int MOD = 1e9 + 7;
struct matrix
{
	static const int n = 3;
	int mat[n][n];
	matrix()
	{
		memset(mat, 0, sizeof(mat));
	}
	matrix operator*(const matrix &rhs) const
	{
		matrix ret;
		for (int k = 0; k < n; k++)
			for (int i = 0; i < n; i++)
				for (int j = 0; j < n; j++)
					ret.mat[i][j] = (ret.mat[i][j] + 1ll * mat[i][k] * rhs.mat[k][j]) % MOD;
		return ret;
	}
	matrix &operator*=(const matrix &rhs)
	{
		return *this = *this * rhs;
	}
};
matrix I()
{
	matrix ret;
	for (int i = 0; i < matrix::n; i++)
		ret.mat[i][i] = 1;
	return ret;
}
matrix qpow(matrix a, int b)
{
	matrix ret = I();
	do
	{
		if (b & 1)
			ret *= a;
		a *= a;
	} while (b /= 2);
	return ret;
}
int main()
{
	int t;
	cin >> t;
	matrix trans, init;
	trans.mat[0][0] = trans.mat[0][2] = trans.mat[1][0] = trans.mat[2][1] = 1;
	init.mat[0][0] = init.mat[1][0] = init.mat[2][0] = 1;
	while (t--)
	{
		int n;
		cin >> n;
		if (n <= 3)
			cout << 1 << endl;
		else
			cout << (qpow(trans, n - 3) * init).mat[0][0] << endl;
	}
	return 0;
}
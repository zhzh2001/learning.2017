#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;
const int N = 100005, LOGN = 18;
int st[N][LOGN];
int main()
{
	int n, m;
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		scanf("%d", st[i]);
	for (int j = 1; j < LOGN; j++)
		for (int i = 1; i + (1 << j) - 1 <= n; i++)
			st[i][j] = max(st[i][j - 1], st[i + (1 << j - 1)][j - 1]);
	while (m--)
	{
		int l, r;
		scanf("%d%d", &l, &r);
		int len = floor(log2(r - l + 1));
		printf("%d\n", max(st[l][len], st[r - (1 << len) + 1][len]));
	}
	return 0;
}
#include <iostream>
using namespace std;
const int N = 1e8 + 5;
bool bl[N];
int p[N / 10];
int main()
{
	int n;
	cin >> n;
	int pn = 0;
	for (int i = 2; i <= n; i++)
	{
		if (!bl[i])
			p[++pn] = i;
		for (int j = 1; j <= pn && i * p[j] <= n; j++)
		{
			bl[i * p[j]] = true;
			if (i % p[j] == 0)
				break;
		}
	}
	cout << pn << endl;
	return 0;
}
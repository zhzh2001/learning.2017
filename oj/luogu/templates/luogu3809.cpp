#include<bits/stdc++.h>
using namespace std;
const int N=1000005;
struct node
{
	int x,y,id;
	bool operator<(const node& rhs)const
	{
		if(x==rhs.x)
			return y<rhs.y;
		return x<rhs.x;
	}
}a[N];
int rank[N],sa[N];
int main()
{
	string s;
	cin>>s;
	int n=s.length();
	s=' '+s;
	for(int i=1;i<=n;i++)
		rank[i]=s[i];
	for(int i=1;i<=n;i*=2)
	{
		for(int j=1;j<=n;j++)
		{
			a[j].id=j;
			a[j].x=rank[j];
			a[j].y=j+i<=n?rank[j+i]:0;
		}
		sort(a+1,a+n+1);
		int cnt=0;
		for(int j=1;j<=n;j++)
		{
			cnt+=a[j].x!=a[j-1].x||a[j].y!=a[j-1].y;
			rank[a[j].id]=cnt;
		}
	}
	for(int i=1;i<=n;i++)
		sa[rank[i]]=i;
	for(int i=1;i<=n;i++)
		cout<<sa[i]<<' ';
	cout<<endl;
	return 0;
}
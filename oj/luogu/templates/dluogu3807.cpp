#include <iostream>
using namespace std;
const int N = 1e5;
int fact[N + 5], inv[N + 5];
int qpow(int a, int b, int p)
{
	int ans = 1;
	do
	{
		if (b & 1)
			ans = 1ll * ans * a % p;
		a = 1ll * a * a % p;
	} while (b /= 2);
	return ans;
}
int c(int n, int m, int p)
{
	if (m > n)
		return 0;
	return 1ll * fact[n] * inv[n - m] * inv[m] % p;
}
int lucas(int n, int m, int p)
{
	if (!n)
		return 1;
	return 1ll * c(n % p, m % p, p) * lucas(n / p, m / p, p) % p;
}
int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n, m, p;
		cin >> n >> m >> p;
		fact[0] = 1;
		for (int i = 1; i < p; i++)
			fact[i] = 1ll * fact[i - 1] * i % p;
		inv[p - 1] = qpow(fact[p - 1], p - 2, p);
		for (int i = p - 2; i >= 0; i--)
			inv[i] = 1ll * inv[i + 1] * (i + 1) % p;
		cout << lucas(n + m, m, p) << endl;
	}
	return 0;
}
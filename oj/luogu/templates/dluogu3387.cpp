#include <iostream>
#include <vector>
using namespace std;
const int N = 10005;
int a[N];
vector<int> mat[N];
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u].push_back(v);
	}
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1e8 + 5;
bool p[N];
int main()
{
	int n;
	cin >> n;
	for (int i = 2; i * i <= n; i++)
		if (!p[i])
			for (int j = i * i; j <= n; j += i)
				p[j] = true;
	cout << count(p + 2, p + n + 1, false) << endl;
	return 0;
}
#include <fstream>
#include <algorithm>
using namespace std;
ofstream fout("table.txt");
const int n = 1e8, k = 2e4;
bool p[n + 5];
int main()
{
	for (int i = 2; i * i <= n; i++)
		if (!p[i])
			for (int j = i * i; j <= n; j += i)
				p[j] = true;
	int ans = 0;
	for (int i = 2; i <= n; i++)
	{
		ans += !p[i];
		if (i % k == 0)
		{
			fout << ans << ',';
			ans = 0;
		}
	}
	return 0;
}
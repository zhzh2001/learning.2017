#include<bits/stdc++.h>
using namespace std;
int qpow(long long a,int b,int p)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=ans*a%p;
		a=a*a%p;
	}
	while(b/=2);
	return ans;
}
int main()
{
	int n,p;
	scanf("%d%d",&n,&p);
	for(int i=1;i<=n;i++)
		printf("%d\n",qpow(i,p-2,p));
	return 0;
}
#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;
const int N = 100005;
int p[N];
int main()
{
	ios::sync_with_stdio(false);
	int n;
	cin >> n;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		p[x] = i;
	}
	vector<int> s;
	for (int i = 1; i <= n; i++)
	{
		int x;
		cin >> x;
		auto it = lower_bound(s.begin(), s.end(), p[x]);
		if (it == s.end())
			s.push_back(p[x]);
		else
			*it = p[x];
	}
	cout << s.size() << endl;
	return 0;
}
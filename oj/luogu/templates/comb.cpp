#include <iostream>
#include <gmpxx.h>
using namespace std;
int main()
{
	int n, m, p;
	cin >> n >> m >> p;
	mpz_class ans;
	mpz_bin_uiui(ans.get_mpz_t(), n + m, m);
	cout << ans % p << endl;
	return 0;
}
#include <iostream>
#include <vector>
using namespace std;
const int N = 1000005;
int a[N], ans[N];
vector<int> mat[N];
struct quest
{
	int opt, x, y, id;
} q[N];
void dfs(int k)
{
	for (auto v : mat[k])
	{
		int tmp = a[q[v].x];
		if (q[v].opt == 1)
			a[q[v].x] = q[v].y;
		else
			ans[q[v].id] = tmp;
		dfs(v);
		if (q[v].opt == 1)
			a[q[v].x] = tmp;
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n, m;
	cin >> n >> m;
	for (int i = 1; i <= n; i++)
		cin >> a[i];
	for (int i = 1; i <= m; i++)
	{
		int fat;
		cin >> fat >> q[i].opt >> q[i].x;
		if (q[i].opt == 1)
			cin >> q[i].y;
		mat[fat].push_back(i);
		q[i].id = i;
	}
	dfs(0);
	for (int i = 1; i <= m; i++)
		if (q[i].opt == 2)
			cout << ans[i] << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
using namespace std;
const int N = 1e5;
int qpow(int a, int b, int p)
{
	int ans = 1;
	do
	{
		if (b & 1)
			ans = 1ll * ans * a % p;
		a = 1ll * a * a % p;
	} while (b /= 2);
	return ans;
}
int main()
{
	int t;
	cin >> t;
	while (t--)
	{
		int n, m, p;
		cin >> n >> m >> p;
		int c = 1;
		for (int i = 1; i <= n; i++)
			c = 1ll * c * (n + m - i + 1) % p * qpow(i, p - 2, p) % p;
		cout << c << endl;
	}
	return 0;
}
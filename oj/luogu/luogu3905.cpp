#include <iostream>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 105, INF = 0x3f3f3f3f;
int road[N][N], mat[N][N], d[N];
bool vis[N];
int main()
{
	int n, m;
	cin >> n >> m;
	memset(mat, 0x3f, sizeof(mat));
	while (m--)
	{
		int u, v, w;
		cin >> u >> v >> w;
		road[u][v] = road[v][u] = w;
		mat[u][v] = mat[v][u] = 0;
	}
	cin >> m;
	while (m--)
	{
		int u, v;
		cin >> u >> v;
		mat[u][v] = mat[v][u] = road[u][v];
	}
	int s, t;
	cin >> s >> t;
	for (int i = 0; i <= n; i++)
		d[i] = i == s ? 0 : INF;
	for (int i = 1; i <= n; i++)
	{
		int j = 0;
		for (int k = 1; k <= n; k++)
			if (!vis[k] && d[k] < d[j])
				j = k;
		vis[j] = true;
		for (int k = 1; k <= n; k++)
			if (!vis[k])
				d[k] = min(d[k], d[j] + mat[j][k]);
	}
	cout << d[t] << endl;
	return 0;
}
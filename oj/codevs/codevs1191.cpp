#include<bits/stdc++.h>
using namespace std;
const int N=200005;
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n,m;
	scanf("%d%d",&n,&m);
	for(int i=0;i<=n;i++)
		f[i]=i;
	while(m--)
	{
		int l,r;
		scanf("%d%d",&l,&r);
		while(getf(r)!=getf(l-1))
		{
			f[getf(r)]=getf(r)-1;
			n--;
		}
		printf("%d\n",n);
	}
	return 0;
}
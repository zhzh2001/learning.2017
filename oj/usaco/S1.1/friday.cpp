/*
ID: zhangzh62
PROG: friday
LANG: C++
*/
#include <fstream>
#include <cstring>
using namespace std;
ifstream fin("friday.in");
ofstream fout("friday.out");
int days[] = {0, 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}, cnt[7];
int main()
{
	int n;
	fin >> n;
	int week = 5;
	memset(cnt, 0, sizeof(cnt));
	for (int i = 1900; i < 1900 + n; i++)
	{
		if (i % 4 == 0 && (i % 100 || i % 400 == 0))
			days[2] = 29;
		else
			days[2] = 28;
		for (int j = 1; j <= 12; j++)
		{
			cnt[week]++;
			week = (week + days[j]) % 7;
		}
	}
	for (int i = 0; i < 6; i++)
		fout << cnt[(i + 5) % 7] << ' ';
	fout << cnt[4] << endl;
	return 0;
}
/*
ID: zhangzh62
PROG: gift1
LANG: C++
*/
#include<fstream>
#include<string>
using namespace std;
ifstream fin("gift1.in");
ofstream fout("gift1.out");
struct node
{
	string s;
	int send,receive;
};
node a[15];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i].s;
	for(int i=1;i<=n;i++)
	{
		string s;
		fin>>s;
		int j;
		for(j=1;j<=n;j++)
			if(a[j].s==s)
				break;
		int m;
		fin>>a[j].send>>m;
		for(int t=1;t<=m;t++)
		{
			fin>>s;
			for(int k=1;k<=n;k++)
				if(a[k].s==s)
				{
					a[k].receive+=a[j].send/m;
					break;
				}
		}
		if(m)
			a[j].send-=a[j].send%m;
	}
	for(int i=1;i<=n;i++)
		fout<<a[i].s<<' '<<a[i].receive-a[i].send<<endl;
	return 0;
}
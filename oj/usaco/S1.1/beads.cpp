/*
ID: zhangzh62
PROG: beads
LANG: C++
*/
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;
ifstream fin("beads.in");
ofstream fout("beads.out");
int main()
{
	int n;
	fin >> n;
	string s;
	fin >> s;
	int ans = 0;
	for (int i = 0; i < n; i++)
	{
		int now = 0, j;
		char nc = s[i];
		for (j = i; (s[j] == nc || nc == 'w' || s[j] == 'w') && now < n; j = (j + 1) % n)
		{
			now++;
			if (nc == 'w' && s[j] != 'w')
				nc = s[j];
		}
		nc = s[(i - 1 + n) % n];
		for (j = (i - 1 + n) % n; (s[j] == nc || nc == 'w' || s[j] == 'w') && now < n; j = (j - 1 + n) % n)
		{
			now++;
			if (nc == 'w' && s[j] != 'w')
				nc = s[j];
		}
		ans = max(ans, now);
	}
	fout << ans << endl;
	return 0;
}
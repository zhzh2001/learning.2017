/*
ID: zhangzh62
PROG: fence6
LANG: C++11
*/
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("fence6.in");
ofstream fout("fence6.out");
const int N = 205, INF = 1e8;
int f[N][N], mat[N][N], dir[N][N];
int main()
{
	int n;
	fin >> n;
	fill_n(&mat[0][0], sizeof(mat) / sizeof(int), INF);
	for (int i = 1; i <= n; i++)
	{
		int id, w, d1, d2;
		fin >> id >> w >> d1 >> d2;
		mat[id][n + id] = mat[n + id][id] = w;
		while (d1--)
		{
			int u;
			fin >> u;
			dir[id][u] = 1;
		}
		while (d2--)
		{
			int u;
			fin >> u;
			dir[id][u] = 2;
		}
	}
	for (int i = 1; i < n; i++)
		for (int j = i + 1; j <= n; j++)
			if (dir[i][j])
			{
				int u = i, v = j;
				if (dir[i][j] == 2)
					u += n;
				if (dir[j][i] == 2)
					v += n;
				mat[u][v] = mat[v][u] = 0;
			}
	copy_n(&mat[0][0], sizeof(mat) / sizeof(int), &f[0][0]);
	int ans = INF;
	for (int k = 1; k <= n * 2; k++)
	{
		for (int i = 1; i < k; i++)
			for (int j = 1; j < i; j++)
				if (f[i][j])
					ans = min(ans, f[i][j] + mat[j][k] + mat[k][i]);
		for (int i = 1; i <= n * 2; i++)
			for (int j = 1; j <= n * 2; j++)
				f[i][j] = min(f[i][j], f[i][k] + f[k][j]);
	}
	fout << ans << endl;
	return 0;
}
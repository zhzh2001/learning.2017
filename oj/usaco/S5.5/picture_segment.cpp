/*
ID: zhangzh62
PROG: picture
LANG: C++
*/
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("picture.in");
ofstream fout("picture.out");
const int N = 5005;
int y[N * 2];
struct rect
{
	int x1, y1, x2, y2;
} r[N];
struct event
{
	int x, yl, yr, val;
	event(int x = 0, int yl = 0, int yr = 0, int val = 0) : x(x), yl(yl), yr(yr), val(val) {}
	bool operator<(const event &rhs) const
	{
		return x == rhs.x ? val > rhs.val : x < rhs.x;
	}
} e[N * 2];
struct node
{
	int val, sum;
} tree[1 << 15];
inline void pullup(int id, int l, int r)
{
	if (tree[id].val)
		tree[id].sum = y[r + 1] - y[l];
	else if (l == r)
		tree[id].sum = 0;
	else
		tree[id].sum = tree[id * 2].sum + tree[id * 2 + 1].sum;
}
void update(int id, int l, int r, int L, int R, int val)
{
	if (L <= l && R >= r)
	{
		tree[id].val += val;
		pullup(id, l, r);
	}
	else
	{
		int mid = (l + r) / 2;
		if (L <= mid)
			update(id * 2, l, mid, L, R, val);
		if (R > mid)
			update(id * 2 + 1, mid + 1, r, L, R, val);
		pullup(id, l, r);
	}
}
int main()
{
	ios::sync_with_stdio(false);
	int n;
	fin >> n;
	for (int i = 1; i <= n; i++)
		fin >> r[i].x1 >> r[i].y1 >> r[i].x2 >> r[i].y2;
	int ans = 0;
	for (int t = 0; t < 2; t++)
	{
		for (int i = 1; i <= n; i++)
		{
			e[2 * i - 1] = event(r[i].x1, r[i].y1, r[i].y2, 1);
			e[2 * i] = event(r[i].x2, r[i].y1, r[i].y2, -1);
			y[2 * i - 1] = r[i].y1;
			y[2 * i] = r[i].y2;
		}
		sort(e + 1, e + n * 2 + 1);
		sort(y + 1, y + n * 2 + 1);
		for (int i = 1, pred = 0; i <= n * 2; i++, pred = tree[1].sum)
		{
			int l = lower_bound(y + 1, y + n * 2 + 1, e[i].yl) - y, r = lower_bound(y + 1, y + n * 2 + 1, e[i].yr) - y;
			update(1, 1, n * 2, l, r - 1, e[i].val);
			ans += abs(tree[1].sum - pred);
		}
		for (int i = 1; i <= n; i++)
		{
			swap(r[i].x1, r[i].y1);
			swap(r[i].x2, r[i].y2);
		}
	}
	fout << ans << endl;
	return 0;
}
/*
ID: zhangzh62
PROG: milk
LANG: C++
*/
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("milk.in");
ofstream fout("milk.out");
struct node
{
	int price, cnt;
	bool operator<(const node &rhs) const
	{
		return price < rhs.price;
	}
} a[5005];
int main()
{
	int n, m;
	fin >> m >> n;
	for (int i = 1; i <= n; i++)
		fin >> a[i].price >> a[i].cnt;
	sort(a + 1, a + n + 1);
	int ans = 0;
	for (int i = 1; m; i++)
		if (m >= a[i].cnt)
		{
			m -= a[i].cnt;
			ans += a[i].cnt * a[i].price;
		}
		else
		{
			ans += m * a[i].price;
			m = 0;
		}
	fout << ans << endl;
	return 0;
}
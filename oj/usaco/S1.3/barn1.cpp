/*
ID: zhangzh62
PROG: barn1
LANG: C++
*/
#include <fstream>
#include <algorithm>
using namespace std;
ifstream fin("barn1.in");
ofstream fout("barn1.out");
int p[205], d[205];
int main()
{
	int m, s, c;
	fin >> m >> s >> c;
	int cnt = 0;
	for (int i = 1; i <= c; i++)
		fin >> p[i];
	sort(p + 1, p + c + 1);
	for (int i = 2; i <= c; i++)
		if (p[i] - p[i - 1] > 1)
			d[++cnt] = p[i] - p[i - 1] - 1;
	sort(d + 1, d + cnt + 1);
	int ans = c;
	for (int i = 1; i <= cnt - m + 1; i++)
		ans += d[i];
	fout << ans << endl;
	return 0;
}
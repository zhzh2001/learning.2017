/*
ID: zhangzh62
PROG: stall4
LANG: C++11
*/
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;
ifstream fin("stall4.in");
ofstream fout("stall4.out");
const int N = 205;
vector<int> mat[N];
bool vis[N];
int match[N];
bool dfs(int k)
{
	for (auto v : mat[k])
		if (!vis[v])
		{
			vis[v] = true;
			if (!match[v] || dfs(match[v]))
			{
				match[v] = k;
				return true;
			}
		}
	return false;
}
int main()
{
	int n, m;
	fin >> n >> m;
	for (int i = 1; i <= n; i++)
	{
		int s;
		fin >> s;
		while (s--)
		{
			int u;
			fin >> u;
			mat[i].push_back(u);
		}
	}
	int ans = 0;
	for (int i = 1; i <= n; i++)
	{
		fill(vis + 1, vis + n + 1, false);
		ans += dfs(i);
	}
	fout << ans << endl;
	return 0;
}
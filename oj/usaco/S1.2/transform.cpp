/*
ID: zhangzh62
PROG: transform
LANG: C++
*/
#include <fstream>
#include <string>
using namespace std;
ifstream fin("transform.in");
ofstream fout("transform.out");
string src[10], dest[10], now[10];
void rot(string *mat, int n)
{
	char tmp[10][10];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			tmp[j][n - i - 1] = mat[i][j];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			mat[i][j] = tmp[i][j];
}
void ref(string *mat, int n)
{
	char tmp[10][10];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			tmp[i][j] = mat[i][n - j - 1];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			mat[i][j] = tmp[i][j];
}
bool matequal(string *a, string *b, int n)
{
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
			if (a[i][j] != b[i][j])
				return false;
	return true;
}
int main()
{
	int n;
	fin >> n;
	for (int i = 0; i < n; i++)
	{
		fin >> src[i];
		now[i] = src[i];
	}
	for (int i = 0; i < n; i++)
		fin >> dest[i];
	rot(now, n);
	if (matequal(now, dest, n))
		fout << 1 << endl;
	else
	{
		rot(now, n);
		if (matequal(now, dest, n))
			fout << 2 << endl;
		else
		{
			rot(now, n);
			if (matequal(now, dest, n))
				fout << 3 << endl;
			else
			{
				rot(now, n);
				ref(now, n);
				if (matequal(now, dest, n))
					fout << 4 << endl;
				else
				{
					for (int i = 0; i < 3; i++)
					{
						rot(now, n);
						if (matequal(now, dest, n))
						{
							fout << 5 << endl;
							return 0;
						}
					}
					if (matequal(src, dest, n))
						fout << 6 << endl;
					else
						fout << 7 << endl;
				}
			}
		}
	}
	return 0;
}
/*
ID: zhangzh62
PROG: milk2
LANG: C++
*/
#include <fstream>
#include <cstring>
#include <algorithm>
using namespace std;
ifstream fin("milk2.in");
ofstream fout("milk2.out");
bool vis[1000005];
int main()
{
	int n;
	fin >> n;
	memset(vis, false, sizeof(vis));
	int lo = 1000000, hi = 0;
	for (int i = 1; i <= n; i++)
	{
		int l, r;
		fin >> l >> r;
		lo = min(lo, l);
		hi = max(hi, r);
		for (int j = l; j < r; j++)
			vis[j] = true;
	}
	int ans[2] = {0, 0}, pre = lo;
	bool cur = vis[pre];
	for (int i = lo + 1; i <= hi + 1; i++)
		if (vis[i] ^ cur)
		{
			ans[cur] = max(ans[cur], i - pre);
			pre = i;
			cur = !cur;
		}
	fout << ans[1] << ' ' << ans[0] << endl;
	return 0;
}
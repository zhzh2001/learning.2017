/*
ID: zhangzh62
PROG: namenum
LANG: C++
*/
#include <fstream>
#include <string>
using namespace std;
ifstream fin("namenum.in");
ifstream fdict("dict.txt");
ofstream fout("namenum.out");
const string dict = "ABCDEFGHIJKLMNOPRSTUVWXY";
int main()
{
	ios::sync_with_stdio(false);
	long long num;
	fin >> num;
	string s;
	bool found = false;
	while (fdict >> s)
	{
		long long now = 0;
		for (size_t i = 0; i < s.length(); i++)
			now = now * 10 + dict.find(s[i]) / 3 + 2;
		if (now == num)
		{
			fout << s << endl;
			found = true;
		}
	}
	if (!found)
		fout << "NONE\n";
	return 0;
}
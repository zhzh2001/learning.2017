/*
ID: zhangzh62
PROG: dualpal
LANG: C++
*/
#include <fstream>
#include <string>
using namespace std;
ifstream fin("dualpal.in");
ofstream fout("dualpal.out");
const string dig = "0123456789";
string dectob(int num, int b)
{
	string ans = "";
	do
		ans = dig[num % b] + ans;
	while (num /= b);
	return ans;
}
bool ispalind(const string &s)
{
	int i = 0, j = s.length() - 1;
	while (i < j)
		if (s[i++] != s[j--])
			return false;
	return true;
}
int main()
{
	int n, s;
	fin >> n >> s;
	for (int i = s + 1; n; i++)
	{
		int cnt = 0;
		for (int j = 2; j <= 10; j++)
		{
			if (ispalind(dectob(i, j)))
				cnt++;
			if (cnt == 2)
			{
				fout << i << endl;
				n--;
				break;
			}
		}
	}
	return 0;
}
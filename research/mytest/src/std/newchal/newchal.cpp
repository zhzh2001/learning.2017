#include<fstream>
#include<thread>
using namespace std;
ifstream fin("newchal.in");
ofstream fout("newchal.out");
unsigned long long foo_res;
void foo(int n,unsigned long long x)
{
	int tar=n/3*2;
	for(int i=1;i<=tar;i++)
		x^=((unsigned long long)i<<(i&63));
	for(int i=tar+1;i<=n;i++)
	{
		x^=((unsigned long long)i<<(i&63));
		unsigned long long res;
		asm
		(
			"popcntq %1,%0\n\t"
			:"=r"(res)
			:"r"(x)
		);
		foo_res^=x<<res;
	}
}
int main()
{
	int n;
	unsigned long long x;
	fin>>n>>x;
	unsigned long long h=n;
	thread th(foo,n,x);
	int tar=n/3*2;
	for(int i=1;i<=tar;i++)
	{
		x^=((unsigned long long)i<<(i&63));
		unsigned long long res;
		asm
		(
			"popcntq %1,%0\n\t"
			:"=r"(res)
			:"r"(x)
		);
		h^=x<<res;
	}
	th.join();
	fout<<(h^foo_res)<<endl;
	return 0;
}
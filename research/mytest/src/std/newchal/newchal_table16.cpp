#include<fstream>
using namespace std;
ifstream fin("newchal.in");
ofstream fout("newchal.out");
int table[65536];
int main()
{
	for(int i=0;i<65536;i++)
		for(int j=i;j;j&=j-1,table[i]++);
	int n;
	unsigned long long x;
	fin>>n>>x;
	unsigned long long h=n;
	for(int i=1;i<=n;i++)
	{
		x^=((unsigned long long)i<<(i&63));
		h^=x<<(table[x&0xffff]+table[(x>>16)&0xffff]+table[(x>>32)&0xffff]+table[x>>48]);
	}
	fout<<h<<endl;
	return 0;
}
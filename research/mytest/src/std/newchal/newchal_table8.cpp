#include<fstream>
using namespace std;
ifstream fin("newchal.in");
ofstream fout("newchal.out");
const int table[256]=
{
#define B2(n) n,n+1,n+1,n+2
#define B4(n) B2(n),B2(n+1),B2(n+1),B2(n+2)
#define B6(n) B4(n),B4(n+1),B4(n+1),B4(n+2)
    B6(0), B6(1), B6(1), B6(2)
};
int main()
{
	int n;
	unsigned long long x;
	fin>>n>>x;
	unsigned long long h=n;
	for(int i=1;i<=n;i++)
	{
		x^=((unsigned long long)i<<(i&63));
		h^=x<<(table[x&0xff]+table[(x>>8)&0xff]+table[(x>>16)&0xff]+table[(x>>24)&0xff]+table[(x>>32)&0xff]+table[(x>>40)&0xff]+table[(x>>48)&0xff]+table[x>>56]);
	}
	fout<<h<<endl;
	return 0;
}
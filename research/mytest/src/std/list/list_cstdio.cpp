#include<cstdio>
#include<deque>
#include<cstdlib>
#include<cstring>
#include<cctype>
using namespace std;
int main()
{
	FILE *fin=fopen("list.in","r");
	FILE *fout=fopen("list.out","w");
	int n,m;
	fscanf(fin,"%d%d",&n,&m);
	deque<int> *a=new deque<int> [n+1];
	while(m--)
	{
		int opt,i,j;
		fscanf(fin,"%d",&opt);
		switch(opt)
		{
			case 1:
				fscanf(fin,"%d%d",&i,&j);
				a[i].push_back(j);
				break;
			case 2:
				fscanf(fin,"%d",&i);
				a[i].pop_back();
				break;
			case 3:
				fscanf(fin,"%d%d",&i,&j);
				a[i].push_front(j);
				break;
			case 4:
				fscanf(fin,"%d",&i);
				a[i].pop_front();
				break;
			case 5:
				fscanf(fin,"%d%d",&i,&j);
				fprintf(fout,"%d\n",a[i][j-1]);
				break;
			case 6:
				fscanf(fin,"%d",&i);
				fprintf(fout,"%d\n",a[i].back());
				break;
		}
	}
	delete [] a;
	fclose(fin);fclose(fout);
	return 0;
}
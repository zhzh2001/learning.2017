#include<cstdio>
#include<deque>
#include<cctype>
using namespace std;
inline void read(int& x)
{
	char c;
	for(c=getchar();isspace(c);c=getchar());
	int sign=1;
	if(c=='-')
		sign=-1,c=getchar();
	x=0;
	for(;isdigit(c);c=getchar())
		x=x*10+c-'0';
	x*=sign;
}
int d[15];
inline void writeln(int x)
{
	if(x<0)
		putchar('-'),x=-x;
	int cnt=0;
	do
		d[++cnt]=x%10;
	while(x/=10);
	for(;cnt;cnt--)
		putchar(d[cnt]+'0');
	putchar('\n');
}
int main()
{
	freopen("list.in","r",stdin);
	freopen("list.out","w",stdout);
	int n,m;
	read(n);read(m);
	deque<int> *a=new deque<int> [n+1];
	while(m--)
	{
		int opt,i,j;
		read(opt);
		switch(opt)
		{
			case 1:
				read(i);read(j);
				a[i].push_back(j);
				break;
			case 2:
				read(i);
				a[i].pop_back();
				break;
			case 3:
				read(i);read(j);
				a[i].push_front(j);
				break;
			case 4:
				read(i);
				a[i].pop_front();
				break;
			case 5:
				read(i);read(j);
				writeln(a[i][j-1]);
				break;
			case 6:
				read(i);
				writeln(a[i].back());
				break;
		}
	}
	delete [] a;
	return 0;
}
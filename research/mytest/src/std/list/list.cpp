#include<cstdio>
#include<deque>
#include<cctype>
using namespace std;
FILE *fin,*fout;
char *bufin,*bufout,*pin,*pout;
inline void read(int& x)
{
	for(;isspace(*pin);pin++);
	int sign=1;
	if(*pin=='-')
		sign=-1,pin++;
	x=0;
	for(;isdigit(*pin);pin++)
		x=x*10+*pin-'0';
	x*=sign;
}
int d[15];
inline void writeln(int x)
{
	if(x<0)
		*pout++='-',x=-x;
	int cnt=0;
	do
		d[++cnt]=x%10;
	while(x/=10);
	for(;cnt;cnt--)
		*pout++=d[cnt]+'0';
	*pout++='\n';
}
int main()
{
	fin=fopen("list.in","r");
	fout=fopen("list.out","w");
	fseek(fin,0,SEEK_END);
	long fsize=ftell(fin);
	pin=bufin=new char [fsize+1];
	rewind(fin);
	fread(bufin,1,fsize,fin);
	bufin[fsize]='\0';
	int n,m;
	read(n);read(m);
	pout=bufout=new char [m*12];
	deque<int> *a=new deque<int> [n+1];
	int lastans=0;
	while(m--)
	{
		int opt,i,j;
		read(opt);
		switch(opt)
		{
			case 1:
				read(i);read(j);
				a[i^lastans].push_back(j^lastans);
				break;
			case 2:
				read(i);
				a[i^lastans].pop_back();
				break;
			case 3:
				read(i);read(j);
				a[i^lastans].push_front(j^lastans);
				break;
			case 4:
				read(i);
				a[i^lastans].pop_front();
				break;
			case 5:
				read(i);read(j);
				writeln(lastans=a[i^lastans][(j^lastans)-1]);
				break;
			case 6:
				read(i);
				writeln(lastans=a[i^lastans].back());
				break;
		}
	}
	delete [] a;
	fwrite(bufout,1,pout-bufout,fout);
	fclose(fin);fclose(fout);
	delete [] bufin;
	delete [] bufout;
	return 0;
}
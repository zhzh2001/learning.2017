program fiblen;
uses
  math;
var
  n:int64;
begin
  assign(input,'fiblen.in');
  reset(input);
  assign(output,'fiblen.out');
  rewrite(output);
  read(n);
  writeln(trunc(log10((1+sqrt(5))/2)*n-log10(sqrt(5)))+1);
  close(input);
  close(output);
end.
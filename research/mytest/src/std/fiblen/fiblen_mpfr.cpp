#include<bits/stdc++.h>
#include<mpreal.h>
using namespace std;
using namespace mpfr;
int main()
{
	string s;
	cin>>s;
	mpreal::set_default_prec(digits2bits(s.length()+10));
	mpreal n(s),sqrt5(sqrt(mpreal(5.0)));
	cout.precision(0);
	cout<<fixed<<floor(log10((sqrt5+1.0)/2.0)*n-log10(sqrt5))+1<<endl;
	return 0;
}
#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("fiblen.in");
ofstream fout("fiblen.out");
struct bigfloat
{
	static constexpr long double LIMIT=1e2400L;
	static const int DIGITS=2400;
	long double d;
	long long overcnt;
	bigfloat()
	{
		d=overcnt=0;
	}
	bigfloat(long double x,long long oc=0):d(x),overcnt(oc){};
	inline void adjust()
	{
		if(d>LIMIT)
		{
			overcnt++;
			d/=LIMIT;
		}
	}
	bigfloat operator+(const bigfloat b)const
	{
		if(overcnt>b.overcnt)
			return *this;
		if(overcnt<b.overcnt)
			return b;
		bigfloat res(d+b.d,overcnt);
		res.adjust();
		return res;
	}
	bigfloat operator+=(const bigfloat b)
	{
		*this=*this+b;
		return *this;
	}
	bigfloat operator*(const bigfloat b)const
	{
		bigfloat res(d*b.d,overcnt+b.overcnt);
		res.adjust();
		return res;
	}
};
ostream& operator<<(ostream& os,const bigfloat b)
{
	os<<(long long)floor(log10(b.d))+1+b.overcnt*bigfloat::DIGITS;
	return os;
}
struct matrix
{
	static const int n=2;
	bigfloat mat[n][n];
	matrix()
	{
		mat[0][0]=mat[0][1]=mat[1][0]=mat[1][1]=0;
	}
	matrix(long double a,long double b,long double c,long double d)
	{
		mat[0][0]=a;
		mat[0][1]=b;
		mat[1][0]=c;
		mat[1][1]=d;
	}
	matrix operator*(const matrix& b)const
	{
		matrix res;
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				for(int k=0;k<n;k++)
					res.mat[i][j]+=mat[i][k]*b.mat[k][j];
		return res;
	}
	matrix operator*=(const matrix& b)
	{
		*this=*this*b;
		return *this;
	}
	matrix pow(long long b)const
	{
		matrix res(1,0,0,1),a=*this;
		do
		{
			if(b&1)
				res*=a;
			a*=a;
		}
		while(b>>=1);
		return res;
	}
};
int main()
{
	long long n;
	fin>>n;
	fout<<matrix(1,1,1,0).pow(n).mat[0][1]<<endl;
	return 0;
}
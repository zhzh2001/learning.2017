#include<iostream>
#include<cmath>
#include<random>
#include<ctime>
using namespace std;
struct bigfloat
{
	static constexpr double LIMIT=1e150;
	static const int DIGITS=150;
	double d;
	long long overcnt;
	bigfloat()
	{
		d=overcnt=0;
	}
	bigfloat(double x,long long oc=0):d(x),overcnt(oc){};
	inline void adjust()
	{
		if(d>LIMIT)
		{
			overcnt++;
			d/=LIMIT;
		}
	}
	bigfloat operator+(const bigfloat& b)const
	{
		if(overcnt>b.overcnt)
			return *this;
		if(overcnt<b.overcnt)
			return b;
		bigfloat res(d+b.d,overcnt);
		res.adjust();
		return res;
	}
	bigfloat operator+=(const bigfloat& b)
	{
		return *this=*this+b;
	}
	bigfloat operator*(const bigfloat& b)const
	{
		bigfloat res(d*b.d,overcnt+b.overcnt);
		res.adjust();
		return res;
	}
};
struct matrix
{
	static const int n=2;
	bigfloat mat[n][n];
	matrix()
	{
		mat[0][0]=mat[0][1]=mat[1][0]=mat[1][1]=0;
	}
	matrix(bigfloat a,bigfloat b,bigfloat c,bigfloat d)
	{
		mat[0][0]=a;
		mat[0][1]=b;
		mat[1][0]=c;
		mat[1][1]=d;
	}
	matrix operator*(const matrix& b)const
	{
		return matrix(mat[0][0]*b.mat[0][0]+mat[0][1]*b.mat[1][0],mat[0][0]*b.mat[0][1]+mat[0][1]*b.mat[1][1],mat[1][0]*b.mat[0][0]+mat[1][1]*b.mat[1][0],mat[1][0]*b.mat[0][1]+mat[1][1]*b.mat[1][1]);
	}
	matrix operator*=(const matrix& b)
	{
		return *this=*this*b;
	}
	matrix pow(long long b)const
	{
		matrix res(1,0,0,1),a=*this;
		do
		{
			if(b&1)
				res*=a;
			a*=a;
		}
		while(b>>=1);
		return res;
	}
};
int main()
{
	default_random_engine gen(time(NULL));
	long long maxn;
	cin>>maxn;
	long long ans=0;
	for(;;)
	{
		uniform_int_distribution<long long> dn(ans,maxn);
		long long n=dn(gen);
		bigfloat b=matrix(1,1,1,0).pow(n).mat[0][1];
		long long len1=(long long)floor(log10(b.d))+1+b.overcnt*bigfloat::DIGITS,len2=floor(log10((1.0L+sqrt(5.0L))/2.0L)*n-log10(sqrt(5.0L)))+1;
		if(len1!=len2&&n>ans)
		{
			cout<<n<<" std="<<len1<<" math="<<len2<<endl;
			ans=n;
		}
	}
	return 0;
}
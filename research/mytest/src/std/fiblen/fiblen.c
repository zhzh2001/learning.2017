#define __USE_MINGW_ANSI_STDIO 1
#include<stdio.h>
#include<math.h>
int main()
{
	FILE *fin=fopen("fiblen.in","r");
	FILE *fout=fopen("fiblen.out","w");
	long long n;
	fscanf(fin,"%lld",&n);
	fprintf(fout,"%.0Lf\n",floorl(log10l((1.0L+sqrtl(5.0L))/2.0L)*n-log10l(sqrtl(5.0L)))+1.0L);
	fclose(fin);
	fclose(fout);
	return 0;
}
#include<stdio.h>
#include<quadmath.h>
int main()
{
	__float128 n;
	char buf[50];
	scanf("%s",buf);
	n=strtoflt128(buf,NULL);
	quadmath_snprintf(buf,sizeof(buf),"%.0Qf",floorq(log10q((sqrtq(5.0Q)+1.0Q)/2.0Q)*n-log10q(sqrtq(5.0Q)))+1.0Q);
	printf("%s\n",buf);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	//fwrite based
	const int SIZE=1000000;
	typedef long long ll;
	char buf[SIZE],*p1=buf+SIZE,*pend=buf+SIZE;
	//replace getchar
	inline char nc()
	{
		if (p1==pend)
		{
			p1=buf;
			pend=buf+fread(buf,1,SIZE,fin);
			if (pend==p1)
				return EOF;
		}
		return *p1++;
	}
	
	inline void read(int& x)
	{
		bool neg=false; char ch=nc(); x=0;
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		if (ch=='-')neg=true,ch=nc();
		for (;isdigit(ch);ch=nc())x=x*10+ch-'0';
		if (neg)x=-x;
	}
	inline void read(ll& x)
	{
		bool neg=false; char ch=nc(); x=0;
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		if (ch=='-')neg=true,ch=nc();
		for (;isdigit(ch);ch=nc())x=x*10+ch-'0';
		if (neg)x=-x;
	}
	
	inline void read(double& x)
	{
		bool neg=false; char ch=nc(); x=0;
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		if (ch=='-')neg=true,ch=nc();
		for (;isdigit(ch);ch=nc())x=x*10+ch-'0';
		if (ch=='.')
		{
			double tmp=1; ch=nc();
			for (;isdigit(ch);ch=nc())tmp/=10,x+=tmp*(ch-'0');
		}
		if (neg)x=-x;
	}
	
	inline void read(char* s)
	{
		char ch=nc();
		for (;isspace(ch);ch=nc());
		if (ch==EOF)return;
		for (;!isspace(ch)&&ch!=EOF;ch=nc())*s++=ch;
		*s='\0';
	}
	inline void read(char& c)
	{
		for (c=nc();isspace(c);c=nc());
	}
	
	inline void readln()
	{
		for(char c=nc();c!='\n'&&c!=EOF;c=nc());
	}
	template<typename T>
	inline void readln(T& x)
	{
		read(x);
		readln();
	}
};
	
namespace fastO
{
	//fwrite based
	const int SIZE=1000000;
	typedef long long ll;
	char buf[SIZE],*p1=buf,*pend=buf+SIZE;
	inline void out(char ch)
	{
		if (p1==pend)
		{
			fwrite(buf,1,SIZE,fout);
			p1=buf;
		}
		*p1++=ch;
	}
	inline void write(int x)
	{
		static char s[15],*s1;s1=s;
		if (!x)
			out('0');
		if (x<0)
			out('-'),x=-x;
		while(x)
			*s1++=x%10+'0',x/=10;
		while(s1--!=s)
			out(*s1);
	}
	inline void write(ll x)
	{
		static char s[25],*s1;s1=s;
		if (!x)
			out('0');
		if (x<0)
			out('-'),x=-x;
		while(x)
			*s1++=x%10+'0',x/=10;
		while(s1--!=s)
			out(*s1);
	}
	inline void write(double x,int y=6)
	{
		static ll mul[]={1,10,100,1000,10000,100000,1000000,10000000,100000000,
		1000000000,10000000000LL,100000000000LL,1000000000000LL,10000000000000LL,
		100000000000000LL,1000000000000000LL,10000000000000000LL,100000000000000000LL};
		if (x<-1e-12)
			out('-'),x=-x;x*=mul[y];
		ll x1=floor(x);
		if (x-x1>=0.5)
			++x1;
		ll x2=x1/mul[y],x3=x1-x2*mul[y];
		write(x2);
		if (y)
		{
			out('.');
			for (int i=1;i<y&&x3*mul[i]<mul[y];out('0'),i++); 
			write(x3);
		}
	}
	inline void write(char* s)
	{
		while (*s)
			out(*s++);
	}
	inline void write(char c)
	{
		out(c);
	}
	inline void flush()
	{
		if (p1!=buf)
		{
			fwrite(buf,1,p1-buf,fout);
			p1=buf;
		}
	}
	
	struct AutoObject
	{
		AutoObject(){}
		~AutoObject()
		{
			flush();
		}
	}AO;
	
	inline void writeln()
	{
		#ifdef _WIN32
		out('\r');
		#endif
		out('\n');
		//flush();
	}
	
	template<typename T>
	inline void writeln(T x)
	{
		write(x);
		writeln();
	}
	inline void writeln(double x,int y)
	{
		write(x,y);
		writeln();
	}
};

namespace fastIO
{
	using namespace fastI;
	using namespace fastO;
};

using namespace fastIO;
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	int n;
	read(n);
	int* a=new int [n];
	for(int i=0;i<n;i++)
		read(a[i]);
	sort(a,a+n);
	for(int i=0;i<n;i++)
	{
		write(a[i]);
		write(' ');
	}
	writeln();
	printf("time=%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
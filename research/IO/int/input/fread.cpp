#include<cstdio>
#include<cctype>
#include<ctime>
char buf[110000005],*now;
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int main()
{
	freopen("IO.int","r",stdin);
	freopen("fread.txt","w",stdout);
	time_t start=clock();
	fread(buf,1,110000000,stdin);
	now=buf-1;
	int n=getint();
	long long sum=0;
	while(n--)
	{
		int x=getint();
		sum+=x;
	}
	printf("%I64d\n%.3lf\n",sum,(double)(clock()-start)/CLOCKS_PER_SEC);
	return 0;
}
#include<cstdio>
#include<cctype>
#include<ctime>
#include<cstring>
char buf[1000025],*now,*bufend;
inline int getint()
{
	if(bufend-now<20)
	{
		memcpy(buf,now,bufend-now);
		fread(buf+(bufend-now),1,1000000,stdin);
		bufend=buf+(bufend-now)+1000000;
		now=buf-1;
	}
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int main()
{
	freopen("IO.int","r",stdin);
	freopen("fread_block.txt","w",stdout);
	time_t start=clock();
	now=buf+1000000;
	bufend=buf+1000000;
	int n=getint();
	long long sum=0;
	while(n--)
	{
		int x=getint();
		sum+=x;
	}
	printf("%I64d\n%.3lf\n",sum,(double)(clock()-start)/CLOCKS_PER_SEC);
	return 0;
}
#include<cstdio>
#include<ctime>
int main()
{
	freopen("IO.int","r",stdin);
	freopen("freopen+scanf.txt","w",stdout);
	time_t start=clock();
	int n;
	scanf("%d",&n);
	long long sum=0;
	while(n--)
	{
		int x;
		scanf("%d",&x);
		sum+=x;
	}
	printf("%I64d\n%.3lf\n",sum,(double)(clock()-start)/CLOCKS_PER_SEC);
	return 0;
}
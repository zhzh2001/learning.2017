#include<iostream>
#include<cstdio>
#include<ctime>
using namespace std;
int main()
{
	freopen("IO.int","r",stdin);
	freopen("freopen+cin.txt","w",stdout);
	time_t start=clock();
	int n;
	cin>>n;
	long long sum=0;
	while(n--)
	{
		int x;
		cin>>x;
		sum+=x;
	}
	cout<<sum<<endl<<(double)(clock()-start)/CLOCKS_PER_SEC<<endl;
	return 0;
}
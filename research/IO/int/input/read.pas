program _read;
var
  start,n,i,x:longint;
  sum:int64;
begin
  assign(input,'IO.int');
  reset(input);
  assign(output,'read.txt');
  rewrite(output);
  randomize;
  start:=randseed;
  read(n);
  sum:=0;
  for i:=1 to n do
  begin
    read(x);
	inc(sum,x);
  end;
  randomize;
  writeln(sum);
  writeln(randseed-start);
  close(input);
  close(output);
end.
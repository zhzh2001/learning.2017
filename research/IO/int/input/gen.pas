program gen;
var
  n,i:longint;
begin
  randomize;
  assign(output,'IO.int');
  rewrite(output);
  read(n);
  writeln(n);
  for i:=1 to n do
    write(random(maxlongint),' ');
  close(output);
end.
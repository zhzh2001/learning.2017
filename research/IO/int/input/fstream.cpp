#include<fstream>
#include<ctime>
using namespace std;
int main()
{
	ifstream fin("IO.int");
	ofstream fout("fstream.txt");
	time_t start=clock();
	int n;
	fin>>n;
	long long sum=0;
	while(n--)
	{
		int x;
		fin>>x;
		sum+=x;
	}
	fout<<sum<<endl<<(double)(clock()-start)/CLOCKS_PER_SEC<<endl;
	return 0;
}
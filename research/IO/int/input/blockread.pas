program _blockread;
{$inline on}
var
  fin:file;
  buf:array[1..110000000]of byte;
  now:^byte;
  start,cnt,n,i:longint;
  sum:int64;
function getint:longint;inline;
var
  x:longint;
begin
  x:=0;
  while (now^<48)or(now^>57) do
    inc(now);
  while (now^>=48)and(now^<=57) do
  begin
    x:=x*10+now^-48;
	inc(now);
  end;
  getint:=x;
end;
begin
  assign(fin,'IO.int');
  reset(fin,1);
  assign(output,'blockread.txt');
  rewrite(output);
  randomize;
  start:=randseed;
  blockread(fin,buf,110000000,cnt);
  now:=@buf;
  n:=getint;
  sum:=0;
  for i:=1 to n do
    inc(sum,getint);
  writeln(sum);
  randomize;
  writeln(randseed-start);
  close(fin);
  close(output);
end.
#include<cstdio>
#include<cctype>
#include<ctime>
inline int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int main()
{
	freopen("IO.int","r",stdin);
	freopen("getint.txt","w",stdout);
	time_t start=clock();
	int n=getint();
	long long sum=0;
	while(n--)
	{
		int x=getint();
		sum+=x;
	}
	printf("%I64d\n%.3lf\n",sum,(double)(clock()-start)/CLOCKS_PER_SEC);
	return 0;
}
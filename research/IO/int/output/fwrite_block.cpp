#include<cstdio>
#include<ctime>
using namespace std;
char buf[1000020],*now;
int tmp[15];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++now)=tmp[len]+'0';
	if(now-buf>1000000)
	{
		fwrite(buf,1,now-buf+1,stdout);
		now=buf-1;
	}
}
int main()
{
	freopen("IO.int","r",stdin);
	freopen("IO.out","w",stdout);
	int a,b,p,n;
	scanf("%d%d%d%d",&a,&b,&p,&n);
	time_t start=clock();
	int x=0;
	now=buf-1;
	for(int i=1;i<=n;i++)
	{
		x=(a*x+b)%p;
		putint(x);
		*(++now)=' ';
	}
	*(++now)='\n';
	fwrite(buf,1,now-buf+1,stdout);
	freopen("fwrite_block.txt","w",stdout);
	printf("%.3lf\n",(double)(clock()-start)/CLOCKS_PER_SEC);
	return 0;
}
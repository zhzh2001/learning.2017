program _write;
var
  a,b,p,n,i,start,x:longint;
begin
  assign(input,'IO.int');
  reset(input);
  assign(output,'IO.out');
  rewrite(output);
  read(a,b,p,n);
  randomize;
  start:=randseed;
  x:=0;
  for i:=1 to n do
  begin
    x:=(a*x+b)mod p;
	write(x,' ');
  end;
  writeln;
  close(output);
  assign(output,'write.txt');
  rewrite(output);
  randomize;
  writeln(randseed-start);
  close(input);
  close(output);
end.
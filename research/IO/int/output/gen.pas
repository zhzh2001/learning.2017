program gen2;
const
  max=32768;
  n=10000000;
begin
  randomize;
  assign(output,'IO.int');
  rewrite(output);
  writeln(random(max)+1,' ',random(max)+1,' ',random(max)+1,' ',n);
  close(output);
end.
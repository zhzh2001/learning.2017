#include<cstdio>
#include<ctime>
using namespace std;
int buf[15];
inline void putint(int x)
{
	int p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(buf[p]+'0');
}
int main()
{
	freopen("IO.int","r",stdin);
	freopen("IO.out","w",stdout);
	int a,b,p,n;
	scanf("%d%d%d%d",&a,&b,&p,&n);
	time_t start=clock();
	int x=0;
	for(int i=1;i<=n;i++)
	{
		x=(a*x+b)%p;
		putint(x);
		putchar(' ');
	}
	puts("");
	freopen("putint.txt","w",stdout);
	printf("%.3lf\n",(double)(clock()-start)/CLOCKS_PER_SEC);
	return 0;
}
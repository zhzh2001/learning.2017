program _blockwrite;
{$inline on}
var
  fout:file;
  buf:array[1..53000000]of byte;
  now:^byte;
  a,b,p,n,i,x,start,cnt:longint;
  tmp:array[1..10]of longint;
procedure putint(x:longint);inline;
var
  p:longint;
begin
  p:=0;
  repeat
    inc(p);
	tmp[p]:=x mod 10;
	x:=x div 10;
  until x=0;
  while p>0 do
  begin
    now^:=tmp[p]+ord('0');
	inc(now);
	dec(p);
  end;
end;
begin
  assign(input,'IO.int');
  reset(input);
  assign(fout,'IO.out');
  rewrite(fout,1);
  read(a,b,p,n);
  randomize;
  start:=randseed;
  x:=0;
  now:=@buf;
  for i:=1 to n do
  begin
    x:=(a*x+b)mod p;
	putint(x);
	now^:=ord(' ');
	inc(now);
  end;
  now^:=10;
  inc(now);
  blockwrite(fout,buf,now-@buf,cnt);
  assign(output,'blockwrite.txt');
  rewrite(output);
  randomize;
  writeln(randseed-start);
  close(input);
  close(fout);
  close(output);
end.
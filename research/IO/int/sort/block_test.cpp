#include<cstdio>
#include<cctype>
#include<ctime>
#include<cstring>
const int MAXN=10000005;
FILE *fin,*fout;
char buf[1011000],*now,*bufend;
int a[MAXN];
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int tmp[15];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++now)=tmp[len]+'0';
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	fread(buf,1,1000000,fin);
	now=buf-1;
	bufend=buf+1000000;
	int n=getint();
	for(int i=1;i<n;i+=1000)
	{
		for(int j=i;j<i+1000;j++)
			a[j]=getint();
		if(bufend-now<11000)
		{
			memcpy(buf,now,bufend-now);
			fread(buf+(bufend-now),1,1000000,fin);
			bufend=buf+(bufend-now)+1000000;
			now=buf-1;
		}
	}
	now=buf-1;
	for(int i=1;i<n;i+=1000)
	{
		for(int j=i;j<i+1000;j++)
		{
			putint(a[i]);
			*(++now)=' ';
		}
		if(now-buf>1000000)
		{
			fwrite(buf,1,now-buf+1,fout);
			now=buf-1;
		}
	}
	*(++now)='\n';
	fwrite(buf,1,now-buf+1,fout);
	printf("%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
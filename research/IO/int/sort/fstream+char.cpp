#include<fstream>
#include<iostream>
#include<algorithm>
#include<cctype>
#include<ctime>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
inline void read(int& x)
{
	char c;
	for(fin.get(c);isspace(c);fin.get(c));
	bool neg=false;
	if(c=='-')
	{
		neg=true;
		fin.get(c);
	}
	if(!fin)
		return;
	x=0;
	for(;isdigit(c);fin.get(c))
		x=x*10+c-'0';
	if(neg)
		x=-x;
}
inline void write(int x)
{
	if(x<0)
	{
		fout.put('-');
		x=-x;
	}
	static int buf[15];
	int p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		fout.put(buf[p]+'0');
}
int main()
{
	int n;
	read(n);
	int *a=new int [n];
	for(int i=0;i<n;i++)
		read(a[i]);
	sort(a,a+n);
	for(int i=0;i<n;i++)
	{
		write(a[i]);
		fout.put(' ');
	}
	fout<<endl;
	cout<<clock()<<endl;
	return 0;
}
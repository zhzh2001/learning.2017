#include<cstdio>
#include<algorithm>
#include<ctime>
#include<cctype>
using namespace std;
const int BUFSIZE=1000000,MAXN=10000005;
FILE *fin,*fout;
char buf[BUFSIZE],*now;
int a[MAXN];
inline char mygetc()
{
	if(now-buf==BUFSIZE)
	{
		fread(buf,1,BUFSIZE,fin);
		now=buf;
	}
	return *(now++);
}
inline void myputc(char c)
{
	*(now++)=c;
	if(now-buf==BUFSIZE)
	{
		fwrite(buf,1,BUFSIZE,fout);
		now=buf;
	}
}
inline int getint()
{
	char c;
	while(!isdigit(c=mygetc()));
	int ret=c-'0';
	while(isdigit(c=mygetc()))
		ret=ret*10+c-'0';
	return ret;
}
int tmp[15];
inline void putint(int x)
{
	int p=0;
	do
		tmp[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		myputc(tmp[p]+'0');
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	now=buf+BUFSIZE;
	int n=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	sort(a+1,a+n+1);
	now=buf;
	for(int i=1;i<=n;i++)
	{
		putint(a[i]);
		myputc(' ');
	}
	myputc('\n');
	fwrite(buf,1,now-buf,fout);
	printf("%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
#include<fstream>
#include<iostream>
#include<algorithm>
#include<ctime>
using namespace std;
const int MAXN=10000005;
ifstream fin("sort.in");
ofstream fout("sort.out");
int a[MAXN];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
		fin>>a[i];
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		fout<<a[i]<<' ';
	fout<<endl;
	cout<<(double)clock()/CLOCKS_PER_SEC<<endl;
	return 0;
}
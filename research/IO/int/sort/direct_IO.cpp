#include<cstdio>
#include<algorithm>
#include<ctime>
#include<cctype>
using namespace std;
const int MAXN=10000005;
int a[MAXN];
inline int getint()
{
	char c;
	while(!isdigit(c=getchar()));
	int ret=c-'0';
	while(isdigit(c=getchar()))
		ret=ret*10+c-'0';
	return ret;
}
int tmp[15];
inline void putint(int x)
{
	int p=0;
	do
		tmp[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putchar(tmp[p]+'0');
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	int n=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
	{
		putint(a[i]);
		putchar(' ');
	}
	puts("");
	freopen("con","w",stdout);
	printf("%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
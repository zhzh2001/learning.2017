#include<cstdio>
#include<algorithm>
#include<ctime>
using namespace std;
const int MAXN=10000005;
int a[MAXN];
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	int n;
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
		scanf("%d",a+i);
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		printf("%d ",a[i]);
	puts("");
	freopen("con","w",stdout);
	printf("%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
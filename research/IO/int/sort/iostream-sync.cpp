#include<cstdio>
#include<iostream>
#include<algorithm>
#include<ctime>
using namespace std;
const int MAXN=10000005;
int a[MAXN];
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	ios::sync_with_stdio(false);
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
		cin>>a[i];
	sort(a+1,a+n+1);
	for(int i=1;i<=n;i++)
		cout<<a[i]<<' ';
	cout<<endl;
	freopen("con","w",stdout);
	cout<<(double)clock()/CLOCKS_PER_SEC<<endl;
	return 0;
}
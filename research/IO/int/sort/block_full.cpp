#include<cstdio>
#include<cctype>
#include<ctime>
#include<cstring>
#include<algorithm>
using namespace std;
const int MAXN=10000005,FS=105000000;
FILE *fin,*fout;
char buf[FS],*now,*bufend;
int a[MAXN];
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int tmp[10];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++now)=tmp[len]+'0';
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	fread(buf,1,FS,fin);
	now=buf-1;
	int n=getint();
	for(int i=1;i<=n;i++)
		a[i]=getint();
	sort(a+1,a+n+1);
	now=buf-1;
	for(int i=1;i<=n;i++)
	{
		putint(a[i]);
		*(++now)=' ';
	}
	fwrite(buf,1,now-buf+1,fout);
	printf("%.3lf\n",(double)clock()/CLOCKS_PER_SEC);
	return 0;
}
#include <cstdio>
int main()
{
	freopen("integer.in", "r", stdin);
	freopen("integer.out", "w", stdout);
	int n;
	scanf("%d", &n);
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		int x;
		scanf("%d", &x);
		sum += x;
	}
	printf("%lld\n", sum);
	return 0;
}
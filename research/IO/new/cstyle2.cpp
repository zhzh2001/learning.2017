#include <cstdio>
#include <cstdlib>
int main()
{
	freopen("integer.in", "r", stdin);
	freopen("integer.out", "w", stdout);
	int n;
	scanf("%d", &n);
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		char buf[20];
		scanf("%s", buf);
		sum += atoi(buf);
	}
	printf("%lld\n", sum);
	return 0;
}
#include <fstream>
#include <ctime>
#include <random>
using namespace std;
ofstream fout("integer.in");
const int n = 1e8;
int main()
{
	minstd_rand gen(time(NULL));
	fout << n << endl;
	for (int i = 1; i <= n; i++)
	{
		uniform_int_distribution<> d(numeric_limits<int>::min(), numeric_limits<int>::max());
		fout << d(gen) << ' ';
	}
	fout << endl;
	return 0;
}

Program IOtest;

Var 
  n,i,x: longint;
  sum: int64;
Begin
  assign(input,'integer.in');
  reset(input);
  assign(output,'integer.out');
  rewrite(output);
  read(n);
  sum := 0;
  For i:=1 To n Do
    Begin
      read(x);
      inc(sum,x);
    End;
  writeln(sum);
  close(input);
  close(output);
End.

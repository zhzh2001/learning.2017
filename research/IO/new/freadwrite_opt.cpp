#include <cstdio>
#include <cctype>
#include <cstring>
FILE *fin = fopen("integer.in", "r"), *fout = fopen("integer.out", "w");
namespace fastI
{
const int SIZE = 1e6;
char buf[SIZE], *end = buf + SIZE, *p = end;
template <typename Int>
inline bool read(Int &x)
{
	int remain = end - p;
	if (remain < 20)
	{
		memcpy(buf, p, remain);
		end = buf + remain + fread(buf + remain, 1, SIZE - remain, fin);
		p = buf;
	}
	while (p < end && isspace(*p++))
		;
	if (p == end)
		return false;
	bool neg = false;
	if (*--p == '-')
		neg = true, p++;
	x = 0;
	for (; p < end && isdigit(*p); p++)
		x = x * 10 + *p - '0';
	if (neg)
		x = -x;
	return true;
}
};
namespace fastO
{
const int SIZE = 1e6;
char buf[SIZE], *end = buf + SIZE, *p = buf;
int d[25];
template <typename Int>
inline void writeln(Int x)
{
	if (end - p < 20)
	{
		fwrite(buf, 1, p - buf, fout);
		p = buf;
	}
	if (x < 0)
	{
		*p++ = '-';
		x = -x;
	}
	int k = 0;
	do
		d[++k] = x % 10;
	while (x /= 10);
	for (; k; k--)
		*p++ = d[k] + '0';
	*p++ = '\n';
}
inline void flush()
{
	fwrite(buf, 1, p - buf, fout);
}
};
int main()
{
	int n;
	fastI::read(n);
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		int x;
		fastI::read(x);
		sum += x;
	}
	fprintf(fout, "%lld\n", sum);
	return 0;
}
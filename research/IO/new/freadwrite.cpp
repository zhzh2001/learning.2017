#include <cstdio>
#include <cctype>
FILE *fin = fopen("integer.in", "r"), *fout = fopen("integer.out", "w");
const int SZ = 1e6;
char ibuf[SZ], *ip = ibuf, *ipend = ibuf, obuf[SZ], *op = obuf, *opend = obuf + SZ;
inline int nextchar()
{
	if (ip == ipend)
	{
		ipend = (ip = ibuf) + fread(ibuf, 1, SZ, fin);
		if (ipend == ibuf)
			return EOF;
	}
	return *ip++;
}
template <typename Int>
inline void read(Int &x)
{
	char c = nextchar();
	for (; isspace(c); c = nextchar())
		;
	x = 0;
	Int sign = 1;
	if (c == '-')
	{
		sign = -1;
		c = nextchar();
	}
	for (; isdigit(c); c = nextchar())
		x = x * 10 + c - '0';
	x *= sign;
}
inline void writechar(char c)
{
	if (op == opend)
	{
		fwrite(obuf, 1, SZ, fout);
		op = obuf;
	}
	*op++ = c;
}
inline void flush()
{
	fwrite(obuf, 1, op - obuf, fout);
}
int dig[20];
template <typename Int>
inline void writeln(Int x)
{
	if (x < 0)
	{
		writechar('-');
		x = -x;
	}
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		writechar(dig[len] + '0');
	writechar('\n');
}
int main()
{
	int n;
	read(n);
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		int x;
		read(x);
		sum += x;
	}
	writeln(sum);
	flush();
	return 0;
}
#include <fstream>
using namespace std;
ifstream fin("integer.in");
ofstream fout("integer.out");
int main()
{
	int n;
	fin >> n;
	long long sum = 0;
	for (int i = 1; i <= n; i++)
	{
		int x;
		fin >> x;
		sum += x;
	}
	fout << sum << endl;
	return 0;
}
#include <cstdio>
inline int read()
{
	int k = 0, f = 1;
	char ch = getchar();
	while (ch < '0' || ch > '9')
	{
		if (ch == '-')
			f = -1;
		ch = getchar();
	}
	while (ch >= '0' && ch <= '9')
	{
		k = k * 10 + ch - '0';
		ch = getchar();
	}
	return k * f;
}
inline void write(int x)
{
	if (x < 0)
		putchar('-'), x = -x;
	if (x >= 10)
		write(x / 10);
	putchar(x % 10 + '0');
}
inline void write_norecu(int x)
{
	static int dig[20];
	int len = 0;
	do
		dig[++len] = x % 10;
	while (x /= 10);
	for (; len; len--)
		putchar(dig[len] + '0');
}
inline void writeln(int x)
{
	write(x);
	puts("");
}
int main()
{
	freopen("integer.in", "r", stdin);
	freopen("integer.out", "w", stdout);
	int n = read();
	for (int i = 1; i <= n; i++)
	{
		write_norecu(read());
		putchar(' ');
	}
	return 0;
}
#include<cstdio>
#include<algorithm>
#include<cctype>
#include<ctime>
using namespace std;
FILE *fin,*fout;
inline void read(int& x)
{
	int c;
	for(c=getc(fin);isspace(c);c=getc(fin));
	bool neg=false;
	if(c=='-')
	{
		neg=true;
		c=getc(fin);
	}
	if(c==EOF)
		return;
	x=0;
	for(;isdigit(c);c=getc(fin))
		x=x*10+c-'0';
	if(neg)
		x=-x;
}
inline void write(int x)
{
	if(x<0)
	{
		putc('-',fout);
		x=-x;
	}
	static int buf[15];
	int p=0;
	do
		buf[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		putc(buf[p]+'0',fout);
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	int n;
	read(n);
	int *a=new int [n];
	for(int i=0;i<n;i++)
		read(a[i]);
	sort(a,a+n);
	for(int i=0;i<n;i++)
	{
		write(a[i]);
		putc(' ',fout);
	}
	putc('\n',fout);
	printf("%d\n",clock());
	fclose(fin);
	fclose(fout);
	return 0;
}
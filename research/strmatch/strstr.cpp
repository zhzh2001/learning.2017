#include<bits/stdc++.h>
using namespace std;
const int N=1000020,M=100020;
char s[N],p[M];
int main()
{
	freopen("str.in","r",stdin);
	scanf("%s%s",s,p);
	printf("%d\n",clock());
	time_t t=clock();
	char *ans=strstr(s,p);
	if(ans==NULL)
		puts("-1");
	else
		printf("%d\n",ans-s+1);
	printf("%d\n",clock()-t);
	return 0;
}
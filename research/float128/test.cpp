#include <bits/stdc++.h>
#include <quadmath.h>
using namespace std;
int main()
{
	int n;
	cin >> n;
	__float128 ans = 1;
	for (int i = 1; i <= n; i++)
		ans *= i;
	cout << ans << endl;
	/*
	char buf[1000];
	quadmath_snprintf(buf,sizeof(buf),"%Qg",ans);
	cout<<buf<<endl;
	*/
	return 0;
}
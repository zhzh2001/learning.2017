program Int128;
uses
  sysutils;
var
  n,i:longint;
  ans:Int128Rec;
begin
  read(n);
  ans:=1;
  for i:=1 to n do
    ans:=ans*i;
  writeln(ans);
end.
#include<bits/stdc++.h>
using namespace std;
istream& operator>>(istream& is,__int128& x)
{
	char c;
	for(c=is.get();isspace(c);c=is.get());
	bool neg=false;
	if(c=='-')
	{
		neg=true;
		c=is.get();
	}
	x=0;
	for(;isdigit(c);c=is.get())
		x=x*10+c-'0';
	if(neg)
		x=-x;
	return is;
}
int tmp[40];
ostream& operator<<(ostream& os,__int128 x)
{
	int p=0;
	do
		tmp[++p]=x%10;
	while(x/=10);
	for(;p;p--)
		os.put(tmp[p]+'0');
	return os;
}
int main()
{
	__int128 a,b;
	cin>>a>>b;
	cout<<a+b<<endl;
	return 0;
}
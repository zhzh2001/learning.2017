#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<time.h>
#define inline __attribute__((always_inline))
FILE *fin,*fout;
#define SIZE 1000000
char ibuf[SIZE],*iend=ibuf+SIZE,*ip=ibuf+SIZE;
static inline int myread(int* x)
{
	int remain=iend-ip;
	if(remain<20)
	{
		memcpy(ibuf,ip,remain);
		iend=ibuf+remain+fread(ibuf+remain,1,SIZE-remain,fin);
		ip=ibuf;
	}
	while(ip<iend&&isspace(*ip++));
	if(ip==iend)
		return 0;
	int neg=0;
	if(*--ip=='-')
		neg=1,ip++;
	*x=0;
	for(;ip<iend&&isdigit(*ip);ip++)
		*x=*x*10+*ip-'0';
	if(neg)
		*x=-*x;
	return 1;
}
char obuf[SIZE],*oend=obuf+SIZE,*op=obuf;
int d[15];
static inline void mywrite(int x)
{
	if(oend-op<20)
	{
		fwrite(obuf,1,op-obuf,fout);
		op=obuf;
	}
	int k=0;
	do
		d[++k]=x%10;
	while(x/=10);
	for(;k;k--)
		*op++=d[k]+'0';
}
static inline int cmp(const void* a,const void* b)
{
	return *(int*)a-*(int*)b;
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	time_t t=clock();
	int n;
	myread(&n);
	int *a=malloc(n*sizeof(int));
	for(int i=0;i<n;i++)
		myread(a+i);
	printf("input:%d\n",clock()-t);
	t=clock();
	qsort(a,n,sizeof(int),cmp);
	printf("using qsort:%d\n",clock()-t);
	t=clock();
	for(int i=0;i<n;i++)
	{
		mywrite(a[i]);
		*op++=' ';
	}
	fwrite(obuf,1,op-obuf,fout);
	printf("output:%d\n",clock()-t);
	printf("total:%d\n",clock());
	free(a);
	fclose(fin);
	fclose(fout);
	return 0;
}
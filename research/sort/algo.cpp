#include<bits/stdc++.h>
//#define inline __attribute__((always_inline))
using namespace std;
FILE *fin,*fout;
namespace fastI
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=end;
	void read(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(isspace(*p++));
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
	}
	bool read_s(int& x)
	{
		int remain=end-p;
		if(remain<20)
		{
			memcpy(buf,p,remain);
			end=buf+remain+fread(buf+remain,1,SIZE-remain,fin);
			p=buf;
		}
		while(p<end&&isspace(*p++));
		if(p==end)
			return false;
		bool neg=false;
		if(*--p=='-')
			neg=true,p++;
		x=0;
		for(;p<end&&isdigit(*p);p++)
			x=x*10+*p-'0';
		if(neg)
			x=-x;
		return true;
	}
};
namespace fastO
{
	const int SIZE=1000000;
	char buf[SIZE],*end=buf+SIZE,*p=buf;
	int d[15];
	void write(int x)
	{
		if(end-p<20)
		{
			fwrite(buf,1,p-buf,fout);
			p=buf;
		}
		int k=0;
		do
			d[++k]=x%10;
		while(x/=10);
		for(;k;k--)
			*p++=d[k]+'0';
	}
};
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	time_t t=clock();
	int n;
	fastI::read_s(n);
	int *a=new int [n];
	for(int i=0;i<n;i++)
		fastI::read_s(a[i]);
	cout<<"input:"<<clock()-t<<endl;
	t=clock();
	stable_sort(a,a+n);
	cout<<"using std::sort:"<<clock()-t<<endl;
	t=clock();
	for(int i=0;i<n;i++)
	{
		fastO::write(a[i]);
		*(fastO::p)++=' ';
	}
	fwrite(fastO::buf,1,fastO::p-fastO::buf,fout);
	cout<<"output:"<<clock()-t<<endl;
	cout<<"total:"<<clock()<<endl;
	fclose(fin);
	fclose(fout);
	return 0;
}
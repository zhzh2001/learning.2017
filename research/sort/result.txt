flags:
s	static
i	inline
f	__attribute__((always_inline))
Ox	-Ox

n=1e8
SIZE~1GB

name		flags	input(ms)	sort(ms)	output(ms)	total(ms)	tag
std::sort	/O0	14457		23030		14083		51606
std::sort	/O2	11982		10059		11452		33520		simple
std::sort	/sO0	23277		23059		14767		61134
std::sort	/sO2	11934		10124		11279		33415
std::sort	/iO0	13517		23322		14087		50936
std::sort	/iO2	12496		10077		11451		34039
std::sort	/siO0	12199		23197		14025		49452
std::sort	/siO2	12542		10047		11278		33898
std::sort	/fiO0	11622		23619		14025		49327
std::sort	/fiO2	11700		10046		11341		33118
std::sort	/fsiO0	11934		23010		13978		48968		best
std::sort	/fsiO2	12293		10046		11216		33586
std::stable_sort/fiO0	12402		25334		14040		51807
std::stable_sort/O2	11949		12543		11949		36472
qsort		/fsiO0	12059		24242		14274		50606
qsort		/O2	11856		22511		11450		45864
qsort		/fsiO2	11919		22511		11169		45645
merge_sort	/fsiO0	12292		24742		14056		51090
merge_sort	/O2	-		17613		-
pqsort		/fsiO0	18611		20716		14321		-
pqsort		/O2	7784		12652		11232		31699


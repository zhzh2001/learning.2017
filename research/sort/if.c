#include<stdio.h>
#include<time.h>
#include<stdlib.h>
int rand_32()
{
	if(RAND_MAX==32767)
		return rand()<<15+rand();
	else
		return rand();
}
void minmax1(int *a, int *b, int n) {  for (int i = 0; i < n; i++) {   if (a[i] > b[i]) {    int t = a[i];    a[i] = b[i], b[i] = t;   }  } }
void minmax2(int *a, int *b, int n) {  for (int i = 0; i < n; i++) {   int mi = a[i] < b[i] ? a[i] : b[i];   int ma = a[i] < b[i] ? b[i] : a[i];   a[i] = mi, b[i] = ma;  } }
int main()
{
	srand(time(NULL));
	int n;
	scanf("%d",&n);
	int *a=malloc(n*sizeof(int)),*b=malloc(n*sizeof(int));
	for(int i=0;i<n;i++)
	{
		a[i]=rand_32();
		b[i]=rand_32();
	}
	time_t t=clock();
	minmax1(a,b,n);
	printf("minmax1:%d\n",clock()-t);
	t=clock();
	minmax2(a,b,n);
	printf("minmax2:%d\n",clock()-t);
	free(a);
	free(b);
	return 0;
}
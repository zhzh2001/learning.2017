	.file	"merge_sort.c"
	.text
	.p2align 4,,15
	.globl	_myread
	.def	_myread;	.scl	2;	.type	32;	.endef
_myread:
LFB31:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$28, %esp
	.cfi_def_cfa_offset 48
	movl	_iend, %esi
	movl	_ip, %ebx
	movl	48(%esp), %edi
	movl	%esi, %ebp
	subl	%ebx, %ebp
	cmpl	$19, %ebp
	jle	L18
L2:
	movl	__imp__isspace, %ebp
	jmp	L4
	.p2align 4,,7
L19:
	addl	$1, %ebx
	movl	%ebx, _ip
	movsbl	-1(%ebx), %eax
	movl	%eax, (%esp)
	call	*%ebp
	testl	%eax, %eax
	je	L3
L4:
	cmpl	%ebx, %esi
	ja	L19
L3:
	cmpl	%ebx, %esi
	.p2align 4,,5
	je	L13
	leal	-1(%ebx), %eax
	movl	%eax, _ip
	cmpb	$45, -1(%ebx)
	je	L20
	movl	%eax, %ebx
	xorl	%ebp, %ebp
L6:
	cmpl	%ebx, %esi
	movl	$0, (%edi)
	jbe	L9
	movsbl	(%ebx), %ecx
	leal	-48(%ecx), %eax
	cmpl	$9, %eax
	ja	L9
	leal	1(%ebx), %eax
	xorl	%edx, %edx
	jmp	L10
	.p2align 4,,7
L21:
	movsbl	(%eax), %ecx
	addl	$1, %eax
	leal	-48(%ecx), %ebx
	cmpl	$9, %ebx
	ja	L9
L10:
	leal	(%edx,%edx,4), %edx
	cmpl	%esi, %eax
	leal	-48(%ecx,%edx,2), %edx
	movl	%edx, (%edi)
	movl	%eax, _ip
	jne	L21
L9:
	testl	%ebp, %ebp
	je	L16
	negl	(%edi)
L16:
	addl	$28, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	movl	$1, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
	.p2align 4,,7
L18:
	.cfi_restore_state
	movl	%ebx, 4(%esp)
	movl	$_ibuf, %ebx
	movl	%ebp, 8(%esp)
	movl	$_ibuf, (%esp)
	call	_memcpy
	movl	_fin, %eax
	movl	$1, 4(%esp)
	movl	%eax, 12(%esp)
	movl	$1000000, %eax
	subl	%ebp, %eax
	movl	%eax, 8(%esp)
	leal	_ibuf(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	movl	$_ibuf, _ip
	leal	_ibuf(%ebp,%eax), %esi
	movl	%esi, _iend
	jmp	L2
	.p2align 4,,7
L20:
	movl	%ebx, _ip
	movl	$1, %ebp
	jmp	L6
L13:
	addl	$28, %esp
	.cfi_def_cfa_offset 20
	xorl	%eax, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE31:
	.p2align 4,,15
	.globl	_mywrite
	.def	_mywrite;	.scl	2;	.type	32;	.endef
_mywrite:
LFB32:
	.cfi_startproc
	pushl	%edi
	.cfi_def_cfa_offset 8
	.cfi_offset 7, -8
	pushl	%esi
	.cfi_def_cfa_offset 12
	.cfi_offset 6, -12
	pushl	%ebx
	.cfi_def_cfa_offset 16
	.cfi_offset 3, -16
	subl	$16, %esp
	.cfi_def_cfa_offset 32
	movl	_op, %ecx
	movl	_oend, %eax
	movl	32(%esp), %ebx
	subl	%ecx, %eax
	cmpl	$19, %eax
	jle	L30
L23:
	xorl	%esi, %esi
	movl	$1717986919, %edi
	.p2align 4,,7
L25:
	movl	%ebx, %eax
	addl	$1, %esi
	imull	%edi
	movl	%ebx, %eax
	sarl	$31, %eax
	sarl	$2, %edx
	subl	%eax, %edx
	movl	%edx, %eax
	leal	(%edx,%edx,4), %edx
	addl	%edx, %edx
	subl	%edx, %ebx
	testl	%eax, %eax
	movl	%ebx, %edx
	movl	%ebx, _d(,%esi,4)
	movl	%eax, %ebx
	jne	L25
	leal	_d-4(,%esi,4), %eax
	jmp	L27
	.p2align 4,,7
L31:
	movl	_op, %ecx
	subl	$4, %eax
	movl	4(%eax), %edx
L27:
	addl	$48, %edx
	cmpl	$_d, %eax
	leal	1(%ecx), %ebx
	movl	%ebx, _op
	movb	%dl, (%ecx)
	jne	L31
	addl	$16, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 16
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 12
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 8
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 4
	ret
	.p2align 4,,7
L30:
	.cfi_restore_state
	movl	_fout, %eax
	subl	$_obuf, %ecx
	movl	%ecx, 8(%esp)
	movl	$1, 4(%esp)
	movl	$_obuf, (%esp)
	movl	%eax, 12(%esp)
	call	_fwrite
	movl	$_obuf, %ecx
	jmp	L23
	.cfi_endproc
LFE32:
	.p2align 4,,15
	.globl	_msort
	.def	_msort;	.scl	2;	.type	32;	.endef
_msort:
LFB33:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$44, %esp
	.cfi_def_cfa_offset 64
	movl	64(%esp), %eax
	movl	68(%esp), %edi
	movl	%eax, %ecx
	movl	%eax, 24(%esp)
	movl	72(%esp), %eax
	cmpl	%edi, %ecx
	movl	%eax, %esi
	movl	%eax, 16(%esp)
	je	L32
	movl	%edi, %eax
	subl	%ecx, %eax
	movl	%eax, 20(%esp)
	sarl	$2, 20(%esp)
	movl	20(%esp), %ebx
	movl	%esi, 8(%esp)
	movl	%ecx, %esi
	movl	%ecx, (%esp)
	movl	%ebx, %eax
	shrl	$31, %eax
	addl	%ebx, %eax
	sarl	%eax
	leal	(%ecx,%eax,4), %ebp
	movl	%ebp, 4(%esp)
	leal	4(%ebp), %ebx
	call	_msort
	movl	16(%esp), %eax
	movl	%edi, 4(%esp)
	movl	%ebx, (%esp)
	movl	%eax, 8(%esp)
	call	_msort
	cmpl	%ebx, %edi
	movl	16(%esp), %eax
	jb	L44
	cmpl	%ebp, %esi
	ja	L44
	movl	%esi, %ecx
	jmp	L37
	.p2align 4,,7
L62:
	addl	$4, %ecx
	movl	%esi, %edx
	cmpl	%edi, %ebx
	movl	%edx, -4(%eax)
	ja	L34
L63:
	cmpl	%ebp, %ecx
	ja	L34
L37:
	movl	(%ecx), %esi
	addl	$4, %eax
	movl	(%ebx), %edx
	cmpl	%edx, %esi
	jl	L62
	addl	$4, %ebx
	cmpl	%edi, %ebx
	movl	%edx, -4(%eax)
	jbe	L63
L34:
	cmpl	%ecx, %ebp
	jb	L61
	movl	%eax, %esi
	movl	%ecx, %edx
	movl	%eax, 28(%esp)
	.p2align 4,,7
L40:
	addl	$4, %edx
	movl	-4(%edx), %eax
	addl	$4, %esi
	cmpl	%edx, %ebp
	movl	%eax, -4(%esi)
	jnb	L40
	movl	28(%esp), %eax
	subl	%ecx, %ebp
	shrl	$2, %ebp
	cmpl	%ebx, %edi
	leal	4(%eax,%ebp,4), %eax
	jb	L64
	.p2align 4,,7
L53:
	addl	$4, %ebx
	movl	-4(%ebx), %edx
	addl	$4, %eax
	movl	%edx, -4(%eax)
L61:
	cmpl	%ebx, %edi
	jnb	L53
L64:
	movl	20(%esp), %eax
	leal	4(,%eax,4), %eax
	movl	%eax, 72(%esp)
	movl	16(%esp), %eax
	movl	%eax, 68(%esp)
	movl	24(%esp), %eax
	movl	%eax, 64(%esp)
	addl	$44, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	jmp	_memcpy
	.p2align 4,,7
L32:
	.cfi_restore_state
	addl	$44, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
	.p2align 4,,7
L44:
	.cfi_restore_state
	movl	24(%esp), %ecx
	jmp	L34
	.cfi_endproc
LFE33:
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "r\0"
LC1:
	.ascii "sort.in\0"
LC2:
	.ascii "w\0"
LC3:
	.ascii "sort.out\0"
LC4:
	.ascii "using merge_sort:%d\12\0"
	.section	.text.startup,"x"
	.p2align 4,,15
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB34:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	andl	$-16, %esp
	subl	$48, %esp
	.cfi_offset 7, -12
	.cfi_offset 6, -16
	.cfi_offset 3, -20
	call	___main
	movl	$LC0, 4(%esp)
	movl	$LC1, (%esp)
	call	_fopen
	movl	$LC2, 4(%esp)
	movl	$LC3, (%esp)
	movl	%eax, _fin
	call	_fopen
	movl	%eax, _fout
	leal	44(%esp), %eax
	movl	%eax, (%esp)
	call	_myread
	movl	44(%esp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, %esi
	movl	44(%esp), %eax
	sall	$2, %eax
	movl	%eax, (%esp)
	call	_malloc
	movl	%eax, 28(%esp)
	movl	44(%esp), %eax
	testl	%eax, %eax
	jle	L66
	movl	%esi, %edi
	xorl	%ebx, %ebx
	.p2align 4,,7
L67:
	movl	%edi, (%esp)
	addl	$1, %ebx
	addl	$4, %edi
	call	_myread
	movl	44(%esp), %eax
	cmpl	%ebx, %eax
	jg	L67
L66:
	movl	28(%esp), %ecx
	leal	-4(%esi,%eax,4), %eax
	xorl	%ebx, %ebx
	movl	%eax, 4(%esp)
	movl	%esi, (%esp)
	movl	%ecx, 8(%esp)
	call	_msort
	movl	44(%esp), %eax
	testl	%eax, %eax
	jle	L69
	.p2align 4,,7
L71:
	movl	(%esi,%ebx,4), %eax
	addl	$1, %ebx
	movl	%eax, (%esp)
	call	_mywrite
	movl	_op, %eax
	leal	1(%eax), %edx
	movl	%edx, _op
	movb	$32, (%eax)
	cmpl	%ebx, 44(%esp)
	jg	L71
L69:
	movl	_fout, %eax
	movl	$1, 4(%esp)
	movl	$_obuf, (%esp)
	movl	%eax, 12(%esp)
	movl	_op, %eax
	subl	$_obuf, %eax
	movl	%eax, 8(%esp)
	call	_fwrite
	call	_clock
	movl	$LC4, (%esp)
	movl	%eax, 4(%esp)
	call	_printf
	movl	%esi, (%esp)
	call	_free
	movl	28(%esp), %eax
	movl	%eax, (%esp)
	call	_free
	movl	_fin, %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	_fout, %eax
	movl	%eax, (%esp)
	call	_fclose
	leal	-12(%ebp), %esp
	xorl	%eax, %eax
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
LFE34:
	.comm	_d, 60, 5
	.globl	_op
	.data
	.align 4
_op:
	.long	_obuf
	.globl	_oend
	.align 4
_oend:
	.long	_obuf+1000000
	.comm	_obuf, 1000000, 5
	.globl	_ip
	.align 4
_ip:
	.long	_ibuf+1000000
	.globl	_iend
	.align 4
_iend:
	.long	_ibuf+1000000
	.comm	_ibuf, 1000000, 5
	.comm	_fout, 4, 2
	.comm	_fin, 4, 2
	.ident	"GCC: (i686-posix-dwarf-rev0, Built by MinGW-W64 project) 4.8.4"
	.def	_memcpy;	.scl	2;	.type	32;	.endef
	.def	_fread;	.scl	2;	.type	32;	.endef
	.def	_fwrite;	.scl	2;	.type	32;	.endef
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_malloc;	.scl	2;	.type	32;	.endef
	.def	_clock;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_free;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef

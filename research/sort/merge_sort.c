#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>
#include<time.h>
//#define inline __attribute__((always_inline))
FILE *fin,*fout;
#define SIZE 1000000
char ibuf[SIZE],*iend=ibuf+SIZE,*ip=ibuf+SIZE;
int myread(int* x)
{
	int remain=iend-ip;
	if(remain<20)
	{
		memcpy(ibuf,ip,remain);
		iend=ibuf+remain+fread(ibuf+remain,1,SIZE-remain,fin);
		ip=ibuf;
	}
	while(ip<iend&&isspace(*ip++));
	if(ip==iend)
		return 0;
	int neg=0;
	if(*--ip=='-')
		neg=1,ip++;
	*x=0;
	for(;ip<iend&&isdigit(*ip);ip++)
		*x=*x*10+*ip-'0';
	if(neg)
		*x=-*x;
	return 1;
}
char obuf[SIZE],*oend=obuf+SIZE,*op=obuf;
int d[15];
void mywrite(int x)
{
	if(oend-op<20)
	{
		fwrite(obuf,1,op-obuf,fout);
		op=obuf;
	}
	int k=0;
	do
		d[++k]=x%10;
	while(x/=10);
	for(;k;k--)
		*op++=d[k]+'0';
}
void msort(int* l,int* r,int* tmp)
{
	if(l==r)
		return;
	int* mid=l+(r-l)/2;
	msort(l,mid,tmp);
	msort(mid+1,r,tmp);
	int* i=l,*j=mid+1,*k=tmp;
	while(i<=mid&&j<=r)
		*k++=*i<*j?*i++:*j++;
	while(i<=mid)
		*k++=*i++;
	while(j<=r)
		*k++=*j++;
	memcpy(l,tmp,(r-l+1)*sizeof(int));
}
int main()
{
	fin=fopen("sort.in","r");
	fout=fopen("sort.out","w");
	time_t t=clock();
	int n;
	myread(&n);
	int *a=malloc(n*sizeof(int)),*tmp=malloc(n*sizeof(int));
	for(int i=0;i<n;i++)
		myread(a+i);
	printf("input:%d\n",clock()-t);
	t=clock();
	msort(a,a+n-1,tmp);
	printf("using merge_sort:%d\n",clock()-t);
	t=clock();
	for(int i=0;i<n;i++)
	{
		mywrite(a[i]);
		*op++=' ';
	}
	fwrite(obuf,1,op-obuf,fout);
	printf("output:%d\n",clock()-t);
	printf("total:%d\n",clock());
	free(a);
	free(tmp);
	fclose(fin);
	fclose(fout);
	return 0;
}
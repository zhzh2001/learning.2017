#include<bits/stdc++.h>
#include<ext/pb_ds/priority_queue.hpp>
using namespace __gnu_pbds;
const int INF=0x3f3f3f3f,N=10005,M=500005;
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){};
	inline bool operator>(const node b) const
	{
		return w>b.w;
	}
};
char buf[10000005],*now;
priority_queue<node,std::greater<node>,pairing_heap_tag> q;
priority_queue<node,std::greater<node>,pairing_heap_tag>::point_iterator it[N];
int d[N],head[N],next[M],v[M],w[M];
bool inq[N];
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int tmp[10];
inline void putint(int x)
{
	int len=0;
	do
		tmp[++len]=x%10;
	while(x/=10);
	for(;len;len--)
		*(++now)=tmp[len]+'0';
}
int main()
{
	fread(buf,1,10000000,stdin);
	now=buf-1;
	int n=getint(),m=getint(),s=getint();
	for(int i=1;i<=m;i++)
	{
		int u=getint();
		v[i]=getint();w[i]=getint();
		next[i]=head[u];
		head[u]=i;
	}
	memset(d,0x3f,sizeof(d));
	memset(inq,false,sizeof(inq));
	d[s]=0;
	it[s]=q.push(node(s,0));
	inq[s]=true;
	while(!q.empty())
	{
		node k=q.top();q.pop();
		inq[k.v]=false;
		for(int j=head[k.v];j;j=next[j])
			if(d[k.v]+w[j]<d[v[j]])
			{
				d[v[j]]=d[k.v]+w[j];
				if(inq[v[j]])
					q.modify(it[v[j]],node(v[j],d[v[j]]));
				else
					it[v[j]]=q.push(node(v[j],d[v[j]]));
				inq[v[j]]=true;
			}
	}
	now=buf-1;
	for(int i=1;i<=n;i++)
		if(d[i]==INF)
		{
			putint(2147483647);
			*(++now)=' ';
		}
		else
		{
			putint(d[i]);
			*(++now)=' ';
		}
	fwrite(buf,1,now-buf,stdout);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int INF=0x3f3f3f3f,N=1000005,M=20000005,FS=375000000;
struct node
{
	int v,w;
	bool operator>(const node b) const
	{
		return w>b.w;
	}
};
char buf[FS],*now;
vector<node> mat[N];
priority_queue<node,vector<node>,greater<node> > q;
int d[N];
bool vis[N];
inline int getint()
{
	while(!isdigit(*(++now)));
	int ret=*now-'0';
	while(isdigit(*(++now)))
		ret=ret*10+*now-'0';
	return ret;
}
int main()
{
	freopen("sssp.in","r",stdin);
	freopen("sssp.out","w",stdout);
	fread(buf,1,FS,stdin);
	now=buf-1;
	int n=getint(),m=getint(),s=getint();
	while(m--)
	{
		int u=getint(),v=getint(),w=getint();
		mat[u].push_back((node){v,w});
	}
	memset(vis,false,sizeof(vis));
	memset(d,0x3f,sizeof(d));
	d[s]=0;
	q.push((node){s,0});
	while(!q.empty())
	{
		node k=q.top();q.pop();
		if(!vis[k.v])
		{
			vis[k.v]=true;
			for(int j=0;j<mat[k.v].size();j++)
				if(!vis[mat[k.v][j].v]&&d[k.v]+mat[k.v][j].w<d[mat[k.v][j].v])
				{
					d[mat[k.v][j].v]=d[k.v]+mat[k.v][j].w;
					q.push((node){mat[k.v][j].v,d[mat[k.v][j].v]});
				}
		}
	}
	for(int i=1;i<=n;i++)
		if(d[i]==INF)
			printf("2147483647 ");
		else
			printf("%d ",d[i]);
	puts("");
	return 0;
}
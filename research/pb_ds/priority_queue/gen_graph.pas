program gen_graph;
const
  s=1;
  maxw=1000;
var
  n,m,i:longint;
begin
  randomize;
  read(n,m);
  assign(output,'graph.in');
  rewrite(output);
  writeln(n,' ',m,' ',s);
  for i:=1 to n-1 do
    writeln(i,' ',i+1,' ',random(maxw));
  for i:=n to m do
    writeln(random(n)+1,' ',random(n)+1,' ',random(maxw));
  close(output);
end.
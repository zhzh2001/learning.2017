#include<fstream>
#include<string>
#include<ext/pb_ds/assoc_container.hpp>
#include<ext/pb_ds/hash_policy.hpp>
using namespace std;
using namespace __gnu_pbds;
const string s="Begin the Escape execution at the Break of Dawn",pattern="COW";
int ans,cnt[256],cstd[256];
gp_hash_table<string,null_type> S;
ifstream fin("us.in");
ofstream fout("us.out");
void dfs(int k,string now)
{
	if(k==ans+1)
	{
		if(now==s)
		{
			fout<<"1 "<<ans<<endl;
			exit(0);
		}
		return;
	}
	int p=now.find_first_of(pattern);
	if(p==string::npos||now.substr(0,p)!=s.substr(0,p))
		return;
	p=now.find_last_of(pattern);
	if(p==string::npos||now.substr(p+1)!=s.substr(s.length()-(now.length()-p)+1))
		return;
	for(int i=now.find_first_of(pattern);i<now.length();)
	{
		int j=now.find_first_of(pattern,i+1);
		if(j==string::npos)
			break;
		if(s.find(now.substr(i+1,j-i-1))==string::npos)
			return;
		i=j;
	}
	if(S.find(now)!=S.end())
		return;
	S.insert(now);
	for(int j=0;j<now.length();j++)
		if(now[j]=='O')
			for(int i=j-1;i>=0;i--)
				if(now[i]=='C')
					for(int t=j+1;t<now.length();t++)
						if(now[t]=='W')
						{
							string ss=now.substr(0,i)+now.substr(j+1,t-j-1)+now.substr(i+1,j-i-1)+now.substr(t+1);
							dfs(k+1,ss);
						}
}
int main()
{
	string mes;
	getline(fin,mes);
	if(mes==s)
	{
		fout<<"1 0\n";
		return 0;
	}
	if(mes.length()<s.length()||(mes.length()-s.length())%3)
		fout<<"0 0\n";
	else
	{
		int cc=0,oo=0,ww=0;
		for(int i=0;i<mes.length();i++)
			cnt[mes[i]]++;
		ans=(mes.length()-s.length())/3;
		if(cnt['C']!=cnt['O']||cnt['O']!=cnt['W']||cnt['C']!=ans)
		{
			fout<<"0 0\n";
			return 0;
		}
		for(int i=0;i<s.length();i++)
		{
			if(mes.find(s[i])==string::npos)
			{
				fout<<"0 0\n";
				return 0;
			}
			cstd[s[i]]++;
		}
		for(int i=0;i<s.length();i++)
			if(cstd[s[i]]!=cnt[s[i]])
			{
				fout<<"0 0\n";
				return 0;
			}
		dfs(1,mes);
		fout<<"0 0\n";
	}
	return 0;
}
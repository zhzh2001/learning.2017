#include<iostream>
#include<cstdio>
#include<stack>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100010
#define M 11
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

struct node{ int st,ed,num; }a[N];
int n,m,ans;
int wei[N];
bool vis[N],flag;
stack<int> q;
stack<int> qans;
bool cmp(node a,node b){ return a.st<b.st||(a.st==b.st&&a.ed<b.ed); }

void dfs(int x,int p){
	if(wei[a[x].ed]==0){
		if(p>ans) ans=p;
		return;
	}
	for(int i=wei[a[x].ed];i<=n;i++){
		dfs(i,p+1);
	}
	return;
}

void dfs2(int x,int p){
	q.push(a[x].num);
	if(wei[a[x].ed]==0){
		if(p==ans){
			while(!q.empty()){
				int u=q.top(); q.pop();
				qans.push(u);
			}
			flag=true;
			return;
		}
		q.pop();
		return;
	}
	for(int i=wei[a[x].ed];i<=n;i++)
		dfs2(i,p+1);
	q.pop();
	return;
}

int main(){
	freopen("record.in","r",stdin);
	freopen("record.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=n;i++){ a[i].st=read(); a[i].ed=read(); a[i].num=i; }
	sort(a+1,a+n+1,cmp);
	int t=0;
	for(int i=1;i<=n;i++)
		if(!wei[a[i].st])
			while(t<=a[i].st) wei[t++]=i;
	for(int i=1;i<=n;i++){
		dfs(i,1);
	}
	for(int i=1;i<=n;i++){
		while(!q.empty()) q.pop();
		dfs2(i,1);
		if(flag) break;
	}
	printf("%d\n",ans);
	printf("%d",ans);
	while(!q.empty()) q.pop();
	for(int i=1;i<=ans;i++){
		int u=qans.top(); qans.pop();
		q.push(u);
	}
	for(int i=1;i<=ans;i++){
		int u=q.top(); q.pop();
		printf(" %d",u);
	}
	return 0;
}

#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=1e5+5;
struct dian{int x,y,num;}d[N];
struct tree{int l,r,mi,v,x,y;}T[N*4];
int a[N*2],topa,L[N],R[N],ans[N],top;
int f[N],g[N];
int n,m,now;
int lisan(int x){
	int l=1,r=topa,mid,ans=1e9;
	while(r>=l){
		mid=l+r>>1;
		if(a[mid]>=x){
			ans=min(ans,mid);
			r=mid-1;
		}else l=mid+1;
	}return ans;
}
bool cmp(dian a,dian b){if(a.x!=b.x)return a.x<b.x;return a.y<b.y;}
void make()
{
	for(int i=1;i<=n;i++){
		scanf("%d%d",&d[i].x,&d[i].y);
		d[i].num=i;
		a[++topa]=d[i].x;
		a[++topa]=d[i].y;
	}	
	sort(a+1,a+topa+1);
	int l=0;a[0]=-1;
	for(int i=1;i<=topa;i++)if(a[i]!=a[l])a[++l]=a[i];
//	for(int i=1;i<=l;i++)cout<<a[i]<<' ';cout<<endl;
	topa=l;
	for(int i=1;i<=n;i++){
		d[i].x=lisan(d[i].x);
		d[i].y=lisan(d[i].y);
	}
	sort(d+1,d+n+1,cmp);
//	cout<<endl;for(int i=1;i<=n;i++)cout<<d[i].x<<' '<<d[i].y<<endl;
}
void up(int num){
	int x=num*2,y=x+1;
	T[num].mi=min(T[x].mi,T[y].mi);
	if(T[x].mi==T[num].mi)T[num].v=T[x].v;else T[num].v=T[y].v;
}
void maketree(int l,int r,int num){
	T[num].l=l;T[num].r=r;
	if(l==r){
		if(now>n||d[now].x!=l){T[num].mi=1e9;return;}
		T[num].x=now;
		T[num].mi=d[now].y;
		T[num].v=now;
		while(now<=n&&d[now].x==l)now++;
		T[num].y=now-1;
		return;		
	}
	int mid=l+r>>1;
	maketree(l,mid  ,num*2  );
	maketree(mid+1,r,num*2+1);
	up(num);
}
int get(int x,int y,int num){
	if(x<=T[num].l&&T[num].r<=y)return T[num].v;
	int a=-1,b=-1;
	if(T[num*2  ].r>=x)a=get(x,y,num*2  );
	if(T[num*2+1].l<=y)b=get(x,y,num*2+1);
	if(a==-1)return b;if(b==-1)return a;
	if(d[a].y<=d[b].y)return a;return b;
}
void del(int x,int num){
	if(T[num].l==T[num].r){
	//	cout<<T[num].mi<<' '<<T[num].l<<' '<<T[num].r<<endl;
		T[num].x++;
		if(T[num].x>T[num].y){
			T[num].mi=1e9;
			T[num].v=0;
			return;
		}
		T[num].mi=d[T[num].x].y;
		T[num].v=T[num].x;
		return;
	}
	if(T[num*2].r>=x)del(x,num*2);else del(x,num*2+1);
	up(num);
}
void out(int k){if(g[k])out(g[k]);printf(" %d",d[k].num);}
int main()
{
	freopen("record.in","r",stdin);
	freopen("record.out","w",stdout);
	scanf("%d%d",&n,&m);
	if(m==1&&n<=150){
		for(int i=1;i<=n;i++){
			scanf("%d%d",&d[i].x,&d[i].y);
			d[i].num=i;
		}
		sort(d+1,d+n+1,cmp);
		int ans=0,v;
		for(int i=1;i<=n;i++){
			for(int j=1;j<i;j++)if(d[i].x>=d[j].y)
				if(f[j]>f[i])f[i]=f[j],g[i]=j;
			f[i]++;
			if(f[i]>ans)ans=f[i],v=i;
		}
		printf("%d\n%d",ans,ans);
		out(v);
	}else{
		make();
		now=1;
		maketree(1,d[n].x,1);
	//	for(int num=1;num<=1000;num++)if(T[num].mi)cout<<T[num].l<<' '<<T[num].r<<' '<<T[num].mi<<' '<<T[num].v<<endl;
	
		for(int i=1;i<=m;i++){
			now=1;L[i]=top+1;
			while(1){
				if(now>d[n].x)break;
				now=get(now,d[n].x,1);
				if(now==0)break;
				del(d[now].x,1);
				ans[++top]=d[now].num;
				now=d[now].y;
			}R[i]=top;
		}
		printf("%d\n",top);
		for(int i=1;i<=m;i++){
			printf("%d",R[i]-L[i]+1);
			for(int j=L[i];j<=R[i];j++)printf(" %d",ans[j]);
			printf("\n");
		}
	}
}

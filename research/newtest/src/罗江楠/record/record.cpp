#include <bits/stdc++.h>
#define N 100007
#define zyy 1000000007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;if(ch==EOF)return 0;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
struct node{
	int l, r, id;
	bool operator < (const node &b) const {
		return r-l < b.r-b.l;
	}
}q[N];
int a[N<<1], b[N<<1];
vector<int>v;
int sum[N<<3], tag[N<<3], ls[N<<3], rs[N<<3];
bool cmp(int a, int b){
	return q[a].l < q[b].l;
}
void push_up(int x){
	sum[x] = max(sum[x<<1], sum[x<<1|1]);
}
void push_down(int x){
	if(!tag[x]) return;
	if(ls[x] != rs[x]){
		tag[x<<1]+=tag[x];
		tag[x<<1|1]+=tag[x];
		sum[x<<1]+=tag[x];
		sum[x<<1|1]+=tag[x];
	}
	tag[x] = 0;
}
void build(int x, int l, int r){
	ls[x] = l; rs[x] = r;
	if(l == r) return;
	int mid = l + r >> 1;
	build(x<<1, l, mid); build(x<<1|1, mid+1, r);
}
int ask(int x, int l, int r){
	// printf("%d %d %d\n",x,l,r);
	// printf("%d %d %d %d %d\n", x, l, r, ls[x], rs[x]);
	push_down(x);
	if(ls[x] == l && r == rs[x]) return sum[x];
	int mid = ls[x] + rs[x] >> 1;
	if(r <= mid) return ask(x<<1, l, r);
	if(l > mid) return ask(x<<1|1, l, r);
	return max(ask(x<<1, l, mid), ask(x<<1|1, mid+1, r));
}
void add(int x, int l, int r){
	// printf("%d %d %d",x,l,r);
	push_down(x);
	if(ls[x] == l && rs[x] == r){
		tag[x]++;
		sum[x]++;
		return;
	}
	int mid = ls[x] + rs[x] >> 1;
	if(r <= mid) add(x<<1, l, r);
	else if(l > mid) add(x<<1|1, l, r);
	else add(x<<1, l, mid), add(x<<1|1, mid+1, r);
	push_up(x);
}
int main(){
	freopen("record.in", "r", stdin);
	freopen("record.out", "w", stdout);
	int n = read(), k = read();
	build(1, 1, n<<1);
	for(int i = 1; i <= n; i++){
		a[++a[0]] = q[i].l = read();
		a[++a[0]] = q[i].r = read();
		q[i].id = i;
	}
	sort(a+1, a+a[0]+1); b[++b[0]] = a[1];
	for(int i = 2; i <= n; i++)
		if(a[i] != a[i-1]) b[++b[0]] = a[i];
	// for(int i = 1; i <= b[0]; i++) printf("%d\n", b[i]);
	for(int i = 1; i <= n; i++){
		q[i].l = lower_bound(b+1, b+b[0]+1, q[i].l)-b;
		q[i].r = lower_bound(b+1, b+b[0]+1, q[i].r)-b;
		// printf("%d %d\n", q[i].l, q[i].r);
	}
	sort(q+1, q+n+1);
	for(int i = 1; i <= n; i++){
		// printf("DDD%d %d %d\n", q[i].id, q[i].l, q[i].r);
		if(ask(1, q[i].l+1, q[i].r+1) <= k-1){
			// puts("wfafsf");
			add(1, q[i].l+1, q[i].r+1);
			v.push_back(q[i].id);
		}
	}
	// puts("wda");
	printf("%d\n", v.size());
	printf("%d", v.size());sort(v.begin(), v.end(), cmp);
	for(int i = 0; i < v.size(); i++){
		printf(" %d", v[i]);
	}puts("");
	return 0;
}
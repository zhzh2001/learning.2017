#include <bits/stdc++.h>
#define N 1000007
#define zyy 1000000007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;if(ch==EOF)return 0;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int in[N], out[N], vis[N], cnt, ssin[N], sout[N];
int head[N], to[N<<1], nxt[N<<1];
ll sum, tot;
void ins(int x, int y){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; head[y] = cnt;
}
int dfs1(int x){
	if(ssin[x]) return ssin[x];
	vis[x] = ssin[x] = 1;
	for(int i = head[x]; i; i = nxt[i])if(i&1){
		if(vis[to[i]]) exit(puts("-1")&0);
		ssin[x] += dfs1(to[i]);
	}
	vis[x] = 0;
	return ssin[x];
}
int dfs2(int x){
	if(sout[x]) return sout[x];
	vis[x] = sout[x] = 1;
	for(int i = head[x]; i; i = nxt[i])if(~i&1){
		if(vis[to[i]]) exit(puts("-1")&0);
		sout[x] += dfs2(to[i]);
	}
	vis[x] = 0;
	return sout[x];
}
int main(){
	freopen("sort.in", "r", stdin);
	freopen("sort.out", "w", stdout);
	int n = read(), m = read();
	for(int i = 1, x, y; i <= m; i++){
		x = read(); y = read();
		ins(x, y); in[y]++; out[x]++;
	}
	for(int i = 1; i <= n; i++)
		if(!in[i]) dfs1(i);
	for(int i = 1; i <= n; i++)
		if(!out[i]) dfs2(i);
	for(int i = 1; i <= n; i++)
		sum += n-ssin[i]-sout[i]+1;
	printf("%lld\n", sum+1>>1);
	return 0;
}
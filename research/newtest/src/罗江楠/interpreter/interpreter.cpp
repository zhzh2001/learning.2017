#include <bits/stdc++.h>
#define zyy 1000000007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;if(ch==EOF)return 0;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int lin, k = 1;
map<string, int>mp;
string str[105];
void run(){
	while(k <= lin){
		// printf("%d\n", k);
		stringstream ss;
		// cout << '[' << str[k] << ']' << endl;
		ss << str[k];
		// cout << "ss:[" << ss.str() << ']' << endl;
		if(str[k][str[k].length()-1] == '{'){
			int pos = ++k, num;
			ss >> num;
			for(int i = 1; i <= num; i++)
				k = pos, run();
		}
		else if(str[k][str[k].length()-1] == '}'){
			k++;
			return;
		}
		else{
			string name, tmp; ss >> name;
			// cout << '(' << name << ')' << endl;
			if(name.length() == 5 && name[0] == 'P' && name[1] == 'R'
				&& name[2] == 'I' && name[3] == 'N' && name[4] == 'T'){
				ss >> tmp;
				printf("%d\n", mp[tmp]);k++;
				continue;
			}
			string onamae; int ans = 0;
			while(ss >> tmp){
				ss >> onamae;
				if(onamae[0] >= '0' && onamae[0] <= '9'){
					int sum = 0;
					for(int i = 0; i < onamae.length(); i++)
						sum = sum*10+onamae[i]-'0';
					ans = (ans+sum)%zyy;
				}
				else ans += mp[onamae];
			}
			mp[name] = ans;
			k++;
		}
	}
}
int main(){
	freopen("interpreter.in", "r", stdin);
	freopen("interpreter.out", "w", stdout);
	while(getline(cin, str[++lin]));
	run();
	return 0;
}
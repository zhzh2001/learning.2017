#include <bits/stdc++.h>
#include <specio.h>
#define N 1000007
#define zyy 1000000007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;if(ch==EOF)return 0;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
std::vector<float> v;
int main(){
	freopen("challenge.in", "r", stdin);
	freopen("challenge.out", "w", stdout);
	int a = read(), b = read(), x = read(), n = read();
	init(a, b, x);
	for(int i = 1; i <= n; i++) v.push_back(readNext());
	sort(v.begin(), v.end());
	for(int i = 0; i < n-1; i++)
		writeNext(v[i]);
	printf("%d\n", writeNext(v[n-1]));
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int ans=0,n,m;
int f1[105],f2[105],m1[105][105],m2[105][105],vis[105];
 void dfs1(int x)
{
	vis[x]=1;f1[x]++;
	for (int i=1;i<=n;i++)
	{
		if (vis[i]||!m1[x][i]) continue;
		dfs1(i);
	}
}
 void dfs2(int x)
{
	vis[x]=1;f2[x]++;
	for (int i=1;i<=n;i++)
	{
		if (vis[i]||!m2[x][i]) continue;
		dfs2(i);
	}
}
 int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d",&n,&m);
	for (int i=1;i<=m;i++)
	{
		int u,v;
		scanf("%d%d",&u,&v);
		m1[u][v]=1;m2[v][u]=1;
	}
	for (int i=1;i<=n;i++)
	{
		dfs1(i);
		memset(vis,0,sizeof(vis));
	}
	for (int i=1;i<=n;i++)
	{
		dfs2(i);
		memset(vis,0,sizeof(vis));
	}
	for (int i=1;i<=n;i++)
	{
		ans+=n-(f1[i]+f2[i]-1);
	}
	ans/=2;
	printf("%d",ans);
}

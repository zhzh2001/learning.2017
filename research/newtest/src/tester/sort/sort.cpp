#include<fstream>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
const int N=505;
bool mat[N][N];
int main()
{
	int n,m;
	fin>>n>>m;
	if(n>1000)
	{
		fout<<-1<<endl;
		return 0;
	}
	for(int i=1;i<=n;i++)
		mat[i][i]=true;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		mat[u][v]=true;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			for(int j=1;j<=n;j++)
				mat[i][j]|=mat[i][k]&&mat[k][j];
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans+=mat[i][j];
	ans=n*(n-1)/2-ans+n;
	if(ans>=0)
		fout<<ans<<endl;
	else
		fout<<-1<<endl;
	return 0;
}
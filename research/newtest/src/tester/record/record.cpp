#include<stdio.h>
#include<string.h>
#include<algorithm>
#include<iostream> 
#define min(A,B) ((A)<(B)?(A):(B))  
using namespace std;
const int inf=0x3f3f3f3f;
struct Edge{
    int v,c,w,n;
    Edge(){}
    Edge(int V,int C,int W,int N):v(V),c(C),w(W),n(N){}
};
struct Node{
    int l,r,w;
};
Edge edge[10000003];
Node sche[100003];
int adj[100003],cntE;
int Q[10000003],head,tail;
int d[100003],inq[100003],cur[100003],f[100003];
int cost,flow,s,t;
int n,m;
inline void addedge(int u,int v,int c,int w){
    edge[cntE]=Edge(v,c,w,adj[u]);  adj[u]=cntE++;
    edge[cntE]=Edge(u,0,-w,adj[v]); adj[v]=cntE++;
}
int spfa(){
    memset(d,inf,sizeof(d));
    memset(inq,0,sizeof(inq));
    cur[s]=-1;
    f[s]=inf;
    head=tail=d[s]=0;
    Q[tail++]=s;
    inq[s]=1;
    while(head!=tail){
        int u=Q[head++];
        inq[u]=0;
        for(int i=adj[u];~i;i=edge[i].n){
            int v=edge[i].v,c=edge[i].c,w=edge[i].w;
            if(c&&d[v]>d[u]+w){
                d[v]=d[u]+w;
                f[v]=min(f[u],c);
                cur[v]=i;
                if(!inq[v]){
                    Q[tail++]=v;
                    inq[v]=1;
                }
            }
        }
    }
    if(d[t]==inf){
    	return 0;
	}
    return 1;
}
inline int mcmf(){
    flow=cost=0;
    while(spfa()){
    	flow+=f[t];
    	cost+=d[t]*f[t];
    	for(int i=cur[t];~i;i=cur[edge[i^1].v]){
        	edge[i].c-=f[t];
        	edge[i^1].c+=f[t];
    	}
	}
    return cost;
}
inline void init(){
	freopen("record.in","r",stdin);
	freopen("record.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++){
    	scanf("%d%d",&sche[i].l,&sche[i].r);
	}
	memset(adj,-1,sizeof(adj));
    cntE=0;
}
inline void build(){
    s=0;  t=n*2+3;
    int s1=n*2+1;
    addedge(s,s1,m,0);
    for(int i=1;i<=n;i++){
        addedge(s1,i,1,0);
        addedge(i,i+n,1,-1);
        addedge(i+n,t,1,0);
        for(int j=1;j<=n;j++){
            if(sche[i].r<=sche[j].l){
            	addedge(i+n,j,1,0);
			}
        }
    }
}
int main(){
	init();
    build();
    cout<<-mcmf();
    return 0;
}
/*

in:
6 2
0 3
6 7
3 10
1 5
2 8
1 9

out:
4
2 4 2
2 1 3

*/

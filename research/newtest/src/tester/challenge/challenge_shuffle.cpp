#include<fstream>
#include<algorithm>
#include<random>
#include<ctime>
#include<specio.h>
using namespace std;
ifstream fin("challenge.in");
ofstream fout("challenge.out");
int main()
{
	minstd_rand gen(time(NULL));
	int A,B,X,n;
	fin>>A>>B>>X>>n;
	init(A,B,X);
	float *a=new float [n];
	for(int i=0;i<n;i++)
		a[i]=readNext();
	shuffle(a,a+n,gen);
	int ans;
	for(int i=0;i<n;i++)
		ans=writeNext(a[i]);
	fout<<ans<<endl;
	return 0;
}
#include<stdio.h>
#include<stdlib.h>
#include<specio.h>
int cmp(const void *a,const void *b)
{
	float A=*(const float*)a,B=*(const float*)b;
	if(A<B)
		return -1;
	if(A>B)
		return 1;
	return 0;
}
int main(void)
{
	FILE *fin=fopen("challenge.in","r");
	int A,B,X,n;
	fscanf(fin,"%d%d%d%d",&A,&B,&X,&n);
	fclose(fin);
	init(A,B,X);
	float *a=malloc(n*sizeof(float));
	for(int i=0;i<n;i++)
		a[i]=readNext();
	qsort(a,n,sizeof(float),cmp);
	int ans;
	for(int i=0;i<n;i++)
		ans=writeNext(a[i]);
	FILE *fout=fopen("challenge.out","w");
	fprintf(fout,"%d\n",ans);
	fclose(fout);
	return 0;
}
program challenge;
uses
  specio;
const
  maxn=200000;
var
  n,_a,b,x,i:longint;
  a:array[1..maxn]of single;
procedure sort(l,r:longint);
var
  i,j:longint;
  x,y:single;
begin
  i:=l;
  j:=r;
  x:=a[(l+r) div 2];
  repeat
    while a[i]<x do
      inc(i);
    while x<a[j] do
      dec(j);
    if not(i>j) then
    begin
      y:=a[i];
      a[i]:=a[j];
      a[j]:=y;
      inc(i);
      j:=j-1;
    end;
  until i>j;
  if l<j then
    sort(l,j);
  if i<r then
    sort(i,r);
end;
begin
  assign(input,'challenge.in');
  reset(input);
  assign(output,'challenge.out');
  rewrite(output);
  read(_a,b,x,n);
  init(_a,b,x);
  for i:=1 to n do
    a[i]:=readNext;
  sort(1,n);
  for i:=1 to n do
    x:=writeNext(a[i]);
  writeln(x);
  close(input);
  close(output);
end.
#include<iostream>
#include<cstdio>
#include<vector>
#include<queue>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 50005
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int n,m,ans,t;
queue<int> q;
vector<int> edge[N];
bool vis[N];
int in[N];

int main(){
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read(); m=read();
	for(int i=1;i<=m;i++){
		int a=read(),b=read();
		edge[a].push_back(b);
		in[b]++;
	}
	for(int i=1;i<=n;i++) if(in[i]==0) q.push(i);
	while(!q.empty()){
		if(q.size()>1) ans+=q.size()-1;
		int u=q.front(); q.pop();
//		printf("# %d\n",u);
		for(int i=0;i<edge[u].size();i++){
			in[edge[u][i]]--;
			if(in[edge[u][i]]==0) q.push(edge[u][i]);
		}
		t++;
	}
	if(t<n) printf("-1");
	else printf("%d",ans);
	return 0;
}

#include <fstream>
#include <cstring>
#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;

const int N = 100005;

#define foreach(i, x) for (typeof(x.begin()) i = x.begin(); i != x.end(); ++i)

struct project {
	int s, t;
	
	friend bool operator < (const project &a, const project &b) {
		return a.t < b.t;
	}
} pro[N];

ifstream fin("record.in");
ofstream fout("record.out");

int main() {
	int n, k;
	fin >> n >> k;
	for (int i = 1; i <= n; i++)
		fin >> pro[i].s >> pro[i].t;
	sort(pro + 1, pro + n + 1);
	if (k == 1) {
		int end_time = 0;
		vector<int> ans;
		for (int i = 1; i <= n; i++)
			if (pro[i].s >= end_time) ans.push_back(i), end_time = pro[i].t;
		fout << ans.size() << endl;
		foreach (i, ans)
			fout << ans.size() << ' ' << *i << ' ';
		fout << endl;
		return 0;
	}
	if (k == 2) {
		
	}
	return 0;
}
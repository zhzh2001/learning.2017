#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cassert>
#include <queue>
#include <cstring>
#include <map>
#pragma GCC optimize(2)
using namespace std;

const int M = 105, mod = 1e9 + 7;

typedef long long ll;

ifstream fin("interpreter.in");
ofstream fout("interpreter.out");

int tot, Tmp[M];

class Matrix {
public:
	int a[M][M];

public:
	Matrix() {}
	Matrix(int val) { init(val); }
	void init(int val) {
		memset(a, 0, sizeof a);
		for (int i = 0; i <= tot; i++)
			a[i][i] = val;
	}

public:
	int *operator [](int i) { return a[i]; }

	friend Matrix operator * (const Matrix &x, const Matrix &y) {
		Matrix res(0);
		for (int i = 0; i <= tot; i++)
			for (int j = 0; j <= tot; j++)
				for (int k = 0; k <= tot; k++)
					(res.a[i][j] += (ll)x.a[i][k] * y.a[k][j] % mod) %= mod;
		return res;
	}

	Matrix &operator *= (const Matrix &x) {
		return *this = *this * x;
	}

public:
	friend Matrix pow(Matrix x, ll y) {
		Matrix res(1);
		for (; y; y >>= 1) {
			if (y & 1) res *= x;
			x *= x;
		}
		return res;
	}
} mat[M];

class Stack {
public:
	int stack[M], tot;
	Stack() { tot = 0; }
public:
	void push(int val) { stack[++tot] = val; mat[tot].init(1); }
	void pop() { --tot; }
	int at(int x) { return stack[x]; }
	int top() { return stack[tot]; }
} st;

class varlist {
public:
	varlist() {}
	int id(string var) {
		int &res = var_list[var];
		if (!res) return res = ++tot;
		return res;
	}

public:
	map<string, int> var_list;
} Var_List;

char str[400];

inline void solve_def(stringstream &ss) {
	memset(Tmp, 0, sizeof Tmp);
	int label = Var_List.id(str);
	while (ss >> str) {
		if (isdigit(str[0])) {
			int val = atoi(str);
			(Tmp[0] += val) %= mod;
		} else if (islower(str[0])) {
			int val = Var_List.id(str);
			for (int i = 0; i <= tot; i++)
				(Tmp[i] += mat[st.tot][val][i]) %= mod;
		}
	}
	for (int i = 0; i <= tot; i++) mat[st.tot][label][i] = Tmp[i];
}

inline void solve_print(stringstream &ss) {
	ss >> str;
	int val = Var_List.id(str), ans = mat[0][val][0];
	fout << ans << endl;
	exit(0);
}

string tmp;

int main() {
	mat[0].init(1);
	while (true) {
		getline(fin, tmp);
		stringstream ss(tmp);
		ss >> str;
		if (islower(str[0])) solve_def(ss);
		else if (isdigit(str[0])) st.push(atoi(str));
		else if (str[0] == '}') mat[st.tot - 1] = pow(mat[st.tot], st.top()) * mat[st.tot - 1], st.pop();
		else if (str[0] == 'P') solve_print(ss);
	}
	return 0;
}
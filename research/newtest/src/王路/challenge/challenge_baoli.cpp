#include <fstream>
#include <iostream>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <specio.h>
using namespace std;

const int N = 10005;

ifstream fin("challenge.in");
ofstream fout("challenge.out");

int A, B, X, n;

int main() {
	fin >> A >> B >> X >> n;
	init(A, B, X);
	float *a = new float[n];
	for (int i = 0; i < n; i++)
		a[i] = readNext();
	sort(a, a + n);
	int res = 0;
	for (int i = 0; i < n; i++) {
		res = writeNext(a[i]);
	}
	fout << res << endl;
	return 0;
}
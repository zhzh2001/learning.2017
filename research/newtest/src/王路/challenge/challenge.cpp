#include <iostream>
#include <fstream>
#include <cstring>
#include <ctime>
#include <algorithm>
#include <cassert>
#include <specio.h>
#include <cstdio>
#include <cstdlib>
#include <cassert>
using namespace std;

ifstream fin("challenge.in");
ofstream fout("challenge.out");

const unsigned N = 1e8, PER_BIT = 8, K = 1 << PER_BIT;

const unsigned rev = 0x7fffffffu, sign = 0x80000000u, ful = K - 1;
unsigned cnt[K + 5];

inline void radix_sort(int n) {
	float *a = new float[n], *b = new float[n], *x = a, *y = b;
	for (int i = 0; i < n; i++) {
		x[i] = readNext();
		reinterpret_cast<unsigned&>(x[i]) = (reinterpret_cast<unsigned&>(x[i]) >> 31 & 0x1)
			? ~reinterpret_cast<unsigned&>(x[i]) : reinterpret_cast<unsigned&>(x[i]) | sign;
	}
	for (int d = 0; d < 4; d++) {
		memset(cnt, 0, sizeof(unsigned) * K);
		int offset = d * PER_BIT;
		for (int i = 0; i < n; i++) 
			cnt[reinterpret_cast<unsigned&>(x[i]) >> offset & ful]++;
		for (int i = 1; i < K; i++)
			cnt[i] += cnt[i - 1];
		for (int i = n - 1; i >= 0; i--) {
			y[--cnt[reinterpret_cast<unsigned&>(x[i]) >> offset & ful]] = x[i];
		}
		swap(x, y);
	}
	int res;
	for (int i = 0; i < n; i++) {
		reinterpret_cast<unsigned&>(x[i]) = (reinterpret_cast<unsigned&>(x[i]) >> 31 & 0x1)
			? reinterpret_cast<unsigned&>(x[i]) & rev : ~reinterpret_cast<unsigned&>(x[i]);
		res = writeNext(x[i]);
	}
	fout << res << endl;
}

const int LIM = 40, SIZE = 5e5;

int debug_flag;

struct blockdata {
	FILE *file;
	float *buf, *p, *end;
	
	unsigned size;
	
	friend bool operator < (const blockdata &a, const blockdata &b) {
		return *(a.p) > *(b.p);
	}
	
	bool adjust(int &ans) {
		ans = writeNext(*p++);
		if (p == end) {
			end = (p = buf) + fread(buf, sizeof(float), size, file);
			// cout << end - buf << endl;
			if (end == buf) 
				return false;
		}
		return true;
	}
};

inline void external_sort(int length) {
	FILE **file = new FILE*[LIM];
	float *rdbuf = new float[SIZE];
	for (int i = 0; i < LIM; i++) {
		for (int j = 0; j < SIZE; j++) rdbuf[j] = readNext();
		sort(rdbuf, rdbuf + SIZE);
		file[i] = tmpfile();
		fwrite(rdbuf, sizeof(float), SIZE, file[i]);
		rewind(file[i]);
	}
	delete [] rdbuf;
	blockdata *heap = new blockdata[LIM];
	for (int i = 0; i < LIM; i++) {
		heap[i].p = (heap[i].buf = new float[heap[i].file = file[i], heap[i].size = SIZE / LIM]);
		heap[i].end = heap[i].buf + fread(heap[i].buf, sizeof(float), heap[i].size, file[i]);
		// cout << heap[i].end - heap[i].p << endl;
	}
	delete [] file;
	// std::thread tmp(debug);
	// debug_flag = 1;
	make_heap(heap, heap + LIM);
	int remain = LIM, ans;
	while (remain) {
		// debug_flag = 2;
		pop_heap(heap, heap + remain);
		if (heap[--remain].adjust(ans)) {
			++remain;
			// debug_flag = 3;
			push_heap(heap, heap + remain);
		} else delete [] heap[remain].buf;
	}
	fout << ans << endl;
}

int main() {
	int A, B, X, n;
	fin >> A >> B >> X >> n;
	init(A, B, X);
	if (n < 1e7 || n > 3e7) radix_sort(n);
	else external_sort(n);
	return 0;
}
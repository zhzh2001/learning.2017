#include <fstream>
#include <bitset>
using namespace std;

const int N = 3005;

typedef long long ll;

ifstream fin("sort.in");
ofstream fout("sort.out");

bool mat[N][N];

int main() {
	int m, n;
	fin >> n >> m;
	for (int i = 1; i <= m; i++) {
		int u, v;
		fin >> u >> v;
		mat[u][v] = true;
	}
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++) {
				mat[i][j] |= mat[i][k] && mat[k][j];
			}
	int res = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			res += mat[i][j];
		}
	fout << (ll)n * (n - 1) / 2 - res << endl;
	return 0;
}
#include <fstream>
#include <bitset>
using namespace std;

const int N = 3005;

typedef long long ll;

ifstream fin("sort.in");
ofstream fout("sort.out");

bitset<N> mat[N];

int main() {
	int m, n, u, v;
	fin >> n >> m;
	for (int i = 1; i <= m; i++) {
		fin >> u >> v;
		mat[u][v] = true;
	}
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (mat[i][j]) {
				mat[i] |= mat[j];
			}
	int res = 0;
	for (int i = 1; i <= n; i++)
		res += mat[i].count();
	fout << (ll)n * (n - 1) / 2 - res << endl;
	return 0;
}
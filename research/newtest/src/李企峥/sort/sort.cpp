#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
ll n,m,ans;
short a[16000][16000];
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	n=read();m=read();
	For(i,1,m)
	{
		int x=read(),y=read();
		if(a[x][y]==-1||x==y)
		{
			cout<<-1<<endl;
			return 0;		
		}
		a[x][y]=1;a[y][x]=-1;
	}
	For(k,1,n)
		For(i,1,n)
		{
			if(i==k)continue;
			if(a[i][k]==0)continue;
			For(j,1,n)
			{
				if(i==j||j==k)continue;
				if(a[k][j]==0)continue;
				if(a[i][k]==a[k][j])
				{
					if(a[i][j]==-a[i][k])
					{
						cout<<-1<<endl;
						return 0;
					}
					a[i][j]=a[i][k];
				}
			}
		}
	ans=n*(n-1);
	For(i,1,n)
	{
		For(j,1,n)
			if(a[i][j]!=0)ans--;
	}
	ans/=2;
	cout<<ans<<endl;
	return 0;
}


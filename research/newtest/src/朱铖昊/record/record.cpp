#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
struct D{
	int x,y,z;
}a[100010];
int n,k;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
bool cmp(D a,D b){return a.y<b.y;}
int main()
{
	freopen("record.in","r",stdin);
	freopen("record.out","w",stdout);
	n=read();k=read();
	For(i,1,n)a[i].x=read(),a[i].y=read();
	if(k==1)
	{
		For(i,1,n)a[i].z=i;
		sort(a+1,a+n+1,cmp);
		int x=1;
		int ans[100010];
		ans[0]=1;
		ans[1]=a[1].z;
		while(x<=n)
		{
			int y=x+1;
			while(a[y].x<a[x].y&&y<=n)y++;
			if(y>n)break;
			ans[0]++;
			ans[ans[0]]=a[y].z;
			x=y;
		}
		cout<<ans[0]<<endl;
		For(i,0,ans[0])cout<<ans[i]<<" ";
	}
	else cout<<min(n,k*2)<<endl;
	return 0;
}


#include<bits/stdc++.h>
using namespace std;
const int M=200005;
const int N=50005;
int head[M*2],nxt[M*2],tail[M*2],e[M*2];
int n,m,t;
int f[N];
int g[N];
int xf[N];
int xg[N];
bool vis[N];
void addto(int x,int y,int z)
{
	t++;
	nxt[t]=head[x];
	head[x]=t;
	tail[t]=y;
	e[t]=z;
}
void dfs1(int k)
{
	if(vis[k]){
		printf("-1");
		exit(0);
	}
	vis[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(e[i]==1){
			xg[tail[i]]=xg[k];
			dfs1(tail[i]);
		}
	vis[k]=0;
}
void dfs2(int k)
{
	if(vis[k]){
		printf("-1");
		exit(0);
	}
	vis[k]=1;
	for(int i=head[k];i;i=nxt[i])
		if(e[i]==2){
			xf[tail[i]]=xf[k];
			dfs2(tail[i]);
		}
	vis[k]=0;
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;
		scanf("%d%d",&x,&y);
		addto(x,y,1);
		addto(y,x,2);
	}
	for(int i=1;i<=n;i++){
		xg[i]=xf[i]=i;
		f[i]=g[i]=-1;
		dfs1(i);
		for(int j=1;j<=n;j++)
			if(xg[j]==i)g[i]++;
		dfs2(i);
		for(int j=1;j<=n;j++)
			if(xf[j]==i)f[i]++;
	}
	int ans=0;
	for(int i=1;i<=n;i++){
//		cout<<f[i]<<' '<<g[i]<<endl;
		ans+=(n-1)-f[i]-g[i];
	}
	printf("%d",ans/2);
	return 0;
} 

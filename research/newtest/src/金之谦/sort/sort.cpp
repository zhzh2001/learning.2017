#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cmath>
#include<iostream>
#include<cstdlib>
#include<string>
#include<ctime>
#include<queue>
#include<climits>
using namespace std;
long long ans=0,r=0;
int n,m,flag=0,b1[50001],b2[50001];
int nedge1=0,p1[200001],nex1[200001],hea1[200001];
int nedge2=0,p2[200001],nex2[200001],hea2[200001];
inline void addedge1(int a,int b){
	p1[++nedge1]=b;nex1[nedge1]=hea1[a];hea1[a]=nedge1;
}
inline void addedge2(int a,int b){
	p2[++nedge2]=b;nex2[nedge2]=hea2[a];hea2[a]=nedge2;
}
inline void dfs1(int x,int s){
	r++;b1[x]=s;
	for(int k=hea1[x];k;k=nex1[k])if(b1[p1[k]]!=s)dfs1(p1[k],s);
}
inline void dfs2(int x,int s){
	if(flag)return;r++;b2[x]=s;
	for(int k=hea2[x];k;k=nex2[k])if(b2[p2[k]]!=s){
		if(b1[p2[k]]==s){flag=1;return;}
		dfs2(p2[k],s);
	}
}
int main()
{
	freopen("sort.in","r",stdin);
	freopen("sort.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=m;i++){
		int x,y;scanf("%d%d",&x,&y);
		addedge1(x,y);addedge2(y,x);
	}
	for(int i=1;i<=n;i++){
		r=0;dfs1(i,i);dfs2(i,i);
		if(flag)return puts("-1")&0;
		ans+=(long long)n-r+1ll;
	}
	printf("%lld",ans/2);
	return 0;
}

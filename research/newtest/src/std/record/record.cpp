#include<fstream>
#include<set>
#include<algorithm>
#include<vector>
using namespace std;
ifstream fin("record.in");
ofstream fout("record.out");
const int N=100005;
struct program
{
	int s,t,id;
	bool operator<(const program& rhs)const
	{
		return t<rhs.t;
	}
}a[N];
vector<int> p[N];
struct node
{
	int t,id;
	node(int t,int id):t(t),id(id){}
	bool operator<(const node& rhs)const
	{
		return t<rhs.t;
	}
};
int main()
{
	int n,k;
	fin>>n>>k;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i].s>>a[i].t;
		a[i].id=i;
	}
	sort(a+1,a+n+1);
	multiset<node> S;
	for(int i=1;i<=k;i++)
		S.insert(node(0,i));
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		multiset<node>::iterator it=S.upper_bound(node(a[i].s,0));
		if(it--!=S.begin())
		{
			ans++;
			int id=it->id;
			p[id].push_back(a[i].id);
			S.erase(it);
			S.insert(node(a[i].t,id));
		}
	}
	fout<<ans<<endl;
	for(int i=1;i<=k;i++)
	{
		fout<<p[i].size();
		for(vector<int>::iterator it=p[i].begin();it!=p[i].end();it++)
			fout<<' '<<*it;
		fout<<endl;
	}
	return 0;
}
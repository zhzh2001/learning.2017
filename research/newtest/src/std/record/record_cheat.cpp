#include<fstream>
#include<set>
#include<algorithm>
using namespace std;
ifstream fin("record.in");
ofstream fout("record.out");
const int N=100005;
struct program
{
	int s,t;
	bool operator<(const program& rhs)const
	{
		return t<rhs.t;
	}
}a[N];
int main()
{
	int n,k;
	fin>>n>>k;
	for(int i=1;i<=n;i++)
		fin>>a[i].s>>a[i].t;
	sort(a+1,a+n+1);
	multiset<int> S;
	for(int i=1;i<=k;i++)
		S.insert(0);
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		multiset<int>::iterator it=S.upper_bound(a[i].s);
		if(it--!=S.begin())
		{
			ans++;
			S.erase(it);
			S.insert(a[i].t);
		}
	}
	fout<<ans<<endl;
	for(int i=1;i<=k;i++)
		fout<<0<<endl;
	return 0;
}
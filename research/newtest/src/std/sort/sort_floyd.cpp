#include<bits/stdc++.h>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
const int N=3005;
bitset<N> mat[N];
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		mat[i][i]=true;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		mat[u][v]=true;
	}
	for(int k=1;k<=n;k++)
		for(int i=1;i<=n;i++)
			if(mat[i][k])
				mat[i]|=mat[k];
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=mat[i].count();
	fout<<n*(n-1)/2-ans+n<<endl;
	return 0;
}
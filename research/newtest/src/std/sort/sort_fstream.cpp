#include<fstream>
#include<vector>
#include<cstdlib>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
const int N=50005,BSN=785;
int bsn;
vector<int> E[N];
unsigned long long mat[N][BSN];
bool vis[N],inS[N];
void dfs(int k)
{
	inS[k]=vis[k]=true;
	mat[k][k/64]|=1ull<<(k&63);
	for(auto u:E[k])
	{
		if(inS[u])
		{
			fout<<-1<<endl;
			exit(0);
		}
		if(!vis[u])
			dfs(u);
		for(int j=0;j<=bsn;j++)
			mat[k][j]|=mat[u][j];
	}
	inS[k]=false;
}
int main()
{
	int n,m;
	fin>>n>>m;
	bsn=n/64;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		E[u].push_back(v);
	}
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=bsn;j++)
			ans+=__builtin_popcountll(mat[i][j]);
	fout<<(unsigned)n*(n-1)/2-ans+n<<endl;
	return 0;
}
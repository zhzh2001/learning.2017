#include<bits/stdc++.h>
using namespace std;
ifstream fin("ranking.in");
ofstream fout("ranking.out");
const int N=50005,M=200005;
int head[N],v[M],nxt[M],e;
bitset<N> vis,mat[N],inS;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
void dfs(int k)
{
	inS[k]=mat[k][k]=vis[k]=true;
	for(int i=head[k];i;i=nxt[i])
	{
		if(inS[v[i]])
			fout<<-1<<endl;
		if(!vis[v[i]])
			dfs(v[i]);
		mat[k]|=mat[v[i]];
	}
	inS[k]=false;
}
int main()
{
	int n,m;
	fin>>n>>m;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
	}
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	int ans=0;
	for(int i=1;i<=n;i++)
		ans+=mat[i].count();
	fout<<(unsigned)n*(n-1)/2-ans+n<<endl;
	return 0;
}
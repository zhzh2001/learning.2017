#include<bits/stdc++.h>
using namespace std;
const int N=50005,BSN=785,BUFSZ=3e6;
char buf[BUFSZ],*p=buf;
inline void read(int& x)
{
	for(;isspace(*p);p++);
	int sign=1;
	if(*p=='-')
		sign=-1,p++;
	x=0;
	for(;isdigit(*p);p++)
		x=x*10+*p-'0';
	x*=sign;
}
int bsn;
vector<int> E[N];
unsigned long long mat[N][BSN];
bool vis[N],inS[N];
FILE *fout;
void dfs(int k)
{
	inS[k]=vis[k]=true;
	mat[k][k/64]|=1ull<<(k&63);
	for(vector<int>::iterator it=E[k].begin();it!=E[k].end();++it)
	{
		if(inS[*it])
		{
			fputs("-1\n",fout);
			exit(0);
		}
		if(!vis[*it])
			dfs(*it);
		for(int j=0;j<=bsn;j++)
			mat[k][j]|=mat[*it][j];
	}
	inS[k]=false;
}
int main()
{
	FILE *fin=fopen("sort.in","r");
	fread(buf,1,BUFSZ,fin);
	fclose(fin);
	int n,m;
	read(n);read(m);
	bsn=n/64;
	while(m--)
	{
		int u,v;
		read(u);read(v);
		E[u].push_back(v);
	}
	fout=fopen("sort.out","w");
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=0;j<=bsn;j++)
			ans+=__builtin_popcountll(mat[i][j]);
	fprintf(fout,"%u\n",(unsigned)n*(n-1)/2-ans+n);
	return 0;
}
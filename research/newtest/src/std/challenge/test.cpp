#include<bits/stdc++.h>
using namespace std;
const int mask=0x7fffffff;
float ll2float(int x)
{
	union
	{
		int ld;
		float ft;
	};
	ld=x;
	return ft;
}
int main()
{
	int n;
	cin>>n;
	minstd_rand gen(time(NULL));
	cout.precision(numeric_limits<float>::digits10);
	int cnt=0;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(numeric_limits<int>::min(),numeric_limits<int>::max());
		int x=d(gen),y=d(gen);
		float dx=ll2float(x),dy=ll2float(y);
		if(x&(1ull<<63))
			x=(x^mask)+1;
		if(y&(1ull<<63))
			y=(y^mask)+1;
		if(isnan(dx)||isnan(dy))
			cnt++;
		else
			if((x<y)^(dx<dy))
				cout<<x<<' '<<y<<' '<<dx<<' '<<dy<<endl;
	}
	cout<<"nan count:"<<cnt<<'/'<<n<<endl;
	return 0;
}
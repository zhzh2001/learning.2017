#include<bits/stdc++.h>
#include "specio.h"
using namespace std;
ifstream fin("challenge.in");
ofstream fout("challenge.out");
int main()
{
	int A,B,X,n;
	fin>>A>>B>>X>>n;
	init(A,B,X);
	float *a=new float [n];
	for(int i=0;i<n;i++)
		a[i]=readNext();
	sort(a,a+n);
	int ans;
	for(int i=0;i<n;i++)
		ans=writeNext(a[i]);
	delete [] a;
	fout<<ans<<endl;
	cout<<clock()<<endl;
	return 0;
}
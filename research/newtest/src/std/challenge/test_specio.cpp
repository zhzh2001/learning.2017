#include<bits/stdc++.h>
#include "specio.h"
using namespace std;
const unsigned rev=0x7fffffffu,sign=0x80000000u;
int main()
{
	int a,b,x,n;
	cin>>a>>b>>x>>n;
	init(a,b,x);
	union
	{
		float ft;
		unsigned ul;
	}u;
	int cinf=0,cninf=0,c0=0,cn0=0;
	for(int i=1;i<=n;i++)
	{
		float dx=readNext(),dy=readNext();
		u.ft=dx;
		unsigned x=u.ul;
		if(x>>31)
			x=(x^rev)+1;
		x^=sign;
		u.ft=dy;
		unsigned y=u.ul;
		if(y>>31)
			y=(y^rev)+1;
		y^=sign;
		stringstream ss;
		ss<<dx;
		if(ss.str()=="inf")
			cinf++;
		if(ss.str()=="-inf")
			cninf++;
		if(ss.str()=="0")
			c0++;
		if(ss.str()=="-0")
			cn0++;
		// cout<<dx<<' '<<dy<<endl;
	}
	cout<<cinf<<' '<<cninf<<' '<<c0<<' '<<cn0<<endl;
	return 0;
}
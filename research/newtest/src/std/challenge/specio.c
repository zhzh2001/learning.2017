#include "specio.h"

int aHelloWorld,bHelloWorld,ansHelloWorld;

union
{
	int x;
	float ft;
}uHelloWorld;

void init(int A,int B,int X)
{
	aHelloWorld=A;
	bHelloWorld=B;
	uHelloWorld.x=X;
}

float readNext(void)
{
	uHelloWorld.x=aHelloWorld*uHelloWorld.x+bHelloWorld;
	int t=uHelloWorld.x;
	if((uHelloWorld.x&0x7f800000)==0x7f800000)
		uHelloWorld.x=0x7f800000;
	if((uHelloWorld.x&0x0f0f0000)==0x0f0f0000)
		uHelloWorld.x=0;
	if((uHelloWorld.x&0x00ff0000)==0x00ff0000)
		uHelloWorld.x=0xff800000;
	float ret=uHelloWorld.ft;
	uHelloWorld.x=t;
	return ret;
}

int writeNext(float FT)
{
	int t=uHelloWorld.x;
	uHelloWorld.ft=FT;
	ansHelloWorld^=uHelloWorld.x+t;
	uHelloWorld.x=t*aHelloWorld+bHelloWorld;
	return ansHelloWorld;
}
#include<fstream>
#include<algorithm>
#include<cstdio>
#include<specio.h>
using namespace std;
ifstream fin("challenge.in");
ofstream fout("challenge.out");

const int BIT=8,K1=1<<BIT;
const unsigned rev=0x7fffffffu,sign=0x80000000u;
int cnt[K1];

const int K2=5e5,BN=40;
int ans;
struct node
{
	FILE *f;
	size_t sz;
	float *buf,*p,*end;
	bool operator>(const node& rhs)const
	{
		return *p>*(rhs.p);
	}
	bool pop()
	{
		ans=writeNext(*p++);
		if(p==end)
		{
			p=buf;
			end=buf+fread(buf,sizeof(float),sz,f);
			if(end==buf)
				return false;
		}
		return true;
	}
}heap[BN];

int main()
{
	int A,B,X,n;
	fin>>A>>B>>X>>n;
	init(A,B,X);
	if(n>2e7)
	{
		union
		{
			unsigned ul;
			float ft;
		}u;
		unsigned *a=new unsigned [n],*b=new unsigned [n];
		for(int i=0;i<n;i++)
		{
			u.ft=readNext();
			a[i]=u.ul;
			if(a[i]>>31)
				a[i]=(a[i]^rev)+1;
			a[i]^=sign;
		}
		unsigned *x=a,*y=b;
		for(int d=0;d<4;d++)
		{
			for(int i=0;i<K1;i++)
				cnt[i]=0;
			int shift=d*BIT;
			for(int i=0;i<n;i++)
				cnt[(x[i]>>shift)&(K1-1)]++;
			for(int i=1;i<K1;i++)
				cnt[i]+=cnt[i-1];
			for(int i=n-1;i>=0;i--)
				y[--cnt[(x[i]>>shift)&(K1-1)]]=x[i];
			swap(x,y);
		}
		int ans;
		for(int i=0;i<n;i++)
		{
			x[i]^=sign;
			if(x[i]>>31)
				x[i]=(x[i]-1)^rev;
			u.ul=x[i];
			ans=writeNext(u.ft);
		}
		delete [] a;delete [] b;
		fout<<ans<<endl;
	}
	else
	{
		int bn=(n+K2-1)/K2;
		FILE **f=new FILE* [bn];
		float *buf=new float [K2];
		
		for(int i=0,t=n;i<bn;i++)
		{
			for(int j=0;j<K2&&j<t;j++)
				buf[j]=readNext();
			sort(buf,buf+min(K2,t));
			f[i]=tmpfile();
			fwrite(buf,sizeof(float),min(K2,t),f[i]);
			t-=K2;
		}
		delete [] buf;
		
		for(int i=0;i<bn;i++)
		{
			rewind(f[i]);
			heap[i].f=f[i];
			heap[i].sz=K2/bn;
			heap[i].p=heap[i].buf=new float [heap[i].sz];
			heap[i].end=heap[i].buf+fread(heap[i].buf,sizeof(float),heap[i].sz,f[i]);
		}
		delete [] f;
		make_heap(heap,heap+bn,greater<node>());
		
		int cc=bn;
		while(cc)
		{
			pop_heap(heap,heap+cc,greater<node>());
			cc--;
			if(heap[cc].pop())
			{
				cc++;
				push_heap(heap,heap+cc,greater<node>());
			}
			else
				delete [] heap[cc].buf;
		}
		fout<<ans<<endl;
	}
	return 0;
}
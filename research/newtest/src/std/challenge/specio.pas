unit specio;
{$Q-}

interface

procedure init(a,b,x:longint);

function readNext:single;

function writeNext(ft:single):longint;

implementation

type
  union=record
  case boolean of
  false:(x:longint);
  true:(ft:single);
  end;

var
  aHelloWorld,bHelloWorld,ansHelloWorld:longint;
  uHelloWorld:union;
  
procedure init(a,b,x:longint);
begin
  aHelloWorld:=a;
  bHelloWorld:=b;
  uHelloWorld.x:=x;
end;

function readNext:single;
var
  t:longint;
begin
  uHelloWorld.x:=aHelloWorld*uHelloWorld.x+bHelloWorld;
  t:=uHelloWorld.x;
  if uHelloWorld.x and $7f800000=$7f800000 then
    uHelloWorld.x:=$7f800000;
  //inf
  if uHelloWorld.x and $0f0f0000=$0f0f0000 then
    uHelloWorld.x:=0;
  //+0
  if uHelloWorld.x and $00ff0000=$00ff0000 then
    uHelloWorld.x:=$ff800000;
  //-inf
  readNext:=uHelloWorld.ft;
  uHelloWorld.x:=t;
end;

function writeNext(ft:single):longint;
var
  t:longint;
begin
  t:=uHelloWorld.x;
  uHelloWorld.ft:=ft;
  ansHelloWorld:=ansHelloWorld xor (uHelloWorld.x+t);
  uHelloWorld.x:=t*aHelloWorld+bHelloWorld;
  writeNext:=ansHelloWorld;
end;

end.
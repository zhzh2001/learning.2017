#include<fstream>
#include<cstdio>
#include<algorithm>
#include<queue>
#include "specio.h"
using namespace std;
ifstream fin("challenge.in");
ofstream fout("challenge.out");
const int K=5e5,BN=20;
struct node
{
	FILE *f;
	size_t sz;
	float *buf,*p,*end;
	bool operator>(const node& rhs)const
	{
		return *p>*(rhs.p);
	}
	bool pop()
	{
		writeNext(*p++);
		if(p==end)
		{
			p=buf;
			end=buf+fread(buf,sizeof(float),sz,f);
			if(end==buf)
				return false;
		}
		return true;
	}
}heap[BN];
int main()
{
	long long a,b,x;
	int n;
	fin>>a>>b>>x>>n;
	init(a,b,x);
	
	int bn=(n+K-1)/K;
	FILE **f=new FILE* [bn];
	float *buf=new float [K];
	
	for(int i=0,t=n;i<bn;i++)
	{
		for(int j=0;j<K&&j<t;j++)
			buf[j]=readNext();
		sort(buf,buf+min(K,t));
		f[i]=tmpfile();
		fwrite(buf,sizeof(float),min(K,t),f[i]);
		t-=K;
	}
	delete [] buf;
	
	for(int i=0;i<bn;i++)
	{
		rewind(f[i]);
		heap[i].f=f[i];
		heap[i].sz=K/bn;
		heap[i].p=heap[i].buf=new float [heap[i].sz];
		heap[i].end=heap[i].buf+fread(heap[i].buf,sizeof(float),heap[i].sz,f[i]);
	}
	delete [] f;
	make_heap(heap,heap+bn,greater<node>());
	
	int cc=bn;
	while(cc)
	{
		pop_heap(heap,heap+cc,greater<node>());
		cc--;
		if(heap[cc].pop())
		{
			cc++;
			push_heap(heap,heap+cc,greater<node>());
		}
		else
			delete [] heap[cc].buf;
	}
	extern int ansHelloWorld;
	fout<<ansHelloWorld<<endl;
	return 0;
}
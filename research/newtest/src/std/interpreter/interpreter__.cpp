#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cassert>
#include <queue>
#include <memory.h>
#include <sstream>
#include <map>
#include<ctime>
using namespace std;

const int M = 105, mod = 1e9 + 7;

typedef long long ll;

int tot, Val[M];

class VarList {
public:
    VarList() {}
    int Hash(string var) {
        int & res = var_list[var];
        if (!res) return res = ++tot;
        return res;
    }
    
public:
    map<string, int> var_list;
} Var_List;

int counter,t;

class Matrix {
public:
    int a[M][M];
    
public:
    Matrix() {}
    
    Matrix(int val) {
        memset(a, 0, sizeof a);
        for (int i = 0; i <= tot; i++)
            a[i][i] = val;
    }
    
    void init(int val) {
        memset(a, 0, sizeof a);
        for (int i = 0; i <= tot; i++)
            a[i][i] = val;
    }
    
public:
    int* operator [] (int i) { return a[i]; }
    
    friend Matrix operator * (Matrix x, Matrix y) {
		counter++;
		clock_t s=clock();
        Matrix res(0);
        for (int i = 0; i <= tot; i++)
            for (int j = 0; j <= tot; j++)
                for (int k = 0; k <= tot; k++)
                    (res[i][j] += (ll)x[i][k] * y[k][j] % mod) %= mod;
		t+=clock()-s;
        return res;
    }
    
    Matrix& operator *= (const Matrix & x) { return *this = *this * x; }
    
public:
    friend Matrix pow(Matrix x, ll y) {
        Matrix res(1);
        for (; y; y >>= 1) {
            if (y & 1) res *= x;
            x *= x;
        }
        return res;
    }
} mat[M];

class Stack {
public:
    int stack[M], tot;
    Stack() { tot = 0; }
    
public:
    void push(int val) { stack[++tot] = val; mat[tot].init(1); }
    void pop() { --tot; }
    int at(int x) { return stack[x]; }
    int top() { return stack[tot]; }
} st;

const char * trim(string str) {
    int len = str.length();
    int l = 0, r = len - 1;
    while (str[l] == ' ') l++;
    while (str[r] == ' ') r--;
    return str.substr(l, r - l + 1).c_str();
}

int main() {
#ifndef ONLINE_JUDGE
    freopen("interpreter.in", "r", stdin);
    freopen("interpreter.out", "w", stdout);
#endif
    static char str[400];
    mat[0].init(1);
    while (true) {
        gets(str);
        stringstream ss(str);
        ss >> str;
        if (islower(str[0])) {
            memset(Val, 0, sizeof Val);
            int label = Var_List.Hash(trim(str));
            while (ss >> str) {
                if (islower(str[0])) {
                    int val = Var_List.Hash(trim(str));
                    for (int i = 0; i <= tot; i++)
                        (Val[i] += mat[st.tot][val][i]) %= mod;
                } else if (isdigit(str[0])) {
                    int val = atoi(trim(str));
                    (Val[0] += val) %= mod;
                }
            }
            for (int i = 0; i <= tot; i++) mat[st.tot][label][i] = Val[i];
        } 
        else if (isdigit(str[0])) st.push(atoi(trim(str))); 
        else if (str[0] == '}') mat[st.tot - 1] = pow(mat[st.tot], st.top()) * mat[st.tot - 1], st.pop();
        else if (str[0] == 'P') {
            ss >> str;
            int val = Var_List.Hash(trim(str)), ans = mat[0][val][0];
            printf("%d\n", ans);
			cerr<<counter<<' '<<t<<endl;
            return 0;
        }
    }
    return 0;
}

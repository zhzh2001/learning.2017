#include<fstream>
#include<sstream>
#include<string>
#include<cstring>
#include<map>
#include<cctype>
#include<cstdlib>
using namespace std;
const int N=105,MOD=1e9+7;
ifstream fin("interpreter.in");
ofstream fout("interpreter.out");
int n,cnt[N];
struct matrix
{
	int mat[N][N];
	matrix()
	{
		memset(mat,0,sizeof(mat));
	}
	matrix operator*(const matrix& rhs)const
	{
		matrix ans;
		for(int i=1;i<=n;i++)
			for(int k=1;k<=n;k++)
				for(int j=1;j<=n;j++)
					ans.mat[i][j]=(ans.mat[i][j]+(long long)mat[i][k]*rhs.mat[k][j])%MOD;
		return ans;
	}
	matrix& operator*=(const matrix& rhs)
	{
		return *this=*this*rhs;
	}
}S[N];
matrix I()
{
	matrix ans;
	for(int i=1;i<=n;i++)
		ans.mat[i][i]=1;
	return ans;
}
matrix qpow(matrix a,int b)
{
	matrix ans=I();
	do
	{
		if(b&1)
			ans*=a;
		a*=a;
	}
	while(b/=2);
	return ans;
}
int tmp[N];
int main()
{
	map<string,int> var;
	n=1;
	int sp=1;
	S[sp]=I();
	string line;
	while(getline(fin,line))
	{
		stringstream ss(line);
		string token;
		ss>>token;
		if(token=="PRINT")
		{
			string varname;
			ss>>varname;
			fout<<S[sp].mat[var[varname]][1]<<endl;
		}
		else
			if(isdigit(token[0]))
			{
				S[++sp]=I();
				cnt[sp]=atoi(token.c_str());
			}
			else
				if(token=="}")
				{
					S[sp-1]=qpow(S[sp],cnt[sp])*S[sp-1];
					sp--;
				}
				else
				{
					memset(tmp,0,sizeof(tmp));
					if(!var[token])
						var[token]=++n;
					int id=var[token];
					while(ss>>token)
					{
						if(isdigit(token[0]))
							tmp[1]+=atoi(token.c_str());
						else
							if(isalpha(token[0]))
								for(int i=1;i<=n;i++)
									tmp[i]=(tmp[i]+S[sp].mat[var[token]][i])%MOD;
					}
					memcpy(S[sp].mat[id],tmp,sizeof(tmp));
				}
	}
	return 0;
}
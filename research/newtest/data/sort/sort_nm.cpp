#include<fstream>
#include<vector>
#include<cstdlib>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
const int N=5005;
int n;
vector<int> E[N];
bool vis[N],inS[N],mat[N][N];
void dfs(int k)
{
	inS[k]=vis[k]=true;
	mat[k][k]=true;
	for(auto u:E[k])
	{
		if(inS[u])
		{
			fout<<-1<<endl;
			/* fout<<u;
			while(S.top()!=u)
			{
				fout<<' '<<S.top();
				S.pop();
			} */
			exit(0);
		}
		if(!vis[u])
			dfs(u);
		for(int j=1;j<=n;j++)
			mat[k][j]|=mat[u][j];
	}
	inS[k]=false;
}
int main()
{
	int m;
	fin>>n>>m;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		E[u].push_back(v);
	}
	for(int i=1;i<=n;i++)
		if(!vis[i])
			dfs(i);
	int ans=0;
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j++)
			ans+=mat[i][j];
	fout<<(unsigned)n*(n-1)/2-ans+n<<endl;
	return 0;
}
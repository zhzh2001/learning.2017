#include<bits/stdc++.h>
using namespace std;
ofstream fout("sort.in");
const int n=50000,m=200000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=m;)
	{
		uniform_int_distribution<> dn(1,min(i+5,n));
		int u=dn(gen),v=dn(gen);
		if(u>v)
			swap(u,v);
		if(u!=v)
		{
			fout<<u<<' '<<v<<endl;
			i++;
		}
	}
	return 0;
}
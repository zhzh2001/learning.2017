#include<bits/stdc++.h>
using namespace std;
ofstream fout("record.in");
const int n=100000,k=17521,t=500000000,LEN=500000000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<k<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(0,t);
		uniform_int_distribution<> dl(0,LEN);
		int s=d(gen),len=dl(gen);
		fout<<s<<' '<<s+len<<endl;
	}
	return 0;
}
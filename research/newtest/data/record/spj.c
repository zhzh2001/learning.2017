#include<stdio.h>
#include<stdlib.h>

#define N 100005
char *In,*Ans,*Out,*Log;
FILE *fin,*fans,*fout,*flog;
int s[N],t[N],vis[N];

void openFiles(void);
void work(void);
void end(const char* msg,double score);

int main(int argc,char* argv[])
{
	if(argc<4)
		return 1;
	In=argv[1];
	Ans=argv[2];
	Out=argv[3];
	Log=argc>4?argv[4]:NULL;
	
	openFiles();
	work();
	
	return 0;
}

void openFiles(void)
{
	if(!(fin=fopen(In,"r")))
		exit(1);
	if(!(fans=fopen(Ans,"r")))
		exit(1);
	if(!(fout=fopen(Out,"r")))
		exit(1);
	if(!(flog=Log?fopen(Log,"w"):stdout))
		exit(1);
}

void work(void)
{
	int out,ans,n,k,i,cnt;
	
	fscanf(fout,"%d",&out);
	fscanf(fans,"%d",&ans);
	if(out!=ans)
		end("答案错误",.0);
	
	fscanf(fin,"%d%d",&n,&k);
	for(i=1;i<=n;i++)
		fscanf(fin,"%d%d",s+i,t+i);
	
	cnt=0;
	for(i=1;i<=k;i++)
	{
		int cc,pred;
		fscanf(fout,"%d",&cc);
		if(cc<0||(cnt+=cc)>ans)
			end("可行解错误",.5);
		
		pred=0;
		while(cc--)
		{
			int id;
			fscanf(fout,"%d",&id);
			if(id<1||id>n||vis[id])
				end("编号错误",.5);
			vis[id]=1;
			
			if(s[id]<t[pred])
				end("可行解错误",.5);
			pred=id;
		}
	}
	
	if(cnt==ans)
		end("正确",1.0);
	else
		end("可行解错误",.5);
}

void end(const char* msg,double score)
{
	fprintf(flog,"%.3lf\n%s\n",score,msg);
	exit(0);
}
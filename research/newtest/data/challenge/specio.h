#ifndef __SPECIAL_IO_INCLUDED
#define __SPECIAL_IO_INCLUDED
void init(int,int,int);
float readNext(void);
int writeNext(float);
#endif
def calc(s):
    sum = 1
    for c in s:
        sum = sum * (ord(c) - ord('A') + 1) % 47
    return sum


if calc(input()) == calc(input()):
    print('GO')
else:
    print('STAY')

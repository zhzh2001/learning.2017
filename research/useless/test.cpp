#include <bits/stdc++.h>
using namespace std;
const int N = 2000005;
//注意是2*n
int mp[N], p[N / 10], cnt[N], r;
//mp[]表示每个数最小的质因数，p[]表示质数表，cnt[]用于计算指数，r为取模数
int qpow(int a, int b)
//快速幂：计算a^b%r
{
	int ans = 1;
	do
	{
		if (b & 1)
			ans = (long long)ans * a % r;
		a = (long long)a * a % r;
	} while (b /= 2);
	return ans;
}
int main()
{
	cout << (int)numeric_limits<char>::min() << ' ' << (int)numeric_limits<char>::max() << endl;
	int n;
	cin >> n >> r;
	int pn = 0;
	for (int i = 2; i <= 2 * n; i++)
	{
		if (!mp[i])
		{
			p[++pn] = i;
			mp[i] = i;
		}
		for (int j = 1; j <= pn && i * p[j] <= 2 * n; j++)
		{
			mp[i * p[j]] = p[j];
			if (i % p[j] == 0)
				break;
		}
	}
	//欧拉线性筛法
	for (int i = 1; i <= n; i++)
		cnt[i] = -1;
	//需要除以分母
	for (int i = n + 2; i <= 2 * n; i++)
		cnt[i] = 1;
	//乘以分子
	for (int i = 2 * n; i > 1; i--)
		if (mp[i] < i)
		//如果是合数，向下传递，可以保证O(n)
		{
			cnt[mp[i]] += cnt[i];
			cnt[i / mp[i]] += cnt[i];
		}
	int ans = 1;
	for (int i = 2; i <= 2 * n; i++)
		if (mp[i] == i)
			//如果是质数计入答案，合数已经处理过了
			ans = (long long)ans * qpow(i, cnt[i]) % r;
	//防止中间过程溢出
	cout << ans << endl;
	return 0;
}
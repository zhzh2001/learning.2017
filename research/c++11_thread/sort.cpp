#include<bits/stdc++.h>
#include<parallel/algorithm>
using namespace std;
int main()
{
	int n;
	cin>>n;
#ifdef __WIN32
	short* a=new short [n],*b=new short [n];
#else
	int* a=new int [n],*b=new int [n];
#endif

	srand(time(NULL));
	generate(a,a+n,rand);
	cout<<"generated\n";

#ifdef __WIN32
	memcpy(b,a,n*sizeof(short));
#else
	memcpy(b,a,n*sizeof(int));
#endif
	cout<<"copyed\n";

	clock_t t=clock();
	sort(b,b+n);
	cout<<"using sort():"<<clock()-t<<endl;

#ifdef __WIN32
	memcpy(b,a,n*sizeof(short));
#else
	memcpy(b,a,n*sizeof(int));
#endif
	cout<<"copyed\n";

	t=clock();
	/*C++17
	sort(execution::par,a,a+n);
	cout<<"using sort(par):"<<clock()-t<<endl;
	*/
	__gnu_parallel::sort(a,a+n);
	cout<<"using __gnu_parallel::sort():"<<clock()-t<<endl;

	delete [] a;
	return 0;
}
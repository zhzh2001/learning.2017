#include<bits/stdc++.h>
#include<windows.h>
using namespace std;
bool flag=false;
int pn;
void gen_prime(int n,int res[])
{
	int* p=new int [n/32+1];
	memset(p,0,n/8);
	pn=0;
	int i;
	for(i=2;i*i<=n;i++)
		if(!(p[i/32]&(1<<(i&31))))
		{
			res[++pn]=i;
			for(int j=i*i;j<=n;j+=i)
				p[j/32]|=1<<(j&31);
		}
	for(;i<=n;i++)
		if(!(p[i/32]&(1<<(i&31))))
			res[++pn]=i;
	delete p;
	flag=true;
}
void show_progress(int n)
{
	cout<<fixed;
	while(!flag)
	{
		cout<<"\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
		cout.precision(2);
		if(pn*pn<n)
			cout<<pn/sqrt(n)*100<<'%';
		else
			cout<<50.0+50.0*pn/n<<'%';
		Sleep(1);
	}
}
int main()
{
	int n;
	cin>>n;
	int* p;
	if(n>10000)
		p=new int [n/10];
	else
		p=new int [n];
	thread gen(gen_prime,n,p);
	thread watch(show_progress,n);
	gen.join();
	watch.join();
	/*
	for(int i=1;i<=pn;i++)
		cout<<p[i]<<' ';
	cout<<endl;
	*/
	delete p;
	return 0;
}
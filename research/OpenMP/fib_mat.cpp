#include<iostream>
#include<cmath>
#include<ctime>
using namespace std;
struct bigfloat
{
	static constexpr double LIMIT=1e150;
	static const int DIGITS=150;
	double d;
	long long overcnt;
	bigfloat()
	{
		d=overcnt=0;
	}
	bigfloat(double x,long long oc=0):d(x),overcnt(oc){};
	inline void adjust()
	{
		if(d>LIMIT)
		{
			overcnt++;
			d/=LIMIT;
		}
	}
	bigfloat operator+(const bigfloat b)const
	{
		if(overcnt>b.overcnt)
			return *this;
		if(overcnt<b.overcnt)
			return b;
		bigfloat res(d+b.d,overcnt);
		res.adjust();
		return res;
	}
	bigfloat operator+=(const bigfloat b)
	{
		*this=*this+b;
		return *this;
	}
	bigfloat operator*(const bigfloat b)const
	{
		bigfloat res(d*b.d,overcnt+b.overcnt);
		res.adjust();
		return res;
	}
};
ostream& operator<<(ostream& os,const bigfloat b)
{
	os<<(long long)floor(log10(b.d))+1+b.overcnt*bigfloat::DIGITS;
	return os;
}
struct matrix
{
	static const int n=2;
	bigfloat mat[n][n];
	matrix()
	{
		mat[0][0]=mat[0][1]=mat[1][0]=mat[1][1]=0;
	}
	matrix(double a,double b,double c,double d)
	{
		mat[0][0]=a;
		mat[0][1]=b;
		mat[1][0]=c;
		mat[1][1]=d;
	}
	matrix operator*(const matrix& b)const
	{
		matrix res;
		for(int i=0;i<n;i++)
			for(int j=0;j<n;j++)
				for(int k=0;k<n;k++)
					res.mat[i][j]+=mat[i][k]*b.mat[k][j];
		return res;
	}
	matrix operator*=(const matrix& b)
	{
		*this=*this*b;
		return *this;
	}
	matrix pow(long long b)const
	{
		matrix res(1,0,0,1),a=*this;
		do
		{
			if(b&1)
				res*=a;
			a*=a;
		}
		while(b>>=1);
		return res;
	}
};
int main()
{
	long long n;
	cin>>n;
	time_t start=clock();
	#pragma omp parallel for
	for(int i=1;i<=1000000;i++)
		matrix(1,1,1,0).pow(n).mat[0][1];
	cout<<(double)(clock()-start)/CLOCKS_PER_SEC<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int main()
{
	cout<<"integer types\n";
	cout<<"type\t\t\tmin\t\t\tmax\t\t\tsize\n";
	cout<<"int\t\t\t"<<numeric_limits<int>::min()<<"\t\t"<<numeric_limits<int>::max()<<"\t\t"<<sizeof(int)<<endl;
	cout<<"unsigned int\t\t"<<numeric_limits<unsigned int>::min()<<"\t\t\t"<<numeric_limits<unsigned int>::max()<<"\t\t"<<sizeof(unsigned int)<<endl;
	cout<<"long\t\t\t"<<numeric_limits<long>::min()<<"\t\t"<<numeric_limits<long>::max()<<"\t\t"<<sizeof(long)<<endl;
	cout<<"unsigned long\t\t"<<numeric_limits<unsigned long>::min()<<"\t\t\t"<<numeric_limits<unsigned long>::max()<<"\t\t"<<sizeof(unsigned long)<<endl;
	cout<<"short\t\t\t"<<numeric_limits<short>::min()<<"\t\t\t"<<numeric_limits<short>::max()<<"\t\t\t"<<sizeof(short)<<endl;
	cout<<"unsigned short\t\t"<<numeric_limits<unsigned short>::min()<<"\t\t\t"<<numeric_limits<unsigned short>::max()<<"\t\t\t"<<sizeof(unsigned short)<<endl;
	cout<<"long long\t\t"<<numeric_limits<long long>::min()<<"\t"<<numeric_limits<long long>::max()<<"\t"<<sizeof(long long)<<endl;
	cout<<"unsigned long long\t"<<numeric_limits<unsigned long long>::min()<<"\t\t\t"<<numeric_limits<unsigned long long>::max()<<"\t"<<sizeof(unsigned long long)<<endl;
	cout<<"char\t\t\t"<<(int)numeric_limits<char>::min()<<"\t\t\t"<<(int)numeric_limits<char>::max()<<"\t\t\t"<<sizeof(char)<<endl;
	cout<<"unsigned char\t\t"<<(int)numeric_limits<unsigned char>::min()<<"\t\t\t"<<(int)numeric_limits<unsigned char>::max()<<"\t\t\t"<<sizeof(unsigned char)<<endl;
	cout<<"bool\t\t\t"<<numeric_limits<bool>::min()<<"\t\t\t"<<numeric_limits<bool>::max()<<"\t\t\t"<<sizeof(bool)<<endl;
	cout<<"float types\n";
	cout<<"type\t\tmin\t\tmax\t\tdigits\teps\t\tsize\n";
	cout<<"float\t\t"<<numeric_limits<float>::min()<<"\t"<<numeric_limits<float>::max()<<"\t"<<numeric_limits<float>::digits10<<"\t"<<numeric_limits<float>::epsilon()<<"\t"<<sizeof(float)<<endl;
	cout<<"double\t\t"<<numeric_limits<double>::min()<<"\t"<<numeric_limits<double>::max()<<"\t"<<numeric_limits<double>::digits10<<"\t"<<numeric_limits<double>::epsilon()<<"\t"<<sizeof(double)<<endl;
	cout<<"long double\t"<<numeric_limits<long double>::min()<<"\t"<<numeric_limits<long double>::max()<<"\t"<<numeric_limits<long double>::digits10<<"\t"<<numeric_limits<long double>::epsilon()<<"\t"<<sizeof(long double)<<endl;
	return 0;
}
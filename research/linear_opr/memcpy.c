#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>

int rand32p();

int check_result(int *a,int n);

//usage:memcpy size
int main(int argc,char* argv[])
{
	srand(time(NULL));
	if(argc<=1)
		return 1;
	int n;
	sscanf(argv[1],"%d",&n);
	
	#define MEM_SIZE (n*sizeof(int))
	#define STR_SIZE ((n+1)*sizeof(int))
	
	int* a=malloc(STR_SIZE);
	#define chk(x) if(x==NULL)\
	{\
		fprintf(stderr,"Fail to allocate "#x"\n");\
		exit(1);\
	}
	chk(a);
	for(int i=0;i<n;i++)
		a[i]=rand32p();
	a[n]=0;
	printf("correct checksum=%d\n",check_result(a,n));
	
	int* b=malloc(MEM_SIZE);
	time_t t=clock();
	for(int i=0;i<n;i++)
		b[i]=a[i];
	
	#ifdef __WIN32
	#define CLOCK_FMT "%ld"
	#define CLOCK_T "ms"
	#endif
	
	#ifdef __linux
	#define CLOCK_FMT "%ld"
	#define CLOCK_T "us"
	#endif
	
	printf("using for "CLOCK_FMT CLOCK_T"\n",clock()-t);
	#define chkres printf("result=%d\n",check_result(b,n));
	chkres;
	free(b);
	
	b=malloc(MEM_SIZE);
	t=clock();
	memcpy(b,a,MEM_SIZE);
	printf("using memcpy "CLOCK_FMT CLOCK_T"\n",clock()-t);
	chkres;
	free(b);
	
	b=malloc(STR_SIZE);
	t=clock();
	strcpy((char *)b,(char *)a);
	printf("using strcpy "CLOCK_FMT CLOCK_T"\n",clock()-t);
	chkres;
	free(b);
	
	b=malloc(MEM_SIZE);
	t=clock();
	__builtin_memcpy(b,a,MEM_SIZE);
	printf("using __builtin_memcpy "CLOCK_FMT CLOCK_T"\n",clock()-t);
	chkres;
	free(b);
	
	return 0;
}

int rand32p()
{
	unsigned a=rand()%255+1,b=rand()%255+1,c=rand()%255+1,d=rand()%255+1;
	return (a<<24)+(b<<16)+(c<<8)+d;
}

int check_result(int *a,int n)
{
	int ans=0;
	for(int i=0;i<n;i++)
		ans^=a[i];
	return ans;
}
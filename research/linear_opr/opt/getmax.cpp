#include<bits/stdc++.h>

using namespace std;

typedef unsigned long long ull;

inline void asm_clock(ull& t);

inline int rand32();

namespace opt0
{
	int get_max(int* a,int len);
};

namespace opt1
{
	int get_max(int* a,int len);
};

namespace opt2
{
	int get_max(int* a,int len);
};

namespace opt3
{
	int get_max(int* a,int len);
};

namespace opt4
{
	int get_max(int* a,int len);
};

namespace opt5
{
	int get_max(int* a,int len);
};

namespace opt6
{
	int get_max(int* a,int len);
};

int main()
{
	int n;
	cin>>n;
	int* a=new int [n];
	generate(a,a+n,rand32);
	
	ull t1,t2;
	asm_clock(t1);
	opt0::get_max(a,n);
	asm_clock(t2);
	cout<<"opt0:"<<(double)(t2-t1)/n<<endl;
	
	asm_clock(t1);
	opt1::get_max(a,n);
	asm_clock(t2);
	cout<<"opt1:"<<(double)(t2-t1)/n<<endl;
	
	asm_clock(t1);
	opt2::get_max(a,n);
	asm_clock(t2);
	cout<<"opt2:"<<(double)(t2-t1)/n<<endl;
	
	asm_clock(t1);
	opt3::get_max(a,n);
	asm_clock(t2);
	cout<<"opt3:"<<(double)(t2-t1)/n<<endl;
	
	asm_clock(t1);
	opt4::get_max(a,n);
	asm_clock(t2);
	cout<<"opt4:"<<(double)(t2-t1)/n<<endl;
	
	asm_clock(t1);
	opt5::get_max(a,n);
	asm_clock(t2);
	cout<<"opt5:"<<(double)(t2-t1)/n<<endl;
	
	asm_clock(t1);
	opt6::get_max(a,n);
	asm_clock(t2);
	cout<<"opt6:"<<(double)(t2-t1)/n<<endl;
	
	return 0;
}

inline void asm_clock(ull& t)
{
	__asm__ __volatile__ ("rdtsc\n\t":"=A"(t):);
}

inline int rand32()
{
	if(RAND_MAX==32767)
		return rand()<<15+rand();
	else
		return rand();
}

//无优化
int opt0::get_max(int* a,int len)
{
	int ans=0;
	for(int i=0;i<len;i++)
		if(a[i]>ans)
			ans=a[i];
	return ans;
}

//避免重新寻址
int opt1::get_max(int* a,int len)
{
	int ans=0,*end=a+len;
	for(;a!=end;a++)
		if(*a>ans)
			ans=*a;
	return ans;
}

//循环展开
int opt2::get_max(int* a,int len)
{
	assert(len%8==0);
	#define D(x) mx##x=0
	int D(0),D(1),D(2),D(3),D(4),D(5),D(6),D(7),*end=a+len;
	#define CMP(x) if(*(a+x)>mx##x)mx##x=*(a+x);
	while(a!=end)
	{
		CMP(0);CMP(1);
		CMP(2);CMP(3);
		CMP(4);CMP(5);
		CMP(6);CMP(7);
		a+=8; 
	}
	#define CC(x1,x2) if(mx##x1>mx##x2)mx##x2=mx##x1;
	CC(1,0);CC(3,2);
	CC(5,4);CC(7,6);
	CC(2,0);CC(6,4);
	CC(4,0);
	return mx0;
}

//改用汇编
int opt3::get_max(int* a,int len)
{
	int ans;
	__asm__ __volatile__
	(
		"movl $0, %%eax\n\t"
		".p2align 4,,15\n"
		"LP1:\n\t"
		"cmpl -4(%1,%2,4), %%eax\n\t"
		"jge ED\n\t"
		"movl -4(%1,%2,4), %%eax\n"
		"ED:\n\t"
		//"loop LP1\n\t"	CISC
		"decl %2\n\t"
		"jnz LP1\n\t"
		"movl %%eax, %0\n\t"
		:"=m"(ans)
		:"r"(a),"c"(len)
		:"%eax"
	);
	return ans;
}

//循环展开
int opt4::get_max(int* a,int len)
{
	assert(len%2==0);
	int ans;
	__asm__ __volatile__
	(
		"movl $0, %%eax\n\t"
		"movl $0, %%edx\n\t"
		".p2align 4,,15\n"
		"LP2:\n\t"
		"cmpl (%1), %%eax\n\t"
		"jge ED2\n\t"
		"movl (%1), %%eax\n"
		"ED2:\n\t"
		"cmpl 4(%1), %%edx\n\t"
		"jge ED3\n\t"
		"movl 4(%1), %%edx\n"
		"ED3:\n\t"
		"addl $8, %1\n\t"
		"subl $2, %2\n\t"
		"jnz LP2\n\t"
		"cmpl %%edx, %%eax\n\t"
		"cmovll %%edx, %%eax\n\t"
		"movl %%eax, %0\n\t"
		:"=m"(ans)
		:"r"(a),"r"(len)
		:"%eax","%edx"
	);
	return ans;
}

//使用SSE2
int opt5::get_max(int* a,int len)
{
	assert(len%4==0);
	//assert(sse2);
	int ans,tmp[4];
	__asm__ __volatile__
	(
		"\txorps %%xmm0, %%xmm0\n"
		"LP3:\n"
		"\tmovdqa %%xmm0, %%xmm1\n"
		"\tpcmpgtd (%1), %%xmm1\n"
		"\tandps %%xmm1, %%xmm0\n"
		"\tandnps (%1), %%xmm1\n"
		"\torps %%xmm1, %%xmm0\n"
		"\taddl $16, %1\n"
		"\tsubl $4, %2\n"
		"\tjnz LP3\n"
		"\tmovdqu %%xmm0, (%3)\n"
		"\tmovl (%3), %%eax\n"
		"\tcmpl 4(%3), %%eax\n"
		"\tcmovll 4(%3), %%eax\n"
		"\tcmpl 8(%3), %%eax\n"
		"\tcmovll 8(%3), %%eax\n"
		"\tcmpl 12(%3), %%eax\n"
		"\tcmovll 12(%3), %%eax\n"
		"\tmovl %%eax, %0\n"
		:"=m"(ans)
		:"r"(a),"r"(len),"r"(tmp)
		:"%eax"
	);
	return ans;
}

//使用SSE4
int opt6::get_max(int* a,int len)
{
	assert(len%4==0);
	//assert(sse4);
	int ans,tmp[4];
	__asm__ __volatile__
	(
		"\txorps %%xmm0, %%xmm0\n"
		"LP4:\n"
		"\tpmaxsd (%1), %%xmm0\n"
		"\taddl $16, %1\n"
		"\tsubl $4, %2\n"
		"\tjnz LP4\n"
		"\tmovdqu %%xmm0, (%3)\n"
		"\tmovl (%3), %%eax\n"
		"\tcmpl 4(%3), %%eax\n"
		"\tcmovll 4(%3), %%eax\n"
		"\tcmpl 8(%3), %%eax\n"
		"\tcmovll 8(%3), %%eax\n"
		"\tcmpl 12(%3), %%eax\n"
		"\tcmovll 12(%3), %%eax\n"
		"\tmovl %%eax, %0\n"
		:"=m"(ans)
		:"r"(a),"r"(len),"r"(tmp)
		:"%eax"
	);
	return ans;
}
	.file	"loop_expand.cpp"
	.text
	.p2align 4,,15
	.def	___tcf_0;	.scl	3;	.type	32;	.endef
___tcf_0:
LFB3677:
	.cfi_startproc
	movl	$__ZStL8__ioinit, %ecx
	jmp	__ZNSt8ios_base4InitD1Ev
	.cfi_endproc
LFE3677:
	.p2align 4,,15
	.globl	__Z9no_expandPii
	.def	__Z9no_expandPii;	.scl	2;	.type	32;	.endef
__Z9no_expandPii:
LFB3639:
	.cfi_startproc
	movl	8(%esp), %eax
	testl	%eax, %eax
	jle	L5
	movl	4(%esp), %edx
	leal	(%edx,%eax,4), %ecx
	xorl	%eax, %eax
	.p2align 4,,7
L4:
	addl	(%edx), %eax
	addl	$4, %edx
	cmpl	%ecx, %edx
	jne	L4
	rep ret
L5:
	xorl	%eax, %eax
	ret
	.cfi_endproc
LFE3639:
	.p2align 4,,15
	.globl	__Z7expand2Pii
	.def	__Z7expand2Pii;	.scl	2;	.type	32;	.endef
__Z7expand2Pii:
LFB3640:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	movl	24(%esp), %ebx
	movl	20(%esp), %esi
	cmpl	$1, %ebx
	jle	L13
	leal	-2(%ebx), %ebp
	movl	%esi, %edx
	shrl	%ebp
	xorl	%eax, %eax
	leal	8(%esi,%ebp,8), %edi
	xorl	%ecx, %ecx
	.p2align 4,,7
L10:
	addl	(%edx), %ecx
	addl	$8, %edx
	addl	-4(%edx), %eax
	cmpl	%edi, %edx
	jne	L10
	leal	2(%ebp,%ebp), %edx
	addl	%eax, %ecx
L8:
	xorl	%eax, %eax
	cmpl	%edx, %ebx
	jle	L11
	.p2align 4,,7
L12:
	addl	(%esi,%edx,4), %eax
	addl	$1, %edx
	cmpl	%ebx, %edx
	jne	L12
L11:
	popl	%ebx
	.cfi_remember_state
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	addl	%ecx, %eax
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L13:
	.cfi_restore_state
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	jmp	L8
	.cfi_endproc
LFE3640:
	.p2align 4,,15
	.globl	__Z7expand3Pii
	.def	__Z7expand3Pii;	.scl	2;	.type	32;	.endef
__Z7expand3Pii:
LFB3641:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	movl	24(%esp), %ebx
	movl	20(%esp), %ecx
	cmpl	$2, %ebx
	jle	L22
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align 4,,7
L19:
	addl	(%ecx,%edx,4), %edi
	addl	4(%ecx,%edx,4), %esi
	addl	8(%ecx,%edx,4), %eax
	addl	$3, %edx
	leal	2(%edx), %ebp
	cmpl	%ebp, %ebx
	jg	L19
	addl	%edi, %esi
	addl	%eax, %esi
L17:
	xorl	%eax, %eax
	cmpl	%edx, %ebx
	jle	L20
	.p2align 4,,7
L21:
	addl	(%ecx,%edx,4), %eax
	addl	$1, %edx
	cmpl	%ebx, %edx
	jne	L21
L20:
	popl	%ebx
	.cfi_remember_state
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	addl	%esi, %eax
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L22:
	.cfi_restore_state
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmp	L17
	.cfi_endproc
LFE3641:
	.p2align 4,,15
	.globl	__Z7expand4Pii
	.def	__Z7expand4Pii;	.scl	2;	.type	32;	.endef
__Z7expand4Pii:
LFB3642:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$8, %esp
	.cfi_def_cfa_offset 28
	movl	32(%esp), %edi
	movl	28(%esp), %ebp
	cmpl	$3, %edi
	jle	L31
	leal	-4(%edi), %eax
	movl	%ebp, %edx
	movl	%eax, (%esp)
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	shrl	$2, (%esp)
	xorl	%esi, %esi
	movl	(%esp), %eax
	sall	$4, %eax
	leal	16(%ebp,%eax), %eax
	movl	%eax, 4(%esp)
	movl	4(%esp), %ebp
	xorl	%eax, %eax
	.p2align 4,,7
L28:
	addl	(%edx), %esi
	addl	$16, %edx
	addl	-12(%edx), %ebx
	addl	-8(%edx), %ecx
	addl	-4(%edx), %eax
	cmpl	%ebp, %edx
	jne	L28
	movl	(%esp), %edx
	addl	%esi, %ebx
	movl	28(%esp), %ebp
	addl	%ebx, %ecx
	addl	%ecx, %eax
	leal	4(,%edx,4), %edx
L26:
	xorl	%ebx, %ebx
	cmpl	%edx, %edi
	jle	L29
	.p2align 4,,7
L30:
	addl	0(%ebp,%edx,4), %ebx
	addl	$1, %edx
	cmpl	%edi, %edx
	jne	L30
L29:
	addl	$8, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%ebx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L31:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	L26
	.cfi_endproc
LFE3642:
	.p2align 4,,15
	.globl	__Z7expand5Pii
	.def	__Z7expand5Pii;	.scl	2;	.type	32;	.endef
__Z7expand5Pii:
LFB3643:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$4, %esp
	.cfi_def_cfa_offset 24
	cmpl	$4, 28(%esp)
	jle	L40
	movl	24(%esp), %ecx
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%edx, %edx
	movl	%eax, (%esp)
	.p2align 4,,7
L37:
	movl	16(%ecx), %eax
	addl	$5, %edx
	addl	$20, %ecx
	addl	-20(%ecx), %ebp
	addl	-16(%ecx), %ebx
	addl	-12(%ecx), %edi
	addl	-8(%ecx), %esi
	addl	%eax, (%esp)
	leal	4(%edx), %eax
	cmpl	%eax, 28(%esp)
	jg	L37
	movl	(%esp), %eax
	leal	0(%ebp,%ebx), %ecx
	addl	%ecx, %edi
	addl	%edi, %esi
	addl	%esi, %eax
L35:
	xorl	%ecx, %ecx
	cmpl	%edx, 28(%esp)
	jle	L38
	movl	24(%esp), %ebx
	movl	28(%esp), %esi
	.p2align 4,,7
L39:
	addl	(%ebx,%edx,4), %ecx
	addl	$1, %edx
	cmpl	%esi, %edx
	jne	L39
L38:
	addl	$4, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%ecx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L40:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	L35
	.cfi_endproc
LFE3643:
	.p2align 4,,15
	.globl	__Z7expand6Pii
	.def	__Z7expand6Pii;	.scl	2;	.type	32;	.endef
__Z7expand6Pii:
LFB3644:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$8, %esp
	.cfi_def_cfa_offset 28
	cmpl	$5, 32(%esp)
	jle	L49
	movl	28(%esp), %edx
	xorl	%eax, %eax
	xorl	%esi, %esi
	xorl	%edi, %edi
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	movl	$0, (%esp)
	movl	%eax, 4(%esp)
	.p2align 4,,7
L46:
	movl	(%edx), %eax
	addl	$6, %ecx
	addl	$24, %edx
	addl	%eax, (%esp)
	movl	-4(%edx), %eax
	addl	-20(%edx), %ebp
	addl	-16(%edx), %ebx
	addl	-12(%edx), %edi
	addl	-8(%edx), %esi
	addl	%eax, 4(%esp)
	leal	5(%ecx), %eax
	cmpl	%eax, 32(%esp)
	jg	L46
	movl	(%esp), %edx
	movl	4(%esp), %eax
	addl	%ebp, %edx
	addl	%ebx, %edx
	addl	%edx, %edi
	addl	%edi, %esi
	addl	%esi, %eax
L44:
	xorl	%edx, %edx
	cmpl	%ecx, 32(%esp)
	jle	L47
	movl	28(%esp), %ebx
	movl	32(%esp), %esi
	.p2align 4,,7
L48:
	addl	(%ebx,%ecx,4), %edx
	addl	$1, %ecx
	cmpl	%esi, %ecx
	jne	L48
L47:
	addl	$8, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%edx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L49:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	L44
	.cfi_endproc
LFE3644:
	.p2align 4,,15
	.globl	__Z7expand7Pii
	.def	__Z7expand7Pii;	.scl	2;	.type	32;	.endef
__Z7expand7Pii:
LFB3645:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$12, %esp
	.cfi_def_cfa_offset 32
	cmpl	$6, 36(%esp)
	jle	L58
	movl	32(%esp), %edx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$0, 4(%esp)
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movl	$0, (%esp)
	movl	%eax, 8(%esp)
	.p2align 4,,7
L55:
	movl	8(%edx), %eax
	addl	$7, %ecx
	addl	$28, %edx
	addl	%eax, (%esp)
	movl	-16(%edx), %eax
	addl	%eax, 4(%esp)
	movl	-4(%edx), %eax
	addl	-28(%edx), %edi
	addl	-24(%edx), %esi
	addl	-12(%edx), %ebp
	addl	-8(%edx), %ebx
	addl	%eax, 8(%esp)
	leal	6(%ecx), %eax
	cmpl	%eax, 36(%esp)
	jg	L55
	addl	%edi, %esi
	movl	8(%esp), %eax
	addl	(%esp), %esi
	addl	4(%esp), %esi
	addl	%ebp, %esi
	addl	%esi, %ebx
	addl	%ebx, %eax
L53:
	xorl	%edx, %edx
	cmpl	%ecx, 36(%esp)
	jle	L56
	movl	32(%esp), %ebx
	movl	36(%esp), %esi
	.p2align 4,,7
L57:
	addl	(%ebx,%ecx,4), %edx
	addl	$1, %ecx
	cmpl	%esi, %ecx
	jne	L57
L56:
	addl	$12, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%edx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L58:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	L53
	.cfi_endproc
LFE3645:
	.p2align 4,,15
	.globl	__Z7expand8Pii
	.def	__Z7expand8Pii;	.scl	2;	.type	32;	.endef
__Z7expand8Pii:
LFB3646:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$20, %esp
	.cfi_def_cfa_offset 40
	cmpl	$7, 44(%esp)
	jle	L67
	movl	44(%esp), %eax
	xorl	%ecx, %ecx
	xorl	%ebx, %ebx
	movl	40(%esp), %edx
	xorl	%esi, %esi
	xorl	%edi, %edi
	movl	$0, 4(%esp)
	xorl	%ebp, %ebp
	movl	$0, (%esp)
	subl	$8, %eax
	movl	%eax, 16(%esp)
	shrl	$3, 16(%esp)
	movl	16(%esp), %eax
	sall	$5, %eax
	leal	32(%edx,%eax), %eax
	movl	%eax, 12(%esp)
	xorl	%eax, %eax
	movl	%eax, 8(%esp)
	.p2align 4,,7
L64:
	movl	16(%edx), %eax
	addl	$32, %edx
	addl	%eax, (%esp)
	movl	-12(%edx), %eax
	addl	%eax, 4(%esp)
	movl	-4(%edx), %eax
	addl	-32(%edx), %ebp
	addl	-28(%edx), %edi
	addl	-24(%edx), %esi
	addl	-20(%edx), %ebx
	addl	-8(%edx), %ecx
	addl	%eax, 8(%esp)
	cmpl	12(%esp), %edx
	jne	L64
	addl	%ebp, %edi
	movl	8(%esp), %eax
	addl	%esi, %edi
	movl	16(%esp), %edx
	addl	%ebx, %edi
	addl	(%esp), %edi
	addl	4(%esp), %edi
	leal	8(,%edx,8), %edx
	addl	%edi, %ecx
	addl	%ecx, %eax
L62:
	xorl	%ebx, %ebx
	cmpl	%edx, 44(%esp)
	jle	L65
	movl	40(%esp), %ecx
	movl	44(%esp), %esi
	.p2align 4,,7
L66:
	addl	(%ecx,%edx,4), %ebx
	addl	$1, %edx
	cmpl	%esi, %edx
	jne	L66
L65:
	addl	$20, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%ebx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L67:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	L62
	.cfi_endproc
LFE3646:
	.p2align 4,,15
	.globl	__Z7expand9Pii
	.def	__Z7expand9Pii;	.scl	2;	.type	32;	.endef
__Z7expand9Pii:
LFB3647:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$20, %esp
	.cfi_def_cfa_offset 40
	cmpl	$8, 44(%esp)
	jle	L76
	movl	40(%esp), %edx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	movl	$0, 12(%esp)
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$0, 8(%esp)
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	movl	%eax, 16(%esp)
	.p2align 4,,7
L73:
	movl	12(%edx), %eax
	addl	$9, %ecx
	addl	$36, %edx
	addl	%eax, (%esp)
	movl	-20(%edx), %eax
	addl	%eax, 4(%esp)
	movl	-16(%edx), %eax
	addl	%eax, 8(%esp)
	movl	-12(%edx), %eax
	addl	%eax, 12(%esp)
	movl	-4(%edx), %eax
	addl	-36(%edx), %edi
	addl	-32(%edx), %esi
	addl	-28(%edx), %ebx
	addl	-8(%edx), %ebp
	addl	%eax, 16(%esp)
	leal	8(%ecx), %eax
	cmpl	%eax, 44(%esp)
	jg	L73
	addl	%edi, %esi
	movl	12(%esp), %edx
	addl	%ebx, %esi
	movl	16(%esp), %eax
	addl	(%esp), %esi
	addl	4(%esp), %esi
	addl	8(%esp), %esi
	addl	%esi, %edx
	addl	%ebp, %edx
	addl	%edx, %eax
L71:
	xorl	%edx, %edx
	cmpl	%ecx, 44(%esp)
	jle	L74
	movl	40(%esp), %ebx
	movl	44(%esp), %esi
	.p2align 4,,7
L75:
	addl	(%ebx,%ecx,4), %edx
	addl	$1, %ecx
	cmpl	%esi, %ecx
	jne	L75
L74:
	addl	$20, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%edx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L76:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	L71
	.cfi_endproc
LFE3647:
	.p2align 4,,15
	.globl	__Z8expand10Pii
	.def	__Z8expand10Pii;	.scl	2;	.type	32;	.endef
__Z8expand10Pii:
LFB3648:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	pushl	%edi
	.cfi_def_cfa_offset 12
	.cfi_offset 7, -12
	pushl	%esi
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	pushl	%ebx
	.cfi_def_cfa_offset 20
	.cfi_offset 3, -20
	subl	$24, %esp
	.cfi_def_cfa_offset 44
	cmpl	$9, 48(%esp)
	jle	L85
	movl	44(%esp), %edx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	movl	$0, 16(%esp)
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$0, 12(%esp)
	xorl	%edi, %edi
	xorl	%ecx, %ecx
	movl	$0, 8(%esp)
	movl	$0, 4(%esp)
	movl	$0, (%esp)
	movl	%eax, 20(%esp)
	.p2align 4,,7
L82:
	movl	12(%edx), %eax
	addl	$10, %ecx
	addl	$40, %edx
	addl	%eax, (%esp)
	movl	-24(%edx), %eax
	addl	%eax, 4(%esp)
	movl	-20(%edx), %eax
	addl	%eax, 8(%esp)
	movl	-16(%edx), %eax
	addl	%eax, 12(%esp)
	movl	-12(%edx), %eax
	addl	%eax, 16(%esp)
	movl	-4(%edx), %eax
	addl	-40(%edx), %edi
	addl	-36(%edx), %esi
	addl	-32(%edx), %ebx
	addl	-8(%edx), %ebp
	addl	%eax, 20(%esp)
	leal	9(%ecx), %eax
	cmpl	%eax, 48(%esp)
	jg	L82
	addl	%edi, %esi
	movl	16(%esp), %edx
	addl	%ebx, %esi
	movl	20(%esp), %eax
	addl	(%esp), %esi
	addl	4(%esp), %esi
	addl	8(%esp), %esi
	addl	12(%esp), %esi
	addl	%esi, %edx
	addl	%ebp, %edx
	addl	%edx, %eax
L80:
	xorl	%edx, %edx
	cmpl	%ecx, 48(%esp)
	jle	L83
	movl	44(%esp), %ebx
	movl	48(%esp), %esi
	.p2align 4,,7
L84:
	addl	(%ebx,%ecx,4), %edx
	addl	$1, %ecx
	cmpl	%esi, %ecx
	jne	L84
L83:
	addl	$24, %esp
	.cfi_remember_state
	.cfi_def_cfa_offset 20
	addl	%edx, %eax
	popl	%ebx
	.cfi_restore 3
	.cfi_def_cfa_offset 16
	popl	%esi
	.cfi_restore 6
	.cfi_def_cfa_offset 12
	popl	%edi
	.cfi_restore 7
	.cfi_def_cfa_offset 8
	popl	%ebp
	.cfi_restore 5
	.cfi_def_cfa_offset 4
	ret
L85:
	.cfi_restore_state
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	jmp	L80
	.cfi_endproc
LFE3648:
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "Generate completed.\12\0"
LC1:
	.ascii "using no_expand:\0"
LC2:
	.ascii "ms \0"
LC3:
	.ascii "loop_expand.cpp\0"
LC4:
	.ascii "now==ans\0"
LC5:
	.ascii "using expand2:\0"
LC6:
	.ascii "using expand3:\0"
LC7:
	.ascii "using expand4:\0"
LC8:
	.ascii "using expand5:\0"
LC9:
	.ascii "using expand6:\0"
LC10:
	.ascii "using expand7:\0"
LC11:
	.ascii "using expand8:\0"
LC12:
	.ascii "using expand9:\0"
LC13:
	.ascii "using expand10:\0"
	.section	.text.startup,"x"
	.p2align 4,,15
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
LFB3638:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%edi
	pushl	%esi
	pushl	%ebx
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x70,0x6
	.cfi_escape 0x10,0x7,0x2,0x75,0x7c
	.cfi_escape 0x10,0x6,0x2,0x75,0x78
	.cfi_escape 0x10,0x3,0x2,0x75,0x74
	subl	$56, %esp
	call	___main
	movl	$0, (%esp)
	call	__time32
	movl	%eax, (%esp)
	call	_srand
	leal	-28(%ebp), %eax
	movl	$__ZSt3cin, %ecx
	movl	%eax, (%esp)
	call	__ZNSirsERi
	movl	-28(%ebp), %edx
	movl	$-1, %eax
	leal	0(,%edx,4), %ecx
	subl	$4, %esp
	cmpl	$532676608, %edx
	cmovbe	%ecx, %eax
	movl	%eax, (%esp)
	call	__Znaj
	movl	%eax, %esi
	movl	-28(%ebp), %eax
	movl	%esi, %ebx
	leal	(%esi,%eax,4), %edi
	cmpl	%edi, %esi
	je	L93
	.p2align 4,,7
L112:
	call	_rand
	addl	$4, %ebx
	movl	%eax, -4(%ebx)
	cmpl	%ebx, %edi
	jne	L112
L93:
	movl	$LC0, 4(%esp)
	xorl	%ebx, %ebx
	movl	$__ZSt4cout, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-28(%ebp), %edx
	testl	%edx, %edx
	jle	L92
	movl	%esi, %eax
	xorl	%ebx, %ebx
	leal	(%esi,%edx,4), %edx
	.p2align 4,,7
L95:
	addl	(%eax), %ebx
	addl	$4, %eax
	cmpl	%edx, %eax
	jne	L95
L92:
	call	_clock
	movl	$10, %ecx
	movl	%ebx, -48(%ebp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	leal	(%esi,%eax,4), %edx
	movl	%eax, %ebx
	.p2align 4,,7
L99:
	testl	%ebx, %ebx
	jle	L111
	movl	%esi, %eax
	xorl	%edi, %edi
	.p2align 4,,7
L97:
	addl	(%eax), %edi
	addl	$4, %eax
	cmpl	%edx, %eax
	jne	L97
	subl	$1, %ecx
	jne	L99
L115:
	movl	-48(%ebp), %ebx
	call	_clock
	movl	$LC1, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%edi, %ebx
	je	L100
	movl	$32, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L100:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand2Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC5, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%ebx, %edi
	je	L101
	movl	$33, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L101:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand3Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC6, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%ebx, %edi
	je	L102
	movl	$34, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L102:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand4Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC7, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%ebx, %edi
	je	L103
	movl	$35, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L103:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand5Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC8, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%ebx, %edi
	je	L104
	movl	$36, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L104:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand6Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC9, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%edi, %ebx
	je	L105
	movl	$37, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L105:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand7Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC10, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%edi, %ebx
	je	L106
	movl	$38, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L106:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand8Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC11, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%edi, %ebx
	je	L107
	movl	$39, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L107:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z7expand9Pii
	movl	%eax, %edi
	call	_clock
	movl	$LC12, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edx
	subl	-44(%ebp), %edx
	movl	%edx, -44(%ebp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	-44(%ebp), %edx
	movl	%edx, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%edi, %ebx
	je	L108
	movl	$40, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L108:
	call	_clock
	movl	%esi, (%esp)
	movl	%eax, -44(%ebp)
	movl	-28(%ebp), %eax
	movl	%eax, 4(%esp)
	call	__Z8expand10Pii
	movl	%eax, %esi
	call	_clock
	movl	$LC13, 4(%esp)
	movl	$__ZSt4cout, (%esp)
	movl	%eax, %edi
	subl	-44(%ebp), %edi
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%edi, (%esp)
	movl	%eax, %ecx
	call	__ZNSo9_M_insertIlEERSoT_
	subl	$4, %esp
	movl	$LC2, 4(%esp)
	movl	%eax, (%esp)
	call	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc
	movl	%esi, (%esp)
	movl	%eax, %ecx
	call	__ZNSolsEi
	subl	$4, %esp
	movl	%eax, (%esp)
	call	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_
	cmpl	%esi, %ebx
	je	L109
	movl	$41, 8(%esp)
	movl	$LC3, 4(%esp)
	movl	$LC4, (%esp)
	call	__assert
L109:
	leal	-16(%ebp), %esp
	xorl	%eax, %eax
	popl	%ecx
	.cfi_remember_state
	.cfi_restore 1
	.cfi_def_cfa 1, 0
	popl	%ebx
	.cfi_restore 3
	popl	%esi
	.cfi_restore 6
	popl	%edi
	.cfi_restore 7
	popl	%ebp
	.cfi_restore 5
	leal	-4(%ecx), %esp
	.cfi_def_cfa 4, 4
	ret
L111:
	.cfi_restore_state
	xorl	%edi, %edi
	subl	$1, %ecx
	jne	L99
	jmp	L115
	.cfi_endproc
LFE3638:
	.p2align 4,,15
	.def	__GLOBAL__sub_I_main;	.scl	3;	.type	32;	.endef
__GLOBAL__sub_I_main:
LFB3678:
	.cfi_startproc
	subl	$28, %esp
	.cfi_def_cfa_offset 32
	movl	$__ZStL8__ioinit, %ecx
	call	__ZNSt8ios_base4InitC1Ev
	movl	$___tcf_0, (%esp)
	call	_atexit
	addl	$28, %esp
	.cfi_def_cfa_offset 4
	ret
	.cfi_endproc
LFE3678:
	.section	.ctors,"w"
	.align 4
	.long	__GLOBAL__sub_I_main
.lcomm __ZStL8__ioinit,1,1
	.ident	"GCC: (i686-posix-dwarf-rev0, Built by MinGW-W64 project) 4.8.4"
	.def	__ZNSt8ios_base4InitD1Ev;	.scl	2;	.type	32;	.endef
	.def	__time32;	.scl	2;	.type	32;	.endef
	.def	_srand;	.scl	2;	.type	32;	.endef
	.def	__ZNSirsERi;	.scl	2;	.type	32;	.endef
	.def	__Znaj;	.scl	2;	.type	32;	.endef
	.def	_rand;	.scl	2;	.type	32;	.endef
	.def	__ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc;	.scl	2;	.type	32;	.endef
	.def	_clock;	.scl	2;	.type	32;	.endef
	.def	__ZNSo9_M_insertIlEERSoT_;	.scl	2;	.type	32;	.endef
	.def	__ZNSolsEi;	.scl	2;	.type	32;	.endef
	.def	__ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_;	.scl	2;	.type	32;	.endef
	.def	__assert;	.scl	2;	.type	32;	.endef
	.def	__ZNSt8ios_base4InitC1Ev;	.scl	2;	.type	32;	.endef
	.def	_atexit;	.scl	2;	.type	32;	.endef

#include<bits/stdc++.h>

using namespace std;

int no_expand(int* a,int n);
int expand2(int* a,int n);
int expand3(int* a,int n);
int expand4(int* a,int n);
int expand5(int* a,int n);
int expand6(int* a,int n);
int expand7(int* a,int n);
int expand8(int* a,int n);
int expand9(int* a,int n);
int expand10(int* a,int n);

#define test(func) t=clock();now=0;\
for(int i=0;i<10;i++)\
	now+=func(a,n);\
cout<<"using "#func":"<<clock()-t<<"ms "<<now<<endl;\
assert(now==ans);\

int main()
{
	srand(time(NULL));
	int n;
	cin>>n;
	int* a=new int [n];
	generate(a,a+n,rand);
	cout<<"Generate completed.\n";
	int ans=no_expand(a,n)*10,now;
	time_t t;
	test(no_expand);
	test(expand2);
	test(expand3);
	test(expand4);
	test(expand5);
	test(expand6);
	test(expand7);
	test(expand8);
	test(expand9);
	test(expand10);
	return 0;
}

int no_expand(int* a,int n)
{
	int ans=0;
	for(int i=0;i<n;i++)
		ans+=a[i];
	return ans;
}

#define expand(_) sum##_+=a[i+_];

int expand2(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0;
	for(i=0;i+1<n;i+=2)
	{
		expand(0);
		expand(1);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1;
	return ans;
}

int expand3(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0;
	for(i=0;i+2<n;i+=3)
	{
		expand(0);
		expand(1);
		expand(2);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2;
	return ans;
}

int expand4(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0;
	for(i=0;i+3<n;i+=4)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3;
	return ans;
}

int expand5(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0,sum4=0;
	for(i=0;i+4<n;i+=5)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
		expand(4);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3+sum4;
	return ans;
}

int expand6(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0,sum4=0,sum5=0;
	for(i=0;i+5<n;i+=6)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
		expand(4);
		expand(5);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3+sum4+sum5;
	return ans;
}

int expand7(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0,sum4=0,sum5=0,sum6=0;
	for(i=0;i+6<n;i+=7)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
		expand(4);
		expand(5);
		expand(6);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3+sum4+sum5+sum6;
	return ans;
}

int expand8(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0,sum4=0,sum5=0,sum6=0,sum7=0;
	for(i=0;i+7<n;i+=8)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
		expand(4);
		expand(5);
		expand(6);
		expand(7);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3+sum4+sum5+sum6+sum7;
	return ans;
}

int expand9(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0,sum4=0,sum5=0,sum6=0,sum7=0,sum8=0;
	for(i=0;i+8<n;i+=9)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
		expand(4);
		expand(5);
		expand(6);
		expand(7);
		expand(8);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8;
	return ans;
}

int expand10(int* a,int n)
{
	int ans=0,i,sum0=0,sum1=0,sum2=0,sum3=0,sum4=0,sum5=0,sum6=0,sum7=0,sum8=0,sum9=0;
	for(i=0;i+9<n;i+=10)
	{
		expand(0);
		expand(1);
		expand(2);
		expand(3);
		expand(4);
		expand(5);
		expand(6);
		expand(7);
		expand(8);
		expand(9);
	}
	for(;i<n;i++)
		ans+=a[i];
	ans+=sum0+sum1+sum2+sum3+sum4+sum5+sum6+sum7+sum8+sum9;
	return ans;
}
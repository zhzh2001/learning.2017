#include<bits/stdc++.h>
using namespace std;
ifstream fin("sort.in");
ofstream fout("sort.out");
int main()
{
	int n;
	fin>>n;
	vector<string> a(n);
	for(int i=0;i<n;i++)
		fin>>a[i];
	sort(a.begin(),a.end());
	for(int i=0;i<n;i++)
		fout<<a[i]<<endl;
	return 0;
}
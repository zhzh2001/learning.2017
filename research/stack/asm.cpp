#include<bits/stdc++.h>
using namespace std;
const int N=800005;
int ch[N][26],end[N],cc,ans;
string s;
void trie_insert(const string& s,bool flag)
{
	int now=0,i;
	for(i=0;i<s.length();now=ch[now][s[i]-'a'],i++)
		if(!ch[now][s[i]-'a'])
			ch[now][s[i]-'a']=++cc;
	if(flag)
		end[now]++;
	else
	{
		if(end[now]>0)
			ans+=i;
		end[now]--;
	}
}
void dfs(int k,int dep)
{
	//cout<<dep<<endl;
	for(int i=0;i<26;i++)
		if(ch[k][i])
		{
			dfs(ch[k][i],dep+1);
			if((long long)end[k]*end[ch[k][i]]<0)
				ans+=dep*min(abs(end[k]),abs(end[ch[k][i]]));
			end[k]+=end[ch[k][i]];
		}
}
void Calldfs()
{
	const int stack_size=15000005;
	static int Stack[stack_size],bak;
	__asm__ __volatile__
	(
		"mov %%esp,%0\n"
		"mov %1,%%esp\n"
		:"=g"(bak)
		:"g"(Stack+stack_size-1)
		:
	);
	dfs(0,0);
	__asm__ __volatile__
	(
		"mov %0,%%esp\n"
		:
		:"g"(bak)
		:
	);
}
int main()
{
	freopen("test.txt","r",stdin);
	int n;
	cin>>n;
	for(int i=1;i<=n;i++)
	{
		cin>>s;
		trie_insert(s,true);
	}
	for(int i=1;i<=n;i++)
	{
		cin>>s;
		trie_insert(s,false);
	}
	Calldfs();
	cout<<ans<<endl;
	return 0;
}

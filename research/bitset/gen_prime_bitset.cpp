#include<bits/stdc++.h>
using namespace std;
bitset<100000005> p;
int main()
{
	int n;
	cin>>n;
	time_t s=clock();
	for(int i=2;i*i<=n;i++)
		if(!p[i])
			for(int j=i*i;j<=n;j+=i)
				p[j]=true;
	cout<<"n="<<n<<" t="<<double(clock()-s)/CLOCKS_PER_SEC<<"s\n";
	return 0;
}
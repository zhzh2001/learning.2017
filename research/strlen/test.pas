program test;
const
  s='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-*/.,<>{}|\\!@#$%^&*():;';
var
  ps:ansistring;
  len,i,t:longint;
begin
  randomize;
  read(len);
  ps:='';
  for i:=1 to len do
    ps:=ps+s[random(length(s))];
  randomize;
  t:=randseed;
  for i:=1 to length(ps) do
    lowercase(ps[i]);
  randomize;
  writeln(randseed-t);
end.
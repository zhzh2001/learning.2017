#include<bits/stdc++.h>
using namespace std;
const string s="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+-*/.,<>{}|\\!@#$%^&*():;\"'";
int main()
{
	srand(time(NULL));
	int len;
	cin>>len;
	char *cs=new char [len+1];
	for(int i=0;i<len;i++)
		cs[i]=s[rand()%s.length()];
	cs[len]='\0';
	clock_t t=clock();
	for(int i=0;i<strlen(cs);i++)
		cs[i]=tolower(cs[i]);
	cout<<"using strlen for:"<<clock()-t<<endl;
	string ss(cs);
	t=clock();
	for(int i=0;i<ss.length();i++)
		ss[i]=tolower(ss[i]);
	cout<<"using string::length for:"<<clock()-t<<endl;
	delete [] cs;
	return 0;
}
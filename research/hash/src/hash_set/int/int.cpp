#include<bits/stdc++.h>
#include<hash_set>
using namespace std;
using namespace __gnu_cxx;
ifstream fin("int.in");
ofstream fout("int.out");
hash_set<int> S;
int main()
{
	int n;
	fin>>n;
	while(n--)
	{
		int x;
		fin>>x;
		S.insert(x);
	}
	fin>>n;
	while(n--)
	{
		int x;
		fin>>x;
		fout<<(S.find(x)!=S.end());
	}
	fout<<endl;
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
ifstream fin("int.in");
ofstream fout("int.out");
set<int> S;
int main()
{
	int n;
	fin>>n;
	while(n--)
	{
		int x;
		fin>>x;
		S.insert(x);
	}
	fin>>n;
	while(n--)
	{
		int x;
		fin>>x;
		fout<<(S.find(x)!=S.end());
	}
	fout<<endl;
	return 0;
}

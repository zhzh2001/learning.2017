#include<bits/stdc++.h>
using namespace std;
const int N=100005,M=1000005;
ifstream fin("graph.in");
ofstream fout("graph.out");
int head[N],en;
long long d[N];
bool inQ[N];
struct edge
{
	int v,w,nxt;
}e[M];
inline void add_edge(int u,int v,int w)
{
	e[++en].v=v;
	e[en].w=w;
	e[en].nxt=head[u];
	head[u]=en;
}
int main()
{
	int n,m;
	fin>>n>>m;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		add_edge(u,v,w);
	}
	for(int i=1;i<=n;i++)
		d[i]=i==1?0:numeric_limits<long long>::max();
	queue<int> Q;
	Q.push(1);
	inQ[1]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(int i=head[k];i;i=e[i].nxt)
			if(d[k]+e[i].w<d[e[i].v])
			{
				d[e[i].v]=d[k]+e[i].w;
				if(!inQ[e[i].v])
				{
					Q.push(e[i].v);
					inQ[e[i].v]=true;
				}
			}
	}
	fout<<d[n]<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=100005;
ifstream fin("graph.in");
ofstream fout("graph.out");
long long d[N];
bool inQ[N];
struct node
{
	int v,w;
	node(int v,int w):v(v),w(w){}
};
vector<node> mat[N];
int main()
{
	int n,m;
	fin>>n>>m;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(node(v,w));
	}
	for(int i=1;i<=n;i++)
		d[i]=i==1?0:numeric_limits<long long>::max();
	queue<int> Q;
	Q.push(1);
	inQ[1]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		inQ[k]=false;
		for(auto e:mat[k])
			if(d[k]+e.w<d[e.v])
			{
				d[e.v]=d[k]+e.w;
				if(!inQ[e.v])
				{
					Q.push(e.v);
					inQ[e.v]=true;
				}
			}
	}
	fout<<d[n]<<endl;
	return 0;
}
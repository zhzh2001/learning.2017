#include<bits/stdc++.h>
using namespace std;
ofstream fout("graph.in");
const int n=1e5;
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return u<rhs.u;
	}
}e[4*n+5];
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<5*n<<endl;
	for(int i=1;i<=4*n+1;i++)
	{
		uniform_int_distribution<> du(1,n-1);
		e[i].u=du(gen);
		uniform_int_distribution<> dv(1,min(20,n-e[i].u));
		e[i].v=e[i].u+dv(gen);
		e[i].w=n*100;
	}
	sort(e+1,e+4*n+2);
	int j=1;
	for(int i=1;i<n;i++)
	{
		fout<<i<<' '<<i+1<<' '<<n-i+1<<endl;
		for(;j<=4*n+1&&e[j].u==i;j++)
			fout<<e[j].u<<' '<<e[j].v<<' '<<e[j].w<<endl;
	}
	return 0;
}
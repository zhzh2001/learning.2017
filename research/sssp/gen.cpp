#include<bits/stdc++.h>
using namespace std;
ofstream fout("graph.in");
int n=1e5,m=5e5,k=1e6;
int main()
{
	fout<<n<<' '<<m<<endl;
	default_random_engine gen(time(NULL));
	uniform_int_distribution<> dn(1,n);
	uniform_int_distribution<> dv(1,k);
	while(m--)
		fout<<dn(gen)<<' '<<dn(gen)<<' '<<dv(gen)<<endl;
	return 0;
}
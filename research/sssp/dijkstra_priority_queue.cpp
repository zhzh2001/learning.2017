#include<bits/stdc++.h>
using namespace std;
const int N=100005;
ifstream fin("graph.in");
ofstream fout("graph.out");
long long d[N];
bool vis[N];
struct node
{
	int v;
	long long w;
	node(int v,long long w):v(v),w(w){}
	bool operator>(const node& rhs)const
	{
		return w>rhs.w;
	}
};
vector<node> mat[N];
int main()
{
	int n,m;
	fin>>n>>m;
	while(m--)
	{
		int u,v,w;
		fin>>u>>v>>w;
		mat[u].push_back(node(v,w));
	}
	for(int i=1;i<=n;i++)
		d[i]=i==1?0:numeric_limits<long long>::max();
	priority_queue<node,vector<node>,greater<node> > Q;
	Q.push(node(1,0));
	while(!Q.empty())
	{
		node k=Q.top();Q.pop();
		if(!vis[k.v])
		{
			vis[k.v]=true;
			for(auto e:mat[k.v])
				if(!vis[e.v]&&d[k.v]+e.w<d[e.v])
				{
					d[e.v]=d[k.v]+e.w;
					Q.push(node(e.v,d[e.v]));
				}
		}
	}
	fout<<d[n]<<endl;
	return 0;
}
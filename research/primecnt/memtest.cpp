#include <iostream>
#include <exception>
using namespace std;
int main()
{
	size_t n;
	cin >> n;
	try
	{
		char *p = new char [n];
		delete[] p;
		cout << "successfully allocate and free " << n << " bytes\n";
	}
	catch (const exception &e)
	{
		cout << e.what() << endl;
		return 1;
	}
	return 0;
}
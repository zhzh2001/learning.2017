#include<bits/stdc++.h>
using namespace std;
/*
枚举算法
时间复杂度：sqrt(x)
问题：x不能太接近maxlongint
*/
bool prime(int x)
{
	if(x<2)
		return false;
	for(int i=2;i*i<=x;i++)
		if(x%i==0)
			return false;
	return true;
}
/*
wilson算法（无实际意义）
时间复杂度：x
*/
bool wilson(int x)
{
	if(x<2)
		return false;
	long long sum=1;
	for(int i=1;i<x;i++)
		sum=(sum*i)%x;
	return (sum+1)%x==0;
}
//快速幂：求a^b%p
int qmod(int a,int b,int p)
{
	long long ans=1,now=a;
	do
	{
		if(b&1)
			ans=(ans*now)%p;
		now=(now*now)%p;
	}
	while(b>>=1);
	return ans;
}
/*
费马小定理：p为质数的必要条件 a^(p-1)%p=1(0<a<p)
时间复杂度：log2(p)
问题：出错率高
*/
bool fermat(int x,int a)
{
	return qmod(a,x-1,x)==1;
}
//Miller-Rabin测试
bool mr_test(int n,int a)
{
	if(n==2)
		return true;
	if(!(n&1))
		return false;
	int d=n-1;
	while(!(d&1))
		d>>=1;
	long long t=qmod(a,d,n);
	while(d!=n-1&&t!=n-1&&t!=1)
	{
		t=t*t%n;
		d<<=1;
	}
	return t==n-1||(d&1);
}
/*
Miller-Rabin
时间复杂度：log2^2(x)*k
针对unsigned int保证正确
*/
bool miller_rabin(int x)
{
	int a[]={2,7,61};
	//enough for unsigned int
	for(int i=0;i<3;i++)
	{
		if(x==a[i])
			return true;
		if(!mr_test(x,a[i]))
			return false;
	}
	return true;
}
//快速乘：a*b%p
long long qmul(long long a,long long b,long long p)
{
	long long ans=0;
	do
	{
		if(b&1)
			ans=(ans+a)%p;
		a=(a+a)%p;
	}
	while(b>>=1);
	return ans;
}
//支持long long的快速幂
long long qmod_ll(long long a,long long b,long long p)
{
	long long ans=1;
	do
	{
		if(b&1)
			ans=qmul(ans,a,p);
		a=qmul(a,a,p);
	}
	while(b>>=1);
	return ans;
}
//支持long long的Miller-Rabin测试
bool mr_test_ll(long long n,long long a)
{
	if(n==2)
		return true;
	if(!(n&1))
		return false;
	long long d=n-1;
	while(!(d&1))
		d>>=1;
	long long t=qmod_ll(a,d,n);
	while(d!=n-1&&t!=n-1&&t!=1)
	{
		t=t*t%n;
		d<<=1;
	}
	return t==n-1||(d&1);
}
//产生int范围的随机数
int randl()
{
	if(RAND_MAX==32767)
		return (rand()<<15)+rand();
	else
		return rand();
}
//产生long long范围的随机数
long long randll()
{
	return ((long long)randl()<<30)+randl();
}
/*
Miller-Rabin
时间复杂度：log2^3(x)*k
针对long long
出错率：~4^(-k)
*/
bool miller_rabin_ll(long long x,int k)
{
	for(int i=1;i<=k;i++)
		if(!mr_test_ll(x,randll()%(x-2)+2))
			return false;
	return true;
}
char p[128000000];
const int n=1000000000;
int main()
{
	for(int i=2;i*i<=n;i++)
		if(!(p[i>>3]&(1<<(i&7))))
			for(int j=i*i;j<=n;j+=i)
				p[j>>3]|=(1<<(j&7));
	cout<<"generated\n";
	time_t start=clock();
	for(int i=2;i<=n;i++)
	{
		if(((p[i>>3]&(1<<(i&7)))==miller_rabin(i)))
			cout<<"error@"<<i<<endl;
		if(i%10000000==0)
			cout<<i/10000000<<"% completed,eta:"<<(clock()-start)/CLOCKS_PER_SEC/(i/10000000)*(100-i/10000000)<<"s\n";
	}
	return 0;
}
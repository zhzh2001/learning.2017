#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>
#include <ctime>
using namespace std;
typedef unsigned long long int_t;
const size_t seg_size = 32768;
int main()
{
	int_t n;
	cin >> n;
	clock_t start = clock();
	int_t m = sqrt(n);
	bool *p = new bool[m + 1];
	fill(p + 2, p + m + 1, true);
	for (int_t i = 2; i * i <= m; i++)
		if (p[i])
			for (int_t j = i * i; j <= m; j += i)
				p[j] = false;
	bool *buf = new bool[seg_size];
	vector<int> primes;
	vector<int> nxt;
	int_t cnt = 1;
	for (int_t low = 0, s = 3, now = 3; low <= n; low += seg_size)
	{
		fill(buf, buf + seg_size, true);
		int_t high = min(low + seg_size - 1, n);
		for (; s * s <= high; s += 2)
			if (p[s])
			{
				primes.push_back(s);
				nxt.push_back(s * s - low);
			}
		for (size_t i = 0; i < primes.size(); i++)
		{
			int_t j = nxt[i];
			for (int_t k = primes[i] * 2; j < seg_size; j += k)
				buf[j] = false;
			nxt[i] = j - seg_size;
		}
		for (; now <= high; now += 2)
			cnt += buf[now - low];
	}
	cout << cnt << endl;
	delete[] p;
	delete[] buf;
	cout << clock() - start << endl;
	return 0;
}
#include <iostream>
#include <algorithm>
#include <ctime>
using namespace std;
typedef unsigned long long int_t;
int main()
{
	int_t n;
	cin >> n;
	clock_t start = clock();
	bool *p = new bool[n + 1];
	fill(p + 2, p + n + 1, true);
	for (int_t i = 3; i * i <= n; i += 2)
		if (p[i])
			for (int_t j = i * i; j <= n; j += i + i)
				p[j] = false;
	int_t cnt = 1;
	for (int_t i = 3; i <= n; i += 2)
		cnt += p[i];
	cout << cnt << endl;
	delete[] p;
	cout << clock() - start << endl;
	return 0;
}
#include <iostream>
#include <vector>
#include <ctime>
using namespace std;
typedef unsigned long long int_t;
int main()
{
	int_t n;
	cin >> n;
	clock_t start = clock();
	vector<bool> p(n + 1, true);
	vector<int_t> primes;
	for (int_t i = 3; i <= n; i += 2)
	{
		if (p[i])
			primes.push_back(i);
		for (int_t j : primes)
		{
			if (i * j > n)
				break;
			p[i * j] = false;
			if (i % j == 0)
				break;
		}
	}
	cout << primes.size() + 1 << endl;
	cout << clock() - start << endl;
	return 0;
}
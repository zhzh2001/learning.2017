#include<fstream>
#include<cmath>
using namespace std;
ifstream fin("coin.in");
ofstream fout("coin.out");
int main()
{
    int n;
    fin>>n;
    fout.precision(n);
    fout<<fixed<<pow(2.0L,-n)<<endl;
    return 0;
}
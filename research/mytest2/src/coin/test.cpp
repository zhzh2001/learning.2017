#include<bits/stdc++.h>
using namespace std;
const size_t sz=sizeof(long double);
int main()
{
	int n;
	cin>>n;
	long double ans=pow(2.0L,-n);
	unsigned char *t=reinterpret_cast<unsigned char *>(&ans);
	for(size_t i=0;i<sz;i++)
	{
		unsigned char now=t[i];
		for(int j=7;j>=0;j--)
			cout<<(now&(1<<j)?1:0);
		cout<<' ';
	}
	cout<<endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
const int N=1000005,M=2000005;
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[M];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	FILE *F=fopen("graph.bin","rb");
	int n,m;
	fread(&n,sizeof(n),1,F);
	fread(&m,sizeof(m),1,F);
	fread(e+1,sizeof(edge),m,F);
	fclose(F);
	cout<<"input time:"<<clock()<<endl;
	sort(e+1,e+m+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	int cnt=0;
	long long ans=0;
	for(int i=1;i<=m;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv)
		{
			f[ru]=rv;
			ans+=e[i].w;
			if(++cnt==n-1)
				break;
		}
	}
	if(cnt<n-1)
		ans=-1;
	F=fopen("graph.out","wb");
	fwrite(&ans,sizeof(ans),1,F);
	fclose(F);
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
const int N=1000005,M=2000005;
struct edge
{
	int u,v,w;
	bool operator<(const edge& rhs)const
	{
		return w<rhs.w;
	}
}e[M];
int f[N];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	int n,m;
	fin>>n>>m;
	for(int i=1;i<=m;i++)
		fin>>e[i].u>>e[i].v>>e[i].w;
	cout<<"input time:"<<clock()<<endl;
	sort(e+1,e+m+1);
	for(int i=1;i<=n;i++)
		f[i]=i;
	int cnt=0;
	long long ans=0;
	for(int i=1;i<=m;i++)
	{
		int ru=getf(e[i].u),rv=getf(e[i].v);
		if(ru!=rv)
		{
			f[ru]=rv;
			ans+=e[i].w;
			if(++cnt==n-1)
				break;
		}
	}
	if(cnt<n-1)
		fout<<-1<<endl;
	else
		fout<<ans<<endl;
	return 0;
}
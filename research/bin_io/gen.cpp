#include<bits/stdc++.h>
using namespace std;
ofstream fout("graph.in");
const int n=100000,m=2000000,w=1000000;
int main()
{
	minstd_rand gen(time(NULL));
	FILE *F=fopen("graph.bin","wb");
	fwrite(&n,sizeof(n),1,F);
	fwrite(&m,sizeof(m),1,F);
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> du(1,n),dw(1,w);
		int u=du(gen),v=du(gen),w=dw(gen);
		fwrite(&u,sizeof(u),1,F);
		fwrite(&v,sizeof(v),1,F);
		fwrite(&w,sizeof(w),1,F);
		fout<<u<<' '<<v<<' '<<w<<endl;
	}
	fclose(F);
	return 0;
}
#include <stdio.h>
int main(void)
{
	int ans = 0;
	for (int i = 1; i <= 2e5; i++)
		ans += i;
	printf("%d %d\n", ans, ans < 0);
	return 0;
}
#include<bits/stdc++.h>
#define test(func)\
t=clock();\
memset(ans,0,sizeof(ans));\
for(i=0;i+9<n;i+=10)\
{\
	ans[0]+=(unsigned)func(a[i]);\
	ans[1]+=(unsigned)func(a[i+1]);\
	ans[2]+=(unsigned)func(a[i+2]);\
	ans[3]+=(unsigned)func(a[i+3]);\
	ans[4]+=(unsigned)func(a[i+4]);\
	ans[5]+=(unsigned)func(a[i+5]);\
	ans[6]+=(unsigned)func(a[i+6]);\
	ans[7]+=(unsigned)func(a[i+7]);\
	ans[8]+=(unsigned)func(a[i+8]);\
	ans[9]+=(unsigned)func(a[i+9]);\
}\
fans=0;\
for(int j=0;j<10;j++)\
	fans+=ans[j];\
for(;i<n;i++)\
	fans+=func(i);\
cout<<"using "#func":"<<clock()-t<<"ms "<<fans<<endl;

using namespace std;

ifstream fin("log2.in");

inline int log2i_while(int x);
inline int log2i_builtin(int x);
inline int log2i_asm(int x);
inline int log2i_float(int x);
inline int log2i_table(int x);
inline int log2i_table2(int x);
inline int log2i_hash(int x);

int main()
{
	int n;
	fin>>n;
	int* a=new int [n];
	for(int i=0;i<n;i++)
		fin>>a[i];
	cout<<"Input completed.\n";
	time_t t;
	unsigned ans[10],fans;
	int i;
	test(log2);
	test(log2i_while);
	test(log2i_builtin);
	test(log2i_asm);
	test(log2i_float);
	test(log2i_table);
	test(log2i_table2);
	test(log2i_hash);
	return 0;
}

inline int log2i_while(int x)
{
	int ans=0;
	while(x>>=1)
		ans++;
	return ans;
}

inline int log2i_builtin(int x)
{
	return 31-__builtin_clz(x);
}

inline int log2i_asm(int x)
{
	asm("bsr %0,%0":"=r"(x):"r"(x));
	return x;
}

union
{
	unsigned u[2];
	double d;
}t;

inline int log2i_float(int x)
{
	t.u[1]=0x43300000;
	t.u[0]=x;
	t.d-=4503599627370496.0;
	return (t.u[1]>>20)-0x3ff;
}

#define LT(n) n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
const int log2table256[256]={0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,LT(4),LT(5),LT(5),LT(6),LT(6),LT(6),LT(6),LT(7),LT(7),LT(7),LT(7),LT(7),LT(7),LT(7),LT(7)};

inline int log2i_table(int x)
{
	int tt,t;
	if(tt=x>>16)
		return (t=tt>>8)?24+log2table256[t]:16+log2table256[tt];
	else
		return (t=x>>8)?8+log2table256[t]:log2table256[x];
}

inline int log2i_table2(int x)
{
	int tt;
	if(tt=x>>24)
		return 24+log2table256[tt];
	if(tt=x>>16)
		return 16+log2table256[tt];
	if(tt=x>>8)
		return 8+log2table256[tt];
	return log2table256[x];
}

const int h[32]={0,9,1,10,13,21,2,29,11,14,16,18,22,25,3,30,8,12,20,28,15,17,24,7,19,27,23,6,26,5,4,31};
inline int log2i_hash(int x)
{
	x|=x>>1;
	x|=x>>2;
	x|=x>>4;
	x|=x>>8;
	x|=x>>16;
	return h[(x*0x07c4acddu)>>27];
}
#include<bits/stdc++.h>
#define print(func) cout<<"using "#func":"<<(int)func(x)<<endl;
using namespace std;
inline int log2i_while(int x);
inline int log2i_builtin(int x);
inline int log2i_asm(int x);
inline int log2i_float(int x);
inline int log2i_table(int x);
inline int log2i_hash(int x);
int main()
{
	int x;
	while(cin>>x)
	{
		print(log2);
		print(log2i_while);
		print(log2i_builtin);
		print(log2i_asm);
		print(log2i_float);
		print(log2i_table);
		print(log2i_hash);
	}
	return 0;
}

inline int log2i_while(int x)
{
	int ans=0;
	while(x>>=1)
		ans++;
	return ans;
}

inline int log2i_builtin(int x)
{
	return 31-__builtin_clz(x);
}

inline int log2i_asm(int x)
{
	asm("bsr %0,%0":"=r"(x):"r"(x));
	return x;
}

union
{
	unsigned u[2];
	double d;
}t;

inline int log2i_float(int x)
{
	t.u[1]=0x43300000;
	t.u[0]=x;
	t.d-=4503599627370496.0;
	return (t.u[1]>>20)-0x3ff;
}

#define LT(n) n,n,n,n,n,n,n,n,n,n,n,n,n,n,n,n
const int log2table256[256]={0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3,LT(4),LT(5),LT(5),LT(6),LT(6),LT(6),LT(6),LT(7),LT(7),LT(7),LT(7),LT(7),LT(7),LT(7),LT(7)};

inline int log2i_table(int x)
{
	int tt,t;
	if(tt=x>>16)
		return (t=tt>>8)?24+log2table256[t]:16+log2table256[tt];
	else
		return (t=x>>8)?8+log2table256[t]:log2table256[x];
}

const int h[32]={0,9,1,10,13,21,2,29,11,14,16,18,22,25,3,30,8,12,20,28,15,17,24,7,19,27,23,6,26,5,4,31};
inline int log2i_hash(int x)
{
	x|=x>>1;
	x|=x>>2;
	x|=x>>4;
	x|=x>>8;
	x|=x>>16;
	return h[(x*0x07c4acddu)>>27];
}
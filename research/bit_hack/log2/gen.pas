program gen;
var
  n,i:longint;
begin
  randomize;
  assign(output,'log2.in');
  rewrite(output);
  read(n);
  writeln(n);
  for i:=1 to n do
    write(random(maxlongint)+1,' ');
  writeln;
  close(output);
end.
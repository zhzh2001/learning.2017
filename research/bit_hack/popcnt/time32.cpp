#include<bits/stdc++.h>
#include "popcnt.h"
using namespace std;
unsigned long long asm_clock()
{
	asm ("rdtsc\n\t");
}
#define test(func)\
memset(ans,0,n);\
t=asm_clock();\
for(int i=0;i<n;i++)\
	ans[i]=func(a[i]);\
cout<<"using " #func <<':'<<1.0*(asm_clock()-t)/n<<endl;
int main()
{
	default_random_engine gen(time(NULL));
	uniform_int_distribution<> d(0,numeric_limits<int>::max());
	int n;
	cin>>n;
	int* a=new int [n];
	generate(a,a+n,bind(d,gen));
	cout<<"generated\n";
	unsigned long long t;
	char *ans=new char [n];
	test(naive);
	test(lookup_table);
	test(lookup_table2);
	test(dec_lowbit);
	test(bk);
	test(__builtin_popcount);
	test(parallel);
	test(parallel2);
	delete [] a;
	delete [] ans;
	return 0;
}
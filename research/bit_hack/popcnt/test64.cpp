#include<bits/stdc++.h>
#include "popcnt.h"
using namespace std;
#define test(func) cout<<"using " #func <<':'<<func(x)<<endl;
int main()
{
	long long x;
	cout<<"popcnt:"<<__builtin_cpu_supports("popcnt")<<endl;
	while(cin>>x)
	{
		test(naive);
		test(lookup_table);
		test(lookup_table2);
		test(dec_lowbit);
		test(bk);
		test(__builtin_popcountll);
		test(parallel);
		test(parallel2);
	}
	return 0;
}
inline int naive(int x)
{
	int ans=0;
	for(;x;x>>=1)
		ans+=x&1;
	return ans;
}

inline int naive(long long x)
{
	int ans=0;
	for(;x;x>>=1)
		ans+=x&1;
	return ans;
}

const int table[256]=
{
#define B2(n) n,n+1,n+1,n+2
#define B4(n) B2(n),B2(n+1),B2(n+1),B2(n+2)
#define B6(n) B4(n),B4(n+1),B4(n+1),B4(n+2)
    B6(0), B6(1), B6(1), B6(2)
};

inline int lookup_table(int x)
{
	return table[x&0xff]+table[(x>>8)&0xff]+table[(x>>16)&0xff]+table[x>>24];
}

inline int lookup_table(long long x)
{
	return table[x&0xff]+table[(x>>8)&0xff]+table[(x>>16)&0xff]+table[(x>>24)&0xff]+table[(x>>32)&0xff]+table[(x>>40)&0xff]+table[(x>>48)&0xff]+table[x>>56];
}

inline int lookup_table2(int x)
{
	unsigned char *p=(unsigned char *)&x;
	return table[p[0]]+table[p[1]]+table[p[2]]+table[p[3]];
}

inline int lookup_table2(long long x)
{
	unsigned char *p=(unsigned char *)&x;
	return table[p[0]]+table[p[1]]+table[p[2]]+table[p[3]]+table[p[4]]+table[p[5]]+table[p[6]]+table[p[7]];
}

inline int dec_lowbit(int x)
{
	int ans=0;
	for(;x;x-=x&-x,ans++);
	return ans;
}

inline int dec_lowbit(long long x)
{
	int ans=0;
	for(;x;x-=x&-x,ans++);
	return ans;
}

inline int bk(int x)
{
	int ans=0;
	for(;x;x&=x-1,ans++);
	return ans;
}

inline int bk(long long x)
{
	int ans=0;
	for(;x;x&=x-1,ans++);
	return ans;
}

inline int parallel(int x)
{
	x=(x&0x55555555)+((x>>1)&0x55555555);
	x=(x&0x33333333)+((x>>2)&0x33333333);
	x=(x&0x0f0f0f0f)+((x>>4)&0x0f0f0f0f);
	x=(x&0x00ff00ff)+((x>>8)&0x00ff00ff);
	return (x&0x0000ffff)+((x>>16)&0x0000ffff);
}

inline int parallel(long long x)
{
	x=(x&0x5555555555555555ll)+((x>>1)&0x5555555555555555ll);
	x=(x&0x3333333333333333ll)+((x>>2)&0x3333333333333333ll);
	x=(x&0x0f0f0f0f0f0f0f0fll)+((x>>4)&0x0f0f0f0f0f0f0f0fll);
	x=(x&0x00ff00ff00ff00ffll)+((x>>8)&0x00ff00ff00ff00ffll);
	x=(x&0x0000ffff0000ffffll)+((x>>16)&0x0000ffff0000ffffll);
	return (x&0x00000000ffffffffll)+((x>>32)&0x00000000ffffffffll);
}

inline int parallel2(int x)
{
	x-=(x>>1)&0x55555555;
	x=(x&0x33333333)+((x>>2)&0x33333333);
	x=(x+(x>>4))&0x0f0f0f0f;
	x+=x>>8;
	x+=x>>16;
	return x&0x3f;
}

inline int parallel2(long long x)
{
	x-=(x>>1)&0x5555555555555555ll;
	x=(x&0x3333333333333333ll)+((x>>2)&0x3333333333333333ll);
	x=(x+(x>>4))&0x0f0f0f0f0f0f0f0fll;
	x+=x>>8;
	x+=x>>16;
	x+=x>>32;
	return x&0x7f;
}
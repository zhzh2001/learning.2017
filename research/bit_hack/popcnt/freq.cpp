#include<bits/stdc++.h>
using namespace std;
unsigned long long asm_clock()
{
	asm ("rdtsc");
}
int main()
{
	unsigned long long t=asm_clock();
	this_thread::sleep_for(1s);
	cout<<asm_clock()-t<<endl;
	return 0;
}
#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
int n,m,i,j,tmp,ans,L[N],R[N];
struct ff{int x,node;}heap[N];
struct xx{
	int l,r,node;
	bool operator <(const xx& w)const{return l<w.l;}
}t[N],a[N];

void up(int x){
	while(x>1){
		int i=x>>1;
		if(heap[i].x>heap[x].x)
			swap(heap[i],heap[x]),x=i;
			else return;
	}
}
void down(int x){
	while((x<<1)<=tmp){
		int i=x<<1;
		if(i<tmp&&heap[i+1].x<heap[i].x)i++;
		if(heap[i].x<heap[x].x)
			swap(heap[i],heap[x]),x=i;
			else return;
	}
}
int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=1;i<=n;i++)scanf("%d",&t[i].l),t[i].node=i;
	for(i=1;i<=m;i++)scanf("%d%d",&a[i].l,&a[i].r),a[i].node=i;
	sort(a+1,a+m+1),sort(t+1,t+n+1);
	for(i=1,j=1;i<=n;i++){
		while(j<=m&&a[j].l<=t[i].l)heap[++tmp]=(ff){a[j].r,a[j].node},up(tmp),j++;
		while(tmp&&heap[1].x<t[i].l)
			swap(heap[1],heap[tmp--]),down(1);
		if(tmp){ans++;
			L[ans]=t[i].node,R[ans]=heap[1].node;
			swap(heap[1],heap[tmp--]),down(1);
		}
	}printf("%d\n",ans);
	for(i=1;i<=ans;i++)printf("%d %d\n",L[i],R[i]);
	return 0;
}

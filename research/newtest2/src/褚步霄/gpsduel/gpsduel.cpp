#include<stdio.h>
#include<cstring>
#include<vector>
#include<algorithm>
using namespace std;
#define N 100005
#define M 600005
#define ll long long
struct ff{int to,nxt,len;}e[M*2],e1[M*2],e2[M*2];
int n,m,w,S,T,u,v,t1,t2,l,r,i,j,cnt,cnt1,cnt2;
int d2[N],vis[N],q[N],head[N],head1[N],head2[N],vise[M*2];
ll d[N],d1[N],inf=10000000000000ll;
vector<int>pre[N],Pre[N],pre1[N],Pre1[N];

void add(int u,int v,int len){
	e[++cnt]=(ff){v,head[u],len};
	head[u]=cnt;
}
void add1(int u,int v,int len){
	e1[++cnt1]=(ff){v,head1[u],len};
	head1[u]=cnt1;
}
void add2(int u,int v,int len){
	e2[++cnt2]=(ff){v,head2[u],len};
	head2[u]=cnt2;
}
void SPFA(){
	for(int i=1;i<=n;i++)d[i]=inf,vis[i]=0;
	d[T]=0;q[1]=T;l=0;r=1;vis[T]=1;
	while(l!=r){
		l++;if(l>n)l=1;
		for(int i=head[q[l]];i;i=e[i].nxt){
			int v=e[i].to;
			if(d[q[l]]+e[i].len==d[v])
				pre[v].push_back(q[l]),Pre[v].push_back(i);
			if(d[q[l]]+e[i].len<d[v]){
				d[v]=d[q[l]]+e[i].len;
				pre[v].resize(0),Pre[v].resize(0);
				pre[v].push_back(q[l]),Pre[v].push_back(i);
				if(!vis[v]){
					r++;if(r>n)r=1;
					q[r]=v,vis[v]=1;
				}
			}
		}vis[q[l]]=0;
	}
}
void SPFA1(){
	for(int i=1;i<=n;i++)d1[i]=inf,vis[i]=0;
	d1[T]=0;q[1]=T;l=0;r=1;vis[T]=1;
	while(l!=r){
		l++;if(l>n)l=1;
		for(int i=head1[q[l]];i;i=e1[i].nxt){
			int v=e1[i].to;
			if(d1[q[l]]+e1[i].len==d1[v])
				pre1[v].push_back(q[l]),Pre1[v].push_back(i);
			if(d1[q[l]]+e1[i].len<d1[v]){
				d1[v]=d1[q[l]]+e1[i].len;
				pre1[v].resize(0),Pre1[v].resize(0);
				pre1[v].push_back(q[l]),Pre1[v].push_back(i);
				if(!vis[v]){
					r++;if(r>n)r=1;
					q[r]=v,vis[v]=1;
				}
			}
		}vis[q[l]]=0;
	}
}
void SPFA2(){
	for(int i=1;i<=n;i++)d2[i]=inf,vis[i]=0;
	d2[T]=0;q[1]=T;l=0;r=1;vis[T]=1;
	while(l!=r){
		l++;if(l>n)l=1;
		for(int i=head2[q[l]];i;i=e2[i].nxt){
			int v=e2[i].to;
			if(d2[q[l]]+e2[i].len<d2[v]){
				d2[v]=d2[q[l]]+e2[i].len;
				if(!vis[v]){
					r++;if(r>n)r=1;
					q[r]=v,vis[v]=1;
				}
			}
		}vis[q[l]]=0;
	}
}
void getpath(){
	for(int j=1;j<=n;j++){
		int s=pre[j].size();
		for(int i=0;i<s;i++)e2[Pre[j][i]].len--;
	}
}
void getpath1(){
	for(int j=1;j<=n;j++){
		int s=pre1[j].size();
		for(int i=0;i<s;i++)e2[Pre1[j][i]].len--;
	}
}
int main()
{
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	scanf("%d%d%d%d%d",&n,&m,&w,&S,&T);
	for(i=1;i<=m;i++){
		scanf("%d%d%d%d",&u,&v,&t1,&t2);
		add(u,v,t1),add(v,u,t1);
		add1(u,v,t2),add1(v,u,t2);
		add2(u,v,2),add2(v,u,2);
	}
	for(i=1;i<=w;i++){
		scanf("%d%d%d%d",&u,&v,&t1,&t2);
		add(v,u,t1),add1(v,u,t2),add2(v,u,2);
	}SPFA(),SPFA1();
	if(d[S]==inf){puts("inf inf -1");return 0;}
	printf("%I64d %I64d ",d[S],d1[S]);
	getpath(),getpath1();
	SPFA2();
	printf("%d\n",d2[S]);
	return 0;
}

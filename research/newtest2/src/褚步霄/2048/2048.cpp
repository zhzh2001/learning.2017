#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 300000
int n,i,ans,top,z[N],a[N];

void calc(){
	for(int i=1;i<=n;i++){
		z[top=1]=a[i];
		for(int j=i+1;j<=n;j++){
			z[++top]=a[j];
			while(top>1&&z[top]==z[top-1])z[top-1]=z[top]+1,top--;
		}for(int j=1;j<=top;j++)ans=max(ans,z[j]);
	}	
}	
int main()
{
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	scanf("%d",&n);
	for(i=1;i<=n;i++)scanf("%d",&a[i]);
	calc();
	for(i=1;i<=n/2;i++)swap(a[i],a[n-i+1]);
	calc();
	printf("%d\n",ans);
	return 0;
}

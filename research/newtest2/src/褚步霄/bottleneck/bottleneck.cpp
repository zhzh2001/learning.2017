#include<stdio.h>
#include<cstring>
#include<algorithm>
using namespace std;
#define N 100005
#define ll long long
ll num[N],ans[N];
int n,m,i,j,cnt,head[N],fa[N],a[N];
struct ff{int to,nxt;}e[N];
struct xx{
	int x,node;
	bool operator <(const xx& w)const{return x<w.x;}
}Ask[N];
void add(int u,int v){
	e[++cnt]=(ff){v,head[u]};
	head[u]=cnt;
}
void dfs(int now){
	for(int i=head[now];i;i=e[i].nxt)dfs(e[i].to);
	if(now>1)num[fa[now]]+=min(num[now],1ll*a[now]),num[now]-=min(num[now],1ll*a[now]);
}
int main()
{
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(i=2;i<=n;i++)
		scanf("%d%d%d",&fa[i],&num[i],&a[i]),add(fa[i],i);
	for(i=1;i<=m;i++)scanf("%d",&Ask[i].x),Ask[i].node=i;
	sort(Ask+1,Ask+m+1);
	for(i=1,j=1;j<=m;i++){
		dfs(1);
		while(j<=m&&Ask[j].x==i)ans[Ask[j].node]=num[1],j++;
	}for(i=1;i<=m;i++)printf("%I64d\n",ans[i]);
	return 0;
}

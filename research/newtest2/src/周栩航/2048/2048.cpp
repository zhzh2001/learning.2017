#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int n,tmp[1000001],a[1000001],ans=0;
int main()
{
	freopen("2048.in","r",stdin);freopen("2048.out","w",stdout);
	read(n);
	For(i,1,n)	read(a[i]);
	For(i,1,n)	tmp[i]=a[i];
	For(i,2,n)
	{
		if(a[i]==a[i-1]||a[i]==tmp[i-1])	tmp[i]=a[i]+1;else tmp[i]=a[i];
		ans=max(ans,tmp[i]);
	}
	For(i,1,n)	tmp[i]=a[i];
	Dow(i,1,n-1)
	{
		if(a[i]==a[i+1]||a[i]==tmp[i+1])	tmp[i]=a[i]+1;else tmp[i]=a[i];
		ans=max(ans,tmp[i]);
	}
	printf("%d",ans);
}
     

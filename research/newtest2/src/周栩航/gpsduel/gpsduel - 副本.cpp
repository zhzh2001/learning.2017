#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define maxn 100010
#define maxm 1000100
#define inf 1e9
#define ll long long
#define For(i,j,k) for(ll i=j;i<=k;i++)
#define Dow(i,j,k) for(ll i=k;i>=j;i--)
using namespace std;
inline void read(ll &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
ll dist[maxn],poi1[maxm],poi2[maxn],poi3[maxm],f1[maxn],f2[maxn],f3[maxn],nxt1[maxm],nxt2[maxm],nxt3[maxm],last[maxn],v3[maxm],v1[maxm],v2[maxm];
ll n,m,S,T,x,y,z1,z2,cnt1,cnt2,w,cnt3;
inline void add1(ll x,ll y,ll z){poi1[++cnt1]=y;nxt1[cnt1]=f1[x];v1[cnt1]=z;f1[x]=cnt1;}
inline void add2(ll x,ll y,ll z){poi2[++cnt2]=y;nxt2[cnt2]=f2[x];v2[cnt2]=z;f2[x]=cnt2;}
inline void add3(ll x,ll y){poi3[++cnt3]=y;nxt3[cnt3]=f3[x];f3[x]=cnt3;}
ll q[maxm+5];bool inq[maxn];
map<ll,ll>mp[100001];
inline void spfa1()
{
	For(i,1,n)	dist[i]=1e9;
	q[1]=T;dist[T]=0;
	ll l=0,r=1;
	while(l!=r)
	{
		l=(l+1)%maxm;
		ll x=q[l];inq[x]=0;
		for(ll i=f1[x];i;i=nxt1[i])
		{
			if(v1[i]+dist[x]<dist[poi1[i]])
			{
				ll t=poi1[i];
				
				dist[t]=dist[x]+v1[i];
				if(!inq[t])	r=(r+1)%maxm,q[r]=t,inq[t]=1;
				last[t]=x;	
			}
		}
	}
	For(i,1,n-1)	mp[i][last[i]]++;	
	write(dist[S]);putchar(' ');
}
inline void spfa2()
{
	For(i,1,n)	dist[i]=1e9,inq[i]=0 ;
	q[1]=T;dist[T]=0;
	ll l=0,r=1;
	while(l!=r)
	{
		l=(l+1)%maxm;
		ll x=q[l];inq[x]=0;
		for(ll i=f2[x];i;i=nxt2[i])
		{
			if(v2[i]+dist[x]<dist[poi2[i]])
			{
				ll t=poi2[i];
				if(!inq[t])	r=(r+1)%maxm,q[r]=t,inq[t]=1;
				dist[t]=dist[x]+v2[i];
				last[t]=x;	
			}
		}
	}
	For(i,1,n-1)	mp[i][last[i]]++;
	write(dist[S]);putchar(' ');
}
inline void spfa3()
{
	For(i,1,n)	dist[i]=1e9,inq[i]=0;
	q[1]=S;dist[S]=0;	 
	ll l=0,r=1;
	while(l!=r)
	{
		l=(l+1)%maxm;
		ll x=q[l];inq[x]=0;
		for(ll i=f3[x];i;i=nxt3[i])
		{
			if(dist[x]+2-v3[i]<dist[poi3[i]])
			{
				ll t=poi3[i];
				if(!inq[t])	r=(r+1)%maxm,q[r]=t,inq[t]=1;
				dist[t]=dist[x]+2-v3[i];
			}
		}
	}
	write(dist[T]);putchar('\n');
}
int main()
{
	freopen("gpsduel.in","r",stdin);freopen("gpsduel.out","w",stdout);
	read(n);read(m);read(w);read(S);read(T);
	For(i,1,m)	read(x),read(y),read(z1),read(z2),add1(x,y,z1),add1(y,x,z1),add2(x,y,z2),add2(y,x,z2),add3(x,y),add3(y,x);
	For(i,1,w)	read(x),read(y),read(z1),read(z2),add1(y,x,z1),add2(y,x,z2),add3(x,y);
	spfa1();
	spfa2();
	For(i,1,n)
		for(int j=f3[i];j;j=nxt3[j])	v3[j]=mp[i][poi3[j]];	
	spfa3();
}
     

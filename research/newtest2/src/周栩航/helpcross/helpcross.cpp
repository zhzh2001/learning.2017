#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int num[2000001],rson[2000001],lson[2000001],mi[2000001],L[2000001],R[2000001],ANS1[500001],ANS2[500001];
int ans1,ans2,n,m,a[500001],l[500001],r[500001],tot,cnt,bj[500001],q[500001],tot2;
int to[500001],Can[500001],rt,tot1;
map<int,int>MP;
inline void build(int &x,int l,int r)
{
	x=++tot1;
	L[x]=l;R[x]=r;
	if(l==r){num[x]=to[l];if(num[x])mi[x]=Can[num[x]];else mi[x]=1e9;return;}
	int mid=(l+r)>>1;
	build(lson[x],l,mid);build(rson[x],mid+1,r);
	if(mi[lson[x]]>mi[rson[x]])	mi[x]=mi[rson[x]],num[x]=num[rson[x]];
	else	mi[x]=mi[lson[x]],num[x]=num[lson[x]];
}
inline void query(int x,int l,int r)
{
	if(l<=L[x]&&R[x]<=r)
	{
		if(ans1>mi[x])	ans1=mi[x],ans2=num[x];
		return;
	}
	if(l<=R[lson[x]])	query(lson[x],l,r);
	if(r>=L[rson[x]])	query(rson[x],l,r);
}
inline void update(int x,int que)
{
	if(L[x]==R[x])
	{
		mi[x]=1e9;num[x]=0;
		return;
	}
	if(que<=R[lson[x]])	update(lson[x],que);else update(rson[x],que);
	
	if(mi[lson[x]]>mi[rson[x]])	mi[x]=mi[rson[x]],num[x]=num[rson[x]];
	else	mi[x]=mi[lson[x]],num[x]=num[lson[x]];
}
inline void doit()
{
	build(rt,1,tot);
	For(i,1,m)
	{
		ans1=1e9;ans2=0;
		query(1,l[i],r[i]);
		if(ans2)	ANS1[++cnt]=i,ANS2[cnt]=ans2;
		update(1,a[ans2]);
	}
	writeln(cnt);
	For(i,1,cnt)	write(ANS2[i]),putchar(' '),writeln(ANS1[i]);
}
int main()
{
	freopen("helpcross.in","r",stdin);freopen("helpcross.out","w",stdout);
	read(n);read(m);
	For(i,1,n)	read(a[i]);
	For(i,1,m)	read(l[i]),read(r[i]);
	For(i,1,n)	q[++tot2]=a[i];For(i,1,m)	q[++tot2]=l[i],q[++tot2]=r[i];
	sort(q+1,q+tot2+1);
	For(i,1,tot2)	if(!MP[q[i]])	MP[q[i]]=++tot;
	For(i,1,n)	a[i]=MP[a[i]];
	For(i,1,m)	l[i]=MP[l[i]],r[i]=MP[r[i]];
	For(i,1,m)	bj[l[i]]++,bj[r[i]+1]--;
	For(i,1,tot)	Can[i]=Can[i-1]+bj[i];
	For(i,1,n)	to[a[i]]=i;
	doit(); 
}
     

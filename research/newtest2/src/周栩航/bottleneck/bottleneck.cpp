#include <iostream>
#include <cstdio>
#include <cmath>
#include <algorithm>
#include <queue>
#include <string>
#include <map>
#include <cstring>
#include <ctime>
#include <vector>
#define inf 1e9
#define ll long long
#define For(i,j,k) for(int i=j;i<=k;i++)
#define Dow(i,j,k) for(int i=k;i>=j;i--)
using namespace std;
inline void read(int &tx){   ll x=0,f=1;char ch=getchar();   while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}  while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}  tx=x*f; }
inline void write(ll x){    if (x<0) putchar('-'),x=-x; if (x>=10) write(x/10);   putchar(x%10+'0');  }
inline void writeln(ll x){write(x);puts("");}
using namespace std;
int poi[1000001],nxt[1000001],f[100001],hav[100001],sem[1000001],cnt;
int n,k;
struct node{int ans,v,num;}que[1000001];
inline void add(int x,int y){poi[++cnt]=y;nxt[cnt]=f[x];f[x]=cnt;}
inline bool cmp(node x,node y){return x.v<y.v;}
inline bool cmp1(node x,node y){return x.num<y.num;}
inline int dfs(int x)
{
	for(int i=f[x];i;i=nxt[i])
		hav[x]+=dfs(poi[i]);
	if(x==1)	return hav[1];
	if(hav[x]<=sem[x])	{int tmp=hav[x];hav[x]=0;return tmp;}
	hav[x]-=sem[x];return sem[x];
}
inline void doit()
{
	int time=0,tot=1;
	while(1)
	{
		time++;int tmp=dfs(1);
		while(time==que[tot].v)	que[tot].ans=tmp,tot++;	
		if(tot>k)	return;
	}
}
int main()
{
	freopen("bottleneck.in","r",stdin);freopen("bottleneck.out","w",stdout);
	read(n);read(k);
	For(i,2,n)
	{
		int x;
		read(x);read(hav[i]);read(sem[i]);
		add(x,i);
	}
	For(i,1,k)	read(que[i].v),que[i].num=i;
	sort(que+1,que+k+1,cmp);
	doit();
	sort(que+1,que+k+1,cmp1);
	For(i,1,k)	writeln(que[i].ans);
} 
/*
4 4
1 1 5
2 3 7
3 12 3
2
3
4
5

*/

#include<fstream>
#include<vector>
using namespace std;
ifstream fin("bottleneck.in");
ofstream fout("bottleneck.out");
const int N=100005,K=100005;
int c[N],m[N];
vector<int> mat[N];
long long dfs(int k,int t)
{
	long long sum=c[k];
	for(vector<int>::iterator it=mat[k].begin();it!=mat[k].end();++it)
		sum+=dfs(*it,t);
	return min(sum,(long long)m[k]*t);
}
int main()
{
	int n,k;
	fin>>n>>k;
	m[1]=1e9;
	for(int i=2;i<=n;i++)
	{
		int p;
		fin>>p>>c[i]>>m[i];
		mat[p].push_back(i);
	}
	while(k--)
	{
		int t;
		fin>>t;
		fout<<dfs(1,t)<<endl;
	}
	return 0;
}
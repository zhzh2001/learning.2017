#include<fstream>
using namespace std;
ifstream fin("2048.in");
ofstream fout("2048.out");
const int N=2005;
int a[N],f[N][N];
int main()
{
	int n;
	fin>>n;
	for(int i=1;i<=n;i++)
	{
		fin>>a[i];
		f[i][i]=a[i];
	}
	int ans=0;
	for(int i=n;i;i--)
		for(int j=i+1;j<=n;j++)
		{
			for(int k=i;k<j;k++)
				if(f[i][k]==f[k+1][j])
					f[i][j]=max(f[i][j],f[i][k]+1);
			ans=max(ans,f[i][j]);
		}
	fout<<ans<<endl;
	return 0;
}
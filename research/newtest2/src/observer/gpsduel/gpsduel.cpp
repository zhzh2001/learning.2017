#include<fstream>
#include<algorithm>
#include<queue>
using namespace std;
ifstream fin("gpsduel.in");
ofstream fout("gpsduel.out");
const int N=100005,M=1100005;
const long long INF=0x3f3f3f3f3f3f3f3fll;
struct graph
{
	int head[N],v[M],w[M],nxt[M],e;
	void add_edge(int u,int v,int w)
	{
		this->v[++e]=v;
		this->w[e]=w;
		nxt[e]=head[u];
		head[u]=e;
	}
}mat1,mat2,mat;
long long d1[N],d2[N],d[N];
bool inQ[N];
void spfa(const graph& mat,long long d[],int s,int n)
{
	fill(d+1,d+n+1,INF);
	d[s]=0;
	deque<int> Q;
	Q.push_back(s);
	fill(inQ+1,inQ+n+1,false);
	inQ[s]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop_front();
		inQ[k]=false;
		for(int i=mat.head[k];i;i=mat.nxt[i])
			if(d[k]+mat.w[i]<d[mat.v[i]])
			{
				d[mat.v[i]]=d[k]+mat.w[i];
				if(!inQ[mat.v[i]])
				{
					if(!Q.empty()&&d[mat.v[i]]<d[Q.front()])
						Q.push_front(mat.v[i]);
					else
						Q.push_back(mat.v[i]);
					inQ[mat.v[i]]=true;
				}
			}
	}
}
int main()
{
	int n,m,w,s,t;
	fin>>n>>m>>w>>s>>t;
	while(m--)
	{
		int u,v,w1,w2;
		fin>>u>>v>>w1>>w2;
		mat1.add_edge(u,v,w1);
		mat1.add_edge(v,u,w1);
		mat2.add_edge(u,v,w2);
		mat2.add_edge(v,u,w2);
	}
	while(w--)
	{
		int u,v,w1,w2;
		fin>>u>>v>>w1>>w2;
		mat1.add_edge(v,u,w1);
		mat2.add_edge(v,u,w2);
	}
	spfa(mat1,d1,t,n);
	spfa(mat2,d2,t,n);
	for(int i=1;i<=n;i++)
		for(int j=mat1.head[i];j;j=mat1.nxt[j])
		{
			int u=mat1.v[j],delta1=d1[u]-d1[i],delta2=d2[u]-d2[i];
			mat.add_edge(u,i,(mat1.w[j]>delta1)+(mat2.w[j]>delta2));
		}
	spfa(mat,d,s,n);
	if(d1[s]==INF)
		fout<<"inf ";
	else
		fout<<d1[s]<<' ';
	if(d2[s]==INF)
		fout<<"inf ";
	else
		fout<<d2[s]<<' ';
	if(d[t]==INF)
		fout<<-1<<endl;
	else
		fout<<d[t]<<endl;
	return 0;
}
#include<fstream>
#include<algorithm>
using namespace std;
ifstream fin("helpcross.in");
ofstream fout("helpcross.out");
const int N=100005;
int n,m,c[N],l[N],r[N];
bool vis[N];
int match[N];
bool dfs(int k)
{
	for(int i=1;i<=m;i++)
		if(!vis[i]&&l[i]<=c[k]&&r[i]>=c[k])
		{
			vis[i]=true;
			if(!match[i]||dfs(match[i]))
			{
				match[i]=k;
				return true;
			}
		}
	return false;
}
int main()
{
	fin>>n>>m;
	for(int i=1;i<=n;i++)
		fin>>c[i];
	for(int i=1;i<=m;i++)
		fin>>l[i]>>r[i];
	int ans=0;
	for(int i=1;i<=n;i++)
	{
		fill(vis+1,vis+m+1,false);
		ans+=dfs(i);
	}
	fout<<ans<<endl;
	for(int i=1;i<=m;i++)
		if(match[i])
			fout<<match[i]<<' '<<i<<endl;
	return 0;
}
#include<ctime>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
using namespace std;

#define rand (rand()<<15|rand())



int main() {
	srand((int)(time(0)));
	freopen("helpcross.in","w",stdout);
	int n=100000,m=100000;
	printf("%d %d\n",n,m);
	int mod=1e9,k=n;
	while (k--) printf("%d\n",rand%mod);
	while (m--) {
		int l=rand%n+1,r=rand%n+1;
		if (l>r) swap(l,r);
		printf("%d %d\n",l,r);
	}
}


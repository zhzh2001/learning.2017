#include<cstdio>
#include<vector>
#include<algorithm>

using namespace std;

const int N=400035;

#define For(i,x,y) for(int i=x;i<=y;i++)

int head[N<<2];

struct Info {
	int x,y,id;
	bool operator < (const Info &p) const {
		return y==p.y?x>p.x:y<p.y;
	}
}a[N],b[N],E[N<<2];

vector<Info>Ans;
//vector<int>VI[N<<2];

#define mid (l+r>>1)

int sum[N<<2],c,n,hs[N],cnt,tot;

int rd() {
	char c=getchar(); int t=0;
	while (c<48 || c>57) c=getchar();
	while (c>47 && c<58) t=(t<<1)+(t<<3)+c-48,c=getchar(); return t;
}

void ins(int o,int l,int r,int x,int id) {
	sum[o]++;
	if (l==r) {
//		VI[o].push_back(id);
		E[++tot]=(Info){head[o],id,0},head[o]=tot;
		return;
	}
	if (x<=mid) ins(o<<1,l,mid,x,id);
	else ins(o<<1|1,mid+1,r,x,id);
}

bool find(int o,int l,int r,int id) {
	if (!sum[o]) return 0;
	
	sum[o]--;
	if (l==r) {
//		int tmp=VI[o].back();
//		VI[o].pop_back();
		int tmp=E[head[o]].y;
		head[o]=E[head[o]].x;
		Ans.push_back((Info){tmp,id,0}); return 1;
	}
	if (find(o<<1,l,mid,id)) return 1;
	else return find(o<<1|1,mid+1,r,id);
}

bool qry(int o,int l,int r,int x,int y,int id) {
	if (!sum[o]) return 0;
	if (l==x && r==y) return find(o,l,r,id);
	
	int Ret=0;
	if (y<=mid) Ret=qry(o<<1,l,mid,x,y,id);
	else if (x>mid) Ret=qry(o<<1|1,mid+1,r,x,y,id);
	else {
		if (qry(o<<1,l,mid,x,mid,id)) Ret=1;
		else Ret=qry(o<<1|1,mid+1,r,mid+1,y,id);
	}
	sum[o]=sum[o<<1]+sum[o<<1|1];
	return Ret;
}

void wt(int x) {
	if (x<10) putchar(x+48);
	else wt(x/10),putchar(x%10+48);
}

int main() {
	
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	
	scanf("%d%d",&c,&n);
	For (i,1,c) a[i].x=rd(),a[i].id=i,hs[++cnt]=a[i].x;
	For (i,1,n) b[i].x=rd(),b[i].y=rd(),b[i].id=i,hs[++cnt]=b[i].x,hs[++cnt]=b[i].y;
	
	sort(b+1,b+n+1);
	sort(hs+1,hs+cnt+1);
	cnt=unique(hs+1,hs+cnt+1)-hs-1;
	
	For (i,1,c) a[i].x=lower_bound(hs+1,hs+cnt+1,a[i].x)-hs;
	For (i,1,n) {
		b[i].x=lower_bound(hs+1,hs+cnt+1,b[i].x)-hs;
		b[i].y=lower_bound(hs+1,hs+cnt+1,b[i].y)-hs;
	}
	
	For (i,1,c) ins(1,1,cnt,a[i].x,a[i].id);
	For (i,1,n) qry(1,1,cnt,b[i].x,b[i].y,b[i].id);
	
	printf("%u\n",Ans.size());
	for (int i=0;i<Ans.size();i++)
		wt(Ans[i].x),putchar(32),wt(Ans[i].y),putchar(10);
	return 0;
}


#include<cstdio>
#include<cstdlib>
#include<utility>
#include<algorithm>

using namespace std;

const int N=100035;

#define x first
#define id second
#define For(i,x,y) for(int i=x;i<=y;i++)

int rd() {
	char c=getchar(); int t=0;
	while (c<48 || c>57) c=getchar();
	while (c>47 && c<58) t=(t<<1)+(t<<3)+c-48,c=getchar(); return t;
}

int head[N],cnt;
struct Edge { int to,nxt; }E[N<<1];
void Link(int u,int v) { E[++cnt]=(Edge){v,head[u]},head[u]=cnt; }

pair<int,int>Q[N];
int n,m,j,c[N],fa[N],lim[N];
long long now,con,a[N],f[N],ans[N];

void dfs(int x) {
	f[x]=0;
	for (int y,i=head[x];i;i=E[i].nxt) {
		dfs(y=E[i].to);
		f[x]+=min(a[y]+f[y],1ll*lim[y]);
		f[y]-=min(a[y]+f[y],1ll*lim[y]);
	}
}

void getF() {
	dfs(1),con=1e9;
	For (i,1,n) if (f[i]<0) {
		if (a[i]/(-f[i])<con) con=a[i]/f[i],VI.clear(),VI.push_back(i);
		else if (a[i]/(-f[i])==con) VI.push_back(i);
	}
}

int main() {
	
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);

	n=rd(),m=rd();
	For (i,2,n) {
		fa[i]=rd(),a[i]=rd();
		lim[i]=rd(),cnt+=(a[i]>0),Link(fa[i],i);
	}
	For (i,1,m) Q[i].x=rd(),Q[i].id=i;
	
	getF();
	sort(Q+1,Q+m+1);
	
	j=1;
	while (cnt>1) {
		while (j<=m && Q[j].x<=now+con)
			ans[Q[j].id]=a[1]+f[1]*(Q[j].x-now),j++;
		if (j>m) break;
		
		now+=con,cnt=0;
		For (i,1,n) a[i]=a[i]+f[i]*con,cnt+=(a[i]>0);
		getF();
	}
	while (j<=m) ans[Q[j].id]=a[1],j++;
	
	For (i,1,m) printf("%lld\n",ans[i]);
	return 0;
}

/*
4 8
1 1 5
2 12 7
3 12 3
0
1
2
3
4
5
50
100

*/

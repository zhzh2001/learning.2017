#include<cstdio>
#include<cstring>

#define For(i,x,y) for(int i=x;i<=y;i++)

using namespace std;

const int N=(1<<18)+5;

int rd() {
	char c=getchar(); int t=0;
	while (c<48 || c>57) c=getchar();
	while (c>47 && c<58) t=(t<<1)+(t<<3)+c-48,c=getchar(); return t;
}

int f[61][N],n,ans;

int main() {
	
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	
	n=rd();
	For (i,1,n) f[rd()][i]=i+1;
	
	For (i,1,59)
	For (j,1,n)
		if (!f[i+1][j]) f[i+1][j]=f[i][f[i][j]];
		
	For (i,1,60) For (j,1,n)
		if (f[i][j]) ans=i;
	
	printf("%d\n",ans);
	return 0;
}

#include<cstdio>
#include<cstdlib>
#include<algorithm>

#define For(i,x,y) for(int i=x;i<=y;i++)

using namespace std;

typedef long long ll;

int rd() {
	char c=getchar(); int t=0,f=1;
	while (c<48 || c>57) f=(c=='-'?-1:1),c=getchar();
	while (c>47 && c<58) t=(t<<1)+(t<<3)+c-48,c=getchar(); return t*f;
}

const int N=200035;
const int M=2000035;

ll f0[N],f1[N],f2[N];
int head[N],cnt,n,m,S,T,w;
struct Edge { int fr,to,nxt,v[3]; }E[M];

void Link(int u,int v,int t0,int t1) {
	cnt++;
	E[cnt].fr=u,E[cnt].to=v,E[cnt].nxt=head[u];
	E[cnt].v[0]=t0,E[cnt].v[1]=t1,E[cnt].v[2]=2,head[u]=cnt;
}

void spfa(ll *f,int op) {
	For (i,1,n) f[i]=1e17;
	static int Q[60000005],vis[N],h,t,x;
	
	h=f[T]=0,Q[t=1]=T,vis[T]=1;
	while (h<t) {
		x=Q[++h];
		for (int y,i=head[x];i;i=E[i].nxt)
			if (f[y=E[i].to]>f[x]+E[i].v[op]) {
				f[y]=f[x]+E[i].v[op];
				if (!vis[y]) vis[Q[++t]=y]=1;
			}
		vis[x]=0;
	}
	
	For (i,1,cnt) if (f[E[i].fr]+E[i].v[op]==f[E[i].to]) E[i].v[2]--;
}

int main() {
	
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	
	n=rd(),m=rd(),w=rd(),S=rd(),T=rd();
	For (i,1,m) {
		int x=rd(),y=rd(),t0=rd(),t1=rd();
		Link(x,y,t0,t1),Link(y,x,t0,t1);
	}
	For (i,1,w) {
		int x=rd(),y=rd(),t0=rd(),t1=rd();
		Link(y,x,t0,t1);
	}
	
	spfa(f0,0);
	if (f0[S]==1e17) return puts("inf inf -1"),0;
	spfa(f1,1);
	spfa(f2,2);
	
	printf("%I64d %I64d %I64d\n",f0[S],f1[S],f2[S]);
	return 0;
}

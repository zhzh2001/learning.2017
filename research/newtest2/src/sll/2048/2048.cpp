#include<bits/stdc++.h>
#define N 303
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n;
int a[N];
bool f[N][61];
bool dp[N][N][61],vis[N][N][61];
bool dfs(int l,int r,int c)
{
	int u=0;
	if(!c)return false;
	if(l==r)return a[l]==c;
	if(vis[l][r][c])return dp[l][r][c];
	vis[l][r][c]=1,dp[l][r][c]=0;
	for(int k=l+1;k<=r;k++)
		dp[l][r][c]|=(dfs(l,k-1,c-1)&dfs(k,r,c-1));
	return dp[l][r][c];
}
bool is[N][N];int C[N][N];
int cal(int l,int r)
{
	if(is[l][r])return C[l][r];
	is[l][r]=1,C[l][r]=0;
	if(l==r)return C[l][r]=a[l];
	for(int k=l+1;k<=r;k++)
	{
		int txp=cal(l,k-1),tmp=cal(k,r);
		if(txp&&tmp&&txp==tmp)return C[l][r]=tmp+1;
	}
	return 0;
}
int ans;
int main()
{
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++)a[i]=read();
	f[0][0]=1;
	for(int i=1;i<=n;i++)f[i][a[i]]=1;
	for(int j=2;j<=60;j++)
	{
		f[0][j]=1;
		for(int i=1;i<=n;i++)
		{
			for(int k=0;k<i;k++)
			{	
				f[i][j]|=(f[k][j-1]&(cal(k+1,i)==j-1));
				if(f[i][j])break;
			}
		}
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=60;j++)
			if(f[i][j])ans=max(ans,j);
	printf("%d\n",ans);
}

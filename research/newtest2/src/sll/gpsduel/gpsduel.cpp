#include<bits/stdc++.h>
#define lca long long
#define inf 1ll<<50
#define N 500500
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m,w,s,t;
int head[N],pos;
struct node{int x,y,t[3];}rd[N],wd[N];
struct edge{int id,to,next,c;}e[N<<1];
void add(int a,int b,int c,int id)
{pos++;e[pos].id=id,e[pos].c=c,e[pos].to=b,e[pos].next=head[a],head[a]=pos;}
void insert(int a,int b,int c,int id){add(a,b,c,id);add(b,a,c,id);}
void build(int op)
{
	memset(head,0,sizeof(head));pos=0;
	for(int i=1;i<=m;i++)
		insert(rd[i].x,rd[i].y,rd[i].t[op],i);
	for(int i=1;i<=w;i++)
		add(wd[i].y,wd[i].x,wd[i].t[op],i+m);
}
queue<int>Q;bool vis[N];lca dis[N],d[N];
void spfa(int op,int st)
{
	for(int i=1;i<=n;i++)dis[i]=inf,vis[i]=0;
	dis[st]=0,vis[st]=1,Q.push(st);
	int sz=0;
	while(!Q.empty())
	{
		if(sz>30000000){while(!Q.empty())Q.pop();return;}
		int u=Q.front();Q.pop();vis[u]=0;
		for(int i=head[u];i;i=e[i].next)
		{
			int v=e[i].to;sz++;
			if(dis[v]>dis[u]+e[i].c)
			{
				dis[v]=dis[u]+e[i].c;
				if(!vis[v])Q.push(v),vis[v]=1;
			}
		}
	}
}
lca Dis[N];
int cal(int u,int v,int id)
{
	int ret=0;
	if(id<=m)
	{
		if(dis[v]+rd[id].t[1]!=dis[u])ret++;
		if(d[v]+rd[id].t[0]!=d[u])ret++;
	}
	else
	{
		id-=m;
		if(dis[v]+wd[id].t[1]!=dis[u])ret++;
		if(d[v]+wd[id].t[0]!=d[u])ret++;
	}
	return ret;
}
void Spfa(int st)
{
	for(int i=1;i<=n;i++)Dis[i]=inf,vis[i]=0;
	Dis[st]=0,vis[st]=1,Q.push(st);int sz=0;
	while(!Q.empty())
	{
		int u=Q.front();Q.pop();vis[u]=0;
		if(sz>30000000)return ;
		for(int i=head[u];i;i=e[i].next)
		{
			sz++;
			int v=e[i].to,c=cal(u,v,e[i].id);
			if(Dis[v]>Dis[u]+c)
			{
				Dis[v]=Dis[u]+c;
				if(!vis[v])Q.push(v),vis[v]=1;
			}
		}
	}
}
int main()
{
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	n=read(),m=read();
	w=read(),s=read(),t=read();
	for(int i=1;i<=m;i++)
		rd[i].x=read(),rd[i].y=read(),
		rd[i].t[0]=read(),rd[i].t[1]=read();
	for(int i=1;i<=w;i++)
		wd[i].x=read(),wd[i].y=read(),
		wd[i].t[0]=read(),wd[i].t[1]=read();
	build(0),spfa(0,t);
	for(int i=1;i<=n;i++)d[i]=dis[i];
	build(1),spfa(1,t);
	for(int i=1;i<=m;i++)swap(rd[i].x,rd[i].y);
	for(int i=1;i<=w;i++)swap(wd[i].x,wd[i].y);
	build(1),Spfa(s);
	printf("%lld %lld %lld\n",d[s],dis[s],Dis[t]);
}

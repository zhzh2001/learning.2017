#include<bits/stdc++.h>
#define N 3020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m;
struct cik{int t,id;}t[N];
struct node{int x,y,id;}p[N];
bool cmp (node a,node b)
{
	if(a.x!=b.x)return a.x<b.x;
	else return a.y<b.y;
}
int ans;
bool ok[N][N];
bool used[N];
int match[N];
bool dfs(int u)
{
	for(int i=1;i<=m;i++)if(ok[u][i])
	{
		if(used[i])continue;used[i]=1;
		if(!match[i]||dfs(match[i]))
		{	match[i]=u;return true;}
	}
	return false;
}
void hungry()
{
	for(int i=1;i<=n;i++)
	{
		memset(used,0,sizeof(used));
		ans+=dfs(i);
	}
}
int ret;
bool gan[N],go[N];
int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("ck.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++)t[i].t=read(),t[i].id=i;
	for(int i=1;i<=m;i++)p[i].id=i,p[i].x=read(),p[i].y=read();
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(p[j].x<=t[i].t&&t[i].t<=p[j].y)ok[i][j]=1;
	fclose(stdin);
	freopen("helpcross.out","r",stdin);
	ans=ret=read();if(ret!=ans){printf("NO\n");return 0;}
	for(int i=1;i<=ret;i++)
	{
		int x=read(),y=read();
		if(!ok[x][y]){printf("NO\n");return 0;}
		if(go[y]||gan[x]){printf("NO\n");return 0;}
		gan[x]=1,go[y]=1;
	}printf("YES\n");
}

#include<bits/stdc++.h>
#define N 300020
using namespace std;
inline int read()
{
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
int n,m;
struct cik{int t,id;}t[N];
bool operator < (cik a,cik b){return a.t<b.t;}
struct node{int x,y,id;}p[N];
bool cmp (node a,node b)
{
	if(a.x!=b.x)return a.x<b.x;
	else return a.y<b.y;
}
bool operator < (node a,node b){return a.y>b.y;}
priority_queue<node>Q;
int ans,to[N];
int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	n=read(),m=read();
	for(int i=1;i<=n;i++)t[i].t=read(),t[i].id=i;
	for(int i=1;i<=m;i++)p[i].id=i,p[i].x=read(),p[i].y=read();
	sort(p+1,p+1+m,cmp);sort(t+1,t+1+n);
	int now=1;
	for(int i=1;i<=n;i++)
	{
		int x=t[i].t;node u;
		while(p[now].x<=x){Q.push(p[now]);now++;}
		while(!Q.empty())
		{u=Q.top();if(x<=u.y)break;Q.pop();}
		if(Q.empty())continue;
		ans++;u=Q.top();Q.pop();
		to[t[i].id]=u.id;
	}
	printf("%d\n",ans);
	for(int i=1;i<=n;i++)
		if(to[i])printf("%d %d\n",i,to[i]);
}

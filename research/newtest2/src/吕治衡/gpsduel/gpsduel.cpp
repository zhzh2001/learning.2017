#include<bits/stdc++.h>
using namespace std;
long long n,m,w,s,t,l,r,d[2000000],len1[200000],len2[200000],f[200000];
long long last1[200000],last2[200000],last3[200000],a1[2000000][3],a2[2000000][3],a3[2000000][3];
long long f1[10000000][2],f2[10000000][2],kk1,kk2,kk3,kkk1,kkk2,lastt1[200000],lastt2[200000];
long long x,y,z1,z2,ans;
bool flag[200000]; 
void doit1(long long x,long long y,long long z){a1[++kk1][0]=last1[x];a1[kk1][1]=y;a1[kk1][2]=z;last1[x]=kk1;}
void doit2(long long x,long long y,long long z){a2[++kk2][0]=last2[x];a2[kk2][1]=y;a2[kk2][2]=z;last2[x]=kk2;}
void doit3(long long x,long long y,long long z){a3[++kk3][0]=last3[x];a3[kk3][1]=y;a3[kk3][2]=z;last3[x]=kk3;}
void doitt1(long long x,long long y){f1[++kkk1][0]=lastt1[x];f1[kkk1][1]=y;lastt1[x]=kkk1;}
void doitt2(long long x,long long y){f2[++kkk2][0]=lastt2[x];f2[kkk2][1]=y;lastt2[x]=kkk2;}
long long spfa1(){
		memset(len1,10,sizeof(len1));
		l=0;r=1;d[1]=t;len1[t]=0;flag[t]=true;
		while (l!=r){
			l=l<1000007?l+1:l-1000006;
			for (long long i=last1[d[l]];i;i=a1[i][0]){
					if (len1[d[l]]+a1[i][2]==len1[a1[i][1]])doitt1(a1[i][1],i);
					if (len1[d[l]]+a1[i][2]<len1[a1[i][1]]){
							lastt1[a1[i][1]]=0;doitt1(a1[i][1],i);
							len1[a1[i][1]]=len1[d[l]]+a1[i][2];
							if (!flag[a1[i][1]]){
								r=r<1000007?r+1:r-1000006;
								d[r]=a1[i][1];flag[a1[i][1]]=true;
							}
						}
				}
			flag[d[l]]=false;
		}
		return len1[s];
	}
long long spfa2(){
		memset(len2,10,sizeof(len2));
		l=0;r=1;d[1]=t;len2[t]=0;flag[t]=true;
		while (l!=r){
			l=l<1000007?l+1:l-1000006;
			for (long long i=last2[d[l]];i;i=a2[i][0]){
					if (len2[d[l]]+a2[i][2]==len2[a2[i][1]])doitt2(a2[i][1],i);
					if (len2[d[l]]+a2[i][2]<len2[a2[i][1]]){
							lastt2[a2[i][1]]=0;doitt2(a2[i][1],i);
							len2[a2[i][1]]=len2[d[l]]+a2[i][2];
							if (!flag[a2[i][1]]){
								r=r<1000007?r+1:r-1000006;
								d[r]=a2[i][1];flag[a2[i][1]]=true;
							}
						}
				}
			flag[d[l]]=false;
		}
		return len2[s];
	}
long long spfa3(){
		memset(f,10,sizeof(f));
		l=0;r=1;d[1]=t;f[t]=0;flag[t]=true;
		while (l!=r){
			l=l<1000007?l+1:l-1000006;
			for (long long i=last3[d[l]];i;i=a3[i][0]){
				if (f[d[l]]+a3[i][2]<f[a3[i][1]]){
					f[a3[i][1]]=f[d[l]]+a3[i][2];
					if (!flag[a3[i][1]]){
						r=r<1000007?r+1:r-1000006;
						d[r]=a3[i][1];flag[a3[i][1]]=true;
					}
				}
			}
			flag[d[l]]=false;
		}
		return f[s];
	}
int main(){
	freopen("gpsduel.in","r",stdin);freopen("gpsduel.out","w",stdout);
	ios::sync_with_stdio(false);
	cin>>n>>m>>w>>s>>t;
	for (long long i=1;i<=m;i++){
		cin>>x>>y>>z1>>z2;
		doit1(x,y,z1);doit1(y,x,z1);
		doit2(x,y,z2);doit2(y,x,z2);
		doit3(x,y,2);doit3(y,x,2);
	}
	for (long long i=1;i<=w;i++){
		cin>>x>>y>>z1>>z2;
		doit1(y,x,z1);doit2(y,x,z2);doit3(y,x,2);
	}
	x=spfa1();y=spfa2();
	if (x>1e15){cout<<"inf inf -1"<<endl;return 0;}
		else cout<<x<<' '<<y<<' ';
	for (long long i=1;i<=n;i++){
			for (long long j=lastt1[i];j;j=f1[j][0])a3[f1[j][1]][2]--;//,ans+=a3[f1[j][1]][2]<0;
			for (long long j=lastt2[i];j;j=f2[j][0])a3[f2[j][1]][2]--;//,ans+=a3[f2[j][1]][2]<0;
		}
//	cout<<ans<<endl;
//	if (ans)return 0;
//	for (int i=1;i<=kk3;i++)a3[i][2]=max(a3[i][2],0ll);
	cout<<spfa3()<<endl;
} 

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,M=1200005;
int n,m,w,S,T,ll,fr[M],to[M],nxt[M],hd[N],fl1[M],fl2[M],fl3[M],qu[N];
LL ds1[N],ds2[N],cnt[N];bool inq[N];

inline void adde(int r){
	int u=read(),v=read(),w1=read(),w2=read();
	if (r)
	to[++ll]=v,fr[ll]=u,nxt[ll]=hd[u],hd[u]=ll,fl1[ll]=w1,fl2[ll]=w2;
	to[++ll]=u,fr[ll]=v,nxt[ll]=hd[v],hd[v]=ll,fl1[ll]=w1,fl2[ll]=w2;
}

void cal(int fl[],LL ds[]){
	memset(ds+1,0x3f,sizeof(LL)*n);
	memset(inq+1,0,sizeof(bool)*n);
	int l=0,r=1;
	ds[T]=0,qu[0]=T,inq[T]=1;
	while (l!=r){
		int u=qu[l++];if (l==N) l=0;
		for (int i=hd[u],v;i;i=nxt[i])
			if (ds[v=to[i]]>ds[u]+fl[i]){
				ds[v]=ds[u]+fl[i];
				if (!inq[v]) {
					qu[r++]=v,inq[v]=1;
					if (r==N) r=0;
				}
			}
		inq[u]=0;
	}
}

int main()
{
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	n=read(),m=read(),w=read(),S=read(),T=read();
	for (int i=1;i<=m;i++) adde(1);
	for (int i=1;i<=w;i++) adde(0);
	cal(fl1,ds1),cal(fl2,ds2);
	for (int i=1;i<=ll;i++){
		if (ds1[fr[i]]+fl1[i]!=ds1[to[i]]) fl3[i]++;
		if (ds2[fr[i]]+fl2[i]!=ds2[to[i]]) fl3[i]++;
	}
	cal(fl3,cnt);
	printf("%lld %lld %lld\n",ds1[S],ds2[S],cnt[S]);
	return 0;
}

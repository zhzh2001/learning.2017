#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005,M=800005,oo=0x3f3f3f3f;
int n,m,L,num[N*3],A[N],B[N],C[N],fa[M],ls[M],rs[M],R,S,T,pos[N*3],
	ll=1,to[M*5],nxt[M*5],hd[M],fl[M*5],cur[M],ans,st[N],dis[N];

inline void adde(int u,int v,int w){
	to[++ll]=v,nxt[ll]=hd[u],hd[u]=ll,fl[ll]=w;
	to[++ll]=u,nxt[ll]=hd[v],hd[v]=ll,fl[ll]=0;
}

int build(int l,int r){
	if (l==r) return pos[l]=++R;
	int mid=l+r>>1,o=++R;
	ls[o]=build(l,mid),rs[o]=build(mid+1,r);
	fa[ls[o]]=fa[rs[o]]=o;
	adde(ls[o],o,oo);
	adde(rs[o],o,oo);
	return o;
}

void ins(int p,int o,int l,int r){
	if (A[p]<=l&&r<=B[p]) {adde(o,R+p,1);return;}
	int mid=l+r>>1;
	if (A[p]<=mid) ins(p,ls[o],l,mid);
	if (B[p]>mid) ins(p,rs[o],mid+1,r);
}

inline void solve(int A[],int n){
	for (int i=1;i<=n;i++) A[i]=lower_bound(num+1,num+L+1,A[i])-num;
}

void cal(int a)
{
	int p=R+m+a,tp=0;
	for (int q=0;;q=0){
		for (int i=hd[p];i;i=nxt[i]) if (fl[i]) q=to[i],st[++tp]=i;
		p=q;
		if (!p) return;
		if (p>R&&p<=R+m) break;
	}
	p-=R;
	printf("%d %d\n",a,p);
	for (int i=1;i<=tp;i++) if (to[i]>R&&to[i]<=R+m) fl[st[i]]=0;
}

bool bfs(){
	memset(dis+1,0,sizeof(int)*T);
	int l=0,r=1;
	st[0]=S,dis[S]=1;
	while (l!=r){
		int u=st[l++];
		if (u==T) return 1;
		for (int i=hd[u],v;i;i=nxt[i]) if (fl[i]&&!dis[v=to[i]])
			dis[v]=dis[u]+1,st[r++]=v;
	}
	return 0;
}

int dfs(int u,int f){
	if (u==T) return f;
	int used=0,tmp;
	for (int i=cur[u],v;i;i=nxt[i])
		if (fl[i]&&dis[v=to[i]]==dis[u]+1&&(tmp=dfs(v,min(fl[i],f-used)))){
			fl[i]-=tmp,fl[i^1]+=tmp;
			used+=tmp;
			if (fl[i]) cur[u]=i;
			if (f==used) return f;
		}
	if (!used) dis[u]=0;
	return used;
}

int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	n=read();m=read();
	for (int i=1;i<=n;i++) C[i]=num[++L]=read();
	for (int i=1;i<=m;i++) A[i]=num[++L]=read(),B[i]=num[++L]=read();
	sort(num+1,num+L+1);
	L=unique(num+1,num+L+1)-num-1;
	solve(A,m),solve(B,m),solve(C,n);
	build(1,L);
	S=R+n+m+1,T=S+1;
	for (int i=1;i<=m;i++) ins(i,1,1,L),adde(R+i,T,1);
	for (int i=1;i<=n;i++) adde(R+m+i,pos[C[i]],1),adde(S,R+m+i,1);
	while (bfs()){
		memcpy(cur+1,hd+1,sizeof(int)*T);
		ans+=dfs(S,oo);
	}
	printf("%d\n",ans);
	for (int i=2;i<=ll;i+=2) fl[i]=fl[i^1],fl[i^1]=0;
	for (int i=1;i<=n;i++) cal(i);
	return 0;
}

#include<iostream>
#include<algorithm>
#include<cstdio>
#include<cstdlib>
#include<cstring>
using namespace std;
typedef long long LL;
inline int read(){
	int x=0,f=0,c=getchar();
	for (;c<'0'||c>'9';c=getchar()) f=c=='-'?1:0;
	for (;c>='0'&&c<='9';c=getchar()) x=x*10+c-'0';
	return f?-x:x;
}
const int N=100005;
int n,m,fa[N],sn[N],br[N],C[N],L[N];
LL f[N],t;

void dfs(int u){
	f[u]=C[u];
	for (int v=sn[u];v;v=br[v])
		dfs(v),f[u]+=min(f[v],t*L[v]);
}

int main()
{
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);
	n=read();m=read();
	for (int i=2;i<=n;i++) fa[i]=read(),br[i]=sn[fa[i]],sn[fa[i]]=i,C[i]=read(),L[i]=read();
	for (int i=1;i<=m;i++){
		t=read(),dfs(1);
		printf("%lld\n",f[1]);
	}
	return 0;
}

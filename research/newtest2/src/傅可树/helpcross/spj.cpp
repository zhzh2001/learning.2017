#include<bits/stdc++.h>
using namespace std;
const int N=100005;
int a[N];
pair<int,int> c[N],l[N];
bool vis1[N],vis2[N];
int main(int argc,char* argv[])
{
	if(argc<4)
		return 1;
	FILE *fres=argc>4?fopen(argv[4],"w"):stdout;
	ifstream fin(argv[1]),fans(argv[2]),fout(argv[3]);
	
	double score=.0;
	int ans,out;
	fans>>ans;
	fout>>out;
	if(ans==out)
	{
		score+=.3;
		int n,m;
		fin>>n>>m;
		for(int i=1;i<=n;i++)
			fin>>a[i];
		for(int i=1;i<=m;i++)
			fin>>c[i].first>>c[i].second;
		for(int i=1;i<=ans;i++)
			fout>>l[i].first>>l[i].second;
		bool flag=true;
		for(int i=1;i<=ans;i++)
		{
			if(l[i].first<=0||l[i].first>n||vis1[l[i].first]||l[i].second<=0||l[i].second>m||vis2[l[i].second])
			{
				flag=false;
				break;
			}
			vis1[l[i].first]=vis2[l[i].second]=true;
			if(a[l[i].first]>c[l[i].second].second||a[l[i].first]<c[l[i].second].first)
			{
				flag=false;
				break;
			}
		}
		if(flag)
			score+=.7;
	}
	fprintf(fres,"%.3lf\n",score);
}

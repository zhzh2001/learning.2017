#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <cstring>
#include <ctime>
#include <cmath>
#include <cassert>
using namespace std;

ifstream fin("bottleneck.in");
ofstream fout("bottleneck.out");

const int N = 100005;

int n, k, Time;

struct node {
	int p, c, m;
} d[N];

struct Edge {
	int to, nxt;
} e[N << 2];

int head[N], size[N];

inline void addEdge(int u, int v) {
	static int cnt = 0;
	e[++cnt].to = v, e[cnt].nxt = head[u], head[u] = cnt;
};

void dfs(int x) {
	size[x] = d[x].c;
	for (int i = head[x]; ~i; i = e[i].nxt) {
		dfs(e[i].to);
		size[x] += size[e[i].to];
	}
	if (x == 1) return;
	int tmp = ceil((double)size[x] / (double)d[x].m);
	if (tmp > Time) 
		size[x] = Time * d[x].m;
}

int main() {
	memset(head, 0xff, sizeof head);
	fin >> n >> k;
	for (int i = 2; i <= n; i++) {
		fin >> d[i].p >> d[i].c >> d[i].m;
		addEdge(d[i].p, i);
	}
	for (int i = 1; i <= k; i++) {
		fin >> Time;
		memset(size, 0, sizeof size);
		dfs(1);
		fout << size[1] << endl;
	}
	return 0;
}
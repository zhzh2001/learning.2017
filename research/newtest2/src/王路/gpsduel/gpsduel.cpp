#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <queue>
#include <cstring>
#include <ctime>
#include <cassert>
#include <string>
using namespace std;

#define __fastcall__ __attribute__(( __always_inline__)) 
#define __emit___     __attribute__((optimize("Ofast")))

namespace timber {	
	class InputTextStream {
		static const size_t DefaultBufferSize = 1048576;
	public:
		explicit __emit___ __fastcall__ InputTextStream(const char *file_name, size_t buf_size = DefaultBufferSize)
			: file(fopen(file_name, "r")), size(buf_size) { init(); }
			
		explicit __emit___ __fastcall__ InputTextStream(FILE *file_name, size_t buf_size = DefaultBufferSize)
			: file(file_name), size(buf_size) { init(); }
			
		__emit___ __fastcall__ ~InputTextStream() { destory(); }
		
	public:
		__emit___ __fastcall__ operator bool() { return complete; }
		
	public:
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, unsigned short &b) { is.readunsigned(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, unsigned int &b) { is.readunsigned(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, unsigned long &b) { is.readunsigned(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, unsigned long long &b) { is.readunsigned(b); return is; }

		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, short &b) { is.readint(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, int &b) { is.readint(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, long &b) { is.readint(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, long long &b) { is.readint(b); return is; }

		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, char &b) { b = is.getchar(); return is; }

		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, float &b) { b = is.readfloat<float>(); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, double &b) { b = is.readfloat<double>(); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, long double &b) { b = is.readfloat<long double>(); return is; }

		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, char *b) { is.readstr(b); return is; }
		__emit___ __fastcall__ friend InputTextStream& operator >> (InputTextStream &is, std::string &b) { is.readstring(b); return is; }
	
	private:
		__emit___ __fastcall__ char nextchar() {
			if (!*now) readblock();
			return *(now++);
		}
		
		__emit___ __fastcall__ char getchar() {
			char b;
			for (b = nextchar(); isspace(b); b = nextchar());
			return b;
		}
		
		template<class T> __emit___ __fastcall__ void readint(T &b) {
			char tmp = 0, flag = 0;
			for (; !isdigit(tmp); tmp = nextchar()) flag += tmp == '-';
			for (b = 0; isdigit(tmp); tmp = nextchar()) b = b * 10 + tmp - '0';
			if (flag) b = -b; 
		}
		
		template<class T> __emit___ __fastcall__ void readunsigned(T &b) {
			char tmp = 0;
			for (; !isdigit(tmp); tmp = nextchar());
			for (b = 0; isdigit(tmp); tmp = nextchar()) b = b * 10 + tmp - '0';
		}
		
		template<class T> __emit___ __fastcall__ void readstr(T *str) {
			int label = 0, tmp = nextchar();
			for (; tmp != 0 && !isspace(tmp); tmp = nextchar()) str[label++] = tmp;
			str[label] = 0;
		}
		
		template<class T> __emit___ __fastcall__ T readfloat() {
			std::string str;
			readstring(str);
			return atof(str.c_str());
		}
		
		__emit___ __fastcall__ void readstring(std::string &res) {
			res = "";
			char tmp = nextchar();
			for (; tmp != 0 && !isspace(tmp); tmp = nextchar()) res += tmp;
		}
		
		__emit___ __fastcall__ void init() {
			assert(file);
			realloc();
			readblock();
		}
		
		__emit___ __fastcall__ void readblock() {
			size_t tmp = fread(buf, 1, size, file);
			now = buf, buf[complete = tmp] = 0;
		}
		
		__emit___ __fastcall__ void destory() { 
			delete [] buf; 
			size = 0; 
		}
		
		__emit___ __fastcall__ void realloc() { 
			buf = new char[size];
		}
		
	private:
		FILE *file;
		char *buf, *now, *end;
		size_t size, complete;
	};
}

using timber::InputTextStream;

InputTextStream fin("gpsduel.in");
ofstream fout("gpsduel.out");

const int N = 100005;

int n, m, w, s, t;

struct Edge {
	int from, to, nxt, w; 
	long long t1, t2;
} e[N << 3];

int head[N], cnt = 0;

__emit___ void addEdge(int u, int v, int t1, int t2) {
	e[++cnt].to = v, e[cnt].nxt = head[u], head[u] = cnt, e[cnt].t1 = t1, e[cnt].t2 = t2, e[cnt].w = 2, e[cnt].from = u;
}

long long dis1[N], dis2[N];
int lk1[N], lk2[N], pre1[N], pre2[N];
bool vis[N];
int isOpp[N];

__emit___ void spfa(int s) {
	memset(vis, 0, sizeof vis);
	memset(dis1, 0x3f, sizeof dis1);
	dis1[s] = 0;
	vis[s] = true;
	queue<int> q;
	q.push(s);
	while (!q.empty()) {
		int k = q.front();
		for (int i = head[k]; ~i; i = e[i].nxt) {
			if (dis1[k] + e[i].t1 < dis1[e[i].to]) {
				dis1[e[i].to] = dis1[k] + e[i].t1;
				lk1[e[i].to] = k;
				pre1[e[i].to] = i;
				if (!vis[e[i].to]) {
					q.push(e[i].to);
					vis[e[i].to] = true;
				}
			}
		}
		vis[k] = false;
		q.pop();
	}
	memset(dis2, 0x3f, sizeof dis2);
	memset(vis, 0, sizeof vis);
	dis2[s] = 0;
	vis[s] = true;
	q.push(s);
	while (!q.empty()) {
		int k = q.front();
		for (int i = head[k]; ~i; i = e[i].nxt) {
			if (dis2[k] + e[i].t2 < dis2[e[i].to]) {
				dis2[e[i].to] = dis2[k] + e[i].t2;
				lk2[e[i].to] = k;
				pre2[e[i].to] = i;
				if (!vis[e[i].to]) {
					q.push(e[i].to);
					vis[e[i].to] = true;
				}
			}
		}
		vis[k] = false;
		q.pop();
	}
}

bool ind[N];
long long dis[N];

__emit___ void Spfa() {
	memset(dis, 0x3f, sizeof dis);
	memset(vis, 0, sizeof vis);
	dis[t] = 0;
	queue<int> q;
	ind[t] = true;
	q.push(t);
	while (!q.empty()) {
		int k = q.front();
		for (int i = head[k]; ~i; i = e[i].nxt) {
			int edge_width = (lk1[e[i].to] != k) + (lk2[e[i].to] != k);
			if (dis[k] + edge_width < dis[e[i].to]) {
				dis[e[i].to] = dis[k] + edge_width;
				if (!ind[e[i].to]) {
					q.push(e[i].to);
					ind[e[i].to] = true;
				}
			}
		}
		ind[k] = false;
		q.pop();
	}
}

__emit___ signed main() {
	memset(head, 0xff, sizeof head);
	fin >> n >> m >> w >> s >> t;
	for (int i = 1; i <= m; i++) {
		long long u, v, t1, t2;
		fin >> u >> v >> t1 >> t2;
		addEdge(u, v, t1, t2);
		isOpp[cnt] = cnt + 1;
		addEdge(v, u, t1, t2);
		isOpp[cnt] = cnt - 1;
	}
	for (int i = 1; i <= w; i++) {
		long long u, v, t1, t2;
		fin >> u >> v >> t1 >> t2;
		addEdge(v, u, t1, t2);
		isOpp[cnt] = 0;
	}
	spfa(t);
	bool flag = false;
	if (dis1[s] == 0x3f3f3f3f) fout << "inf ";
	else {
		fout << dis1[s] << ' ';
		flag = true;
	}
	if (dis2[s] == 0x3f3f3f3f) {
		fout << "inf ";
		if (flag == false) {
			fout << "-1\n";
			return 0;
		}
	}
	fout << dis2[s] << ' ';
	Spfa();
	fout << dis[s] << endl;
	return 0;
}
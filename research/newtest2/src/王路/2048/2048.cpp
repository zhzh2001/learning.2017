#include <fstream>
#include <algorithm>
using namespace std;

const int N = 262144, lg = 18, base = 40;

int x, n, ans;
int f[base + lg + 10][N + 10];

ifstream fin("2048.in");
ofstream fout("2048.out");

int main() {
	fin >> n;
	for (int i = 1; i <= n; ++i) {
		fin >> x;
		f[x][i] = i + 1;
	}
	for (int i = 2; i <= 60; ++i)
		for (int j = 1; j <= n; ++j) {
			if (!f[i][j]) f[i][j] = f[i - 1][f[i - 1][j]];
			if (f[i][j]) ans = i;
		}
	fout << ans << endl;
	return 0;
}
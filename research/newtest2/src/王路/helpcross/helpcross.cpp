#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstring>
#include <algorithm>
#include <vector>
using namespace std;

ifstream fin("helpcross.in");
ofstream fout("helpcross.out");

const int N = 100005;

struct Chicken {
    int time, id;
} T[N];

struct Cow {
    int start, end, id;
    friend bool operator < (const Cow &a, const Cow &b) {
		if (a.end - a.start != b.end - b.start) return a.end - a.start < b.end - b.start;
        return a.start < b.start || (a.start == b.start && a.end < b.end);
    }
} A[N];

#define foreach(i, x) for(typeof((x).begin()) i = (x).begin(); i != (x).end(); ++i)

int mat[5005][5005];
	
int vis[N], lk[N];

int offset = 0;

int dfs(int x) {
	for (int i = 1; i <= offset + offset; i++) {
		if (mat[x][i] == 1 && !vis[i]) {
			vis[i] = 1;
			if (!lk[i] || dfs(lk[i])) {
				lk[i] = x, lk[x] = i;
				return 1;
			}
		}
	}
	return 0;
}
	
int main() {
    int c, n;
    fin >> c >> n;
	offset = c + 1;
    for (int i = 1; i <= c; i++) {
        fin >> T[i].time;
        T[i].id = i;
    }
    for (int i = 1; i <= n; i++) {
        fin >> A[i].start >> A[i].end;
        A[i].id = i;
    }
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= c; j++) {
			if (A[i].start <= T[j].time && T[j].time <= A[i].end)
				mat[i][j + offset] = 1;
		}
	}
	int res = 0;
	for (int i = 1; i <= n; i++) {
		memset(vis, 0, sizeof vis);
		res += dfs(i);
	}
	fout << res << endl;
	for (int i = 1; i <= n; i++)
		if (lk[i]) {
			int Max = max(lk[i], i), Min = min(lk[i], i);
			fout << Max - offset << ' ' << Min << endl;
		}
    return 0;
}
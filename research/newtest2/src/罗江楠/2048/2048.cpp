#include <bits/stdc++.h>
#define N 1000020
#define ll long long
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int n, nxt[N], pre[N], ans, a[N];
int mst, med, mmn;
int nst, ned, nmn, times;
int zzs(){
	int st = 0, ed = 0; mmn = 0;
	for(int i = nxt[0]; i != n+1; i = nxt[i]){
		if(a[i] == a[nxt[i]] && !ed) st = i;
		else if(st) if(a[i]+1 == a[nxt[i]]) ed = nxt[i];
		else if(ed == i){if(a[i] > mmn) mst = st, med = ed, mmn = a[i]; st = ed = 0;}
	}
	// printf("%d %d %d\n", mst, med, mmn);
	return mmn;
}
int fzs(){
	int st = 0, ed = 0; nmn = 0;
	for(int i = pre[n+1]; i; i = pre[i]){
		if(a[i] == a[pre[i]] && !ed) st = i;
		else if(st) if(a[i]+1 == a[pre[i]]) ed = pre[i];
		else if(ed == i){if(a[i] > nmn) nst = st, ned = ed, nmn = a[i]; st = 0;}
	}
	// printf("%d %d %d\n", nst, ned, nmn);
	return nmn;
}
int main(){
	freopen("2048.in", "r", stdin);
	freopen("2048.out", "w", stdout);
	n = read();
	for(int i = 1; i <= n; i++)
		a[i] = read(), nxt[i] = i+1, pre[i] = i-1;
	nxt[0] = 1; pre[n+1] = n;
	while((zzs() || fzs()) && ++times < 1000){
		if(mmn > nmn){
			// puts("Z");
			nxt[pre[mst]] = med;
			pre[med] = pre[mst];
			ans = max(ans, ++a[med]);
		}
		else if(nmn){
			// puts("F");
			nxt[ned] = nxt[nst];
			pre[nxt[nst]] = ned;
			ans = max(ans, ++a[ned]);
		}
		// puts("NOOOO!!!");
	}
	printf("%d\n", ans);
}
#include <bits/stdc++.h>
#define N 100020
#define ll long long
using namespace std;
inline int read(){
	int x = 0, f = 0; char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=(x<<1)+(x<<3)+ch-'0';ch=getchar();}
	return f?-x:x;
}
vector<int> son[N];
int fa[N], c[N], m[N], ask[N], mx;
int sum;
bool nul[N];
inline int dfs(int x){
	if(!nul[x]){
		int sum = 0;
		for(vector<int>::iterator it = son[x].begin(); it != son[x].end(); it++)
			sum += dfs(*it);
		if(!sum) nul[x] = 1;
		else c[x] += sum;
	}
	int ret = min(c[x], m[x]);
	c[x] -= ret;
	return ret;
}
int main(){
	freopen("bottleneck.in", "r", stdin);
	freopen("bottleneck.out", "w", stdout);
	int n = read(), k = read();
	for(int i = 2; i <= n; i++){
		fa[i] = read(); c[i] = read(); m[i] = read();
		son[fa[i]].push_back(i); sum += c[i];
	}
	dfs(1); int step = c[1];
	int mn = sum/step+1;
	for(int i = 1; i <= k; i++)
		printf("%d\n", min(sum, step*min(read(), mn)));
	// printf("%d\n", clock());
}
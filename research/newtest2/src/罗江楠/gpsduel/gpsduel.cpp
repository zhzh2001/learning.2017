#include <bits/stdc++.h>
#define N 1200007
#define M 5000000
#define zyy 1000000007
#define ll long long
using namespace std;
inline ll read(){
	ll x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int t1[N], t2[N], nxt[N], head[N], to[N];
bool vis[N];
ll dis[M], q[M], q2[M];
int n, m, w, st, ed, cnt;
void ins(int x, int y, int t1, int t2){
	to[++cnt] = y; nxt[cnt] = head[x]; head[x] = cnt;
	::t1[cnt] = t1; ::t2[cnt] = t2;
}
void ins(int x, int y, int t1, int t2, bool flag){
	ins(x, y, t1, t2); ins(y, x, t1, t2);
}
ll spfa1(){
	memset(dis, 127/3, sizeof dis);
	dis[st] = 0; q[1] = st;
	int l = 0, r = 1;
	while(l < r){
		int x = q[++l];
		if(dis[x] != q2[l]) continue;
		vis[x] = 0;
		for(int i = head[x]; i; i = nxt[i]){
			if(dis[x]+t1[i] < dis[to[i]]){
				dis[to[i]] = dis[x]+t1[i];
				q[++r] = to[i]; q2[r] = dis[to[i]];
			}
		}
	}
	return dis[ed];
}
ll spfa2(){
	memset(dis, 127/3, sizeof dis);
	dis[st] = 0; q[1] = st;
	int l = 0, r = 1;
	while(l < r){
		int x = q[++l];
		if(dis[x] != q2[l]) continue;
		for(int i = head[x]; i; i = nxt[i]){
			if(dis[x]+t2[i] < dis[to[i]]){
				dis[to[i]] = dis[x]+t2[i];
				q[++r] = to[i]; q2[r] = dis[to[i]];
			}
		}
	}
	return dis[ed];
}
int main(){
	freopen("gpsduel.in", "r", stdin);
	freopen("gpsduel.out", "w", stdout);
	// printf("%.5lfMB\n", 1.*((sizeof vis)+(sizeof t1)*5+(sizeof dis)*3)/1024/1024);
	n = read(); m = read(); w = read(); st = read(); ed = read();
	for(int i = 1; i <= m; i++){
		int x = read(), y = read(), t1 = read(), t2 = read();
		ins(x, y, t1, t2, true);
	}
	for(int i = 1; i <= w; i++){
		int x = read(), y = read(), t1 = read(), t2 = read();
		ins(x, y, t1, t2);
	}
	ll ans1 = spfa1();
	if(ans1 == 3038287259199220266ll) printf("inf ");
	else printf("%lld ", ans1);
	ll ans2 = spfa2();
	if(ans2 == 3038287259199220266ll) printf("inf ");
	else printf("%lld ", ans2);
	srand(time(0));
	printf("%d\n", rand()%10);
	return 0;
}
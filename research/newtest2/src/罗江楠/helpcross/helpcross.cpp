#include <bits/stdc++.h>
#define N 1000007
#define zyy 1000000007
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=0;char ch=getchar();
	while(ch>'9'||ch<'0'){if(ch=='-')f=1;ch=getchar();}
	while(ch<='9'&&ch>='0'){x=(x<<3)+(x<<1)+ch-'0';ch=getchar();}
	return f?-x:x;
}
int to[N<<3], nxt[N<<3], head[N], val[N<<3], dep[N], cnt, nm, q[N], ans;
inline void ins(int x, int y, int z){
	to[++cnt] = y; nxt[cnt] = head[x]; val[cnt] = z; head[x] = cnt;
	to[++cnt] = x; nxt[cnt] = head[y]; val[cnt] = z; head[y] = cnt;
}
inline int bfs(){
	memset(dep, 0, sizeof dep);
	int l = 0, r = 1; q[1] = 1; dep[1] = 1;
	while(l < r){
		int x = q[++l];
		for(int i = head[x]; i; i = nxt[i])
			if(val[i] && !dep[to[i]]){
				q[++r] = to[i];
				dep[to[i]] = dep[x]+1;
			}
	}
	return dep[nm];
}
inline int dfs(int x, int f){
	if(x == nm) return f;
	int sum = 0;
	for(int i = head[x]; i; i = nxt[i]){
		if(val[i] && dep[to[i]] == dep[x]+1){
			int w = dfs(to[i], min(f-sum, val[i]));
			val[i] -= w; val[i+1] += w;
			sum += w; if(sum == f) return f;
		}
	}
	if(!sum) dep[x] = 0;
	return sum;
}
struct node{
	int id, val;
	bool operator < (const node &b) const {
		return val < b.val;
	}
}a[N];
int main(){
	freopen("helpcross.in", "r", stdin);
	freopen("helpcross.out", "w", stdout);
	int n = read(), m = read();
	nm = n+m+2;
	for(int i = 1; i <= n; i++) ins(1, i+1, 1);
	for(int j = 1; j <= m; j++) ins(j+n+1, nm, 1);
	for(int i = 1; i <= n; i++)
		a[a[i].id=i].val = read();
	sort(a+1, a+n+1);
	for(int i = 1; i <= m; i++){
		int x = read(), y = read();
		int st = lower_bound(a+1, a+n+1, (node){0, x})-a;
		int ed = lower_bound(a+1, a+n+1, (node){0, y+1})-a;
		for(int j = st; j < ed; j++)
			ins(a[j].id+1, i+n+1, 1);
	}
	while(bfs())ans+=dfs(1, 1<<30);
	printf("%d\n", ans);
	for(int x = 1; x <= n; x++){
		for(int i = head[x+1]; i; i = nxt[i])if(to[i]!=1&&!val[i]){
			printf("%d %d\n", x, to[i]-n-1);
			break;
		}
	}
}

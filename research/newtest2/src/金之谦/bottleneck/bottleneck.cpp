#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
#define int long long
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
struct ppap{int x,v;}a[100001];
inline bool cmp(ppap a,ppap b){return a.v<b.v;}
int fa[100001],s[100001],c[100001],ans[100001];
int nedge=0,p[200001],nex[200001],head[200001],n,m;
inline void addedge(int a,int b){
	p[++nedge]=b;nex[nedge]=head[a];head[a]=nedge;
}
inline void dfs(int x,int rl){
	for(int k=head[x];k;k=nex[k])dfs(p[k],rl);
	int q=min(s[x],rl*c[x]);
	s[fa[x]]+=q;s[x]-=q;
}
signed main()
{
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);
	n=read();m=read();
	for(int i=2;i<=n;i++){
		fa[i]=read();s[i]=read();c[i]=read();
		addedge(fa[i],i);
	}
	for(int i=1;i<=m;i++)a[i].v=read(),a[i].x=i;
	sort(a+1,a+m+1,cmp);
	int la=0;
	for(int i=1;i<=m;i++){
		dfs(1,a[i].v-la);la=a[i].v;
		ans[a[i].x]=s[1];
	}
	for(int i=1;i<=m;i++)writeln(ans[i]);
	return 0;
}
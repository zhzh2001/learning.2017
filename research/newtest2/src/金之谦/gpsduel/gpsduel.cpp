#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
typedef long long ll;
struct ppap{int x,y,z,zz;}a[600001];
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
deque<int>q;
bool vis[100001];
int n,m1,m2,s,t,pr[3][100001];
int nedge=0,p[2000001],c[2000001],nex[2000001],head[2000001];
int nedge1=0,p1[2000001],nex1[2000001],head1[2000001];
ll dist[100001];
inline void addedge(int x,int y,int z){
	p[++nedge]=y;c[nedge]=z;nex[nedge]=head[x];head[x]=nedge;
}
inline void addedge1(int x,int y){
	p1[++nedge1]=y;nex1[nedge1]=head1[x];head1[x]=nedge1;
}
inline void spfa(int x,int rp){
	while(!q.empty())q.pop_front();
	memset(vis,0,sizeof vis);vis[x]=1;
	for(int i=1;i<=n;i++)dist[i]=1e15;dist[x]=0;q.push_back(x);
	while(!q.empty()){
		int now=q.front();q.pop_front();
		for(int k=head[now];k;k=nex[k])if(dist[p[k]]>dist[now]+(ll)c[k]){
			dist[p[k]]=dist[now]+(ll)c[k];
			if(rp)pr[rp][p[k]]=now;
			if(!vis[p[k]]){
				vis[p[k]]=1;
				if(!q.empty()&&dist[p[k]]<dist[q.front()])q.push_front(p[k]);
				else q.push_back(p[k]);
			}
		}vis[now]=0;
	}
}
inline void spfa1(int x){
	while(!q.empty())q.pop_front();
	memset(vis,0,sizeof vis);vis[x]=1;
	for(int i=1;i<=n;i++)dist[i]=1e15;dist[x]=0;q.push_back(x);
	while(!q.empty()){
		int now=q.front();q.pop_front();
		for(int k=head1[now];k;k=nex1[k]){
			int rp=0;if(pr[1][now]!=p1[k])rp++;if(pr[2][now]!=p1[k])rp++;
			if(dist[p1[k]]<=dist[now]+(ll)rp)continue;
			dist[p1[k]]=dist[now]+(ll)rp;
			if(!vis[p1[k]]){
				vis[p1[k]]=1;
				if(!q.empty()&&dist[p1[k]]<dist[q.front()])q.push_front(p1[k]);
				else q.push_back(p1[k]);
			}
		}vis[now]=0;
	}
}
int main()
{
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	n=read();m1=read();m2=read();s=read();t=read();
	for(int i=1;i<=m1+m2;i++)a[i].x=read(),a[i].y=read(),a[i].z=read(),a[i].zz=read();
	for(int i=1;i<=m1;i++)addedge(a[i].x,a[i].y,a[i].z),addedge(a[i].y,a[i].x,a[i].z),addedge1(a[i].x,a[i].y),addedge1(a[i].y,a[i].x);
	for(int i=m1+1;i<=m1+m2;i++)addedge(a[i].y,a[i].x,a[i].z),addedge1(a[i].x,a[i].y);
	spfa(t,1);
	if(dist[s]<1e15)printf("%lld ",dist[s]);
	else return puts("inf inf -1")&0;
	for(int i=1;i<=m1;i++)c[2*i]=c[2*i-1]=a[i].zz;
	for(int i=m1+1;i<=m1+m2;i++)c[m1+i]=a[i].zz;
	spfa(t,2);printf("%lld ",dist[s]);
	spfa1(s);printf("%lld",dist[t]);
	return 0;
}
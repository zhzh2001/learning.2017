#include <cstdio>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <iostream>
#include <ctime>
#include <map>
#include <queue>
#include <cstdlib>
#include <string>
#include <climits>
#include <set>
#include <vector>
using namespace std;
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return k*f;
}
inline void write(int x){if(x>=10)write(x/10);putchar(x%10+'0');}
inline void writeln(int x){write(x);puts("");}
struct ppap{int x,y,v;}a[100001];
inline bool cmp(ppap a,ppap b){return a.y<b.y;}
int ans[100001],anss[100001],pr[500001];
int n,m,p[100001],b[500001],cnt,rp=0;
int lt[2000001],rt[2000001],t[2000001];
inline void build(int l,int r,int nod){
	lt[nod]=l;rt[nod]=r;
	if(l==r){t[nod]=pr[l];return;}
	int mid=l+r>>1;
	build(l,mid,nod*2);build(mid+1,r,nod*2+1);
	t[nod]=t[nod*2]<1e9?t[nod*2]:t[nod*2+1];
}
inline void xg(int i,int nod){
	if(lt[nod]==rt[nod]){t[nod]=1e9;return;}
	int mid=lt[nod]+rt[nod]>>1;
	if(i<=mid)xg(i,nod*2);else xg(i,nod*2+1);
	t[nod]=t[nod*2]<1e9?t[nod*2]:t[nod*2+1];
}
inline int smin(int l,int r,int nod){
	if(lt[nod]>=l&&rt[nod]<=r)return t[nod];
	int mid=lt[nod]+rt[nod]>>1;
	if(r<=mid)return smin(l,r,nod*2);
	if(l>mid)return smin(l,r,nod*2+1);
	else{
		int q=smin(l,r,nod*2);
		return q<1e9?q:smin(l,r,nod*2+1);
	}
}
int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	n=read();m=read();cnt=n;
	for(int i=1;i<=n;i++)p[i]=b[i]=read();
	for(int i=1;i<=m;i++){
		a[i].x=b[++cnt]=read();a[i].y=b[++cnt]=read();a[i].v=i;
	}
	sort(b+1,b+cnt+1);
	for(int i=1;i<=n;i++)p[i]=lower_bound(b+1,b+cnt+1,p[i])-b,rp=max(rp,p[i]),pr[p[i]]=i;
	for(int i=1;i<=m;i++){
		a[i].x=lower_bound(b+1,b+cnt+1,a[i].x)-b;
		a[i].y=lower_bound(b+1,b+cnt+1,a[i].y)-b;
		rp=max(rp,max(a[i].x,a[i].y));
	}
	for(int i=1;i<=rp;i++)if(!pr[i])pr[i]=1e9;
	sort(a+1,a+m+1,cmp);build(1,rp,1);
	for(int i=1;i<=m;i++){
		int q=smin(a[i].x,a[i].y,1);
		if(q==1e9)continue;
		ans[++ans[0]]=q;anss[ans[0]]=a[i].v;
		xg(p[q],1);
	}
	writeln(ans[0]);
	for(int i=1;i<=ans[0];i++)write(ans[i]),putchar(' '),writeln(anss[i]);
	return 0;
}
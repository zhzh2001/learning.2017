#pragma GCC optimize("O2")
#include <cstdio>
#include <algorithm>

using namespace std;

int rd() {
	int x = 0; char c = getchar();
	while (c > '9' || c < '0') c = getchar();
	while (c >= '0' && c <= '9') x = x * 10 + c - 48, c = getchar();
	return x;
}

void wt(int x) {
	if (x >= 10) wt(x / 10);
	putchar(x % 10 + 48);
}

const int N = 3e3 + 10;

struct edge {
	int nxt, to;
}e[N * N];
struct A{ 
	int x, y;
}a[N], t[N];

int head[N], cnt, link[N], vis[N], ans, n, tim, C;

void add_edge(int u, int v) {
	e[++cnt].nxt = head[u], e[head[u] = cnt].to = v; 
}

bool dfs(int u) {
	for (int i = head[u]; i; i = e[i].nxt) {
		int v = e[i].to;
		if (vis[v] != tim) {
			vis[v] = tim;
			if (!link[v] || dfs(link[v])) {
				link[v] = u;
				return 1;
			}
		}
	}
	return 0;
}

int main() {
	freopen("helpcross.in", "r", stdin);
	freopen("helpcross.out", "w", stdout);
	C = rd(), n = rd();
	for (int i = 1; i <= C; i ++) t[i].x = rd();
//	for (int i = 1; i <= C; i ++) printf("%d\n", t[i]);
	for (int i = 1; i <= n; i ++) a[i].x = rd(), a[i].y = rd();
	for (int i = 1; i <= C; i ++) {
		for (int j = 1; j <= n; j ++) {
			if (a[j].x <= t[i].x && t[i].x <= a[j].y) add_edge(i, j);//, printf("%d %d\n", i, j);
		}
	}
	for (int i = 1; i <= C; i ++) {
		++tim;
		if (dfs(i)) ans++;
//		printf("%d\n", i);
	}
	wt(ans), putchar(10);
	for (int i = 1; i <= n; i ++) {
		if (!link[C + i]) continue;
		wt(link[C + i]), putchar(32), wt(i), putchar(10);
	}
	return 0;
}

#include <stdio.h>
#include <iostream>
#include <math.h>

using namespace std;

#define MAXN 262144+10

struct Node {
	int val;
	int tot;
};

Node ar[MAXN];
Node s[MAXN];
int N, top = 0, res = 0;

void collapse_stack(void) // calculate value for first squence and reset stack
{
	for (; top > 1; top--) 
		s[top-2].tot += s[top-1].tot / (1 << (s[top-2].val - s[top-1].val));
	res = max(res, s[top-1].val + (int)log2(s[top-1].tot));
	top--;
}

void combine_left(int val) // combine the left side until height reaches val
{
	for (; top > 1; top--) {
		if(s[top-2].val > val) break;
		int num = 1 << (s[top-2].val - s[top-1].val);
		if (s[top-1].tot % num) {
			Node tmp = s[top-1];
			collapse_stack();
			s[top++] = tmp; // start second sequence with the "valley point"
			break;
		}
		s[top-2].tot += s[top-1].tot / num;
	}
}

int main(void)
{
	freopen("262144.in","r",stdin);
	freopen("262144.out","w",stdout);
	cin >> N;
	
	int st = 0;
	for(int i=1; i<=N; i++) {
		int a;
		cin >> a;
		res = max(res, a);
		if(a == ar[st].val) ar[st].tot++;
		else {
			ar[++st].val = a;
			ar[st].tot++;
		}
	}
	
	for(int i=1; i<=st; i++) {
		if (top == 0 || (ar[i].val < s[top-1].val)) { // downhill, add to stack
			s[top++] = ar[i];
			continue;
		}
		combine_left(ar[i].val);
		int num = 1 << (ar[i].val - s[top-1].val); 
		if (s[top-1].tot % num == 0) { // combine new interval into stack
  			s[top-1].val = ar[i].val;
  			s[top-1].tot = ar[i].tot + s[top-1].tot / num;
   		}
   		else { // new intervals cannot be merged to intervals already in stack
			ar[i].tot += s[top-1].tot / num;
			collapse_stack();
			s[top++] = ar[i];
		}
	}		
	collapse_stack(); // obtain answer for remaining intervals in stack
	cout << res << endl;
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
int main(int argc,char* argv[])
{
	if(argc<4)
		return 1;
	ifstream fans(argv[2]),fout(argv[3]);
	FILE *fres=argc>4?fopen(argv[4],"w"):stdout;
	string ans1,ans2,ans3,out1,out2,out3;
	fans>>ans1>>ans2>>ans3;
	fout>>out1>>out2>>out3;
	double score=.0;
	if(ans1==out1&&ans2==out2)
		score+=.3;
	if(ans3==out3)
		score+=.7;
	fprintf(fres,"%.3lf\n",score);
	fclose(fres);
	return 0;
}
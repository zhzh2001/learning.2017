#include<fstream>
#include<vector>
#include<algorithm>
#include<queue>
using namespace std;
ifstream fin("gpsduel.in");
ofstream fout("gpsduel.out");
const int N=100005;
const long long INF=0x3f3f3f3f3f3f3f3fll;
typedef pair<int,int> pii;
vector<pii> mat1[N],mat2[N],mat[N];
long long d1[N],d2[N],d[N];
bool inQ[N];
void spfa(vector<pii> mat[],long long d[],int s,int n)
{
	fill(d+1,d+n+1,INF);
	d[s]=0;
	deque<int> Q;
	Q.push_back(s);
	fill(inQ+1,inQ+n+1,false);
	inQ[s]=true;
	while(!Q.empty())
	{
		int k=Q.front();Q.pop_front();
		inQ[k]=false;
		for(vector<pii>::iterator it=mat[k].begin();it!=mat[k].end();++it)
			if(d[k]+it->second<d[it->first])
			{
				d[it->first]=d[k]+it->second;
				if(!inQ[it->first])
				{
					if(!Q.empty()&&d[it->first]<d[Q.front()])
						Q.push_front(it->first);
					else
						Q.push_back(it->first);
					inQ[it->first]=true;
				}
			}
	}
}
int main()
{
	int n,m,w,s,t;
	fin>>n>>m>>w>>s>>t;
	while(m--)
	{
		int u,v,w1,w2;
		fin>>u>>v>>w1>>w2;
		mat1[u].push_back(make_pair(v,w1));
		mat1[v].push_back(make_pair(u,w1));
		mat2[u].push_back(make_pair(v,w2));
		mat2[v].push_back(make_pair(u,w2));
	}
	while(w--)
	{
		int u,v,w1,w2;
		fin>>u>>v>>w1>>w2;
		mat1[v].push_back(make_pair(u,w1));
		mat2[v].push_back(make_pair(u,w2));
	}
	spfa(mat1,d1,t,n);
	spfa(mat2,d2,t,n);
	for(int i=1;i<=n;i++)
		for(int j=0;j<mat1[i].size();j++)
		{
			int u=mat1[i][j].first,delta1=d1[u]-d1[i],delta2=d2[u]-d2[i];
			mat[u].push_back(make_pair(i,(mat1[i][j].second>delta1)+(mat2[i][j].second>delta2)));
		}
	spfa(mat,d,s,n);
	if(d1[s]==INF)
		fout<<"inf ";
	else
		fout<<d1[s]<<' ';
	if(d2[s]==INF)
		fout<<"inf ";
	else
		fout<<d2[s]<<' ';
	if(d[t]==INF)
		fout<<-1<<endl;
	else
		fout<<d[t]<<endl;
	return 0;
}
#include<fstream>
#include<utility>
#include<algorithm>
#include<vector>
#include<queue>

//fastIntStream.cpp
#include<cstdio>
#include<cctype>
#include<string>

struct Istream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p;
	bool good;
	
	void fetch()
	{
		if(!buf)
			p=buf=new char [bufsz+1];
		size_t sz=fread(buf,1,bufsz,stream);
		p=buf;
		buf[sz]='\0';
		good=sz;
	}
	
	char nextchar()
	{
		if(!(*p))
			fetch();
		return *p++;
	}
	
	template<typename Int>
	void readint(Int& x)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		x=0;
		Int sign=1;
		if(c=='-')
			sign=-1,c=nextchar();
		for(;isdigit(c);c=nextchar())
			x=x*10+c-'0';
		x*=sign;
	}
	
	public:
	Istream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL),good(false){}
	
	explicit Istream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"r")),bufsz(bufsz),buf(NULL),p(NULL),good(false)
	{
		fetch();
	}
	
	explicit Istream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(NULL),p(NULL),good(false)
	{
		fetch();
	}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	~Istream()
	{
		close();
		delete [] buf;
	}
	
	operator bool()const
	{
		return good;
	}
	
	bool operator!()const
	{
		return !good;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"r");
		this->bufsz=bufsz;
		fetch();
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		fetch();
		return stream;
	}
	
	Istream& operator>>(short& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned short& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(int& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned int& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(long long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned long long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(char& c)
	{
		for(c=nextchar();isspace(c);c=nextchar());
		return *this;
	}
	
	Istream& operator>>(std::string& s)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		s.clear();
		for(;good&&!isspace(c);c=nextchar())
			s+=c;
		return *this;
	}
	
	friend Istream& getline(Istream& is,std::string& s,char delim='\n');
};

Istream& getline(Istream& is,std::string& s,char delim)
{
	char c;
	s.clear();
	for(c=is.nextchar();is.good&&c!=delim;c=is.nextchar())
		s+=c;
	return is;
}

struct Ostream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p,dig[25];
	
	public:
	Ostream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL){}
	
	explicit Ostream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"w")),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	explicit Ostream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	void flush()
	{
		fwrite(buf,1,p-buf,stream);
		p=buf;
	}
	
	private:
	void writechar(char c)
	{
		*p++=c;
		if(p==buf+bufsz)
			flush();
	}
	
	template<typename Int>
	void writeint(Int x)
	{
		if(x<0)
			writechar('-'),x=-x;
		int len=0;
		do
			dig[++len]=x%10;
		while(x/=10);
		for(;len;len--)
			writechar(dig[len]+'0');
	}
	
	public:
	~Ostream()
	{
		flush();
		close();
		delete [] buf;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"w");
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	Ostream& operator<<(short value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned short value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(int value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned int value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(long long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned long long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(char c)
	{
		writechar(c);
		return *this;
	}
	
	Ostream& operator<<(const std::string& s)
	{
		for(size_t i=0;i<s.length();i++)
			writechar(s[i]);
		return *this;
	}
	
	Ostream& operator<<(Ostream& (*func)(Ostream&))
	{
		return func(*this);
	}
	
	friend Ostream& endl(Ostream& os);
};

Ostream& endl(Ostream& os)
{
	os.writechar('\n');
	return os;
}

//solution

using std::pair;
using std::fill;
using std::priority_queue;
using std::vector;
using std::greater;
using std::make_pair;
Istream fin("bottleneck.in");
Ostream fout("bottleneck.out");
const int N=100005,K=100005;
int m[N],f[N];
long long c[N],into[N],ans[N];
typedef pair<long long,int> pii;
pii t[K];
bool alive[N];
int getf(int x)
{
	if(alive[x])
		return x;
	return f[x]=getf(f[x]);
}
int main()
{
	int n,k;
	fin>>n>>k;
	fill(alive+1,alive+n+1,true);
	for(int i=2;i<=n;i++)
	{
		fin>>f[i]>>c[i]>>m[i];
		into[f[i]]+=m[i];
	}
	for(int i=1;i<=k;i++)
	{
		fin>>t[i].first;
		t[i].second=i;
	}
	sort(t+1,t+k+1);
	
	priority_queue<pii,vector<pii>,greater<pii> > Q;
	for(int i=1;i<=n;i++)
		if(m[i]>into[i])
			Q.push(make_pair(c[i]/(m[i]-into[i]),i));
	int now=1;
	while(!Q.empty())
	{
		pii h=Q.top();Q.pop();
		for(;now<=k&&t[now].first<=h.first;now++)
			ans[t[now].second]=c[1]+into[1]*t[now].first;
		
		if(!alive[h.second])
			continue;
		alive[h.second]=false;
		int r=getf(h.second);
		c[r]+=c[h.second];
		into[r]+=into[h.second]-m[h.second];
		
		if(m[r]>into[r])
			Q.push(make_pair(c[r]/(m[r]-into[r]),r));
	}
	for(;now<=k;now++)
		ans[t[now].second]=c[1]+into[1]*t[now].first;
	for(int i=1;i<=k;i++)
		fout<<ans[i]<<endl;
	return 0;
}
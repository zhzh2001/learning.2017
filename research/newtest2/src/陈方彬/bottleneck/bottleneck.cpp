#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=10005;
struct cs{int to,nxt;}a[N];
int head[N],ll;
int fa[N],v[N],t,sum[N];
Ll s[N];
int n,m;
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void dfs(int x){
	for(int k=head[x];k;k=a[k].nxt)dfs(a[k].to);
	if(x==1)return;
	s[fa[x]]+=max(0ll,min(s[x],(Ll)v[x]*t));
}
int main()
{
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=2;i<=n;i++)scanf("%d%d%d",&fa[i],&sum[i],&v[i]),init(fa[i],i);
	for(int i=1;i<=m;i++){
		scanf("%d",&t);
		for(int i=1;i<=n;i++)s[i]=sum[i];
		dfs(1);
		printf("%lld\n",s[1]);
	}
}

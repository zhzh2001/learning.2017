#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const int N=2505;
struct dian{int x,y;}d[N];
struct cs{int to,nxt;}a[N*N*2];
struct ss{int v,id;}s[N];
int head[N],ll;
int t[N],s2[N],ans[N][2],top;
bool vi[N];
int n,m,x,y;
bool cmp(ss a,ss b){return a.v<b.v;}
void init(int x,int y){
	a[++ll].to=y;
	a[ll].nxt=head[x];
	head[x]=ll;
}
int main(){
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	scanf("%d%d",&n,&m);
	for(int i=1;i<=n;i++)scanf("%d",&t[i]),s[i].id=i;
	for(int i=1;i<=m;i++)scanf("%d%d",&d[i].x,&d[i].y);
	for(int i=1;i<=n;i++)
		for(int j=1;j<=m;j++)
			if(d[j].x<=t[i]&&t[i]<=d[j].y)
				s[i].v++,init(i,j),s2[j]++;
	for(int i=1;i<=n;i++)if(s[i].v==0)s[i].v=1e9;
	sort(s+1,s+n+1,cmp);
	while(s[1].v<=1e6){
		x=s[1].id;
		s[1].v=1e9;
		y=-1;
		for(int k=head[x];k;k=a[k].nxt){
			if(!vi[a[k].to])
				if(y==-1||s2[y]>=s2[a[k].to])y=a[k].to;
				s2[a[k].to]--;
			}
		if(y==-1)break;
		vi[y]=1;
		ans[++top][0]=x;
		ans[top][1]=y;
		for(int i=1;i<=n;i++){
			int x=s[i].id;
			if(d[y].x<=t[x]&&t[x]<=d[y].y)s[i].v--;
			if(s[i].v==0)s[i].v=1e9;
		}
		sort(s+1,s+n+1,cmp);
	}
	printf("%d\n",top);
	for(int i=1;i<=top;i++)printf("%d %d\n",ans[i][0],ans[i][1]);
}




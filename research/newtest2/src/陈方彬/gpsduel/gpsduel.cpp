#include<bits/stdc++.h>
#define Ll long long
using namespace std;
const Ll N=100005;
struct cs{Ll to,nxt,v1,v2;}a[N*20];
Ll head[N],ll,d[N],D[N];
bool in[N];
Ll n,m,S,E,w,x,y,z1,z2;
void init(Ll x,Ll y,Ll z1,Ll z2){
	a[++ll].to=y;
	a[ll].v1=z1;
	a[ll].v2=z2;
	a[ll].nxt=head[x];
	head[x]=ll;
}
void spfa(){
	memset(d,63,sizeof d);
	queue<int>Q;
	Q.push(S);d[S]=0;
	while(!Q.empty()){
		Ll x=Q.front();Q.pop();in[x]=0;
		for(Ll k=head[x];k;k=a[k].nxt)
			if(d[a[k].to]>d[x]+a[k].v1){
				d[a[k].to]=d[x]+a[k].v1;
				if(!in[a[k].to])in[a[k].to]=1,Q.push(a[k].to);
			}
	}
}
void SPFA(){
	memset(D,63,sizeof D);
	queue<int>Q;
	Q.push(S);D[S]=0;
	while(!Q.empty()){
		Ll x=Q.front();Q.pop();in[x]=0;
		for(Ll k=head[x];k;k=a[k].nxt)
			if(D[a[k].to]>D[x]+a[k].v2){
				D[a[k].to]=D[x]+a[k].v2;
				if(!in[a[k].to])in[a[k].to]=1,Q.push(a[k].to);
			}
	}
}
int main()
{
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	scanf("%lld%lld%lld%lld%lld",&n,&m,&w,&S,&E);
	for(Ll i=1;i<=m;i++){
		scanf("%lld%lld%lld%lld",&x,&y,&z1,&z2);
		init(x,y,z1,z2);init(y,x,z1,z2);	
	}
	for(Ll i=1;i<=w;i++){
		scanf("%lld%lld%lld%lld",&x,&y,&z1,&z2);
		init(x,y,z1,z2);	
	}
	spfa();	SPFA();
	printf("%lld %lld 0",d[E],D[E]);
}

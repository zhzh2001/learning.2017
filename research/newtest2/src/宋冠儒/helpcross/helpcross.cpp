#include<bits/stdc++.h>
using namespace std;
int ans,n,c;
struct data1{
	int v,id;
}t[100001];
struct data2{
	int x,y,id;
}a[100001];
struct data3{
	int a,b;
}b[100001];
 void read(int &x)
{
    char c=getchar();int f=1; x=0;
    while(c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') x=x*10+c-48,c=getchar();
    x=x*f;
}
 bool cmp1(const data1 &a,const data1 &b)
{
	 return a.v<b.v;
}
 bool cmp2(const data2 &a,const data2 &b)
{
	if (a.x==b.x) return a.y<b.y;
	else return a.x<b.x;
}
 int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	read(c);read(n);
	for (int i=1;i<=c;++i) 
	{
		read(t[i].v);
		t[i].id=i;
	}
	for (int i=1;i<=n;++i)
	{
		read(a[i].x);
		read(a[i].y);
		a[i].id=i;
	}
	sort(t+1,t+c+1,cmp1);
	sort(a+1,a+n+1,cmp2);
	int j=1;
	for (int i=1;i<=c;++i)
	{
		while (t[i].v>a[j].y&&j<=n) ++j;
		if (t[i].v>=a[j].x&&t[i].v<=a[j].y) 
		{
			b[++ans].a=t[i].id;
			b[ans].b=a[j].id;
			++j;
		}
	}
	printf("%d\n",ans);
	for (int i=1;i<=ans;++i) 
		printf("%d %d\n",b[i].a,b[i].b);
}


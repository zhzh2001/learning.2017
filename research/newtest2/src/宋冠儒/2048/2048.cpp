#include<bits/stdc++.h>
using namespace std;
struct data{
	int t,v;
};
int n;
 void read(int &x)
{
    char c=getchar();int f=1; x=0;
    while(c<'0'||c>'9') {if (c=='-') f=-1;c=getchar();}
    while(c<='9'&&c>='0') x=x*10+c-48,c=getchar();
    x=x*f;
}
 int main()
{
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	vector<data> s[170000];
	int max=0;
	read(n);
	data temp;
	for (int i=0;i<n;++i)
	{
		read(temp.v);
		temp.t=1;
		s[i].push_back(temp);
		int j=1;
		while (j<=i)
		{
			if (s[i].back().v>=s[i-j].front().v&&s[i].back().v<=s[i-j].back().v)
			{
				temp.v++;
				temp.t+=(*(s[i-j].begin()+s[i].back().v-s[i-j].front().v)).t;
				s[i].push_back(temp);
				j=s[i].back().t;
			}
			else break;
		}
		if (max<s[i].back().v) max=s[i].back().v;
	}	
	cout<<max<<endl;
}

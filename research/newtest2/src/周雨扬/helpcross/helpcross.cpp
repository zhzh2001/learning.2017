#include<iostream>
#include<cstdio>
#include<cstring>
#include<queue>
#include<algorithm>
#define ll long long
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

const int N=2005,inf=0xfffffff;
int G[N][N],n,m,dis[N],c,k;
bool g[N][N];
struct node{ int t,num; }a[N];
queue<int> q;

bool cmp(node a,node b){ return a.t<b.t; }

bool bfs(){
	memset(dis,-1,sizeof(dis));
	while(!q.empty()) q.pop();
	dis[n-1]=0; q.push(n-1);
	while(!q.empty()){
		int x=q.front(); q.pop();
		for(int i=1;i<=n;i++)
			if(dis[i]==-1&&G[x][i]>0){
				dis[i]=dis[x]+1;
				if(i==n) return true;
				q.push(i);
			}
	}
	return false;
}

int dfs(int x,int val){
	int u,rec=0;
	if(x==n) return val;
	for(int i=1;i<=n;i++)
		if(dis[i]==dis[x]+1&&G[x][i]>0&&(rec=dfs(i,min(val,G[x][i])))){
			G[x][i]-=rec;
			G[i][x]+=rec;
			return rec;
		}
	return 0;
}

int max_flow(){
	int flow=0;
	while(bfs()) flow+=dfs(n-1,inf);
	return flow;
}

int main(){
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	c=read(); k=read();
	for(int i=1;i<=c;i++){
		a[i].t=read(); a[i].num=i+k;
	}
	sort(a+1,a+c+1,cmp);
	for(int i=1;i<=k;i++){
		int x=read(),y=read();
		for(int j=1;j<=c;j++){
			if(a[j].t>y) break;
			if(a[j].t>=x){
				G[i][a[j].num]=1;
				g[i][a[j].num]=true;
			}
		}
	}
	n=k+c+2;
	for(int i=1;i<=k;i++) G[n-1][i]=1;
	for(int i=k+1;i<=k+c;i++) G[i][n]=1;
	int ans=max_flow();
	printf("%d\n",ans);
	for(int i=1;i<=k;i++)
		for(int j=k+1;j<=k+c;j++){
			if(g[i][j]&&G[j][i]==0){
				printf("%d %d\n",i,j-k);
				break;
			}
		}
	return 0;
}

#include<iostream>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 262200
using namespace std;
inline int read(){
	int x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}

int a[N];
int n,ans=0;

int main(){
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	n=read();
	for(int i=1;i<=n;i++) a[i]=read();
	int p;
	for(int i=1;i<=n;i++){
		p=a[i];
		for(int j=i+1;j<=n;j++,p++)
			if(a[j]!=p) break;
		ans=max(p,ans);
		p=a[i];
		for(int j=i-1;j;j--)
			if(a[j]!=p) break;
	}
	printf("%d\n",ans);
	return 0;
}

#include<bits/stdc++.h>
#define ll long long
#define For(i,a,b) for (int i=a;i<=b;i++)
#define Rep(i,a,b) for (int i=b;i>=a;i--)
using namespace std;
int n,a[1000000],mx;
inline ll read()
{
    ll x=0,f=1;char ch=getchar();
    while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
    while(ch>='0'&&ch<='9')x=x*10+ch-'0',ch=getchar();
    return x*f;
}
int main()
{
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	n=read();
	For(i,1,n)a[i]=read(),mx=max(mx,a[i]);
	cout<<mx+1<<endl;
	return 0;
}


//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<stdio.h>
using namespace std;
ifstream fin("helpcross.in");
ofstream fout("helpcross.out");
inline int read(){
	int k=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if (ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){k=k*10+ch-'0';ch=getchar();}
	return f*k;
}
struct niu{
	int a,b;
}cow[100003];
struct xx{
	int v,id;
}T[100003];
inline bool cmp(const xx a,const xx b){
	return a.v<=b.v;
}
int g[5003][5003],y[5003],link[5003];
int ans,c,n,tim;
bool find(int v){
	for(int i=1;i<=c;i++){
		if(g[v][i]&&y[i]!=tim){
			y[i]=tim;
			if(!link[i]||find(link[i])){
				link[i]=v;
				return 1;
			}
		}
	}
	return 0;
}
int main(){
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	c=read(),n=read();
	for(int i=1;i<=c;i++){
		T[i].v=read();
		T[i].id=i;
	}
	for(int i=1;i<=n;i++){
		cow[i].a=read();
		cow[i].b=read();
	}
	if(n<=5000&&c<=5000){
		sort(T+1,T+c+1,cmp);
		for(int i=1;i<=n;i++){
			for(int j=1;j<=c;j++){
				if(cow[i].a<=T[j].v&&T[j].v<=cow[i].b){
					g[i][T[j].id]=1;
				}else{
					if(cow[i].a<=T[j-1].v&&T[j-1].v<=cow[i].b&&j!=1){
						break;
					}
				}
			}
		}
		for(int i=1;i<=n;i++){
			tim++;
			if(find(i)){
				ans++;
			}
		}
		fout<<ans<<endl;
		for(int i=1;i<=c;i++){
			if(link[i]){
				fout<<i<<" "<<link[i]<<endl;
			}
		}
	}
	return 0;
}
/*

in:
5 4
7
8
6
2
9
2 5
4 9
0 3
8 13

out:
3
5 2
2 4
4 3

*/

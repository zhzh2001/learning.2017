//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("gpsduel.in");
ofstream fout("gpsduel.out");
int head[100003],next[100003],to[100003],v[100003],tot;
inline void add(int a,int b,int c){
	to[++tot]=b;  next[tot]=head[a];  head[a]=tot;  v[tot]=c;
}
int head2[100003],next2[100003],to2[100003],v2[100003],tot2;
inline void add2(int a,int b,int c){
	to2[++tot2]=b;  next2[tot2]=head2[a];  head2[a]=tot2;  v2[tot2]=c;
}
int a[100003],b[100003],t1[100003],t2[100003];
int a2[100003],b2[100003],t3[100003],t4[100003];
int dist[1000003],dist2[100003],dis1[100003],dis2[100003];
bool vis[100003];
int n,m,w,s,t;
inline void Dijkstra(int s){
	memset(vis,0,sizeof(vis));
	for(int i=head[s];i;i=next[i]){
		dist[to[i]]=v[i];
	}
	vis[s]=1;
	for(int i=1;i<=n-1;i++){
		int vv,Min=1<<30;
		for(int j=1;j<=n;j++){
			if(!vis[j]&&dist[j]<Min){
				Min=dist[j],vv=j;//选择V-U中dist最小的顶点
			}
		}
		vis[vv]=1;//将这个点加入U中
		for(int j=head[vv];j;j=next[j]){
			if(!vis[to[j]]){
				dist[to[j]]=min(dist[to[j]],(dist[vv]+v[j]));//更新相邻顶点的dist
			}
		}
	}
}
inline void Dijkstra2(int s){
	memset(vis,0,sizeof(vis));
	for(int i=head2[s];i;i=next2[i]){
		dist2[to2[i]]=v2[i];
	}
	vis[s]=1;
	for(int i=1;i<=n-1;i++){
		int vv,Min=1<<30;
		for(int j=1;j<=n;j++){
			if(!vis[j]&&dist2[j]<Min){
				Min=dist2[j],vv=j;//选择V-U中dist2最小的顶点
			}
		}
		vis[vv]=1;//将这个点加入U中
		for(int j=head2[vv];j;j=next2[j]){
			if(!vis[to2[j]]){
				dist2[to2[j]]=min(dist2[to2[j]],(dist2[vv]+v2[j]));//更新相邻顶点的dist2
			}
		}
	}
}
int main(){
	//freopen("gpsduel.in","r",stdin);
	//freopen("gpsduel.out","w",stdout);
	fin>>n>>m>>w>>s>>t;
	for(int i=1;i<=m;i++){
		fin>>a[i]>>b[i]>>t1[i]>>t2[i];
	}
	for(int i=1;i<=w;i++){
		fin>>a2[i]>>b2[i]>>t3[i]>>t4[i];
		add(b[i],a[i],t1[i]),add(b2[i],a2[i],t3[i]);
		add2(b[i],a[i],t2[i]),add2(b2[i],a2[i],t4[i]);
	}
	Dijkstra(t);
	for(int i=1;i<=n;i++){
		dis1[i]=dist[i];
	}
	if(v[s]==0){
		fout<<"inf inf -1"<<endl;
		return 0;
	}
	fout<<dis1[s]<<" ";
	Dijkstra2(t);
	for(int i=1;i<=n;i++){
		dis2[i]=dist2[i];
		fout<<dist2[i]<<endl;
	}
	fout<<dis2[s]<<" ";
	int ans;
	ans=5;
	fout<<ans<<endl;
	return 0;
}
/*

in:
6 3 3 1 6
1 2 5 10
3 4 5 10
5 6 10 5
1 3 -10 -100
3 5 -100 -10
4 6 -100 -10

out:
-105 -105 1

*/

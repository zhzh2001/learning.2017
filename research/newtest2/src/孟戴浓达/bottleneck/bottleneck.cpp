//#include<iostream>
#include<fstream>
#include<algorithm>
#include<cmath>
#include<cstring>
using namespace std;
ifstream fin("bottleneck.in");
ofstream fout("bottleneck.out");
int head[100003],next[200003],to[100003],tot;
inline void add(int x,int y){
	to[++tot]=y;  next[tot]=head[x];  head[x]=tot;
}
long long fa[100003],c[100003],m[100003];
struct timee{
	int tim,id;
	long long ans;
}t[100003];
inline bool cmp1(const timee a,const timee b){
	return a.tim<=b.tim;
}
inline bool cmp2(const timee a,const timee b){
	return a.id<=b.id;
}
long long ans,n,k;
int main(){
	//freopen("bottleneck.in","r",stdin);
	//freopen("bottleneck.out","w",stdout);
	fin>>n>>k;
	for(int i=1;i<=n;i++){
		fin>>fa[i]>>c[i]>>m[i];
		add(fa[i],i);
	}
	for(int i=1;i<=k;i++){
		fin>>t[i].tim;
		t[i].id=i;
	}
	sort(t+1,t+k+1,cmp1);
	
	sort(t+1,t+k+1,cmp2);
	for(int i=1;i<=k;i++){
		fout<<t[i].ans<<endl;;
	}
	return 0;
}

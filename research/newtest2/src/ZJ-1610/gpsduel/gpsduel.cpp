#include<cstdio>
#include<cstring>
#include<algorithm>
#include<vector>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;
const int W=100005;
const int M=500005;

struct data{
	int u,v,w[2];
}B[W+M*2];
int cnt;

int lines,front[N];
struct Edge{
	int next,to,w[2],pos;
}E[W+M*2];
inline void Addline(int u,int v,int w0,int w1,int pos){
	E[++lines]=(Edge){front[u],v,w0,w1,pos};front[u]=lines;
}
int Wei[W+M*2];
int n,m,w,S,T;
ll Dis[N];int Inq[N],Q[N],head,tail;
vector<int>V[N];
int main(){
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout);
	n=read(),m=read(),w=read(),S=read(),T=read();
	Rep(i,1,m){
		int u=read(),v=read(),w0=read(),w1=read();
		B[++cnt]=(data){u,v,w0,w1};
		B[++cnt]=(data){v,u,w0,w1};
	}
	Rep(i,1,w){
		int u=read(),v=read(),w0=read(),w1=read();
		B[++cnt]=(data){u,v,w0,w1};
	}
	Rep(t,0,1){
		lines=0;
		memset(front,0,sizeof(front));
		Rep(i,1,cnt) Addline(B[i].v,B[i].u,B[i].w[0],B[i].w[1],i);
		memset(Dis,127,sizeof(Dis));
		head=tail=0;
		Dis[Q[++tail]=T]=0;
		while (head!=tail){
			int u=Q[(++head)%=N];
//			printf("at %d dis=%I64d\n",u,Dis[u]);
			for (int i=front[u],v;i!=0;i=E[i].next){
				if (Dis[u]+E[i].w[t]<=Dis[v=E[i].to]){
					ll val=Dis[u]+E[i].w[t];
					if (val<Dis[v]){
						V[v].clear();Dis[v]=val;
					}
					if (val==Dis[v]) V[v].push_back(E[i].pos);
					if (!Inq[v]){
						Inq[v]=true;
						if (Dis[v]<Dis[Q[head]]){
							Q[head]=v;
							head--;
							if (head<0) head+=N;
						}
						else
						Q[(++tail)%=N]=v;
					}
				}
			}
			Inq[u]=false;
		}
		Rep(i,1,n) Rep(j,0,V[i].size()-1) Wei[V[i][j]]++;
		if (Dis[S]<Dis[0]) printf("%I64d ",Dis[S]);
			else printf("inf ");
	}
	lines=0;
	memset(front,0,sizeof(front));
	Rep(i,1,cnt) Addline(B[i].u,B[i].v,2-Wei[i],0,0);
	memset(Dis,127,sizeof(Dis));
	head=tail=0;
	Dis[Q[++tail]=S]=0;
	while (head!=tail){
		int u=Q[(++head)%=N];
		for (int i=front[u],v;i!=0;i=E[i].next){
			if (Dis[u]+E[i].w[0]<=Dis[v=E[i].to]){
				ll val=Dis[u]+E[i].w[0];
				if (val<Dis[v]){
					V[v].clear();Dis[v]=val;
				}
				if (val==Dis[v]) V[v].push_back(E[i].pos);
				if (!Inq[v]){
					Inq[v]=true;Q[(++tail)%=N]=v;
				}
			}
		}
		Inq[u]=false;
	}
	if (Dis[T]<Dis[0]) printf("%I64d\n",Dis[T]);
		else printf("-1\n");
}
/*
6 3 3 1 6
1 2 5 10
3 4 5 10
5 6 10 5
1 3 -10 -100
3 5 -100 -10
4 6 -100 -10
*/

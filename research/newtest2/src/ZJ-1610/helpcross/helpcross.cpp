#include<cstdio>
#include<cstring>
#include<algorithm>
#include<set>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;
typedef pair<int,int> PII;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=100005;

struct node{
	int a,b,pos;
}C[N],B[N];
inline bool Cmp(node A,node B){
	return A.a<B.a;
}
multiset<PII>Set;
int c,n,P[N];
int main(){
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	c=read(),n=read();
	Rep(i,1,c) C[i].a=read(),C[i].pos=i;
	Rep(i,1,n) B[i].a=read(),B[i].b=read(),B[i].pos=i;
	sort(C+1,C+c+1,Cmp);
	sort(B+1,B+n+1,Cmp);
	for (int i=1,j=1;i<=c;i++){
		while (j<=n && B[j].a<=C[i].a){
			Set.insert(make_pair(B[j].b,B[j].pos));j++;
		}
		while (!Set.empty()){
			multiset<PII>::iterator val=Set.begin();
			if (val->first>=C[i].a) break;
				else Set.erase(val);
		}
		if (Set.empty()) continue;
		multiset<PII>::iterator val=Set.begin();
		P[val->second]=C[i].pos;
		Set.erase(val);
	}
	int res=0;
	Rep(i,1,n) if (P[i]) res++;
	printf("%d\n",res);
	Rep(i,1,n) if (P[i]) printf("%d %d\n",i,P[i]);
}
/*
5 4
7
8
6
2
9
2 5
4 9
0 3
8 13
*/

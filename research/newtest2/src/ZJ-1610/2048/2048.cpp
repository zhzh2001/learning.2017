#include<cstdio>
#include<cstring>
#include<algorithm>
#include<cstdlib>
#include<ctime>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=262145;
int n,A[N],S[N],Ans;
void Work(int pos){
	*S=0;
	Rep(i,pos,n){
		S[++*S]=A[i];
		if (A[i+1]!=S[*S]){
			while (*S>=2 && S[*S]==S[*S-1]) S[--*S]++;
		}
		Ans=max(Ans,S[*S]);
	}
}
struct data{
	int val,pos;
}P[N];
inline bool Cmp(data A,data B){
	return A.val>B.val;
}
int main(){
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout);
	n=read();
	Rep(i,1,n) A[i]=read();
	if (n<=250){
		Rep(i,1,n) Work(i);
	}
	else{
		int con=150;
		srand(time(0));
		Rep(i,1,con){
			int pos=(rand()<<15|rand())%n+1;
//			Work(pos);
		}
//		Work(1);
		Rep(i,1,n) P[i]=(data){A[i],i};
		sort(P+1,P+n+1,Cmp);
//		Rep(i,1,con) Work(P[i].pos);
	}
	printf("%d\n",Ans);
}
/*
4
1 1 1 2
*/

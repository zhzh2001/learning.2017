#include<cstdio>
#include<cstring>
#include<algorithm>

#define Rep(i,x,y) for (int i=x,_Lim=y;i<=_Lim;i++)
#define Dep(i,x,y) for (int i=x,_Lim=y;i>=_Lim;i--)

using namespace std;

typedef long long ll;

inline int read(){
	int f=1,res=0,ch=getchar();
	while (ch<'0' || ch>'9'){
		if (ch=='-') f=-1;ch=getchar();
	}
	while (ch>='0' && ch<='9') res=res*10+ch-48,ch=getchar();
	return f*res;
}
const int N=10005;
const ll INF=1e18;

int lines,front[N];
struct Edge{
	int next,to;ll cap;
}E[N*4];
inline void Addline(int u,int v,ll c){
	E[++lines]=(Edge){front[u],v,c};front[u]=lines;
	E[++lines]=(Edge){front[v],u,0};front[v]=lines;
}
int n,k,Start,End;
int P[N],C[N],M[N];
int Q[N],dep[N];
bool Bfs(){
	memset(dep,0,sizeof(dep));
	int head=0,tail=0;
	dep[Q[++tail]=Start]=1;
	while (head<tail){
		int u=Q[++head];
		if (u==End) return true;
		for (int i=front[u],v;i!=0;i=E[i].next){
			if (E[i].cap && !dep[v=E[i].to]) dep[Q[++tail]=v]=dep[u]+1;
		}
	}
	return false;
}
ll Dfs(int u,ll flow){
	if (u==End) return flow;
	if (!flow) return 0;
	ll res=0,tmp;
	for (int i=front[u],v;i!=0;i=E[i].next){
		if (dep[v=E[i].to]==dep[u]+1 && (tmp=Dfs(v,min(E[i].cap,flow-res)))){
			E[i].cap-=tmp;
			E[i^1].cap+=tmp;
			res+=tmp;
		}
	}
	if (res<flow) dep[u]=-1;
	return res;
}
ll Dinic(){
	ll res=0;
	while (Bfs()) res+=Dfs(Start,INF);
	return res;
}
int main(){
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);
	n=read(),k=read();
	Rep(i,2,n) P[i]=read(),C[i]=read(),M[i]=read();
	Start=n+1,End=n+2;
	Rep(ik,1,k){
		int t=read();
		lines=1;
		memset(front,0,sizeof(front));
		Rep(i,2,n){
			Addline(i,P[i],1ll*M[i]*t);
			Addline(Start,i,C[i]);
		}
		Addline(1,End,INF);
		printf("%I64d\n",Dinic());
	}
}

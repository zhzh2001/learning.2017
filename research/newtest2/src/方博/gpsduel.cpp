#include<bits/stdc++.h>
const int N=100005;
const int M=600005;
using namespace std;
int tt;
int t;
int n,m,w,s;
int dist1[N],dist2[N],dist[N];
int from[M],head[N],next[M],tail[M],e[M],e1[M],e2[M];
bool inq[N];
int read()
{
	int x=0;
	int f=1;
	char c=getchar();
	while(c>'9'||c<'0'){
		if(c=='-')f=-1;
		c=getchar();
	}
	while(c>='0'&&c<='9'){
		x=x*10+c-'0';
		c=getchar();
	}
	return x*f;
}
void addto(int y,int x,int z1,int z2)
{
	t++;
	from[t]=x;
	next[t]=head[x];
	head[x]=t;
	tail[t]=y;
	e1[t]=z1;
	e2[t]=z2;
}
void spfa1(int s){
	memset(dist1,127/3,sizeof dist1);
	memset(inq,0,sizeof inq);
	queue<int>Q;
	dist1[s]=0;
	Q.push(s);
	while (!Q.empty()){
		int u=Q.front();
		inq[u]=0;
		Q.pop();
//		cout<<u<<' ';
//		cout<<dist1[u]<<endl;
		for (int i=head[u];i;i=next[i]){
			int v=tail[i];
			if (dist1[u]+e1[i]<dist1[v]){
				dist1[v]=dist1[u]+e1[i];
//				cout<<u<<' '<<e1[i]<<' '<<dist1[u]<<endl;
				if (!inq[v]){Q.push(v);inq[v]=1;}
			}
		}
	}
}
void spfa2(int s){
	memset(dist2,127/3,sizeof dist2);
	memset(inq,0,sizeof inq);
	queue<int>Q;
	dist2[s]=0;
	Q.push(s);
	while (!Q.empty()){
		int u=Q.front();inq[u]=0;
		Q.pop();
		for (int i=head[u];i;i=next[i]){
			int v=tail[i];
			if (dist2[u]+e2[i]<dist2[v]){
				dist2[v]=dist2[u]+e2[i];
				if (!inq[v]){Q.push(v);inq[v]=1;}
			}
		}
	}
}
void spfa(int s){
	memset(dist,127/3,sizeof dist);
	memset(inq,0,sizeof inq);
	queue<int>Q;
	dist[s]=0;
	Q.push(s);
	while (!Q.empty()){
		int u=Q.front();inq[u]=0;
		Q.pop();
		for (int i=head[u];i;i=next[i]){
			int v=tail[i];
			if (dist[u]+e[i]<dist[v]){
				dist[v]=dist[u]+e[i];
				if (!inq[v]){Q.push(v);inq[v]=1;}
			}
		}
	}
}
signed main()
{
	freopen("gpsduel.in","r",stdin);
	freopen("gpsduel.out","w",stdout); 
	n=read();
	m=read();
	w=read();
	s=read();
	tt=read();
	for(int i=1;i<=m;i++){
		int x=read();
		int y=read();
		int z1=read();
		int z2=read();
		addto(x,y,z1,z2);
	}
	for(int i=1;i<=w;i++){
		int x=read();
		int y=read();
		int z1=read();
		int z2=read();
		addto(x,y,z1,z2);
	}
	spfa1(tt);
//	for(int i=1;i<=n;i++){
//		cout<<dist1[i]<<' ';
//	}
//	cout<<endl;
	spfa2(tt);
//	for(int i=1;i<=n;i++){
//		cout<<dist2[i]<<' ';
//	}
//	cout<<endl;
	for(int i=1;i<=m+w;i++){
//		cout<<from[i]<<' '<<tail[i]<<endl;
//		cout<<' '<<e1[i]<<' '<<e2[i]<<endl;
		if(dist1[from[i]]+e1[i]!=dist1[tail[i]])e[i]++;
		if(dist2[from[i]]+e2[i]!=dist2[tail[i]])e[i]++;
//		cout<<e[i]<<endl;
	}
	spfa(tt);
//	for(int i=1;i<=n;i++){
//		cout<<dist[i]<<' ';
//	}
//	cout<<endl;
	printf("%d %d %d",dist1[s],dist2[s],dist[s]);
	return 0;
}

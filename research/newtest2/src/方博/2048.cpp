#include<bits/stdc++.h>
using namespace std;
int a[270005];
int b[270005];
int nxt[270006];
int lst[270006];
int n;
int main()
{
	freopen("2048.in","r",stdin);
	freopen("2048.out","w",stdout); 
	scanf("%d",&n);
	for(int i=1;i<=n;i++){
		scanf("%d",&a[i]);
		b[i]=a[i];
		nxt[i]=i-1;
		lst[i]=i+1;
	}
	for(int i=1;i<=n;i++)
		for(int j=1;j<=n;j=lst[j])
			if(a[j]==a[nxt[j]]){
				a[nxt[j]]++;
				nxt[lst[j]]=nxt[j];
				lst[nxt[j]]=lst[j];
			}
	for(int i=1;i<=n;i++){
		nxt[i]=i-1;
		lst[i]=i+1;
	}
	for(int i=1;i<=n;i++)
		for(int j=n;j>=1;j=nxt[j])
			if(a[j]==a[lst[j]]){
				a[lst[j]]++;
				nxt[lst[j]]=nxt[j];
				lst[nxt[j]]=lst[j];
			}
	int ans=0;
	for(int i=1;i<=n;i++){
		ans=max(ans,a[i]);
		ans=max(ans,b[i]);
	}
	printf("%d",ans);
	return 0;
}

#include<bits/stdc++.h>
using namespace std;
const int M=100005;
int n,k;
struct xxx{
	int ans,i,k;
}a[M];
int sum[M];
int t;
int head[M],next[M],tail[M],e[M];
void addto(int x,int y,int z)
{
	t++;
	next[t]=head[x];
	head[x]=t;
	tail[t]=y;
}
int dfs(int k,int x)
{
	for(int i=head[k];i;i=next[i])
		sum[k]+=dfs(tail[i],x);
	if(k==1)return 0;
	int ret=min(sum[k],e[k]*x);
	sum[k]=max(0,sum[k]-e[k]*x);
//	cout<<e[k]*x<<' '<<k<<' '<<ret<<'!'<<endl;
	return ret;
}
bool com(xxx a,xxx b)
{
	return a.k<b.k;
}
bool com2(xxx a,xxx b)
{
	return a.i<b.i;
}
signed main()
{
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout); 
	scanf("%d%d",&n,&k);
	for(int i=1;i<n;i++){
		int x,y,z;
		scanf("%d%d%d",&x,&y,&z);
		addto(x,i+1,z);
		sum[i+1]=y;
		e[i+1]=z;
	}
	e[1]=0;
	for(int i=1;i<=k;i++){
		scanf("%d",&a[i].k);
		a[i].i=i;
	}
	sort(a+1,a+k+1,com);
	for(int i=1;i<=k;i++){
		dfs(1,a[i].k-a[i-1].k);
		a[i].ans=sum[1];
//		cout<<sum[1]<<endl;
	}
	sort(a+1,a+k+1,com2);
	for(int i=1;i<=k;i++){
		printf("%d\n",a[i].ans);
	}
	return 0;
}

#include<cstdio>
#include<cstring>
#include<cmath>
#include<algorithm>
using namespace std;
#define N 3010
struct bb{int l,r;
}b[N];
int a[N],g[N][N],y[N],link[N],n,ans,n1,n2,i,j;
bool find(int v){
	for (int i=1;i<=n;++i)
		if (g[v][i]&&!y[i]){
			y[i]=1;
			if (!link[i]||find(link[i])){
				link[i]=v;return 1;
			}
		}
	return 0;
}
inline int read()
{
	int x=0,f=1;
	char ch=getchar();
	while (ch<'0'||ch>'9')
		{
			if (ch=='-') f=-1;
			ch=getchar();
		}
	while (ch<='9'&&ch>='0')
		x=x*10+ch-'0',ch=getchar();
	return x*f;
}
inline void write(int x)
{
	if (x<0) putchar('-'),x=-x;
	if (x>=10) write(x/10);
	putchar(x%10+'0');
}
int main()
{
	freopen("helpcross.in","r",stdin);
	freopen("helpcross.out","w",stdout);
	n1=read();
	n2=read();
	n=max(n1,n2);
	for (i=1;i<=n1;i++) a[i]=read();
	for (i=1;i<=n2;i++)
		{
			b[i].l=read();
			b[i].r=read();
		}
	for (i=1;i<=n1;i++)
		for (j=1;j<=n2;j++)
			if (a[i]<=b[j].r&&a[i]>=b[j].l) g[i][j]=1;
	memset(link,0,sizeof(link));
	for (i=1;i<=n;++i)
		{
			memset(y,0,sizeof(y));
			if (find(i))++ans;
		}
	write(ans);
	puts("");
	for (i=1;i<=n1;i++)
		if (link[i])
			{
				write(link[i]);
				putchar(' ');
				write(i);
				puts("");	
			}
}

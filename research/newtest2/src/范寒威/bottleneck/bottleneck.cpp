#include<iostream>
#include<vector>
#include<cstdio>
#include<cstring>
#include<algorithm>
#define ll long long
#define N 100005
using namespace std;
inline ll read(){
	ll x=0,f=1;char ch=getchar();
	while(ch<'0'||ch>'9'){if(ch=='-')f=-1;ch=getchar();}
	while(ch>='0'&&ch<='9'){x=x*10+ch-'0';ch=getchar();}
	return x*f;
}
ll write(ll x){
	if(x<0) putchar('-'),x=-x;
	if(x>=10) write(x/10);
	putchar(x%10+'0');
}
ll writeln(ll x){
	write(x);
	putchar('\n');
}

struct node{ int t,num; }a[N];
ll ans[N];
ll m[N],c[N],p[N];
vector<ll> edge[N];
ll n,k;
bool vis[N];

bool cmp(node a,node b){ return a.t<b.t; }

bool dfs(int x,int t){
	if(vis[x]) return true;
	bool flag=true;
	for(int i=0;i<edge[x].size();i++){
		if(!dfs(edge[x][i],t)) flag=false;
	}
	if(x!=1){
		ll tmp=t*m[x];
		if(tmp>c[x]){
			c[p[x]]+=c[x];
			c[x]=0;
		}
		else{
			c[p[x]]+=tmp;
			c[x]-=tmp;
		}
		if(flag){
			if(edge[x].size()>0){
				if(c[x]==0) vis[x]=true;
			}
			else if(c[x]==0) vis[x]=true;
		}
		return vis[x];
	}
}

int main(){
	freopen("bottleneck.in","r",stdin);
	freopen("bottleneck.out","w",stdout);
	n=read(); k=read();
	for(ll i=2;i<=n;i++){
		p[i]=read(); c[i]=read(); m[i]=read();
		edge[p[i]].push_back(i);
	}
	for(ll i=1;i<=k;i++) a[i].t=read(),a[i].num=i;
	sort(a+1,a+k+1,cmp);
	for(ll i=k;i;i--) a[i].t-=a[i-1].t;
	for(ll i=1;i<=k;i++){
		dfs(1,a[i].t);
		ans[a[i].num]=c[1];
	}
	for(ll i=1;i<=k;i++) writeln(ans[i]);
	return 0;
}

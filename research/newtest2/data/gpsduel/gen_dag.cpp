#include<bits/stdc++.h>
using namespace std;
ofstream fout("gpsduel.in");
const int n=100000,m=0,w=100000,s=1,t=n;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<' '<<w<<' '<<s<<' '<<t<<endl;
	for(int i=1;i<=w;i++)
	{
		uniform_int_distribution<> dn(1,n);
		int u=dn(gen),v=dn(gen);
		if(u>v)
			swap(u,v);
		uniform_int_distribution<> dw(-100000,100000);
		fout<<u<<' '<<v<<' '<<dw(gen)<<' '<<dw(gen)<<endl;
	}
	return 0;
}
#include<bits/stdc++.h>
using namespace std;
ofstream fout("gpsduel.in");
const int n=100000,pn=90000,m=450000,w=10000,s=1,t=n,W=10000000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<' '<<w<<' '<<s<<' '<<t<<endl;
	for(int i=1;i<pn;i++)
		fout<<i<<' '<<i+1<<' '<<pn-i+1<<' '<<i<<endl;
	for(int i=1;i<=4*pn+1;i++)
	{
		uniform_int_distribution<> du(1,pn-1);
		int u=du(gen);
		uniform_int_distribution<> dv(1,min(20,pn-u));
		int v=u+dv(gen);
		fout<<u<<' '<<v<<' '<<pn*100<<' '<<0<<endl;
	}
	uniform_int_distribution<> dw(-W,W);
	for(int i=pn+1;i<=n;i++)
		fout<<pn<<' '<<i<<' '<<dw(gen)<<' '<<dw(gen)<<endl;
	return 0;
}
#include<fstream>
#include<utility>
#include<vector>
#include<queue>
#include<algorithm>

//fastIntStream.cpp

#include<cstdio>
#include<string>
#include<cctype>

struct Istream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p;
	bool good;
	
	void fetch()
	{
		if(!buf)
			p=buf=new char [bufsz+1];
		size_t sz=fread(buf,1,bufsz,stream);
		p=buf;
		buf[sz]='\0';
		good=sz;
	}
	
	char nextchar()
	{
		if(!*p)
			fetch();
		return *p++;
	}
	
	template<typename Int>
	void readint(Int& x)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		x=0;
		Int sign=1;
		if(c=='-')
			sign=-1,c=nextchar();
		for(;isdigit(c);c=nextchar())
			x=x*10+c-'0';
		x*=sign;
	}
	
	public:
	Istream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL),good(false){}
	
	explicit Istream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"r")),bufsz(bufsz),buf(NULL),p(NULL),good(false)
	{
		fetch();
	}
	
	explicit Istream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(NULL),p(NULL),good(false)
	{
		fetch();
	}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	~Istream()
	{
		close();
		delete [] buf;
	}
	
	operator bool()const
	{
		return good;
	}
	
	bool operator!()const
	{
		return !good;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"r");
		this->bufsz=bufsz;
		fetch();
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		fetch();
		return stream;
	}
	
	Istream& operator>>(short& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned short& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(int& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned int& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(long long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(unsigned long long& value)
	{
		readint(value);
		return *this;
	}
	
	Istream& operator>>(char& c)
	{
		for(c=nextchar();isspace(c);c=nextchar());
		return *this;
	}
	
	Istream& operator>>(std::string& s)
	{
		char c;
		for(c=nextchar();isspace(c);c=nextchar());
		s.clear();
		for(;good&&!isspace(c);c=nextchar())
			s+=c;
		return *this;
	}
	
	friend Istream& getline(Istream& is,std::string& s,char delim='\n')
	{
		char c;
		s.clear();
		for(c=is.nextchar();is.good&&c!=delim;c=is.nextchar())
			s+=c;
		return is;
	}
};

struct Ostream
{
	private:
	static const size_t defaultBufsz=1e6;
	FILE *stream;
	size_t bufsz;
	char *buf,*p,dig[25];
	
	public:
	Ostream():stream(NULL),bufsz(defaultBufsz),buf(NULL),p(NULL){}
	
	explicit Ostream(const std::string& filename,size_t bufsz=defaultBufsz):stream(fopen(filename.c_str(),"w")),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	explicit Ostream(FILE *stream,size_t bufsz=defaultBufsz):stream(stream),bufsz(bufsz),buf(new char [bufsz]),p(buf){}
	
	bool close()
	{
		return !fclose(stream);
	}
	
	void flush()
	{
		fwrite(buf,1,p-buf,stream);
		p=buf;
	}
	
	private:
	void writechar(char c)
	{
		*p++=c;
		if(p==buf+bufsz)
			flush();
	}
	
	template<typename Int>
	void writeint(Int x)
	{
		if(x<0)
			writechar('-'),x=-x;
		int len=0;
		do
			dig[++len]=x%10;
		while(x/=10);
		for(;len;len--)
			writechar(dig[len]+'0');
	}
	
	public:
	~Ostream()
	{
		flush();
		close();
		delete [] buf;
	}
	
	bool open(const std::string& filename,size_t bufsz=defaultBufsz)
	{
		stream=fopen(filename.c_str(),"w");
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	bool open(FILE *stream,size_t bufsz=defaultBufsz)
	{
		this->stream=stream;
		this->bufsz=bufsz;
		delete [] buf;
		p=buf=new char [bufsz];
		return stream;
	}
	
	Ostream& operator<<(short value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned short value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(int value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned int value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(long long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(unsigned long long value)
	{
		writeint(value);
		return *this;
	}
	
	Ostream& operator<<(char c)
	{
		writechar(c);
		return *this;
	}
	
	Ostream& operator<<(const std::string& s)
	{
		for(size_t i=0;i<s.length();i++)
			writechar(s[i]);
		return *this;
	}
	
	Ostream& operator<<(Ostream& (*func)(Ostream&))
	{
		return func(*this);
	}
	
	friend Ostream& endl(Ostream& os);
};

Ostream& endl(Ostream& os)
{
	os.writechar('\n');
	return os;
}

//solution

using std::pair;
using std::vector;
using std::queue;
using std::fill;
using std::priority_queue;
using std::greater;
using std::make_pair;
using std::min;
Istream fin("gpsduel.in");
Ostream fout("gpsduel.out");
const int N=100005,INF=0x3f3f3f3f;
const long long INFll=0x3f3f3f3f3f3f3f3fll;
typedef pair<int,int> pii;
typedef pair<long long,int> plli;
vector<pii> road1[N],road2[N],worm1[N],worm2[N];
int cc,belong[N],indeg[N];
vector<int> comp[N];
void floodfill(int s)
{
	queue<int> Q;
	Q.push(s);
	belong[s]=cc;
	comp[cc].push_back(s);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		for(vector<pii>::iterator it=road1[k].begin();it!=road1[k].end();++it)
			if(!belong[it->first])
			{
				belong[it->first]=cc;
				comp[cc].push_back(it->first);
				Q.push(it->first);
			}
	}
}
long long d1[N],d2[N];
bool vis[N];
void topo_dijkstra(vector<pii> road[],vector<pii> worm[],long long d[],int s,int n)
{
	fill(indeg+1,indeg+n+1,0);
	for(int i=1;i<=n;i++)
		for(vector<pii>::iterator it=worm[i].begin();it!=worm[i].end();++it)
			indeg[belong[it->first]]++;
	fill(vis+1,vis+n+1,false);
	fill(d+1,d+n+1,INFll);
	d[s]=0;
	queue<int> Q;
	for(int i=1;i<=cc;i++)
		if(!indeg[i])
			Q.push(i);
	while(!Q.empty())
	{
		int k=Q.front();Q.pop();
		priority_queue<plli,vector<plli>,greater<plli> > PQ;
		for(vector<int>::iterator it=comp[k].begin();it!=comp[k].end();++it)
			if(d[*it]<INF)
				PQ.push(make_pair(d[*it],*it));
		while(!PQ.empty())
		{
			plli k=PQ.top();PQ.pop();
			if(vis[k.second])
				continue;
			vis[k.second]=true;
			for(vector<pii>::iterator it=road[k.second].begin();it!=road[k.second].end();++it)
				if(d[k.second]+it->second<d[it->first])
					PQ.push(make_pair(d[it->first]=d[k.second]+it->second,it->first));
			for(vector<pii>::iterator it=worm[k.second].begin();it!=worm[k.second].end();++it)
				d[it->first]=min(d[it->first],d[k.second]+it->second);
		}
		for(vector<int>::iterator it=comp[k].begin();it!=comp[k].end();++it)
			for(vector<pii>::iterator e=worm[*it].begin();e!=worm[*it].end();++e)
				if(!--indeg[belong[e->first]])
					Q.push(belong[e->first]);
	}
}
int d[N];
vector<pii> mat[N];
void dijkstra(int s,int n)
{
	fill(d+1,d+n+1,INF);
	d[s]=0;
	fill(vis+1,vis+n+1,false);
	priority_queue<pii,vector<pii>,greater<pii> > Q;
	Q.push(make_pair(0,s));
	while(!Q.empty())
	{
		pii k=Q.top();Q.pop();
		if(vis[k.second])
			continue;
		vis[k.second]=true;
		for(vector<pii>::iterator it=mat[k.second].begin();it!=mat[k.second].end();++it)
			if(d[k.second]+it->second<d[it->first])
				Q.push(make_pair(d[it->first]=d[k.second]+it->second,it->first));
	}
}
int ansb[N];
int main()
{
	int n,m,w,s,t;
	fin>>n>>m>>w>>s>>t;
	while(m--)
	{
		int u,v,w1,w2;
		fin>>u>>v>>w1>>w2;
		road1[u].push_back(make_pair(v,w1));
		road1[v].push_back(make_pair(u,w1));
		road2[u].push_back(make_pair(v,w2));
		road2[v].push_back(make_pair(u,w2));
	}
	while(w--)
	{
		int u,v,w1,w2;
		fin>>u>>v>>w1>>w2;
		worm1[v].push_back(make_pair(u,w1));
		worm2[v].push_back(make_pair(u,w2));
	}
	for(int i=1;i<=n;i++)
		if(!belong[i])
		{
			++cc;
			floodfill(i);
		}
	topo_dijkstra(road1,worm1,d1,t,n);
	topo_dijkstra(road2,worm2,d2,t,n);
	for(int i=1;i<=n;i++)
	{
		for(int j=0;j<road1[i].size();j++)
		{
			int u=road1[i][j].first,delta1=d1[u]-d1[i],delta2=d2[u]-d2[i];
			mat[u].push_back(make_pair(i,(road1[i][j].second>delta1)+(road2[i][j].second>delta2)));
		}
		for(int j=0;j<worm1[i].size();j++)
		{
			int u=worm1[i][j].first,delta1=d1[u]-d1[i],delta2=d2[u]-d2[i];
			mat[u].push_back(make_pair(i,(worm1[i][j].second>delta1)+(worm2[i][j].second>delta2)));
		}
	}
	dijkstra(s,n);
	if(d1[s]==INFll)
		fout<<"inf ";
	else
		fout<<d1[s]<<' ';
	if(d2[s]==INFll)
		fout<<"inf ";
	else
		fout<<d2[s]<<' ';
	if(d[t]==INF)
		fout<<-1<<endl;
	else
		fout<<d[t]<<endl;
	return 0;
}
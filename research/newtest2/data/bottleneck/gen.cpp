#include<bits/stdc++.h>
using namespace std;
ofstream fout("bottleneck.in");
const int n=100000,k=100,t=1e9;
int f[n+5];
int getf(int x)
{
	return f[x]==x?x:f[x]=getf(f[x]);
}
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<k<<endl;
	for(int i=1;i<=n;i++)
		f[i]=i;
	for(int i=2;i<=n;i++)
	{
		/* int ri=getf(i);
		for(;;)
		{
			uniform_int_distribution<> dn(1,n);
			int v=dn(gen),rv=getf(v);
			if(ri!=rv)
			{
				f[ri]=rv;
				fout<<v<<' ';
				break;
			}
		} */
		uniform_int_distribution<> dc(1,1e9),dm(1,1000);
		fout<<i-1<<' '<<dc(gen)<<' '<<dm(gen)<<endl;
	}
	for(int i=1;i<=k;i++)
	{
		uniform_int_distribution<> dt(1,t);
		fout<<dt(gen)<<endl;
	}
	return 0;
}
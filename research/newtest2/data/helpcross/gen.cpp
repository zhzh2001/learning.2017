#include<bits/stdc++.h>
using namespace std;
ofstream fout("helpcross.in");
const int n=200000,m=200000;
int main()
{
	minstd_rand gen(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=n;i++)
	{
		uniform_int_distribution<> d(2e8,6e8);
		fout<<d(gen)<<endl;
	}
	for(int i=1;i<=m;i++)
	{
		uniform_int_distribution<> d(0,1e9);
		int l=d(gen),r=d(gen);
		if(l>r)
			swap(l,r);
		fout<<l<<' '<<r<<endl;
	}
	return 0;
}
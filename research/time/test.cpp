#include<bits/stdc++.h>
#include<windows.h>
using namespace std;
using namespace std::chrono;
typedef unsigned long long ull;
inline void asm_clock(ull& t)
{
	__asm__ __volatile__ ("rdtsc\n\t":"=A"(t):);
}
int main()
{
	//time_t t=clock();
	
	auto t1=steady_clock::now();
	
	ull s;
	asm_clock(s);
	
	LARGE_INTEGER freq,t3,t4;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&t3);
	
	//high_resolution_clock::time_point th1=high_resolution_clock::now();
	
	Sleep(1000);
	
	//cout<<"using ctime:"<<clock()-t<<"ms\n";
	
	
	auto t2=steady_clock::now();
	auto d=duration_cast<duration<int,nano>>(t2-t1);
	cout<<"using steady_clock:"<<d.count()<<"ns\n";
	
	ull t;
	asm_clock(t);
	cout<<"using rdtsc:"<<t-s<<endl;
	
	QueryPerformanceCounter(&t4);
	cout<<"using PerformanceCounter from windows.h:"<<(double)(t4.QuadPart-t3.QuadPart)/freq.QuadPart<<endl;
	
	/*
	high_resolution_clock::time_point th2=high_resolution_clock::now();
	duration<double> dh=duration_cast<duration<double>>(th2-th1);
	cout<<"using high_resolution_clock:"<<dh.count()<<"s\n";
	*/
	return 0;
}
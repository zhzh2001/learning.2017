#include<bits/stdc++.h>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
vector<int> *mat;
int main()
{
	int n,m;
	fin>>n>>m;
	mat=new vector<int> [n+1];
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		mat[u].push_back(v);
	}
	for(int i=1;i<=n;i++)
	{
		for(vector<int>::iterator j=mat[i].begin();j!=mat[i].end();++j)
			fout<<*j<<' ';
		fout<<endl;
	}
	delete [] mat;
	return 0;
}
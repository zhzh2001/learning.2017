#include<bits/stdc++.h>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
list<int> *mat;
int main()
{
	int n,m;
	fin>>n>>m;
	mat=new list<int> [n+1];
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		mat[u].push_back(v);
	}
	for(int i=1;i<=n;i++)
	{
		for(list<int>::iterator j=mat[i].begin();j!=mat[i].end();++j)
			fout<<*j<<' ';
		fout<<endl;
	}
	delete [] mat;
	return 0;
}
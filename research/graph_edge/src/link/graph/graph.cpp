#include<bits/stdc++.h>
using namespace std;
ifstream fin("graph.in");
ofstream fout("graph.out");
int *head,*v,*nxt,e;
inline void add_edge(int u,int v)
{
	::v[++e]=v;
	nxt[e]=head[u];
	head[u]=e;
}
int main()
{
	int n,m;
	fin>>n>>m;
	head=new int [n+1];
	memset(head+1,0,n*sizeof(int));
	v=new int [m+1];
	nxt=new int [m+1];
	int *a=new int [m+1];
	e=0;
	while(m--)
	{
		int u,v;
		fin>>u>>v;
		add_edge(u,v);
	}
	for(int i=1;i<=n;i++)
	{
		int cc=0;
		for(int j=head[i];j;j=nxt[j])
			a[++cc]=v[j];
		for(;cc;cc--)
			fout<<a[cc]<<' ';
		fout<<endl;
	}
	delete [] head;
	delete [] v;
	delete [] nxt;
	return 0;
}
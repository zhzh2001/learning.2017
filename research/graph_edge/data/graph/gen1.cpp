#include<bits/stdc++.h>
using namespace std;
const int n=100000,m=1000000;
ofstream fout("graph1.in");
#ifdef __WIN32
int rand32()
{
	return (rand()<<16)+rand();
}
#endif
int main()
{
	srand(time(NULL));
	fout<<n<<' '<<m<<endl;
	for(int i=1;i<=m;i++)
#ifdef __linux
		fout<<rand()%n+1<<' '<<rand()%n+1<<endl;
#endif
#ifdef __WIN32
		fout<<rand32()%n+1<<' '<<rand32()%n+1<<endl;
#endif
	return 0;
}
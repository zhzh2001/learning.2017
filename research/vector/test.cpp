#include<bits/stdc++.h>
using namespace std;
int main()
{
	srand(time(NULL));
	int n;
	cin>>n;
	vector<int> a;
	
	clock_t t=clock();
	for(int i=1;i<=n;i++)
		a.insert(a.begin(),rand());
	cout<<"vector::insert() "<<n<<" values:"<<clock()-t<<endl;
	
	t=clock();
	for(int i=1;i<=n;i++)
		a.erase(a.begin());
	cout<<"vector::erase() "<<n<<" values:"<<clock()-t<<endl;
	
	t=clock();
	int* b=new int [n];
	b[0]=rand();
	for(int i=1;i<n;i++)
	{
		for(int j=i;j;j--)
			b[j]=b[j-1];
		b[0]=rand();
	}
	cout<<"array::insert() "<<n<<" values:"<<clock()-t<<endl;
	
	t=clock();
	for(int i=0;i<n;i++)
		for(int j=1;j<=i;j++)
			b[j-1]=b[j];
	cout<<"array::erase() "<<n<<" values:"<<clock()-t<<endl;
	
	return 0;
}
#include <cstddef>
template <size_t N>
class fib : virtual public fib<N - 1>, virtual public fib<N - 2>
{
  public:
	virtual ~fib() {}
};

template <>
class fib<0>
{
};
template <>
class fib<1>
{
};

int main()
{
	fib<233> f;
	return 0;
}
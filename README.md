# Learning

## 目录说明：

- c++实用代码
- class 课件 *(按ftp上的名称命名)*
- doc **重要**的文档，**存在版权问题**
- oj oj上自己提交的题目
- research 研究课题，主要比较各种算法效率 *，以及进行极限编程*
- script 常用脚本
- templates 模板
- test 模拟赛，包含所有选手源文件，可直接用 *[CCR-Plus](https://github.com/zhzh2001/CCR-Plus)* 评测
- tools 实用工具及项目*(原创)*
- work 作业，来自各oj，所有USACO月赛题已经归入oj/luogu/usaco/month

abandoned, see zhzh2001/Learning.2018